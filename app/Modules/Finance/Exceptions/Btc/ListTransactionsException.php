<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * ListTransactionsException.
 */
final class ListTransactionsException extends BtcException
{
}
