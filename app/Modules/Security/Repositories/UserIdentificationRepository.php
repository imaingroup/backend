<?php
declare(strict_types=1);

namespace App\Modules\Security\Repositories;

use App\Facades\DB;
use App\Modules\Security\Models\UserIdentification;
use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Security\Models\UserIdentificationRelation;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

/**
 * User identification repository.
 */
final class UserIdentificationRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param UserIdentification $model
     */
    public function __construct(protected UserIdentification $model)
    {
    }

    /**
     * Get ips by user.
     *
     * @param int $userId
     * @param bool $isAccurate
     * @return Collection
     */
    public function getIpsByUser(int $userId, bool $isAccurate): Collection
    {
        return $this->model
            ->newQuery()
            ->select('ip', 'country', DB::raw('MIN(created_at) as first_date'))
            ->whereIn('id', function (Builder $builder) use ($userId, $isAccurate) {
                $builder->select('users_identifications_id')
                    ->from((new UserIdentificationRelation())->getTable())
                    ->where('users_id', $userId)
                    ->where('is_accurate', $isAccurate);
            })
            ->groupBy('ip', 'country')
            ->orderByRaw('first_date')
            ->get();
    }

    /**
     * Get browsers by user.
     *
     * @param int $userId
     * @param bool $isAccurate
     * @return Collection
     */
    public function getBrowsersByUser(int $userId, bool $isAccurate): Collection
    {
        return $this->model
            ->newQuery()
            ->select('browser.user_agent', DB::raw('MIN(users_identifications.created_at) as first_date'))
            ->join('users_identifications_user_agent as browser', 'users_identifications.user_agent_id', '=', 'browser.id')
            ->whereIn('users_identifications.id', function (Builder $builder) use ($userId, $isAccurate) {
                $builder->select('users_identifications_id')
                    ->from((new UserIdentificationRelation())->getTable())
                    ->where('users_id', $userId)
                    ->where('is_accurate', $isAccurate);
            })
            ->groupBy('browser.user_agent')
            ->orderByRaw('first_date')
            ->get();
    }

    /**
     * Count history by user.
     *
     * @param int $userId
     * @param bool $isAccurate
     * @return int
     */
    public function countHistoryByUser(int $userId, bool $isAccurate): int
    {
        return $this->model
            ->newQuery()
            ->whereIn('id', function (Builder $builder) use ($userId, $isAccurate) {
                $builder->select('users_identifications_id')
                    ->from((new UserIdentificationRelation())->getTable())
                    ->where('users_id', $userId)
                    ->where('is_accurate', $isAccurate);
            })
            ->count(['id']);
    }

    /**
     * Get history by user.
     *
     * @param int $userId
     * @param bool $isAccurate
     * @param Collection $fields
     * @return Collection
     */
    public function getHistoryByUser(int $userId, bool $isAccurate, Collection $fields): Collection
    {
        return $this->model
            ->newQuery()
            ->select($fields->toArray())
            ->with('userAgent')
            ->whereIn('id', function (Builder $builder) use ($userId, $isAccurate) {
                $builder->select('users_identifications_id')
                    ->from((new UserIdentificationRelation())->getTable())
                    ->where('users_id', $userId)
                    ->where('is_accurate', $isAccurate);
            })
            ->orderBy('id', 'DESC')
            ->get();
    }

    /**
     * @param string $searchColumn
     * @param mixed $value
     * @param string $field
     * @return Collection
     */
    public function getIdentifiersByColumn(string $searchColumn, mixed $value, string $field): Collection
    {
        $query = $this->model
            ->newQuery()
            ->select($field);

        if ($value instanceof Collection || is_array($value)) {
            if (count($value) === 0) {
                return collect();
            }

            $query->whereIn($searchColumn, $value);
        } else {
            $query->where($searchColumn, $value);
        }

        return $query->whereNotNull($field)
            ->groupBy($field)
            ->get()
            ->pluck($field);
    }

    /**
     * Get users ids by collection.
     * Id collection empty this method
     * will return empty collection.
     *
     * @param Collection|array $collection
     * @return Collection
     */
    public function getIdsByCollection(Collection|array $collection): Collection
    {
        $allowQuery = false;

        $query = $this->model
            ->newQuery()
            ->select('id', 'users_id')
            ->where(function (\Illuminate\Database\Eloquent\Builder $builder) use ($collection, &$allowQuery) {
                foreach ($collection as $key => $items) {
                    if (count($items) > 0) {
                        $builder->orWhereIn($key, $items);

                        $allowQuery = true;
                    }
                }
            });

        return $allowQuery ?
            $query
                ->get() :
            collect();
    }

    /**
     * Get users ids by collection.
     * Id collection empty this method
     * will return empty collection.
     *
     * @param Collection|array $collection
     * @return Collection
     */
    public function getUserIdsByCollection(Collection|array $collection): Collection
    {
        $allowQuery = false;

        $query = $this->model
            ->newQuery()
            ->select('users_id')
            ->whereNotNull('users_id')
            ->where(function (\Illuminate\Database\Eloquent\Builder $builder) use ($collection, &$allowQuery) {
                foreach ($collection as $key => $items) {
                    if (count($items) > 0) {
                        $builder->orWhereIn($key, $items);

                        $allowQuery = true;
                    }
                }
            })
            ->groupBy('users_id');

        return $allowQuery ?
            $query
                ->get()
                ->pluck('users_id') :
            collect();
    }

    /**
     * Get data by collection.
     *
     * @param Collection|array $collection
     * @return Collection
     */
    public function getByCollection(Collection|array $collection): Collection
    {
        $allowQuery = false;

        $query = $this->model
            ->newQuery()
            ->select('*')
            ->addSelect([
                'user_real_id' => $this->model
                    ->newQuery()
                    ->select('id')
                    ->from('users')
                    ->whereColumn('users.id', 'users_identifications.users_id')
                    ->orWhereColumn('users.email', 'users_identifications.userid')
                    ->orWhereColumn('users.username', 'users_identifications.userid')
                    ->limit(1)
            ])
            ->where(function (\Illuminate\Database\Eloquent\Builder $builder) use ($collection, &$allowQuery) {
                foreach ($collection as $key => $items) {
                    if (count($items) > 0) {
                        $builder->orWhereIn($key, $items);

                        $allowQuery = true;
                    }
                }
            })
            ->orderBy('id', 'ASC');

        return $allowQuery ?
            $query
                ->get() :
            collect();
    }
}
