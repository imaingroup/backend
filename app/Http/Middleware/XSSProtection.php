<?php


namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;

/**
 * This middleware protect about xss.
 */
final class XSSProtection
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (!str_contains($request->url(), config('voyager.prefix'))) {
            $input = $request->all();

            array_walk_recursive($input, function (&$input) {
                if (is_string($input)) {
                    $input = strip_tags($input);
                }
            });

            $request->merge($input);
        }

        return $next($request);
    }
}
