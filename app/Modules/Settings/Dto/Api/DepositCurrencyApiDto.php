<?php
declare(strict_types=1);

namespace App\Modules\Settings\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Finance\Contracts\Enum\NetworkEnumContract;
use App\Modules\Finance\Dto\Api\TokenApiDto;
use BackedEnum;

/**
 * DepositCurrencyApiDto.
 *
 * @property-read int $id
 * @property-read string $name
 * @property-read string $fullName
 * @property-read NetworkEnumContract&BackedEnum $network
 * @property-read float $depositMin
 * @property-read bool $depositBlock
 * @property-read int $confirmationsMin
 * @property-read float $luftDeposit
 * @property-read float $luftWithdraw
 * @property-read float|null $rate
 * @property-read bool $customRate
 * @property-read string|null $image
 * @property-read int $order
 * @property-read TokenApiDto $token
 */
final class DepositCurrencyApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $id
     * @param string $name
     * @param string $fullName
     * @param BackedEnum&NetworkEnumContract $network
     * @param float $depositMin
     * @param bool $depositBlock
     * @param int $confirmationsMin
     * @param float $luftDeposit
     * @param float $luftWithdraw
     * @param float|null $rate
     * @param bool $customRate
     * @param string|null $image
     * @param int $order
     * @param TokenApiDto $token
     */
    public function __construct(
        protected int                         $id,
        protected string                         $name,
        protected string                         $fullName,
        protected NetworkEnumContract&BackedEnum $network,
        protected float                          $depositMin,
        protected bool                           $depositBlock,
        protected int                            $confirmationsMin,
        protected float                          $luftDeposit,
        protected float                          $luftWithdraw,
        protected ?float                         $rate,
        protected bool                           $customRate,
        protected ?string                        $image,
        protected int                            $order,
        protected TokenApiDto                   $token,
    )
    {

    }
}
