<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Crypt;

/**
 * This migration encrypt google2fa_secret
 * field in database.
 */
final class EncryptGoogle2FaSecretUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::beginTransaction();

        foreach (DB::table('users')
                     ->whereNotNull('google2fa_secret')
                     ->get() as $item) {
            DB::table('users')
                ->where('id', $item->id)
                ->lockForUpdate()
                ->first();

            DB::table('users')
                ->where('id', $item->id)
                ->update(
                    [
                        'google2fa_secret' => Crypt::encryptString($item->google2fa_secret)
                    ]
                );
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::beginTransaction();

        foreach (DB::table('users')
                     ->whereNotNull('google2fa_secret')
                     ->get() as $item) {
            DB::table('users')
                ->where('id', $item->id)
                ->lockForUpdate()
                ->first();

            DB::table('users')
                ->where('id', $item->id)
                ->update(
                    [
                        'google2fa_secret' => Crypt::decryptString($item->google2fa_secret)
                    ]
                );
        }

        DB::commit();
    }
}
