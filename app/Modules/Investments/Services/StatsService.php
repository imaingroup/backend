<?php
declare(strict_types=1);

namespace App\Modules\Investments\Services;

use App\Modules\Investments\Models\TradeOption;
use App\Modules\Investments\Repositories\TradeOptionRepository;
use App\Modules\Investments\Repositories\TidHistoryTradeOptionRepository;
use DateTime;
use Exception;

/**
 * Investments stats service.
 */
final class StatsService
{
    /**
     * Update previous percent.
     *
     * @throws Exception
     */
    public function updatePreviousPercent(): void
    {
        throw new \RuntimeException();

        $nowDate = new DateTime((new DateTime())->format('Y-m-d 00:00:00'));
        $startDateBase = new DateTime();

        if ((int)$nowDate->format('d') === 1) {
            $startDateBase->modify('-1 DAY');
        }

        $startDate = (new DateTime($startDateBase->format('Y-m-01 00:00:00')));
        $endDate = (clone $startDate)->modify('+1 MONTH -1 DAY');
        $startDate->setDate(
            (int)$startDate->format('m')-1 === 0 ? (int)$startDate->format('Y')-1 : (int)$startDate->format('Y'),
            (int)$startDate->format('m')-1 === 0 ? 12 : (int)$startDate->format('m')-1
        );
        $endDate->setDate(
            (int)$endDate->format('m')-1 === 0 ? (int)$endDate->format('Y')-1 : (int)$endDate->format('Y'),
            (int)$endDate->format('m')-1 === 0 ? 12 : (int)$endDate->format('m')-1
        );

        foreach ($this->getTradeOptionRepository()
                     ->all(lockForUpdate: true) as $tradeOption) {
            $update = false;

            if ($tradeOption->previous_percent === null || $tradeOption->previous_updated_at === null) {
                $update = true;
            } else {
                $diff = (new DateTime())->diff($tradeOption->previous_updated_at);

                if($diff->invert === 1 &&
                    (int)$nowDate->format('d') >= 2 &&
                    $diff->m >= 1
                ) {
                    $update = true;
                }
            }

            if ($update) {
                $tradeOption->previous_percent = $this->getSumPercentByDate(
                    $tradeOption,
                    $startDate,
                    $endDate
                );

                $tradeOption->save();
            }
        }
    }

    /**
     * Get sum percent by date.
     *
     * @param TradeOption $tradeOption
     * @param DateTime $from
     * @param DateTime $to
     * @return float
     */
    public function getSumPercentByDate(TradeOption $tradeOption, DateTime $from, DateTime $to): float
    {
        return $this->getTidHistoryTradeOptionRepository()
            ->sumPercentByDate(
                $tradeOption->id,
                [$from, $to]
            );
    }

    /**
     * Get TradeOptionRepository.
     *
     * @return TradeOptionRepository
     */
    private function getTradeOptionRepository(): TradeOptionRepository
    {
        return app(TradeOptionRepository::class);
    }

    /**
     * Get getTidHistoryTradeOptionRepository.
     *
     * @return TidHistoryTradeOptionRepository
     */
    private function getTidHistoryTradeOptionRepository(): TidHistoryTradeOptionRepository
    {
        return app(TidHistoryTradeOptionRepository::class);
    }
}
