<?php
declare(strict_types=1);

namespace App\Modules\External\Dto\Api\Imperium;

use App\Modules\Core\Dto\BaseDto;

/**
 * Rates.
 */
final class Rates extends BaseDto
{
    /**
     * Constructor.
     *
     * @param float $ratePrevious
     * @param float $rate
     */
    public function __construct(
        protected float $ratePrevious,
        protected float $rate,
    )
    {
    }
}
