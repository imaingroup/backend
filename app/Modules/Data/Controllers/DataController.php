<?php
declare(strict_types=1);

namespace App\Modules\Data\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Data\Dto\Services\DispatchedDto;
use App\Modules\Data\Requests\DataRequest;
use App\Modules\Data\Services\DataService;
use Illuminate\Http\Response;
use Throwable;

/**
 * Data controller. This is core point to access
 * to any api methods. All methods must use only
 * through this controller.
 */
final class DataController extends Controller
{
    /**
     * Access to api methods.
     *
     * @param DataRequest $request
     * @return Response|array
     * @throws Throwable
     */
    protected function data(DataRequest $request): Response|DispatchedDto
    {
        return $this->getDataService()
            ->dispatch(...$request->validated());
    }

    /**
     * Get DataService.
     *
     * @return DataService
     */
    private function getDataService(): DataService
    {
        return app(DataService::class);
    }
}
