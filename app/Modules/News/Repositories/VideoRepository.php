<?php


namespace App\Modules\News\Repositories;

use App\Modules\News\Models\Video;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * Video repository.
 */
final class VideoRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\News\Models\Video $model
     */
    public function __construct(protected Video $model)
    {}

    /**
     * Get video list.
     *
     * @return Collection
     */
    public function getVideoList(): Collection
    {
        $list = $this->model
            ->newQuery()
            ->where('is_visible', true)
            ->orderBy('sort', 'ASC')
            ->get();

        $list->load('lang');

        return $list;
    }
}
