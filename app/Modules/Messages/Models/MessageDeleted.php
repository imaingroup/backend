<?php
declare(strict_types=1);

namespace App\Modules\Messages\Models;

use App\Modules\Core\Models\Model;

final class MessageDeleted extends Model
{
    protected $table = 'messages_deleted';
}
