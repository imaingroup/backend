<?php
declare(strict_types=1);

namespace App\Modules\Settings\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Settings\Dto\Api\DepositCurrencyApiDto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property-read int $crypto_currencies_id
 * @property float $deposit_min
 * @property bool $deposit_block
 * @property int $confirmations_min
 * @property float $luft_deposit
 * @property float $luft_withdraw
 * @property bool $custom_rate
 * @property string $name
 * @property string $full_name
 * @property int $order
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read CryptoCurrency $currency
 */
final class DepositCurrency extends Model
{
    /** {@inheritdoc} */
    protected $primaryKey = 'crypto_currencies_id';

    /** {@inheritdoc} */
    protected $table = 'deposit_currencies';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'crypto_currencies_id' => 'integer',
        'deposit_min' => 'float',
        'deposit_block' => 'boolean',
        'confirmations_min' => 'integer',
        'luft_deposit' => 'float',
        'luft_withdraw' => 'float',
        'custom_rate' => 'boolean',
        'name' => 'string',
        'full_name' => 'string',
        'order' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Relation with {@see CryptoCurrency::class}.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_currencies_id');
    }

    /**
     * To api dto.
     *
     * @return DepositCurrencyApiDto
     */
    public function toApiDto(): DepositCurrencyApiDto
    {
        return new DepositCurrencyApiDto(
            $this->crypto_currencies_id,
            $this->name,
            $this->full_name,
            $this->currency->network->toEnum(),
            $this->deposit_min,
            $this->deposit_block,
            $this->confirmations_min,
            $this->luft_deposit,
            $this->luft_withdraw,
            $this->currency?->market?->price,
            $this->custom_rate,
            $this->currency?->market?->image ?? null,
            $this->order,
            $this->currency->toApiDto(),
        );
    }
}
