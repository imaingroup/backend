<?php
declare(strict_types=1);

namespace App\Providers;

use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Messages\Models\Chat;
use App\Modules\Messages\Models\ChatUser;
use App\Modules\Messages\Models\Contact;
use App\Modules\Messages\Models\Message;
use App\Modules\Settings\Models\DepositCurrency;
use App\Modules\Users\Models\User;
use App\Modules\Messages\Repositories\ChatRepository;
use App\Modules\Messages\Repositories\ContactRepository;
use App\Modules\Investments\Models\Trade;
use App\Modules\Investments\Repositories\TradeOptionRepository;
use App\Modules\Users\Repositories\UserRepository;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

final class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
        });

        Route::model('userId', User::class);
        Route::model('teamLead', User::class);
        Route::model('contact', Contact::class);
        Route::model('chat', Chat::class);
        Route::model('chatUser', ChatUser::class);
        Route::model('message', Message::class);
        Route::model('messageFrom', Message::class);
        Route::model('messageTo', Message::class);
        Route::model('parentMessage', Message::class);
        Route::bind('messageSource', function (string $value) {
            $object = app(ContactRepository::class)
                ->findBySid((int)$value);

            if (!$object) {
                $object = app(ChatRepository::class)
                    ->findBySid((int)$value);
            }

            if (!$object) {
                throw new ModelNotFoundException();
            }

            return $object;
        });
        Route::model('trade', Trade::class);
        Route::bind('tradeOptionChart', function (string $value) {
            return app(TradeOptionRepository::class)
                    ->getActiveById((int)$value) ??
                throw new ModelNotFoundException();
        });
        Route::model('invitedUser', User::class);
        Route::model('blockedUser', User::class);
        Route::bind('userId', function (string $value) {
            return app(UserRepository::class)
                ->find((int)$value) ??
                throw new ModelNotFoundException();
        });
        Route::model('cryptoCurrency', CryptoCurrency::class);
        Route::model('depositCurrency', DepositCurrency::class);
        Route::model('cryptoAddress', CryptoAddress::class);
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60);
        });
    }
}
