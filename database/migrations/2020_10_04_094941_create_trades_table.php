<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->id();
            $table->string('tid', 255);
            $table->dateTime('opened_at');
            $table->unsignedBigInteger('trade_options_id');
            $table->unsignedBigInteger('users_id');
            $table->longText('trade_option');
            $table->float('amount', 16, 8);
            $table->timestamps();

            $table->foreign('trade_options_id')->references('id')->on('trade_options');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
