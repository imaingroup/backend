<?php
declare(strict_types=1);

namespace App\Modules\Notifications\Mails\External;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Contact mail class.
 */
final class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new mail instance.
     */
    public function __construct(
        private string $first_name,
        private string $last_name,
        private string $subjectMail,
        private string $email,
        private string $message
    )
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->subject('Contact request')
            ->markdown('emails.external.contact', [
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'subject' => $this->subjectMail,
                'email' => $this->email,
                'message' => $this->message,
            ]);
    }
}
