<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add usdt field to crypto tables.
 */
class AddUsdtToCryptoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->float('usdt_balance', 16, 8)
                ->after('eth_balance')
                ->nullable(true);
        });

        Schema::table('crypto_transactions', function (Blueprint $table) {
            $table->float('usdt_amount', 16, 8)
                ->after('amount')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->dropColumn('usdt_balance');
        });

        Schema::table('crypto_transactions', function (Blueprint $table) {
            $table->dropColumn('usdt_amount');
        });
    }
}
