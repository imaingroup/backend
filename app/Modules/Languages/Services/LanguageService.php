<?php


namespace App\Modules\Languages\Services;


use App\Modules\Languages\Models\Lang;
use App\Modules\Languages\Repositories\LangRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

/**
 * Language service.
 * This service work only with current user.
 */
final class LanguageService
{
    /** @var \App\Modules\Languages\Repositories\LangRepository Language repository. */
    private LangRepository $langRepository;

    /**
     * Constructor.
     *
     * @param \App\Modules\Languages\Repositories\LangRepository $langRepository
     */
    public function __construct(LangRepository $langRepository)
    {
        $this->langRepository = $langRepository;
    }

    /**
     * Get locale.
     *
     * @return string
     */
    public function getLocale()
    {
        $locale = App::getLocale();

        if (Auth::check()) {
            $lang = Auth::user()->lang;

            if ($lang->short_name != $locale) {
                $this->setLocale($lang->short_name);
            }
        }

        return App::getLocale();
    }

    /**
     * Set locale.
     *
     * @param string $locale
     */
    public function setLocale(string $locale)
    {
        Cookie::queue('locale', $locale, 100000);
        App::setLocale($locale);

        if (Auth::check()) {
            $lang = $this->langRepository->findByShortName($locale);

            $user = Auth::user();

            $user->refresh();

            $user->langs_id = $lang->id;

            $user->save();
        }
    }

    /**
     * Get language model.
     *
     * @return \App\Modules\Languages\Models\Lang
     */
    public function getLanguage(): Lang
    {
        return $this->langRepository->findByShortName($this->getLocale());
    }
}
