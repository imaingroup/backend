<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * UnlockAccountException.
 */
final class UnlockAccountException extends EthereumException
{
}
