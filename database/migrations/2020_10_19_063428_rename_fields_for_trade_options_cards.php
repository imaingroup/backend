<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration renaming fields.
 */
class RenameFieldsForTradeOptionsCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('trade_options_cards');

        Schema::create('trade_options_cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('trade_option_id');
            $table->unsignedBigInteger('profit_card_id');

            $table->timestamps();

            $table->foreign('trade_option_id')
                ->references('id')
                ->on('trade_options')
                ->onDelete('CASCADE');

            $table->foreign('profit_card_id')
                ->references('id')
                ->on('profit_cards')
                ->onDelete('CASCADE');

            $table->unique(['trade_option_id', 'profit_card_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_options_cards');

        Schema::create('trade_options_cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('trade_options_id');
            $table->unsignedBigInteger('profit_cards_id');

            $table->timestamps();

            $table->foreign('trade_options_id')
                ->references('id')
                ->on('trade_options')
                ->onDelete('CASCADE');

            $table->foreign('profit_cards_id')
                ->references('id')
                ->on('profit_cards')
                ->onDelete('CASCADE');

            $table->unique(['trade_options_id', 'profit_cards_id']);
        });
    }
}
