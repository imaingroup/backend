<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation add get chats request.
 */
final class GetChatsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape(['user' => "App\\Modules\\Users\\Models\\User"])]
    public function validated(): array
    {
        return ['user' => Auth::user()];
    }
}
