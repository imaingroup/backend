<?php

namespace App\Modules\News\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This request class validating news like request.
 */
final class NewsLikeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws \Exception
     */
    public function after(Validator $validator): void
    {
        if (new \DateTime($this->news->published_at) > new \DateTime() || !$this->news->is_view_lk || !$this->news->is_visible) {
            $validator->errors()->add('global', 'Not allowed to like');
        }

        if($this->news->liked(Auth::user())) {
            $validator->errors()->add('global', 'You were already liked');
        }
    }
}
