<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Withdraw;

/**
 * InsufficientFundsException.
 */
class InsufficientFundsException extends WithdrawException
{
}
