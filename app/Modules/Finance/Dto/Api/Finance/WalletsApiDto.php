<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api\Finance;

use App\Modules\Core\Dto\BaseDto;
use Illuminate\Support\Collection;

/**
 * WalletsDto.
 *
 * @property-read Collection|WalletApiDto[] $wallets
 */
final class WalletsApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param Collection $wallets
     */
    public function __construct(protected Collection $wallets)
    {
    }
}
