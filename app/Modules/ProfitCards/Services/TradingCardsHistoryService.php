<?php
declare(strict_types=1);

namespace App\Modules\ProfitCards\Services;

use App\Modules\ProfitCards\Enums\Models\ProfitCard\Type;
use App\Modules\ProfitCards\Repositories\ProfitCardBuyHistoryRepository;
use Illuminate\Support\Collection;

/**
 * TradingCardsHistoryService.
 */
final class TradingCardsHistoryService
{
    /**
     * Get latest list.
     *
     * @param int $limit
     * @return Collection
     */
    public function getLatest(int $limit): Collection
    {
        return $this->getProfitCardBuyHistoryRepository()
            ->getLatest($limit);
    }

    /**
     * Get latest list.
     *
     * @param Type $type
     * @param int $limit
     * @return Collection
     */
    public function getLatestByType(Type $type, int $limit): Collection
    {
        return $this->getProfitCardBuyHistoryRepository()
            ->getLatestByType($type, $limit);
    }

    /**
     * Get {@see ProfitCardBuyHistoryRepository::class}.
     *
     * @return ProfitCardBuyHistoryRepository
     */
    private function getProfitCardBuyHistoryRepository(): ProfitCardBuyHistoryRepository
    {
        return app(ProfitCardBuyHistoryRepository::class);
    }
}
