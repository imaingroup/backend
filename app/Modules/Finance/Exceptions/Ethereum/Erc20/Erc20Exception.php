<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum\Erc20;

use App\Modules\Finance\Exceptions\Ethereum\EthereumException;

/**
 * Erc20Exception.
 */
abstract class Erc20Exception extends EthereumException
{
}
