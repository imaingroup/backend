<?php

namespace App\Modules\Notifications\Jobs;

use App\Modules\Notifications\Mails\ContactFormAdminEmail;
use App\Modules\Notifications\Mails\MailSender;
use App\Modules\Landing\Models\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Send message from contact form on landing page
 * to administrator email job.
 */
final class SendContactFormAdminEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Modules\Landing\Models\Contact Contact model. */
    private Contact $contact;

    /**
     * Create a new job instance.
     *
     * @param \App\Modules\Landing\Models\Contact $contact
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MailSender::send(env('MAIL_ADMIN_ADDRESS'), new ContactFormAdminEmail($this->contact));
    }
}
