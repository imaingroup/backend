<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Services\MessageService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation
 * add user to group chat request.
 */
final class AddUserToGroupChatRequest extends FormRequest
{
    public function __construct(private MessageService $messageService)
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user
            && $this->messageService->checkAccessAddUserToGroupChat(
                $user,
                $this->chat,
                $this->contact
            );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'chat' => 'App\Modules\Messages\Models\Chat',
        'contact' => 'App\Modules\Messages\Models\Contact'
    ])]
    public function validated(): array
    {
        return [
            'chat' => $this->chat,
            'newUser' => $this->contact->contact
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function (Validator $validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     */
    private function after(Validator $validator): void
    {
        if ($this->messageService->getLimitAddUsersToGroupChat($this->chat) <= 0) {
            $validator->errors()->add('global', 'Maximum 100 users may be added in group chat.');
            return;
        }
    }
}
