<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Users\Dto\Api\UserShortDtoApi;
use DateTime;
use Illuminate\Support\Collection;

/**
 * CryptoAddressApiDto.
 *
 * @property-read int $id
 * @property-read TokenApiDto $currency
 * @property-read string $address
 * @property-read bool $isArchived
 * @property-read Collection $balances
 * @property-read UserShortDtoApi|null $user
 * @property-read DateTime|null $lastCheckAt
 * @property-read DateTime $createdAt
 * @property-read DateTime $updatedAt
 */
final class CryptoAddressApiDto extends BaseDto
{
    /**
     * Construct.
     *
     * @param int $id
     * @param TokenApiDto $currency
     * @param string $address
     * @param bool $isArchived
     * @param Collection $balances
     * @param UserShortDtoApi|null $user
     * @param DateTime|null $lastCheckAt
     * @param DateTime $createdAt
     * @param DateTime $updatedAt
     */
    public function __construct(
        protected int $id,
        protected TokenApiDto $currency,
        protected string $address,
        protected bool $isArchived,
        protected Collection $balances,
        protected ?UserShortDtoApi $user,
        protected ?DateTime $lastCheckAt,
        protected DateTime $createdAt,
        protected DateTime $updatedAt,
    )
    {

    }
}
