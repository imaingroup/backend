<?php


namespace App\Modules\Money\Exceptions;


use Exception;

/**
 * This exception may be throwing in money transfer service.
 */
final class MoneyTransferException extends Exception
{

}
