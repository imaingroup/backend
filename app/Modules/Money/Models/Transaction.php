<?php

namespace App\Modules\Money\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Users\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * Transaction model.
 *
 * @property float $amount
 * @property int $type
 * @property int $currency
 * @property int $operation_type
 * @property-read Transaction|null $parent
 */
final class Transaction extends Model
{
    //types
    const TYPE_TRANSFER_TO_USER = 1; //transfer money to user
    const TYPE_TRANSFER_FROM_USER = 2; //transfer money from user
    const TYPE_ADD_MONEY_ADMIN = 3; //add money admin
    const TYPE_WITHDRAW_MONEY_ADMIN = 4; //withdraw money admin
    const TYPE_REGISTER_BONUS = 5; //register bonus if user giving promo code
    const TYPE_BUY_PROFIT_CARD = 7; //buying profit card
    const TYPE_FROZEN_ABC = 8; //frozen money
    const TYPE_WITHDRAW_FOR_TRADING = 9; //withdraw for trading
    const TYPE_ADD_TO_TRADING = 10; //add to trading
    const TYPE_WITHDRAW_FROM_TRADING = 11; //withdraw from trading
    const TYPE_ADD_FROM_TRADING = 12; //add from trading
    const TYPE_ADD_DIVIDENDS = 13; //add dividends
    const TYPE_DEPOSIT = 14; //type deposit
    const TYPE_REFERRAL_COMMISSION_DEPOSIT = 15; //type referral commission deposit
    const TYPE_REFERRAL_COMMISSION_BUY_PROFIT_CARD = 16; //type referral commission buy profit card
    const TYPE_REFERRAL_COMMISSION_DEPOSIT_BY_ADMIN = 17; //type referral commission when admin adds money to usdt deposited
    const TYPE_EXCHANGE_ABC_USDT = 18; //type exchange abc to usdt
    const TYPE_WITHDRAW = 19; //type withdraw
    const TYPE_WITHDRAW_CANCELLATION = 20; //type withdraw cancellation
    const TYPE_EXCHANGE_USDT_ABC = 21; //type type exchange usdt to abc

    const TYPES = [
        self::TYPE_TRANSFER_TO_USER => 'Transferred to user',
        self::TYPE_TRANSFER_FROM_USER => 'Transferred from user',
        self::TYPE_ADD_MONEY_ADMIN => 'Added by admin',
        self::TYPE_WITHDRAW_MONEY_ADMIN => 'Withdrawn by admin',
        self::TYPE_REGISTER_BONUS => 'Added bonus at after register',
        self::TYPE_BUY_PROFIT_CARD => 'Purchased profit card',
        self::TYPE_FROZEN_ABC => 'Frozen',
        self::TYPE_WITHDRAW_FOR_TRADING => 'Withdrawn for trading',
        self::TYPE_ADD_TO_TRADING => 'Added to trading',
        self::TYPE_WITHDRAW_FROM_TRADING => 'Withdrawn from trading',
        self::TYPE_ADD_FROM_TRADING => 'Added from trading',
        self::TYPE_ADD_DIVIDENDS => 'Added dividends',
        self::TYPE_DEPOSIT => 'Deposited',
        self::TYPE_REFERRAL_COMMISSION_DEPOSIT => 'Added referral commission after deposited',
        self::TYPE_REFERRAL_COMMISSION_BUY_PROFIT_CARD => 'Added referral commission after purchased profit card',
        self::TYPE_REFERRAL_COMMISSION_DEPOSIT_BY_ADMIN => 'Added referral commission after deposited by admin',
        self::TYPE_EXCHANGE_ABC_USDT => 'Exchanged ABC to USDT',
        self::TYPE_WITHDRAW => 'Withdrawn',
        self::TYPE_WITHDRAW_CANCELLATION => 'Withdraw cancelled',
        self::TYPE_EXCHANGE_USDT_ABC => 'Exchanged USDT to ABC'
    ];

    const TYPES_LIST = [
        self::TYPE_TRANSFER_TO_USER,
        self::TYPE_TRANSFER_FROM_USER,
        self::TYPE_ADD_MONEY_ADMIN,
        self::TYPE_WITHDRAW_MONEY_ADMIN,
        self::TYPE_REGISTER_BONUS,
        self::TYPE_BUY_PROFIT_CARD,
        self::TYPE_FROZEN_ABC,
        self::TYPE_WITHDRAW_FOR_TRADING,
        self::TYPE_ADD_TO_TRADING,
        self::TYPE_WITHDRAW_FROM_TRADING,
        self::TYPE_ADD_FROM_TRADING,
        self::TYPE_ADD_DIVIDENDS,
        self::TYPE_DEPOSIT,
        self::TYPE_REFERRAL_COMMISSION_DEPOSIT,
        self::TYPE_REFERRAL_COMMISSION_BUY_PROFIT_CARD,
        self::TYPE_REFERRAL_COMMISSION_DEPOSIT_BY_ADMIN,
        self::TYPE_EXCHANGE_ABC_USDT,
        self::TYPE_WITHDRAW,
        self::TYPE_WITHDRAW_CANCELLATION,
        self::TYPE_EXCHANGE_USDT_ABC
    ];

    //currencies
    const CURRENCY_USDT_DEPOSITED = 1; //usdt deposited currency
    const CURRENCY_USDT_DIVIDENTS = 2; //usdt dividends currency
    const CURRENCY_ABC = 3; //abc currency

    public static array $currencyName = [
        self::CURRENCY_USDT_DEPOSITED => 'USDT Deposited',
        self::CURRENCY_USDT_DIVIDENTS => 'USDT Dividends',
        self::CURRENCY_ABC => 'ABC',
    ];

    public const CURRENCY_LIST = [
        self::CURRENCY_USDT_DEPOSITED,
        self::CURRENCY_USDT_DIVIDENTS,
        self::CURRENCY_ABC,
    ];

    const OPERATION_TYPE_ADD = 1; //add money
    const OPERATION_TYPE_WITHDRAW = 2; //withdraw money
    const OPERATION_TYPE_ADD_TO_TRADING = 3; //add abc money to trading
    const OPERATION_TYPE_WITHDRAW_FROM_TRADING = 4; //withdraw abc money from trading

    const OPERATION_TYPES = [
        self::OPERATION_TYPE_ADD => 'Add money',
        self::OPERATION_TYPE_WITHDRAW => 'Withdraw money',
        self::OPERATION_TYPE_ADD_TO_TRADING => 'Add abc money to trading',
        self::OPERATION_TYPE_WITHDRAW_FROM_TRADING => 'Withdraw abc money from trading'
    ];

    /**
     * Get relation with User
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Related with {@see Transaction::class}.
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Transaction::class, 'related_transaction_id');
    }

    /**
     * Is type.
     *
     * @param int $type
     * @return bool
     */
    public function isType(
        #[ExpectedValues(self::TYPES_LIST)] int $type
    ): bool
    {
        return $this->type === $type;
    }

    /**
     * Is currency.
     *
     * @param int $currency
     * @return bool
     */
    public function isCurrency(
        #[ExpectedValues(self::CURRENCY_LIST)] int $currency
    ): bool
    {
        return $this->currency === $currency;
    }

    /**
     * Get currency readable attribute.
     *
     * @return string
     */
    public function getCurrencyReadableAttribute(): string
    {
        return self::$currencyName[$this->currency] ?? $this->currency;
    }
}
