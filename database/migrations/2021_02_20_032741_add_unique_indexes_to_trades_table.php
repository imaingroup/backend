<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add unique index to trades tables.
 */
final class AddUniqueIndexesToTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('trade_history', function (Blueprint $table) {
            $table->unique(['trades_id', 'date'], 'trade_history_trades_id_date_unique');
        });

        Schema::table('trades_accruals', function (Blueprint $table) {
            $table->unique(['trades_id', 'date'], 'trades_accruals_trades_id_date_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('trade_history', function (Blueprint $table) {
            $table->dropUnique('trade_history_trades_id_date_unique');
        });

        Schema::table('trades_accruals', function (Blueprint $table) {
            $table->dropUnique('trades_accruals_trades_id_date_unique');
        });
    }
}
