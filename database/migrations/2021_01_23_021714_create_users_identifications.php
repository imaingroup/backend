<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates table
 * users_identifications for store
 * identifications data.
 */
final class CreateUsersIdentifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('users_identifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_id')->nullable(true);
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');
            $table->string('userid', 255)->index();
            $table->unsignedSmallInteger('action')->nullable(true)->index();
            $table->string('ip', 15)->nullable(true)->index();
            $table->string('country', 2)->nullable(true)->index();
            $table->string('id1', 255)
                ->nullable(true)
                ->index();
            $table->string('id2', 255)
                ->nullable(true)
                ->index();
            $table->string('user_agent', 255)
                ->nullable(true)
                ->index();
            $table->string('browser', 255)
                ->nullable(true)
                ->index();
            $table->string('browser_version', 255)
                ->nullable(true)
                ->index();
            $table->string('browser_major_version', 255)
                ->nullable(true)
                ->index();
            $table->boolean('is_ie')
                ->nullable(true)
                ->index();
            $table->boolean('is_chrome')
                ->nullable(true)
                ->index();
            $table->boolean('is_firefox')
                ->nullable(true)
                ->index();
            $table->boolean('is_safari')
                ->nullable(true)
                ->index();
            $table->boolean('is_opera')
                ->nullable(true)
                ->index();
            $table->string('engine', 255)
                ->nullable(true)
                ->index();
            $table->string('engine_version', 255)
                ->nullable(true)
                ->index();
            $table->string('platform', 255)
                ->nullable(true)
                ->index();
            $table->string('os', 255)
                ->nullable(true)
                ->index();
            $table->string('os_version', 255)
                ->nullable(true)
                ->index();
            $table->boolean('is_windows')
                ->nullable(true)
                ->index();
            $table->boolean('is_linux')
                ->nullable(true)
                ->index();
            $table->boolean('is_ubuntu')
                ->nullable(true)
                ->index();
            $table->boolean('is_solaris')
                ->nullable(true)
                ->index();
            $table->string('device', 255)
                ->nullable(true)
                ->index();
            $table->string('device_type', 255)
                ->nullable(true)
                ->index();
            $table->string('device_vendor', 255)
                ->nullable(true)
                ->index();
            $table->string('cpu', 255)
                ->nullable(true)
                ->index();
            $table->boolean('is_mobile')
                ->nullable(true)
                ->index();
            $table->boolean('is_mobile_android')
                ->nullable(true)
                ->index();
            $table->boolean('is_mobile_opera')
                ->nullable(true)
                ->index();
            $table->boolean('is_mobile_windows')
                ->nullable(true)
                ->index();
            $table->boolean('is_mobile_blackberry')
                ->nullable(true)
                ->index();
            $table->boolean('is_mobile_ios')
                ->nullable(true)
                ->index();
            $table->boolean('is_iphone')
                ->nullable(true)
                ->index();
            $table->boolean('is_ipod')
                ->nullable(true)
                ->index();
            $table->string('color_depth', 255)
                ->nullable(true)
                ->index();
            $table->string('current_resolution', 255)
                ->nullable(true)
                ->index();
            $table->string('available_resolution', 255)
                ->nullable(true)
                ->index();
            $table->string('device_xdpi', 255)
                ->nullable(true)
                ->index();
            $table->string('device_ydpi', 255)
                ->nullable(true)
                ->index();
            $table->text('plugins')
                ->nullable(true);
            $table->boolean('is_java')
                ->nullable(true)
                ->index();
            $table->string('java_version', 255)
                ->nullable(true)
                ->index();
            $table->boolean('is_flash')
                ->nullable(true)
                ->index();
            $table->string('flash_version', 255)
                ->nullable(true)
                ->index();
            $table->boolean('is_silverlight')
                ->nullable(true)
                ->index();
            $table->string('silverlight_version', 255)
                ->nullable(true)
                ->index();
            $table->string('mime_types', 255)
                ->nullable(true)
                ->index();
            $table->boolean('is_mime_types')
                ->nullable(true)
                ->index();
            $table->boolean('is_font')
                ->nullable(true)
                ->index();
            $table->text('fonts')
                ->nullable(true);
            $table->boolean('is_local_storage')
                ->nullable(true)
                ->index();
            $table->boolean('is_session_storage')
                ->nullable(true)
                ->index();
            $table->boolean('is_cookie')
                ->nullable(true)
                ->index();
            $table->string('time_zone', 255)
                ->nullable(true)
                ->index();
            $table->string('language', 255)
                ->nullable(true)
                ->index();
            $table->string('languages', 255)
                ->nullable(true)
                ->index();
            $table->string('system_language', 255)
                ->nullable(true)
                ->index();
            $table->boolean('is_canvas')
                ->nullable(true)
                ->index();
            $table->string('canvas_hash', 255)
                ->nullable(true)
                ->index();
            $table->string('local_storage', 255)
                ->nullable(true)
                ->index();
            $table->string('cookie', 255)
                ->nullable(true)
                ->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('users_identifications');
    }
}
