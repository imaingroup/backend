<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\EthereumGas;

/**
 * RequestException.
 */
final class RequestException extends EthereumGasException
{
}
