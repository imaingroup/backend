<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * ListUnspentException.
 */
final class ListUnspentException extends BtcException
{
}
