<?php

namespace App\Modules\News\Models;

use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Core\Models\Model;
use TCG\Voyager\Traits\Translatable;

final class NewsCategory extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;
    protected $translatable = ['title'];

    /**
     * Events for model.
     */
    protected static function booted()
    {
        static::creating(function (NewsCategory $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }
}
