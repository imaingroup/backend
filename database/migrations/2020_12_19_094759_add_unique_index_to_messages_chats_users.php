<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates unique index
 * for columns messages_chats_id,users_id
 * for messages_chats_users table.
 */
final class AddUniqueIndexToMessagesChatsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages_chats_users', function (Blueprint $table) {
            $table->unique(['messages_chats_id', 'users_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages_chats_users', function (Blueprint $table) {
            $table->dropIndex('messages_chats_users_messages_chats_id_users_id_unique');
        });
    }
}
