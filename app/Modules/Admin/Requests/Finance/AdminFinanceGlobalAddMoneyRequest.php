<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Finance;

use App\Modules\Money\Models\Transaction;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * This class validates request for add money to all users.
 */
final class AdminFinanceGlobalAddMoneyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'amount' => 'required|numeric|min:-99999999.99999999|max:99999999.99999999',
            'type' => [
                'required',
                'string',
                Rule::in(['abc', 'usdt_deposited', 'usdt_dividents'])
            ]
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): array
    {
        $data = parent::validated();

        if ($data['type'] == 'abc') {
            $currency = Transaction::CURRENCY_ABC;
        } else if ($data['type'] == 'usdt_deposited') {
            $currency = Transaction::CURRENCY_USDT_DEPOSITED;
        } else if ($data['type'] == 'usdt_dividents') {
            $currency = Transaction::CURRENCY_USDT_DIVIDENTS;
        }

        return [
            'amount' => $this->amount,
            'currency' => $currency
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'amount' => is_numeric($this->amount) ? (float) $this->amount : $this->amount
        ]);
    }
}
