<?php

namespace App\Modules\Access\Requests;

use App\Modules\Users\Models\User;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This request class need for validating data from reset password form.
 */
final class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $recaptchaEnabled = (int)env('GOOGLE_RECAPTCHA_ENABLED') === 1;
        $data = [];

        if ($recaptchaEnabled) {
            $data['g-recaptcha-response'] = 'required|scalar|recaptcha';
        }

        return array_merge($data, [
            'password' => [
                'required',
                'scalar',
                'min:6',
                'max:20',
                sprintf('regex:/^[%s]+$/', User::ALLOWED_PASSWORD_SYMBOLS)
            ],
            'password_confirmation' => 'nullable|scalar'
        ]);
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws Exception
     */
    public function after(Validator $validator): void
    {
        if ($this->get('password') != $this->get('password_confirmation')) {
            $validator->errors()->add('password_confirmation', 'Passwords didn\'t match');
        }

        if (new \DateTime($this->user->reset_token_valid_at) < new \DateTime()) {
            $validator->errors()->add('global', 'Password reset link has expired');
        }
    }
}
