<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * AddRateFieldsToSettingsImperium.
 */
final class AddRateFieldsToSettingsImperium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('settings_imperium', function (Blueprint $table) {
            $table->unsignedFloat('rate')
                ->default(0)
                ->after('version');
            $table->unsignedFloat('rate_previous')
                ->default(0)
                ->after('version');
        });

        DB::transaction(function() {
            $id = DB::table('data_types')
                ->where([
                    'name' => 'settings_imperium'
                ])
                ->first()
                ->id;

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'rate_previous',
                    'type' => 'number',
                    'display_name' => 'Rate previous',
                    'required' => 0,
                    'browse' => 1,
                    'read' => 1,
                    'edit' => 1,
                    'add' => 1,
                    'delete' => 1,
                    'details' => '{"validation":{"rule":"nullable|numeric"}}',
                    'order' => 3
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'rate',
                    'type' => 'number',
                    'display_name' => 'Rate',
                    'required' => 0,
                    'browse' => 1,
                    'read' => 1,
                    'edit' => 1,
                    'add' => 1,
                    'delete' => 1,
                    'details' => '{"validation":{"rule":"nullable|numeric"}}',
                    'order' => 4
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('settings_imperium', function (Blueprint $table) {
            $table->dropColumn(['rate_previous', 'rate']);
        });

        DB::transaction(function() {
            $id = DB::table('data_types')
                ->where([
                    'name' => 'settings_imperium'
                ])
                ->first()
                ->id;

            DB::table('data_rows')
                ->where('data_type_id', $id)
                ->whereIn('field', ['rate_previous', 'rate'])
                ->delete();
        });
    }
}
