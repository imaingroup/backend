<?php
declare(strict_types=1);


namespace App\Modules\Messages\Repositories;

use App\Modules\Messages\Models\MessageDeleted;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Message deleted repository.
 */
final class MessageDeletedRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Messages\Models\MessageDeleted $model
     */
    public function __construct(protected MessageDeleted $model)
    {
    }

    /**
     * Check deleted message.
     *
     * @param int $messageId
     * @param int $userId
     * @return bool
     */
    public function checkDeleted(int $messageId, int $userId): bool
    {
        return $this->model
            ->newQuery()
            ->where('messages_id', $messageId)
            ->where('users_id', $userId)
            ->exists();
    }
}
