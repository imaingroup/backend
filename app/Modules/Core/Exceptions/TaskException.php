<?php


namespace App\Modules\Core\Exceptions;


use Exception;

/**
 * This exception may be throws in task service.
 */
final class TaskException extends Exception
{
}
