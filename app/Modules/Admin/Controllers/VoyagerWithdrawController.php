<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\Withdraw\Exceptions\WithdrawServiceException;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Withdraw\Models\Withdraw;
use App\Modules\Withdraw\Services\WithdrawService;
use Closure;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator as FacadeValidator;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;
use Throwable;

/**
 * Voyager withdraw controller.
 */
final class VoyagerWithdrawController extends VoyagerBaseController
{
    /** @var \App\Modules\Withdraw\Services\WithdrawService Withdraw service. */
    private WithdrawService $withdrawService;

    /** @var \App\Modules\Logs\Loggers\Logger Logger. */
    private Logger $logger;

    /**
     * Constructor.
     *
     * @param \App\Modules\Withdraw\Services\WithdrawService $withdrawService
     * @param \App\Modules\Logs\Loggers\Logger $logger
     */
    public function __construct(WithdrawService $withdrawService, Logger $logger)
    {
        $this->withdrawService = $withdrawService;
        $this->logger = $logger;
    }

    /**
     * Update data.
     *
     * @param Request $request
     * @param $id
     * @param Closure|null $extend
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id, Closure $extend = null)
    {
        try {
            DB::beginTransaction();

            $slug = $this->getSlug($request);

            $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

            // Compatibility with Model binding.
            $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

            $model = app($dataType->model_name);
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $data = $model->withTrashed()->findOrFail($id);
            } else {
                $data = $model->findOrFail($id);
            }

            // Check permission
            $this->authorize('edit', $data);

            // Validate fields with ajax
            /** @var Validator $validator */
            $validator = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

            $val = $validator->validate();
            $status = (int)$val['status'];
            $statusChanged = $data->status !== $status;

            $validator->after(function ($validator) use ($status) {
                if (!in_array($status, array_keys(Withdraw::STATUSES))) {
                    $validator->errors()->add('status', 'This status is incorrect');
                }
            });

            $validator->after(function ($validator) use ($data, $statusChanged) {
                if ($data->status !== Withdraw::STATUS_PENDING &&
                    $statusChanged
                ) {
                    $validator->errors()->add('status', 'You doesn\'t have opportunity change status');
                }
            });

            $validator->validate();

            if($statusChanged && $data->status === Withdraw::STATUS_PENDING) {
                if ($status === Withdraw::STATUS_CONFIRMED) {
                    $data->confirmed_at = new \DateTime();
                    $data->save();
                } else if ($status === Withdraw::STATUS_DECLINED) {
                    $this->withdrawService->declineRequest($data);
                }
            }

            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            if (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            DB::commit();

            if ($statusChanged && $status === Withdraw::STATUS_CONFIRMED) {
                $rollbackStatus = false;

                try {
                    $this->withdrawService->processRequest($data);
                } catch (WithdrawServiceException $e) {
                    $rollbackStatus = true;

                    $this->addStatusChangingErrorToValidator($e->getMessage());
                } catch (Throwable $e) {
                    $rollbackStatus = true;

                    $this->addStatusChangingErrorToValidator('Bitcoin server can\'t process this request. Withdraw program was locked. Please contact to developer for decide this problem.');
                } finally {
                    if ($rollbackStatus) {
                        $data->refresh();

                        $data->status = Withdraw::STATUS_PENDING;
                        $data->confirmed_at = null;

                        $data->save();
                    }
                }
            }

            return $redirect->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } catch (ValidationException $e) {
            DB::rollBack();

            throw $e;
        } catch (\Exception $e) {
            DB::rollBack();

            $this->logger->rollbackTransaction($e, $request->all());

            abort(503);
        }
    }

    /**
     * Add status changing error to validator.
     *
     * @param string $message
     * @throws ValidationException
     */
    private function addStatusChangingErrorToValidator(string $message)
    {
        $validator = FacadeValidator::make([], []);

        $validator->after(function ($validator) use ($message) {
            $validator->errors()->add('status', $message);
        });

        $validator->validate();
    }
}
