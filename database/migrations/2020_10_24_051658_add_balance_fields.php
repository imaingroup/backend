<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adding balance total abc to users.
 */
class AddBalanceFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->float('balance_total_abc', 16, 8)->default(0);
            $table->float('total_deposited', 16, 8)->default(0);
            $table->float('total_received_abc', 16, 8)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('balance_total_abc');
            $table->dropColumn('total_deposited');
            $table->dropColumn('total_received_abc');
        });
    }
}
