<?php return [
	'site_name' => 'Insider Protocol',
	'copyright' => '© 2021 Insider Protocol by Mechanics of the Future',
	'form' => [
		'errors' => [
			'code_401' => 'You aren\'t authorized',
			'code_403' => 'You don\'t have permission for this action',
			'other' => 'Server error',
		],
	],
];
