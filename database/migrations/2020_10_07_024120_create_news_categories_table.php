<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_categories', function (Blueprint $table) {
            $table->id();
            $table->string('title', 255);
            $table->integer('sort');
            $table->boolean('is_visible');
            $table->timestamps();
        });

        Schema::table('news', function (Blueprint $table) {
            $table->unsignedBigInteger('news_categories_id');

            $table->foreign('news_categories_id')->references('id')->on('news_categories')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('news_categories_id');
        });

        Schema::dropIfExists('news_categories');
    }
}
