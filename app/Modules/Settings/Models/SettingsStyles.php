<?php
declare(strict_types=1);

namespace App\Modules\Settings\Models;

use App\Modules\Core\Models\Model;
use Carbon\Carbon;

/**
 * SettingsStyles.
 *
 * @property-read int $id
 * @property bool $enable_custom_body_css
 * @property string $body_cls_name
 * @property string $body_css
 * @property bool $enable_custom_header_css
 * @property string $header_cls_name
 * @property string $header_css
 * @property bool $enable_custom_sidebar_css
 * @property string $sidebar_cls_name
 * @property string $sidebar_css
 * @property bool $enable_custom_buttons_css
 * @property string $buttons_cls_name
 * @property string $buttons_css
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
final class SettingsStyles extends Model
{
    protected $table = 'settings_styles';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'enable_custom_body_css' => 'boolean',
        'body_cls_name' => 'string',
        'body_css' => 'string',
        'enable_custom_header_css' => 'boolean',
        'header_cls_name' => 'string',
        'header_css' => 'string',
        'enable_custom_sidebar_css' => 'boolean',
        'sidebar_cls_name' => 'string',
        'sidebar_css' => 'string',
        'enable_custom_buttons_css' => 'boolean',
        'buttons_cls_name' => 'string',
        'buttons_css' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
