<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creating table settings_links for store links settings.
 */
class CreateSettingsLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_links', function (Blueprint $table) {
            $table->id();
            $table->string('title', 255);
            $table->string('icon', 255);
            $table->text('href');
            $table->boolean('is_social');
            $table->boolean('is_product');
            $table->integer('sort');
            $table->boolean('is_visible');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_links');
    }
}
