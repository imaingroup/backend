<?php
declare(strict_types=1);


namespace App\Modules\Messages\Models;

use App\Modules\Core\Models\Model;

/**
 * Chat deleted model.
 */
final class ChatDeleted extends Model
{
    /** {@inheritdoc} */
    protected $table = 'messages_chats_deleted';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'messages_chats_id' => 'integer',
        'users_id' => 'integer',
    ];
}
