<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Models\Chat;
use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation invitation accept request.
 */
final class InvitationAcceptRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param MessageService $messageService
     */
    public function __construct(private MessageService $messageService)
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            $this->chat instanceof Chat &&
            $this->messageService->checkAccessChat($user, $this->chat) &&
            !$this->chat->checkAcceptedInvitation($user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape([
        'user' => "App\\Modules\\Users\\Models\\User",
        'chat' => "App\\Modules\\Messages\\Models\\Chat",
    ])]
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'chat' => $this->chat,
        ];
    }
}
