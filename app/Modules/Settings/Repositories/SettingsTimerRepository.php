<?php


namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsTimer;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Settings timer repository.
 */
final class SettingsTimerRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param SettingsTimer $model
     */
    public function __construct(protected SettingsTimer $model)
    {}

    /**
     * Get settings model.
     *
     * @return SettingsTimer
     */
    public function get(): SettingsTimer
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
