<?php

namespace App\Modules\Settings\Models;

use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Core\Models\Model;
use Exception;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Traits\Translatable;

final class SettingsLanding extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;
    protected $translatable = ['ito_title', 'ito_text', 'what_is_abc_text', 'bottom_text'];

    protected $table = 'settings_landing';

    /**
     * Get main video.
     *
     * @return array|null
     */
    public function getVideoMain()
    {
        return $this->formatVideoField('video_main');
    }

    /**
     * Get video manifest.
     *
     * @return array|null
     */
    public function getVideoManifest()
    {
        return $this->formatVideoField('video_manifest');
    }

    /**
     * Get formatted video field.
     *
     * @param string $field
     * @return array|null
     */
    private function formatVideoField(string $field)
    {
        try {
            $json = \json_decode($this->$field, true, 3, JSON_THROW_ON_ERROR);

            if(count($json) > 0) {
                $data = $json[0];
                $data['download_link'] = Storage::disk('local')->url($data['download_link']);

                return $data;
            }
        }
        catch (Exception $e) {}

        return null;
    }

}
