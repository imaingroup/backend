<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services;

use App\Modules\Finance\Exceptions\SecureCryptRequestException;
use App\Modules\Finance\Exceptions\SecureCryptRequestParamsException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use StdClass;
use Throwable;
use function sprintf;

/**
 * Service for working with bitcoin core rpc server.
 */
final class CryptJsonRpcClientService
{
    /**
     * Execute request to bitcoin core api.
     *
     * @param string $key
     * @param string $secret
     * @param string $url
     * @param string $method
     * @param array $params
     * @param array $credentials
     * @return StdClass
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    public function request(
        string $key,
        string $secret,
        string $url,
        string $method,
        array $params = [],
        array $credentials = []
    ): StdClass
    {
        return $this->requestMulti(
            $key,
            $secret,
            $url,
            [['method' => $method, 'params' => $params]],
            $credentials
        )[0];
    }

    /**
     * Execute request to bitcoin core api.
     *
     * @param string $key
     * @param string $secret
     * @param string $url
     * @param array $request
     * @param array $credentials
     * @param array $search
     * @return array
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    public function requestMulti(
        string $key,
        string $secret,
        string $url,
        array $request,
        array $credentials = [],
        array $search = [],
    ): array
    {
        $dataMultiRequests = [];

        foreach ($request as $item) {
            if (!isset($item['method']) ||
                !is_string($item['method']) ||
                !isset($item['params']) ||
                !is_array($item['params'])
            ) {
                throw new SecureCryptRequestParamsException();
            }

            $data = new stdClass();
            $data->jsonrpc = '1.0';
            $data->id = 'iclient';
            $data->method = $item['method'];
            $data->params = $item['params'];

            $dataMultiRequests[] = $data;
        }

        $dataMulti = [
            'request' => $dataMultiRequests,
            'search' => $search,
        ];

        $headers = [
            'Content-Type' =>
                $this->getAesCryptService()
                    ->encrypt($key, 'application/json'),
            'Secret' => $this->getAesCryptService()
                ->encrypt($key, $secret),
        ];

        if (count($credentials) === 2) {
            $headers['Authorization'] =
                $this->getAesCryptService()
                    ->encrypt($key, sprintf('Basic %s', base64_encode(sprintf('%s:%s', $credentials[0], $credentials[1]))));
        }

        $client = new Client();

        try {
            $clientRequest = $client->request('POST', $url, [
                'headers' => $headers,
                'body' => $this->getAesCryptService()
                    ->encrypt($key, json_encode($dataMulti))
            ]);

            if ($clientRequest->getStatusCode() == 200) {
                $content = null;
                $decodedRaw = null;
                $error = null;

                try {
                    $content = $clientRequest->getBody()->__toString();
                    $decodedRaw = $this->getAesCryptService()->decrypt($key, $content);
                    $decoded = json_decode(
                        $decodedRaw,
                        false,
                        520,
                        JSON_THROW_ON_ERROR
                    );

                    if (!is_array($decoded) || (count($search) === 0 && count($decoded) === 0)) {
                        throw new SecureCryptRequestException();
                    }

                    foreach ($decoded as $decodedItem) {
                        if (!property_exists($decodedItem, 'result')) {

                            if (property_exists($decodedItem, 'error')) {
                                $error = $decodedItem->error;
                            }

                            throw new SecureCryptRequestException();
                        }
                    }

                    return $decoded;
                } catch (Throwable $e) {
                    throw new SecureCryptRequestException(
                        sprintf('%s|%s', $content, $decodedRaw),
                        0,
                        $e,
                        $error
                    );
                }
            } else {
                throw new SecureCryptRequestException($clientRequest->getBody()->__toString());
            }
        } catch (GuzzleException $e) {
            throw new SecureCryptRequestException($e->getMessage(), 0, $e);
        }
    }

    /**
     * Get AesCryptService.
     *
     * @return AesCryptService
     */
    protected function getAesCryptService(): AesCryptService
    {
        return app(AesCryptService::class);
    }
}
