<?php

namespace App\Modules\Access\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This request class need for validating data from resend activate form.
 */
final class ResendActivateRequest extends FormRequest
{
    /** @var int Resend requests limit in minutes. */
    private int $resendLimitMinutes = 5;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $recaptchaEnabled = (int)env('GOOGLE_RECAPTCHA_ENABLED') === 1;
        $data = [];

        if ($recaptchaEnabled) {
            $data['g-recaptcha-response'] = 'required|scalar|recaptcha';
        }

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator): void
    {
        if (!$this->session()->has('current_user')) {
            $validator->errors()->add('global', 'You are not authorized');

            return;
        }

        if ($this->session()->get('current_user')->email_verified_at) {
            $validator->errors()->add('global', 'Your account already activated');

            return;
        }

        if ($this->session()->has('resend_activate_request_at')) {
            $resendActivateRequestAt = clone $this->session()->get('resend_activate_request_at');

            if ($resendActivateRequestAt->modify(\sprintf('+ %s MINUTES', $this->resendLimitMinutes)) > new \DateTime()) {
                $validator->errors()->add('global', 'Account activation requests limit');

                return;
            }
        }
    }
}
