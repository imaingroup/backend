<?php
declare(strict_types=1);


namespace App\Modules\Messages\Repositories;

use App\Modules\Messages\Models\MessageRead;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Message read repository.
 */
final class MessageReadRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param MessageRead $model
     */
    public function __construct(protected MessageRead $model)
    {
    }
}
