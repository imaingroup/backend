<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adding fields to table tid_history_trade_option.
 */
class AddFieldsToTidHistoryTradeOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tid_history_trade_option', function (Blueprint $table) {
            $table->float('default_percent', 10, 4);
            $table->integer('coefficient');
            $table->float('percent', 10, 4)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tid_history_trade_option', function (Blueprint $table) {
            $table->dropColumn('default_percent');
            $table->dropColumn('coefficient');
            $table->float('percent', 8, 2)->change();
        });
    }
}
