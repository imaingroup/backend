<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Models\ChatUser;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation
 * create group chat request.
 */
final class CreateGroupChatRequest extends FormRequest
{
    public function __construct(private \App\Modules\Messages\Repositories\ContactRepository $contactRepository)
    {

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'contacts' => "string",
        'contacts.*' => "array",
        'name' => "string",
        'avatar' => "string"
    ])]
    public function rules(): array
    {
        return [
            'contacts' => sprintf('required|array|min:1|max:%s', ChatUser::LIMIT_USERS_IN_GROUP_CHAT),
            'contacts.*' => [
                'required',
                Rule::exists('messages_contacts', 'sid')
                    ->where(function (Builder $query) {
                        $query
                            ->where('users_id', Auth::user()->id);
                    })
            ],
            'name' => 'required|scalar|max:255',
            'avatar' => 'nullable|image|dimensions:max_width=800,max_height=800|mimes:jpeg,jpg,png|max:1024',
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'contacts' => "array",
        'name' => 'string',
        'avatar' => 'Illuminate\Http\UploadedFile|null',
    ])]
    public function validated(): array
    {
        $validated = parent::validated();

        $contacts = [];
        foreach ($validated['contacts'] as $contact) {
            $contacts[] = $this->contactRepository->findBySid((int)$contact);
        }

        $data = [];
        $data['contacts'] = $contacts;
        $data['name'] = $validated['name'];
        $data['avatar'] = $validated['avatar'] ?? null;

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'name' => is_scalar($this->name) ? trim((string)$this->name) : $this->name,
            'contacts' => is_array($this->contacts) ? array_unique($this->contacts) : $this->contacts,
        ]);
    }
}
