<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Create crypto tables.
 */
final class CreateCryptoModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('crypto_addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('type')
                ->index();
            $table->string('address', 100)
                ->unique();
            $table->string('client_id', 255)
                ->nullable(true)
                ->index();
            $table->dateTime('monitoring_before_at');
            $table->unsignedSmallInteger('min_conf');
            $table->unsignedInteger('cnt_check')
                ->default(0);
            $table->unsignedBigInteger('eth_block')
                ->nullable(true);
            $table->text('eth_password');
            $table->float('eth_balance', 16, 8)
                ->nullable(true);
            $table->dateTime('last_check_at')
                ->nullable(true);
            $table->unsignedBigInteger('users_id')
                ->nullable(true);
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');
            $table->timestamps();
        });

        Schema::create('crypto_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('txid', 100)
                ->unique();
            $table->float('amount', 16, 8);
            $table->integer('confirmations');
            $table->unsignedBigInteger('crypto_addresses_id')
                ->nullable(true);
            $table->foreign('crypto_addresses_id')
                ->references('id')
                ->on('crypto_addresses')
                ->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('crypto_transactions');
        Schema::dropIfExists('crypto_addresses');
    }
}
