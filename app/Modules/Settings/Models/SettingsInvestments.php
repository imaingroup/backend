<?php

namespace App\Modules\Settings\Models;

use App\Modules\Core\Models\Model;

final class SettingsInvestments extends Model
{
    protected $table = 'settings_investments';
}
