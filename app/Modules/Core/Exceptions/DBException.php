<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions;

use Exception;

/**
 * This exception may be throws in DB.
 */
final class DBException extends Exception
{
}
