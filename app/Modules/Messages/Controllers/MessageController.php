<?php
declare(strict_types=1);

namespace App\Modules\Messages\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Core\Attributes\Updatable;
use App\Modules\Core\Exceptions\DataException;
use App\Modules\Core\Services\DataService;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Messages\Models\Chat;
use App\Modules\Messages\Models\ChatUser;
use App\Modules\Messages\Models\Contact;
use App\Modules\Messages\Models\Message;
use App\Modules\Messages\Requests\AddContactRequest;
use App\Modules\Messages\Requests\AddUserToGroupChatRequest;
use App\Modules\Messages\Requests\CleanMessagesRequest;
use App\Modules\Messages\Requests\ContactUnblockRequest;
use App\Modules\Messages\Requests\CreateGroupChatRequest;
use App\Modules\Messages\Requests\DeleteChatRequest;
use App\Modules\Messages\Requests\DeleteContactRequest;
use App\Modules\Messages\Requests\DeleteMessageRequest;
use App\Modules\Messages\Requests\DeleteUserFromGroupChatRequest;
use App\Modules\Messages\Requests\EditGroupChatRequest;
use App\Modules\Messages\Requests\EditMessageRequest;
use App\Modules\Messages\Requests\GetChatsRequest;
use App\Modules\Messages\Requests\GetContactsRequest;
use App\Modules\Messages\Requests\GetMessagesRequest;
use App\Modules\Messages\Requests\InvitationAcceptRequest;
use App\Modules\Messages\Requests\LeaveGroupChatRequest;
use App\Modules\Messages\Requests\ObserveMessagesRequest;
use App\Modules\Messages\Requests\SearchMessagesRequest;
use App\Modules\Messages\Requests\SendMessageRequest;
use App\Modules\Messages\Requests\StartConversationRequest;
use App\Modules\Messages\Requests\UserBlockRequest;
use App\Modules\Messages\Services\MessageService;
use App\Modules\Users\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

/**
 * Message controller.
 */
final class MessageController extends Controller
{
    /**
     * Get contact list.
     *
     * @param GetContactsRequest $request
     * @return array
     * @throws DataException
     */
    #[Updatable]
    protected function getContacts(GetContactsRequest $request): array
    {
        return $this->getDataService()->getData(
            DataService::TYPE_MESSAGES_CONTACTS,
            array_values($request->validated())
        );
    }

    /**
     * Get contact list.
     *
     * @param GetContactsRequest $request
     * @return array
     * @throws DataException
     */
    protected function getContactsOld(GetContactsRequest $request): array
    {
        return $this->getDataService()->getData(
            DataService::TYPE_MESSAGES_CONTACTS,
            array_values($request->validated())
        );
    }

    /**
     * Get chat list.
     *
     * @param GetChatsRequest $request
     * @return array
     * @throws DataException
     */
    protected function getChats(GetChatsRequest $request): array
    {
        return $this->getDataService()->getData(
            DataService::TYPE_MESSAGES_CHATS,
            array_values($request->validated())
        );
    }

    /**
     * Add contact.
     *
     * @param Contact $contact
     * @return Response|array
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function conversationStart(Contact $contact): Response|array
    {
        return $this->inTransaction(function () use ($contact): array {
            Contact::refreshAndLockForUpdate($contact);

            $chat = $this->getMessageService()->conversationStart(
                ...app(StartConversationRequest::class)
                ->validated()
            );

            return [
                'data' => ['chat_id' => $chat->sid]
            ];
        });
    }

    /**
     * Add contact.
     *
     * @param User $invitedUser
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function addContact(User $invitedUser): Response
    {
        return $this->inTransaction(function () use ($invitedUser): void {
            User::refreshAndLockForUpdate($invitedUser);

            $this->getMessageService()->addContact(
                ...app(AddContactRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Send message.
     *
     * @param Contact|Chat $messageSource
     * @param Message|null $parentMessage
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function sendMessage(Contact|Chat $messageSource, ?Message $parentMessage = null): Response
    {
        return $this->inTransaction(function () use ($messageSource, $parentMessage): Response {
            User::refreshAndLockForUpdate(Auth::user());
            SendMessageRequest::$messageSource = $messageSource;
            SendMessageRequest::$message = $parentMessage;

            $data = app(SendMessageRequest::class)->validated();

            $res = $this->getMessageService()->sendMessage(
                $data['user'],
                $messageSource,
                $data['message'],
                $parentMessage
            );

            return response(['data' => $res]);
        });
    }

    /**
     * Edit message.
     *
     * @param Message $message
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function editMessage(Message $message): Response
    {
        return $this->inTransaction(function () use ($message): void {
            Message::refreshAndLockForUpdate($message);

            $this->getMessageService()->editMessage(
                ...app(EditMessageRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Get messages.
     *
     * @param Contact|Chat $messageSource
     * @param int|null $type
     * @param Message|null $afterMessage
     * @return Response
     */
    protected function getMessages(
        Contact|Chat $messageSource,
        int $type = null,
        Message $afterMessage = null
    ): Response
    {
        //@todo refactor it.
        GetMessagesRequest::$messageSource = $messageSource;
        GetMessagesRequest::$type = $type;
        GetMessagesRequest::$message = $afterMessage;

        $rdata = app(GetMessagesRequest::class)->validated();
        $data = $this->getMessageService()
            ->getMessages(
                $rdata['user'],
                $messageSource,
                Message::MESSAGES_LIMIT_IN_CHAT,
                $type,
                $afterMessage,
                $rdata['order']
            );

        return response([
            'data' => [
                'messages' => $data,
                'source_id' => $messageSource->sid,
            ]
        ]);
    }

    /**
     * Observe for update messages.
     *
     * @param Message $messageFrom
     * @param Message|null $messageTo
     * @return Response
     */
    protected function observe(Message $messageFrom, ?Message $messageTo = null): Response
    {
        ObserveMessagesRequest::$messageFrom = $messageFrom;
        ObserveMessagesRequest::$messageTo = $messageTo;

        $data = $this->getMessageService()
            ->observe(
                ...app(ObserveMessagesRequest::class)
                    ->validated()
            );

        return response([
            'data' => [
                'messages' => $data,
                'source_id' => $messageFrom->chat->sid,
            ]
        ]);
    }

    /**
     * Accept invitation to chat.
     *
     * @param InvitationAcceptRequest $request
     * @param Chat $chat
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function acceptInvitation(InvitationAcceptRequest $request, Chat $chat): Response
    {
        return $this->inTransaction(function () use ($request, $chat): void {
            Chat::refreshAndLockForUpdate($chat);

            $this->getMessageService()->acceptInvitation(
                ...app(InvitationAcceptRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Create group chat.
     *
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function createGroupChat(): Response
    {
        return $this->inTransaction(function (): Response {
            User::refreshAndLockForUpdate(Auth::user());

            $chat = $this->getMessageService()->createGroupChat(
                ...app(CreateGroupChatRequest::class)
                ->validated()
            );

            return response([
                'data' => [
                    'chat_id' => $chat->sid
                ]
            ]);
        });
    }

    /**
     * Delete chat user.
     *
     * @param Chat $chat
     * @param ChatUser $chatUser
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function deleteUserFromGroupChat(Chat $chat, ChatUser $chatUser): Response
    {
        return $this->inTransaction(function () use ($chat, $chatUser): void {
            Chat::refreshAndLockForUpdate($chat);
            ChatUser::refreshAndLockForUpdate($chatUser);

            $this->getMessageService()->deleteChatUser(
                ...app(DeleteUserFromGroupChatRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Add user to group chat.
     *
     * @param Chat $chat
     * @param Contact $contact
     * @return Response|array
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function addUserToGroupChat(Chat $chat, Contact $contact): Response|array
    {
        return $this->inTransaction(function () use ($chat, $contact): array {
            Chat::refreshAndLockForUpdate($chat);
            Contact::refreshAndLockForUpdate($contact);

            $chatUser = $this->getMessageService()->addUserToChat(
                ...app(AddUserToGroupChatRequest::class)
                ->validated()
            );

            return [
                'status' => true,
                'data' => [
                    'chat_user_id' => $chatUser->sid,
                ],
            ];
        });
    }

    /**
     * Edit group chat.
     *
     * @param Chat $chat
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function editGroupChat(Chat $chat): Response
    {
        return $this->inTransaction(function () use ($chat): void {
            Chat::refreshAndLockForUpdate($chat);

            $this->getMessageService()->editGroupChat(
                ...app(EditGroupChatRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Leave group chat.
     *
     * @param Chat $chat
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function leaveGroupChat(Chat $chat): Response
    {
        return $this->inTransaction(function () use ($chat): void {
            Chat::refreshAndLockForUpdate($chat);

            $this->getMessageService()->leaveGroupChat(
                ...app(LeaveGroupChatRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Clean messages.
     *
     * @param Chat $chat
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function cleanMessages(Chat $chat): Response
    {
        return $this->inTransaction(function () use ($chat): void {
            Chat::refreshAndLockForUpdate($chat);

            $this->getMessageService()->cleanMessages(
                ...app(CleanMessagesRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Delete message.
     *
     * @param Message $message
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function deleteMessage(Message $message): Response
    {
        return $this->inTransaction(function () use ($message): void {
            Message::refreshAndLockForUpdate($message);

            $this->getMessageService()->deleteMessage(
                ...app(DeleteMessageRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Delete chat.
     *
     * @param Chat $chat
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function deleteChat(Chat $chat): Response
    {
        return $this->inTransaction(function () use ($chat): void {
            Chat::refreshAndLockForUpdate($chat);

            $this->getMessageService()->deleteChat(
                ...app(DeleteChatRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Delete contact.
     *
     * @param Contact $contact
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function deleteContact(Contact $contact): Response
    {
        return $this->inTransaction(function () use ($contact): void {
            Contact::refreshAndLockForUpdate($contact);

            $this->getMessageService()->deleteContact(
                ...app(DeleteContactRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Block user.
     *
     * @param \App\Modules\Users\Models\User $blockedUser
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function userBlock(User $blockedUser): Response
    {
        return $this->inTransaction(function () use ($blockedUser): void {
            User::refreshAndLockForUpdate($blockedUser);

            $this->getMessageService()->userBlock(
                ...app(UserBlockRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Contact unblock.
     *
     * @param Contact $contact
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function contactUnblock(Contact $contact): Response
    {
        return $this->inTransaction(function () use ($contact): void {
            Contact::refreshAndLockForUpdate($contact);

            $this->getMessageService()->contactUnblock(
                ...app(ContactUnblockRequest::class)
                ->validated()
            );
        });
    }

    /**
     * Search messages.
     *
     * @param SearchMessagesRequest $request
     * @return Response
     */
    protected function search(SearchMessagesRequest $request): Response
    {
        $data = $this->getMessageService()->search(...$request->validated());

        return response(['data' => $data]);
    }

    /**
     * Get MessageService.
     *
     * @return MessageService
     */
    public function getMessageService(): MessageService
    {
        return app(MessageService::class);
    }

    /**
     * Get DataService.
     *
     * @return DataService
     */
    public function getDataService(): DataService
    {
        return app(DataService::class);
    }

    /**
     * Get Logger.
     *
     * @return Logger
     */
    public function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
