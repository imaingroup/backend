<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * SendException.
 */
final class SendException extends BtcException
{
}
