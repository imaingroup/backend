<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Deposit;

use App\Modules\Deposites\Models\Deposit;
use App\Modules\Deposites\Models\DepositTransaction;
use App\Modules\Users\Models\User;
use DateTime;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Add deposit transaction manually.
 *
 * @property-read Deposit|mixed $deposit
 */
final class AdminDepositTransactionAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            $user->hasRole(User::ROLE_ADMIN_NAME) &&
            in_array($user->id, User::USERS_ALLOWED_SEND_MONEY) &&
            $this->deposit instanceof Deposit;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['amount' => "string", 'txid' => "string"])]
    public function rules(): array
    {
        return [
            'amount' => 'required|numeric|min:0.00000001',
            'txid' => 'required|crypto_tx',
        ];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape([
        'deposit' => Deposit::class,
        'amount' => "float",
        'transactionAt' => DateTime::class,
        'confirmed' => "bool",
        'txid' => "mixed"
    ])]
    public function validated(): array
    {
        $data = parent::validated();

        return [
            'deposit' => $this->deposit,
            'amount' => (float)$data['amount'],
            'transactionAt' => new DateTime(),
            'status' => DepositTransaction::CONFIRMED,
            'txid' => $data['txid'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()
            ->after(function (Validator $validator) {
                if (count($validator->failed()) === 0) {
                    $this->after($validator);
                }
            });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     */
    private function after(Validator $validator): void
    {
        if ($this->deposit->findTransaction($this['txid'])) {
            $validator->errors()
                ->add('txid', 'Transaction already was added.');
        }
    }
}
