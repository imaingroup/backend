<?php
declare(strict_types=1);

namespace App\Modules\Security\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Users\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * UserIdentificationRelation.
 *
 * @property-read int $id
 * @property-read int $users_main_id
 * @property-read int $users_id
 * @property boolean $is_accurate
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $mainUser
 * @property-read User $user
 */
final class UserIdentificationUserRelation extends Model
{
    /** {@inheritdoc} */
    protected $table = 'users_identifications_users_relations';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'users_main_id' => 'integer',
        'users_id' => 'integer',
        'is_accurate' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get relation with {@see User::class}.
     *
     * @return BelongsTo
     */
    public function mainUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_main_id');
    }

    /**
     * Get relation with {@see User::class}.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }
}
