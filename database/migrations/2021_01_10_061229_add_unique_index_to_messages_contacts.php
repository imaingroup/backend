<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * Add unique index to messages_contacts.
 */
final class AddUniqueIndexToMessagesContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::statement('create unique index messages_contacts_users_id_contact_users_id_uindex on messages_contacts (users_id, contact_users_id)');

        try {
            DB::statement('drop index messages_contacts_users_id_foreign on messages_contacts');
        }
        catch (Throwable) {}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        try {
            DB::statement('create index messages_contacts_users_id_foreign on messages_contacts (users_id)');
        }
        catch (Throwable) {}

        DB::statement('drop index messages_contacts_users_id_contact_users_id_uindex on messages_contacts');
    }
}
