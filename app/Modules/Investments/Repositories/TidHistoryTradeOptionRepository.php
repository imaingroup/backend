<?php
declare(strict_types=1);

namespace App\Modules\Investments\Repositories;

use App\Modules\Investments\Models\TidHistoryTradeOption;
use App\Modules\Core\Repositories\BaseRepository;
use DateTime;
use Illuminate\Database\Eloquent\Collection;

/**
 * Tid history trade options repository.
 */
final class TidHistoryTradeOptionRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Investments\Models\TidHistoryTradeOption $model
     */
    public function __construct(protected TidHistoryTradeOption $model)
    {
    }

    /**
     * Find by date.
     *
     * @param int $tidHistoryId
     * @param int $tradeOptionsId
     * @return \App\Modules\Investments\Models\TidHistoryTradeOption|null
     */
    public function findBy(int $tidHistoryId, int $tradeOptionsId): ?TidHistoryTradeOption
    {
        return $this->model
            ->newQuery()
            ->where('tid_history_id', $tidHistoryId)
            ->where('trade_options_id', $tradeOptionsId)
            ->first();
    }

    /**
     * Get by trade options id and date tid history great then parameter.
     *
     * @param int $tradeOptionsId
     * @param DateTime|array|null $gtDate
     * @param DateTime|null $ltDate
     * @return Collection
     */
    public function getByTradeOptionsId(int $tradeOptionsId, DateTime|array $gtDate = null, DateTime $ltDate = null): Collection
    {
        $query = $this->model
            ->newQuery()
            ->select('tid_history_trade_option.*', 'tid_history.date')
            ->join('tid_history', 'tid_history_trade_option.tid_history_id', '=', 'tid_history.id')
            ->where('tid_history_trade_option.trade_options_id', $tradeOptionsId)
            ->orderBy('tid_history.date', 'ASC');

        if (is_array($gtDate) && count($gtDate) === 2) {
            $query->whereBetween('tid_history.date', $gtDate);
        } else {
            if ($gtDate) {
                $query->where('tid_history.date', '>', $gtDate);
            }

            if ($ltDate) {
                $query->where('tid_history.date', '<', $ltDate);
            }
        }

        return $query->get();
    }

    /**
     * Get sum percent by date.
     *
     * @param int $tradeOptionsId
     * @param DateTime|array|null $lteDate
     * @return float
     */
    public function sumPercentByDate(int $tradeOptionsId, DateTime|array $lteDate = null): float
    {
        $query = $this->model
            ->newQuery()
            ->select('tid_history_trade_option.*', 'tid_history.date')
            ->join('tid_history', 'tid_history_trade_option.tid_history_id', '=', 'tid_history.id')
            ->where('tid_history_trade_option.trade_options_id', $tradeOptionsId);

        if(is_array($lteDate) && count($lteDate) === 2) {
            $query->whereBetween('tid_history.date', $lteDate);
        }
        else if ($lteDate) {
            $query->where('tid_history.date', '<=', $lteDate);
        }

        return (float)$query->sum('tid_history_trade_option.percent');
    }
}
