<?php
declare(strict_types=1);

namespace App\Modules\Users\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Investments\Models\TradeOption;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * This model storing expanse data for tid and user.
 *
 * @property-read int $id
 * @property-read int $users_id
 * @property-read int $trade_options_id
 * @property string $title
 * @property integer $period_min
 * @property integer $period_max
 * @property integer $amount_min
 * @property integer $amount_max
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $user
 * @property-read TradeOption $tradeOption
 */
final class UserCustomTidOptions extends Model
{
    protected $table = 'users_custom_tid_options';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'users_id' => 'integer',
        'trade_options_id' => 'integer',
        'title' => 'string',
        'period_min' => 'integer',
        'period_max' => 'integer',
        'amount_min' => 'integer',
        'amount_max' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get relation {@see User::class}.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Get relation {@see TradeOption::class}.
     *
     * @return BelongsTo
     */
    public function tradeOption(): BelongsTo
    {
        return $this->belongsTo(TradeOption::class, 'trade_options_id');
    }
}
