<?php


namespace App\Modules\Deposites\Repositories;

use App\Modules\Deposites\Models\DepositTransaction;
use App\Modules\Users\Models\User;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Deposit transaction repository.
 */
final class DepositTransactionRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Deposites\Models\DepositTransaction $model
     */
    public function __construct(protected DepositTransaction $model)
    {}

    /**
     * Get transactions by user.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return Collection
     */
    public function getByUser(User $user): Collection
    {
        return $this->model
            ->newQuery()
            ->whereHas('deposit', function(Builder $query) use($user) {
                $query->where('users_id', $user->id)
                    ->whereHas('currency');
            })
            ->orderBy('transaction_at', 'DESC')
            ->get();
    }
}
