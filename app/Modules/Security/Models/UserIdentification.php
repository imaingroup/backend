<?php
declare(strict_types=1);

namespace App\Modules\Security\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Users\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * User identification model.
 *
 * @property-read int $id
 * @property-read int $users_id
 * @property-read User $user
 * @property-read UserIdentificationUserAgent $userAgent
 * @property-read Collection|UserIdentificationRelation[] $relations
 */
final class UserIdentification extends Model
{
    public const ACTION_LOGIN_SUCCESS = 1;
    public const ACTION_LOGIN_ERROR = 2;
    public const ACTION_REGISTER_SUCCESS = 3;
    public const ACTION_REGISTER_ERROR = 4;
    public const ACTION_FORGOT_SUCCESS = 5;
    public const ACTION_FORGOT_ERROR = 6;

    /** @var array Actions relation with identifications. */
    public const ACTIONS = [
        self::ACTION_LOGIN_SUCCESS => 'Login success',
        self::ACTION_LOGIN_ERROR => 'Login error',
        self::ACTION_REGISTER_SUCCESS => 'Register success ',
        self::ACTION_REGISTER_ERROR => 'Register error',
        self::ACTION_FORGOT_SUCCESS => 'Forgot success',
        self::ACTION_FORGOT_ERROR => 'Forgot error',
    ];

    /** {@inheritdoc} */
    protected $table = 'users_identifications';

    /**
     * Get relation with {@see User::class}.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Get relation with {@see UserIdentificationUserAgent::class}.
     *
     * @return BelongsTo
     */
    public function userAgent(): BelongsTo
    {
        return $this->belongsTo(UserIdentificationUserAgent::class, 'user_agent_id');
    }

    /**
     * Get relation with {@see UserIdentificationRelation::class}.
     *
     * @return HasMany
     */
    public function relations(): HasMany
    {
        return $this->hasMany(UserIdentificationRelation::class, 'users_identifications_id');
    }
}
