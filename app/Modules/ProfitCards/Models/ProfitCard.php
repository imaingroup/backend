<?php
declare(strict_types=1);

namespace App\Modules\ProfitCards\Models;

use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Core\Models\Model;
use App\Modules\Investments\Models\TradeOption;
use App\Modules\ProfitCards\Enums\Models\ProfitCard\Type;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use TCG\Voyager\Traits\Translatable;

/**
 * Profit card model.
 *
 * @property-read int $id
 * @property-read int $sid
 * @property string $title
 * @property string $image
 * @property string $type
 * @property integer $available_count
 * @property integer $abc
 * @property float $price
 * @property float $bonus
 * @property float $withdrawals_expanse
 * @property integer $sort
 * @property boolean $is_visible
 * @property boolean $show_number_of_cards
 * @property string|null $text_after_zero
 * @property string|null $stop_sell_after_zero
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Collection|ProfitCardTidExpanse[] $tidsExpanses
 * @property Type $typeEnum
 */
final class ProfitCard extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;

    protected $translatable = ['title'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sid' => 'integer',
        'title' => 'string',
        'image' => 'string',
        'type' => 'string',
        'available_count' => 'integer',
        'abc' => 'integer',
        'price' => 'float',
        'bonus' => 'float',
        'withdrawals_expanse' => 'float',
        'sort' => 'integer',
        'is_visible' => 'boolean',
        'show_number_of_cards' => 'boolean',
        'text_after_zero' => 'string',
        'stop_sell_after_zero' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Relation with {@see ProfitCardTidExpanse::class}.
     *
     * @return HasMany
     */
    public function tidsExpanses(): HasMany
    {
        return $this->hasMany(ProfitCardTidExpanse::class, 'profit_cards_id');
    }

    /**
     * Get attribute "typeEnum".
     *
     * @return Type
     */
    public function getTypeEnumAttribute(): Type
    {
        return Type::from($this->type);
    }

    /**
     * Get ProfitCardTidExpanse by TradeOption.
     *
     * @param TradeOption $tradeOption
     * @return ProfitCardTidExpanse|null
     */
    public function getTidExpanseByOption(TradeOption $tradeOption): ?ProfitCardTidExpanse
    {
        return $this->tidsExpanses
            ->where('trade_options_id', $tradeOption->id)
            ->first();
    }

    /**
     * Events for model.
     */
    protected static function booted()
    {
        ProfitCard::creating(function (self $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }
}
