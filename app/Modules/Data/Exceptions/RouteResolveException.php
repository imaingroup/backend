<?php
declare(strict_types=1);

namespace App\Modules\Data\Exceptions;

/**
 * RouteResolveException.
 */
final class RouteResolveException extends DataException
{
}
