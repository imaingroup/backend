<?php
declare(strict_types=1);

namespace App\Modules\Security\Dto\Services;

use App\Modules\Core\Dto\BaseDto;
use Illuminate\Support\Collection;

/**
 * Identifications data dto.
 *
 * @property-read Collection $relatedAccurate
 * @property-read Collection $relatedHighProbably
 * @property-read Collection|null $history
 * @property-read Collection|null $relatedIps
 * @property-read Collection|null $relatedUserAgents
 * @property-read Collection $relatedIdentificationsAccurate
 * @property-read Collection $relatedIdentificationsHighProbably
 */
final class IdentificationsDataDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param Collection $relatedAccurate
     * @param Collection $relatedHighProbably
     * @param Collection|null $history
     * @param Collection|null $relatedIps
     * @param Collection|null $relatedUserAgents
     * @param Collection $relatedIdentificationsAccurate
     * @param Collection $relatedIdentificationsHighProbably
     */
    public function __construct(
        protected Collection $relatedAccurate,
        protected Collection $relatedHighProbably,
        protected ?Collection $history,
        protected ?Collection $relatedIps,
        protected ?Collection $relatedUserAgents,
        protected Collection $relatedIdentificationsAccurate,
        protected Collection $relatedIdentificationsHighProbably,
    )
    {
    }

    /**
     * Get related accurate.
     *
     * @return Collection
     */
    public function getRelatedAccurate(): Collection
    {
        return $this->relatedAccurate;
    }

    /**
     * Get related high probably.
     *
     * @return Collection
     */
    public function getRelatedHighProbably(): Collection
    {
        return $this->relatedHighProbably;
    }

    /**
     * Get history.
     *
     * @return Collection
     */
    public function getHistory(): Collection
    {
        return $this->history;
    }

    /**
     * Get related ips.
     *
     * @return Collection
     */
    public function getRelatedIps(): Collection
    {
        return $this->relatedIps;
    }

    /**
     * Get related user agents.
     *
     * @return Collection
     */
    public function getRelatedUserAgents(): Collection
    {
        return $this->relatedUserAgents;
    }
}
