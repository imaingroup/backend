<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * GetBalanceException.
 */
final class GetBalanceException extends EthereumException
{
}
