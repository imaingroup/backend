<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates new deposits_transactions table.
 */
class CreateDepositsTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('deposits_id');
            $table->dateTime('transaction_at');
            $table->float('amount_coin', 16, 8);
            $table->float('amount_usdt', 16, 8);
            $table->unsignedTinyInteger('status');
            $table->string('txid', 255);
            $table->unsignedBigInteger('related_transactions_id')->nullable(true);
            $table->timestamps();

            $table->foreign('deposits_id')
                ->references('id')
                ->on('deposits');

            $table->foreign('related_transactions_id')
                ->references('id')
                ->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits_transactions');
    }
}
