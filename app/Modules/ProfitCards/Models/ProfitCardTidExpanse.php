<?php

namespace App\Modules\ProfitCards\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Investments\Models\TradeOption;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * This model storing expanse data for tid and profit cards.
 */
final class ProfitCardTidExpanse extends Model
{
    protected $table = 'profit_cards_tid_options_expanses';

    /**
     * Trade option relation.
     *
     * @return BelongsTo
     */
    public function tradeOption(): BelongsTo
    {
        return $this->belongsTo(TradeOption::class, 'trade_options_id');
    }
}
