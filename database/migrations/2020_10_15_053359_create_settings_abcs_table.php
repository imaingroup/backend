<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creating table settings_abc for store abc count settings.
 */
class CreateSettingsAbcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_abc', function (Blueprint $table) {
            $table->id();
            $table->string('total_supply_title', 255);
            $table->float('total_supply', 16, 8);
            $table->string('soft_cap_title', 255);
            $table->float('soft_cap', 16, 8);
            $table->string('abc_sold_title', 255);
            $table->float('abc_sold', 16, 8);
            $table->float('abc_sold_luft', 16, 8);
            $table->string('abc_left_title', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_abc');
    }
}
