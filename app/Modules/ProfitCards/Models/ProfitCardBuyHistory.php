<?php

namespace App\Modules\ProfitCards\Models;

use App\Modules\Money\Models\Transaction;
use App\Modules\Core\Models\Model;
use App\Modules\ProfitCards\Models\ProfitCard;
use App\Modules\Users\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

final class ProfitCardBuyHistory extends Model
{
    protected $table = 'profit_card_buy_histories';

    /**
     * Get relation with Transaction
     *
     * @return BelongsTo
     */
    public function transaction(): BelongsTo
    {
        return $this->belongsTo(\App\Modules\Money\Models\Transaction::class, 'related_transactions_id');
    }

    /**
     * Get relation with ProfitCard
     *
     * @return BelongsTo
     */
    public function card(): BelongsTo
    {
        return $this->belongsTo(ProfitCard::class, 'profit_cards_id');
    }

    /**
     * Get relation with User
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }
}
