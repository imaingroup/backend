<?php
declare(strict_types=1);

namespace App\Modules\Finance\Events;

use App\Modules\Core\Traits\ProtectedProperties;
use App\Modules\Finance\Models\CryptoTransaction;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * CryptoTransactionUpdatedEvent.
 *
 * @property-read CryptoTransaction $transaction
 */
final class CryptoTransactionUpdatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels, ProtectedProperties;

    /**
     * Constructor.
     *
     * @param CryptoTransaction $transaction
     */
    public function __construct(protected CryptoTransaction $transaction)
    {
    }
}
