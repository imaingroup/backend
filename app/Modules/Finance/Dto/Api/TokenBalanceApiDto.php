<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api;

use App\Modules\Core\Dto\BaseDto;

/**
 * TokenBalanceApiDto.
 *
 * @property-read int $id
 * @property-read float $balance
 * @property-read TokenApiDto $token
 */
final class TokenBalanceApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $id
     * @param float $balance
     * @param TokenApiDto $token
     */
    public function __construct(
        protected int $id,
        protected float $balance,
        protected TokenApiDto $token,
    )
    {
    }
}
