<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * This migration fixes model's namespaces in admin
 * after refactoring modules.
 */
class FixAdminNamespacesAfterRefactoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::beginTransaction();

        DB::table('data_rows')
            ->where('data_type_id', 11)
            ->where('field', 'video_belongsto_lang_relationship')
            ->update([
                'details' => '{"model":"App\\\\Modules\\\\Core\\\\Models\\\\Model\\\\Lang","table":"langs","type":"belongsTo","column":"langs_id","key":"id","label":"name","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 10)
            ->where('field', 'news_belongsto_news_category_relationship')
            ->update([
                'details' => '{"model":"App\\\\Modules\\\\News\\\\Models\\\\NewsCategory","table":"news_categories","type":"belongsTo","column":"news_categories_id","key":"id","label":"title","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 1)
            ->where('field', 'user_belongsto_lang_relationship')
            ->update([
                'details' => '{"model":"App\\\\Modules\\\\Core\\\\Models\\\\Model\\\\Lang","table":"langs","type":"belongsTo","column":"langs_id","key":"id","label":"name","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 18)
            ->where('field', 'trade_option_belongstomany_profit_card_relationship')
            ->update([
                'details' => '{"model":"App\\\\Modules\\\\ProfitCards\\\\Models\\\\ProfitCard","table":"profit_cards","type":"belongsToMany","column":"id","key":"id","label":"title","pivot_table":"trade_options_cards","pivot":"1","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 55)
            ->where('field', 'mails_variable_belongsto_mail_relationship')
            ->update([
                'details' => '{"model":"App\\\\Modules\\\\Notifications\\\\Models\\\\Mail","table":"mails","type":"belongsTo","column":"mails_id","key":"id","label":"name","pivot_table":"bitcoin_logs","pivot":"0","taggable":"0"}',
            ]);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::beginTransaction();

        DB::table('data_rows')
            ->where('data_type_id', 10)
            ->where('field', 'news_belongsto_news_category_relationship')
            ->update([
                'details' => '{"model":"App\\\\Models\\\\NewsCategory","table":"news_categories","type":"belongsTo","column":"news_categories_id","key":"id","label":"title","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 11)
            ->where('field', 'video_belongsto_lang_relationship')
            ->update([
                'details' => '{"model":"App\\\\Models\\\\Lang","table":"langs","type":"belongsTo","column":"langs_id","key":"id","label":"name","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 1)
            ->where('field', 'user_belongsto_lang_relationship')
            ->update([
                'details' => '{"model":"App\\\\Models\\\\Lang","table":"langs","type":"belongsTo","column":"langs_id","key":"id","label":"name","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 18)
            ->where('field', 'trade_option_belongstomany_profit_card_relationship')
            ->update([
                'details' => '{"model":"App\\\\Models\\\\ProfitCard","table":"profit_cards","type":"belongsToMany","column":"id","key":"id","label":"title","pivot_table":"trade_options_cards","pivot":"1","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 55)
            ->where('field', 'mails_variable_belongsto_mail_relationship')
            ->update([
                'details' => '{"model":"App\\\\Models\\\\Mail","table":"mails","type":"belongsTo","column":"mails_id","key":"id","label":"name","pivot_table":"bitcoin_logs","pivot":"0","taggable":"0"}',
            ]);

        DB::commit();
    }
}
