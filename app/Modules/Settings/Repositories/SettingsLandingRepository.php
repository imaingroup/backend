<?php


namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsLanding;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Settings landing repository.
 */
final class SettingsLandingRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Settings\Models\SettingsLanding $model
     */
    public function __construct(protected SettingsLanding $model)
    {}

    /**
     * Get settings model.
     *
     * @return \App\Modules\Settings\Models\SettingsLanding
     */
    public function get(): SettingsLanding
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
