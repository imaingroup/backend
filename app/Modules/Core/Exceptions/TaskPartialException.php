<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions;

use Exception;

/**
 * TaskPartialException.
 */
final class TaskPartialException extends Exception
{
}
