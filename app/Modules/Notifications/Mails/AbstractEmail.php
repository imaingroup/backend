<?php

namespace App\Modules\Notifications\Mails;

use App\Modules\Notifications\Repositories\MailRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use function array_values;
use function str_replace;

/**
 * Abstract mail class.
 */
abstract class AbstractEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->getMailData();

        return $this->subject($data['subject'])
            ->markdown('emails.template', [
                'content' => $data['content']
            ]);
    }

    /**
     * Get mail data.
     *
     * @return array
     */
    private function getMailData()
    {
        /** @var MailRepository $mailRepository */
        $mailRepository = app(MailRepository::class);
        $mail = $mailRepository->find($this->getMailId());

        $variables = $this->getVariables();
        $keys = array_keys($variables);
        $values = array_values($variables);

        $data = [];
        $data['subject'] = str_replace($keys, $values, $mail->subject);
        $data['content'] = str_replace($keys, $values, $mail->mail);;

        return $data;
    }

    /**
     * Get variables.
     *
     * @return array
     */
    abstract protected function getVariables(): array;

    /**
     * Get mail id.
     *
     * @return int
     */
    abstract protected function getMailId(): int;
}
