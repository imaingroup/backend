<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Controllers;

use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Core\Exceptions\DataException;
use App\Http\Controllers\Controller;
use App\Modules\Withdraw\Requests\WithdrawCreateRequest;
use App\Modules\Withdraw\Requests\WithdrawHistoryRequest;
use App\Modules\Withdraw\Requests\WithdrawInfoRequest;
use App\Modules\Users\Models\User;
use App\Modules\Core\Services\DataService;
use App\Modules\Withdraw\Services\WithdrawService;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

/**
 * Withdraw controller.
 */
final class WithdrawController extends Controller
{

    /**
     * Get info.
     *
     * @param WithdrawInfoRequest $request
     * @return ResponseDto
     * @throws Exception
     */
    public function data(WithdrawInfoRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getWithdrawService()
                ->data($request->validated())
        );
    }

    /**
     * Withdraw history.
     *
     * @param WithdrawHistoryRequest $request
     * @return ResponseDto
     * @throws Exception
     */
    public function history(WithdrawHistoryRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getWithdrawService()
                ->history($request->validated())
        );
    }

    /**
     * Create withdraw request.
     *
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function create(): Response
    {
        return $this->inTransaction(function (): void {
            $user = Auth::user();

            User::refreshAndLockForUpdate($user);

            $this->getWithdrawService()
                ->create(
                    ...app(WithdrawCreateRequest::class)
                    ->validated()
                );
        });
    }

    /**
     * Get {@see WithdrawService::class}.
     *
     * @return WithdrawService
     */
    private function getWithdrawService(): WithdrawService
    {
        return app(WithdrawService::class);
    }

    /**
     * Get DataService.
     *
     * @return \App\Modules\Core\Services\DataService
     */
    private function getDataService(): DataService
    {
        return app(DataService::class);
    }
}
