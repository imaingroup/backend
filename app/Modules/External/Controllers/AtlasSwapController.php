<?php
declare(strict_types=1);

namespace App\Modules\External\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\External\Requests\ContactMainRequest;
use App\Modules\External\Requests\ContactRequest;
use App\Modules\External\Requests\SubscribeRequest;
use App\Modules\External\Services\AtlasSwapService;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Landing controller need for atlas swap page.
 */
final class AtlasSwapController extends Controller
{
    /**
     * Subscribe action.
     *
     * @param SubscribeRequest $request
     * @return array
     */
    #[ArrayShape(['status' => "bool"])]
    public function subscribe(SubscribeRequest $request): array
    {
        $this->getAtlasSwapService()
            ->subscribe(...$request->validated());

        return ['status' => true];
    }

    /**
     * Contact action.
     *
     * @param ContactRequest $request
     * @return array
     */
    #[ArrayShape(['status' => "bool"])]
    public function contact(ContactRequest $request): array
    {
        $this->getAtlasSwapService()
            ->contact(...$request->validated());

        return ['status' => true];
    }

    /**
     * Contact main action.
     *
     * @param ContactMainRequest $request
     * @return array
     */
    #[ArrayShape(['status' => "bool"])]
    public function contactMain(ContactMainRequest $request): array
    {
        $this->getAtlasSwapService()
            ->contactMain(...$request->validated());

        return ['status' => true];
    }

    /**
     * Contact action.
     *
     * @param ContactRequest $request
     * @return array
     */
    #[ArrayShape(['status' => "bool"])]
    public function contactV2(ContactRequest $request): array
    {
        $this->getAtlasSwapService()
            ->contact(...$request->validated());

        return ['status' => true];
    }

    /**
     * Get {@see AtlasSwapService::class}.
     *
     * @return AtlasSwapService
     */
    private function getAtlasSwapService(): AtlasSwapService
    {
        return app(AtlasSwapService::class);
    }
}
