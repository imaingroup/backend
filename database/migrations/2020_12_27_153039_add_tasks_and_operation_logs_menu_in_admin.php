<?php

use App\Modules\Money\Models\Transaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration adds tasks and operation logs menu in admin.
 */
final class AddTasksAndOperationLogsMenuInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::beginTransaction();

        $systemMenuId = DB::table('menu_items')
            ->insertGetId([
                'menu_id' => 1,
                'title' => 'System',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-logbook',
                'color' => '#000000',
                'order' => 9,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);

        DB::table('menu_items')
            ->insertGetId([
                'menu_id' => 1,
                'title' => 'Tasks',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-logbook',
                'color' => '#000000',
                'parent_id' => $systemMenuId,
                'order' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
                'route' => 'voyager.tasks.index'
            ]);

        DB::table('menu_items')
            ->insertGetId([
                'menu_id' => 1,
                'title' => 'Operation logs',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-logbook',
                'color' => '#000000',
                'parent_id' => $systemMenuId,
                'order' => 1,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
                'route' => 'voyager.operation-logs.index'
            ]);

        $taskTypeId = DB::table('data_types')
            ->insertGetId([
                'name' => 'tasks',
                'slug' => 'tasks',
                'display_name_singular' => 'Task',
                'display_name_plural' => 'Tasks',
                'model_name' => 'App\Modules\Core\Models\Task',
                'controller' => 'App\Modules\Admin\Controllers\VoyagerTasksController',
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":"id","scope":null}',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);

        $taskOperationLogsId = DB::table('data_types')
            ->insertGetId([
                'name' => 'operation_logs',
                'slug' => 'operation-logs',
                'display_name_singular' => 'Operation log',
                'display_name_plural' => 'Operation logs',
                'model_name' => 'App\Modules\Logs\Models\OperationLog',
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":"id","scope":null}',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);

        $taskFields = [
            //tasks
            [$taskTypeId, 'id', 'number', 'Id', 1, 1, 1, 0, 0, 0, '{}', 1],
            [$taskTypeId, 'type', 'select_dropdown', 'Type', 1, 1, 1, 0, 0, 0, '{"options":{"1":"Mass change abc balance","2":"Mass change usdt deposited balance","3":"Mass change usdt dividends balance"}}', 4],
            [$taskTypeId, 'status', 'select_dropdown', 'Status', 1, 1, 1, 0, 0, 0, '{"options":{"1":"New","2":"In process","3":"Fails","4":"Processed"}}', 5],
            [$taskTypeId, 'users_id', 'number', 'Users Id', 1, 1, 1, 0, 0, 0, '{}', 2],
            [$taskTypeId, 'operation_logs_id', 'number', 'Operation Logs Id', 0, 1, 1, 0, 0, 0, '{}', 3],
            [$taskTypeId, 'data', 'text_area', 'Data', 1, 0, 1, 0, 0, 0, '{}', 6],
            [$taskTypeId, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 7],
            [$taskTypeId, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 8],
            //operation logs
            [$taskOperationLogsId, 'id', 'number', 'Id', 1, 1, 1, 0, 0, 0, '{}', 1],
            [$taskOperationLogsId, 'type', 'select_dropdown', 'Type', 1, 1, 1, 0, 0, 0, '{"options":{"1":"Mass money operation"}}', 2],
            [$taskOperationLogsId, 'users_id', 'number', 'Users Id', 1, 1, 1, 0, 0, 0, '{}', 3],
            [$taskOperationLogsId, 'data', 'text_area', 'Data', 1, 0, 1, 0, 0, 0, '{}', 5],
            [$taskOperationLogsId, 'description', 'text_area', 'Description', 1, 0, 1, 0, 0, 0, '{}', 6],
            [$taskOperationLogsId, 'tasks_id', 'number', 'Users Id', 0, 1, 1, 0, 0, 0, '{}', 7],
            [$taskOperationLogsId, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 8],
            [$taskOperationLogsId, 'updated_at', 'timestamp', 'Updated At', 0, 1, 1, 0, 0, 0, '{}', 9],
        ];

        foreach ($taskFields as $item) {
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $item[0],
                    'field' => $item[1],
                    'type' => $item[2],
                    'display_name' => $item[3],
                    'required' => $item[4],
                    'browse' => $item[5],
                    'read' => $item[6],
                    'edit' => $item[7],
                    'add' => $item[8],
                    'delete' => $item[9],
                    'details' => $item[10],
                    'order' => $item[11]
                ]);
        }

        $permissions = [
            ['browse_tasks', 'tasks'],
            ['read_tasks', 'tasks'],
            ['browse_operation_logs', 'operation_logs'],
            ['read_operation_logs', 'operation_logs'],
        ];

        foreach ($permissions as $item) {
            $permissionId = DB::table('permissions')
                ->insertGetId([
                    'key' => $item[0],
                    'table_name' => $item[1],
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()
                ]);

            DB::table('permission_role')
                ->insertGetId([
                    'permission_id' => $permissionId,
                    'role_id' => 1
                ]);
        }

        $dataTypeTransactionId = DB::table('data_types')
            ->where('name', 'transactions')
            ->first()
            ->id;

        DB::table('data_rows')
            ->where('data_type_id', $dataTypeTransactionId)
            ->where('field', 'currency')
            ->update([
                'type' => 'select_dropdown',
                'details' => json_encode(['options' => Transaction::$currencyName]),
                'order' => 3
            ]);

        DB::table('data_rows')
            ->where('data_type_id', $dataTypeTransactionId)
            ->where('field', 'type')
            ->update([
                'type' => 'select_dropdown',
                'details' => json_encode(['options' => Transaction::TYPES])
            ]);

        DB::table('data_rows')
            ->where('data_type_id', $dataTypeTransactionId)
            ->where('field', 'created_at')
            ->update([
                'browse' => 0
            ]);

        //operation_type
        DB::table('data_rows')
            ->insert([
                'data_type_id' => $dataTypeTransactionId,
                'field' => 'operation_type',
                'type' => 'select_dropdown',
                'display_name' => 'Operation type',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'details' => json_encode(['options' => Transaction::OPERATION_TYPES]),
                'order' => 4
            ]);

        DB::table('data_rows')
            ->insert([
                'data_type_id' => $dataTypeTransactionId,
                'field' => 'tasks_id',
                'type' => 'number',
                'display_name' => 'Task id',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'details' => json_encode(['options' => Transaction::OPERATION_TYPES]),
                'order' => 7
            ]);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::beginTransaction();

        DB::table('menu_items')
            ->where([
                'menu_id' => 1,
                'title' => 'System',
            ])
            ->delete();

        DB::table('menu_items')
            ->whereIn('route', [
                'voyager.tasks.index',
                'voyager.operation-logs.index'
            ])
            ->delete();

        DB::table('data_types')
            ->whereIn('model_name', [
                'App\Modules\Core\Models\Task',
                'App\Modules\Logs\Models\OperationLog',
            ])
            ->delete();

        DB::table('permissions')
            ->whereIn('table_name', [
                'tasks',
                'operation_logs'
            ])
            ->delete();

        $dataTypeTransactionId = DB::table('data_types')
            ->where('name', 'transactions')
            ->first()
            ->id;

        DB::table('data_rows')
            ->where('data_type_id', $dataTypeTransactionId)
            ->whereIn('field', [
                'operation_type',
                'tasks_id'
            ])
            ->delete();

        DB::commit();
    }
}
