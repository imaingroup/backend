<?php
declare(strict_types=1);

namespace App\Modules\Data\Dto\Params;

use App\Modules\Core\Dto\BaseDto;
use Illuminate\Support\Collection;

/**
 * Data route params dto.
 *
 * @property-read string $route
 * @property-read Collection $parameters
 * @property-read Collection $requests
 * @property-read int $version
 * @property-read RouteActionParamsDto $action
 */
final class DataRouteParamsDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param string $route
     * @param Collection $parameters
     * @param Collection $requests
     * @param int $version
     * @param RouteActionParamsDto $action
     */
    public function __construct(
        protected string $route,
        protected Collection $parameters,
        protected Collection $requests,
        protected int $version,
        protected RouteActionParamsDto $action,
    )
    {
    }
}
