<?php
declare(strict_types=1);

namespace App\Modules\Core\Validation;

use App\Modules\Finance\Services\BtcApiService;
use Illuminate\Validation\Validator;
use Throwable;

/**
 * This class validates that value is any crypto tx id.
 */
final class CryptoTx
{
    /**
     * Validate.
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     * @param Validator $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        if (!is_string($value)) {
            return false;
        }

        try {
            if(hexdec($value) > pow(10, 18)) {
                return true;
            }
        } catch (Throwable) {}

        try {
            $this->getBtcApiService()->getTransaction($value);

            return true;
        } catch (Throwable) {}

        return false;
    }

    /**
     * Get BtcApiService.
     *
     * @return BtcApiService
     */
    private function getBtcApiService(): BtcApiService
    {
        return app(BtcApiService::class);
    }
}
