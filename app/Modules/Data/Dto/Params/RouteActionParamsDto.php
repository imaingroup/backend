<?php
declare(strict_types=1);

namespace App\Modules\Data\Dto\Params;

use App\Modules\Core\Attributes\Updatable;
use App\Modules\Core\Dto\BaseDto;
use App\Modules\Data\Requests\DataRequest;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * RouteActionDto.
 *
 * @property-read string $class
 * @property-read string $method
 * @property-read string $httpMethod
 * @property-read Updatable|null $updatableAttribute
 */
final class RouteActionParamsDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param string $class
     * @param string $method
     * @param string $httpMethod
     * @param Updatable|null $updatableAttribute
     */
    public function __construct(
        protected string $class,
        protected string $method,
        #[ExpectedValues(DataRequest::SUPPORTED_METHODS)] protected string $httpMethod,
        protected ?Updatable $updatableAttribute,
    )
    {
    }
}
