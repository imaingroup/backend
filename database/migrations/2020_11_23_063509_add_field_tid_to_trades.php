<?php

use App\Modules\Investments\Models\Trade;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds tid field to trades table.
 */
class AddFieldTidToTrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->unsignedBigInteger('tid')->unique()->nullable(true);
        });

        foreach(Trade::all() as $trade) {
            $trade->tid = mt_rand(100000000000, 999999999999);
            $trade->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->dropColumn('tid');
        });
    }
}
