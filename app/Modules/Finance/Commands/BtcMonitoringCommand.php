<?php
declare(strict_types=1);

namespace App\Modules\Finance\Commands;

use App\Modules\Logs\Loggers\Logger;
use App\Modules\Finance\Services\MonitoringBtcService;
use Illuminate\Console\Command;
use Throwable;

/**
 * This command need for monitoring btc transactions.
 */
final class BtcMonitoringCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finance:btc_monitoring';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finance btc monitoring';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        try {
            $this->getMonitoringBtcService()
                ->runBtcMonitoring();
        }
        catch (Throwable $e) {
            $this->getLogger()
                ->registerException($e);
        }

        return 0;
    }

    /**
     * Get MonitoringBtcService.
     *
     * @return MonitoringBtcService
     */
    private function getMonitoringBtcService(): MonitoringBtcService
    {
        return app(MonitoringBtcService::class);
    }

    /**
     * Get Logger.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
