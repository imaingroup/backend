<?php
declare(strict_types=1);

namespace App\Modules\Messages\Exceptions;

/**
 * Send message exception.
 */
final class SendMessageException extends MessageException
{

}
