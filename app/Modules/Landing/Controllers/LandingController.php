<?php

namespace App\Modules\Landing\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Landing\Requests\LandingContactRequest;
use App\Modules\Landing\Requests\LandingSubscribeRequest;
use App\Modules\Landing\Services\LandingService;
use Illuminate\Support\Facades\Auth;

/**
 * Landing controller need for landing page.
 */
final class LandingController extends Controller
{
    /** @var LandingService Landing service. */
    private LandingService $landingService;

    /**
     * Constructor.
     *
     * @param LandingService $landingService
     */
    public function __construct(LandingService $landingService)
    {
        $this->landingService = $landingService;
    }

    /**
     * Get content action.
     * @return array
     */
    public function content(): array
    {
        return ['data' => $this->landingService->getContent()];
    }

    /**
     * Subscribe action.
     *
     * @param \App\Modules\Landing\Requests\LandingSubscribeRequest $request
     * @return array
     */
    public function subscribe(LandingSubscribeRequest $request): array
    {
        $this->landingService->createSubscribe($request->validated()['email'], Auth::user());

        return ['status' => true];
    }

    /**
     * Contact action.
     *
     * @param LandingContactRequest $request
     * @return array
     */
    public function contact(LandingContactRequest $request): array
    {
        $this->landingService->createContact($request->validated(), Auth::user());

        return ['status' => true];
    }
}
