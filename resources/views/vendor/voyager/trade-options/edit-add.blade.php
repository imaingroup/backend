@extends('voyager::bread-base.edit-add')

@section('append_form_fields')
    <div style="border-bottom:0;">
        <hr style="margin:0;"/>
        <h3 class="panel-title">Comments</h3>
    </div>
    <div class="panel-body" style="padding-top:0;">
        Minimum image size for field "Statement link" 720 pixels in width.
    </div>
@endsection
