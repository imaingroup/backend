<?php
declare(strict_types=1);

namespace App\Modules\Data\Dto\Params;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Users\Models\User;
use Illuminate\Support\Collection;

/**
 * Data params dto.
 *
 * @property-read Collection|DataRouteParamsDto[] $methods
 * @property-read bool $always
 * @property-read User $user
 */
final class DataParamsDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param Collection $methods
     * @param bool $always
     * @param User $user
     */
    public function __construct(
        protected Collection $methods,
        protected bool $always,
        protected User $user,
    )
    {
    }
}
