<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Finance;

use App\Modules\Users\Models\User;
use App\Modules\Finance\Exceptions\Btc\StatusException;
use App\Modules\Finance\Services\BtcApiService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;
use function app;

/**
 * AdminFinanceBtcSendRequest.
 */
final class AdminFinanceBtcSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            $user->hasRole(User::ROLE_ADMIN_NAME) &&
            in_array($user->id, User::USERS_ALLOWED_SEND_MONEY);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'to' => "string",
        'amount' => "string",
        'g2fa' => "string",
    ])]
    public function rules(): array
    {
        return [
            'to' => 'required|string|bitcoin',
            'amount' => 'required|numeric|min:0.00000001',
            'g2fa' => 'required|g2fa_install|g2fa',
        ];
    }

    #[ArrayShape([
        'user' => "App\\Modules\\Users\\Models\\User",
        'to' => "string",
        'amount' => "float",
    ])]
    public function validated(): array
    {
        $data = parent::validated();

        return [
            'user' => Auth::user(),
            'to' => $data['to'],
            'amount' => (float) $data['amount']
        ];
    }
    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function (Validator $validator) {
            if (count($validator->failed()) === 0) {
                $this->after($validator);
            }
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    private function after(Validator $validator)
    {
        try {
            $this->getBtcApiService()->checkStatus();
        }
        catch (StatusException) {
            $validator->errors()->add('global', 'BTC server not available.');
        }
    }

    /**
     * Get BtcApiService.
     *
     * @return BtcApiService
     */
    private function getBtcApiService(): BtcApiService
    {
        return app(BtcApiService::class);
    }
}
