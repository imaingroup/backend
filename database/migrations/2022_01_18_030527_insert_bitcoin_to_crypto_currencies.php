<?php
declare(strict_types=1);

use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * Insert bitcoin to crypto currencies.
 */
final class InsertBitcoinToCryptoCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::transaction(function () {
            DB::table('crypto_currencies')
                ->insert([
                    'crypto_networks_id' => DB::table('crypto_networks')
                        ->where('code', BitcoinNetworkEnum::BITCOIN->value)
                        ->first()
                        ->id,
                    'name' => 'Bitcoin',
                    'symbol' => 'BTC',
                    'decimals' => 8,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::transaction(function () {
            DB::table('crypto_currencies')
                ->where([
                    'crypto_networks_id' => DB::table('crypto_networks')
                        ->where('code', BitcoinNetworkEnum::BITCOIN->value)
                        ->first()
                        ->id,
                    'symbol' => 'BTC',
                ])
                ->whereNull('address')
                ->delete();
        });
    }
}
