<?php


namespace App\Modules\Withdraw\Exceptions;


use Exception;

/**
 * This exception may be throws in withdraw service.
 */
class WithdrawServiceException extends Exception
{

}
