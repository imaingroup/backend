<?php

namespace App\Modules\Exchange\Requests;


use App\Modules\Settings\Repositories\SettingsDepositRepository;
use App\Modules\Settings\Repositories\SettingsPriceRepository;
use App\Modules\Users\Services\UserService;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This class validates create deposit request.
 */
final class ExchangeUsdtAbcRequest extends FormRequest
{
    /** @var SettingsPriceRepository Settings price repository. */
    private SettingsPriceRepository $settingsPriceRepository;

    /** @var UserService User service. */
    private UserService $userService;

    /**
     * Construct.
     *
     * @param \App\Modules\Settings\Repositories\SettingsPriceRepository $settingsPriceRepository
     * @param \App\Modules\Settings\Repositories\SettingsDepositRepository $settingsDepositRepository
     * @param UserService $userService
     */
    public function __construct(
        SettingsPriceRepository $settingsPriceRepository,
        UserService $userService
    )
    {
        $this->settingsPriceRepository = $settingsPriceRepository;
        $this->userService = $userService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'amount_usdt' => 'required|scalar|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'accepted_rules' => 'scalar|accepted'
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws Exception
     */
    public function after(Validator $validator)
    {
        $user = Auth::user();

        if(is_scalar($this['amount_usdt'])) {
            if($user->balance_usdt < (float) $this['amount_usdt']) {
                $validator->errors()->add('amount_usdt', 'Insufficient USDT funds');
                return;
            }
        }
    }
}
