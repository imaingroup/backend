<?php

namespace App\Modules\Notifications\Jobs;

use App\Modules\Notifications\Mails\DepositBlockAdminEmail;
use App\Modules\Notifications\Mails\MailSender;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * This job sends message in case if was executed method
 * SystemService::blockDepositModule.
 */
final class SendDepositBlockAdminEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MailSender::send(env('MAIL_ADMIN_ADDRESS'), new DepositBlockAdminEmail());
    }
}
