<?php


namespace App\Modules\Settings\Services;


use App\Modules\Settings\Repositories\SettingsAbcRepository;

/**
 * Abc settings service.
 */
final class AbcSettingsService
{
    /** @var \App\Modules\Settings\Repositories\SettingsAbcRepository Abc repository. */
    private SettingsAbcRepository $settingsAbcRepository;

    /**
     * Constructor.
     *
     * @param \App\Modules\Settings\Repositories\SettingsAbcRepository $settingsAbcRepository
     */
    public function __construct(SettingsAbcRepository $settingsAbcRepository)
    {
        $this->settingsAbcRepository = $settingsAbcRepository;
    }

    /**
     * Get data.
     *
     * @return array
     */
    public function getData(): array
    {
        $settings = $this->settingsAbcRepository->get();

        $abcSold = $settings->abc_sold + $settings->abc_sold_luft;

        return [
            'total_supply_title' => $settings->getTranslates('total_supply_title'),
            'total_supply' => $settings->total_supply,
            'soft_cap_title' => $settings->getTranslates('soft_cap_title'),
            'soft_cap' => $settings->soft_cap,
            'abc_sold_title' => $settings->getTranslates('abc_sold_title'),
            'abc_sold' => $abcSold,
            'abc_left_title' => $settings->getTranslates('abc_left_title'),
            'abc_left' => $settings->soft_cap - $abcSold
        ];
    }
}
