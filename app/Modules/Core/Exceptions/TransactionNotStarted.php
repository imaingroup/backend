<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions;

use Exception;

/**
 * TransactionNotStarted.
 */
final class TransactionNotStarted extends Exception
{
}
