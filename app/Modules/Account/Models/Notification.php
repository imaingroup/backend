<?php

namespace App\Modules\Account\Models;

use App\Modules\Notifications\Models\NotificationUser;
use App\Modules\Core\Models\Model;
use App\Modules\Users\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class Notification extends Model
{
    /** @var string Table name. */
    protected $table = 'notifications';

    /**
     * Events for model.
     */
    protected static function booted()
    {
        static::creating(function (Notification $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }

    /**
     * Get NotificationUser relation.
     *
     * @return HasMany
     */
    public function notificationsUsers(): HasMany
    {
        return $this->hasMany(\App\Modules\Notifications\Models\NotificationUser::class, 'notifications_id');
    }

    /**
     * Get notification user.
     *
     * @param User $user
     * @return \App\Modules\Notifications\Models\NotificationUser|null
     */
    public function notificationUser(User $user): ?NotificationUser
    {
        return $this->notificationsUsers()
            ->where('users_id', $user->id)
            ->first();
    }

    /**
     * Have read.
     *
     * @param User $user
     * @return bool
     */
    public function haveRead(User $user): bool
    {
        return $this->notificationsUsers()
            ->where('users_id', $user->id)
            ->exists();
    }

    /**
     * Check that it have deleted.
     *
     * @param User $user
     * @return bool
     */
    public function haveDeleted(User $user): bool
    {
        return $this->notificationsUsers()
            ->where('users_id', $user->id)
            ->where('deleted', true)
            ->exists();
    }
}
