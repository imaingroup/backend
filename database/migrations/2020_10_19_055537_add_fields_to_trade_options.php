<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add fields to trade options.
 */
class AddFieldsToTradeOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->integer('amount_min')->after('period_max');
            $table->integer('amount_max')->after('amount_min');
            $table->tinyInteger('percent')->after('amount_max');
            $table->tinyInteger('max_quantity')->after('percent');
            $table->integer('requirements_total_deposit')->after('max_quantity')->nullable(true);
            $table->dropColumn('period_type');
        });

        Schema::create('trade_options_cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('trade_options_id');
            $table->unsignedBigInteger('profit_cards_id');

            $table->timestamps();

            $table->foreign('trade_options_id')
                ->references('id')
                ->on('trade_options')
                ->onDelete('CASCADE');

            $table->foreign('profit_cards_id')
                ->references('id')
                ->on('profit_cards')
                ->onDelete('CASCADE');

            $table->unique(['trade_options_id', 'profit_cards_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_options_cards');

        Schema::table('trade_options', function (Blueprint $table) {
            $table->tinyInteger('period_type');

            $table->dropColumn('amount_min');
            $table->dropColumn('amount_max');
            $table->dropColumn('percent');
            $table->dropColumn('max_quantity');
            $table->dropColumn('requirements_total_deposit');
        });
    }
}
