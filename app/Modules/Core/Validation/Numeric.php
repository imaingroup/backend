<?php


namespace App\Modules\Core\Validation;


/**
 * This class validating type is numeric value.
 */
final class Numeric
{
    /**
     * Validate.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator): bool
    {
        return is_numeric($value);
    }
}
