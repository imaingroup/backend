<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->dateTime('published_at');
            $table->string('title', 255);
            $table->text('external_preview');
            $table->text('catalog_preview');
            $table->string('preview_image');
            $table->longText('images');
            $table->string('video');
            $table->boolean('is_view_lk');
            $table->boolean('is_view_landing');
            $table->integer('sort');
            $table->boolean('is_visible');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
