<?php

namespace App\Modules\Notifications\Mails;

use App\Modules\Landing\Models\Contact;
use App\Modules\Notifications\Mails\AbstractEmail;
use App\Modules\Notifications\Models\Mail;
use function protectFromXss;

/**
 * Email class for sending message from contact form on landing page
 * to administrator email.
 */
final class ContactFormAdminEmail extends AbstractEmail
{
    /** @var Contact Contact model. */
    private Contact $contact;

    /**
     * Create a new job instance.
     *
     * @param \App\Modules\Landing\Models\Contact $contact
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * {@inheritdoc}
     */
    protected function getVariables(): array
    {
        return [
            '{name}' => protectFromXss($this->contact->name),
            '{email}' => $this->contact->email,
            '{message}' => protectFromXss($this->contact->message),
            '{users_id}' => $this->contact->users_id,
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getMailId(): int
    {
        return Mail::MAIL_LANDING_CONTACT;
    }
}
