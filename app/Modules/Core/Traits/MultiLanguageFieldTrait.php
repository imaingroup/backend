<?php


namespace App\Modules\Core\Traits;

use Illuminate\Support\Facades\Config;

/**
 * Trait for adding method getTranslates to models.
 */
trait MultiLanguageFieldTrait
{
    /**
     * Get all translates for field.
     *
     * @param string $field
     * @return array
     */
    public function getTranslates(string $field): array
    {
        $locales = array_keys(Config::get('app.locales'));
        $fallbackLocale = Config::get('app.fallback_locale');

        $data = [];
        foreach($locales as $locale) {
            $data[$locale] = $this->getTranslatedAttribute($field, $locale, $fallbackLocale);;
        }

        return $data;
    }

    /**
     * Get all translates for field.
     *
     * @param string $field
     * @param mixed $value
     * @return array
     */
    public function getTranslatesWithValue(string $field, mixed $value): array
    {
        $locales = array_keys(Config::get('app.locales'));

        $data = [];
        foreach($locales as $locale) {
            $data[$locale] = $value;
        }

        return $data;
    }
}
