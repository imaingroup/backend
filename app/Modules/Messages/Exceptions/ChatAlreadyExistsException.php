<?php
declare(strict_types=1);


namespace App\Modules\Messages\Exceptions;

/**
 * Chat already exists exception.
 */
final class ChatAlreadyExistsException extends MessageException
{

}
