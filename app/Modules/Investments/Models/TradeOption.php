<?php
declare(strict_types=1);

namespace App\Modules\Investments\Models;

use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Core\Models\Model;
use App\Modules\Users\Models\User;
use DateTime;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use TCG\Voyager\Traits\Translatable;

/**
 * Trade option model.
 *
 * @property string $title
 * @property integer $period_min
 * @property integer $period_max
 * @property integer $amount_min
 * @property integer $amount_max
 */
final class TradeOption extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;

    protected $translatable = ['title', 'description'];

    const ACCRUAL_END = 1;
    const ACCRUAL_DAILY = 2;
    const ACCRUAL_30_DAYS = 3;
    const ACCRUAL_60_DAYS = 4;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'chart_enabled' => 'boolean',
        'statement_link' => 'string',
        'previous_percent' => 'float',
        'previous_updated_at' => 'datetime',
        'title' => 'string',
        'period_min' => 'integer',
        'period_max' => 'integer',
        'amount_min' => 'integer',
        'amount_max' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Events for model.
     */
    protected static function booted()
    {
        static::creating(function (TradeOption $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });

        static::saving(function (TradeOption $model) {
            if ($model->isDirty('previous_percent') && $model->previous_percent !== null) {
                $startDate = (new DateTime((new DateTime())->format('Y-m-01 00:00:00')))
                    ->modify('-1 DAY');

                $model->previous_updated_at = $startDate;
            }
        });
    }

    /**
     * Related with ProfitCard.
     *
     * @return BelongsToMany
     */
    public function requiredCards(): BelongsToMany
    {
        return $this->belongsToMany(\App\Modules\ProfitCards\Models\ProfitCard::class, 'trade_options_cards', 'trade_option_id', 'profit_card_id')
            ->orderBy('profit_cards.id');
    }

    /**
     * Get attribute "title" by user.
     *
     * @param User $user
     * @return string
     */
    public function getTitleByUser(User $user): string
    {
        if ($user->tid_default) {
            return $this->title;
        }

        return $user->getCustomTidOptionsByOption($this)->title ?? $this->title;
    }

    /**
     * Get attribute "period_min" by user.
     *
     * @param User $user
     * @return int
     */
    public function getPeriodMinByUser(User $user): int
    {
        if ($user->tid_default) {
            return $this->period_min;
        }

        return $user->getCustomTidOptionsByOption($this)->period_min ?? $this->period_min;
    }

    /**
     * Get attribute "period_max" by user.
     *
     * @param User $user
     * @return int
     */
    public function getPeriodMaxByUser(User $user): int
    {
        if ($user->tid_default) {
            return $this->period_max;
        }

        return $user->getCustomTidOptionsByOption($this)->period_max ?? $this->period_max;
    }

    /**
     * Get attribute "amount_min" by user.
     *
     * @param User $user
     * @return int
     */
    public function getAmountMinByUser(User $user): int
    {
        if ($user->tid_default) {
            return $this->amount_min;
        }

        return $user->getCustomTidOptionsByOption($this)->amount_min ?? $this->amount_min;
    }

    /**
     * Get attribute "amount_max" by user.
     *
     * @param User $user
     * @return int
     */
    public function getAmountMaxByUser(User $user): int
    {
        if ($user->tid_default) {
            return $this->amount_max;
        }

        return $user->getCustomTidOptionsByOption($this)->amount_max ?? $this->amount_max;
    }
}
