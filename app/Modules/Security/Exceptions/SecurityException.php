<?php
declare(strict_types=1);

namespace App\Modules\Security\Exceptions;

use Exception;

/**
 * SecurityException abstract.
 */
abstract class SecurityException extends Exception
{
}
