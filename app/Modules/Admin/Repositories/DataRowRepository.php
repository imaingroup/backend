<?php


namespace App\Modules\Admin\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use TCG\Voyager\Models\DataRow;

/**
 * Data row repository.
 */
final class DataRowRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param DataRow $model
     */
    public function __construct(protected DataRow $model)
    {}

    /**
     * Get DataType by model name.
     *
     * @param int $dataTypeId
     * @return Collection
     */
    public function getByDataTypeId(int $dataTypeId): Collection
    {
        return $this->model
            ->newQuery()
            ->where('data_type_id', $dataTypeId)
            ->orderBy('id', 'ASC')
            ->get();
    }

    /**
     * Find by field and data type id.
     *
     * @param int $dataTypeId
     * @param string $field
     * @return DataRow|null
     */
    public function findByField(int $dataTypeId, string $field): ?DataRow
    {
        return $this->model
            ->newQuery()
            ->where('data_type_id', $dataTypeId)
            ->where('field', $field)
            ->first();
    }
}
