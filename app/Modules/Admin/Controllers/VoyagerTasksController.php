<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Requests\Tasks\AdminTasksGetRequest;
use App\Modules\Core\Services\TaskService;

/**
 * Admin tasks controller.
 */
final class VoyagerTasksController extends VoyagerBaseController
{
    /** @var TaskService Task service. */
    private TaskService $taskService;

    /**
     * Constructor.
     *
     * @param TaskService $taskService
     */
    public function __construct(
        TaskService $taskService
    )
    {
        $this->taskService = $taskService;
    }

    /**
     * Get tasks.
     *
     * @param AdminTasksGetRequest $request
     * @return array
     */
    public function get(AdminTasksGetRequest $request): array
    {
        return $this->taskService->getByTypes($request->validated()['types']);
    }
}
