@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Finance global actions')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-folder"></i> Finance global actions
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid" id="finance-actions">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form @submit.prevent="moneyOperation">
                            <div class="form-group">
                                <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                    <h2>Mass money operation</h2>
                                </div>

                                <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                    <div>This action add or withdraw money to/from users who have activated their
                                        account and logged
                                        into their account at least once.
                                    </div>

                                    <div class="form-group">
                                        <input type="number" value="" placeholder="Amount"
                                               step="0.01" class="form-control" v-model="form.amount"/>
                                        <div v-if="errors.amount" v-for="error in errors.amount"
                                             class="text-error amount">@{{ error }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <select placeholder="Money type" class="form-control" v-model="form.type">
                                            <option value="">Select type</option>
                                            <option value="abc">ABC</option>
                                            <option value="usdt_deposited">USDT (Deposited)</option>
                                            <option value="usdt_dividents">USDT (Dividents)</option>
                                        </select>
                                        <div v-if="errors.type" v-for="error in errors.type" class="text-error amount">
                                            @{{ error }}
                                        </div>
                                    </div>

                                    <input type="submit" style="margin-top: 15px"
                                           value="Execute operation" class="form-control"/>
                                </div>
                            </div>
                        </form>

                        <div class="form-group col-md-{{ $display_options->width ?? 12 }}">
                            <div>
                                <h4>Task list</h4>
                            </div>

                            <table class="table">
                                <tr>
                                    <th>Id</th>
                                    <th>Users id</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Amount</th>
                                    <th>Currency</th>
                                    <th>Log</th>
                                    <th>Transactions</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                </tr>
                                <tr v-for="task in tasks">
                                    <td>@{{ task.id }}</td>
                                    <td>@{{ task.users_id }}</td>
                                    <td>@{{ task.readable_type }}</td>
                                    <td v-bind:style="{color: getColorByStatus(task.status)}">
                                        @{{ task.readable_status}}
                                    </td>
                                    <td>@{{ task.data.amount }}</td>
                                    <td>@{{ toReadableCurrency(task.data.currency) }}</td>
                                    <td>
                                        <a v-bind:href="'{{ sprintf('/%s/operation-logs/', config('voyager.prefix')) }}' + task.operation_logs_id"
                                           target="_blank" v-if="task.operation_logs_id !== null">show</a>
                                        <span v-else>Not done</span>
                                    </td>
                                    <td>
                                        <a v-bind:href="'{{ route('voyager.transactions.index') }}/?key=tasks_id&filter=equals&s=' + task.id"
                                           target="_blank" v-if="task.status === 4">search</a>
                                        <span v-else>Not done</span>
                                    </td>
                                    <td>@{{ task.created_at }}</td>
                                    <td>@{{ task.updated_at }}</td>
                                </tr>
                                <tr v-if="tasks.length === 0">
                                    <td colspan="9">No data.</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('javascript')
    <script>
        new Vue({
            el: '#finance-actions',
            data: () => {
                return {
                    loading: false,
                    form: {
                        amount: null,
                        type: ''
                    },
                    errors: {},
                    updatingTasks: null,
                    tasks: [],
                    readableCurrencies: {!! json_encode(\App\Modules\Money\Models\Transaction::$currencyName)  !!}
                }
            },
            methods: {
                getColorByStatus(status) {
                    if (status === 3) {
                        return 'red';
                    } else if (status === 4) {
                        return 'green';
                    }

                    return 'gray';
                },
                moneyOperation() {
                    if (this.loading) {
                        return;
                    }

                    this.loading = true;

                    axios.post('{{ route('voyager.finance.global.money.operation') }}', this.form).then(() => {
                        this.form.amount = null;
                        this.form.type = null;
                        this.errors = {};

                        toastr.success('Task added. See status in list.', 'Money operation.');
                    }).catch((e) => {
                        if (e.response.status === 422) {
                            this.errors = e.response.data.errors;
                        } else if (e.response.status == 403) {
                            toastr.error('You are not authorized.', 'Money operation.');
                        } else {
                            toastr.error('Server error.', 'Money operation.');
                        }
                    }).finally(() => {
                        this.loading = false;
                    });
                },
                updateTasks() {
                    const request = {
                        'params': {
                            'types': [{{ implode(', ', \App\Modules\Core\Models\Task::TYPES_MONEY_GROUP) }}]
                        }
                    };

                    this.updatingTasks = setInterval(() => {
                        axios.get('{{ route('voyager.tasks.list.get') }}', request).then((r) => {
                            this.tasks = r.data;
                        }).catch((e) => {
                            if (e.response) {
                                toastr.error('Server error.', 'Update tasks data.');
                            }
                        });
                    }, 1000);
                },
                toReadableCurrency(currency) {
                    return this.readableCurrencies[currency];
                }
            },
            beforeDestroy() {
                clearInterval(this.updatingTasks)
            },
            created() {
                this.updateTasks()
            }
        });
    </script>
@endpush
