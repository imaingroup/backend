<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration removing field icon in table settings_links.
 */
class RemoveFieldIcon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_links', function (Blueprint $table) {
            $table->dropColumn('icon');
            $table->string('class', 255)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_links', function (Blueprint $table) {
            $table->text('icon');
            $table->dropColumn('class');
        });
    }
}
