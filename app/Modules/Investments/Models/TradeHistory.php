<?php

namespace App\Modules\Investments\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Investments\Models\Trade;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

final class TradeHistory extends Model
{
    protected $table = 'trade_history';

    /**
     * Relation with BelongsTo.
     *
     * @return BelongsTo
     */
    public function trade(): BelongsTo
    {
        return $this->belongsTo(Trade::class, 'trades_id');
    }
}
