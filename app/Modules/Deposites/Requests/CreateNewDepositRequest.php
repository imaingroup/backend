<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Requests;

use App\Modules\Deposites\Models\Deposit;
use App\Modules\Deposites\Repositories\DepositRepository;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This class validates create new deposit request.
 */
final class CreateNewDepositRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function (Validator $validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws Exception
     */
    public function after(Validator $validator)
    {
        $lastInvoice = $this->getDepositRepository()
            ->getLastInvoice(Auth::user()->id);

        if ($lastInvoice
            && $lastInvoice->status === Deposit::STATUS_NEW
            && ($lastInvoice->created_at)->modify(\sprintf('+ %s MINUTES', Deposit::LIMIT_CREATE_NEW_INVOICE_MINUTES)) > (new \DateTime())
        ) {
            $validator->errors()->add('global', 'Invoice opening limit');

            return;
        }

        if (!$lastInvoice) {
            $validator->errors()->add('global', 'Last invoice doesn\'t exist');

            return;
        }

        if ($lastInvoice->status !== Deposit::STATUS_NEW) {
            $validator->errors()->add('global', 'Last invoice was already closed');

            return;
        }
    }

    /**
     * Get deposit service.
     *
     * @return DepositRepository
     */
    private function getDepositRepository(): DepositRepository
    {
        return app(DepositRepository::class);
    }
}
