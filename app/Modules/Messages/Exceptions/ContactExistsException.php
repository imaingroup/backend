<?php
declare(strict_types=1);

namespace App\Modules\Messages\Exceptions;

/**
 * This exception may throws if contact already
 * exists in contact list.
 */
final class ContactExistsException extends MessageException
{

}
