<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto;

/**
 * RollbackAndResponse interface..
 */
interface RollbackAndResponse
{
    /**
     * Is rollback required.
     *
     * @return bool
     */
    public function isRollback(): bool;
}
