<?php
declare(strict_types=1);

namespace App\Modules\Investments\Exceptions;

use Exception;

/**
 * Abstract exception InvestmentException.
 */
abstract class InvestmentException extends Exception
{
}
