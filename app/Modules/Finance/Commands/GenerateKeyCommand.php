<?php
declare(strict_types=1);

namespace App\Modules\Finance\Commands;

use Illuminate\Console\Command;

/**
 * This command need for monitoring transactions.
 */
final class GenerateKeyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finance:key:generate {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finance aes key generate';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        file_put_contents(
            $this->argument('path'),
            openssl_random_pseudo_bytes(32)
        );

        return 0;
    }
}
