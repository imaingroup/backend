<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services\V2\Rate;

use App\Modules\Finance\Dto\Api\MarketShortDataApiDto;
use App\Modules\Finance\Enum\CoingeckoCoinEnum;
use App\Modules\Finance\Enum\CoingeckoNetworkEnum;
use App\Modules\Finance\Exceptions\Services\BitQuery\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use stdClass;
use Throwable;

/**
 * RateService.
 */
final class RateService
{
    private const CONNECT_TIMEOUT = 5;
    private const READ_TIMEOUT = 100;
    private const ENDPOINT = 'https://api.coingecko.com/api/v3';

    /**
     * Get coin data.
     *
     * @param CoingeckoCoinEnum $coin
     * @return MarketShortDataApiDto|null
     * @throws RequestException
     */
    public function getCoinData(CoingeckoCoinEnum $coin): ?MarketShortDataApiDto
    {
        $data = $this->request(sprintf('/coins/%s', $coin->value));

        return (isset($data->error) || !isset($data->market_data->current_price->usd) || $data->market_data->current_price->usd <= 0) ?
            null :
            new MarketShortDataApiDto(
                null,
                $coin,
                null,
                $data->image->large ?? null,
                (string)$data->market_data->current_price->usd,
            );
    }

    /**
     * Get asset martket data.
     *
     * @param CoingeckoNetworkEnum $network
     * @param string $address
     * @return MarketShortDataApiDto|null
     * @throws RequestException
     */
    public function getAssetMarketData(CoingeckoNetworkEnum $network, string $address): ?MarketShortDataApiDto
    {
        $data = $this->request(sprintf('/coins/%s/contract/%s', $network->value, $address));

        return (isset($data->error) || !isset($data->market_data->current_price->usd) || $data->market_data->current_price->usd <= 0) ?
            null :
            new MarketShortDataApiDto(
                $network,
                null,
                $address,
                $data->image->large ?? null,
                (string)$data->market_data->current_price->usd,
            );
    }

    /**
     * Execute request.
     *
     * @param string $path
     * @param array $query
     * @return stdClass|array
     * @throws RequestException
     */
    private function request(string $path, array $query = []): StdClass|array
    {
        $log = [];

        $log['code'] = null;

        try {
            $client = new Client();

            $body = [
                'allow_redirects' => false,
                'connect_timeout' => self::CONNECT_TIMEOUT,
                'read_timeout' => self::READ_TIMEOUT,
                'on_stats' => function (TransferStats $stats) use (&$log) {
                    $log['stats'] = $stats->getHandlerStats();

                    if ($stats->hasResponse()) {
                        $log['code'] = $stats->getResponse()->getStatusCode();
                        $log['response']['headers'] = $stats->getResponse()->getHeaders();
                        $log['response']['content'] = json_decode($stats->getResponse()->getBody()->__toString(), true);
                    }
                }
            ];

            $body['query'] = $query;
            $body['headers'] = [
                'Content-Type' => 'application/json',
            ];

            $log['request'] = $body;
            unset($log['request']['on_stats']);

            $request = $client->request('GET', sprintf('%s%s', self::ENDPOINT, $path), $body);

            if ($request->getStatusCode() == 200) {
                return json_decode($request->getBody()->__toString());
            } else {
                throw new RequestException('Status code not equal 200');
            }
        } catch (Throwable $e) {
            throw new RequestException($e->getMessage(), 0, $e);
        }
    }
}
