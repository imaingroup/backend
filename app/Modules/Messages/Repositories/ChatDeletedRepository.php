<?php
declare(strict_types=1);

namespace App\Modules\Messages\Repositories;

use App\Modules\Messages\Models\ChatDeleted;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Chat deleted repository.
 */
final class ChatDeletedRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param ChatDeleted $model
     */
    public function __construct(protected ChatDeleted $model)
    {
    }

    /**
     * Check deleted chat by user.
     *
     * @param int $chatId
     * @param int $userId
     * @return bool
     */
    public function checkDeleted(int $chatId, int $userId): bool
    {
        return $this->model
            ->newQuery()
            ->where('messages_chats_id', $chatId)
            ->where('users_id', $userId)
            ->exists();
    }

    /**
     * Unhidden deleted chats.
     *
     * @param int $chatId
     * @param int|null $userId
     */
    public function deleteByChat(int $chatId, int $userId = null): void
    {
        $query = $this->model
            ->newQuery()
            ->where('messages_chats_id', $chatId);

        if ($userId !== null) {
            $query->where('users_id', $userId);
        }

        $query->delete();
    }
}

