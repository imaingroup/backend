<?php
declare(strict_types=1);

namespace App\Modules\Finance\Enum;

use App\Modules\Finance\Contracts\Enum\NetworkEnumContract;
use BackedEnum;

/**
 * CoingeckoToNetworkEnum.
 */
enum CoingeckoNetworkEnum: string
{
    case ETHEREUM = 'ethereum';
    case ETC = 'ethereum-classic';
    case BSC = 'binance-smart-chain';
    case MATIC = 'polygon-pos';
    case KLAYTN = 'klay-token';

    /**
     * From ethereum network.
     *
     * @param BackedEnum&NetworkEnumContract $enum
     * @return CoingeckoNetworkEnum|null
     */
    public static function fromNetwork(NetworkEnumContract&BackedEnum $enum): ?CoingeckoNetworkEnum
    {
        return match ($enum->value) {
            EthereumNetworkEnum::ETHEREUM->value => self::ETHEREUM,
            EthereumNetworkEnum::ETH_CLASSIC->value => self::ETC,
            EthereumNetworkEnum::BSC->value => self::BSC,
            EthereumNetworkEnum::MATIC->value => self::MATIC,
            default => null,
        };
    }
}
