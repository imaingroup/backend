<?php


namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsDashboard;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Settings dashboard repository.
 */
final class SettingsDashboardRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param SettingsDashboard $model
     */
    public function __construct(protected SettingsDashboard $model)
    {}

    /**
     * Get settings model.
     *
     * @return \App\Modules\Settings\Models\SettingsDashboard
     */
    public function get(): SettingsDashboard
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
