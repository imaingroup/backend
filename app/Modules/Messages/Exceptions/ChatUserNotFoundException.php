<?php
declare(strict_types=1);

namespace App\Modules\Messages\Exceptions;

/**
 * Chat user not found exception.
 */
final class ChatUserNotFoundException extends MessageException
{

}
