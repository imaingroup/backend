<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * RefactoringCryptoAddressesWithCryptoCurrencies.
 */
final class RefactoringCryptoAddressesWithCryptoCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->unsignedBigInteger('crypto_currencies_id')
                ->after('id')
                ->nullable(true);
            $table->foreign('crypto_currencies_id', 'crypto_addresses_crypto_currencies_id_foreign')
                ->references('id')
                ->on('crypto_currencies')
                ->restrictOnDelete();
            $table->unsignedSmallInteger('type')
                ->nullable(true)
                ->change();
            $table->text('access_key')
                ->nullable(true)
                ->after('eth_password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->dropForeign('crypto_addresses_crypto_currencies_id_foreign');
            $table->dropColumn('crypto_currencies_id');
            $table->unsignedSmallInteger('type')
                ->nullable(false)
                ->change();
            $table->dropColumn('access_key');
        });
    }
}
