<?php
declare(strict_types=1);

namespace App\Modules\External\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\External\Requests\Imperium\ReportRequest;
use App\Modules\External\Services\ImperiumService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

/**
 * Imperium controller.
 */
final class ImperiumController extends Controller
{
    /**
     * Download win.
     *
     * @return RedirectResponse
     */
    public function downloadWin(): RedirectResponse
    {
        return redirect($this->getImperiumService()
            ->getWalletDownloadWinUrl());
    }

    /**
     * Download mac.
     *
     * @return RedirectResponse
     */
    public function downloadMac(): RedirectResponse
    {
        return redirect($this->getImperiumService()
            ->getWalletDownloadMacUrl());
    }

    /**
     * Version.
     *
     * @return Response
     */
    public function version(): Response
    {
        return response($this->getImperiumService()
            ->getWalletVersion());
    }

    /**
     * Get rates.
     *
     * @return Response
     */
    public function rates(): Response
    {
        return response($this->getImperiumService()
            ->getRates());
    }

    /**
     * Send report about a bug.
     *
     * @param ReportRequest $request
     * @return ResponseDto
     */
    public function report(ReportRequest $request): ResponseDto
    {
        $this->getImperiumService()
            ->sendReport(...$request->validated());

        return new ResponseDto();
    }

    /**
     * Get {@see ImperiumService::class}.
     *
     * @return ImperiumService
     */
    private function getImperiumService(): ImperiumService
    {
        return app(ImperiumService::class);
    }
}
