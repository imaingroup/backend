<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions;

use Exception;

/**
 * TaskPartialNormalException.
 */
final class TaskPartialNormalException extends Exception
{
}
