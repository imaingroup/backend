<?php

namespace App\Modules\Notifications\Mails;

use App\Modules\Notifications\Mails\AbstractEmail;
use App\Modules\Notifications\Models\Mail;

/**
 * This mail sends message in case if was executed method
 * SystemService::blockDepositModule.
 */
final class DepositBlockAdminEmail extends AbstractEmail
{
    /**
     * {@inheritdoc}
     */
    protected function getVariables(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    protected function getMailId(): int
    {
        return Mail::MAIL_BLOCKING_DEPOSIT;
    }
}
