<?php


namespace App\Modules\Languages\Repositories;

use App\Modules\Languages\Models\Lang;
use App\Modules\Core\Repositories\BaseRepository;

final class LangRepository extends BaseRepository
{
    /**
     * @param Lang $model
     */
    public function __construct(protected Lang $model)
    {}

    public function findByShortName(string $lang)
    {
        return $this->model->newQuery()
            ->where('short_name', $lang)
            ->first();
    }
}
