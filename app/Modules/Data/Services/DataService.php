<?php
declare(strict_types=1);

namespace App\Modules\Data\Services;

use App\Modules\Data\Dto\Params\DataParamsDto;
use App\Modules\Data\Dto\Params\DataRouteParamsDto;
use App\Modules\Data\Dto\Services\DispatchedDto;
use App\Modules\Data\Dto\Services\ResponseDto;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Cache;
use JsonSerializable;
use Throwable;
use function sprintf;

/**
 * Data service.
 * This service is proxy for access to data methods in other services.
 * It need for fast auto detect all changes.
 */
final class DataService
{
    /**
     * Dispatch request.
     *
     * @param DataParamsDto $params
     * @return DispatchedDto
     * @throws Throwable
     */
    public function dispatch(DataParamsDto $params): DispatchedDto
    {
        $responses = collect();
        $rollback = false;
        $is403 = false;
        $is422 = false;

        foreach ($params->methods as $routeParams) {
            $response = $this->dispatchRequest($routeParams);
            $oldData = $this->getOld($routeParams, $params);
            $expired = false;

            if (!$this->compareData($response->response, $oldData)) {
                $this->register($response, $params);
                $this->incrementVersion($routeParams, $params);

                $expired = true;
            }

            if (!($response->status >= 200 && $response->status < 300)) {
                $rollback = true;
            }

            if($response->status === 403) {
                $is403 = true;
            }
            else if($response->status === 422) {
                $is422 = true;
            }

            $version = $this->getOrInitVersion($routeParams, $params);

            if ($expired || $params->always || $routeParams->version < $version) {
                $response->changeVersion($version);
                $responses->add($response);
            }
        }

        return new DispatchedDto($responses, $rollback, $is403, $is422);
    }

    /**
     * Dispatch request.
     *
     * @param DataRouteParamsDto $routeParams
     * @return ResponseDto
     * @throws Throwable
     */
    private function dispatchRequest(DataRouteParamsDto $routeParams): ResponseDto
    {
        $original = request()->all();

        $request = $routeParams->requests->toArray();
        $req = Request::create(
            route($routeParams->route, $routeParams->parameters->toArray()),
            $routeParams->action->httpMethod,
            $request,
            [],
            [],
            [
                'HTTP_ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($request)
        );

        request()->replace($request);

        try {
            $result = $this->getRouter()
                ->dispatch($req);
        }
        catch (Throwable $e) {
            throw $e;
        } finally {
            request()->replace($original);
        }

        return new ResponseDto(
            $routeParams,
            $result->getStatusCode(),
            $result->original
        );
    }

    /**
     * Get or init version incremental counter.
     *
     * @param DataRouteParamsDto $routeParams
     * @param DataParamsDto $params
     * @return int
     */
    private function getOrInitVersion(DataRouteParamsDto $routeParams, DataParamsDto $params): int
    {
        $key = $this->getVersionKey($routeParams, $params);

        if (!Cache::has($key)) {
            Cache::increment($key);
        }

        return (int)Cache::get($key);
    }

    /**
     * Get or init version incremental counter.
     *
     * @param DataRouteParamsDto $routeParams
     * @param DataParamsDto $params
     * @return int
     */
    private function incrementVersion(DataRouteParamsDto $routeParams, DataParamsDto $params): int
    {
        return (int)Cache::increment($this->getVersionKey($routeParams, $params));
    }

    /**
     * Get version counter key.
     *
     * @param DataRouteParamsDto $routeParams
     * @param DataParamsDto $params
     * @return string
     */
    private function getVersionKey(DataRouteParamsDto $routeParams, DataParamsDto $params): string
    {
        return sprintf(
            '%s_counter',
            $this->getCacheKey($routeParams, $params)
        );
    }

    /**
     * Get old data.
     *
     * @param DataRouteParamsDto $routeParams
     * @param DataParamsDto $params
     * @return array|JsonSerializable|null
     */
    private function getOld(DataRouteParamsDto $routeParams, DataParamsDto $params): array|JsonSerializable|null
    {
        return Cache::get($this->getCacheKey($routeParams, $params));
    }

    /**
     * Compare data.
     *
     * @param array|JsonSerializable|null $oldData
     * @param array|JsonSerializable|null $newData
     * @return bool
     */
    private function compareData(array|JsonSerializable|null $newData, array|JsonSerializable|null $oldData): bool
    {
        return crc32(json_encode($newData)) ===
            crc32(json_encode($oldData));
    }

    /**
     * Register event. It need for detecting event of update data.
     *
     * @param ResponseDto $response
     * @param DataParamsDto $params
     */
    private function register(ResponseDto $response, DataParamsDto $params): void
    {
        Cache::put(
            $this->getCacheKey($response->params, $params),
            $response->response
        );
    }

    /**
     * Get cache key.
     *
     * @param DataRouteParamsDto $routeParams
     * @param DataParamsDto $params
     * @return string
     */
    private function getCacheKey(DataRouteParamsDto $routeParams, DataParamsDto $params): string
    {
        return sprintf(
            '_api_data_access_%s_%s_%s',
            $params->user->id,
            $routeParams->route,
            crc32(json_encode([$routeParams->parameters, $routeParams->requests]))
        );
    }

    /**
     * Get Router.
     *
     * @return Router
     */
    private function getRouter(): Router
    {
        return app('router');
    }
}
