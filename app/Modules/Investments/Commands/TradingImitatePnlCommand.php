<?php
declare(strict_types=1);

namespace App\Modules\Investments\Commands;

use App\Modules\Logs\Loggers\Logger;
use App\Modules\Investments\Services\InvestmentService;
use Illuminate\Console\Command;
use Throwable;

/**
 * This command imitations pnl changes.
 */
final class TradingImitatePnlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading:imitate-pnl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command making trading process';

    /** @var InvestmentService Investment service. */
    private InvestmentService $investmentService;

    /** @var Logger Logger. */
    private Logger $logger;

    /**
     * Create a new command instance.
     *
     * @param InvestmentService $investmentService
     * @param Logger $logger
     */
    public function __construct(InvestmentService $investmentService, Logger $logger)
    {
        parent::__construct();

        $this->investmentService = $investmentService;
        $this->logger = $logger;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        try {
            $this->investmentService->imitationPnlProcess();

            return 0;
        } catch (Throwable $e) {
            $this->logger->registerException($e);

            return -1;
        }
    }
}
