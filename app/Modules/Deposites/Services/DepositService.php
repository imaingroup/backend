<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Services;

use App\Facades\DB;
use App\Modules\Actions\Models\UserAction;
use App\Modules\Actions\Services\UserActionService;
use App\Modules\Core\Exceptions\TransactionAlreadyStartedException;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Deposites\Dto\Api\DepositDataApiDto;
use App\Modules\Deposites\Dto\Api\DepositInvoiceApiDto;
use App\Modules\Deposites\Dto\GetQrParams;
use App\Modules\Deposites\Exceptions\DepositServiceException;
use App\Modules\Deposites\Models\Deposit;
use App\Modules\Deposites\Models\DepositTransaction;
use App\Modules\Deposites\Repositories\DepositRepository;
use App\Modules\Deposites\Repositories\DepositTransactionRepository;
use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Enum\NetworkTypeEnum;
use App\Modules\Finance\Exceptions\Btc\CreateAddressException;
use App\Modules\Finance\Exceptions\Btc\SetLabelException;
use App\Modules\Finance\Exceptions\SecureCryptRequestException;
use App\Modules\Finance\Exceptions\SecureCryptRequestParamsException;
use App\Modules\Finance\Exceptions\Services\V2\CryptoData\UnsupportedNetworkException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\RequestException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\ValidationException;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Models\CryptoTransaction;
use App\Modules\Finance\Services\FinanceService;
use App\Modules\Finance\Services\V2\CryptoDataService;
use App\Modules\Money\Exceptions\MoneyTransferException;
use App\Modules\Money\Models\Transaction;
use App\Modules\Money\Services\MoneyTransferService;
use App\Modules\Notifications\Jobs\DepositReplenishedEmailJob;
use App\Modules\PromoCodes\Exceptions\PromoCodeException;
use App\Modules\PromoCodes\Services\PromoCodeService;
use App\Modules\Settings\Models\DepositCurrency;
use App\Modules\Settings\Repositories\SettingsDepositRepository;
use App\Modules\Settings\Services\CurrenciesService;
use App\Modules\Users\Models\User;
use DateTime;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Throwable;

/**
 * Deposit service.
 */
final class DepositService
{
    /**
     * Make deposit.
     *
     * @param User $user
     * @param float $amount
     * @param DepositCurrency $currency
     * @return Deposit
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws SetLabelException
     * @throws CreateAddressException
     * @throws UnsupportedNetworkException
     * @throws RequestException
     * @throws ValidationException
     */
    public function make(
        User            $user,
        float           $amount,
        DepositCurrency $currency
    ): Deposit
    {
        $rate = $currency->currency->market->price;
        $amountInCoins = $amount / $rate;

        $deposit = new Deposit();
        $deposit->user()
            ->associate($user);
        $deposit->currency()
            ->associate($currency->currency);
        $deposit->amount_usdt = $amount;
        $deposit->amount_coin = $amountInCoins;
        $deposit->rate = $rate;
        $deposit->address = $this->getCryptoDataService()
            ->createAddress($currency->currency, $currency->confirmations_min, $user)
            ->address;
        $deposit->status = Deposit::STATUS_NEW;

        $deposit->save();

        return $deposit;
    }

    /**
     * Get deposits data by user.
     *
     * @param User $user
     * @return DepositDataApiDto
     * @throws Exception
     */
    public function getData(User $user): DepositDataApiDto
    {
        $settingsDeposit = $this->getSettingsDepositRepository()
            ->get();

        $lastInvoice = $this->getDepositRepository()
            ->getLastInvoice($user->id);
        $activeDeposits = true;
        $activeNewDeposit = true;

        if ($lastInvoice && $lastInvoice->status === Deposit::STATUS_NEW) {
            $activeDeposits = !($lastInvoice->created_at->modify(sprintf('+ %s MINUTES', Deposit::LIMIT_INVOICE_MINUTES)) > (new DateTime()));
            $activeNewDeposit = !($lastInvoice->created_at->modify(sprintf('+ %s MINUTES', Deposit::LIMIT_CREATE_NEW_INVOICE_MINUTES)) > (new DateTime()));
        }

        return new DepositDataApiDto(
            $this->getCurrenciesService()
                ->getCurrencies()
                ->map(fn(DepositCurrency $currency) => $currency->toApiDto()),
            !$settingsDeposit->is_deposit_block,
            $activeDeposits,
            $activeNewDeposit,
            !$activeDeposits ? $this->getInvoiceData($lastInvoice) : null
        );
    }

    /**
     * Get invoice data.
     *
     * @param Deposit $invoice
     * @return DepositInvoiceApiDto
     */
    public function getInvoiceData(Deposit $invoice): DepositInvoiceApiDto
    {
        return new DepositInvoiceApiDto(
            $invoice->address,
            $invoice->amount_coin,
            Deposit::LIMIT_INVOICE_MINUTES,
            Deposit::LIMIT_INVOICE_MINUTES - Deposit::LIMIT_CREATE_NEW_INVOICE_MINUTES,
            (new DateTime())->format('Y-m-d H:i:s'),
            $invoice->created_at->format('Y-m-d H:i:s'),
            in_array($invoice->currency->network->toEnum(), [EthereumNetworkEnum::ETHEREUM, BitcoinNetworkEnum::BITCOIN]) ?
                $this->getQrCodeService()
                    ->createQrCode(
                        new GetQrParams(
                            200,
                            200,
                            $invoice->address,
                            $invoice->amount_coin,
                        )
                    ) : null,
            $invoice->currency->symbol,
            $invoice->currency->id,
        );
    }

    /**
     * Close last invoice.
     *
     * @param User $user
     * @throws DepositServiceException
     */
    public function closeLastInvoice(User $user)
    {
        $lastInvoice = $this->getDepositRepository()
            ->getLastInvoice($user->id);

        if (!$lastInvoice) {
            throw new DepositServiceException('Last invoice doesn\'t exist');
        }

        if ($lastInvoice->status !== Deposit::STATUS_NEW) {
            throw new DepositServiceException('Last invoice already was closed');
        }

        $lastInvoice->status = Deposit::STATUS_CLOSED;
        $lastInvoice->save();
    }

    /**
     * Get transactions history by user.
     *
     * @param User $user
     * @return Collection
     */
    public function history(User $user): Collection
    {
        return $this->getDepositTransactionRepository()
            ->getByUser($user)
            ->map(fn (DepositTransaction $transaction) => $transaction->toPublicApiDto());
    }

    /**
     * Monitoring method.
     * For realise multithreading need
     * optimize this method for it.
     *
     * @throws DepositServiceException
     * @throws MoneyTransferException
     * @throws PromoCodeException
     * @throws Throwable
     * @throws TransactionAlreadyStartedException
     * @throws TransactionNotStarted
     */
    public function monitoring()
    {
        if (DB::transactionLevel() !== 0) {
            throw new TransactionAlreadyStartedException();
        }

        /** @var Deposit $deposit */
        foreach ($this->getDepositRepository()
                     ->getObserved() as $deposit) {
            try {
                DB::beginTransaction();

                $deposit = $this->getDepositRepository()
                    ->getAndLockForUpdate($deposit->id);
                $foundNewTransactions = $this->updateTransactions($deposit);
                $countUnconfirmed = $deposit->depositTransactions
                    ->where('status', DepositTransaction::UNCONFIRMED)
                    ->count();

                if (($deposit->created_at)->modify(sprintf('+ %s MINUTES', Deposit::OBSERVATION_TIME_MINUTES)) < (new DateTime())
                    && $countUnconfirmed === 0
                    && !$foundNewTransactions
                ) {
                    $deposit->status = Deposit::STATUS_FINISHED;
                    $deposit->save();
                }

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();

                throw $e;
            }
        }
    }

    /**
     * Add transaction.
     *
     * @param Deposit $deposit
     * @param float $amount
     * @param DateTime $transactionAt
     * @param int $status
     * @param string $txid
     * @return DepositTransaction
     * @throws DepositServiceException
     * @throws MoneyTransferException
     * @throws PromoCodeException
     * @throws TransactionNotStarted
     */
    public function addTransaction(Deposit $deposit, float $amount, DateTime $transactionAt, int $status, string $txid): DepositTransaction
    {
        $depositTransaction = new DepositTransaction();
        $depositTransaction->deposit()
            ->associate($deposit);
        $depositTransaction->transaction_at = $transactionAt;
        $depositTransaction->amount_coin = $amount;
        $depositTransaction->amount_usdt = $amount * $deposit->rate;
        $depositTransaction->status = $status;
        $depositTransaction->txid = $txid;

        $depositTransaction->save();

        if ($depositTransaction->status === DepositTransaction::CONFIRMED) {
            $this->processTransaction($depositTransaction);
        }

        return $depositTransaction;
    }

    /**
     * Update transactions.
     *
     * @param Deposit $deposit
     * @return bool
     * @throws DepositServiceException
     * @throws MoneyTransferException
     * @throws PromoCodeException
     * @throws TransactionNotStarted
     */
    private function updateTransactions(Deposit $deposit): bool
    {
        $foundNewTransactions = false;
        $address = $this->getFinanceService()
            ->getCryptoAddress($deposit->address);

        if (!$address) {
            return false;
        }

        $address->cryptoTransactions
            ->filter(fn(CryptoTransaction $transaction) => $transaction->currency->id === $deposit->currency->id)
            ->filter(fn(CryptoTransaction $transaction) => $transaction->amount > 0)
            ->each(function (CryptoTransaction $transaction) use ($deposit, &$foundNewTransactions) {
                $existentTransaction = $deposit->findTransaction($transaction->txid);

                if ($existentTransaction) {
                    DepositTransaction::refreshAndLockForUpdate($existentTransaction);

                    if ($existentTransaction->status !== DepositTransaction::CONFIRMED
                        && $transaction->confirmations >= Deposit::MINCONF
                    ) {
                        $existentTransaction->status = DepositTransaction::CONFIRMED;
                        $existentTransaction->save();

                        $this->processTransaction($existentTransaction);
                    }
                } else {
                    $depositTransaction = $this->addTransaction(
                        $deposit,
                        $transaction->amount,
                        $transaction->created_at,
                        $transaction->confirmations >= Deposit::MINCONF
                            ? DepositTransaction::CONFIRMED : DepositTransaction::UNCONFIRMED,
                        $transaction->txid
                    );

                    if ($depositTransaction->status === DepositTransaction::UNCONFIRMED) {
                        $foundNewTransactions = true;
                    }
                }
            });

        return $foundNewTransactions;
    }

    /**
     * Process transaction. This method must be executed for confirmed transaction
     * for execute money operation.
     *
     * @param DepositTransaction $depositTransaction
     * @throws DepositServiceException
     * @throws MoneyTransferException
     * @throws PromoCodeException
     * @throws TransactionNotStarted
     */
    private function processTransaction(DepositTransaction $depositTransaction)
    {
        DepositTransaction::refreshAndLockForUpdate($depositTransaction);

        if ($depositTransaction->status !== DepositTransaction::CONFIRMED) {
            throw new DepositServiceException('This transaction till unconfirmed');
        }

        if ($depositTransaction->related_transactions_id) {
            throw new DepositServiceException('This transaction already processed');
        }

        $user = $depositTransaction->deposit->user;

        $transaction = $this->getMoneyTransferService()
            ->moneyOperation(
                $user,
                Auth::check() ? Auth::user() : $user,
                $depositTransaction->amount_usdt,
                Transaction::CURRENCY_USDT_DEPOSITED,
                Transaction::TYPE_DEPOSIT
            );

        $depositTransaction->related_transactions_id = $transaction->id;

        $depositTransaction->save();
        $this->getPromoCodeService()
            ->addCommissionAfterDeposit($depositTransaction);
        $this->getUserActionService()
            ->register(
                $user,
                UserAction::ACTION_DEPOSIT,
                [
                    'deposits_transactions' => $depositTransaction->id
                ]
            );

        if ($user->is_enabled_deposit_notifications) {
            DepositReplenishedEmailJob::dispatch($depositTransaction);
        }
    }

    /**
     * Get QrCodeService.
     *
     * @return QrCodeService
     */
    private function getQrCodeService(): QrCodeService
    {
        return app(QrCodeService::class);
    }

    /**
     * Get FinanceService.
     *
     * @return FinanceService
     */
    private function getFinanceService(): FinanceService
    {
        return app(FinanceService::class);
    }

    /**
     * Get {@see CurrenciesService::class}.
     *
     * @return CurrenciesService
     */
    private function getCurrenciesService(): CurrenciesService
    {
        return app(CurrenciesService::class);
    }

    /**
     * Get {@see CryptoDataService::class}.
     *
     * @return CryptoDataService
     */
    private function getCryptoDataService(): CryptoDataService
    {
        return app(CryptoDataService::class);
    }

    /**
     * {@see SettingsDepositRepository::class}.
     *
     * @return SettingsDepositRepository
     */
    private function getSettingsDepositRepository(): SettingsDepositRepository
    {
        return app(SettingsDepositRepository::class);
    }

    /**
     * {@see DepositRepository::class}.
     *
     * @return DepositRepository
     */
    private function getDepositRepository(): DepositRepository
    {
        return app(DepositRepository::class);
    }

    /**
     * {@see MoneyTransferService::class}.
     *
     * @return MoneyTransferService
     */
    private function getMoneyTransferService(): MoneyTransferService
    {
        return app(MoneyTransferService::class);
    }

    /**
     * {@see DepositTransactionRepository::class}.
     *
     * @return DepositTransactionRepository
     */
    private function getDepositTransactionRepository(): DepositTransactionRepository
    {
        return app(DepositTransactionRepository::class);
    }

    /**
     * {@see UserActionService::class}.
     *
     * @return UserActionService
     */
    private function getUserActionService(): UserActionService
    {
        return app(UserActionService::class);
    }

    /**
     * {@see PromoCodeService::class}.
     *
     * @return PromoCodeService
     */
    private function getPromoCodeService(): PromoCodeService
    {
        return app(PromoCodeService::class);
    }
}
