<?php

namespace App\Modules\Access\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Access\Requests\LoginRequest;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Access\Services\LoginService;
use Illuminate\Http\Response;
use Throwable;

/**
 * Login controller.
 */
final class LoginController extends Controller
{
    /** @var LoginService Login service. */
    private LoginService $loginService;

    /** @var Logger Logger service. */
    private Logger $logger;

    /**
     * Constructor.
     *
     * @param LoginService $loginService
     * @param Logger $logger
     */
    public function __construct(LoginService $loginService, Logger $logger)
    {
        $this->loginService = $loginService;
        $this->logger = $logger;
    }

    /**
     * Login action.
     *
     * @param LoginRequest $request
     * @return Response
     */
    public function login(LoginRequest $request): Response
    {
        $data = $request->validated();

        try {
            $this->loginService->login($data['email'], $data['password'], $data['is_remember_me']);

            return response(['status' => true], 200);
        } catch (Throwable $e) {
            $this->logger->registerException($e, $data);

            return response(['status' => false], 503);
        }
    }
}
