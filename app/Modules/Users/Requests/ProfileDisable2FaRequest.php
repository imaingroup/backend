<?php

namespace App\Modules\Users\Requests;

use App\Modules\Users\Services\UserService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;

/**
 * This class need for validating data profile disable 2fa request.
 */
final class ProfileDisable2FaRequest extends FormRequest
{
    /** @var UserService User service. */
    private UserService $userService;

    /**
     * Constructor.
     *
     * @param \App\Modules\Users\Services\UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'password' => 'required|scalar',
            '2fa_code' => 'required|scalar'
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws SecretKeyTooShortException
     */
    public function after(Validator $validator): void
    {
        if (is_scalar($this['password']) && !Hash::check($this['password'], Auth::user()->password)) {
            $validator->errors()->add('password', 'Incorrect password');
            return;
        }

        if (!Auth::user()->is_2fa) {
            $validator->errors()->add('2fa_code', 'Please activate 2FA');
            return;
        }

        if(is_scalar($this['2fa_code'])) {
            if (!$this->userService->check2fa(Auth::user(), (string)$this['2fa_code'])) {
                $validator->errors()->add('2fa_code', 'Incorrect 2FA');
                return;
            }
        }
    }
}
