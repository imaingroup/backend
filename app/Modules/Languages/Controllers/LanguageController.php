<?php

namespace App\Modules\Languages\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Languages\Requests\LanguageSetRequest;
use App\Modules\Languages\Services\LanguageService;

/**
 * Language controller need for set/get current language.
 */
final class LanguageController extends Controller
{
    /** @var LanguageService Language service. */
    private LanguageService $languageService;

    /**
     * Constructor.
     *
     * @param LanguageService $languageService
     */
    public function __construct(LanguageService $languageService)
    {
        $this->languageService = $languageService;
    }


    /**
     * Set language.
     *
     * @param LanguageSetRequest $request
     * @return array
     */
    public function set(LanguageSetRequest $request): array
    {
        $this->languageService->setLocale($request->validated()['lang']);

        return ['status' => true];
    }
}
