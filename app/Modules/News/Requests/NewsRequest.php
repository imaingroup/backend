<?php

namespace App\Modules\News\Requests;

use App\Modules\News\Repositories\NewsCategoryRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * This request class validating view news request.
 */
final class NewsRequest extends FormRequest
{
    /** @var \App\Modules\News\Repositories\NewsCategoryRepository New category repository. */
    private NewsCategoryRepository  $newsCategoryRepository;

    /**
     * Constructor.
     *
     * @param NewsCategoryRepository $newsCategoryRepository
     */
    public function __construct(NewsCategoryRepository $newsCategoryRepository)
    {
        $this->newsCategoryRepository = $newsCategoryRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'sort' => [
                'required',
                'string',
                Rule::in(['ASC', 'DESC'])
            ],
            'category' => [
                'nullable',
                'integer',
                Rule::in($this->newsCategoryRepository->getAllActive()->pluck('sid')->toArray())
            ],
            'search' => 'nullable|scalar',
            'page' => 'nullable|integer|gt:0'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function validated(): array
    {
        $data = parent::validated();

        $data['category'] = isset($data['category']) ? (int)$data['category'] : null;
        $data['search'] = isset($data['search']) ? (string)$data['search'] : null;
        $data['page'] = isset($data['page']) ? (int)$data['page'] : 1;

        return $data;
    }
}
