<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Users;

use App\Modules\Users\Repositories\UserRepository;
use Illuminate\Foundation\Http\FormRequest;

final class AdminChangePasswordRequest extends FormRequest
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|scalar|min:6|confirmed',
            'password_confirmation' => 'required|scalar'
        ];
    }
}
