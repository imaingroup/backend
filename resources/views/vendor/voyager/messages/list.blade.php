@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Messages monitoring')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-folder"></i> Messages monitoring
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid" id="messages-admin">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="form-group col-md-{{ $display_options->width ?? 12 }}" v-if="!selectedChat">
                            <div>
                                <h4>Chat list</h4>
                            </div>

                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Users</th>
                                    <th>Messages all/new</th>
                                    <th>Last message at</th>
                                    <th>Created at</th>
                                </tr>
                                <tr v-for="chat in chats">
                                    <td>
                                        <a href="" v-on:click.prevent="viewMessages(chat)">@{{ chat.name }}</a>
                                        <a v-bind:href="chat.avatar" v-if="chat.avatar" target="_blank">view avatar</a>
                                    </td>
                                    <td>@{{ chat.is_group ? 'group' : 'single' }}</td>
                                    <td>@{{ chat.users.length }}</td>
                                    <td>@{{ chat.count }}/@{{ chat.count_new }}</td>
                                    <td>@{{ chat.last_message_at }}</td>
                                    <td>@{{ chat.created_at }}</td>
                                </tr>
                                <tr v-if="chats.length === 0">
                                    <td colspan="6">No data.</td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group col-md-{{ $display_options->width ?? 12 }}" v-if="selectedChat">
                            <div>
                                <h4>@{{ selectedChat.name }}</h4>
                                <a href="" v-on:click.prevent="selectedChat = null">To chat list</a>
                            </div>

                            <table class="table">
                                <tr>
                                    <th>Message</th>
                                    <th>Author</th>
                                    <th>Created at</th>
                                </tr>
                                <tr v-for="message in messages">
                                    <td>@{{ message.message }}</td>
                                    <td>@{{ message.author.username }}</td>
                                    <td>@{{ message.created_at }}</td>
                                </tr>
                                <tr v-if="messages.length === 0">
                                    <td colspan="3">No data.</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('javascript')
    <script>
        new Vue({
            el: '#messages-admin',
            data: () => {
                return {
                    selectedChat: null,
                    chats: [],
                    messages: [],
                    updatingMessages: null,
                }
            },
            methods: {
                updateChats() {
                    this.loadChats();

                    this.updatingMessages = setInterval(() => {
                        this.loadChats();
                    }, 5000);
                },
                loadChats() {
                    axios.get('{{ route('voyager.messages.chats.get') }}').then((r) => {
                        this.chats = r.data.data.chats;
                    }).catch((e) => {
                        if (e.response) {
                            toastr.error('Server error.', 'Update chats.');
                        }
                    });
                },
                viewMessages(chat) {
                    this.selectedChat = chat;

                    axios.get('/XI400PGNB/messages/' + chat.sid + '/get').then((r) => {
                        this.messages = r.data.data.messages;
                    }).catch(() => {
                        toastr.error('Server error.', 'Update messages.');
                    });
                },
            },
            beforeDestroy() {
                clearInterval(this.updatingMessages);
            },
            created() {
                this.updateChats();
            }
        });
    </script>
@endpush
