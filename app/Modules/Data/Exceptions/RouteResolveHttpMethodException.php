<?php
declare(strict_types=1);

namespace App\Modules\Data\Exceptions;

/**
 * RouteResolveHttpMethodException.
 */
final class RouteResolveHttpMethodException extends DataException
{
}
