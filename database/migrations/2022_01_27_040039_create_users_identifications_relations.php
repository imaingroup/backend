<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * CreateUsersIdentificationsRelations.
 */
final class CreateUsersIdentificationsRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('users_identifications_relations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_identifications_id');
            $table->foreign('users_identifications_id', 'users_identifications_relations_users_identifications_id_foreign')
                ->references('id')
                ->on('users_identifications')
                ->cascadeOnDelete();
            $table->unsignedBigInteger('users_id');
            $table->foreign('users_id', 'users_identifications_relations_users_id_foreign')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();
            $table->boolean('is_accurate');
            $table->timestamps();
            $table->unique(['users_id', 'users_identifications_id'], 'users_identifications_relations_relations_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('users_identifications_relations');
    }
}
