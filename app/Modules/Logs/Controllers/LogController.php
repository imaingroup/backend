<?php
declare(strict_types=1);

namespace App\Modules\Logs\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Logs\Requests\SendLogRequest;
use App\Modules\Logs\Loggers\Logger;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Log controller.
 */
final class LogController extends Controller
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Logs\Loggers\Logger $logger
     */
    public function __construct(private Logger $logger)
    {
    }

    /**
     * Get team data.
     *
     * @param SendLogRequest $request
     * @return array
     */
    #[ArrayShape(['status' => "bool"])]
    public function sendLog(SendLogRequest $request): array
    {
        $this->logger->jsError($request->validated()['log']);

        return ['status' => true];
    }
}
