<?php
declare(strict_types=1);

namespace App\Modules\Data\Dto\Services;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Data\Dto\Params\DataRouteParamsDto;
use JsonSerializable;

/**
 * ResponseDto.
 *
 * @property-read DataRouteParamsDto $params
 * @property-read int $status
 * @property-read array|JsonSerializable|null $response
 * @property-read int $version
 */
final class ResponseDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param DataRouteParamsDto $params
     * @param int $status
     * @param array|JsonSerializable|null $response
     * @param int $version
     */
    public function __construct(
        protected DataRouteParamsDto $params,
        protected int $status,
        protected array|JsonSerializable|null $response,
        protected int $version = 1,
    )
    {
    }

    /**
     * Change version.
     *
     * @param int $version
     */
    public function changeVersion(int $version): void
    {
        $this->version = $version;
    }
}
