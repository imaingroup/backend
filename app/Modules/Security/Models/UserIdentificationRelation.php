<?php
declare(strict_types=1);

namespace App\Modules\Security\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Users\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * UserIdentificationRelation.
 *
 * @property-read int $id
 * @property int $users_identifications_id
 * @property int $users_id
 * @property boolean $is_accurate
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read UserIdentification $userIdentification
 * @property-read User $user
 */
final class UserIdentificationRelation extends Model
{
    /** {@inheritdoc} */
    protected $table = 'users_identifications_relations';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'users_identifications_id' => 'integer',
        'users_id' => 'integer',
        'is_accurate' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get relation with {@see UserIdentification::class}.
     *
     * @return BelongsTo
     */
    public function userIdentification(): BelongsTo
    {
        return $this->belongsTo(UserIdentification::class, 'users_identifications_id');
    }

    /**
     * Get relation with {@see User::class}.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }
}
