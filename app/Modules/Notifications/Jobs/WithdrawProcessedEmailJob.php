<?php

namespace App\Modules\Notifications\Jobs;

use App\Modules\Notifications\Mails\MailSender;
use App\Modules\Notifications\Mails\WithdrawProcessedEmail;
use App\Modules\Withdraw\Models\Withdraw;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Withdraw processed mail job.
 */
final class WithdrawProcessedEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Modules\Withdraw\Models\Withdraw Withdraw. */
    private Withdraw $withdraw;

    /**
     * Create a new mail instance.
     *
     * @param \App\Modules\Withdraw\Models\Withdraw $withdraw
     */
    public function __construct(Withdraw $withdraw)
    {
        $this->withdraw = $withdraw;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MailSender::send($this->withdraw->user->email, new WithdrawProcessedEmail($this->withdraw));
    }
}
