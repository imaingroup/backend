<?php
declare(strict_types=1);

namespace App\Modules\Core\Validation;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Validator;

/**
 * MaxWords.
 */
final class MaxWords implements Rule
{
    public const NAME = 'max_words';
    private int $maxWords;

    /**
     * Constructor.
     *
     * @param int $maxWords
     */
    public function __construct(int $maxWords = 500)
    {
        $this->maxWords = $maxWords;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, mixed $value): bool
    {
        return str_word_count($value) <= $this->maxWords;
    }

    /**
     * Validate.
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     * @param Validator $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        if (isset($parameters[0]) && is_numeric($parameters[0])) {
            $this->maxWords = (int)$parameters[0];
        }

        return $this->passes($attribute, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'The :attribute cannot be longer than ' . $this->maxWords . ' words.';
    }
}
