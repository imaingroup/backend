<?php
declare(strict_types=1);

namespace App\Modules\Core\Services;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\Model\City;
use MaxMind\Db\Reader\InvalidDatabaseException;

/**
 * Geo ip service.
 */
final class GeoIpService
{
    private const PATH_DB = 'database/GeoLite2-City.mmdb';

    /**
     * Get by ip.
     *
     * @param string $ip
     * @return City
     * @throws AddressNotFoundException
     * @throws InvalidDatabaseException
     */
    public function getByIp(string $ip): City
    {
        $reader = new Reader(base_path(self::PATH_DB));

        return $reader->city($ip);
    }
}
