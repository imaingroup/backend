<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * This migration adds role moderator.
 */
final class AddRoleModerator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('roles')
            ->insert([
                'name' => 'moderator',
                'display_name' => 'Moderator',
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('roles')
            ->where('name', 'moderator')
            ->delete();
    }
}
