@foreach($translate as $translateKey => $translateValue)
    @if(isset($translateValue['original_value']) && isset($translateValue['value']) && count($translateValue) == 2)
        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
            <label class="control-label" for="name"><span style="color:black">{{ $translateKey }}</span> (original: "{{ $translateValue['original_value'] }}")</label><br/>

            @if(mb_strlen($translateValue['value']) > 100)
                <textarea rows="4" class="form-control {{ empty($translateValue['value']) ? 'error' : '' }}" name="{{$name_prefix}}[{{ $translateKey }}]">{{ $translateValue['value'] }}</textarea>
            @else
                <input type="text" class="form-control {{ empty($translateValue['value']) ? 'error' : '' }}" name="{{$name_prefix}}[{{ $translateKey }}]" value="{{ $translateValue['value'] }}" placeholder="{{ $translateKey }}"/>
            @endif
        </div>
    @else
        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
            <label class="control-label" for="name">{{ $translateKey }}</label><br/>
            @include('voyager::translate.partials.form-field', ['translate' => $translateValue, 'name_prefix' => \sprintf('%s[%s]', $name_prefix, $translateKey)])
        </div>
    @endif
@endforeach
