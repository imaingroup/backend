<?php
declare(strict_types=1);

namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\DepositCurrency;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * DepositCurrencyRepository.
 */
final class DepositCurrencyRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param DepositCurrency $model
     */
    public function __construct(protected DepositCurrency $model)
    {}

    /**
     * Find by crypto currency.
     *
     * @param int $cryptoCurrencyId
     * @return DepositCurrency|null
     */
    public function findByCryptoCurrency(int $cryptoCurrencyId): ?DepositCurrency
    {
        return $this->model
            ->newQuery()
            ->where('crypto_currencies_id', $cryptoCurrencyId)
            ->first();
    }

    /**
     * Get list.
     *
     * @return Collection
     */
    public function get(): Collection
    {
        return $this->model
            ->newQuery()
            ->orderBy('order')
            ->get();
    }
}
