<?php

namespace App\Modules\Investments\Models;

use App\Modules\Core\Models\Model;

final class TidRequestHistory extends Model
{
    protected $table = 'tid_requests_history';
}
