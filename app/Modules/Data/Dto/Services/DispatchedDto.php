<?php
declare(strict_types=1);

namespace App\Modules\Data\Dto\Services;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Core\Dto\RollbackAndResponse;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Immutable;

/**
 * DispatchedDto.
 *
 * @property-read Collection|ResponseDto[] $responses
 * @property-read bool $rollback
 * @property-read bool $is403
 * @property-read bool $is422
 */
#[Immutable]
final class DispatchedDto extends BaseDto implements RollbackAndResponse
{
    /**
     * Constructor.
     *
     * @param Collection $responses
     * @param bool $rollback
     * @param bool $is403
     * @param bool $is422
     */
    public function __construct(
        protected Collection $responses,
        protected bool $rollback,
        protected bool $is403,
        protected bool $is422,
    )
    {
    }

    /**
     * {@inheritDoc}
     */
    public function isRollback(): bool
    {
        return $this->rollback;
    }

    /**
     * Is 403.
     *
     * @return bool
     */
    public function is403(): bool
    {
        return $this->is403;
    }

    /**
     * Is 403.
     *
     * @return bool
     */
    public function is422(): bool
    {
        return $this->is422;
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape(['status' => "bool", 'data' => "array"])]
    public function jsonSerialize(): array
    {
        $responses = [];

        foreach ($this->responses as $response) {
            $responses[$response->params->route] = [
                'status' => $response->status,
                'version' => $response->version,
                'response' => $response->response,
            ];
        }

        return [
            'status' => !$this->isRollback(),
            'data' => $responses,
        ];
    }
}
