<?php
declare(strict_types=1);

namespace App\Modules\Finance\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Finance\Dto\Api\MarketShortDataApiDto;
use App\Modules\Finance\Enum\CoingeckoCoinEnum;
use App\Modules\Finance\Enum\CoingeckoNetworkEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Litipk\BigNumbers\Decimal;

/**
 * Crypto currency market.
 *
 * @property-read int $crypto_currencies_id
 * @property string|null $image
 * @property float $price
 * @property Carbon $price_updated_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read CryptoCurrency $currency
 * @property-read CoingeckoNetworkEnum $coingeckoNetworkEnum
 */
final class CryptoCurrencyMarket extends Model
{
    /** {@inheritdoc} */
    protected $table = 'crypto_currencies_market';

    /** {@inheritdoc} */
    protected $primaryKey = 'crypto_currencies_id';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'crypto_currencies_id' => 'integer',
        'image' => 'string',
        'price' => 'float',
        'price_updated_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Relation with {@see CryptoCurrency::class}.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_currencies_id');
    }

    /**
     * Get attribute "coingeckoNetworkEnum".
     *
     * @return CoingeckoNetworkEnum|null
     */
    public function getCoingeckoNetworkEnumAttribute(): ?CoingeckoNetworkEnum
    {
        return CoingeckoNetworkEnum::fromNetwork($this->currency->network->toEnum());
    }

    /**
     * To api dto.
     *
     * @return MarketShortDataApiDto
     */
    public function toApiDto(): MarketShortDataApiDto
    {
        return new MarketShortDataApiDto(
            $this->coingeckoNetworkEnum,
            $this->currency->address ? null : CoingeckoCoinEnum::fromNetwork($this->currency->network->toEnum()),
            $this->currency->address,
            $this->image,
            Decimal::create($this->price, $scale=null)->innerValue(),
            $this->price_updated_at,
        );
    }
}
