<?php

use App\Modules\Messages\Models\ChatUser;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds sid field to messages_chats_users.
 */
final class AddSidForChatUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages_chats_users', function (Blueprint $table) {
            $table->unsignedBigInteger('sid')->unique()->after('id')->nullable(true);
        });

        foreach (ChatUser::all() as $chatUser) {
            $chatUser->sid = mt_rand(100000000000, 999999999999);
            $chatUser->save();
        }

        Schema::table('messages_chats_users', function (Blueprint $table) {
            $table->unsignedBigInteger('sid')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages_chats_users', function (Blueprint $table) {
            $table->dropColumn('sid');
        });
    }
}
