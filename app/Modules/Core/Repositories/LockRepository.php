<?php
declare(strict_types=1);

namespace App\Modules\Core\Repositories;

use App\Modules\Core\Models\Lock;
use App\Modules\Core\Repositories\BaseRepository;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * Lock repository.
 */
final class LockRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Lock $model
     */
    public function __construct(protected Lock $model)
    {
    }

    /**
     * Select lock with lock for update.
     *
     * @param int $userId
     * @param int $action
     * @return Lock|null
     */
    public function selectLock(
        int $userId,
        #[ExpectedValues(Lock::ACTIONS)] int $action
    ): ?Lock
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $userId)
            ->where('action', $action)
            ->lockForUpdate()
            ->first();
    }
}
