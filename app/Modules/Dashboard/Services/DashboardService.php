<?php
declare(strict_types=1);

namespace App\Modules\Dashboard\Services;


use App\Modules\Actions\Repositories\UserActionRepository;
use App\Modules\Charts\Repositories\ChartRepository;
use App\Modules\Languages\Repositories\LangRepository;
use App\Modules\News\Repositories\VideoRepository;
use App\Modules\ProfitCards\Models\ProfitCard;
use App\Modules\ProfitCards\Repositories\ProfitCardRepository;
use App\Modules\Settings\Repositories\SettingsDashboardRepository;
use App\Modules\Settings\Repositories\SettingsDepositRepository;
use App\Modules\Settings\Repositories\SettingsPriceRepository;
use App\Modules\Settings\Repositories\SettingsTimerRepository;
use App\Modules\Settings\Services\AbcSettingsService;
use App\Modules\Users\Models\User;
use App\Modules\Users\Services\UserService;
use DateTime;
use Illuminate\Support\Facades\Storage;

/**
 * Dashboard service.
 */
final class DashboardService
{
    /**
     * Construct.
     *
     * @param VideoRepository $videoRepository
     * @param LangRepository $langRepository
     * @param SettingsTimerRepository $settingsTimerRepository
     * @param AbcSettingsService $abcSettingsService
     * @param SettingsDepositRepository $settingsDepositRepository
     * @param SettingsPriceRepository $settingsPriceRepository
     * @param ProfitCardRepository $profitCardRepository
     * @param ChartRepository $chartRepository
     * @param UserActionRepository $userActionRepository
     * @param SettingsDashboardRepository $settingsDashboardRepository
     * @param UserService $userService
     */
    public function __construct(
        private VideoRepository $videoRepository,
        private LangRepository $langRepository,
        private SettingsTimerRepository $settingsTimerRepository,
        private AbcSettingsService $abcSettingsService,
        private SettingsDepositRepository $settingsDepositRepository,
        private SettingsPriceRepository $settingsPriceRepository,
        private ProfitCardRepository $profitCardRepository,
        private ChartRepository $chartRepository,
        private UserActionRepository $userActionRepository,
        private SettingsDashboardRepository $settingsDashboardRepository,
        private UserService $userService
    )
    {
    }

    /**
     * Content.
     *
     * @param User $user
     * @return array
     */
    public function content(User $user): array
    {
        $settingsDashboard = $this->settingsDashboardRepository->get();

        $data = [
            'statement_link' => $settingsDashboard->statement_link,
            'video_list' => null,
            'timer' => $this->settingsTimerRepository->get(),
            'abc' => $this->abcSettingsService->getData(),
            'current_time_utc' => (new DateTime())->format('Y-m-d H:i:s'),
        ];

        $data = array_merge($data, $this->userService->getBalances($user));

        $videoData = [];
        $langs = $this->langRepository->all()->pluck('short_name')->toArray();
        foreach ($langs as $lang) {
            $videoData[$lang] = [];
        }

        $videoList = $this->videoRepository->getVideoList();
        foreach ($videoList as $video) {
            $videoData[$video->lang->short_name][] = [
                'id' => $video->sid,
                'video' => $video->getVideo(),
                'image' => Storage::disk('local')->url($video->image)
            ];
        }

        $data['video_list'] = $videoData;

        $data['timer']->title = $data['timer']->getTranslates('title');
        $data['timer']->title_when_it_end = $data['timer']->getTranslates('title_when_it_end');
        $data['timer']->text_when_it_end = $data['timer']->getTranslates('text_when_it_end');
        $data['timer']->description = $data['timer']->getTranslates('description');
        unset($data['timer']->translations);
        unset($data['timer']->id);
        unset($data['timer']->created_at);
        unset($data['timer']->updated_at);

        $profitCardData = $this->profitCardRepository->get()
            ->map(function (ProfitCard $profitCard) use ($settingsDashboard) {
                $availableCount = $settingsDashboard->hide_number_of_cards ?
                    null :
                    (
                        $profitCard->show_number_of_cards ?
                        $profitCard->available_count :
                        null
                    );

                return [
                    'id' => $profitCard->sid,
                    'available_count' => $availableCount,
                    'abc' => $profitCard->abc,
                    'price' => $profitCard->price,
                    'bonus' => $profitCard->bonus,
                    'title' => $profitCard->getTranslates('title'),
                    'image' => Storage::disk('local')->url($profitCard->image),
                    'type' => $profitCard->typeEnum,
                    'text_after_zero' => $profitCard->text_after_zero,
                    'stop_sell_after_zero' => $profitCard->stop_sell_after_zero,
                ];
            });

        $priceSettings = $this->settingsPriceRepository->get();

        $data['profit_cards'] = $profitCardData;
        $data['exchange_usdt_abc_text'] = $this->settingsDepositRepository->get()->getTranslates('exchange_usdt_abc_text');
        $data['abc_usdt_rate'] = $priceSettings->abc_price;
        $data['usdt_abc_rate'] = 1 / $priceSettings->abc_price;

        $data['charts'] = [];
        foreach ($this->chartRepository->get() as $chart) {
            $data['charts'][] = [
                'id' => $chart->sid,
                'html' => $chart->html,
            ];
        }

        $data['recent_activities'] = [];

        if (!$settingsDashboard->disabled_recent_activities && $settingsDashboard->ra_types) {
            $recentActivities = $this->userActionRepository
                ->getLast(10, $settingsDashboard->ra_types);

            foreach ($recentActivities as $recentActivity) {
                $data['recent_activities'][] = [
                    'date' => $recentActivity->created_at->format('d/m/Y'),
                    'time' => $recentActivity->created_at->format('H:i:s'),
                    'type' => $recentActivity->textAction()
                ];
            }
        }

        $data['geo'] = [];

        return $data;
    }
}
