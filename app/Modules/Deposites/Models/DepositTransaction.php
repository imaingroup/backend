<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Models;

use App\Modules\Deposites\Dto\Api\DepositTransactionPublicApiDto;
use App\Modules\Money\Models\Transaction;
use App\Modules\Core\Models\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use JetBrains\PhpStorm\Deprecated;
use Litipk\BigNumbers\Decimal;

/**
 * Deposit transaction model.
 *
 * @property-read int $id
 * @property-read int $deposits_id
 * @property Carbon|null $transaction_at
 * @property float $amount_coin
 * @property float $amount_usdt
 * @property int $status
 * @property string $txid
 * @property int $related_transactions_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Deposit $deposit
 * @property-read Transaction|null $transaction
 * @property-read string|null $statusText
 * @property-read bool $checkConfirmed
 */
final class DepositTransaction extends Model
{
    protected $table = 'deposits_transactions';

    const UNCONFIRMED = 1;
    const CONFIRMED = 2;

    const STATUSES = [
        self::UNCONFIRMED => 'NOT CONFIRMED',
        self::CONFIRMED => 'CONFIRMED',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'deposits_id' => 'integer',
        'transaction_at' => 'datetime',
        'amount_coin' => 'float',
        'amount_usdt' => 'float',
        'status' => 'integer',
        'txid' => 'string',
        'related_transactions_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get relation with Deposit
     *
     * @return BelongsTo
     */
    public function deposit(): BelongsTo
    {
        return $this->belongsTo(Deposit::class, 'deposits_id');
    }

    /**
     * Get relation with Transaction
     *
     * @return BelongsTo
     */
    public function transaction(): BelongsTo
    {
        return $this->belongsTo(Transaction::class, 'related_transactions_id');
    }

    /**
     * Get status in text representation.
     *
     * @return string|null
     */
    public function statusText(): ?string
    {
        return self::STATUSES[$this->status] ?? null;
    }

    /**
     * Get attribute "statusText".
     *
     * @return string|null
     */
    public function getStatusTextAttribute(): ?string
    {
        return $this->statusText();
    }

    /**
     * Get attribute "checkConfirmed".
     *
     * @return bool
     */
    public function getCheckConfirmedAttribute(): bool
    {
        return $this->status === self::CONFIRMED;
    }

    /**
     * To public api dto.
     *
     * @return DepositTransactionPublicApiDto
     */
    public function toPublicApiDto(): DepositTransactionPublicApiDto
    {
        return new DepositTransactionPublicApiDto(
            $this->transaction_at,
            Decimal::create($this->amount_usdt)->innerValue(),
            Decimal::create($this->amount_coin)->innerValue(),
            $this->deposit->currency->toApiDto(),
            $this->txid,
            $this->status,
            $this->statusText,
            $this->deposit->currency->depositCurrency ?
                $this->deposit->currency->depositCurrency->name :
                $this->deposit->currency->symbol,
            $this->deposit->currency->depositCurrency ?
                $this->deposit->currency->depositCurrency->full_name :
                $this->deposit->currency->name,
        );
    }
}
