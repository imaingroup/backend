-- install bitcoin core --
timedatectl set-timezone UTC
apt install software-properties-common
add-apt-repository -y ppa:ondrej/php
apt update
apt install php8.0 php8.0-fpm php8.0-curl php8.0-mbstring nginx git firewalld zip
systemctl enable firewalld
systemctl start firewalld
firewall-cmd --add-port=46789/tcp
firewall-cmd --add-port=46789/tcp --permanent
firewall-cmd --add-port=46283/tcp
firewall-cmd --add-port=46283/tcp --permanent
adduser bitcoin
cd /var
mkdir bitcoin
wget https://bitcoin.org/bin/bitcoin-core-0.21.0/bitcoin-0.21.0-x86_64-linux-gnu.tar.gz
tar -xf bitcoin-0.21.0-x86_64-linux-gnu.tar.gz
chown -R bitcoin:bitcoin bitcoin/
nano /etc/systemd/system/bitcoind.service
(put text from file server/prod/etc/systemd/system/bitcoind.service from a repository btc_server)
systemctl enable bitcoind.service
systemctl start bitcoind
cd var/bitcoin/bitcoin-core-0.21.0/bin
sudo su bitcoin
(if you login as root).
./bitcoin-cli createwallet bitcoin
nano /home/bitcoin/.bitcoin/bitcoin.conf
(put text from file server/prod/home/bitcoin/.bitcoin/bitcoin.conf from a repository btc_server.
In this file need edit three parameters: «dbcache=4096», set half from db memory site on server. «par=4» и «cores=4», it must be equals cpu on a server.)
systemctl restart bitcoind
nano /etc/nginx/sites-enabled/default
(put text from file server/btc/etc/nginx/sites-enabled/default)
systemctl restart nginx
nano /etc/php/8.0/fpm/php.ini
(add to config options from file server/btc/etc/php/8.0/fpm/php.ini)
nano /etc/php/8.0/fpm/pool.d/www.conf
(add to config options from file server/btc/etc/php/8.0/fpm/pool.d/www.conf)
systemctl restart php8.0-fpm.service
cd /var/www
git clone https://gitlab.com/btc_trading/proxycrypt
cd /var/www/proxycrypt
touch .forward
(put text "http://127.0.0.1:62437")
touch .secret
(generate password (20-100 length) and set it to ".secret" file and to env variable "CRYPTO_BTC_SECRET" for main site.)
(connect to main site, go to site directory, run command "php artisan finance:key:generate .btc.key")
((from main site directory) "scp .btc.key root@proxyServer:/var/www/proxycrypt/.key")
touch /var/log/nginx/proxyCrypt.log
chown www-data:www-data /var/log/nginx/proxyCrypt.log
