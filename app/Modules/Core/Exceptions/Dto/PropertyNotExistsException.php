<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Dto;

use Exception;

/**
 * PropertyNotExistsException
 */
final class PropertyNotExistsException extends Exception
{
}

