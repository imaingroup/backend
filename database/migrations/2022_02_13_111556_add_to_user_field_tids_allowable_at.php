<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * AddToUserFieldTidsAllowableAt.
 */
final class AddToUserFieldTidsAllowableAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dateTime('tids_allowable_at')
                ->nullable(true);
        });

        DB::transaction(function () {
            $typeId = DB::table('data_types')
                ->where('name', 'users')
                ->first()
                ->id;

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'tids_allowable_at',
                    'type' => 'timestamp',
                    'display_name' => 'TIDs allowable at',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 75
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('tids_allowable_at');
        });

        DB::transaction(function() {
            $typeId = DB::table('data_types')
                ->where('name', 'users')
                ->first()
                ->id;

            DB::table('data_rows')
                ->where('data_type_id', $typeId)
                ->whereIn('field', [
                    'tids_allowable_at',
                ])
                ->delete();
        });
    }
}
