<?php
declare(strict_types=1);

namespace App\Modules\Core\Traits;

use App\Facades\DB;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Core\Locks\AtomicLocks;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Database\Eloquent\Model;

/**
 * Lock trait.
 */
trait LockTrait
{
    /**
     * Get model by id with lock for update.
     *
     * @param int $id
     * @param array|null $columns
     * @return Model|null
     * @throws TransactionNotStarted
     */
    public static function getAndLockForUpdate(int $id, array $columns = null): ?Model
    {
        static::checkTransaction();

        $query = static::query()
            ->where('id', $id)
            ->lockForUpdate();

        if ($columns && count($columns) > 0) {
            $query->select($columns);
        }

        return $query->first();
    }

    /**
     * Set lock for model by id.
     *
     * @param int $id
     * @throws TransactionNotStarted
     */
    public static function modelLockForUpdate(int $id): void
    {
        static::checkTransaction();
        static::getAndLockForUpdate($id, ['id']);
    }

    /**
     * Get lock for db row and refresh model after it.
     *
     * @param Model $model
     * @throws TransactionNotStarted
     */
    public static function refreshAndLockForUpdate(Model $model): void
    {
        static::checkTransaction();
        static::modelLockForUpdate($model->id);

        $model->refresh();
    }

    /**
     * Check that transaction was started.
     *
     * @throws TransactionNotStarted
     */
    public static function checkTransaction()
    {
        if (DB::transactionLevel() === 0) {
            throw new TransactionNotStarted();
        }
    }

    /**
     * Lock await.
     *
     * @param int $seconds
     * @param callable $callable
     * @throws LockTimeoutException
     */
    public function lockAwait(int $seconds, callable $callable): void
    {
        AtomicLocks::lockAwait(
            $seconds,
            $seconds,
            sprintf('%s_%s_atomic', $this->table, $this->id),
            $callable
        );
    }

    /**
     * Lock skip.
     *
     * @param int $seconds
     * @param callable $callable
     */
    public function lockSkip(int $seconds, callable $callable): void
    {
        AtomicLocks::lockSkip(
            $seconds,
            sprintf('%s_%s_atomic', $this->table, $this->id),
            $callable
        );
    }
}
