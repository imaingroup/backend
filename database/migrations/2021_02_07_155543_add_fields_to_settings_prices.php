<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add fields to settings prices.
 */
final class AddFieldsToSettingsPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->float('eth_usdt_rate', 16, 8)
                ->after('btc_usdt_rate')
                ->default(1500);
            $table->dateTime('eth_rate_updated_at')
                ->after('rate_updated_at')
                ->default((new DateTime())->format('Y-m-d H:i:s'));
            $table->float('luft_deposit_eth_price')
                ->default(0)
                ->after('luft_deposit_btc_price');
            $table->float('luft_withdraw_eth_price')
                ->default(0)
                ->after('luft_withdraw_btc_price');
        });

        DB::beginTransaction();

        $typeId = DB::table('data_types')
            ->where('name', 'settings_prices')
            ->first()
            ->id;

        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'eth_usdt_rate',
                'type' => 'number',
                'display_name' => 'Eth Usdt Rate',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|numeric"}}',
                'order' => 3
            ]);
        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'eth_rate_updated_at',
                'type' => 'timestamp',
                'display_name' => 'Eth Rate Updated At',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|date"}}',
                'order' => 4
            ]);
        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'luft_deposit_eth_price',
                'type' => 'number',
                'display_name' => 'Luft Deposit Eth Price',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|numeric"}}',
                'order' => 6
            ]);
        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'luft_withdraw_eth_price',
                'type' => 'number',
                'display_name' => 'Luft Withdraw Eth Price',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|numeric"}}',
                'order' => 7
            ]);
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->dropColumn([
                'eth_usdt_rate',
                'eth_rate_updated_at',
                'luft_deposit_eth_price',
                'luft_withdraw_eth_price',
            ]);
        });

        $typeId = DB::table('data_types')
            ->where('name', 'settings_prices')
            ->first()
            ->id;

        DB::beginTransaction();
        DB::table('data_rows')
            ->where('data_type_id', $typeId)
            ->whereIn('field', [
                'eth_usdt_rate',
                'eth_rate_updated_at',
                'luft_deposit_eth_price',
                'luft_withdraw_eth_price',
            ])
            ->delete();
        DB::commit();
    }
}
