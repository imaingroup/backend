<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\Messages\Models\Chat;
use App\Modules\Messages\Services\MessageService;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Controller;

/**
 * Admin messages controller.
 */
final class VoyagerMessagesController extends Controller
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Messages\Services\MessageService $messageService
     */
    public function __construct(
        private MessageService $messageService
    )
    {
    }

    /**
     * View global actions.
     *
     * @return View
     */
    public function list(): View
    {
        return Voyager::view('voyager::messages.list');
    }

    /**
     * Get chats.
     *
     * @return array
     * @throws Exception
     */
    #[ArrayShape(['data' => "array"])]
    public function getChats(): array
    {
        return [
            'data' => [
                'chats' => $this->messageService->getAdminChatList(Auth::user())
            ]
        ];
    }

    /**
     * Get messages.
     *
     * @param Chat $chat
     * @return array
     */
    #[ArrayShape(['data' => "array"])]
    public function getMessages(Chat $chat): array
    {
        return [
            'data' => [
                'messages' => array_reverse(
                    $this->messageService
                        ->getAdminMessagesList(Auth::user(), $chat, null)
                )
            ]
        ];
    }
}
