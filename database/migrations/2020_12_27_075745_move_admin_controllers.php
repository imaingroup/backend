<?php
declare(strict_types=1);


use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migrations moves admin controllers to voyager namespace.
 */
final class MoveAdminControllers extends Migration
{
    /** string[] Migration data. */
    private array $data = [
        'App\Http\Controllers\VoyagerUsersController' => 'App\Modules\Admin\Controllers\VoyagerUsersController',
        'App\Http\Controllers\VoyagerProfitCardsController' => 'App\Modules\Admin\Controllers\VoyagerProfitCardsController',
        'App\Http\Controllers\VoyagerWithdrawController' => 'App\Modules\Admin\Controllers\VoyagerWithdrawController',
        'App\Http\Controllers\VoyageTidHistoryController' => 'App\Modules\Admin\Controllers\VoyagerTidHistoryController'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        foreach ($this->data as $oldController => $newController) {
            DB::table('data_types')
                ->where('controller', $oldController)
                ->update([
                    'controller' => $newController
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        foreach (array_flip($this->data) as $newController => $oldController) {
            DB::table('data_types')
                ->where('controller', $newController)
                ->update([
                    'controller' => $oldController
                ]);
        }
    }
}
