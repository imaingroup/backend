<?php
declare(strict_types=1);

namespace App\Modules\Core\Services;

use App\Facades\DB;
use App\Modules\Core\Exceptions\TaskException;
use App\Modules\Core\Exceptions\TaskPartialException;
use App\Modules\Core\Exceptions\TaskPartialNormalException;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Core\Jobs\TaskJob;
use App\Modules\Core\Models\Task;
use App\Modules\Core\Repositories\TaskRepository;
use App\Modules\Finance\Exceptions\Btc\EstimateSmartFeeException;
use App\Modules\Finance\Exceptions\Btc\GetBalanceException;
use App\Modules\Finance\Exceptions\Btc\NotEnoughBalanceException;
use App\Modules\Finance\Exceptions\Btc\SendException;
use App\Modules\Finance\Exceptions\Btc\SendIncorrectSettingsException;
use App\Modules\Finance\Exceptions\Btc\SetTxFeeException;
use App\Modules\Finance\Exceptions\SecureCryptRequestException;
use App\Modules\Finance\Exceptions\SecureCryptRequestParamsException;
use App\Modules\Finance\Services\BtcApiService;
use App\Modules\Finance\Services\FinanceService;
use App\Modules\Finance\Services\V2\CryptoProcessingService;
use App\Modules\Finance\Services\V2\Ethereum\ManageService;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Logs\Models\OperationLog;
use App\Modules\Logs\Services\OperationLogService;
use App\Modules\Money\Exceptions\MoneyTransferException;
use App\Modules\Money\Models\Transaction;
use App\Modules\Money\Services\MoneyTransferService;
use App\Modules\Users\Models\User;
use App\Modules\Users\Repositories\UserRepository;
use DateTime;
use Illuminate\Support\Collection;
use Throwable;

/**
 * Tasks service.
 */
final class TaskService
{
    /** @var string[] Bind list. */
    private const BIND_TASK_TYPE_TO_METHOD = [
        Task::TYPE_ADMIN_MASS_MONEY_OPERATION_ABC => 'massChangeBalance',
        Task::TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DEPOSITED => 'massChangeBalance',
        Task::TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DIVIDENDS => 'massChangeBalance',
        Task::TYPE_ADMIN_SEND_GETH_MONEY => 'sendGeth',
        Task::TYPE_ADMIN_SEND_BTC_MONEY => 'sendBtc',
        Task::TYPE_ADMIN_SEND_ETHEREUM_TYPE_MONEY => 'sendGeth',
    ];

    /**
     * Create new task.
     *
     * @param int $type
     * @param Collection $data
     * @param User $user
     * @param bool $immediate
     * @return Task
     */
    public function new(int $type, Collection $data, User $user, bool $immediate = false): Task
    {
        $task = new Task();
        $task->type = $type;
        $task->status = Task::STATUS_NEW;
        $task->data = $data;
        $task->user()
            ->associate($user);
        $task->save();

        if ($immediate) {
            DB::afterCommit(fn() => $this->handleTask($task));
        } else {
            TaskJob::dispatch($task);
        }

        return $task;
    }

    /**
     * Get by type.
     *
     * @param int $type
     * @param int $limit
     * @return Collection
     */
    public function getByType(int $type, int $limit = Task::LIMIT): Collection
    {
        return $this->getTaskRepository()
            ->getByType($type, $limit);
    }

    /**
     * Get tasks by types.
     *
     * @param array $types
     * @param int $limit
     * @return array
     */
    public function getByTypes(array $types, int $limit = Task::LIMIT): array
    {
        $data = [];

        foreach ($this->getTaskRepository()
                     ->getByTypes($types, $limit) as $task) {
            $data[] = [
                'id' => $task->id,
                'users_id' => $task->users_id,
                'type' => $task->type,
                'readable_type' => $task->readableType,
                'status' => $task->status,
                'readable_status' => $task->readableStatus,
                'operation_logs_id' => $task->operation_logs_id,
                'data' => $task->data,
                'post_data' => $task->post_data,
                'created_at' => $task->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $task->updated_at->format('Y-m-d H:i:s')
            ];
        }

        return $data;
    }

    /**
     * Handle task.
     *
     * @param Task $task
     * @throws TaskException
     * @throws Throwable
     */
    public function handleTask(Task $task): void
    {
        if (DB::transactionLevel() !== 0) {
            throw new TaskException('Transaction already started. This method required rule no active transaction.');
        }

        $this->changeTaskStatus($task);

        try {
            DB::beginTransaction();

            $task = $this->getTaskRepository()
                ->getAndLockForUpdate($task->id);

            if ($task->status !== Task::STATUS_IN_PROCESS) {
                throw new TaskException('This task cant processed.');
            }

            $method = static::BIND_TASK_TYPE_TO_METHOD[$task->type] ?? null;

            if (!$method) {
                throw new TaskException('This task type not supported.');
            }

            $this->$method($task);

            $task->status = Task::STATUS_PROCESSED;
            $task->save();

            DB::commit();
        } catch (TaskPartialNormalException $e) {
            $task->status = Task::STATUS_NORMAL_PARTIAL;
            $task->save();

            TaskJob::dispatch($task);

            DB::commit();
        } catch (TaskPartialException $e) {
            DB::commit();

            $this->getLogger()
                ->rollbackTransaction($e, $task);
            $task->refresh();

            $task->status = Task::STATUS_FAILS_PARTIAL;

            $task->save();
        } catch (Throwable $e) {
            DB::rollBack();

            $this->getLogger()
                ->rollbackTransaction($e, $task);
            $task->refresh();

            $task->status = Task::STATUS_FAILS;

            $task->save();

            throw $e;
        }
    }

    /**
     * Mass change balance.
     *
     * @param Task $task
     * @throws MoneyTransferException
     * @throws TransactionNotStarted
     */
    private function massChangeBalance(Task $task): void
    {
        $initiator = $task->user;
        $affected = 0;
        $totalAmount = 0;

        foreach (
            $this->getUserRepository()
                ->getByType(User::TYPE_USER)
            as $user
        ) {
            $this->getMoneyTransferService()
                ->moneyOperation(
                    $user,
                    $initiator,
                    $task->data->get('amount'),
                    $task->data->get('currency'),
                    null,
                    null,
                    $task
                );

            $affected++;
            $totalAmount += abs($task->data->get('amount'));
        }

        $this->massChangeBalanceLog($task, $initiator, $affected, $totalAmount);
    }

    /**
     * Send eth.
     *
     * @param Task $task
     * @throws Throwable
     */
    private function sendGeth(Task $task): void
    {
        $balance = $this->getFinanceService()
            ->findCryptoBalance($task->data->get('balance')['id']);

        $sent = $balance->currency->isMainCurrencyInNetwork ?
            $this->getManageService()
                ->send(
                    $balance->currency->network->toEnum(),
                    $balance->address->address,
                    $task->data->get('to'),
                    $task->data->get('amount'),
                    $balance->address->access_key
                ) :
            $this->getManageService()
                ->sendTokens(
                    $balance->currency->network->toEnum(),
                    $balance->currency->address,
                    $task->data->get('to'),
                    $task->data->get('amount'),
                    $balance->currency->decimals,
                    $balance->address->access_key
                );

        $task->post_data = collect(['sent' => $sent]);

        $task->save();

        $this->getCryptoProcessingService()
            ->updateBalanceEthereumType($balance->address);
    }

    /**
     * Sent btc money.
     *
     * @param Task $task
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws EstimateSmartFeeException
     * @throws GetBalanceException
     * @throws NotEnoughBalanceException
     * @throws SendException
     * @throws SendIncorrectSettingsException
     * @throws SetTxFeeException
     */
    private function sendBtc(Task $task): void
    {
        $task->post_data = collect([
            'txid' => $this->getBtcApiService()
                ->sendMoney(
                    $task->data->get('to'),
                    $task->data->get('amount'),
                )
        ]);

        $task->save();
    }

    /**
     * Mass change balance add log.
     *
     * @param Task $task
     * @param User $initiator
     * @param int $affected
     * @param float $totalAmount
     */
    private function massChangeBalanceLog(Task $task, User $initiator, int $affected, float $totalAmount): void
    {
        $task->log()
            ->associate(
                $this->getOperationLogService()
                    ->add(
                        OperationLog::TYPE_ADMIN_MASS_MONEY_OPERATION,
                        $initiator,
                        $task->data->get('amount'),
                        [
                            'admin_user_id' => $initiator->id,
                            'admin_username' => $initiator->username,
                            'amount' => abs($task->data->get('amount')),
                            'currency' => Transaction::$currencyName[$task->data->get('currency')],
                            'count_affected' => $affected,
                            'total_amount' => $totalAmount,
                            'date_operation' => (new DateTime())->format('Y-m-d H:i:s')
                        ],
                        $task
                    )
            );

        $task->save();
    }

    /**
     * Change task status.
     *
     * @param Task $task
     * @throws TaskException
     * @throws Throwable
     */
    private function changeTaskStatus(Task $task)
    {
        try {
            DB::beginTransaction();
            Task::refreshAndLockForUpdate($task);

            if ($task->status !== Task::STATUS_NEW) {
                throw new TaskException('This task cant processed.');
            }

            $task->status = Task::STATUS_IN_PROCESS;
            $task->save();

            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();

            $this->getLogger()
                ->rollbackTransaction($e, $task);

            throw $e;
        }
    }

    /**
     * Get TaskRepository.
     *
     * @return TaskRepository
     */
    private function getTaskRepository(): TaskRepository
    {
        return app(TaskRepository::class);
    }

    /**
     * Get OperationLogService.
     *
     * @return OperationLogService
     */
    private function getOperationLogService(): OperationLogService
    {
        return app(OperationLogService::class);
    }

    /**
     * Get Logger.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }

    /**
     * Get BtcApiService.
     *
     * @return BtcApiService
     */
    private function getBtcApiService(): BtcApiService
    {
        return app(BtcApiService::class);
    }

    /**
     * Get MoneyTransferService.
     *
     * @return MoneyTransferService
     */
    private function getMoneyTransferService(): MoneyTransferService
    {
        return app(MoneyTransferService::class);
    }

    /**
     * Get UserRepository.
     *
     * @return UserRepository
     */
    private function getUserRepository(): UserRepository
    {
        return app(UserRepository::class);
    }

    /**
     * Get {@see ManageService::class}.
     *
     * @return ManageService
     */
    private function getManageService(): ManageService
    {
        return app(ManageService::class);
    }

    /**
     * Get {@see FinanceService::class}.
     *
     * @return FinanceService
     */
    private function getFinanceService(): FinanceService
    {
        return app(FinanceService::class);
    }

    /**
     * Get {@see CryptoProcessingService::class}.
     *
     * @return CryptoProcessingService
     */
    private function getCryptoProcessingService(): CryptoProcessingService
    {
        return app(CryptoProcessingService::class);
    }
}
