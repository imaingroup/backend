<?php
declare(strict_types=1);

namespace App\Modules\Finance\Commands\V2\Migrations;

use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\RequestException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\ValidationException;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Services\V2\Ethereum\ManageService;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

/**
 * Token fetcher command.
 */
final class MigrateEthereumKeystoreToV2Command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finance:migrate_ethereum_keystore_to_v2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate ethereum keystore to V2.';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws FileNotFoundException
     * @throws RequestException
     * @throws ValidationException
     */
    public function handle(): int
    {
        $keystoreFiles = Storage::disk('private')
            ->allFiles();

        collect($keystoreFiles)
            ->each(function (string $keystore, int $index) use ($keystoreFiles) {
                $keystoreArray = explode('--', $keystore);
                $address = sprintf('0x%s', $keystoreArray[count($keystoreArray) - 1]);

                /** @var CryptoAddress|null $cryptoAddress */
                $cryptoAddress = CryptoAddress::query()
                    ->where('address', $address)
                    ->whereNotNull('eth_password')
                    ->whereNull('access_key')
                    ->first();

                if ($cryptoAddress) {
                    $keystoreData = Storage::disk('private')->get($keystore);

                    $wallet = $this->getManageService()
                        ->migrateWallet($keystoreData, $cryptoAddress->eth_password);

                    $cryptoAddress->access_key = $wallet->privateKey;
                    $cryptoAddress->save();

                    $this->info(sprintf('Processed %s %s from %s', $cryptoAddress->address, $index + 1, count($keystoreFiles)));
                }
            });

        return 0;
    }

    /**
     * Get {@see ManageService::class}.
     *
     * @return ManageService
     */
    private function getManageService(): ManageService
    {
        return app(ManageService::class);
    }
}
