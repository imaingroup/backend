<?php
declare(strict_types=1);

namespace App\Modules\Core\Validation;

use Illuminate\Validation\Validator;
use Merkeleon\PhpCryptocurrencyAddressValidation\Validation;
use Throwable;

/**
 * This class validates that value is bitcoin address.
 */
final class Bitcoin
{
    /**
     * Validate.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        if (!is_string($value)) {
            return false;
        }

        try {
            $validator = Validation::make('BTC');

            return $validator->validate($value);
        } catch (Throwable) {
            return false;
        }
    }
}
