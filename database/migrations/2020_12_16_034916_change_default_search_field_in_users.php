<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration change default_search_key in users to email in admin.
 */
final class ChangeDefaultSearchFieldInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $userRow = DB::table('data_types')
            ->where('name', 'users')
            ->first();

        $details = json_decode($userRow->details);
        $details->default_search_key = 'email';

        DB::table('data_types')
            ->where('name', 'users')
            ->update([
                'details' => json_encode($details)
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $userRow = DB::table('data_types')
            ->where('name', 'users')
            ->first();

        $details = json_decode($userRow->details);
        $details->default_search_key = 'username';

        DB::table('data_types')
            ->where('name', 'users')
            ->update([
                'details' => json_encode($details)
            ]);
    }
}
