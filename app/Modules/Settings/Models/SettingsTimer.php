<?php

namespace App\Modules\Settings\Models;

use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Core\Models\Model;
use TCG\Voyager\Traits\Translatable;

final class SettingsTimer extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;
    protected $translatable = ['title', 'title_when_it_end', 'text_when_it_end', 'description'];

    protected $table = 'settings_timer';
}
