<?php
declare(strict_types=1);

namespace App\Modules\Admin\Controllers;

use App\Modules\ProfitCards\Enums\Models\ProfitCard\Type;
use App\Modules\ProfitCards\Services\TradingCardsHistoryService;
use App\Modules\Stats\Services\StatsIproService;
use Illuminate\View\View;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;

/**
 * Voyager controller.
 */
final class VoyagerController extends BaseVoyagerController
{
    /**
     * Index action.
     *
     * @return View
     */
    public function index(): View
    {
        return Voyager::view(
            'voyager::index',
            [
                'iproStats' => $this->getStatsIproService()
                    ->purchase(),
                'purchaseTradingHistory' => $this->getTradingCardsHistoryService()
                    ->getLatestByType(Type::TRADING, 20),
                'purchaseExpandedHistory' => $this->getTradingCardsHistoryService()
                    ->getLatestByType(Type::EXPANDED, 20)
            ]
        );
    }

    /**
     * Get {@see StatsIproService::class}.
     *
     * @return StatsIproService
     */
    private function getStatsIproService(): StatsIproService
    {
        return app(StatsIproService::class);
    }

    /**
     * Get {@see TradingCardsHistoryService::class}.
     *
     * @return TradingCardsHistoryService
     */
    private function getTradingCardsHistoryService(): TradingCardsHistoryService
    {
        return app(TradingCardsHistoryService::class);
    }
}
