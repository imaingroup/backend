<?php
declare(strict_types=1);

namespace App\Modules\Security\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Security\Models\UserIdentification;
use App\Modules\Security\Models\UserIdentificationRelation;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

/**
 * UserIdentificationRelationRepository.
 */
final class UserIdentificationRelationRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param UserIdentificationRelation $model
     */
    public function __construct(protected UserIdentificationRelation $model)
    {
    }

    /**
     * Get users ids by identifiers' collection.
     *
     * @param Collection $collection
     * @param bool $accurate
     * @return Collection
     */
    public function getUsersIdsByByIdentifiersCollection(Collection $collection, bool $accurate): Collection
    {
        $allowQuery = false;

        $query = $this->model
            ->newQuery()
            ->select('users_identifications_id', 'users_id')
            ->whereIn('users_identifications_id', function (Builder $builder) use ($collection, $accurate, &$allowQuery) {
                $builder->select('id')
                    ->from((new UserIdentification())->getTable())
                    ->where(function (Builder $builder) use ($collection, &$allowQuery) {
                        foreach ($collection as $key => $items) {
                            if (count($items) > 0) {
                                $builder->orWhereIn($key, $items);

                                $allowQuery = true;
                            }
                        }
                    });
            })
            ->where('is_accurate', $accurate)
            ->groupBy('users_identifications_id', 'users_id');

        return $allowQuery ?
            $query
                ->get() :
            collect();
    }


    /**
     * Get by user.
     *
     * @param int $userId
     * @param bool $accurate
     * @return Collection
     */
    public function getUsersIdsByUser(int $userId, bool $accurate): Collection
    {
        return $this->model
            ->newQuery()
            ->select('users_id')
            ->whereIn('users_identifications_id', function (Builder $builder) use ($userId, $accurate) {
                $builder->select('users_identifications_id')
                    ->from((new UserIdentificationRelation)->getTable())
                    ->where('users_id', $userId)
                    ->where('is_accurate', $accurate);
            })
            ->where('is_accurate', $accurate)
            ->groupBy('users_id')
            ->get();
    }

    /**
     * Get by user.
     *
     * @param Collection $identificationIds
     * @param bool $accurate
     * @return Collection
     */
    public function getRelatedByIdentificationIds(Collection $identificationIds, bool $accurate): Collection
    {
        if($identificationIds->count() === 0) {
            return collect();
        }

        return $this->model
            ->newQuery()
            ->whereIn('users_identifications_id', $identificationIds)
            ->where('is_accurate', $accurate)
            ->get();
    }
}
