<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Dto\Api;

use App\Modules\Core\Dto\BaseDto;

/**
 * DepositDataApiDto.
 *
 * @property-read string $address
 * @property-read float $amount
 * @property-read int $depositTurnOnThrough
 * @property-read int $createNewInvoiceTurnOnThrough
 * @property-read string $currentTimeUtc
 * @property-read string $invoiceCreated
 * @property-read string|null $qrImage
 * @property-read string $currency
 * @property-read int $currencyId
 */
final class DepositInvoiceApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param string $address
     * @param float $amount
     * @param int $depositTurnOnThrough
     * @param int $createNewInvoiceTurnOnThrough
     * @param string $currentTimeUtc
     * @param string $invoiceCreated
     * @param string|null $qrImage
     * @param string $currency
     * @param int $currencyId
     */
    public function __construct(
        protected string $address,
        protected float $amount,
        protected int $depositTurnOnThrough,
        protected int $createNewInvoiceTurnOnThrough,
        protected string $currentTimeUtc,
        protected string $invoiceCreated,
        protected ?string $qrImage,
        protected string $currency,
        protected int $currencyId,
    ) {

    }
}
