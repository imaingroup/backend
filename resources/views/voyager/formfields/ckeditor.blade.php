<?php $id = mt_rand(1000000000, 9999999999); ?>
<textarea id="{{ $id }}" @if($row->required == 1) required @endif class="test-t-1 form-control" name="{{ $row->field }}"
          rows="{{ $options->display->rows ?? 5 }}">{{ old($row->field, $dataTypeContent->{$row->field} ?? $options->default ?? '') }}</textarea>

<script src="/js-854/ckeditor4/ckeditor.js"></script>

@push('javascript')
    <script>
        new (function () {
            if (CKEDITOR.env.ie && CKEDITOR.env.version < 9)
                CKEDITOR.tools.enableHtml5Elements(document);

            CKEDITOR.config.height = 500;
            //CKEDITOR.config.width = 'auto';
            this.editorInstance = null;
            const wysiwygareaAvailable = isWysiwygareaAvailable();
            const isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

            this.init = function () {
                const editorElement = CKEDITOR.document.getById('{{ $id }}');

                if (wysiwygareaAvailable) {
                    this.editorInstance = CKEDITOR.replace('{{ $id }}');
                } else {
                    editorElement.setAttribute('contenteditable', 'true');
                    this.editorInstance = CKEDITOR.inline('{{ $id }}');
                }

                this.editorInstance.on( 'change', function( evt ) {
                    $('#{{ $id }}').val(evt.editor.getData());
                });
            };

            this.destroy = function() {
                if(this.editorInstance) {
                    this.editorInstance.destroy();
                }
            }

            function isWysiwygareaAvailable() {
                if (CKEDITOR.revision == ('%RE' + 'V%')) {
                    return true;
                }

                return !!CKEDITOR.plugins.get('wysiwygarea');
            }

            const editor = this;
            this.init();

            $().ready(function () {
                $('[name=i18n_selector]').onFirst('change', function () {
                    editor.destroy();
                });

                $('[name=i18n_selector]').on('change', function () {
                    editor.init();
                });
            });
        })
    </script>
@endpush
