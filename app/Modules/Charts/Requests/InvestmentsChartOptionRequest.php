<?php
declare(strict_types=1);

namespace App\Modules\Charts\Requests;

use App\Modules\Investments\Models\TradeOption;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation
 * investments chart by option request.
 */
final class InvestmentsChartOptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->tradeOptionChart instanceof TradeOption;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape([
        'tradeOption' => "App\\Modules\\Investments\\Models\\TradeOption",
        'period' => "string"
    ])]
    public function validated(): array
    {
        return [
            'tradeOption' => $this->tradeOptionChart
        ];
    }
}
