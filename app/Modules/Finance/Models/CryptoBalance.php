<?php
declare(strict_types=1);

namespace App\Modules\Finance\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Finance\Dto\Api\TokenBalanceApiDto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Crypto balances model.
 *
 * @property-read int $id
 * @property-read int $crypto_addresses_id
 * @property-read int $crypto_currencies_id
 * @property float $balance
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read CryptoAddress $address
 * @property-read CryptoCurrency $currency
 */
final class CryptoBalance extends Model
{
    /** {@inheritdoc} */
    protected $table = 'crypto_balances';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'crypto_addresses_id' => 'integer',
        'crypto_currencies_id' => 'integer',
        'balance' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Relation with {@see CryptoAddress::class}.
     *
     * @return BelongsTo
     */
    public function address(): BelongsTo
    {
        return $this->belongsTo(CryptoAddress::class, 'crypto_addresses_id');
    }

    /**
     * Relation with {@see CryptoCurrency::class}.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_currencies_id');
    }

    /**
     * To api dto.
     *
     * @return TokenBalanceApiDto
     */
    public function toApiDto(): TokenBalanceApiDto
    {
        return new TokenBalanceApiDto(
            $this->id,
            $this->balance,
            $this->currency->toApiDto(false),
        );
    }
}
