<?php

namespace App\Modules\Landing\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * This request class need for validating data from landing contact form.
 */
final class LandingContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $recaptchaEnabled = (int)env('GOOGLE_RECAPTCHA_ENABLED') === 1;
        $data = [];

        if ($recaptchaEnabled) {
            $data['g-recaptcha-response'] = 'required|scalar|recaptcha';
        }

        return array_merge($data, [
            'name' => 'required|scalar|max:255',
            'email' => 'required|scalar|email:rfc,dns|max:255',
            'message' => 'required|scalar|max:1000'
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function messages()
    {
        return [
            'name.required' => 'landing.form.validation.contacts.name_required',
            'name.max' => 'landing.form.validation.contacts.name_max',

            'email.required' => 'landing.form.validation.contacts.email_incorrect',
            'email.email' => 'landing.form.validation.contacts.email_incorrect',
            'email.max' => 'landing.form.validation.contacts.email_incorrect',

            'message.required' => 'landing.form.validation.contacts.message_required',
            'message.max' => 'landing.form.validation.contacts.message_max',

            'g-recaptcha-response.required' => 'landing.form.validation.contacts.g_recaptcha_response_required',
            'g-recaptcha-response.recaptcha' => 'landing.form.validation.contacts.g_recaptcha_response_recaptcha'
        ];
    }
}
