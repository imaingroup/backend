<?php
declare(strict_types=1);

namespace App\Modules\Core\Validation;

use App\Modules\Finance\Services\BtcApiService;
use Illuminate\Validation\Validator;
use Throwable;

/**
 * This class validates that value is bitcoin tx id.
 */
final class BitcoinTx
{
    /**
     * Validate.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        if (!is_string($value)) {
            return false;
        }

        try {
            $this->getBtcApiService()->getTransaction($value);

            return true;
        } catch (Throwable) {
            return false;
        }
    }

    /**
     * Get BtcApiService.
     *
     * @return BtcApiService
     */
    private function getBtcApiService(): BtcApiService
    {
        return app(BtcApiService::class);
    }
}
