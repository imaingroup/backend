<?php
declare(strict_types=1);

namespace App\Modules\Security\Services\Identification;

use App\Modules\Security\Dto\Api\Identifications\BrowserApiDto;
use App\Modules\Security\Dto\Api\Identifications\HistoryApiDto;
use App\Modules\Security\Dto\Api\Identifications\HistoryItemApiDto;
use App\Modules\Security\Dto\Api\Identifications\IPApiDto;
use App\Modules\Security\Dto\Api\Identifications\RelatedUserApiDto;
use App\Modules\Security\Models\UserIdentification;
use App\Modules\Security\Models\UserIdentificationUserRelation;
use App\Modules\Security\Repositories\UserIdentificationRepository;
use App\Modules\Security\Repositories\UserIdentificationUserRelationRepository;
use App\Modules\Users\Models\User;
use DateTime;
use Illuminate\Support\Collection;
use Throwable;

/**
 * GetterService.
 */
final class GetterService
{
    private const HISTORY_FIELDS = [
        'id',
        'user_agent_id',
        'users_id',
        'userId',
        'action',
        'ip',
        'country',
        'current_resolution',
        'time_zone',
        'languages',
        'created_at'
    ];

    /**
     * Update multi accounts.
     *
     * @throws Throwable
     */
    public function getHistory(User $user): HistoryApiDto
    {
        return new HistoryApiDto(
            $this->getUserIdentificationUserRelationRepository()
                ->getByMainUser($user->id)
                ->map(function (UserIdentificationUserRelation $userRelation) {
                    return new RelatedUserApiDto(
                        $userRelation->user->id,
                        $userRelation->user->username,
                        $userRelation->user->email,
                        $userRelation->is_accurate,
                        $userRelation->user->created_at->format('Y-m-d H:i:s'),
                        (new DateTime($userRelation->user->last_activity_at))->format('Y-m-d H:i:s'),
                    );
                }),
            $this->getHistoryData($user),
            $this->getIpsData($user),
            $this->getBrowsersData($user)
        );
    }

    /**
     * Get history data.
     *
     * @param User $user
     * @return Collection
     */
    public function getHistoryData(User $user): Collection
    {
        return $this->getUserIdentificationRepository()
            ->getHistoryByUser(
                $user->id,
                false,
                collect(self::HISTORY_FIELDS)
            )
            ->map(fn(UserIdentification $identification) => $this->hydrateHistory($identification, false))
            ->merge(
                $this->getUserIdentificationRepository()
                    ->getHistoryByUser(
                        $user->id,
                        true,
                        collect(self::HISTORY_FIELDS)
                    )
                    ->map(fn(UserIdentification $identification) => $this->hydrateHistory($identification, true))
            )
            ->sortByDesc(function (HistoryItemApiDto $history) {
                return $history->id;
            })
            ->values();
    }

    /**
     * Get ips data.
     *
     * @param User $user
     * @return Collection
     */
    public function getIpsData(User $user): Collection
    {
        return $this->getUserIdentificationRepository()
            ->getIpsByUser(
                $user->id,
                false,
            )
            ->map(function (UserIdentification $identification) {
                return new IPApiDto(
                    $identification->ip,
                    $identification->country,
                    $identification->first_date,
                    false,
                );
            })
            ->merge(
                $this->getUserIdentificationRepository()
                    ->getIpsByUser(
                        $user->id,
                        true,
                    )
                    ->map(function (UserIdentification $identification) {
                        return new IPApiDto(
                            $identification->ip,
                            $identification->country,
                            $identification->first_date,
                            true,
                        );
                    })
            )
            ->sortByDesc(function (IPApiDto $ip) {
                return new DateTime($ip?->firstDate);
            })
            ->values();
    }

    /**
     * Get browsers data.
     *
     * @param User $user
     * @return Collection
     */
    public function getBrowsersData(User $user): Collection
    {
        return $this->getUserIdentificationRepository()
            ->getBrowsersByUser(
                $user->id,
                false,
            )
            ->map(function (UserIdentification $identification) {
                return new BrowserApiDto(
                    $identification->user_agent,
                    $identification->first_date,
                    false,
                );
            })
            ->merge(
                $this->getUserIdentificationRepository()
                    ->getBrowsersByUser(
                        $user->id,
                        true,
                    )
                    ->map(function (UserIdentification $identification) {
                        return new BrowserApiDto(
                            $identification->user_agent,
                            $identification->first_date,
                            true,
                        );
                    })
            )
            ->sortByDesc(function (BrowserApiDto $browser) {
                return new DateTime($browser?->firstDate);
            })
            ->values();
    }

    /**
     * Hydrate history.
     *
     * @param UserIdentification $identification
     * @param bool $isAccurate
     * @return HistoryItemApiDto
     */
    private function hydrateHistory(UserIdentification $identification, bool $isAccurate): HistoryItemApiDto
    {
        return new HistoryItemApiDto(
            $identification->id,
            $isAccurate,
            $identification->users_id,
            $identification->userId,
            $identification->action,
            $identification->ip,
            $identification->country,
            $identification->userAgent?->browser,
            $identification->userAgent?->browser_major_version,
            $identification->userAgent?->platform,
            $identification->userAgent?->os_version,
            $identification->userAgent?->device,
            $identification->userAgent?->device_type,
            $identification->userAgent?->device_vendor,
            $identification->current_resolution,
            $identification->time_zone,
            $identification->languages,
            $identification->created_at->format('Y-m-d H:i:s')
        );
    }

    /**
     * Get {@see UserIdentificationRepository::class}.
     *
     * @return UserIdentificationRepository
     */
    private function getUserIdentificationRepository(): UserIdentificationRepository
    {
        return app(UserIdentificationRepository::class);
    }

    /**
     * Get {@see UserIdentificationUserRelationRepository::class}.
     *
     * @return UserIdentificationUserRelationRepository
     */
    private function getUserIdentificationUserRelationRepository(): UserIdentificationUserRelationRepository
    {
        return app(UserIdentificationUserRelationRepository::class);
    }
}
