<?php
declare(strict_types=1);

namespace App\Modules\Security\Dto\Api\Identifications;

use App\Modules\Core\Dto\BaseDto;

/**
 * BrowserApiDto.
 *
 * @property-read string|null $browser
 * @property-read string|null $firstDate
 * @property-read bool $isAccurate
 */
final class BrowserApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param string|null $browser
     * @param string|null $firstDate
     * @param bool $isAccurate
     */
    public function __construct(
        protected ?string $browser,
        protected ?string $firstDate,
        protected bool $isAccurate,
    )
    {
    }

    /**
     * Get key.
     *
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->browser;
    }
}
