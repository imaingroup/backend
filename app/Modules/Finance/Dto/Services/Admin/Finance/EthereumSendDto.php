<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Services\Admin\Finance;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Finance\Models\CryptoBalance;
use App\Modules\Users\Models\User;
use Litipk\BigNumbers\Decimal;

/**
 * EthereumSendDto.
 *
 * @property-read User $user
 * @property-read CryptoBalance $balance
 * @property-read string $to
 * @property-read Decimal $amount
 */
final class EthereumSendDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param User $user
     * @param CryptoBalance $balance
     * @param string $to
     * @param Decimal $amount
     */
    public function __construct(
        protected User $user,
        protected CryptoBalance $balance,
        protected string $to,
        protected Decimal $amount,
    )
    {
    }
}
