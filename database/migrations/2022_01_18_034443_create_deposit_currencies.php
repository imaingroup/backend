<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * CreateDepositCurrencies.
 */
final class CreateDepositCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('deposit_currencies', function (Blueprint $table) {
            $table->primary('crypto_currencies_id');
            $table->unsignedBigInteger('crypto_currencies_id');
            $table->foreign('crypto_currencies_id', 'deposit_currencies_crypto_currencies_id_foreign')
                ->references('id')
                ->on('crypto_currencies')
                ->cascadeOnDelete();
            $table->float('deposit_min');
            $table->boolean('deposit_block');
            $table->unsignedInteger('confirmations_min');
            $table->float('luft_deposit');
            $table->float('luft_withdraw');
            $table->boolean('custom_rate');
            $table->string('name');
            $table->string('full_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('deposit_currencies');
    }
}
