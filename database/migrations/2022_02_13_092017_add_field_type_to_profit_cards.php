<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * AddFieldTypeToProfitCards.
 */
final class AddFieldTypeToProfitCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('profit_cards', function (Blueprint $table) {
            $table->enum('type', ['trading', 'expanded'])
                ->after('image')
                ->default('trading');
        });

        DB::transaction(function () {
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => DB::table('data_types')->where('name', 'profit_cards')->first()->id,
                    'field' => 'type',
                    'type' => 'select_dropdown',
                    'display_name' => 'Type',
                    'required' => 1,
                    'browse' => 0,
                    'read' => 1,
                    'edit' => 1,
                    'add' => 1,
                    'delete' => 1,
                    'details' => '{"validation":{"rule":"required"},"options":{"trading":"Trading","expanded":"Expanded"}}',
                    'order' => 3
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('profit_cards', function (Blueprint $table) {
            $table->dropColumn('type');
        });

        DB::transaction(function () {
            DB::table('data_rows')
                ->where([
                    'data_type_id' => DB::table('data_types')->where('name', 'profit_cards')->first()->id,
                    'field' => 'type',
                ])
                ->delete();
        });
    }
}
