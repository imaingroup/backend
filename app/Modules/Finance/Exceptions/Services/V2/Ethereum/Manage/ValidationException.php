<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage;

use App\Modules\Finance\Exceptions\FinanceException;

/**
 * ValidationException.
 */
final class ValidationException extends FinanceException
{

}
