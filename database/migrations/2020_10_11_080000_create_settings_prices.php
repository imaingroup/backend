<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_prices', function (Blueprint $table) {
            $table->id();
            $table->float('abc_price', 16, 8);
            $table->float('luft_btc_price', 16, 8);
            $table->float('referral_commission', 16, 8);
            $table->float('promo_code_bonus', 16, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_prices');
    }
}
