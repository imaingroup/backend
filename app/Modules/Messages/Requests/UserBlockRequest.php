<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation block user request.
 */
final class UserBlockRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param MessageService $messageService
     */
    public function __construct(private MessageService $messageService)
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            $this->messageService->checkAccessBlockUser($user, $this->blockedUser);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'contact' => 'App\Modules\Messages\Models\Contact',
        'blockedUser' => 'App\Modules\Users\Models\User',
    ])]
    public function validated(): array
    {
        $data = [];
        $data['user'] = Auth::user();
        $data['blockedUser'] = $this->blockedUser;

        return $data;
    }
}
