<?php


namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsPrice;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Settings abc repository.
 */
final class SettingsPriceRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param SettingsPrice $model
     */
    public function __construct(protected SettingsPrice $model)
    {}

    /**
     * Get settings model.
     *
     * @return SettingsPrice
     */
    public function get(): SettingsPrice
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
