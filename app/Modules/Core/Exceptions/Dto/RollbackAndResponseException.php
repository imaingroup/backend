<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Dto;

use Exception;

/**
 * RollbackAndResponseException
 */
class RollbackAndResponseException extends Exception
{
}

