<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services;

use App\Modules\Finance\Enum\NetworkTypeEnum;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Models\CryptoBalance;
use App\Modules\Finance\Repositories\CryptoAddressRepository;
use App\Modules\Finance\Repositories\CryptoBalanceRepository;

/**
 * Finance service.
 */
final class FinanceService
{
    /**
     * Get crypto address.
     *
     * @param string $address
     * @return CryptoAddress|null
     */
    public function getCryptoAddress(string $address): ?CryptoAddress
    {
        return $this->getCryptoAddressRepository()
            ->getByAddress($address);
    }

    /**
     * Find crypto balance.
     *
     * @param int $id
     * @return CryptoBalance|null
     */
    public function findCryptoBalance(int $id): ?CryptoBalance
    {
        return $this->getCryptoBalanceRepository()
            ->find($id);
    }

    /**
     * Get summary USD balance of ethereum type.
     *
     * @return float
     */
    public function getSummaryUSDBalanceOfEthereumType(): float
    {
        $balance = 0;

        $this->getCryptoAddressRepository()
            ->getWithBalancesByNetworkType(NetworkTypeEnum::ETHEREUM)
            ->each(function (CryptoAddress $ca) use (&$balance) {
                $balance += $ca->summaryUSDBalance;
            });

        return $balance;
    }

    /**
     * Get {@see CryptoAddressRepository::class}.
     *
     * @return CryptoAddressRepository
     */
    private function getCryptoAddressRepository(): CryptoAddressRepository
    {
        return app(CryptoAddressRepository::class);
    }

    /**
     * Get {@see CryptoBalanceRepository::class}.
     *
     * @return CryptoBalanceRepository
     */
    private function getCryptoBalanceRepository(): CryptoBalanceRepository
    {
        return app(CryptoBalanceRepository::class);
    }
}
