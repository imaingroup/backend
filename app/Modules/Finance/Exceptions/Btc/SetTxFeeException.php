<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * SetTxFeeException.
 */
final class SetTxFeeException extends BtcException
{
}
