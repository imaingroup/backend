<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * Обновить типы коинов а админке.
 */
class UpdateCoinTypesInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::transaction(function() {
            DB::table('data_rows')
                ->where('data_type_id', DB::table('data_types')
                    ->where('slug', 'deposits')
                    ->first()
                    ->id)
                ->where('field', 'type_coin')
                ->update([
                    'details' => '{"validation":{"rule":"required|min:1|max:5"},"options":{"1":"BTC","2":"ETH","5":"USDT"}}',
                ]);

            DB::table('data_rows')
                ->where('data_type_id', DB::table('data_types')
                    ->where('slug', 'withdraws')
                    ->first()
                    ->id)
                ->where('field', 'type_coin')
                ->update([
                    'type' => 'select_dropdown',
                    'details' => '{"validation":{"rule":"required|min:1|max:5"},"options":{"1":"BTC","2":"ETH","5":"USDT"}}',
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::transaction(function() {
            DB::table('data_rows')
                ->where('data_type_id', DB::table('data_types')
                    ->where('slug', 'deposits')
                    ->first()
                    ->id)
                ->where('field', 'type_coin')
                ->update([
                    'details' => '{"validation":{"rule":"required|min:1|max:2"},"options":{"1":"BTC","2":"ETH"}}',
                ]);

            DB::table('data_rows')
                ->where('data_type_id', DB::table('data_types')
                    ->where('slug', 'withdraws')
                    ->first()
                    ->id)
                ->where('field', 'type_coin')
                ->update([
                    'type' => 'number',
                    'details' => '{"validation":{"rule":"required|min:1|max:2"},"options":{"1":"BTC","2":"ETH"}}',
                ]);
        });
    }
}
