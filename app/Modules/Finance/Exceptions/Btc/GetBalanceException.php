<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * GetBalanceException.
 */
final class GetBalanceException extends BtcException
{
}
