<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions;

use Exception;

/**
 * This exception may be throws in any service,
 * which required transactionLevel 0.
 */
final class TransactionAlreadyStartedException extends Exception
{
}
