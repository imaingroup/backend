<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates operation_logs,tasks tables.
 * Adds tasks_id field to transactions.
 */
final class AddTableTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('operation_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('type')->index();
            $table->unsignedBigInteger('users_id');
            $table->text('data');
            $table->text('description');
            $table->timestamps();

            $table->foreign('users_id')
                ->references('id')
                ->on('users');
        });

        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('type')->index();
            $table->unsignedTinyInteger('status')->index();
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('operation_logs_id')->nullable(true);
            $table->text('data');
            $table->timestamps();

            $table->foreign('operation_logs_id')
                ->references('id')
                ->on('operation_logs')
                ->onDelete('CASCADE');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });

        Schema::table('operation_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('tasks_id')
                ->after('description')
                ->nullable(true);

            $table->foreign('tasks_id')
                ->references('id')
                ->on('tasks')
                ->onDelete('CASCADE');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->unsignedBigInteger('tasks_id')->nullable(true);

            $table->foreign('tasks_id')
                ->references('id')
                ->on('tasks')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('operation_logs', function (Blueprint $table) {
            $table->dropConstrainedForeignId('users_id');
            $table->dropConstrainedForeignId('tasks_id');
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->dropConstrainedForeignId('operation_logs_id');
            $table->dropConstrainedForeignId('users_id');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->dropConstrainedForeignId('tasks_id');
        });

        Schema::dropIfExists('operation_logs');
        Schema::dropIfExists('tasks');
    }
}
