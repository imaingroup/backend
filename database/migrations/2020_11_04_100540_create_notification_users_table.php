<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds notifications_users table.
 */
final class CreateNotificationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('notifications_id');
            $table->unsignedBigInteger('users_id');
            $table->boolean('deleted')->default(false);
            $table->timestamps();

            $table->foreign('notifications_id')
                ->references('id')
                ->on('notifications')
                ->onDelete('CASCADE');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->unique(['notifications_id', 'users_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications_users');
    }
}
