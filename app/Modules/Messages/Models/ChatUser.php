<?php
declare(strict_types=1);

namespace App\Modules\Messages\Models;

use App\Modules\Users\Models\User;
use App\Modules\Core\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Chat user model.
 */
final class ChatUser extends Model
{
    /** @var int Limit users in group chat. */
    public const LIMIT_USERS_IN_GROUP_CHAT = 100;

    /** {@inheritdoc} */
    protected $table = 'messages_chats_users';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_owner' => 'boolean',
        'deleted' => 'boolean',
    ];

    /**
     * Events for model.
     */
    protected static function booted(): void
    {
        ChatUser::creating(function (ChatUser $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function getRouteKeyName(): string
    {
        return 'sid';
    }

    /**
     * Relation with User.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(\App\Modules\Users\Models\User::class,'users_id');
    }

    /**
     * Check chat.
     *
     * @param Chat $chat
     * @return bool
     */
    public function checkChat(Chat $chat): bool
    {
        return $this->messages_chats_id === $chat->id;
    }
}
