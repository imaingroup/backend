<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add order to deposit_currencies..
 */
final class AddOrderToDepositCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('deposit_currencies', function (Blueprint $table) {
            $table->integer('order')
                ->after('full_name')
                ->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('deposit_currencies', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
