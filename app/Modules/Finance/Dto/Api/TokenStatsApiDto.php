<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use DateTime;

/**
 * TokenStatsApiDto.
 *
 * @property-read int $count
 * @property-read int|null $senders
 * @property-read int|null $receivers
 * @property-read int|null $days
 * @property-read DateTime|null $fromDate
 * @property-read DateTime|null $tillDate
 * @property-read float|null $amount
 */
final class TokenStatsApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $count
     * @param int|null $senders
     * @param int|null $receivers
     * @param int|null $days
     * @param DateTime|null $fromDate
     * @param DateTime|null $tillDate
     * @param float|null $amount
     */
    public function __construct(
        protected int $count,
        protected ?int $senders,
        protected ?int $receivers,
        protected ?int $days,
        protected ?DateTime $fromDate,
        protected ?DateTime $tillDate,
        protected ?float $amount,
    )
    {
    }
}
