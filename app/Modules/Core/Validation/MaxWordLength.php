<?php
declare(strict_types=1);

namespace App\Modules\Core\Validation;

use Illuminate\Contracts\Validation\Validator;
use function str_replace;

/**
 * MaxWordLength validator.
 */
class MaxWordLength
{
    public const NAME = 'max_word_length';

    /**
     * Validate.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        if (!isset($parameters[0])) {
            return false;
        }

        $validator->addReplacer(self::NAME, function (string $message, mixed $attribute, mixed $rule, array $parameters) {
            return str_replace(':max', $parameters[0], $message);
        });

        if (!is_numeric($parameters[0]) || !is_scalar($value)) {
            return false;
        }

        $maxWordsLength = (int)$parameters[0];

        $value = str_replace("\n", ' ', removeUrlsFromText((string)$value));

        foreach (explode(' ', $value) as $word) {
            if (mb_strlen($word) > $maxWordsLength) {
                return false;
            }
        }

        return true;
    }
}
