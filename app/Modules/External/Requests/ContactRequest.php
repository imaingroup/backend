<?php
declare(strict_types=1);

namespace App\Modules\External\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This request class need for validating data from contact form.
 */
final class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'first_name' => "string",
        'last_name' => "string",
        'subject' => "string",
        'email' => "string",
        'message' => "string"
    ])]
    public function rules(): array
    {
        return [
            'first_name' => 'required|scalar|max:255',
            'last_name' => 'required|scalar|max:255',
            'subject' => 'required|scalar|max:255',
            'email' => 'required|scalar|email:rfc,dns|max:255',
            'message' => 'required|scalar|max:5000'
        ];
    }
}
