<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use Illuminate\Support\Collection;

/**
 * DepositDataApiDto.
 *
 * @property-read Collection $currencies
 * @property-read bool $enabledDeposits
 * @property-read bool $activeDeposits
 * @property-read bool $activeNewDeposit
 * @property-read DepositInvoiceApiDto|null $invoice
 */
final class DepositDataApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param Collection $currencies
     * @param bool $enabledDeposits
     * @param bool $activeDeposits
     * @param bool $activeNewDeposit
     * @param DepositInvoiceApiDto|null $invoice
     */
    public function __construct(
        protected Collection            $currencies,
        protected bool                  $enabledDeposits,
        protected bool                  $activeDeposits,
        protected bool                  $activeNewDeposit,
        protected ?DepositInvoiceApiDto $invoice,
    ) {

    }
}
