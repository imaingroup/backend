<?php
declare(strict_types=1);

namespace App\Modules\Security\Dto\Api\Identifications;

use App\Modules\Core\Dto\BaseDto;
use DateTime;

/**
 * RelatedUserApiDto.
 *
 * @property-read int $id
 * @property-read string $username
 * @property-read string $email
 * @property-read bool $isAccurate
 * @property-read string $createdAt
 * @property-read string|null $lastLoginAt
 */
final class RelatedUserApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $id
     * @param string $username
     * @param string $email
     * @param bool $isAccurate
     * @param string $createdAt
     * @param string|null $lastLoginAt
     */
    public function __construct(
        protected int       $id,
        protected string    $username,
        protected string    $email,
        protected bool      $isAccurate,
        protected string  $createdAt,
        protected ?string $lastLoginAt,
    )
    {
    }
}
