<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Models;

use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Money\Models\Transaction;
use App\Modules\Users\Models\User;
use App\Modules\Core\Models\Model;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Withdraw\Dto\Api\WithdrawPublicApiDto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Withdraw model.
 *
 * @property-read int $id
 * @property-read int $users_id
 * @property-read int $crypto_currencies_id
 * @property string $address
 * @property float $amount_usdt
 * @property int|null $type_coin
 * @property float $amount_coin
 * @property float $rate
 * @property int $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property float $frozen_usdt_deposited
 * @property float $frozen_usdt_dividends
 * @property string|null $txid
 * @property Carbon|null $confirmed_at
 * @property Carbon|null $declined_at
 * @property-read int $related_transactions_id
 * @property bool $processed
 * @property Carbon|null $processed_at
 * @property-read User $user
 * @property-read CryptoCurrency|null $currency
 */
final class Withdraw extends Model
{
    const STATUS_PENDING = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_DECLINED = 3;

    const STATUSES = [
        self::STATUS_PENDING => 'PENDING',
        self::STATUS_CONFIRMED => 'CONFIRMED',
        self::STATUS_DECLINED => 'DECLINED'
    ];

    CONST MINIMUM_AMOUNT_USDT = 20;

    const TYPE_BTC = CryptoAddress::TYPE_BTC;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'users_id' => 'integer',
        'crypto_currencies_id' => 'integer',
        'address' => 'string',
        'amount_usdt' => 'float',
        'type_coin' => 'integer',
        'amount_coin' => 'float',
        'rate' => 'float',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'frozen_usdt_deposited' => 'float',
        'frozen_usdt_dividends' => 'float',
        'txid' => 'string',
        'confirmed_at' => 'datetime',
        'declined_at' => 'datetime',
        'related_transactions_id' => 'integer',
        'processed' => 'boolean',
        'processed_at' => 'datetime',
    ];

    /**
     * Get status in text representation.
     *
     * @return string|null
     */
    public function statusText(): ?string
    {
        return self::STATUSES[$this->status] ?? null;
    }

    /**
     * Get relation with User.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Get relation with {@see CryptoCurrency::class}.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_currencies_id');
    }

    /**
     * Get relation with Transaction.
     *
     * @return BelongsTo
     */
    public function transaction(): BelongsTo
    {
        return $this->belongsTo(Transaction::class, 'related_transactions_id');
    }

    /**
     * To public api dto.
     *
     * @return WithdrawPublicApiDto
     */
    public function toPublicApiDto(): WithdrawPublicApiDto
    {
        return new WithdrawPublicApiDto(
            $this->created_at,
            $this->amount_usdt,
            $this->amount_coin,
            $this->currency->toApiDto(),
            $this->txid,
            $this->status,
            $this->statusText(),
            $this->currency->depositCurrency ?
                $this->currency->depositCurrency->name :
                $this->currency->symbol,
            $this->currency->depositCurrency ?
                $this->currency->depositCurrency->full_name :
                $this->currency->name,
        );
    }
}
