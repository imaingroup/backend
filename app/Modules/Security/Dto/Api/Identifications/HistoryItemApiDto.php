<?php
declare(strict_types=1);

namespace App\Modules\Security\Dto\Api\Identifications;

use App\Modules\Core\Dto\BaseDto;

/**
 * HistoryItemApiDto.
 *
 * @property-read int $id
 * @property-read bool $isAccurate
 * @property-read int|null $usersId
 * @property-read string|null $userId
 * @property-read int|null $action
 * @property-read string|null $ip
 * @property-read string|null $country
 * @property-read string|null $browser
 * @property-read string|null $browserMajorVersion
 * @property-read string|null $platform
 * @property-read string|null $osVersion
 * @property-read string|null $device
 * @property-read string|null $deviceType
 * @property-read string|null $deviceVendor
 * @property-read string|null $currentResolution
 * @property-read string|null $timeZone
 * @property-read string|null $languages
 * @property-read string|null $createdAt
 */
final class HistoryItemApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $id
     * @param bool $isAccurate
     * @param int|null $usersId
     * @param string|null $userId
     * @param int|null $action
     * @param string|null $ip
     * @param string|null $country
     * @param string|null $browser
     * @param string|null $browserMajorVersion
     * @param string|null $platform
     * @param string|null $osVersion
     * @param string|null $device
     * @param string|null $deviceType
     * @param string|null $deviceVendor
     * @param string|null $currentResolution
     * @param string|null $timeZone
     * @param string|null $languages
     * @param string|null $createdAt
     */
    public function __construct(
        protected int     $id,
        protected bool    $isAccurate,
        protected ?int    $usersId,
        protected ?string $userId,
        protected ?int    $action,
        protected ?string $ip,
        protected ?string $country,
        protected ?string $browser,
        protected ?string $browserMajorVersion,
        protected ?string $platform,
        protected ?string $osVersion,
        protected ?string $device,
        protected ?string $deviceType,
        protected ?string $deviceVendor,
        protected ?string $currentResolution,
        protected ?string $timeZone,
        protected ?string $languages,
        protected ?string $createdAt,
    )
    {

    }

    /**
     * Get key.
     *
     * @return int
     */
    public function getKey(): int
    {
        return $this->id;
    }
}
