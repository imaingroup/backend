<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds fields to settings_bitcoin
 */
class AddFieldsToSettingsBitcoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_bitcoin', function (Blueprint $table) {
            $table->float('min_commission', 5)->default(0);
            $table->float('max_commission', 5)->default(0);
            $table->float('reserve', 5, 2)->default(0);
            $table->boolean('locked_withdraw')->default(false);
            $table->dateTime('locked_withdraw_at')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_bitcoin', function (Blueprint $table) {
            $table->dropColumn('min_commission');
            $table->dropColumn('max_commission');
            $table->dropColumn('reserve');
            $table->dropColumn('locked_withdraw');
            $table->dropColumn('locked_withdraw_at');
        });
    }
}
