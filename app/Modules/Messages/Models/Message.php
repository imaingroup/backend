<?php
declare(strict_types=1);


namespace App\Modules\Messages\Models;

use App\Modules\Users\Models\User;
use App\Modules\Core\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class Message extends Model
{
    const MESSAGES_LIMIT_IN_CHAT = 100; //limit for get messages in chat.
    const SEARCH_LIMIT = 10;

    const SELECT_TYPE_OLDER = 0;
    const SELECT_TYPE_NEWEST = 1;

    const ORDER_ASC = 'ASC';
    const ORDER_DESC = 'DESC';

    const ORDERS = [
        self::ORDER_ASC,
        self::ORDER_DESC,
    ];

    protected $table = 'messages';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sid' => 'integer',
        'is_group' => 'deleted',
        'message' => 'string',
        'author_users_id' => 'integer',
        'messages_chats_id' => 'integer',
        'parent_id' => 'integer',
        'deleted' => 'boolean',
        'viewed_admin' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * {@inheritDoc}
     */
    public function getRouteKeyName(): string
    {
        return 'sid';
    }

    /**
     * Events for model.
     */
    protected static function booted(): void
    {
        Message::creating(function (Message $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }

    /**
     * Relation with Chat.
     *
     * @return BelongsTo
     */
    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class, 'messages_chats_id');
    }

    /**
     * Relation with User.
     *
     * @return BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(\App\Modules\Users\Models\User::class, 'author_users_id');
    }

    /**
     * Relation with parent Message.
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Message::class, 'parent_id');
    }

    /**
     * Relation with MessageRead.
     *
     * @return HasMany
     */
    public function readMasks(): HasMany
    {
        return $this->hasMany(MessageRead::class, 'messages_id');
    }

    /**
     * Relation with MessageRead.
     *
     * @return HasMany
     */
    public function deletedModels(): HasMany
    {
        return $this->hasMany(MessageDeleted::class, 'messages_id');
    }

    /**
     * Check that this message has read mark for this user.
     *
     * @param User $user
     * @return bool
     */
    public function checkReadUser(\App\Modules\Users\Models\User $user): bool
    {
        return $this->readMasks
                ->where('users_id', $user->id)
                ->first() !== null;
    }

    /**
     * Check that this message was read other user.
     *
     * @return bool
     */
    public function checkRead(): bool
    {
        return $this->readMasks
                ->where('users_id', '!=', $this->author_users_id)
                ->first() !== null;
    }

    /**
     * Check that this user is author this message.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return bool
     */
    public function isAuthor(\App\Modules\Users\Models\User $user): bool
    {
        return $this->author_users_id === $user->id;
    }

    /**
     * Get author username attribute.
     *
     * @return string
     */
    public function getAuthorUsernameAttribute(): string
    {
        return $this->attributes['author_username'] ?? $this->author->username;;
    }

    /**
     * Get author avatar attribute.
     *
     * @return string
     */
    public function getAuthorAvatarAttribute(): string
    {
        return $this->attributes['author_avatar'] ?? $this->author->avatar;
    }

    /**
     * Check chat.
     *
     * @param Chat $chat
     * @return bool
     */
    public function checkChat(Chat $chat): bool
    {
        return $chat->id === $this->messages_chats_id;
    }

    /**
     * This message is same chat.
     *
     * @param Message $message
     * @return bool
     */
    public function isSameChat(Message $message): bool
    {
        return $this->messages_chats_id === $message->messages_chats_id;
    }

    /**
     * Check deleted message.
     *
     * @param User $user
     * @return bool
     */
    public function checkDeleted(User $user): bool
    {
        return $this->deleted || $this->deletedModels
            ->where('users_id', $user->id)
            ->count() > 0;
    }
}
