<?php

use App\Modules\Users\Models\User;
use App\Modules\Access\Services\RegisterService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration adds default contacts for all users.
 */
final class AddDefaultContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        $registerService = app(RegisterService::class);

        foreach (User::all() as $user) {
            $registerService->addDefaultContacts($user);
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();

        DB::table('messages_contacts')
            ->delete();

        DB::commit();
    }
}
