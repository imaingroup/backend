<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Exceptions;

/**
 * UnsupportedCurrencyException.
 */
final class UnsupportedCurrencyException extends DepositException
{
}
