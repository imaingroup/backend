<?php
declare(strict_types=1);

namespace App\Modules\Account\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Account\Requests\AccountCheckUpdateRequest;
use App\Modules\Account\Requests\AccountGlobalRequest;
use App\Modules\Account\Requests\ClearNotificationsRequest;
use App\Modules\Account\Requests\ViewNotificationRequest;
use App\Modules\Account\Services\AccountService;
use App\Modules\Core\Exceptions\DataException;
use App\Modules\Core\Services\DataService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * Account controller.
 */
final class AccountController extends Controller
{
    /** @var AccountService Account service. */
    private AccountService $accountService;

    /** @var DataService Data service. */
    private DataService $dataService;

    /**
     * Construct.
     *
     * @param AccountService $accountService
     * @param DataService $dataService
     */
    public function __construct(AccountService $accountService, DataService $dataService)
    {
        $this->accountService = $accountService;
        $this->dataService = $dataService;
    }

    /**
     * Get global content.
     *
     * @param AccountGlobalRequest $request
     * @return array
     * @throws DataException
     */
    public function global(AccountGlobalRequest $request): array
    {
        return $this->dataService->getData(DataService::TYPE_ACCOUNT_GLOBAL, [Auth::user()]);
    }

    /**
     * View notification.
     *
     * @param ViewNotificationRequest $request
     * @return array
     */
    public function viewNotification(ViewNotificationRequest $request): array
    {
        $this->accountService->viewNotification(Auth::user(), $request->validated()['notification']);

        return ['status' => true];
    }

    /**
     * Clear notifications.
     *
     * @param ClearNotificationsRequest $request
     * @return array
     */
    public function clearNotifications(ClearNotificationsRequest $request): array
    {
        $this->accountService->clearNotifications(Auth::user(), $request['notification_id']);

        return ['status' => true];
    }

    /**
     * Check that user authenticated.
     *
     * @return array
     */
    public function checkAuth(): array
    {
        return [
            'data' => [
                'authenticated' => Auth::check()
            ]
        ];
    }

    /**
     * Check update by rules.
     *
     * @param AccountCheckUpdateRequest $request
     * @return Response|array
     * @throws DataException
     */
    public function checkUpdate(AccountCheckUpdateRequest $request): Response|array
    {
        return $this->dataService
            ->getUpdatedData(...$request->validated());
    }
}
