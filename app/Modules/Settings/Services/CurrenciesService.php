<?php
declare(strict_types=1);

namespace App\Modules\Settings\Services;

use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Finance\Services\V2\DataFetcherService;
use App\Modules\Settings\Models\DepositCurrency;
use App\Modules\Settings\Repositories\DepositCurrencyRepository;
use Illuminate\Support\Collection;

/**
 * CurrenciesService.
 */
final class CurrenciesService
{
    /**
     * Set currency as approved for deposits.
     *
     * @param CryptoCurrency $currency
     * @param float $depositMin
     * @param bool $depositBlock
     * @param int $confirmationsMin
     * @param float $luftDeposit
     * @param float $luftWithdraw
     * @param float|null $rate
     * @param bool $customRate
     * @param string $name
     * @param string $fullName
     * @param int $order
     * @return DepositCurrency|null
     */
    public function setCurrency(
        CryptoCurrency $currency,
        float          $depositMin,
        bool           $depositBlock,
        int            $confirmationsMin,
        float          $luftDeposit,
        float          $luftWithdraw,
        ?float         $rate,
        bool           $customRate,
        string         $name,
        string         $fullName,
        int            $order,
    )
    {
        $depositCurrency = $this->getDepositCurrencyRepository()
            ->findByCryptoCurrency($currency->id);

        if (!$depositCurrency) {
            $depositCurrency = new DepositCurrency();
            $depositCurrency->currency()
                ->associate($currency);
        }

        $depositCurrency->deposit_min = $depositMin;
        $depositCurrency->deposit_block = $depositBlock;
        $depositCurrency->confirmations_min = $confirmationsMin;
        $depositCurrency->luft_deposit = $luftDeposit;
        $depositCurrency->luft_withdraw = $luftWithdraw;
        $depositCurrency->custom_rate = $customRate;
        $depositCurrency->name = $name;
        $depositCurrency->full_name = $fullName;
        $depositCurrency->order = $order;

        $depositCurrency->save();

        if ($customRate && $rate) {
            $this->getDataFetcherService()
                ->setCurrencyMarket($currency, $rate);
        }

        return $depositCurrency;
    }

    /**
     * Delete currency.
     *
     * @param DepositCurrency $currency
     * @return void
     */
    public function deleteCurrency(
        DepositCurrency $currency
    )
    {
        $currency->delete();
    }

    /**
     * Get currencies.
     *
     * @return Collection
     */
    public function getCurrencies(): Collection
    {
        return $this->getDepositCurrencyRepository()
            ->get();
    }

    /**
     * Find by id.
     *
     * @param int $id
     * @return DepositCurrency|null
     */
    public function find(int $id): ?DepositCurrency
    {
        return $this->getDepositCurrencyRepository()
            ->find($id);
    }

    /**
     * Get {@see DepositCurrencyRepository::class}.
     *
     * @return DepositCurrencyRepository
     */
    private function getDepositCurrencyRepository(): DepositCurrencyRepository
    {
        return app(DepositCurrencyRepository::class);
    }

    /**
     * Get {@see DataFetcherService::class}.
     *
     * @return DataFetcherService
     */
    private function getDataFetcherService(): DataFetcherService
    {
        return app(DataFetcherService::class);
    }
}
