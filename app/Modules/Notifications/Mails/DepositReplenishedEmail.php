<?php

namespace App\Modules\Notifications\Mails;

use App\Modules\Deposites\Models\DepositTransaction;
use App\Modules\Notifications\Mails\AbstractEmail;
use App\Modules\Notifications\Models\Mail;
use function protectFromXss;

/**
 * Deposit replenished mail class.
 */
final class DepositReplenishedEmail extends AbstractEmail
{
    /** @var \App\Modules\Deposites\Models\DepositTransaction Deposit transaction. */
    private DepositTransaction $depositTransaction;

    /**
     * Create a new mail instance.
     *
     * @param \App\Modules\Deposites\Models\DepositTransaction $depositTransaction
     */
    public function __construct(DepositTransaction $depositTransaction)
    {
        $this->depositTransaction = $depositTransaction;
    }

    /**
     * {@inheritdoc}
     */
    protected function getVariables(): array
    {
        $user = $this->depositTransaction->deposit->user;

        return [
            '{username}' => protectFromXss($user->username),
            '{amount}' => $this->depositTransaction->amount_usdt,
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getMailId(): int
    {
        return Mail::MAIL_DEPOSIT_REPLENISHED;
    }
}
