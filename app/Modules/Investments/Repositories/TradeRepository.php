<?php


namespace App\Modules\Investments\Repositories;

use App\Modules\Investments\Models\Trade;
use App\Modules\Core\Repositories\BaseRepository;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

/**
 * Trade repository.
 */
final class TradeRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Investments\Models\Trade $model
     */
    public function __construct(protected Trade $model)
    {}

    /**
     * Get count opened tid for user and option.
     *
     * @param int $userId
     * @param int $optionId
     * @return int
     */
    public function countOpenedTidByOptionId(int $userId, int $optionId): int
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $userId)
            ->where('trade_options_id', $optionId)
            ->where('is_open', true)
            ->count();
    }

    /**
     * Get all opening.
     *
     * @return Collection
     */
    public function getAllOpening(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('is_open', true)
            ->get();
    }

    /**
     * Count all opening.
     *
     * @param int $userId
     * @return int
     */
    public function countAllOpeningByUser(int $userId): int
    {
        return $this->model
            ->newQuery()
            ->where('is_open', true)
            ->where('users_id', $userId)
            ->count();
    }

    /**
     * Get all.
     *
     * @param int $userId
     * @param bool|null $isOpen
     * @return Collection
     */
    public function getByUser(int $userId, bool $isOpen = null): Collection
    {
        $query = $this->model
            ->newQuery()
            ->where('users_id', $userId);

        if ($isOpen !== null) {
            $query->where('is_open', $isOpen);
        }

        return $query->get();
    }

    /**
     * Get pnl sum by selected trades.
     *
     * @param array $tradeIds
     * @param DateTime|null $beforeAt
     * @return array
     */
    public function getPnlSumByTrades(array $tradeIds, DateTime $beforeAt = null): array
    {
        if (count($tradeIds) === 0) {
            return [];
        }

        $subQuery = $this->model
            ->newQuery()
            ->select(DB::raw('SUM(th.profit_usdt)'))
            ->from('trade_history', 'th')
            ->whereColumn('th.trades_id', 't.id');

        if ($beforeAt) {
            $subQuery->where('th.date', '<', $beforeAt->format('Y-m-d'));
        }

        $query = $this->model
            ->newQuery()
            ->select('t.tid')
            ->addSelect(['sum' => $subQuery])
            ->from('trades', 't')
            ->whereIn('t.id', $tradeIds);

        return $query
            ->get()
            ->pluck('sum', 'tid')
            ->map(function ($item, $key) use ($beforeAt) {
                return $item === null || !$beforeAt ? 0 : $item;
            })
            ->toArray();
    }

    /**
     * Get pnl sum with every options.
     *
     * @param int $userId
     * @param DateTime|null $beforeAt
     * @return array
     */
    public function getPnlSumWithOptions(int $userId, DateTime $beforeAt = null): array
    {
        $subQuery = $this->model
            ->newQuery()
            ->select(DB::raw('SUM(th.profit_usdt)'))
            ->from('trades', 't')
            ->leftJoin('trade_history as th', function (JoinClause $query) {
                $query->whereColumn('t.id', 'th.trades_id');
            })
            ->where('t.users_id', $userId)
            ->whereColumn('t.trade_options_id', 'to.id');

        if ($beforeAt !== null) {
            $subQuery->where('th.date', '<', $beforeAt->format('Y-m-d'));
        }

        $query = $this->model
            ->newQuery()
            ->from('trade_options', 'to')
            ->select('to.id')
            ->where('to.is_visible', true)
            ->orderBy('to.id', 'ASC')
            ->addSelect(['sum' => $subQuery]);

        return $query->get()
            ->pluck('sum', 'id')
            ->map(function ($item, $key) use ($beforeAt) {
                return $item === null || !$beforeAt ? 0 : $item;
            })
            ->toArray();
    }
}
