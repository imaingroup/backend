<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions;

/**
 * SecureCryptRequestParamsException.
 */
final class SecureCryptRequestParamsException extends FinanceException
{
}
