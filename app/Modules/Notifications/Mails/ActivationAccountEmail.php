<?php

namespace App\Modules\Notifications\Mails;

use App\Modules\Notifications\Mails\AbstractEmail;
use App\Modules\Notifications\Models\Mail;
use App\Modules\Users\Models\User;
use function protectFromXss;

/**
 * Activation account mail class.
 */
final class ActivationAccountEmail extends AbstractEmail
{
    /** @var User User model. */
    private User $user;

    /**
     * Create a new mail instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    protected function getVariables(): array
    {
        return [
            '{link}' => \sprintf('%s#/activate/%s', route('home'), $this->user->email_token),
            '{sitename}' => __('base.site_name'),
            '{username}' => protectFromXss($this->user->username),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getMailId(): int
    {
        return Mail::MAIL_ACTIVATE;
    }
}
