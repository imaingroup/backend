<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api\Finance;

use App\Modules\Core\Dto\Api\TaskApiDto;
use App\Modules\Core\Dto\BaseDto;
use App\Modules\Finance\Dto\Api\TokenApiDto;
use App\Modules\Finance\Enum\NetworkTypeEnum;
use Illuminate\Support\Collection;

/**
 * WalletDto.
 *
 * @property-read NetworkTypeEnum $network
 * @property-read float $balance
 * @property-read TokenApiDto|null $token
 * @property-read Collection $addresses
 * @property-read Collection|TaskApiDto[] $history
 */
final class WalletApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param NetworkTypeEnum $network
     * @param float $balance
     * @param TokenApiDto|null $token
     * @param Collection $addresses
     * @param Collection $history
     */
    public function __construct(
        protected NetworkTypeEnum $network,
        protected float $balance,
        protected ?TokenApiDto $token,
        protected Collection $addresses,
        protected Collection $history,
    )
    {
    }
}
