<?php

namespace App\Modules\Notifications\Jobs;

use App\Modules\Notifications\Mails\DepositReplenishedEmail;
use App\Modules\Notifications\Mails\MailSender;
use App\Modules\Deposites\Models\DepositTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Deposit replenished mail job.
 */
final class DepositReplenishedEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var DepositTransaction Deposit transaction. */
    private DepositTransaction $depositTransaction;

    /**
     * Create a new mail instance.
     *
     * @param \App\Modules\Deposites\Models\DepositTransaction $depositTransaction
     */
    public function __construct(DepositTransaction $depositTransaction)
    {
        $this->depositTransaction = $depositTransaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MailSender::send($this->depositTransaction->deposit->user->email, new DepositReplenishedEmail($this->depositTransaction));
    }
}
