<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Services\BitQuery;

use App\Modules\Finance\Exceptions\FinanceException;

/**
 * RequestException.
 */
class RequestException extends FinanceException
{

}
