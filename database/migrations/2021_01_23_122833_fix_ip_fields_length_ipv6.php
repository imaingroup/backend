<?php

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration fixes ip fields for max length.
 */
final class FixIpFieldsLengthIpv6 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('users_identifications', function (Blueprint $table) {
            $table->string('ip', 45)
                ->nullable(true)
                ->change();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('ip', 45)
                ->nullable(true)
                ->change();

            $table->string('last_ip', 45)
                ->nullable(true)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('users_identifications')
            ->whereRaw('LENGTH(ip) > 15')
            ->update([
                'ip' => DB::raw('SUBSTRING(ip, 1, 15)')
            ]);

        DB::table('users')
            ->whereRaw('LENGTH(ip) > 15')
            ->update([
                'ip' => DB::raw('SUBSTRING(ip, 1, 15)')
            ]);

        DB::table('users')
            ->whereRaw('LENGTH(last_ip) > 15')
            ->update([
                'ip' => DB::raw('SUBSTRING(last_ip, 1, 15)')
            ]);

        Schema::table('users_identifications', function (Blueprint $table) {
            $table->string('ip', 15)
                ->nullable(true)
                ->change();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('ip', 15)
                ->nullable(true)
                ->change();

            $table->string('last_ip', 15)
                ->nullable(true)
                ->change();
        });
    }
}
