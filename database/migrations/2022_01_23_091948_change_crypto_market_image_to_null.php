<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * ChangeCryptoMarketImageToNull.
 */
final class ChangeCryptoMarketImageToNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('crypto_currencies_market', function (Blueprint $table) {
            $table->string('image')
                ->nullable(true)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('crypto_currencies_market', function (Blueprint $table) {
            $table->string('image')
                ->nullable(false)
                ->change();
        });
    }
}
