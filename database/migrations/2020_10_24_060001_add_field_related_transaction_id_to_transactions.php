<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adding field related_transaction_id
 * to table transactions.
 */
class AddFieldRelatedTransactionIdToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->unsignedBigInteger('related_transaction_id')->nullable(true);

            $table->foreign('related_transaction_id')
                ->references('id')
                ->on('transactions')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table transactions drop foreign key transactions_related_transaction_id_foreign;');

        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('related_transaction_id');
        });
    }
}
