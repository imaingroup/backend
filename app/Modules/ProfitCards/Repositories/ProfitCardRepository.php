<?php


namespace App\Modules\ProfitCards\Repositories;

use App\Modules\ProfitCards\Models\ProfitCard;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * Profit card repository.
 */
final class ProfitCardRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param ProfitCard $model
     */
    public function __construct(protected ProfitCard $model)
    {}

    /**
     * Get.
     *
     * @return Collection
     */
    public function get(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('is_visible', true)
            ->orderBy('sort', 'ASC')
            ->get();
    }
}
