<?php

use App\Modules\ProfitCards\Models\ProfitCardBuyHistory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates users_trade_options_rights table
 * for storing rights for opening tid by option.
 */
class CreateUsersTradeOptionsRights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_trade_options_rights', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('trade_options_id');
            $table->timestamps();

            $table->unique(['users_id', 'trade_options_id']);

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->foreign('trade_options_id')
                ->references('id')
                ->on('trade_options')
                ->onDelete('CASCADE');
        });

        $cardHistoryList = ProfitCardBuyHistory::all();

        $tradeOptionsCard = DB::table('trade_options_cards')
            ->get();

        $rights = [];

        foreach ($cardHistoryList as $cardHistory) {
            $options = $tradeOptionsCard->where('profit_card_id', $cardHistory->profit_cards_id);

            if (!isset($rights[$cardHistory->users_id])) {
                $rights[$cardHistory->users_id] = [];
            }

            $rights[$cardHistory->users_id] =
                array_unique(
                    array_merge(
                        $rights[$cardHistory->users_id],
                        $options->pluck('trade_option_id')->toArray()
                    )
                );
        }

        foreach ($rights as $user => $options) {
            foreach ($options as $option) {
                DB::table('users_trade_options_rights')
                    ->insert([
                        'users_id' => $user,
                        'trade_options_id' => $option,
                        'created_at' => new \DateTime(),
                        'updated_at' => new \DateTime()
                    ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_trade_options_rights');
    }
}
