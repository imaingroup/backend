<?php

use App\Modules\Core\Services\ClientInfoService;
use Illuminate\Support\Facades\Storage;

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false): string
    {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        $str_end = "";

        if ($lower_str_end) {
            $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        } else {
            $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }

        $str = $first_letter . $str_end;

        return $str;
    }
}

if (!function_exists('protectFromXss')) {
    function protectFromXss(string $string): string
    {
        return htmlentities(
            strip_tags($string),
            ENT_QUOTES,
            'UTF-8',
            false
        );
    }
};

if (!function_exists('allowedViewGa')) {
    function ipExists(string $ip, array $ips): bool
    {
        $ipData = explode('.', $ip);

        foreach ($ips as $ipItem) {
            if ($ip === $ipItem) {
                return true;
            }

            $ipItemData = explode('.', $ipItem);

            if (count($ipItemData) === 4 &&
                count($ipData) === 4 &&
                $ipItemData[3] === '*'
            ) {
                if (sprintf('%s.%s.%s', $ipData[0], $ipData[1], $ipData[2]) ===
                    sprintf('%s.%s.%s', $ipItemData[0], $ipItemData[1], $ipItemData[2])) {
                    return true;
                }
            }
        }

        return false;
    }
}

if (!function_exists('allowedViewGa')) {
    function allowedViewGa(): bool
    {
        $gaConfig = config('ga');
        $clientIp = app(ClientInfoService::class)->getRealIp();

        return
            $gaConfig['enabled']
            && request()->getHost() === $gaConfig['allowed_domain']
            && !ipExists($clientIp, $gaConfig['disabled_ips']);
    }
}

if (!function_exists('removeUrlsFromText')) {
    function removeUrlsFromText(string $text): string
    {
        return preg_replace('~[a-z]+://\S+~', "", $text);
    }
}

if (!function_exists('writeEthLogs')) {
    function writeEthLogs(array $data): void
    {
        Storage::disk('logs')
            ->append('eth.logs', json_encode($data));
    }
}
