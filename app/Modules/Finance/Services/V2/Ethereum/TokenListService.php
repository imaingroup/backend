<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services\V2\Ethereum;

use App\Modules\Finance\Dto\Api\TokenApiDto;
use App\Modules\Finance\Dto\Api\TokenStatsApiDto;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Exceptions\Services\BitQuery\RequestException;
use App\Modules\Finance\Services\BitQueryService;
use DateTime;
use Illuminate\Support\Collection;
use stdClass;

/**
 * Token list service.
 */
final class TokenListService
{
    /**
     * Get tokens by network.
     *
     * @param EthereumNetworkEnum $network
     * @return Collection
     * @throws RequestException
     */
    public function getTokensByNetwork(EthereumNetworkEnum $network): Collection
    {
        return collect($this->geBitQueryService()
            ->getTopTokens($network, 10000)->data->ethereum->transfers)
            ->map(fn(stdClass $object) => $this->hydrateToken($object, $network));
    }

    /**
     * Get detail token.
     *
     * @param string $address
     * @param EthereumNetworkEnum $network
     * @return TokenApiDto|null
     * @throws RequestException
     */
    public function getDetailToken(string $address, EthereumNetworkEnum $network): ?TokenApiDto
    {
        $transfers = $this->geBitQueryService()
            ->getDetailToken($address, $network)
            ->data
            ->ethereum
            ->transfers;

        return count($transfers) > 0 ? $this->hydrateToken($transfers[0], $network) : null;
    }

    /**
     * Get detail of main currency.
     *
     * @param EthereumNetworkEnum $network
     * @return TokenApiDto|null
     * @throws RequestException
     */
    public function getDetailMainCurrency(EthereumNetworkEnum $network): ?TokenApiDto
    {
        $transfers = $this->geBitQueryService()
            ->getTopTokens($network, 1, true)
            ->data
            ->ethereum
            ->transfers;

        return ($transfers && count($transfers) > 0 && empty($transfers[0]->tokenType)) ?
            $this->hydrateToken($transfers[0], $network) :
            null;
    }

    /**
     * Hydrate token data.
     *
     * @param stdClass $object
     * @param EthereumNetworkEnum $network
     * @return TokenApiDto
     * @throws \Exception
     */
    private function hydrateToken(stdClass $object, EthereumNetworkEnum $network): TokenApiDto
    {
        return new TokenApiDto(
            $object->currency->decimals,
            $object->currency->name,
            $object->currency->symbol,
            $object->currency->address !== '-' ? $object->currency->address : null,
            empty($object->currency->tokenType) ? null : $object->currency->tokenType,
            null,
            $network,
            new TokenStatsApiDto(
                $object->count,
                $object->senders ?? null,
                $object->receivers ?? null,
                $object->days ?? null,
                isset($object->from_date) ? new DateTime($object->from_date) : null,
                isset($object->till_date) ? new DateTime($object->till_date) : null,
                $object->amount ?? null,
            ),
            null,
        );
    }

    /**
     * Get {@see BitQueryService::class}.
     *
     * @return BitQueryService
     */
    private function geBitQueryService(): BitQueryService
    {
        return app(BitQueryService::class);
    }
}
