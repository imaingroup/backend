<?php
declare(strict_types=1);

namespace App\Modules\Access\Requests;

use App\Modules\Security\Models\UserIdentification;
use App\Modules\Security\Services\Identification\IdentificationService;
use App\Modules\Users\Repositories\UserRepository;
use App\Modules\Users\Services\UserService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;

/**
 * This request class need for validating data from forgot password form.
 */
final class ForgotPasswordRequest extends FormRequest
{
    /** @var int Resend requests limit in minutes. */
    private int $resendLimitMinutes = 30;

    /**
     * Constructor.
     *
     * @param \App\Modules\Users\Repositories\UserRepository $userRepository
     * @param \App\Modules\Users\Services\UserService $userService
     * @param \App\Modules\Security\Services\Identification\IdentificationService $identificationService
     */
    public function __construct(
        private UserRepository $userRepository,
        private UserService $userService,
        private IdentificationService $identificationService
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $recaptchaEnabled = (int)env('GOOGLE_RECAPTCHA_ENABLED') === 1;
        $data = [];

        if ($recaptchaEnabled) {
            $data['g-recaptcha-response'] = 'required|scalar|recaptcha';
        }

        return array_merge($data, [
            'email' => 'required|scalar|exists:users,email',
            '2fa' => 'nullable|scalar'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function validated(): array
    {
        $data = parent::validated();
        $data['user'] = $this->userRepository->findByEmail((string)$this['email']);

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws SecretKeyTooShortException
     */
    private function after(Validator $validator): void
    {
        if ($this->session()->has('forgot_password_request_at')) {
            $forgotPasswordRequestAt = clone $this->session()->get('forgot_password_request_at');

            if ($forgotPasswordRequestAt->modify(\sprintf('+ %s MINUTES', $this->resendLimitMinutes)) > new \DateTime()) {
                $validator->errors()->add('limit', 'Forgot password requests limit');

                return;
            }
        }

        $user = $this->userRepository->findByEmail((string)$this['email']);

        if (!$user) {
            return;
        }

        if ($user->is_2fa && !$this['2fa']) {
            $validator->errors()->add('2fa', '2FA required');
            return;
        }

        if ($user->is_2fa) {
            $string2fa = trim(str_replace(' ', '', (string)$this['2fa']));

            if (!$this->userService->check2fa($user, $string2fa)) {
                $validator->errors()->add('2fa', 'Incorrect 2FA');
                return;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function failedValidation(Validator $validator): void
    {
        $this->identificationService
            ->confirmLastIdentification(UserIdentification::ACTION_FORGOT_ERROR);

        parent::failedValidation($validator);
    }
}
