<?php

namespace App\Modules\ProfitCards\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\ProfitCards\Requests\ProfitCardBuyRequest;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Users\Models\User;
use App\Modules\ProfitCards\Services\ProfitCardService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

/**
 * Profit card controller.
 */
final class ProfitCardController extends Controller
{
    /** @var ProfitCardService Profit card service. */
    private ProfitCardService $profitCardService;

    /** @var Logger Logger service. */
    private Logger $logger;

    /**
     * Constructor.
     *
     * @param ProfitCardService $profitCardService
     * @param Logger $logger
     */
    public function __construct(ProfitCardService $profitCardService, Logger $logger)
    {
        $this->profitCardService = $profitCardService;
        $this->logger = $logger;
    }

    /**
     * Buy action.
     *
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function buy(): Response
    {
        return $this->inTransaction(function(): void {
            $user = Auth::user();

            User::refreshAndLockForUpdate($user);

            $data = app(ProfitCardBuyRequest::class)->validated();

            $this->profitCardService->buy($data['card'], $user);
        });
    }
}
