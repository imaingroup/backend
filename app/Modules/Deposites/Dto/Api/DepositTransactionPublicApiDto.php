<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Finance\Dto\Api\TokenApiDto;
use DateTime;
use Litipk\BigNumbers\Decimal;

/**
 * DepositHistoryApiDto.
 *
 * @property-read DateTime $transactionAt
 * @property-read string $amountUsdt
 * @property-read string $amountCoins
 * @property-read TokenApiDto $currency
 * @property-read string $txid
 * @property-read int $status
 * @property-read string $statusReadable
 * @property-read string $assetName
 * @property-read string $assetFullName
 */
final class DepositTransactionPublicApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param DateTime $transactionAt
     * @param string $amountUsdt
     * @param string $amountCoins
     * @param TokenApiDto $currency
     * @param string $txid
     * @param int $status
     * @param string $statusReadable
     * @param string $assetName
     * @param string $assetFullName
     */
    public function __construct(
        protected DateTime $transactionAt,
        protected string $amountUsdt,
        protected string $amountCoins,
        protected TokenApiDto $currency,
        protected string $txid,
        protected int $status,
        protected string $statusReadable,
        protected string $assetName,
        protected string $assetFullName,
    ) {

    }
}
