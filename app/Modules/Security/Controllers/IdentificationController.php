<?php
declare(strict_types=1);

namespace App\Modules\Security\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Security\Requests\IdentificationSetInfoRequest;
use App\Modules\Security\Services\Identification\IdentificationService;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Identification controller.
 */
final class IdentificationController extends Controller
{
    /**
     * Constructor.
     *
     * @param IdentificationService $identificationService
     */
    public function __construct(private IdentificationService $identificationService)
    {
    }

    /**
     * Set identification info with fingerprints
     * from browser.
     *
     * @param IdentificationSetInfoRequest $request
     * @return array
     */
    #[ArrayShape(['status' => "bool"])]
    public function setInfo(IdentificationSetInfoRequest $request): array
    {
        $this->identificationService->saveDate($request->validated());

        return ['status' => true];
    }
}
