<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Services;

use App\Modules\Deposites\Dto\GetQrParams;

/**
 * QR code service generator.
 */
final class QrCodeService
{
    /**
     * Create payment qr code.
     *
     * @param GetQrParams $dto
     * @return string
     */
    public function createQrCode(GetQrParams $dto): string
    {
        return sprintf(
            'data:image/png;base64,%s',
            base64_encode(
                file_get_contents(
                    sprintf(
                        'https://chart.googleapis.com/chart?chs=%sx%s&cht=qr&chl=%s?amount=%s',
                        $dto->getWidth(),
                        $dto->getHeight(),
                        $dto->getAddress(),
                        number_format($dto->getAmount(), 8, '.', '')
                    )
                )
            )
        );
    }
}
