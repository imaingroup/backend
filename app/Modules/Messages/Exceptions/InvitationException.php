<?php
declare(strict_types=1);

namespace App\Modules\Messages\Exceptions;

/**
 * Invitation exception.
 */
final class InvitationException extends MessageException
{

}
