<?php
declare(strict_types=1);

namespace App\Modules\Security\Jobs;

use App\Modules\Security\Models\UserIdentification;
use App\Modules\Security\Services\Identification\IdentificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * ProcessIdentificationUpdating.
 */
final class ProcessIdentificationUpdating implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Constructor.
     *
     * @param UserIdentification $identification
     */
    public function __construct(protected UserIdentification $identification)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getIdentificationService()
            ->updateUserIdentificationRelations($this->identification);
    }

    /**
     * Get {@see IdentificationService::class}.
     *
     * @return IdentificationService
     */
    private function getIdentificationService(): IdentificationService
    {
        return app(IdentificationService::class);
    }
}
