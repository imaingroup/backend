<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Change to nullable eth_password field
 * in crypto_addresses table.
 */
final class ChangeNullableEthPasswordCryptoAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->text('eth_password')
                ->nullable(true)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->text('eth_password')
                ->nullable(false)
                ->change();
        });
    }
}
