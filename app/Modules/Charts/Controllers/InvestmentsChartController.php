<?php
declare(strict_types=1);

namespace App\Modules\Charts\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Charts\Requests\InvestmentsChartOptionRequest;
use App\Modules\Charts\Requests\InvestmentsChartV2Request;
use App\Modules\Charts\Services\InvestmentsChartService;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Core\Services\DataService;
use Exception;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Investment chart controller.
 */
final class InvestmentsChartController extends Controller
{
    /**
     * Get chart data V2.
     *
     * @param InvestmentsChartV2Request $request
     * @return array
     * @throws Exception
     */
    public function chartV2(InvestmentsChartV2Request $request): array
    {
        return app(DataService::class)->getData(
            DataService::TYPE_INVESTMENTS_CHART,
            array_values($request->validated())
        );
    }

    /**
     * Get chart data V2.
     *
     * @param InvestmentsChartOptionRequest $request
     * @return ResponseDto
     */
    #[ArrayShape(['data' => "array"])]
    public function chartByOption(InvestmentsChartOptionRequest $request): ResponseDto
    {
        return new ResponseDto(
            app(InvestmentsChartService::class)->chartByOption(
                ...$request->validated())
        );
    }
}
