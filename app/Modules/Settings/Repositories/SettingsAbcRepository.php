<?php


namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsAbc;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Settings abc repository.
 */
final class SettingsAbcRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Settings\Models\SettingsAbc $model
     */
    public function __construct(protected SettingsAbc $model)
    {}

    /**
     * Get settings model.
     *
     * @return \App\Modules\Settings\Models\SettingsAbc
     */
    public function get(): SettingsAbc
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
