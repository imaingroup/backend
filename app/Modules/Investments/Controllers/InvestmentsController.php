<?php
declare(strict_types=1);

namespace App\Modules\Investments\Controllers;

use App\Modules\Core\Exceptions\DataException;
use App\Http\Controllers\Controller;
use App\Modules\Investments\Requests\HistoryTIDRequest;
use App\Modules\Investments\Requests\InvestmentsGetBalancesRequest;
use App\Modules\Investments\Requests\InvestmentsOptionsRequest;
use App\Modules\Investments\Requests\OpenTIDRequest;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Users\Models\User;
use App\Modules\Core\Services\DataService;
use App\Modules\Charts\Services\InvestmentsChartService;
use App\Modules\Investments\Services\InvestmentService;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

/**
 * Investment controller.
 */
final class InvestmentsController extends Controller
{
    /**
     * Constructor.
     *
     * @param InvestmentService $investmentService
     * @param DataService $dataService
     */
    public function __construct(
        private InvestmentService $investmentService,
        private DataService $dataService,
    )
    {
    }

    /**
     * Get balances.
     *
     * @param InvestmentsGetBalancesRequest $request
     * @return array
     * @throws DataException
     */
    public function balances(InvestmentsGetBalancesRequest $request): array
    {
        return $this->dataService->getData(DataService::TYPE_INVESTMENTS_BALANCES, [Auth::user()]);
    }

    /**
     * Get investments options.
     *
     * @param InvestmentsOptionsRequest $request
     * @return array
     * @throws DataException
     */
    public function options(InvestmentsOptionsRequest $request): array
    {
        return $this->dataService->getData(DataService::TYPE_INVESTMENTS_OPTIONS, [Auth::user()]);
    }

    /**
     * Open TID.
     *
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function open(): Response
    {
        return $this->inTransaction(function(): void {
            $user = Auth::user();

            User::refreshAndLockForUpdate($user);

            $data = app(OpenTIDRequest::class)->validated();

            $this->investmentService->open($user, $data['option'], $data['period'], $data['amount']);
        });
    }

    /**
     * History.
     *
     * @param \App\Modules\Investments\Requests\HistoryTIDRequest $request
     * @return array
     * @throws Exception
     */
    public function history(HistoryTIDRequest $request): array
    {
        return $this->dataService->getData(DataService::TYPE_INVESTMENTS_HISTORY, [Auth::user()]);
    }
}
