<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds field btc_usdt_rate and rate_updated_at to settings_price.
 */
class AddFieldBtcUsdtPriceToSettingsPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->float('btc_usdt_rate', 12, 4)->after('abc_price')->default(0);
        });

        DB::statement('alter table `settings_prices` add `rate_updated_at` datetime not null default NOW() after `btc_usdt_rate`');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->dropColumn('btc_usdt_rate');
            $table->dropColumn('rate_updated_at');
        });
    }
}
