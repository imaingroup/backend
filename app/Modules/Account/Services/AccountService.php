<?php
declare(strict_types=1);

namespace App\Modules\Account\Services;

use App\Modules\Admin\Exceptions\TranslateException;
use App\Modules\Account\Models\Notification;
use App\Modules\Settings\Repositories\SettingsStylesRepository;
use App\Modules\Users\Models\User;
use App\Modules\Languages\Repositories\LangRepository;
use App\Modules\Account\Repositories\NotificationRepository;
use App\Modules\Account\Repositories\NotificationUserRepository;
use App\Modules\Investments\Repositories\TradeRepository;
use App\Modules\Messages\Services\MessageService;
use App\Modules\Core\Services\DataService;
use App\Modules\Languages\Services\LanguageService;
use App\Modules\Admin\Services\TranslateService;
use Illuminate\Support\Facades\Storage;

/**
 * Account service.
 */
final class AccountService
{
    /**
     * Constructor.
     *
     * @param LangRepository $langRepository
     * @param TranslateService $translateService
     * @param LanguageService $languageService
     * @param NotificationRepository $notificationRepository
     * @param NotificationUserRepository $notificationUserRepository
     * @param TradeRepository $tradeRepository
     * @param MessageService $messageService
     * @param SettingsStylesRepository $settingsStylesRepository
     */
    public function __construct(
        private LangRepository $langRepository,
        private TranslateService $translateService,
        private LanguageService $languageService,
        private NotificationRepository $notificationRepository,
        private NotificationUserRepository $notificationUserRepository,
        private TradeRepository $tradeRepository,
        private MessageService $messageService,
        private SettingsStylesRepository $settingsStylesRepository,
    )
    {
    }

    /**
     * Get global content.
     *
     * @param User $user
     * @return array
     * @throws TranslateException
     */
    public function global(User $user): array
    {
        $data = [];
        $data['user'] = [
            'avatar' => Storage::disk('local')->url($user->avatar),
            'username' => $user->username,
            'location' => config('country')[$user->country] ?? $user->country,
            'abc_total' => $user->balance_total_abc,
            'usdt_total' => $user->balance_usdt,
            'tid_active' => $this->tradeRepository->countAllOpeningByUser($user->id) > 0,
            'tids_allowable' => $user->isTidsAllowable,
        ];

        $data['translates'] = $this->translateService->getTranslates(
            [
                'base',
                'account'
            ]
        );

        $data['langs'] = [];

        foreach ($this->langRepository->all() as $lang) {
            $data['langs'][] = [
                'id' => $lang->sid,
                'short_name' => $lang->short_name,
                'name' => $lang->name,
                'icon' => Storage::disk('local')->url($lang->icon)
            ];
        }

        $data['language'] = $this->languageService->getLocale();
        $data['notifications'] = null;

        $notifications = [];

        foreach ($this->notificationRepository->getActive($user) as $notification) {
            $notifications[] = [
                'id' => $notification->sid,
                'title' => $notification->title,
                'text' => $notification->text,
                'link' => $notification->link,
                'time' => $notification->created_at->format('H:i'),
                'date' => $notification->created_at->format('d.m.Y'),
                'have_read' => $notification->haveRead($user)
            ];
        }

        $data['notifications'] = $notifications;
        $data['masks'] = DataService::TYPES_DIGITS;
        $data['rate_update'] = config('app.rate_update');
        $data['new_messages'] = $this->messageService->countNewMessages($user);

        $styles = $this->settingsStylesRepository->get();

        $data['styles'] = [
            'enable_custom_body_css' => $styles->enable_custom_body_css,
            'body_cls_name' => $styles->body_cls_name,
            'body_css' => $styles->body_css,

            'enable_custom_header_css' => $styles->enable_custom_header_css,
            'header_cls_name' => $styles->header_cls_name,
            'header_css' => $styles->header_css,

            'enable_custom_sidebar_css' => $styles->enable_custom_sidebar_css,
            'sidebar_cls_name' => $styles->sidebar_cls_name,
            'sidebar_css' => $styles->sidebar_css,

            'enable_custom_buttons_css' => $styles->enable_custom_buttons_css,
            'buttons_cls_name' => $styles->buttons_cls_name,
            'buttons_css' => $styles->buttons_css,
        ];

        return $data;
    }

    /**
     * View notification.
     *
     * @param User $user
     * @param int $notificationId
     */
    public function viewNotification(User $user, Notification $notification): void
    {
        $this->notificationUserRepository->create([
            'notifications_id' => $notification->id,
            'users_id' => $user->id
        ]);
    }

    /**
     * Clear notifications.
     *
     * @param User $user
     */
    public function clearNotifications(User $user): void
    {
        foreach ($this->notificationRepository->getActive($user) as $notification) {
            $notificationUser = $notification->notificationUser($user);

            if ($notificationUser) {
                $notificationUser->deleted = true;
                $notificationUser->save();
            } else {
                $this->notificationUserRepository->create([
                    'notifications_id' => $notification->id,
                    'users_id' => $user->id,
                    'deleted' => true
                ]);
            }
        }
    }
}
