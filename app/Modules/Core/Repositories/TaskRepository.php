<?php


namespace App\Modules\Core\Repositories;

use App\Modules\Core\Models\Task;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * Task repository.
 */
final class TaskRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Task $model
     */
    public function __construct(protected Task $model)
    {
    }

    /**
     * Get by type.
     *
     * @param int $type
     * @param int $limit
     * @return Collection
     */
    public function getByType(int $type, int $limit = Task::LIMIT): Collection
    {
        return $this->model
            ->newQuery()
            ->where('type', $type)
            ->orderBy('id', 'DESC')
            ->limit($limit)
            ->get();
    }

    /**
     * Get by types.
     *
     * @param array $types
     * @param int $limit
     * @return Collection
     */
    public function getByTypes(array $types, int $limit = Task::LIMIT): Collection
    {
        if (count($types) === 0) {
            return collect();
        }

        return $this->model
            ->newQuery()
            ->whereIn('type', $types)
            ->orderBy('id', 'DESC')
            ->limit($limit)
            ->get();
    }
}
