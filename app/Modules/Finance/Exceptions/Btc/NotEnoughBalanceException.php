<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * NotEnoughBalanceException.
 */
final class NotEnoughBalanceException extends BtcException
{
}
