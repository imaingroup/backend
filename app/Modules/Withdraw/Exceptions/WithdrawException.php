<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Exceptions;

use Exception;

/**
 * WithdrawException.
 */
abstract class WithdrawException extends Exception
{
}
