<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services\V2;

use App\Modules\Finance\Contracts\Enum\NetworkEnumContract;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Exceptions\Services\BitQuery\RequestException;
use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Finance\Models\CryptoNetwork;
use App\Modules\Finance\Repositories\CryptoCurrencyRepository;
use App\Modules\Finance\Repositories\CryptoNetworkRepository;
use Illuminate\Support\Collection;

/**
 * Data getter service.
 */
final class DataGetterService
{
    /**
     * Get currencies.
     *
     * @return Collection
     */
    public function getCurrencies(): Collection
    {
        return $this->getCryptoNetworkRepository()
            ->getSorted()
            ->map(fn(CryptoNetwork $network) => $network->toApiDto());
    }

    /**
     * Get currency details.
     *
     * @param CryptoCurrency $currency
     * @return CryptoCurrency
     * @throws RequestException
     */
    public function getCurrencyDetails(CryptoCurrency $currency): CryptoCurrency
    {
        if ((!$currency->stats || !$currency->stats->senders) && $currency->network->toEnum() instanceof EthereumNetworkEnum) {
            $this->getDataFetcherService()
                ->updateCurrencyStats($currency);
        }

        try {
            $this->getDataFetcherService()
                ->updateCurrencyMarket($currency);
        } catch (RequestException) {}

        $currency->refresh();

        return $currency;
    }

    /**
     * Find main crypto currency by network.
     *
     * @param \BackedEnum&NetworkEnumContract $network
     * @return CryptoCurrency|null
     */
    public function findMainCurrencyByNetwork(NetworkEnumContract&\BackedEnum $network): ?CryptoCurrency
    {
        return $this->getCryptoCurrencyRepository()
            ->findByAddressAndNetwork(null, $network);
    }

    /**
     * Find token.
     *
     * @param \BackedEnum&NetworkEnumContract $network
     * @param string $address
     * @return CryptoCurrency|null
     */
    public function findToken(NetworkEnumContract&\BackedEnum $network, string $address): ?CryptoCurrency
    {
        return $this->getCryptoCurrencyRepository()
            ->findByAddressAndNetwork($address, $network);
    }

    /**
     * Get {@see CryptoNetworkRepository::class}.
     *
     * @return CryptoNetworkRepository
     */
    private function getCryptoNetworkRepository(): CryptoNetworkRepository
    {
        return app(CryptoNetworkRepository::class);
    }

    /**
     * Get {@see DataFetcherService::class}.
     *
     * @return DataFetcherService
     */
    private function getDataFetcherService(): DataFetcherService
    {
        return app(DataFetcherService::class);
    }

    /**
     * Get {@see CryptoCurrencyRepository::class}.
     *
     * @return CryptoCurrencyRepository
     */
    private function getCryptoCurrencyRepository(): CryptoCurrencyRepository
    {
        return app(CryptoCurrencyRepository::class);
    }
}
