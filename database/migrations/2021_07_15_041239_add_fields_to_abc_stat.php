<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Adds fields for disable counters in
 */
final class AddFieldsToAbcStat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('settings_abc', function (Blueprint $table) {
            $table->boolean('counting_off')
                ->default(false)
                ->after('abc_left_title');
        });

        Schema::table('settings_abc', function (Blueprint $table) {
            $table->float('counting_off_trigger', 16, 8)
                ->nullable(true)
                ->after('abc_left_title');
        });

        DB::transaction(function() {
            DB::table('data_rows')
                ->insert([
                    'data_type_id' =>  $this->getDataType()->id,
                    'field' => 'counting_off',
                    'type' => 'checkbox',
                    'display_name' => 'Counting off',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '',
                    'order' => 9
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' =>  $this->getDataType()->id,
                    'field' => 'counting_off_trigger',
                    'type' => 'number',
                    'display_name' => 'Counting off trigger',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"nullable|numeric"}}',
                    'order' => 9
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $fields = ['counting_off', 'counting_off_trigger'];

        Schema::table('settings_abc', function (Blueprint $table) use($fields) {
            $table->dropColumn($fields);
        });

        DB::transaction(function() use($fields) {
            DB::table('data_rows')
                ->where('data_type_id', $this->getDataType()->id)
                ->whereIn('field', $fields)
                ->delete();
        });
    }

    /**
     * Get data type.
     *
     * @return stdClass
     */
    private function getDataType(): stdClass
    {
        return DB::table('data_types')
            ->where('name', 'settings_abc')
            ->first();
    }
}
