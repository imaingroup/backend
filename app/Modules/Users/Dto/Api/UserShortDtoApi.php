<?php
declare(strict_types=1);

namespace App\Modules\Users\Dto\Api;

use App\Modules\Core\Dto\BaseDto;

/**
 * UserShortDtoApi.
 *
 * @property-read int $id
 * @property-read string $username
 */
final class UserShortDtoApi extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $id
     * @param string $username
     */
    public function __construct(
        protected int $id,
        protected string $username,
    )
    {
    }
}
