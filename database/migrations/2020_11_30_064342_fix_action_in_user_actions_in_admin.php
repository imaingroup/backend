<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Fix action name in user actions.
 */
class FixActionInUserActionsInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('data_rows')
            ->where('data_type_id', 9)
            ->where('field', 'action')
            ->update([
                'details' => '{"options":{"1":"Profit card","2":"Deposit","3":"Withdraw","4":"Transfer","5":"COMMISSION (USDT)","6":"COMMISSION (IPRO)","7":"BUY IPRO"}}'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
