<?php
declare(strict_types=1);

namespace App\Modules\Finance\Commands\V2\Migrations;

use App\Facades\DB;
use App\Modules\Deposites\Models\Deposit;
use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Models\CryptoTransaction;
use App\Modules\Finance\Services\V2\DataGetterService;
use App\Modules\Withdraw\Models\Withdraw;
use Illuminate\Console\Command;

/**
 * Token fetcher command.
 */
final class MigrateCryptoDataToV2Command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finance:migrate_crypto_data_to_v2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate crypto data to V2.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $bitcoinCurrency = $this->getDataGetterService()
            ->findMainCurrencyByNetwork(BitcoinNetworkEnum::BITCOIN);
        $ethereumCurrency = $this->getDataGetterService()
            ->findMainCurrencyByNetwork(EthereumNetworkEnum::ETHEREUM);
        $usdtCurrency = $this->getDataGetterService()
            ->findToken(EthereumNetworkEnum::ETHEREUM, CryptoAddress::ERC20_TOKEN_USDT_ADDRESS);

        DB::transaction(function () use ($bitcoinCurrency, $ethereumCurrency) {
            CryptoAddress::query()
                ->whereNull('crypto_currencies_id')
                ->get()
                ->each(function (CryptoAddress $address) use ($bitcoinCurrency, $ethereumCurrency) {
                    if ($address->type === CryptoAddress::TYPE_BTC) {
                        $address->currency()
                            ->associate($bitcoinCurrency);
                    } else if ($address->type === CryptoAddress::TYPE_ETH) {
                        $address->currency()
                            ->associate($ethereumCurrency);
                    }

                    $address->save();
                });
        });

        DB::transaction(function () use ($bitcoinCurrency, $ethereumCurrency, $usdtCurrency) {
            CryptoTransaction::query()
                ->whereNull('crypto_currencies_id')
                ->get()
                ->each(function (CryptoTransaction $transaction) use ($bitcoinCurrency, $ethereumCurrency, $usdtCurrency) {
                    if ($transaction->type_old === CryptoAddress::TYPE_USDT) {
                        $transaction->currency()
                            ->associate($usdtCurrency);
                    } else if ($transaction->address->type === CryptoAddress::TYPE_BTC) {
                        $transaction->currency()
                            ->associate($bitcoinCurrency);
                    } else if ($transaction->address->type === CryptoAddress::TYPE_ETH) {
                        $transaction->currency()
                            ->associate($ethereumCurrency);
                    }

                    $transaction->save();
                });
        });

        DB::transaction(function () use ($bitcoinCurrency, $ethereumCurrency, $usdtCurrency) {
            CryptoTransaction::query()
                ->whereNull('crypto_currencies_id')
                ->get()
                ->each(function (CryptoTransaction $transaction) use ($bitcoinCurrency, $ethereumCurrency, $usdtCurrency) {
                    if ($transaction->type_old === CryptoAddress::TYPE_USDT) {
                        $transaction->currency()
                            ->associate($usdtCurrency);
                    } else if ($transaction->address->type === CryptoAddress::TYPE_BTC) {
                        $transaction->currency()
                            ->associate($bitcoinCurrency);
                    } else if ($transaction->address->type === CryptoAddress::TYPE_ETH) {
                        $transaction->currency()
                            ->associate($ethereumCurrency);
                    }

                    $transaction->save();
                });
        });

        DB::transaction(function () use ($bitcoinCurrency, $ethereumCurrency, $usdtCurrency) {
            Deposit::query()
                ->whereNull('crypto_currencies_id')
                ->get()
                ->each(function (Deposit $deposit) use ($bitcoinCurrency, $ethereumCurrency, $usdtCurrency) {
                    if ($deposit->type_coin === CryptoAddress::TYPE_USDT) {
                        $deposit->currency()
                            ->associate($usdtCurrency);
                    } else if ($deposit->type_coin === CryptoAddress::TYPE_BTC) {
                        $deposit->currency()
                            ->associate($bitcoinCurrency);
                    } else if ($deposit->type_coin === CryptoAddress::TYPE_ETH) {
                        $deposit->currency()
                            ->associate($ethereumCurrency);
                    }

                    $deposit->save();
                });
        });

        DB::transaction(function () use ($bitcoinCurrency, $ethereumCurrency, $usdtCurrency) {
            Withdraw::query()
                ->whereNull('crypto_currencies_id')
                ->get()
                ->each(function (Withdraw $withdraw) use ($bitcoinCurrency, $ethereumCurrency, $usdtCurrency) {
                    if ($withdraw->type_coin === CryptoAddress::TYPE_USDT) {
                        $withdraw->currency()
                            ->associate($usdtCurrency);
                    } else if ($withdraw->type_coin === CryptoAddress::TYPE_BTC) {
                        $withdraw->currency()
                            ->associate($bitcoinCurrency);
                    } else if ($withdraw->type_coin === CryptoAddress::TYPE_ETH) {
                        $withdraw->currency()
                            ->associate($ethereumCurrency);
                    }

                    $withdraw->save();
                });
        });

        return 0;
    }

    /**
     * Get {@see DataGetterService::class}.
     *
     * @return DataGetterService
     */
    private function getDataGetterService(): DataGetterService
    {
        return app(DataGetterService::class);
    }
}
