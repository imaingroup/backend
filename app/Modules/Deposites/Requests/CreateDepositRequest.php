<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Requests;

use App\Modules\Deposites\Models\Deposit;
use App\Modules\Deposites\Services\DepositService;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Deposites\Repositories\DepositRepository;
use App\Modules\Settings\Models\DepositCurrency;
use App\Modules\Settings\Repositories\SettingsDepositRepository;
use App\Modules\Settings\Services\CurrenciesService;
use App\Modules\Users\Models\User;
use DateTime;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class validates create deposit request.
 */
final class CreateDepositRequest extends FormRequest
{
    /** @var int Limit invoices in minutes. */
    private int $limitInvoicesMinutes = Deposit::LIMIT_INVOICE_MINUTES;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'amount' => "string",
        'currency' => "string",
        'accepted_rules' => "string",
    ])]
    public function rules(): array
    {
        return [
            'amount' => 'required|numeric|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'currency' => 'required|integer|exists:deposit_currencies,crypto_currencies_id',
            'accepted_rules' => 'accepted',
        ];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape([
        'user' => User::class,
        'amount' => "float",
        'currency' => DepositCurrency::class,
    ])]
    public function validated(): array
    {
        $data = parent::validated();

        return [
            'user' => Auth::user(),
            'amount' => (float) $data['amount'],
            'currency' => $this->getCurrenciesService()
                ->find((int)$this['currency'])
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function (Validator $validator) {
            if (count($validator->failed()) === 0) {
                $this->after($validator);
            }
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws Exception
     */
    private function after(Validator $validator): void
    {
        $settingsDeposit = $this->getSettingsDepositRepository()
            ->get();

        $currency = $this->getCurrenciesService()
            ->find((int)$this['currency']);

        if ($settingsDeposit->is_deposit_block ||
            Auth::user()->is_deposit_block ||
            $currency->deposit_block
        ) {
            $validator->errors()->add('global', 'You can\'t top up your balance now');
            return;
        }

        $amount = (float)$this['amount'];

        if ($amount < Deposit::MINIMUM_AMOUNT) {
            $validator->errors()->add('amount', 'The top-up amount is too small');
            return;
        }
        else if($amount < $currency->deposit_min) {
            $validator->errors()->add('amount', sprintf('The amount of %s less then minimum amount %s', $currency->full_name, $currency->deposit_min));
            return;
        }

        $lastInvoice = $this->getDepositRepository()
            ->getLastInvoice(Auth::user()->id);
        if ($lastInvoice
            && $lastInvoice->status === Deposit::STATUS_NEW
            && $lastInvoice->created_at->modify(\sprintf('+ %s MINUTES', $this->limitInvoicesMinutes)) > (new DateTime())
        ) {
            $validator->errors()->add('global', 'Invoice opening limit');
        }
    }

    /**
     * Get deposit repository.
     *
     * @return DepositRepository
     */
    private function getDepositRepository(): DepositRepository
    {
        return app(DepositRepository::class);
    }

    /**
     * Get {@see CurrenciesService::class}.
     *
     * @return CurrenciesService
     */
    private function getCurrenciesService(): CurrenciesService
    {
        return app(CurrenciesService::class);
    }

    /**
     * Get settings deposit repository.
     *
     * @return SettingsDepositRepository
     */
    private function getSettingsDepositRepository(): SettingsDepositRepository
    {
        return app(SettingsDepositRepository::class);
    }
}
