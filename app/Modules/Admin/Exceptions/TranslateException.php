<?php


namespace App\Modules\Admin\Exceptions;


use Exception;

/**
 * This exception using in translate service.
 */
final class TranslateException extends Exception
{

}
