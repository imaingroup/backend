<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Finance;

use App\Modules\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * AdminFinanceWalletsDataRequest.
 */
final class AdminFinanceWalletsDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check() &&
            Auth::user()->hasRole(User::ROLE_ADMIN_NAME);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
