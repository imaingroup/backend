<?php
declare(strict_types=1);

namespace App\Modules\Core\Validation;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

/**
 * Validate that 2fa installed.
 */
final class G2FaInstall
{
    /**
     * Validate.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        $user = Auth::user();

        return $user && $user->is_2fa;
    }
}
