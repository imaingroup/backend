<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration create unique index
 * for columns messages_id, users_id
 * for messages_read table.
 */
final class AddUniqueIndexToMessagesRead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('messages_read', function (Blueprint $table) {
            $table->unique(['messages_id', 'users_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('messages_read', function (Blueprint $table) {
            $table->dropIndex('messages_read_messages_id_users_id_unique');
        });
    }
}
