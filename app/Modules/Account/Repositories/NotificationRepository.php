<?php


namespace App\Modules\Account\Repositories;

use App\Modules\Account\Models\Notification;
use App\Modules\Users\Models\User;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Notifications repository.
 */
final class NotificationRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Notification $model
     */
    public function __construct(protected Notification $model)
    {}

    /**
     * Get all active notifications.
     *
     * @param User $user
     * @return Collection
     */
    public function getActive(User $user): Collection
    {
        return $this->model
            ->newQuery()
            ->where('enable_before_at', '>', new \DateTime())
            ->whereDoesntHave('notificationsUsers', function(Builder $query) use($user) {
                $query->where('users_id', $user->id)
                    ->where('deleted', true);
            })
            ->orderBy('id', 'DESC')
            ->get();
    }
}
