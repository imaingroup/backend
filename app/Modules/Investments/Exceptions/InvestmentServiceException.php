<?php


namespace App\Modules\Investments\Exceptions;


use Exception;


/**
 * This exception using in investments service.
 */
final class InvestmentServiceException extends Exception
{

}
