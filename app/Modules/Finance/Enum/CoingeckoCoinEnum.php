<?php
declare(strict_types=1);

namespace App\Modules\Finance\Enum;

use App\Modules\Finance\Contracts\Enum\NetworkEnumContract;
use BackedEnum;

/**
 * CoingeckoCoinEnum.
 */
enum CoingeckoCoinEnum: string
{
    case BTC = 'bitcoin';
    case ETH = 'ethereum';
    case ETC = 'ethereum-classic';
    case BNB = 'binancecoin';
    case VLX = 'velas';
    case MATIC = 'matic-network';
    case KLAYTN = 'klay-token';


    /**
     * From network.
     *
     * @param BackedEnum&NetworkEnumContract $enum
     * @return CoingeckoCoinEnum|null
     */
    public static function fromNetwork(NetworkEnumContract&BackedEnum $enum): ?CoingeckoCoinEnum
    {
        return match ($enum->value) {
            BitcoinNetworkEnum::BITCOIN->value => self::BTC,
            EthereumNetworkEnum::ETHEREUM->value => self::ETH,
            EthereumNetworkEnum::ETH_CLASSIC->value => self::ETC,
            EthereumNetworkEnum::BSC->value => self::BNB,
            EthereumNetworkEnum::VELAS->value => self::VLX,
            EthereumNetworkEnum::MATIC->value => self::MATIC,
            default => null,
        };
    }
}
