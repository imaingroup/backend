IP {{ $ip }} INFO.<br/>
COUNTRY ISO CODE: {{ $geo->country->isoCode ?? 'N/A' }}<br/>
COUNTRY NAME: {{ $geo->country->name ?? 'N/A' }}<br/>
SUBDIVISION NAME: {{ $geo->mostSpecificSubdivision->name ?? 'N/A' }}<br/>
SUBDIVISION ISO CODE: {{ $geo->mostSpecificSubdivision->isoCode ?? 'N/A' }}<br/>
CITY NAME: {{ $geo->city->name ?? 'N/A' }}<br/>
POSTAL CODE: {{ $geo->postal->code ?? 'N/A' }}<br/>
LOCATION LATITUDE: {{ $geo->location->latitude ?? 'N/A' }}<br/>
LOCATION LONGITUDE: {{ $geo->location->longitude ?? 'N/A' }}<br/>
NETWORK: {{ $geo->traits->network ?? 'N/A' }}<br/>
