<?php


namespace App\Modules\Investments\Repositories;

use App\Modules\Investments\Models\TidRequestHistory;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Bitcoin log repository.
 */
final class TidRequestHistoryRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param TidRequestHistory $model
     */
    public function __construct(protected TidRequestHistory $model)
    {}
}
