<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Users;

use App\Modules\Users\Models\User;
use App\Modules\Users\Repositories\UserRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Admin move teammates request.
 */
final class AdminMoveTeammatesRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Users\Repositories\UserRepository $userRepository
     */
    public function __construct(private UserRepository $userRepository)
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'new_team' => "string",
        'teammates' => "string",
        'teammates.*' => "string"
    ])]
    public function rules(): array
    {
        return [
            'new_team' => 'required|exists:users,promo_code',
            'teammates' => 'required|array|min:1',
            'teammates.*' => 'required|exists:users,id'
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'teamLead' => "\App\Models\User",
        'newTeamLead' => "\App\Models\User",
        'teammates' => "\Illuminate\Support\Collection"
    ])]
    public function validated(): array
    {
        return [
            'teamLead' => $this->teamLead,
            'newTeamLead' => $this->userRepository->findByPromoCode($this->new_team),
            'teammates' => $this->userRepository->getByIds($this->teammates)
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function (Validator $validator) {
            if (count($validator->failed()) === 0) {
                $this->after($validator);
            }
        });
    }

    /**
     * Post validation.
     *
     * @param Validator $validator
     * @return void
     */
    private function after(Validator $validator)
    {
        if (!$this->teamLead instanceof User) {
            $validator->errors()->add('global', 'Incorrect teamLead parameter.');

            return;
        }

        $newTeam = $this->userRepository->findByPromoCode($this->new_team);
        $teamMates = $this->userRepository->getByIds($this->teammates);

        if ($newTeam->id === $this->teamLead->id) {
            $validator->errors()->add('new_team', 'Team lead must be other.');

            return;
        }

        foreach ($teamMates as $teamMate) {
            if ($teamMate->team_member_id !== $this->teamLead->id) {
                $validator->errors()->add('teammates', 'Incorrect teammate.');

                return;
            }
        }
    }
}
