<?php


namespace App\Modules\News\Repositories;

use App\Modules\News\Models\News;
use App\Modules\News\Repositories\NewsCategoryRepository;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * News repository.
 */
final class NewsRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\News\Models\News $model
     * @param NewsCategoryRepository $newsCategoryRepository
     */
    public function __construct(protected News $model, protected NewsCategoryRepository $newsCategoryRepository)
    {}

    /**
     * Get landing news.
     *
     * @param int $limit
     * @return Collection
     */
    public function getLandingNews(int $limit): Collection
    {
        return $this->model
            ->newQuery()
            ->where('published_at', '<=', new \DateTime())
            ->where('is_view_landing', true)
            ->where('is_visible', true)
            ->orderBy('published_at', 'DESC')
            ->limit($limit)
            ->get();
    }

    /**
     * Get account news.
     *
     * @param string $sort
     * @param int|null $categoryId
     * @param string|null $search
     * @param int $limit
     * @return Collection
     */
    public function getAccountNews(string $sort, ?int $categoryId, ?string $search, int $page, int $limit): Collection
    {
        return $this->accountNewsQuery($sort, $categoryId, $search)
            ->offset(($page - 1) * $limit)
            ->limit($limit)
            ->get()
            ->load('category');
    }

    /**
     * Get account news count.
     *
     * @param string $sort
     * @param int|null $categoryId
     * @param string|null $search
     * @return int
     */
    public function getAccountNewsCount(string $sort, ?int $categoryId, ?string $search): int
    {
        return $this->accountNewsQuery($sort, $categoryId, $search)->count();
    }

    /**
     * Get account news ids.
     *
     * @param string $sort
     * @param int|null $categoryId
     * @param string|null $search
     * @return array
     */
    public function getAccountNewsSids(string $sort, ?int $categoryId, ?string $search): array
    {
        return $this->accountNewsQuery($sort, $categoryId, $search)
            ->select('sid')
            ->get()
            ->pluck('sid')
            ->toArray();
    }

    /**
     * Account new query.
     *
     * @param string $sort
     * @param int|null $categoryId
     * @param string|null $search
     * @return Builder
     */
    private function accountNewsQuery(string $sort, ?int $categoryId, ?string $search): Builder
    {
        $query = $this->model
            ->newQuery()
            ->where('published_at', '<=', new \DateTime())
            ->where('is_view_lk', true)
            ->where('is_visible', true)
            ->orderBy('published_at', $sort);

        if ($categoryId) {
            $category = $this->newsCategoryRepository->findBySid($categoryId);

            $query->where('news_categories_id', $category->id);
        }

        if ($search) {
            $query->where(function (Builder $query) use ($search) {
                $query->where(function (Builder $query) use ($search) {
                    $query->where('title', 'like', '%' . mb_strtoupper($search) . '%')
                        ->orWhere('title', 'like', '%' . mb_strtolower($search) . '%')
                        ->orWhere('title', 'like', '%' . mb_ucfirst(mb_strtolower($search)) . '%');
                })->orWhere(function (Builder $query) use ($search) {
                    $query->where('catalog_preview', 'like', '%' . mb_strtoupper($search) . '%')
                        ->orWhere('catalog_preview', 'like', '%' . mb_strtolower($search) . '%')
                        ->orWhere('catalog_preview', 'like', '%' . mb_ucfirst(mb_strtolower($search)) . '%');
                });
            });
        }

        return $query;
    }
}
