<?php
declare(strict_types=1);

namespace App\Modules\Security\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Security\Models\UserIdentificationUserRelation;
use Illuminate\Support\Collection;

/**
 * UserIdentificationUserRelationRepository.
 */
final class UserIdentificationUserRelationRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param UserIdentificationUserRelation $model
     */
    public function __construct(protected UserIdentificationUserRelation $model)
    {
    }

    /**
     * Get by user.
     *
     * @param int $userId
     * @return Collection
     */
    public function getByMainUser(int $userId): Collection
    {
        return $this->model
            ->newQuery()
            ->where('users_main_id', $userId)
            ->get();
    }
}
