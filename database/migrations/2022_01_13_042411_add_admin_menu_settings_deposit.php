<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * Add menu settings deposit.
 */
final class AddAdminMenuSettingsDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::transaction(function () {
            DB::table('menu_items')
                ->insert([
                    'menu_id' => 1,
                    'title' => 'CURRENCIES',
                    'url' => '',
                    'target' => '_self',
                    'icon_class' => 'voyager-settings',
                    'color' => '#000000',
                    'parent_id' => DB::table('menu_items')
                        ->where('title', 'Settings')
                        ->whereNull('parent_id')
                        ->first()
                        ->id,
                    'order' => 5,
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime(),
                    'route' => 'voyager.settings.currencies'
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::transaction(function () {
            DB::table('menu_items')
                ->where('parent_id', DB::table('menu_items')
                    ->where('title', 'Settings')
                    ->whereNull('parent_id')
                    ->first()
                    ->id
                )
                ->where('route', 'voyager.settings.currencies')
                ->delete();
        });
    }
}
