<?php

namespace App\Modules\Notifications\Models;

use App\Modules\Notifications\Models\MailVariable;
use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Core\Models\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use TCG\Voyager\Traits\Translatable;

final class Mail extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;

    const MAIL_ACTIVATE = 1;
    const MAIL_LANDING_CONTACT = 2;
    const MAIL_BLOCKING_DEPOSIT = 3;
    const MAIL_PASSWORD_WAS_RESET = 4;
    const MAIL_RESET_PASSWORD = 5;
    const MAIL_DEPOSIT_REPLENISHED = 6;
    const MAIL_WITHDRAW_DECLINED = 7;
    const MAIL_WITHDRAW_PROCESSED = 8;
    const MAIL_BUY_PROFIT_CARD = 9;

    protected $translatable = ['subject', 'mail'];

    protected $table = 'mails';

    public function mailVariables(): HasMany
    {
        return $this->hasMany(MailVariable::class, 'mails_id');
    }
}
