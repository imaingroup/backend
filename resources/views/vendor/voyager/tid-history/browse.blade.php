@extends('voyager::bread.browse')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->getTranslatedAttribute('display_name_plural') }}
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete', app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit', app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary btn-add-new">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        @can('delete', app($dataType->model_name))
            @if($usesSoftDeletes)
                <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle"
                       data-on="{{ __('voyager::bread.soft_deletes_off') }}"
                       data-off="{{ __('voyager::bread.soft_deletes_on') }}">
            @endif
        @endcan
        @foreach($actions as $action)
            @if (method_exists($action, 'massAction'))
                @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
            @endif
        @endforeach
        @include('voyager::multilingual.language-selector')
        <div style="margin-left: 27px;max-width: 400px">
            <form id="generate-form">
                <h4>Trading Data</h4>
                <div class="form-group">
                    <label>Period (e.g. 30)</label>
                    <input type="number" name="period" step="1" min="0" class="form-control"
                           placeholder="Period (e.g. 30)"/>
                    <div class="text-error period" style="display: none"></div>
                </div>
                <div class="form-group">
                    <label>Positive days (e.g. 20)</label>
                    <input type="number" name="positive_days" step="1" min="0" class="form-control"
                           placeholder="Positive days (e.g. 20)"/>
                    <div class="text-error positive_days" style="display: none"></div>
                </div>
                <div class="form-group">
                    <label>Default % (e.g. 15)</label>
                    <input type="number" name="percent" step="0.01" min="0" class="form-control"
                           placeholder="Default % (e.g. 15)"/>
                    <div class="text-error percent" style="display: none"></div>
                </div>
                <div class="form-group">
                    <label>High MAX % (e.g. 9.34)</label>
                    <input type="number" name="high_max" step="0.01" min="0" class="form-control"
                           placeholder="High MAX % (e.g. 9.34)"/>
                    <div class="text-error high_max" style="display: none"></div>
                </div>
                <div class="form-group">
                    <label>Low MAX % (e.g. -7.8)</label>
                    <input type="number" name="low_max" step="0.01" class="form-control"
                           placeholder="High MAX % (e.g. -7.8)"/>
                    <div class="text-error low_max" style="display: none"></div>
                </div>
                <div class="form-group">
                    <label>Loss sum (e.g. 20)</label>
                    <input type="number" name="loss_sum_percent" step="0.01" class="form-control"
                           placeholder="Loss sum % (e.g. 20)"/>
                    <div class="text-error loss_sum_percent" style="display: none"></div>
                </div>
                <div class="form-group">
                    <input type="submit" class="form-control" value="Generate"/>
                    <div class="text-error global" style="display: none"></div>
                </div>
                <div class="form-group">
                    <input id="remove_history" type="button" class="form-control" value="Remove feature history"
                           style="background: red; color: #fff;font-weight: bold"/>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
    <style>
        .text-error {
            color: red;
        }
    </style>
@stop


@push('javascript')
    <script>
        var form = $('#generate-form');
        form.submit(function (e) {
            e.preventDefault();

            var fields = [
                'period',
                'positive_days',
                'percent',
                'high_max',
                'low_max',
                'loss_sum_percent',
                'global'
            ];

            $.post('{{ route('voyager.tid_history.generate') }}', form.serialize(), function () {
                for (var i in fields) {
                    $('.text-error.' + fields[i]).hide();
                }

                $('input[name=' + fields[i] + ']').val('');

                toastr.success('Data was generated.', 'Generate.');
                window.location.reload();
            }).fail(function (err) {
                for (var i in fields) {
                    $('.text-error.' + fields[i]).hide();
                }

                if (err.status == 422) {
                    for (var i in fields) {
                        try {
                            var errorText = err.responseJSON.errors[fields[i]][0];
                            $('.text-error.' + fields[i]).show();
                            $('.text-error.' + fields[i]).text(errorText);
                        } catch {
                        }
                    }
                } else if (err.status == 401) {
                    $('.text-error.global').show();
                    $('.text-error.global').text('You are not authorized');
                } else if (err.status == 503) {
                    $('.text-error.global').show();
                    $('.text-error.global').text('Server can\'t process request with specified data. Please correct input parameters.');
                } else {
                    $('.text-error.global').show();
                    $('.text-error.global').text('Server error ' + err.status);
                }
            });
        });

        const dataTable = $('#dataTable');
        dataTable.find('tbody tr td').dblclick(function () {
            if ($(this).find('form').length > 0) {
                return;
            }

            const index = $(this).index();

            if (index > 2) {
                const id = parseInt($(this).parent().find('td:eq(' + 0 + ')').text().trim());
                const date = new Date($(this).parent().find('td:eq(' + 1 + ')').text().trim());
                if (date < new Date()) {
                    return;
                }

                const percent = parseFloat($(this).text().trim());
                const option = index - 3;
                const editFormSelector = '#edit_percent_form';
                const rowItem = this;

                $(editFormSelector).each(function () {
                    $(this).replaceWith($(this).data('origin-percent'));
                });

                $(rowItem).html('<form id="edit_percent_form" data-origin-percent="' + percent + '"><input type="number" value="' + percent + '" step="0.01" name="edit_percent"/></form>');

                $(editFormSelector).submit(function (e) {
                    e.preventDefault();
                    const data = {
                        'id': id,
                        'option': option,
                        'percent': $(this).find('input[name=edit_percent]').val(),
                    };

                    $.post('{{ route('voyager.tid_history.percent.update') }}', data, function () {
                        toastr.success('Percent was updated.', 'Data update.');

                        $(rowItem).html('<span>' + data.percent + '</span>');
                    }).fail(function (err) {
                        toastr.error('Error during update. Server returned status ' + err.status, 'Data update.');
                    });
                });
            }
        });

        $('#remove_history').click(function () {
            if (confirm('Do you want to delete feature history ?')) {
                $.post('{{ route('voyager.tid_history.delete') }}', {}, function () {
                    toastr.success('History was deleted.', 'Data delete.');
                    window.location.reload();
                }).fail(function (err) {
                    toastr.error('Error during delete. Server returned status ' + err.status, 'Data delete.');
                });
            }
        });
    </script>
@endpush
