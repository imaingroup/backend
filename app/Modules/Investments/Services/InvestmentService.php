<?php
declare(strict_types=1);

namespace App\Modules\Investments\Services;

use App\Modules\Investments\Exceptions\InvestmentServiceException;
use App\Modules\Money\Exceptions\MoneyTransferException;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Investments\Models\Trade;
use App\Modules\Investments\Models\TradeHistory;
use App\Modules\Money\Models\Transaction;
use App\Modules\Security\Exceptions\TimeProtectionException;
use App\Modules\Security\Services\TimeProtectionService;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserTidExpanse;
use App\Modules\Core\Exceptions\TransactionAlreadyStartedException;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Investments\Exceptions\ImitationPnlException;
use App\Modules\Investments\Exceptions\TradingProcessException;
use App\Modules\Investments\Models\TradeOption;
use App\Modules\Investments\Repositories\TradeOptionRepository;
use App\Modules\Settings\Repositories\SettingsPriceRepository;
use App\Modules\Investments\Repositories\TidHistoryRepository;
use App\Modules\Investments\Repositories\TidHistoryTradeOptionRepository;
use App\Modules\Investments\Repositories\TradeAccrualsRepository;
use App\Modules\Investments\Repositories\TradeHistoryRepository;
use App\Modules\Investments\Repositories\TradeRepository;
use App\Modules\Users\Repositories\UserRepository;
use App\Modules\Users\Repositories\UserTidExpanseRepository;
use App\Modules\Money\Services\MoneyTransferService;
use DateTime;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Throwable;
use function json_encode;
use function sprintf;

/**
 * Investment service.
 */
final class InvestmentService
{
    /** @var \App\Modules\Investments\Repositories\TradeRepository Trade repository. */
    private TradeRepository $tradeRepository;

    /** @var \App\Modules\Investments\Repositories\TradeOptionRepository Trade option repository. */
    private TradeOptionRepository $tradeOptionRepository;

    /** @var MoneyTransferService Money transfer repository. */
    private \App\Modules\Money\Services\MoneyTransferService $moneyTransferService;

    /** @var SettingsPriceRepository Settings price repository. */
    private SettingsPriceRepository $settingsPriceRepository;

    /** @var \App\Modules\Investments\Repositories\TradeHistoryRepository Trade history repository. */
    private TradeHistoryRepository $tradeHistoryRepository;

    /** @var TidHistoryRepository Tid history repository. */
    private TidHistoryRepository $tidHistoryRepository;

    /** @var \App\Modules\Investments\Repositories\TidHistoryTradeOptionRepository Tid history trade option repository. */
    private TidHistoryTradeOptionRepository $tidHistoryTradeOptionRepository;

    /** @var \App\Modules\Users\Repositories\UserRepository User repository. */
    private UserRepository $userRepository;

    /** @var TradeAccrualsRepository Trade accruals repository. */
    private TradeAccrualsRepository $tradeAccrualsRepository;

    /** @var UserTidExpanseRepository User tid expanse repository. */
    private UserTidExpanseRepository $userTidExpanseRepository;

    /**
     * Constructor.
     *
     * @param TradeRepository $tradeRepository
     * @param TradeOptionRepository $tradeOptionRepository
     * @param MoneyTransferService $moneyTransferService
     * @param SettingsPriceRepository $settingsPriceRepository
     * @param \App\Modules\Investments\Repositories\TradeHistoryRepository $tradeHistoryRepository
     * @param TidHistoryRepository $tidHistoryRepository
     * @param TidHistoryTradeOptionRepository $tidHistoryTradeOptionRepository
     * @param \App\Modules\Users\Repositories\UserRepository $userRepository
     * @param TradeAccrualsRepository $tradeAccrualsRepository
     * @param \App\Modules\Users\Repositories\UserTidExpanseRepository $userTidExpanseRepository
     */
    public function __construct(
        TradeRepository $tradeRepository,
        TradeOptionRepository $tradeOptionRepository,
        MoneyTransferService $moneyTransferService,
        SettingsPriceRepository $settingsPriceRepository,
        TradeHistoryRepository $tradeHistoryRepository,
        TidHistoryRepository $tidHistoryRepository,
        TidHistoryTradeOptionRepository $tidHistoryTradeOptionRepository,
        UserRepository $userRepository,
        TradeAccrualsRepository $tradeAccrualsRepository,
        UserTidExpanseRepository $userTidExpanseRepository
    )
    {
        $this->tradeRepository = $tradeRepository;
        $this->tradeOptionRepository = $tradeOptionRepository;
        $this->moneyTransferService = $moneyTransferService;
        $this->settingsPriceRepository = $settingsPriceRepository;
        $this->tradeHistoryRepository = $tradeHistoryRepository;
        $this->tidHistoryRepository = $tidHistoryRepository;
        $this->tidHistoryTradeOptionRepository = $tidHistoryTradeOptionRepository;
        $this->userRepository = $userRepository;
        $this->tradeAccrualsRepository = $tradeAccrualsRepository;
        $this->userTidExpanseRepository = $userTidExpanseRepository;
    }

    /**
     * Get trade options.
     *
     * @param User|null $user
     * @return array
     */
    public function options(User $user = null): array
    {
        $data = [];

        /** @var TradeOption $option */
        foreach ($this->tradeOptionRepository->getActive() as $option) {
            $data[] = [
                'id' => $option->sid,
                'title' => $user ? $option->getTranslatesWithValue('title', $option->getTitleByUser($user)) : $option->getTranslates('title'),
                'description' => $option->getTranslates('description'),
                'period_min' => $user ? $option->getPeriodMinByUser($user) : $option->period_min,
                'period_max' => $user ? $option->getPeriodMaxByUser($user) : $option->period_max,
                'amount_min' => $user ? $option->getAmountMinByUser($user) : $option->amount_min,
                'amount_max' => $user ? $option->getAmountMaxByUser($user) : $option->amount_max,
                'is_active' => $user ? $user->checkAllowedOption($option) : false
            ];
        }

        return $data;
    }

    /**
     * Open tid.
     *
     * @param User $user
     * @param TradeOption $option
     * @param int $period
     * @param int $amount
     * @throws InvestmentServiceException
     * @throws MoneyTransferException
     * @throws TransactionNotStarted
     */
    public function open(User $user, TradeOption $option, int $period, int $amount)
    {
        User::refreshAndLockForUpdate($user);

        if ($user->balance_abc < $amount) {
            throw new InvestmentServiceException('You doesn\'t have enough money in IPRO balance');
        }

        if ($this->getOpeningLimit($user, $option) < 1) {
            throw new InvestmentServiceException('You exceed limit for opening tid by this option');
        }

        $transaction = $this->moneyTransferService->addToTrading(
            $user,
            Auth::check() ? Auth::user() : $user,
            $amount
        );

        $optionArray = $option->toArray();
        $optionArray['title'] = $option->getTitleByUser($user);
        $optionArray['period_min'] = $option->getPeriodMinByUser($user);
        $optionArray['period_max'] = $option->getPeriodMaxByUser($user);
        $optionArray['amount_min'] = $option->getAmountMinByUser($user);
        $optionArray['amount_max'] = $option->getAmountMaxByUser($user);

        $this->tradeRepository->create([
            'opened_at' => (new DateTime())->modify('+1 DAY'),
            'trade_options_id' => $option->id,
            'users_id' => $user->id,
            'trade_option' => json_encode($optionArray),
            'amount' => $amount,
            'unrealized_pnl' => 0,
            'period' => $period,
            'is_open' => true,
            'rate_abc_usdt' => $this->settingsPriceRepository->get()->abc_price,
            'related_transactions_id' => $transaction->id,
            'tid' => mt_rand(100000000000, 999999999999),
            'accrual_period' => $option->accrual_period
        ]);
    }

    /**
     * Release tid money and transfer pnl to dividends balance.
     *
     * @param int $tradeId
     * @throws InvestmentServiceException
     * @throws MoneyTransferException
     * @throws TransactionNotStarted
     */
    public function release(int $tradeId)
    {
        $trade = $this->tradeRepository->find($tradeId);
        $user = $trade->user;

        User::refreshAndLockForUpdate($user);
        Trade::refreshAndLockForUpdate($trade);

        $transactionRelated = $trade->transactionRelated;

        if ($trade->is_open) {
            throw new InvestmentServiceException('This TID is till opened');
        }

        if ($trade->is_money_released) {
            throw new InvestmentServiceException('Money for this TID already released');
        }

        $transactionWithdraw = $this->moneyTransferService->withdrawFromTrading(
            $user,
            Auth::check() ? Auth::user() : $user,
            $trade->amount,
            $transactionRelated
        );

        $this->moneyTransferService->moneyOperation(
            $user,
            Auth::check() ? Auth::user() : $user,
            $trade->closed_pnl,
            Transaction::CURRENCY_USDT_DIVIDENTS,
            Transaction::TYPE_ADD_DIVIDENDS,
            $transactionWithdraw
        );

        $user->unrealized_pnl -= $trade->closed_pnl;
        $user->bot_total_profit += $trade->closed_pnl;
        $user->floating_pnl -= $trade->floating_pnl;
        $user->save();

        $trade->closed_pnl += $trade->accruals;
        $trade->accruals = $trade->closed_pnl;
        $trade->floating_pnl = 0;
        $trade->is_money_released = true;
        $trade->money_released_at = new DateTime();
        $trade->save();
    }

    /**
     * Get history by user.
     *
     * @param User $user
     * @return array
     * @throws Exception
     */
    public function history(User $user): array
    {
        $trades = $user->trades;

        $history = collect([]);

        foreach ($trades as $trade) {
            $tidNumber = $trade->getTidNumberText();
            $amount = $trade->amount;
            $openedDate = $trade->opened_at;
            $period = $trade->period;
            $option = $trade->tradeOption;
            $endDate = (new DateTime($openedDate))->modify(sprintf('+ %s DAYS', $period));
            $optionHistoryData = json_decode($trade->trade_option);

            $history->add([
                'tid_id' => $trade->tid,
                'date' => $trade->created_at->format('d/m/Y'),
                'time' => $trade->created_at->format('H:i:s'),
                'profit' => null,
                'number' => $tidNumber,
                'amount' => $amount,
                'interest_rate' => null,
                'open_date' => (new DateTime($openedDate))->format('d/m/Y'),
                'deactivation_in' => $period + 1,
                'option' => $optionHistoryData->title ?? $option->title,
                'type' => 'PENDING',
                'sort' => $trade->created_at
            ]);

            if ((new DateTime()) > (new DateTime($openedDate))) {
                $history->add([
                    'tid_id' => $trade->tid,
                    'date' => (new DateTime($openedDate))->format('d/m/Y'),
                    'time' => '00:01:00',
                    'profit' => null,
                    'number' => $tidNumber,
                    'amount' => $amount,
                    'interest_rate' => null,
                    'open_date' => (new DateTime($openedDate))->format('d/m/Y'),
                    'deactivation_in' => $period,
                    'option' => $optionHistoryData->title ?? $option->title,
                    'type' => 'OPENED',
                    'sort' => (new DateTime($openedDate))
                ]);
            }

            foreach ($trade->history as $historyItem) {
                $deactivationIn = $endDate->diff(new DateTime($historyItem->date))->days - 1;

                $history->add([
                    'tid_id' => $trade->tid,
                    'date' => (new DateTime($historyItem->date))->modify('+1 DAY')->format('d/m/Y'),
                    'time' => '00:01:00',
                    'profit' => round($historyItem->profit_usdt, 2),
                    'number' => $tidNumber,
                    'amount' => $amount,
                    'interest_rate' => round($historyItem->interest_rate, 2),
                    'open_date' => (new DateTime($openedDate))->format('d/m/Y'),
                    'deactivation_in' => $deactivationIn,
                    'option' => $optionHistoryData->title ?? $option->title,
                    'type' => $deactivationIn > 0 ? 'DEAL' : 'CLOSED',
                    'sort' => ((new DateTime(sprintf('%s 00:01:00', $historyItem->date)))->modify('+1 DAY'))
                ]);
            }
        }

        $history = $history->sortByDesc('sort');
        $historyData = array_values($history->toArray());

        foreach ($historyData as $index => $data) {
            unset($historyData[$index]['sort']);
        }

        return $historyData;
    }

    /**
     * Imitation PNL process.
     *
     * @throws TransactionAlreadyStartedException
     * @throws ImitationPnlException
     * @throws TimeProtectionException
     */
    public function imitationPnlProcess(): void
    {
        if (DB::transactionLevel() !== 0) {
            throw new TransactionAlreadyStartedException();
        } else if (!$this->getTimeProtectionService()->checkTime()) {
            throw new TimeProtectionException();
        }

        $trades = $this->tradeRepository->getAllOpening();
        $pnlOptions = [];
        $lastLog = ['attempt' => 0];
        $throws = false;

        foreach ($trades->groupBy('users_id')->keys() as $userId) {
            try {
                $lastLog['users_id'] = $userId;
                $lastLog['attempt'] = 0;

                DB::transaction(function () use ($trades, $userId, &$pnlOptions, &$lastLog) {
                    $lastLog['attempt']++;

                    /** @var User $user */
                    $user = $this->userRepository->find($userId);

                    $user->lockAwait(300, function () use ($user, $trades, $userId, &$pnlOptions, &$lastLog) {
                        User::refreshAndLockForUpdate($user);

                        $totalFloatingPnl = 0;
                        $userTrades = $trades->where('users_id', $userId);

                        /** @var Trade $userTrade */
                        foreach ($userTrades as $userTrade) {
                            $openedAt = new DateTime(sprintf('%s 00:59:59', $userTrade->opened_at));

                            if (new DateTime() <= $openedAt) {
                                continue;
                            }

                            $lastLog['trades_id'] = $userTrade->id;

                            $userTrade->lockAwait(300, function () use ($userTrade, &$totalFloatingPnl) {
                                Trade::refreshAndLockForUpdate($userTrade);

                                $option = $userTrade->tradeOption;
                                $floatingPnl = 0;

                                if (!isset($pnlOptions[$option->id])) {
                                    $pnlOptions[$option->id] = mt_rand($option->float_pnl_low, $option->float_pnl_high);
                                }

                                $tidHistory = $this->tidHistoryRepository->findByDate(new DateTime());
                                if ($tidHistory) {
                                    $optionHistory = $this->tidHistoryTradeOptionRepository->findBy($tidHistory->id, $option->id);

                                    if ($optionHistory) {
                                        $profitAbc = $userTrade->amount / 100 * $optionHistory->percent;
                                        $profitUsdt = $profitAbc * $userTrade->rate_abc_usdt;
                                        $floatingPnl = $profitUsdt / 100 * $pnlOptions[$option->id];

                                        $totalFloatingPnl += $floatingPnl;
                                    }
                                }

                                $userTrade->floating_pnl = $floatingPnl;
                                $userTrade->save();
                            });
                        }

                        $user->floating_pnl = $totalFloatingPnl;
                        $user->save();
                    });
                }, 3);
            } catch (Throwable $e) {
                $this->getLogger()->rollbackTransaction($e, $lastLog);

                $throws = true;
            }
        }

        if ($throws) {
            throw new ImitationPnlException();
        }
    }

    /**
     * Trading process.
     *
     * @throws TransactionAlreadyStartedException
     * @throws TimeProtectionException
     * @throws TradingProcessException
     */
    public function tradingProcess(bool $timeProtection = true): void
    {
        if (DB::transactionLevel() !== 0) {
            throw new TransactionAlreadyStartedException();
        } else if ($timeProtection && !$this->getTimeProtectionService()->checkTime()) {
            throw new TimeProtectionException();
        }

        $trades = $this->tradeRepository->getAllOpening();
        $throws = false;
        $lastLog = ['attempt' => 0];
        $currentDateOrigin = new DateTime();

        /** @var Trade $trade */
        foreach ($trades as $trade) {
            $lastLog['tradeId'] = $trade->id;
            $user = $trade->user;

            try {
                $lastLog['attempt'] = 0;

                DB::transaction(function () use ($user, $trade, $currentDateOrigin, &$lastLog) {
                    $lastLog['attempt']++;

                    $user->lockAwait(300, function () use ($user, $trade, $currentDateOrigin, &$lastLog) {
                        User::refreshAndLockForUpdate($user);

                        $trade->lockAwait(300, function () use ($user, $trade, $currentDateOrigin, &$lastLog) {
                            Trade::refreshAndLockForUpdate($trade);

                            $startDate = new DateTime($trade->opened_at);
                            $endDate = (clone $startDate)->modify(sprintf('+ %s DAYS', $trade->period - 1));
                            $lastDeal = $this->tradeHistoryRepository->getLastDeal($trade->id);
                            $currentDate = clone $currentDateOrigin;
                            $tradeAt = null;

                            if (!$lastDeal) {
                                $interval = $currentDate->diff($startDate);

                                if ($interval->invert) {
                                    $tradeAt = clone $startDate;

                                    $limit = $trade->period;
                                    for ($i = 1; $i <= $interval->days; $i++) {
                                        $lastLog['date'] = $tradeAt;
                                        $this->createTradeHistory($trade, $tradeAt);

                                        $tradeAt->modify('+1 DAY');
                                        $limit--;

                                        if ($limit == 0) {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                $lastDealDate = new DateTime($lastDeal->date);
                                $interval = $currentDate->diff($lastDealDate);

                                if ($interval->invert) {
                                    $days = $interval->days - 1;
                                    $tradeAt = (clone $lastDealDate);

                                    for ($i = 1; $i <= $days; $i++) {
                                        if ($endDate <= $tradeAt) {
                                            break;
                                        }

                                        $tradeAt->modify('+1 DAY');

                                        $lastLog['date'] = $tradeAt;
                                        $this->createTradeHistory($trade, $tradeAt);
                                    }
                                }
                            }

                            if ($tradeAt && $endDate <= $tradeAt) {
                                $lastLog['release'] = true;
                                $lastLog['release_trade'] = $trade->export();
                                $trade->is_open = false;
                                $trade->closed_at = new DateTime();
                                $trade->closed_pnl = $trade->unrealized_pnl;
                                $trade->unrealized_pnl = 0;
                                $trade->save();

                                $this->release($trade->id);
                            }
                        });
                    });
                }, 3);
            } catch (Throwable $e) {
                $this->getLogger()->rollbackTransaction($e, $lastLog);

                $throws = true;
            }
        }

        if ($throws) {
            throw new TradingProcessException();
        }
    }

    /**
     * Get Logger.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }

    /**
     * Enable tids by option.
     *
     * @param User $user
     * @param TradeOption $tradeOption
     */
    public function enableTid(User $user, TradeOption $tradeOption)
    {
        if (!$user->checkAllowedOption($tradeOption)) {
            $user->allowedOptions()->save($tradeOption);
        }
    }

    /**
     * Disable tids by option.
     *
     * @param User $user
     * @param TradeOption $tradeOption
     */
    public function disableTid(User $user, TradeOption $tradeOption)
    {
        if ($user->checkAllowedOption($tradeOption)) {
            $user->allowedOptions()->detach($tradeOption->id);
        }
    }

    /**
     * Apply tid expanse.
     *
     * @param User $user
     * @param TradeOption $tradeOption
     * @param int $expanse
     * @param bool $addOperation
     * @throws TransactionNotStarted
     */
    public function applyTidExpanse(User $user, TradeOption $tradeOption, int $expanse, bool $addOperation = true): void
    {
        $tidExpanse = $user->getTidExpanseByOption($tradeOption);

        if ($tidExpanse) {
            UserTidExpanse::refreshAndLockForUpdate($tidExpanse);

            if ($addOperation) {
                $tidExpanse->tid_expanse += $expanse;
            } else {
                $tidExpanse->tid_expanse = $expanse;
            }

            $tidExpanse->save();
        } else {
            $this->userTidExpanseRepository->create([
                'users_id' => $user->id,
                'trade_options_id' => $tradeOption->id,
                'tid_expanse' => $expanse
            ]);
        }
    }

    /**
     * Get opening limit by option.
     *
     * @param User $user
     * @param TradeOption $tradeOption
     * @return int
     */
    public function getOpeningLimit(User $user, TradeOption $tradeOption): int
    {
        if (!$user->checkAllowedOption($tradeOption)) {
            return 0;
        }

        $tidExpanse = $user->getTidExpanseByOption($tradeOption);
        $maxQuantity = $tradeOption->max_quantity;

        if ($tidExpanse) {
            $maxQuantity += $tidExpanse->tid_expanse;
        }

        return $maxQuantity - $this->tradeRepository
                ->countOpenedTidByOptionId($user->id, $tradeOption->id);
    }

    /**
     * Create trade history entity.
     *
     * @param Trade $trade
     * @param DateTime $tradeAt
     * @throws MoneyTransferException
     */
    private function createTradeHistory(Trade $trade, DateTime $tradeAt)
    {
        $tradeHistory = new TradeHistory();
        $tradeHistory->trades_id = $trade->id;
        $tradeHistory->is_no_trade = true;
        $tradeHistory->date = $tradeAt;

        //get trade data
        $tidHistory = $this->tidHistoryRepository->findByDate($tradeAt);

        if ($tidHistory) {
            $optionHistory = $this->tidHistoryTradeOptionRepository->findBy($tidHistory->id, $trade->tradeOption->id);

            if ($optionHistory) {
                $profitAbc = $trade->amount / 100 * $optionHistory->percent;
                $profitUsdt = $profitAbc * $trade->rate_abc_usdt;

                $tradeHistory->profit_usdt = $profitUsdt;
                $tradeHistory->profit_abc = $profitAbc;
                $tradeHistory->interest_rate = $optionHistory->percent;
                $tradeHistory->is_no_trade = false;

                $trade->unrealized_pnl += $profitUsdt;
                $user = $trade->user;

                $user->unrealized_pnl += $profitUsdt;
                $user->save();

                $trade->save();
            }
        }

        if ($tradeHistory->is_no_trade) {
            $tradeHistory->profit_usdt = 0;
            $tradeHistory->profit_abc = 0;
            $tradeHistory->interest_rate = 0;
        }

        $tradeHistory->save();

        $this->accruals($trade, $tradeAt);
    }

    /**
     * Accruals by option rules.
     *
     * @param Trade $trade
     * @param DateTime $tradeAt
     * @throws MoneyTransferException
     * @throws Exception
     */
    private function accruals(Trade $trade, DateTime $tradeAt)
    {
        $exist = $this->tradeAccrualsRepository->existByDate($trade->id, $tradeAt);

        if (!$exist) {
            if ($trade->accrual_period === TradeOption::ACCRUAL_DAILY) {
                $this->accrualProcess($trade, $tradeAt);
            } else {
                $options = [
                    TradeOption::ACCRUAL_30_DAYS => 30,
                    TradeOption::ACCRUAL_60_DAYS => 60
                ];

                foreach ($options as $option => $days) {
                    if ($trade->accrual_period === $option) {
                        $lastAccrual = $this->tradeAccrualsRepository->getLast($trade->id);
                        $lastDate = new DateTime($lastAccrual ? $lastAccrual->date : $trade->opened_at);
                        $diff = $tradeAt->diff($lastDate);

                        if ($diff->invert && $diff->days === $days) {
                            $this->accrualProcess($trade, $tradeAt);
                        }
                    }
                }
            }
        }
    }

    /**
     * Accrual process.
     *
     * @param Trade $trade
     * @param DateTime $tradeAt
     * @throws MoneyTransferException
     * @throws TransactionNotStarted
     */
    private function accrualProcess(Trade $trade, DateTime $tradeAt)
    {
        $user = $trade->user;

        Trade::refreshAndLockForUpdate($trade);
        User::refreshAndLockForUpdate($user);

        $accruals = $trade->unrealized_pnl;
        $trade->unrealized_pnl = 0;
        $trade->accruals += $accruals;

        $trade->save();

        $this->tradeAccrualsRepository->create([
            'amount' => $accruals,
            'date' => $tradeAt,
            'trades_id' => $trade->id
        ]);

        $this->moneyTransferService->moneyOperation(
            $user,
            Auth::check() ? Auth::user() : $user,
            $accruals,
            Transaction::CURRENCY_USDT_DIVIDENTS,
            Transaction::TYPE_ADD_DIVIDENDS,
            $trade->transactionRelated
        );

        $user->unrealized_pnl -= $accruals;
        $user->bot_total_profit += $accruals;
        $user->save();
    }

    /**
     * Get time protection service.
     *
     * @return TimeProtectionService
     */
    private function getTimeProtectionService(): TimeProtectionService
    {
        return app(TimeProtectionService::class);
    }
}
