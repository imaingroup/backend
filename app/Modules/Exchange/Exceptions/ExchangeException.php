<?php


namespace App\Modules\Exchange\Exceptions;


use Exception;

/**
 * This exception may be throws in exchange service.
 */
final class ExchangeException extends Exception
{

}
