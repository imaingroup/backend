<?php
declare(strict_types=1);

namespace App\Modules\Charts\Services;

use App\Modules\Investments\Models\Trade;
use App\Modules\Security\Services\TimeProtectionService;
use App\Modules\Users\Models\User;
use App\Modules\Charts\Dto\Services\ChartOptionDto;
use App\Modules\Investments\Models\TradeOption;
use App\Modules\Investments\Repositories\TradeOptionRepository;
use App\Modules\Investments\Repositories\TidHistoryTradeOptionRepository;
use App\Modules\Investments\Repositories\TradeHistoryRepository;
use App\Modules\Investments\Repositories\TradeRepository;
use DateTime;
use Exception;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ArrayShape;
use function sprintf;

/**
 * Investment service.
 */
final class InvestmentsChartService
{
    public const PERIOD_10 = '10';
    public const PERIOD_30 = '30';
    public const PERIOD_60 = '60';
    public const PERIOD_90 = '90';
    public const PERIOD_ALL = 'all';

    public const TYPE_BY_TIDS = 'by_tids';
    public const TYPE_WITH_OPTIONS = 'with_options';

    public const CHART_PERIODS = [
        '10',
        '30',
        '60',
        '90',
        'all'
    ];

    /**
     * Constructor.
     *
     * @param TradeRepository $tradeRepository
     * @param TradeOptionRepository $tradeOptionRepository
     * @param \App\Modules\Investments\Repositories\TradeHistoryRepository $tradeHistoryRepository
     * @param \App\Modules\Investments\Repositories\TidHistoryTradeOptionRepository $tidHistoryTradeOptionRepository
     */
    public function __construct(
        private TradeRepository $tradeRepository,
        private TradeOptionRepository $tradeOptionRepository,
        private TradeHistoryRepository $tradeHistoryRepository,
        private TidHistoryTradeOptionRepository $tidHistoryTradeOptionRepository
    )
    {
    }

    /**
     * Get chart data V2.
     *
     * @param User $user
     * @param \App\Modules\Investments\Models\Trade|null $trade
     * @param string|null $period
     * @return array
     * @throws Exception
     */
    public function chartV2(User $user, Trade $trade = null, string $period = null): array
    {
        /** @var Collection[] $dataList */
        $dataList = [];

        $trades = $trade ?
            collect([$trade]) :
            $this->tradeRepository->getByUser($user->id);

        foreach ($trades as $tradeItem) {
            $dataList[] = $this->tradeHistoryRepository
                ->getLastDeals($tradeItem->id)
                ->sortBy('id');
        }

        return $this->getChartDataV2(
            $user,
            $dataList,
            $trades,
            $trade !== null,
            $period ?? static::PERIOD_ALL
        );
    }

    /**
     * Get chart by option.
     *
     * @param TradeOption $tradeOption
     * @param string|null $period
     * @return ChartOptionDto
     * @throws Exception
     */
    public function chartByOption(TradeOption $tradeOption, string $period = null): ChartOptionDto
    {
        $ttl = collect();
        $pnl = collect();
        $days = collect();
        $startDateFixed = null;
        $endDate = null;

        if ($tradeOption->chart_enabled) {
            $nowDate = new DateTime((new DateTime())->format('Y-m-d 00:00:00'));
            $startDateBase = new DateTime();

            if ((int)$nowDate->format('d') === 1) {
                $startDateBase->modify('-1 DAY');
            }

            $startDateFixed = new DateTime($startDateBase->format('Y-m-01 00:00:00'));
            $startDate = clone $startDateFixed;
            $endDate = (clone $startDate)->modify('+1 MONTH -1 DAY');
            $diff = $endDate->diff($startDate);
            $sum = 0;
            $list = $this->tidHistoryTradeOptionRepository->getByTradeOptionsId(
                $tradeOption->id,
                [$startDate, (new DateTime())->modify('-1 DAY')]
            );

            $firstPoint = $startDate->format('M');

            $ttl->add([
                'y' => 0,
                'x' => $firstPoint
            ]);

            $pnl->add([
                'y' => 0,
                'x' => $firstPoint
            ]);

            $days->add($firstPoint);

            for ($i = 0; $i <= $diff->days; $i++) {
                $currentDate = clone($i > 0 ? $startDate->modify('+ 1 DAY') : $startDate);
                $date = $currentDate->format('d/m/Y');

                if ($currentDate < $nowDate) {
                    $item = $list->where('date', $currentDate->format('Y-m-d'))->first();
                    $percent = 0;

                    if ($item) {
                        $sum += $item->percent;
                        $percent = $item->percent;
                    }

                    $ttl->add([
                        'y' => round($sum, 2),
                        'x' => $date
                    ]);

                    $pnl->add([
                        'y' => round($percent, 2),
                        'x' => $date
                    ]);
                }

                $days->add($date);
            }
        }

        $optionInfo = collect();
        $optionInfo->put('title', $tradeOption->title);
        $optionInfo->put('enabled', $tradeOption->chart_enabled);

        if ($tradeOption->chart_enabled) {
            $previousStart = new DateTime(
                (clone $startDateFixed)
                    ->modify('-1 MONTH')
                    ->format('Y-m-01 00:00:00')
            );
            $previousEnd = (clone $previousStart)
                ->modify('+1 MONTH -1 DAY');

            $optionInfo->put('description', $tradeOption->description);
            $optionInfo->put('requirements', $tradeOption->requiredCards->pluck('title'));
            $optionInfo->put('statement_link', $tradeOption->statement_link);

            $optionInfo->put('from', $startDateFixed?->format('d/m/Y'));
            $optionInfo->put('to', $endDate?->format('d/m/Y'));
            $optionInfo->put(
                'previous',
                $tradeOption->previous_percent === null ?
                    round($this->tidHistoryTradeOptionRepository->sumPercentByDate(
                        $tradeOption->id,
                        [$previousStart, $previousEnd]
                    ), 2) :
                    $tradeOption->previous_percent
            );
        }

        if ($this->getTimeProtectionService()
            ->checkTime()) {
            return new ChartOptionDto(
                $ttl,
                $pnl,
                $days,
                $optionInfo
            );
        } else {
            $optionInfo = collect();
            $optionInfo->put('title', $tradeOption->title);
            $optionInfo->put('enabled', false);

            return new ChartOptionDto(
                collect(),
                collect(),
                collect(),
                $optionInfo
            );
        }
    }

    /**
     * Get chart data V2.
     *
     * @param User $user
     * @param array $tradeHistory
     * @param Collection $trades
     * @param bool $byTrades
     * @param string $period
     * @return array
     * @throws Exception
     */
    private function getChartDataV2(
        User $user,
        array $tradeHistory,
        Collection $trades,
        bool $byTrades,
        string $period
    ): array
    {
        $realPeriod = $this->getRealPeriod($trades, $period);

        $startDate = (new DateTime())
            ->modify(sprintf('-%s DAYS', $realPeriod));
        $currentDate = clone $startDate;

        $dataRaw = $this->getChartDataDefaults($user, $trades, $byTrades, $startDate, $period);
        $data = $dataRaw['data'];
        $allSum = $dataRaw['all_sum'];
        $optionByDateDefault = $dataRaw['option_by_date_default'];

        for ($i = 1; $i <= $realPeriod; $i++) {
            $pointsExists = $optionByDateDefault;
            $pointPnl = $optionByDateDefault;

            foreach ($tradeHistory as $key => $item) {
                $tradeHistoryItem = $item->where('date', $currentDate->format('Y-m-d'))->first();

                if ($tradeHistoryItem) {
                    $key = $byTrades ?
                        $tradeHistoryItem->trade->tid :
                        $tradeHistoryItem->trade->tradeOption->id;

                    $pointsExists[$key]++;

                    $allSum[$key] += $tradeHistoryItem->profit_usdt;
                    $pointPnl[$key] += $tradeHistoryItem->profit_usdt;
                }
            }

            $pointData = $currentDate->format('d/m/Y');
            $data['days'][] = $pointData;

            foreach ($allSum as $key => $sum) {
                if ($pointsExists[$key] > 0) {
                    $data['ttl'][$key][] = [
                        'x' => $pointData,
                        'y' => $sum
                    ];
                    $data['pnl'][$key][] = [
                        'x' => $pointData,
                        'y' => $pointPnl[$key]
                    ];
                }
            }

            $currentDate->modify('+1 DAY');
        }

        return $this->roundAmounts(
            $this->removeUnusedOptions(
                $data,
                $trades
            )
        );
    }

    /**
     * Get real chart period.
     *
     * @param Collection $trades
     * @param string $period
     * @return int
     * @throws Exception
     */
    private function getRealPeriod(Collection $trades, string $period): int
    {
        $fromDate = $period !== static::PERIOD_ALL ?
            (new DateTime())->modify(sprintf('-%s DAYS', $period)) :
            null;
        $firstDeal = $this->tradeHistoryRepository->getFirstDealByTrades(
            $trades->pluck('id')->toArray(),
            $fromDate
        );
        $subPeriodDefault = null;

        if ($firstDeal) {
            $interval = (new DateTime())->diff(new DateTime($firstDeal->date));

            if ($interval->invert === 1) {
                $subPeriodDefault = $interval->days;
            }
        }

        if ($subPeriodDefault === null) {
            $subPeriod = $period === static::PERIOD_ALL ?
                static::PERIOD_10 :
                $period;
        } else {
            $subPeriod = $period !== static::PERIOD_ALL && $subPeriodDefault > (int)$period ?
                $period :
                $subPeriodDefault;
        }

        return (int)$subPeriod;
    }

    /**
     * Remove unused options in data.
     *
     * @param array $data
     * @param Collection $trades
     * @return array
     */
    private function removeUnusedOptions(array $data, Collection $trades): array
    {
        $usedOptions = $trades->groupBy('trade_options_id')
            ->keys()
            ->toArray();

        if ($data['type'] === static::TYPE_WITH_OPTIONS) {
            foreach ($data['ttl'] as $key => $list) {
                if (!in_array($key, $usedOptions)) {
                    unset($data['ttl'][$key]);
                }
            }

            foreach ($data['pnl'] as $key => $list) {
                if (!in_array($key, $usedOptions)) {
                    unset($data['pnl'][$key]);
                }
            }

            foreach ($data['legend'] as $key => $options) {
                if (!in_array($key, $usedOptions)) {
                    unset($data['legend'][$key]);
                }
            }
        }

        return $data;
    }

    /**
     * Round amounts in data.
     *
     * @param array $data
     * @return array
     */
    private function roundAmounts(array $data): array
    {
        $callback = function ($value) {
            $value['y'] = round($value['y'], 2);
            return $value;
        };

        foreach ($data['ttl'] as $key => $list) {
            $data['ttl'][$key] = array_map($callback, $list);
        }

        foreach ($data['pnl'] as $key => $list) {
            $data['pnl'][$key] = array_map($callback, $list);
        }

        return $data;
    }

    /**
     * Get chart data defaults.
     *
     * @param User $user
     * @param Collection $trades
     * @param bool $byTrades
     * @param DateTime $currentDate
     * @param string $period
     * @return array
     */
    #[ArrayShape([
        'data' => "array",
        'all_sum' => "array",
        'option_by_date_default' => "array"
    ])]
    private function getChartDataDefaults(
        User $user,
        Collection $trades,
        bool $byTrades,
        DateTime $currentDate,
        string $period
    ): array
    {
        $tids = [];

        foreach ($trades as $trade) {
            $tids[$trade->tid] = (string)$trade->tradeOption->id;
        }

        $data = [
            'period' => $period,
            'type' => ($byTrades ? static::TYPE_BY_TIDS : static::TYPE_WITH_OPTIONS),
            'ttl' => [],
            'pnl' => [],
            'days' => [],
            'legend' => [],
            'tids' => $tids,
            'periods' => static::CHART_PERIODS
        ];
        $rawOptions = $this->tradeOptionRepository
            ->all();
        $data['legend'] = $rawOptions->pluck('title', 'id')->toArray();
        $optionByDateDefault = array_fill_keys(
            array_keys($byTrades ?
                $tids : $data['legend']),
            0
        );

        $beforeAt = $period === static::PERIOD_ALL ? null : $currentDate;

        if ($byTrades) {
            $allSum = $this->tradeRepository->getPnlSumByTrades(
                $trades->pluck('id')->toArray(),
                $beforeAt
            );

            $data['pnl'] = array_fill_keys(
                array_keys($allSum),
                []
            );
            $data['ttl'] = $data['pnl'];
        } else {
            $allSum = $this->tradeRepository->getPnlSumWithOptions($user->id, $beforeAt);
            $data['pnl'] = array_fill_keys(
                $rawOptions
                    ->pluck('id')
                    ->toArray(),
                []
            );
            $data['ttl'] = $data['pnl'];
        }

        return [
            'data' => $data,
            'all_sum' => $allSum,
            'option_by_date_default' => $optionByDateDefault
        ];
    }

    /**
     * Get time protection service.
     *
     * @return TimeProtectionService
     */
    private function getTimeProtectionService(): TimeProtectionService
    {
        return app(TimeProtectionService::class);
    }
}
