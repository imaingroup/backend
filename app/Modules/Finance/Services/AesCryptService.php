<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services;

use App\Modules\Finance\Exceptions\AesCryptException;


/**
 * Proxy aes crypt.
 */
final class AesCryptService
{
    public const AES_256_CBC = 'aes-256-cbc';

    /**
     * Encrypt data.
     *
     * @param string $key
     * @param string $data
     * @return string
     */
    public function encrypt(string $key, string $data): string
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(self::AES_256_CBC));
        $encrypted = openssl_encrypt($data, self::AES_256_CBC, $key, 0, $iv);

        return $encrypted . ':' . base64_encode($iv);
    }

    /**
     * Decrypt data.
     *
     * @param string $key
     * @param string $data
     * @return string
     * @throws AesCryptException
     */
    public function decrypt(string $key, string $data): string
    {
        $parts = explode(':', $data);

        if (count($parts) !== 2) {
            throw new AesCryptException();
        }

        return openssl_decrypt($parts[0], self::AES_256_CBC, $key, 0, base64_decode($parts[1]));
    }
}
