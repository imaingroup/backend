<?php
declare(strict_types=1);

/*
 * This file is part of the NTPClient package.
 *
 * (c) Krzysztof Mazur <krz@ychu.pl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Modules\Core\Libraries\NtpClient\Exceptions;

/**
 * @author Krzysztof Mazur <krz@ychu.pl>
 */
class UnableToConnectException extends ConnectionException
{
}
