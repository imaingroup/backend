<?php


namespace App\Modules\Notifications\Repositories;

use App\Modules\Notifications\Models\Mail;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Mail repository.
 */
final class MailRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Notifications\Models\Mail $model
     */
    public function __construct(protected Mail $model)
    {}
}
