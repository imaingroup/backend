<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration deletes old language data.
 */
class RemoveOldLangData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('translations')
            ->where('table_name', 'data_rows')
            ->orWhere('table_name', 'data_types')
            ->orWhere('table_name', 'menu_items')
            ->delete();

        $data = [
            'user_activities' => ['User activity', 'User activities'],
            'user_actions' => ['User action', 'User actions'],
            'profit_cards' => ['Profit card', 'Profit cards'],
            'charts' => ['Chart', 'Charts'],
            'trade_options' => ['Trade option', 'Trade options'],
            'trades' => ['Tid', 'Tid\'s'],
            'deposits' => ['Deposit', 'Deposits'],
            'transactions' => ['Transaction', 'Transactions'],
            'site_settings' => ['Site settings', 'Transactions'],
            'news_categories' => ['News category', 'News categories'],
            'subscribes' => ['Subscribe', 'Subscribes'],
            'contacts' => ['Contact', 'Contacts'],
        ];

        foreach ($data as $name => $translates) {
            DB::table('data_types')
                ->where('name', $name)
                ->update([
                    'display_name_singular' => $translates[0],
                    'display_name_plural' => $translates[1],
                ]);
        }

        $data2 = [
            [1, 'langs_id', 'Language'],
            [1, 'user_belongsto_lang_relationship', 'Language'],
            [12, 'price', 'Price'],
            [1, 'remember_token', 'Restore token'],
            [23, 'type_coin', 'Coin type'],
            [28, 'type', 'Type'],
            [30, 'text_area_exchange', 'Text "Exchange USDT to ABC"'],
            [23, 'amount_usdt', 'Amount (USDT)'],
            [23, 'amount_coin', 'Amount (Coins)'],
            [19, 'amount', 'Amount (ABC)'],
            [28, 'amount', 'Amount'],
            [1, 'ip_country', 'IP country'],
            [30, 'abc_price', 'ABC price'],
            [23, 'status', 'Status'],
            [30, 'bot_statement_link', 'Bot statement link'],
            [12, 'sort', 'Sort'],
            [15, 'sort', 'Sort'],
            [33, 'sort', 'Sort'],
            [38, 'message', 'Message'],
            [40, 'created_at', 'Created date'],
            [6, 'created_at', 'Created date'],
            [12, 'created_at', 'Created date'],
            [15, 'created_at', 'Created date'],
            [19, 'created_at', 'Created date'],
            [23, 'created_at', 'Created date'],
            [28, 'created_at', 'Created date'],
            [30, 'created_at', 'Created date'],
            [33, 'created_at', 'Created date'],
            [37, 'created_at', 'Created date'],
            [38, 'created_at', 'Created date'],
            [39, 'created_at', 'Created date'],
            [1, 'is_hide_from_public', 'Hide public account'],
            [1, 'role_id', 'Role'],
            [1, 'user_belongstomany_role_relationship', 'Roles'],
            [1, 'promo_code', 'Promo code'],
            [30, 'is_view_balance_recent_transactions', 'View balance in resent activities'],
            [12, 'is_visible', 'Show'],
            [15, 'is_visible', 'Show'],
            [33, 'is_visible', 'Show'],
            [33, 'id', 'ID'],
            [1, 'password', 'Password'],
            [6, 'os', 'OS'],
            [30, 'timer_description', 'Timer description'],
            [40, 'updated_at', 'Updated date'],
            [1, 'updated_at', 'Updated date'],
            [6, 'updated_at', 'Updated date'],
            [12, 'updated_at', 'Updated date'],
            [15, 'updated_at', 'Updated date'],
            [19, 'updated_at', 'Updated date'],
            [28, 'updated_at', 'Updated date'],
            [30, 'updated_at', 'Updated date'],
            [33, 'updated_at', 'Updated date'],
            [37, 'updated_at', 'Updated date'],
            [38, 'updated_at', 'Updated date'],
            [39, 'updated_at', 'Updated date'],
            [23, 'updated_at', 'Updated date'],
            [12, 'abc', 'Amount (ABC)'],
            [1, 'settings', 'Settings'],
            [30, 'timer_title', 'Timer title'],
            [12, 'title', 'Title'],
            [33, 'title', 'Title'],
            [30, 'max_withdraw_per_month', 'Maximum withdraw in month (USDT)'],
            [30, 'rate_updated_at', 'Rate updated date'],
            [30, 'rate_btc_usd', 'Rate (BTC/USD)'],
            [23, 'rate', 'Rate'],
            [28, 'comment', 'Comment'],
            [30, 'withdraw_fee', 'Withdraw commission'],
            [30, 'timer_before', 'Timer finish date'],
            [1, 'name', 'Name'],
            [38, 'name', 'Name'],
            [12, 'image', 'Image'],
            [38, 'is_processed', 'Request processed'],
            [12, 'available_count', 'Available count'],
            [6, 'additional', 'Additional'],
            [19, 'opened_at', 'Opened date'],
            [6, 'logged_at', 'Action date'],
            [28, 'transaction_at', 'Date'],
            [28, 'data', 'Data'],
            [19, 'trade_option', 'Tariff dump'],
            [28, 'currency', 'Currency'],
            [6, 'browser', 'Browser'],
            [12, 'bonus', 'Bonud (ABC)'],
            [1, 'is_enabled_withdrawal_notifications', 'Enabled withdrawal notifications'],
            [1, 'is_enabled_deposit_notifications', 'Enabled deposit notifications'],
            [1, 'is_enabled_purchase_notifications', 'Enabled purchase notifications'],
            [1, 'is_enabled_password_change_notifications', 'Enabled password changing notifications'],
            [30, 'timer_enabled', 'Timer enabled'],
            [23, 'address', 'Address'],
            [19, 'trade_options_id', 'Tariff id'],
            [6, 'users_id', 'Tariff id'],
            [19, 'users_id', 'User id'],
            [23, 'users_id', 'User id'],
            [28, 'users_id', 'User id'],
            [37, 'users_id', 'User id'],
            [38, 'users_id', 'User id'],
            [15, 'html', 'HTML code'],
            [30, 'notifications_deposit_emails', 'Email list deposit notifications'],
            [1, 'is_accepted_cookies', 'Cookie accepted'],
            [1, 'btc', 'BTC address'],
            [1, 'is_trader', 'I trader'],
        ];

        foreach ($data2 as $item) {
            DB::table('data_rows')
                ->where('data_type_id', $item[0])
                ->where('field', $item[1])
                ->update([
                    'display_name' => $item[2]
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
