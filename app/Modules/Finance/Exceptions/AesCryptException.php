<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions;

/**
 * Aes crypt exception.
 */
final class AesCryptException extends FinanceException
{
}
