<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Finance;

use App\Modules\Finance\Enum\NetworkTypeEnum;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * AdminFinanceGethArchiveRequest.
 *
 * @property-read CryptoAddress|mixed $cryptoAddress
 */
final class AdminFinanceGethArchiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            $user->hasRole(User::ROLE_ADMIN_NAME) &&
            in_array($user->id, User::USERS_ALLOWED_SEND_MONEY) &&
            $this->cryptoAddress instanceof CryptoAddress &&
            $this->cryptoAddress->currency->network->toEnum()->type() === NetworkTypeEnum::ETHEREUM;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [ ];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): CryptoAddress
    {
        return $this->cryptoAddress;
    }
}
