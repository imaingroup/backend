<?php
declare(strict_types=1);

namespace App\Modules\Messages\Models;

use App\Modules\Core\Models\Model;

final class MessageRead extends Model
{
    /** {@inheritdoc} */
    protected $table = 'messages_read';

    /** {@inheritdoc} */
    protected $fillable = [
        'messages_id',
        'users_id',
    ];
}
