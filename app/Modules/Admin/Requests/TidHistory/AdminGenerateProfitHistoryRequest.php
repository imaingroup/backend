<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\TidHistory;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * This request class validate data for generate
 * profit history request.
 */
final class AdminGenerateProfitHistoryRequest extends FormRequest
{
    private float $minPercentPerDay = 0;
    private float $minNegativePercentPerDay = 0.1;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'period' => 'required|integer|min:1|max:1000|regex:/^\d+$/',
            'percent' => 'required|scalar|regex:/^-?\d+(\.\d{0,2})?$/',
            'positive_days' => 'required|integer|min:0',
            'high_max' => 'required|scalar|regex:/^\d+(\.\d{0,2})?$/',
            'low_max' => 'required|scalar|regex:/^-?\d+(\.\d{0,2})?$/',
            'loss_sum_percent' => 'required|scalar|regex:/^\d+(\.\d{0,2})?$/',
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator)
    {
        $highMax = (float)$this['high_max'];
        $lowMax = (float)$this['low_max'];
        $percent = (float)$this['percent'];
        $positiveDays = (int)$this['positive_days'];
        $period = (int)$this['period'];
        $lossPercentSum = (float)$this['loss_sum_percent'];
        $negativePeriod = $period - $positiveDays;

        if (!($highMax > 0) && $percent > 0) {
            $validator->errors()->add('high_max', 'High max must be great than zero');
            return;
        } else if (!($period > 0)) {
            $validator->errors()->add('period', 'Period must be great than zero');
            return;
        } else if ($percent > 0 && !($positiveDays > 0)) {
            $validator->errors()->add('positive_days', 'Positive days must be great than current');
            return;
        }

        if ($negativePeriod === 0 && $lossPercentSum > 0) {
            $validator->errors()->add(
                'loss_sum_percent',
                'Loss sum percent must be zero for this parameters'
            );

            return;
        }

        if ($lowMax > 0) {
            $validator->errors()->add(
                'low_max',
                'Maximum low max must be 0'
            );

            return;
        }

        if ($positiveDays > $period) {
            $validator->errors()->add(
                'positive_days',
                sprintf('Maximum positive days must be %s', $period)
            );
        }

        if ($percent >= 0 && $highMax > $lossPercentSum + $percent) {
            $validator->errors()->add(
                'high_max',
                sprintf('Maximum high max for this parameters equals %s', $lossPercentSum + $percent)
            );

            return;
        }

        if ($percent >=0 && $lossPercentSum > 0 && abs($lowMax) > $lossPercentSum) {
            $validator->errors()->add(
                'low_max',
                sprintf('Minimum low max for this parameters equals -%s', $lossPercentSum)
            );

            return;
        }

        if ($lossPercentSum > 0 && $lossPercentSum / $negativePeriod < $this->minNegativePercentPerDay) {
            $validator->errors()->add(
                'loss_sum_percent',
                sprintf('Minimum loss sum must be %s', $this->minNegativePercentPerDay * $negativePeriod)
            );

            return;
        }

        if ($negativePeriod > 0 && !($lossPercentSum > 0) && $percent > 0) {
            $validator->errors()->add('loss_sum_percent', 'Minimum loss sum must be great than zero');

            return;
        }

        if($percent < 0 && $lossPercentSum > 0 && $positiveDays === 0) {
            $validator->errors()->add('positive_days', 'Positive days must be great than zero or loss sum must be zero');

            return;
        }

        $coefficient = $positiveDays > 1 ? 1.2 : 1;

        if($percent < 0 && $highMax / $coefficient < ($lossPercentSum / $positiveDays)) {
            $validator->errors()->add('high_max', sprintf('High max (negative period) for this parameters must be minimum %s', $lossPercentSum / $positiveDays * $coefficient));

            return;
        }

        if ($positiveDays > 0 && !($highMax / 1.2 >= ($percent + $lossPercentSum) / $positiveDays)) {
            $validator->errors()->add(
                'high_max',
                sprintf('Minimum high max for this parameters equals %s', ($percent + $lossPercentSum) / $positiveDays * 1.2)
            );

            return;
        }

        if ($lossPercentSum < $negativePeriod * $this->minPercentPerDay) {
            $validator->errors()->add(
                'loss_sum_percent',
                sprintf('Minimum loss sum percent for this parameters equals %s', $negativePeriod * $this->minPercentPerDay)
            );

            return;
        }

        if ($negativePeriod > 0 && !(abs($lowMax) / 1.4 >= $lossPercentSum / $negativePeriod)) {
            $validator->errors()->add(
                'low_max',
                sprintf('Maximum low max (negative period) for this parameters equals -%s', $lossPercentSum / $negativePeriod * 1.4)
            );

            return;
        }

        if($percent < 0 && $highMax < $lossPercentSum / $positiveDays) {
            $validator->errors()->add(
                'high_max',
                sprintf('Minimum high max (negative period) for this parameters equals %s', $lossPercentSum / $positiveDays)
            );

            return;
        }

        if($percent < 0 && $highMax > $lossPercentSum) {
            $validator->errors()->add(
                'high_max',
                sprintf('Maximum high max (negative period) for this parameters equals %s', $lossPercentSum)
            );

            return;
        }

        if($percent < 0 && abs($lowMax) > abs($percent) + $lossPercentSum) {
            $validator->errors()->add(
                'low_max',
                sprintf('Minimum low max (negative period) for this parameters equals -%s', abs($percent) + $lossPercentSum)
            );

            return;
        }

        if($percent < 0 && abs($lowMax) / 1.4 < ((abs($percent) + $lossPercentSum) / $negativePeriod)) {
            $validator->errors()->add(
                'low_max',
                sprintf('Maximum low max (negative period) for this parameters equals -%s', ((abs($percent) + $lossPercentSum) / $negativePeriod) * 1.4)
            );

            return;
        }
    }
}
