<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds min deposit field for other currencies.
 */
final class AddMinDepositForBtcAndEth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->float('deposit_min_btc', 16, 8)
                ->after('deposit_min_usdt')
                ->default(10);
        });

        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->float('deposit_min_eth', 16, 8)
                ->after('deposit_min_btc')
                ->default(10);
        });

        DB::transaction(function () {
            $typeId = DB::table('data_types')
                ->where('name', 'settings_deposit')
                ->first()
                ->id;

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'deposit_min_btc',
                    'type' => 'number',
                    'display_name' => 'Deposit min btc',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 0
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'deposit_min_eth',
                    'type' => 'number',
                    'display_name' => 'Deposit min eth',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 0
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->dropColumn(['deposit_min_btc', 'deposit_min_eth']);
        });

        DB::transaction(function() {
            $typeId = DB::table('data_types')
                ->where('name', 'settings_deposit')
                ->first()
                ->id;

            DB::table('data_rows')
                ->where('data_type_id', $typeId)
                ->whereIn('field', [
                    'deposit_min_btc',
                    'deposit_min_eth',
                ])
                ->delete();
        });
    }
}
