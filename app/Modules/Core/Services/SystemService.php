<?php


namespace App\Modules\Core\Services;


use App\Modules\Notifications\Jobs\SendDepositBlockAdminEmailJob;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Settings\Repositories\SettingsDepositRepository;
use App\Modules\Settings\Repositories\SettingsPriceRepository;
use Throwable;

/**
 * System service.
 */
final class SystemService
{
    /** @var \App\Modules\Settings\Repositories\SettingsDepositRepository Settings deposit repository. */
    private SettingsDepositRepository $settingsDepositRepository;

    /** @var SettingsPriceRepository Settings price repository. */
    private SettingsPriceRepository $settingsPriceRepository;

    /** @var \App\Modules\Logs\Loggers\Logger Logger. */
    private Logger $logger;

    /**
     * Constructor.
     *
     * @param \App\Modules\Settings\Repositories\SettingsDepositRepository $settingsDepositRepository
     * @param SettingsPriceRepository $settingsPriceRepository
     * @param \App\Modules\Logs\Loggers\Logger $logger
     */
    public function __construct(
        SettingsDepositRepository $settingsDepositRepository,
        SettingsPriceRepository $settingsPriceRepository,
        Logger $logger
    )
    {
        $this->settingsDepositRepository = $settingsDepositRepository;
        $this->settingsPriceRepository = $settingsPriceRepository;
        $this->logger = $logger;
    }

    /**
     * Block deposit module.
     *
     * @throws Throwable
     */
    public function blockFinanceModules()
    {
        $this->logger->error('Executed method SystemService::blockDepositModule');

        try {
            $settingsDeposit = $this->settingsDepositRepository->get();
            $settingsDeposit->is_deposit_block = true;
            $settingsDeposit->is_withdraw_block = true;

            $settingsDeposit->save();
        } finally {
            SendDepositBlockAdminEmailJob::dispatch();
        }
    }
}
