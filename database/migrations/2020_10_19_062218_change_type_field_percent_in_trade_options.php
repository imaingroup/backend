<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration changing type field percent
 * for table trade_options from tinyInteger to integer.
 */
class ChangeTypeFieldPercentInTradeOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->integer('percent')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->tinyInteger('percent')->change();
        });
    }
}
