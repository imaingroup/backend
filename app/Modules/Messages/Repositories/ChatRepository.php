<?php
declare(strict_types=1);

namespace App\Modules\Messages\Repositories;

use App\Modules\Messages\Models\Chat;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;

/**
 * Chat repository.
 */
final class ChatRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Chat $model
     */
    public function __construct(protected Chat $model)
    {
    }

    /**
     * Search single chat between two users.
     *
     * @param int $usersId
     * @param int $contactUsersId
     * @return Chat|null
     */
    public function getSingleChatByContact(int $usersId, int $contactUsersId): ?Chat
    {
        return $this->getChatsByContact($usersId, $contactUsersId, false)
            ->first();
    }

    /**
     * Search group chat by two users.
     *
     * @param int $usersId
     * @param int $contactUsersId
     * @return Collection
     */
    public function getGroupChatsByContact(int $usersId, int $contactUsersId): Collection
    {
        return $this->getChatsByContact($usersId, $contactUsersId, true, false);
    }

    /**
     * Get chats by contact.
     *
     * @param int $usersId
     * @param int $contactUsersId
     * @param bool|null $isGroup
     * @param bool $contactIsOwner
     * @return Collection
     */
    public function getChatsByContact(
        int $usersId,
        int $contactUsersId,
        bool $isGroup = null,
        bool $contactIsOwner = true
    ): Collection
    {
        $query = $this->model
            ->newQuery()
            ->select('mc.*')
            ->from('messages_chats', 'mc')
            ->join('messages_chats_users as mcu_u', function (JoinClause $join) use ($usersId, $isGroup, $contactIsOwner) {
                $join->on('mc.id', '=', 'mcu_u.messages_chats_id')
                    ->where('mcu_u.users_id', '=', $usersId)
                    ->where('mcu_u.deleted', false);

                if ($isGroup && $contactIsOwner) {
                    $join->where('mcu_u.is_owner', false);
                }
            })
            ->join('messages_chats_users as mcu_c', function (JoinClause $join) use ($contactUsersId, $isGroup, $contactIsOwner) {
                $join->on('mc.id', '=', 'mcu_c.messages_chats_id')
                    ->where('mcu_c.users_id', '=', $contactUsersId)
                    ->where('mcu_c.deleted', false);

                if ($isGroup && $contactIsOwner) {
                    $join->where('mcu_u.is_owner', true);
                }
            });

        if ($isGroup !== null) {
            $query->where('mc.is_group', $isGroup);
        }

        return $query->get();
    }

    /**
     * Get all chats by user
     * with sort by recently message.
     *
     * @param int $userId
     * @param Builder|null $messageQuery
     * @return Collection
     */
    public function getChats(int $userId, Builder $messageQuery = null): Collection
    {
        $baseQuery = $this
            ->getChatsBaseQuery($userId)
            ->with('chatUsers');

        if ($messageQuery) {
            $baseQuery
                ->addSelect([
                    'last_message_at' => $messageQuery
                        ->select('messages.created_at')
                        ->whereColumn('messages.messages_chats_id', 'messages_chats.id')
                        ->orderBy('messages.created_at', 'DESC')
                        ->limit(1)
                ])
                ->orderBy('last_message_at', 'DESC');
        }

        return $this->addUserSelection(
            $baseQuery,
            $userId
        )
            ->get();
    }

    /**
     * Get chat query selection for user.
     *
     * @param int $usersId
     * @return Builder
     */
    public function getChatsQuery(int $usersId): Builder
    {
        return $this->addUserSelection(
            $this->getChatsBaseQuery($usersId),
            $usersId
        );
    }

    /**
     * Exists user in chat.
     *
     * @param int $chatId
     * @param int $usersId
     * @return bool
     */
    public function existsChatUser(int $chatId, int $usersId): bool
    {
        return $this->model
            ->newQuery()
            ->from('messages_chats', 'mc')
            ->where('mc.id', $chatId)
            ->join('messages_chats_users as mcu', function (JoinClause $join) use ($usersId) {
                $join->on('mc.id', '=', 'mcu.messages_chats_id')
                    ->where('mcu.users_id', '=', $usersId)
                    ->where('mcu.deleted', false);

            })
            ->exists();
    }

    /**
     * Exists user in chat.
     *
     * @param int $chatId
     * @param int $usersId
     * @return bool
     */
    public function checkAccessChat(int $chatId, int $usersId): bool
    {
        return $this
            ->getChatsBaseQuery($usersId, true)
            ->where('messages_chats.id', $chatId)
            ->join('messages_chats_users as mcu', function (JoinClause $join) use ($usersId) {
                $join->on('messages_chats.id', '=', 'mcu.messages_chats_id')
                    ->where('mcu.users_id', '=', $usersId)
                    ->where('mcu.deleted', false);
            })
            ->exists();
    }

    /**
     * Get all chats by user
     * with sort by recently message.
     *
     * @param Builder|null $messageQuery
     * @return Collection
     */
    public function getAdminChats(Builder $messageQuery = null): Collection
    {
        $baseQuery = $this->model
            ->newQuery()
            ->select('messages_chats.*')
            ->with('chatUsers');

        if ($messageQuery) {
            $baseQuery
                ->addSelect([
                    'last_message_at' => (clone $messageQuery)
                        ->select('messages.created_at')
                        ->whereColumn('messages.messages_chats_id', 'messages_chats.id')
                        ->orderBy('messages.created_at', 'DESC')
                        ->limit(1)
                ])
                ->addSelect([
                    'count_messages' => (clone $messageQuery)
                        ->select([])
                        ->selectRaw('COUNT(messages.id)')
                        ->whereColumn('messages.messages_chats_id', 'messages_chats.id')
                ])
                ->addSelect([
                    'count_messages_new' => (clone $messageQuery)
                        ->select([])
                        ->selectRaw('COUNT(messages.id)')
                        ->whereColumn('messages.messages_chats_id', 'messages_chats.id')
                        ->where('messages.viewed_admin', false)
                ])
                ->orderBy('last_message_at', 'DESC');
        }

        return $baseQuery
            ->get();
    }

    /**
     * Get group chats by owner.
     *
     * @param int $ownerId
     * @return Collection
     */
    public function getGroupChatsByOwner(int $ownerId): Collection
    {
        return $this->model
            ->newQuery()
            ->select('messages_chats.*')
            ->with('chatUsers')
            ->join('messages_chats_users as mcu_u', function (JoinClause $join) use ($ownerId) {
                $join->on('messages_chats.id', '=', 'mcu_u.messages_chats_id')
                    ->where('mcu_u.users_id', '=', $ownerId)
                    ->where('mcu_u.deleted', false)
                    ->where('mcu_u.is_owner', true);
            })
            ->where('messages_chats.is_group', true)
            ->get();
    }

    /**
     * Get chats base query.
     *
     * @param int $usersId
     * @param bool $withoutChatDeleted
     * @return Builder
     */
    private function getChatsBaseQuery(int $usersId, bool $withoutChatDeleted = false): Builder
    {
        $query = $this->model
            ->newQuery()
            ->select('messages_chats.*');

        if (!$withoutChatDeleted) {
            $query->leftJoin('messages_chats_deleted as mcd', function (JoinClause $join) use ($usersId) {
                $join->on('messages_chats.id', '=', 'mcd.messages_chats_id')
                    ->where('mcd.users_id', '=', $usersId);
            })->whereNull('mcd.id');
        }

        return $query->where(function (Builder $query) use ($usersId) {
            $query->where(function (Builder $query) use ($usersId) {
                $query
                    ->where('messages_chats.is_group', false)
                    ->whereDoesntHave('chatUsers', function (Builder $query) use ($usersId) {
                        $query
                            ->join('messages_contacts as mcb', function (JoinClause $join) use ($usersId) {
                                $join->on('messages_chats_users.users_id', '=', 'mcb.contact_users_id')
                                    ->where('mcb.users_id', '=', $usersId);
                            })
                            ->where('mcb.blocked', true);
                    });
            })->orWhere(function (Builder $query) use ($usersId) {
                $query
                    ->where('messages_chats.is_group', true)
                    ->whereDoesntHave('chatUsers', function (Builder $query) use ($usersId) {
                        $query
                            ->join('messages_contacts as mcbg', function (JoinClause $join) use ($usersId) {
                                $join->on('messages_chats_users.users_id', '=', 'mcbg.contact_users_id')
                                    ->where('mcbg.users_id', '=', $usersId);
                            })
                            ->where('mcbg.blocked', true)
                            ->where('messages_chats_users.is_owner', true);
                    });
            });
        });
    }

    /**
     * Add user selection.
     *
     * @param Builder $builder
     * @param int $usersId
     * @return Builder
     */
    private function addUserSelection(Builder $builder, int $usersId): Builder
    {
        return $builder->join('messages_chats_users as mcu_u', function (JoinClause $join) use ($usersId) {
            $join
                ->on('messages_chats.id', '=', 'mcu_u.messages_chats_id')
                ->where('mcu_u.users_id', '=', $usersId)
                ->where('mcu_u.deleted', false);
        });
    }
}

