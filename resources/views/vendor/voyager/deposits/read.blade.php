@extends('voyager::bread-base.read')

@section('append_view_content')
    <hr style="margin:0;"/>
    <div class="panel-heading" style="border-bottom:0;">
        <h3 class="panel-title">Income transactions</h3>
    </div>
    <div class="panel-body" style="padding-top:0;">
        <?php $transactions = $dataTypeContent->depositTransactions;?>

        @if($transactions->count() > 0)
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Creation date</th>
                    <th>Confirmation date</th>
                    <th>Amount (Coins)</th>
                    <th>Amount (USDT)</th>
                    <th>Status</th>
                    <th>Txid</th>
                    <th>Related transaction</th>
                </tr>
                @foreach($transactions as $transaction)
                    <tr>
                        <td>{{ $transaction->id }}</td>
                        <td>{{ $transaction->created_at}}</td>
                        <td>{{ $transaction->check_confirmed ? $transaction->updated_at : '-'}}</td>
                        <td>{{ $transaction->amount_coin }}</td>
                        <td>{{ $transaction->amount_usdt }}</td>
                        <td>{{ $transaction->status_text }}</td>
                        <td>{{ $transaction->txid }}</td>
                        <td>{{ $transaction->related_transactions_id ?? '-' }}</td>
                    </tr>
                @endforeach
            </table>
        @else
            No incoming transactions.
        @endif
    </div>

    <hr style="margin:0;"/>
    <div class="panel-heading" style="border-bottom:0;">
        <h3 class="panel-title">Add transaction manually</h3>
    </div>
    <div class="panel-body" style="padding-top:0;">
        <div id="container-add-transaction"
             style="padding-left: 0;padding-right: 0;">
            <form @submit.prevent="addTransaction" class="add_transaction_form">
                <div class="add_transaction">
                    <div class="form-group col-md-6">
                        <input type="text" v-model="form.txid" placeholder="Txid" class="form-control"/>

                        <div class="text-error" v-if="errors.txid"
                             v-for="error in errors.txid">@{{ error }}
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <input type="text" v-model="form.amount" placeholder="Amount" class="form-control"/>

                        <div class="text-error" v-if="errors.amount"
                             v-for="error in errors.amount">@{{ error }}
                        </div>
                    </div>
                </div>

                <input type="submit" style="margin-top: 15px;width: 25%"
                       value="Add transaction" class="form-control"/>
            </form>
        </div>
    </div>

    <style>
        .add_transaction_form .form-group {
            padding-right: 0;
            padding-left: 0;
        }

        .add_transaction_form .form-group:first-child {
            padding-right: 15px;
        }

        .add_transaction_form input[type=submit] {
            clear: both;
        }
    </style>
@endsection

@push('javascript')
    <script>
        new Vue({
            el: '#container-add-transaction',
            data: () => {
                return {
                    loading: false,
                    form: {
                        txid: null,
                        amount: []
                    },
                    errors: {},
                }
            },
            methods: {
                addTransaction() {
                    if (this.loading || !confirm(`Do you confirm this action ? You want to add ${this.form.amount} to user's balance.`)) {
                        return;
                    }

                    this.loading = true;

                    axios.post('{{ route('voyager.deposit.add.transaction', $dataTypeContent) }}', this.form)
                        .then(() => {
                            this.form.txid = null;
                            this.form.amount = [];
                            this.errors = {};

                            toastr.success('Transaction was added.', 'Add transaction.');

                            setTimeout(() => {
                                window.location.reload();
                            }, 100);
                        })
                        .catch((e) => {
                            if (e.response.status === 422) {
                                this.errors = e.response.data.errors;
                            } else if (e.response.status == 401) {
                                toastr.error('You are not authorized.', 'Add transaction.');
                            } else if (e.response.status == 403) {
                                toastr.error('Action forbidden.', 'Add transaction.');
                            } else {
                                toastr.error('Server error.', 'Add transaction.');
                            }

                            if (e.response.status !== 422) {
                                this.errors = {};
                            }
                        })
                        .finally(() => {
                            this.loading = false;
                        });
                },
            },
        });
    </script>
@endpush
