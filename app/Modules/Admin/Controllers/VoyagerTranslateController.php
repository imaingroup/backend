<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\Languages\Repositories\LangRepository;
use App\Modules\Admin\Services\TranslateService;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Controller;

final class VoyagerTranslateController extends Controller
{
    private LangRepository $langRepository;
    private TranslateService $translateService;

    public function __construct(LangRepository $langRepository, TranslateService $translateService)
    {
        $this->langRepository = $langRepository;
        $this->translateService = $translateService;
    }

    public function list()
    {
        return Voyager::view('voyager::translate.list', [
            'languages' => $this->langRepository->all()
        ]);
    }

    public function edit(string $lang)
    {
        $langObject = $this->langRepository->findByShortName($lang);

        if(!$langObject) {
            abort(404);
        }

        return Voyager::view('voyager::translate.edit', [
            'lang' => $langObject,
            'data' => $this->translateService->loadForEdit($lang)
        ]);
    }

    public function save(string $lang, Request $request)
    {
        $langObject = $this->langRepository->findByShortName($lang);

        if(!$langObject) {
            abort(404);
        }

        $data = $request->all();
        unset($data['_token']);

        $this->translateService->updateLanguageFiles($lang, $data);

        return redirect(route('voyager.translate'))->with([
            'message'    => __('voyager::generic.successfully_updated')." Переводы",
            'alert-type' => 'success',
        ]);
    }
}
