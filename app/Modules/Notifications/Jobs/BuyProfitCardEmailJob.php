<?php

namespace App\Modules\Notifications\Jobs;

use App\Modules\Notifications\Mails\MailSender;
use App\Modules\Notifications\Mails\ProfitCardEmail;
use App\Modules\ProfitCards\Models\ProfitCardBuyHistory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Buy profit card mail job.
 */
final class BuyProfitCardEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var ProfitCardBuyHistory Buy profit card. */
    private ProfitCardBuyHistory $profitCardBuyHistory;

    /**
     * Create a new mail instance.
     *
     * @param ProfitCardBuyHistory $profitCardBuyHistory
     */
    public function __construct(ProfitCardBuyHistory $profitCardBuyHistory)
    {
        $this->profitCardBuyHistory = $profitCardBuyHistory;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MailSender::send($this->profitCardBuyHistory->user->email, new ProfitCardEmail($this->profitCardBuyHistory));
    }
}
