<?php


namespace App\Modules\Account\Repositories;

use App\Modules\Notifications\Models\NotificationUser;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Notification user repository.
 */
final class NotificationUserRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Notifications\Models\NotificationUser $model
     */
    public function __construct(protected NotificationUser $model)
    {}
}
