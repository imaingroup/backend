<?php

namespace App\Modules\Access\Controllers;

use App\Modules\Admin\Exceptions\TranslateException;
use App\Http\Controllers\Controller;
use App\Modules\Access\Requests\ActivateRequest;
use App\Modules\Access\Requests\ResendActivateRequest;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Access\Services\ActivateService;
use App\Modules\Access\Services\RegisterService;
use App\Modules\Admin\Services\TranslateService;

/**
 * Activate controller.
 */
final class ActivateController extends Controller
{
    /** @var ActivateService Activate service. */
    private ActivateService $activateService;

    /** @var RegisterService Register service. */
    private RegisterService $registerService;

    /** @var Logger Logger service. */
    private Logger $logger;

    /** @var \App\Modules\Admin\Services\TranslateService Translate service. */
    private TranslateService $translateService;

    /**
     * Constructor.
     *
     * @param ActivateService $activateService
     * @param \App\Modules\Access\Services\RegisterService $registerService
     * @param \App\Modules\Logs\Loggers\Logger $logger
     * @param \App\Modules\Admin\Services\TranslateService $translateService
     */
    public function __construct(
        ActivateService $activateService,
        RegisterService $registerService,
        Logger $logger,
        TranslateService $translateService
    )
    {
        $this->activateService = $activateService;
        $this->registerService = $registerService;
        $this->logger = $logger;
        $this->translateService = $translateService;
    }

    /**
     * Activate.
     *
     * @param ActivateRequest $request
     * @return array
     */
    public function activate(ActivateRequest $request): array
    {
        $this->activateService->activate($request->validated()['user']);

        return ['status' => true];
    }

    /**
     * Account already activated.
     *
     * @return array
     */
    public function alreadyActivated(): array
    {
        return [
            'Your account was already activated'
        ];
    }

    /**
     * Get content action.
     * @return array
     * @throws TranslateException
     */
    public function content(): array
    {
        return [
            'data' =>
                [
                    'translates' => $this->translateService->getTranslates(
                        [
                            'base',
                            'page_access'
                        ]
                    )
                ]
        ];
    }

    /**
     * Resend action link.
     *
     * @param \App\Modules\Access\Requests\ResendActivateRequest $request
     * @return array
     */
    public function resend(ResendActivateRequest $request): array
    {
        $this->activateService->resend(
            $request->getSession()->get('current_user')
        );

        $request->getSession()->put('resend_activate_request_at', new \DateTime());

        return ['status' => true];
    }
}
