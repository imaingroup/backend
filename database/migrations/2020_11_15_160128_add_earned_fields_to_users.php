<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migrations add earned fields to users table.
 */
class AddEarnedFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->float('teamlead_earned_abc', 16, 8)->default(0);
            $table->float('teamlead_earned_usdt', 16, 8)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('teamlead_earned_abc');
            $table->dropColumn('teamlead_earned_usdt');
        });
    }
}
