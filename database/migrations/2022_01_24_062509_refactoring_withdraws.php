<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * RefactoringWithdraws.
 */
final class RefactoringWithdraws extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('withdraws', function (Blueprint $table) {
            $table->unsignedBigInteger('crypto_currencies_id')
                ->after('users_id')
                ->nullable(true);
            $table->foreign('crypto_currencies_id', 'withdraws_crypto_currencies_id_foreign')
                ->references('id')
                ->on('crypto_currencies')
                ->restrictOnDelete();
            $table->unsignedSmallInteger('type_coin')
                ->nullable(true)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('withdraws', function (Blueprint $table) {
            $table->dropForeign('withdraws_crypto_currencies_id_foreign');
            $table->dropColumn('crypto_currencies_id');
            $table->unsignedSmallInteger('type_coin')
                ->nullable(false)
                ->change();
        });
    }
}
