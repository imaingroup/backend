<?php
declare(strict_types=1);

namespace App\Modules\Finance\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Finance\Models\CryptoBalance;

/**
 * CryptoBalanceRepository.
 */
final class CryptoBalanceRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param CryptoBalance $model
     */
    public function __construct(protected CryptoBalance $model)
    {
    }

    /**
     * Find by addresses and currency.
     *
     * @param int $cryptoAddressesId
     * @param int $cryptoCurrenciesId
     * @return CryptoBalance|null
     */
    public function findByAddressAndCurrency(int $cryptoAddressesId, int $cryptoCurrenciesId): ?CryptoBalance
    {
        return $this->model
            ->newQuery()
            ->where('crypto_addresses_id', $cryptoAddressesId)
            ->where('crypto_currencies_id', $cryptoCurrenciesId)
            ->first();
    }
}
