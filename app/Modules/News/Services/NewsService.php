<?php


namespace App\Modules\News\Services;

use App\Modules\News\Models\News;
use App\Modules\News\Models\NewsLike;
use App\Modules\Users\Models\User;
use App\Modules\News\Repositories\NewsCategoryRepository;
use App\Modules\News\Repositories\NewsRepository;
use Illuminate\Support\Facades\Storage;

/**
 * News service.
 */
final class NewsService
{
    /** @var int Limit new load. */
    private int $limitNewsLoad = 8;

    /** @var \App\Modules\News\Repositories\NewsCategoryRepository News category repository. */
    private NewsCategoryRepository $newsCategoryRepository;

    /** @var NewsRepository News repository. */
    private NewsRepository $newsRepository;

    /**
     * Construct.
     *
     * @param \App\Modules\News\Repositories\NewsCategoryRepository $newsCategoryRepository
     * @param NewsRepository $newsRepository
     */
    public function __construct(NewsCategoryRepository $newsCategoryRepository, NewsRepository $newsRepository)
    {
        $this->newsCategoryRepository = $newsCategoryRepository;
        $this->newsRepository = $newsRepository;
    }

    /**
     * Get new categories.
     *
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];

        foreach ($this->newsCategoryRepository->getAllActive() as $newsCategory) {
            $categories[] = [
                'id' => $newsCategory->sid,
                'title' => $newsCategory->getTranslates('title'),
            ];
        }

        return $categories;
    }

    /**
     * Get news.
     *
     * @param string $sort
     * @param int|null $categoryId
     * @param string|null $search
     * @param int $page
     * @return array
     */
    public function getNews(string $sort, ?int $categoryId, ?string $search, int $page): array
    {
        $list = $this->newsRepository->getAccountNews($sort, $categoryId, $search, $page, $this->limitNewsLoad);
        $data = ['list' => []];

        foreach ($list as $item) {
            $news = [];
            $news['id'] = $item->sid;
            $news['published_at'] = $item->published_at;
            $news['title'] = $item->getTranslates('title');
            $news['preview_image'] = Storage::disk('local')->url($item->preview_image);
            $news['catalog_preview'] = $item->getTranslates('catalog_preview');
            $news['category_title'] = $item->category->getTranslates('title');

            $data['list'][] = $news;
        }

        $count = $this->newsRepository->getAccountNewsCount($sort, $categoryId, $search);

        $data['count'] = $count;
        $data['page'] = $page;
        $data['pages'] = ceil($count / $this->limitNewsLoad);
        $data['ids'] = $this->newsRepository->getAccountNewsSids($sort, $categoryId, $search);

        return $data;
    }

    /**
     * Get news item.
     *
     * @param \App\Modules\News\Models\News $news
     * @param User $user
     * @return array
     */
    public function getNewsItem(News $news, User $user): array
    {
        $data = [];

        $data['title'] = $news->getTranslates('title');
        $data['published_at'] = $news->published_at;
        $data['text'] = $news->getTranslates('text');
        $data['video'] = $news->getVideo();
        $data['likes'] = $news['likes'] + $news->likes()->count();
        $data['liked'] = $news->liked($user);

        return $data;
    }

    /**
     * Like news.
     *
     * @param \App\Modules\News\Models\News $news
     * @param User $user
     */
    public function like(News $news, User $user): void
    {
        $like = new NewsLike();
        $like->users_id = $user->id;

        $news->likes()->save($like);
    }
}
