<?php
declare(strict_types=1);

namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Requests\Deposit\AdminDepositTransactionAddRequest;
use App\Modules\Deposites\Models\Deposit;
use App\Modules\Deposites\Services\DepositService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

/**
 * Admin deposit controller.
 */
final class VoyagerDepositController extends VoyagerBaseController
{
    /**
     * Add transaction.
     *
     * @param Deposit $deposit
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function addTransaction(Deposit $deposit): Response
    {
        return $this->inTransaction(function () use ($deposit): void {
            Deposit::refreshAndLockForUpdate($deposit);

            $this->getDepositService()
                ->addTransaction(
                    ...app(AdminDepositTransactionAddRequest::class)
                    ->validated()
                );
        });
    }

    /**
     * Get {@see DepositService::class}.
     *
     * @return DepositService
     */
    public function getDepositService(): DepositService
    {
        return app(DepositService::class);
    }
}
