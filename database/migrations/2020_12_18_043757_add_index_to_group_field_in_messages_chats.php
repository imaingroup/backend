<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds index for is_group field
 * in messages_chats table.
 */
final class AddIndexToGroupFieldInMessagesChats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages_chats', function (Blueprint $table) {
            $table->index('is_group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages_chats', function (Blueprint $table) {
            $table->dropIndex('messages_chats_is_group_index');
        });
    }
}
