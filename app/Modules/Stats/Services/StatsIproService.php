<?php
declare(strict_types=1);

namespace App\Modules\Stats\Services;

use App\Modules\Exchange\Services\ExchangeStatsService;
use App\Modules\ProfitCards\Services\ProfitCardService;
use App\Modules\Stats\Dto\Services\PurchaseStatsDto;

/**
 * Ipro stats service.
 */
final class StatsIproService
{
    /**
     * Purchase stats.
     *
     * @return PurchaseStatsDto
     */
    public function purchase(): PurchaseStatsDto
    {
        return new PurchaseStatsDto(
            $this->getProfitCardService()
                ->getSumPurchasedCards(),
            $this->getProfitCardService()
                ->getSumBonusPurchasedCards(),
            $this->getExchangeStatsService()
                ->sumAbcBoughtDeposited(),
            $this->getExchangeStatsService()
                ->sumAbcBoughtDividends(),
        );
    }

    /**
     * Get {@see ProfitCardService::class}.
     *
     * @return ProfitCardService
     */
    private function getProfitCardService(): ProfitCardService
    {
        return app(ProfitCardService::class);
    }

    /**
     * Get {@see ExchangeStatsService::class}.
     *
     * @return ExchangeStatsService
     */
    private function getExchangeStatsService(): ExchangeStatsService
    {
        return app(ExchangeStatsService::class);
    }
}
