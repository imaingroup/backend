<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adding fields to trades.
 */
class AddFieldsToTrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->float('closed_pnl', 16, 8)->default(0);
            $table->boolean('is_money_released')->default(false);
            $table->dateTime('money_released_at')->nullable(true);
            $table->unsignedBigInteger('related_transactions_id');

            $table->foreign('related_transactions_id')
                ->references('id')
                ->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table trades drop foreign key trades_related_transactions_id_foreign;');

        Schema::table('trades', function (Blueprint $table) {
            $table->dropColumn('closed_pnl');
            $table->dropColumn('is_money_released');
            $table->dropColumn('money_released_at');
            $table->dropColumn('related_transactions_id');
        });
    }
}
