<?php
declare(strict_types=1);

namespace App\Modules\Core\Repositories;


use Illuminate\Database\Eloquent\Model;

/**
 * Repositories interface.
 */
interface EloquentRepositoryInterface
{
    /**
     * Create object.
     *
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes): Model;

    /**
     * Update or create object.
     *
     * @param array $attributes
     * @param array $values
     * @return Model
     */
    public function updateOrCreate(array $attributes, array $values): Model;

    /**
     * Fin by ID.
     *
     * @param int $id
     * @return Model|null
     */
    public function find(int $id): ?Model;
}
