<?php
declare(strict_types=1);

namespace App\Providers;

use App\Modules\Core\Validation\CountWords;
use App\Modules\Core\Validation\MaxWordLength;
use App\Modules\Core\Validation\MaxWords;
use App\Modules\Investments\Models\TradeOption;
use App\Modules\Investments\Observers\TradeOptionObserver;
use App\Modules\Admin\Actions\Users\ActivateAction;
use App\Modules\Admin\Actions\Users\BlockAction;
use App\Modules\Admin\Actions\Users\UnblockAction;
use App\Modules\Admin\FormFields\CkEditorFormField;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;

final class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Voyager::addFormField(CkEditorFormField::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setLocale();

        Validator::extend('recaptcha', 'App\Modules\Core\Validation\ReCaptcha@validate');
        Validator::extend('scalar', 'App\Modules\Core\Validation\Scalar@validate');
        Validator::extend('numeric', 'App\Modules\Core\Validation\Numeric@validate');
        Validator::extend('bitcoin', 'App\Modules\Core\Validation\Bitcoin@validate');
        Validator::extend('bitcoin_tx', 'App\Modules\Core\Validation\BitcoinTx@validate');
        Validator::extend('crypto_tx', 'App\Modules\Core\Validation\CryptoTx@validate');
        Validator::extend('ethereum', 'App\Modules\Core\Validation\Ethereum@validate');
        Validator::extend('g2fa_install', 'App\Modules\Core\Validation\G2FaInstall@validate');
        Validator::extend('g2fa', 'App\Modules\Core\Validation\G2Fa@validate');
        Validator::extend(MaxWordLength::NAME, 'App\Modules\Core\Validation\MaxWordLength@validate');
        Validator::extend(MaxWords::NAME, MaxWords::class);
        Validator::extend(CountWords::NAME, CountWords::class);

        Voyager::addAction(ActivateAction::class);
        Voyager::addAction(BlockAction::class);
        Voyager::addAction(UnblockAction::class);

        TradeOption::observe(TradeOptionObserver::class);
    }

    /**
     * Set current locale from cookie.
     */
    private function setLocale()
    {
        try {
            $locale = Cookie::get('locale');

            if ($locale) {
                $localeDecrypted = explode('|', Crypt::decrypt($locale, false))[1];

                if (array_key_exists($localeDecrypted, Config::get('app.locales'))) {
                    App::setLocale($localeDecrypted);
                }
            }
        }
        catch (\Exception $e) {}
    }
}
