<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds index for columns status in deposits_transactions table.
 */
class AddIndexStatusToDepositsTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->index('status');
        });

        Schema::table('deposits_transactions', function (Blueprint $table) {
            $table->index('status');
            $table->index('transaction_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->dropIndex('deposits_status_index');
        });

        Schema::table('deposits_transactions', function (Blueprint $table) {
            $table->dropIndex('deposits_transactions_status_index');
            $table->dropIndex('deposits_transactions_transaction_at_index');
        });
    }
}
