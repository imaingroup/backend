<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Add custom tid options.
 */
final class AddCustomTidOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('tid_default')
                ->default(true);
        });

        DB::transaction(function () {
            $typeId = DB::table('data_types')
                ->where('name', 'users')
                ->first()
                ->id;

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'tid_default',
                    'type' => 'checkbox',
                    'display_name' => 'Tid default settings',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 75
                ]);
        });

        Schema::create('users_custom_tid_options', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('trade_options_id');
            $table->string('title');
            $table->integer('period_min');
            $table->integer('period_max');
            $table->integer('amount_min');
            $table->integer('amount_max');
            $table->timestamps();

            $table->unique(['users_id', 'trade_options_id'], 'users_custom_tid_options_unique');

            $table->foreign('users_id', 'users_custom_tid_options_users_id_foreign')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->foreign('trade_options_id', 'users_custom_tid_options_trade_options_id_foreign')
                ->references('id')
                ->on('trade_options')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('tid_default');
        });

        DB::transaction(function() {
            $typeId = DB::table('data_types')
                ->where('name', 'users')
                ->first()
                ->id;

            DB::table('data_rows')
                ->where('data_type_id', $typeId)
                ->whereIn('field', [
                    'tid_default',
                ])
                ->delete();
        });

        Schema::dropIfExists('users_custom_tid_options');
    }
}
