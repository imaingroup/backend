<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * Key not found exception.
 */
final class KeyNotFoundException extends EthereumException
{
}
