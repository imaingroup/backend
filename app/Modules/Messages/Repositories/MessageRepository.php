<?php
declare(strict_types=1);


namespace App\Modules\Messages\Repositories;

use App\Modules\Messages\Models\Message;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * Message repository.
 */
final class MessageRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Message $model
     */
    public function __construct(protected Message $model)
    {
    }

    /**
     * Get messages by chat id.
     *
     * @param int $chatId
     * @param int $userId
     * @param int|null $limit
     * @param int|null $type
     * @param int|null $afterMessageId
     * @return Collection
     */
    public function getByChatId(
        int $chatId,
        int $userId,
        int $limit = null,
        int $type = null,
        int $afterMessageId = null
    ): Collection
    {
        $query = $this->addAuthorToQuery($this->getMessagesQuery($userId, true))
            ->where('messages.messages_chats_id', $chatId)
            ->with('readMasks')
            ->orderBy('messages.id', 'DESC');

        if ($limit !== null) {
            $query->limit($limit);
        }

        if ($afterMessageId) {
            $query->where(
                'messages.id',
                match ($type) {
                    Message::SELECT_TYPE_OLDER => '<',
                    Message::SELECT_TYPE_NEWEST => '>'
                },
                $afterMessageId
            );
        }

        return $query
            ->get();
    }

    /**
     * Get messages by chat id for observe.
     *
     * @param int $chatId
     * @param int $userId
     * @param array $between
     * @param bool $forceAny
     * @return Collection
     */
    public function getByChatIdObserve(
        int $chatId,
        int $userId,
        array $between = [],
        bool $forceAny = false
    ): Collection
    {
        if (count($between) === 0 || count($between) > 2) {
            return collect();
        }

        $query = $this->addAuthorToQuery($this->getMessagesQuery($userId, !$forceAny, $forceAny))
            ->where('messages.messages_chats_id', $chatId)
            ->with('deletedModels')
            ->orderBy('messages.id', 'DESC');

        if (count($between) === 1) {
            $query->where('messages.id', $between[0]);
        } else {
            $query->where('messages.id', '>=', $between[0])
                ->where('messages.id', '<=', $between[1]);
        }

        return $query
            ->get();
    }

    /**
     * Get messages ids by chat id.
     *
     * @param int $chatId
     * @param int $userId
     * @return array
     */
    public function getIdsByChatId(int $chatId, int $userId): array
    {
        return $this->getMessagesQuery($userId, true)
            ->select('messages.id')
            ->where('messages.messages_chats_id', $chatId)
            ->get()
            ->pluck('id')
            ->toArray();
    }

    /**
     * Search messages.
     *
     * @param int $userId
     * @param array $chatIds
     * @param string $search
     * @param int $limit
     * @return Collection
     */
    public function search(int $userId, array $chatIds, string $search, int $limit): Collection
    {
        if (count($chatIds) === 0) {
            return collect();
        }

        return $this->addAuthorToQuery($this->getMessagesQuery($userId, true))
            ->whereIn('messages.messages_chats_id', $chatIds)
            ->where('messages.message', 'like', '%' . $search . '%')
            ->with('chat')
            ->limit($limit)
            ->orderBy('messages.created_at', 'DESC')
            ->get();
    }

    /**
     * Count unread messages.
     *
     * @param array|Builder $chats
     * @param int $userId
     * @return int
     */
    public function countUnread(array|Builder $chats, int $userId): int
    {
        if ($chats instanceof Builder) {
            $chats->select('messages_chats.id');
        }

        return $this->getMessagesQuery($userId, true)
            ->whereIn('messages.messages_chats_id', $chats)
            ->whereDoesntHave('readMasks', function (Builder $query) use ($userId) {
                $query->where('users_id', $userId);
            })
            ->where('messages.author_users_id', '!=', $userId)
            ->count();
    }

    /**
     * Get messages query.
     *
     * @param int $userId
     * @param bool $withoutDeleted
     * @param bool $forceAny
     * @return Builder
     */
    public function getMessagesQuery(int $userId, bool $withoutDeleted, bool $forceAny = false): Builder
    {
        $query = $this->model
            ->newQuery()
            ->select('messages.*');

        if (!$forceAny) {
            $query->where('messages.deleted', false);
        }

        if ($withoutDeleted && !$forceAny) {
            $query
                ->leftJoin('messages_deleted as md', function (JoinClause $join) use ($userId) {
                    $join->on('messages.id', '=', 'md.messages_id')
                        ->where('md.users_id', '=', $userId);
                })
                ->whereNull('md.id');
        }

        return $query;
    }

    /**
     * Add author fields to query.
     *
     * @param Builder $query
     * @return Builder
     */
    private function addAuthorToQuery(Builder $query): Builder
    {
        return $query
            ->addSelect('a.username as author_username', 'a.avatar as author_avatar')
            ->join('users as a', 'a.id', '=', 'messages.author_users_id');
    }
}
