<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api\Ethereum\Manage;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Finance\Enum\EthereumNetworkEnum;

/**
 * BalanceDto.
 *
 * @property-read EthereumNetworkEnum $network
 * @property-read string $address
 * @property-read int $balance
 * @property-read string|null $token
 */
final class BalanceDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param EthereumNetworkEnum $network
     * @param string $address
     * @param string $balance
     * @param string|null $token
     */
    public function __construct(
        protected EthereumNetworkEnum $network,
        protected string $address,
        protected string $balance,
        protected ?string $token,
    ) {

    }
}
