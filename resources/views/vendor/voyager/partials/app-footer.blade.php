<footer class="app-footer">
    <div class="site-footer-right">
        @if (rand(1,100) == 100)
            <i class="voyager-rum-1"></i> {{ __('voyager::theme.footer_copyright2') }}
        @else
            {!! __('voyager::theme.footer_copyright') !!} <a href="http://thecontrolgroup.com" target="_blank">The
                Control Group</a>
        @endif
        @php $version = Voyager::getVersion(); @endphp
        @if (!empty($version))
            - {{ $version }}
        @endif
    </div>
</footer>

@push('javascript')
    <script src="/assets/js/jquery/plugins/onFirst.js"></script>
    <script>
        const viewTableHooks = new (function () {
            const disableSort = [
                {'deposits': 'count_transactions'},
                {'tid-history': 'option_1'},
                {'tid-history': 'option_2'},
                {'tid-history': 'option_3'},
                {'tid-history': 'option_4'},
            ];

            const dataTable = $('#dataTable');

            this.disableSort = function () {
                for (let item in disableSort) {
                    const setting = disableSort[item];
                    const page = Object.keys(setting)[0];
                    const field = setting[page];

                    if (window.location.href.includes('/' + page)) {
                        const sortLink = dataTable.find('th a[href*=order_by\\=' + field + ']');
                        sortLink.replaceWith(sortLink.text());
                    }
                }
            };
        });

        viewTableHooks.disableSort();
    </script>
@endpush
