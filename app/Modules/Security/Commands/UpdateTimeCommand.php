<?php
declare(strict_types=1);

namespace App\Modules\Security\Commands;

use App\Modules\Logs\Loggers\Logger;
use App\Modules\Security\Services\TimeProtectionService;
use Illuminate\Console\Command;
use Throwable;

/**
 * This command need for update time from
 * time servers.
 */
final class UpdateTimeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'time:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command need for update time from time servers';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        try {
            $this->getTimeProtectionService()
                ->updateTime();

            return 0;
        } catch (Throwable $e) {
            $this->getLogger()
                ->registerException($e);

            return -1;
        }
    }

    /**
     * Получить TimeProtectionService.
     *
     * @return TimeProtectionService
     */
    private function getTimeProtectionService(): TimeProtectionService
    {
        return app(TimeProtectionService::class);
    }

    /**
     * Получить Logger.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
