<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migrations add float pnl fields to trade_options table.
 */
class AddFieldsFloatPnlToTradeOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->smallInteger('float_pnl_high')->default(0)->after('requirements_total_deposit');
            $table->smallInteger('float_pnl_low')->default(0)->after('float_pnl_high');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->dropColumn('float_pnl_high');
            $table->dropColumn('float_pnl_low');
        });
    }
}
