<?php
declare(strict_types=1);

namespace App\Modules\PromoCodes\Services;

use App\Modules\Money\Exceptions\MoneyTransferException;
use App\Modules\PromoCodes\Exceptions\PromoCodeException;
use App\Modules\Deposites\Models\DepositTransaction;
use App\Modules\ProfitCards\Models\ProfitCardBuyHistory;
use App\Modules\Money\Models\Transaction;
use App\Modules\Money\Services\MoneyTransferService;
use App\Modules\Users\Models\User;
use App\Modules\Actions\Models\UserAction;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Settings\Repositories\SettingsPriceRepository;
use App\Modules\Actions\Services\UserActionService;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 * Promo code service.
 */
final class PromoCodeService
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Settings\Repositories\SettingsPriceRepository $settingsPriceRepository
     * @param MoneyTransferService $moneyTransferService
     * @param UserActionService $userActionService
     */
    public function __construct(
        private SettingsPriceRepository $settingsPriceRepository,
        private MoneyTransferService $moneyTransferService,
        private UserActionService $userActionService
    )
    {
    }

    /**
     * Add bonus after register if this user giving promo code.
     *
     * @param \App\Modules\Users\Models\User $user
     * @throws Exception
     */
    public function addBonusAfterRegister(User $user)
    {
        $teamLead = $user->teamLead;

        if ($teamLead) {
            $bonus = $teamLead->is_promo_code_bonus_default ?
                $this->settingsPriceRepository->get()->promo_code_bonus
                : $teamLead->promo_code_bonus;

            $this->moneyTransferService->moneyOperation(
                $user,
                Auth::check() ? Auth::user() : $user,
                $bonus,
                Transaction::CURRENCY_ABC,
                Transaction::TYPE_REGISTER_BONUS
            );
        }
    }

    /**
     * Add commission after deposit.
     *
     * @param \App\Modules\Deposites\Models\DepositTransaction $depositTransaction
     * @throws PromoCodeException
     * @throws \App\Modules\Money\Exceptions\MoneyTransferException
     * @throws TransactionNotStarted
     */
    public function addCommissionAfterDeposit(DepositTransaction $depositTransaction)
    {
        $relatedTransaction = $depositTransaction->transaction;
        if (!$relatedTransaction) {
            throw new PromoCodeException('DepositTransaction doesn\'t have related transaction');
        }

        $user = $depositTransaction->deposit->user;

        $teamLead = $user->teamLead;

        if ($teamLead) {
            User::refreshAndLockForUpdate($user);
            User::refreshAndLockForUpdate($teamLead);

            $commissionUsdt = $this->calculateReferralUsdtCommission($teamLead, $depositTransaction->amount_usdt);

            if ($commissionUsdt > 0) {
                $transaction = $this->moneyTransferService->moneyOperation(
                    $teamLead,
                    Auth::check() ? Auth::user() : $teamLead,
                    $commissionUsdt,
                    Transaction::CURRENCY_USDT_DEPOSITED,
                    Transaction::TYPE_REFERRAL_COMMISSION_DEPOSIT,
                    $relatedTransaction
                );

                $user->teamlead_earned_usdt += $commissionUsdt;
                $user->save();

                $teamLead->earned_commission_usdt += $commissionUsdt;
                $teamLead->save();

                $this->userActionService->register(
                    $teamLead,
                    UserAction::ACTION_COMMISSION_USDT,
                    [
                        'transaction' => $transaction->id
                    ]
                );
            }
        }
    }

    /**
     * Add commission after buy profit card.
     *
     * @param \App\Modules\ProfitCards\Models\ProfitCardBuyHistory $profitCardBuyHistory
     * @throws MoneyTransferException
     * @throws TransactionNotStarted
     */
    public function addCommissionAfterBuyProfitCard(ProfitCardBuyHistory $profitCardBuyHistory)
    {
        $relatedTransaction = $profitCardBuyHistory->transaction;
        $user = $profitCardBuyHistory->user;

        $teamLead = $user->teamLead;

        if ($teamLead) {
            User::refreshAndLockForUpdate($user);
            User::refreshAndLockForUpdate($teamLead);

            $commissionAbc = $this->calculateReferralAbcCommission($teamLead, $profitCardBuyHistory->abc);

            if ($commissionAbc > 0) {
                $transaction = $this->moneyTransferService->moneyOperation(
                    $teamLead,
                    Auth::check() ? Auth::user() : $teamLead,
                    $commissionAbc,
                    Transaction::CURRENCY_ABC,
                    Transaction::TYPE_REFERRAL_COMMISSION_BUY_PROFIT_CARD,
                    $relatedTransaction
                );

                $user->teamlead_earned_abc += $commissionAbc;
                $user->save();

                $teamLead->earned_commission_abc += $commissionAbc;
                $teamLead->save();

                $this->userActionService->register(
                    $teamLead,
                    UserAction::ACTION_COMMISSION_ABC,
                    [
                        'transaction' => $transaction->id
                    ]
                );
            }
        }
    }

    /**
     * Add commission after transaction.
     *
     * @param \App\Modules\Money\Models\Transaction $transaction
     * @param int $transactionType
     * @throws \App\Modules\Money\Exceptions\MoneyTransferException
     * @throws \App\Modules\PromoCodes\Exceptions\PromoCodeException
     * @throws TransactionNotStarted
     */
    public function addCommissionAfterTransaction(Transaction $transaction, int $transactionType)
    {
        $user = $transaction->user;
        $teamLead = $user->teamLead;

        if ($teamLead) {
            User::refreshAndLockForUpdate($user);
            User::refreshAndLockForUpdate($teamLead);

            if ($transaction->operation_type !== Transaction::OPERATION_TYPE_ADD) {
                throw new PromoCodeException('This operation type doesn\'t supports');
            } else if ($transaction->amount < 0) {
                throw new PromoCodeException('This amount may not be use for referral commission');
            }

            $commission = null;
            $currency = null;
            $action = null;

            if ($transaction->currency === Transaction::CURRENCY_ABC) {
                $commission = $this->calculateReferralAbcCommission($teamLead, $transaction->amount);
                $currency = Transaction::CURRENCY_ABC;
                $action = UserAction::ACTION_COMMISSION_ABC;
                $user->teamlead_earned_abc += $commission;
                $teamLead->earned_commission_abc += $commission;
            } else if ($transaction->currency === Transaction::CURRENCY_USDT_DEPOSITED) {
                $commission = $this->calculateReferralUsdtCommission($teamLead, $transaction->amount);
                $currency = Transaction::CURRENCY_USDT_DEPOSITED;
                $action = UserAction::ACTION_COMMISSION_USDT;
                $user->teamlead_earned_usdt += $commission;
                $teamLead->earned_commission_usdt += $commission;
            } else {
                throw new PromoCodeException('This currency doesn\'t supports');
            }

            if ($commission > 0) {
                $user->save();
                $teamLead->save();

                $transaction = $this->moneyTransferService->moneyOperation(
                    $teamLead,
                    Auth::check() ? Auth::user() : $teamLead,
                    $commission,
                    $currency,
                    $transactionType,
                    $transaction
                );

                $this->userActionService->register(
                    $teamLead,
                    $action,
                    [
                        'transaction' => $transaction->id
                    ]
                );
            }
        }
    }

    /**
     * Calculate referral commission usdt.
     *
     * @param \App\Modules\Users\Models\User $teamLead
     * @param float $amount
     * @return float
     */
    public function calculateReferralUsdtCommission(User $teamLead, float $amount): float
    {
        return $amount / 100 * $this->getUsdtCommission($teamLead);
    }

    /**
     * Calculate referral commission abc.
     *
     * @param \App\Modules\Users\Models\User $teamLead
     * @param float $amount
     * @return float
     */
    public function calculateReferralAbcCommission(User $teamLead, float $amount): float
    {
        return $amount / 100 * $this->getAbcCommission($teamLead);
    }

    /**
     * Get abc commission.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return float
     */
    public function getAbcCommission(User $user): float
    {
        return $user->is_referral_abc_commission_default ?
            $this->settingsPriceRepository->get()->referral_abc_commission
            : $user->referral_abc_commission;
    }

    /**
     * Get usdt commission.
     *
     * @param User $user
     * @return float
     */
    public function getUsdtCommission(User $user): float
    {
        return $user->is_referral_usdt_commission_default ?
            $this->settingsPriceRepository->get()->referral_usdt_commission
            : $user->referral_usdt_commission;
    }

    /**
     * Generate promo code.
     *
     * @return string
     */
    public function generatePromoCode(): string
    {
        return strtoupper(Str::random(32));
    }
}
