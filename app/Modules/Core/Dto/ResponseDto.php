<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto;

use Illuminate\Support\Collection;

/**
 * Response DTO.
 *
 * @property-read BaseDto|Collection $data
 */
final class ResponseDto extends BaseDto
{
    /** @var array|string[] Excluded read-only. */
    protected array $excludedReadOnly = ['data'];

    /** @var BaseDto|Collection Data. */
    protected BaseDto|Collection $data;

    /**
     * Constructor.
     *
     * @param BaseDto|Collection|null $data
     * @param bool $status
     */
    public function __construct(BaseDto|Collection|null $data = null, protected bool $status = true)
    {
        if ($data !== null) {
            $this->data = $data;
        }
    }
}
