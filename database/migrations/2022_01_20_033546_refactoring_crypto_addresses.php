<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * RefactoringCryptoAddresses.
 */
final class RefactoringCryptoAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->dropColumn(['eth_block', 'eth_block_checked']);
            $table->text('mnemonic')
                ->nullable(true)
                ->after('eth_password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->dropColumn('mnemonic');
            $table->unsignedBigInteger('eth_block_checked')
                ->after('cnt_check')
                ->nullable(true);
            $table->unsignedBigInteger('eth_block')
                ->after('cnt_check')
                ->nullable(true);
        });
    }
}
