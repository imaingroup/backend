<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum\Swap;

/**
 * NoSwapMethodException.
 */
final class NoSwapMethodException extends SwapException
{
}
