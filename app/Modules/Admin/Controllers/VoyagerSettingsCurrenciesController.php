<?php
declare(strict_types=1);

namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Requests\Settings\Currencies\DeleteRequest;
use App\Modules\Admin\Requests\Settings\Currencies\SetRequest;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Finance\Exceptions\Services\BitQuery\RequestException;
use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Finance\Services\V2\DataGetterService;
use App\Modules\Settings\Models\DepositCurrency;
use App\Modules\Settings\Services\CurrenciesService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Facades\Voyager;

/**
 * VoyagerSettingsDepositController.
 */
final class VoyagerSettingsCurrenciesController extends VoyagerBaseController
{
    /**
     * Main.
     *
     * @return View
     */
    public function main(): View
    {
        return Voyager::view('voyager::settings.currencies');
    }

    /**
     * Get currencies.
     *
     * @return ResponseDto
     */
    public function currencies(): ResponseDto
    {
        return new ResponseDto(
            $this->getDataGetterService()
                ->getCurrencies()
        );
    }

    /**
     * Get currencies.
     *
     * @return ResponseDto
     */
    public function data(): ResponseDto
    {
        return new ResponseDto(
            $this->getCurrenciesService()
                ->getCurrencies()
                ->map(fn(DepositCurrency $currency) => $currency->toApiDto())
        );
    }

    /**
     * Currency details.
     *
     * @param CryptoCurrency $currency
     * @return ResponseDto
     * @throws RequestException
     */
    public function currencyDetails(CryptoCurrency $currency): ResponseDto
    {
        return new ResponseDto(
            $this->getDataGetterService()
                ->getCurrencyDetails($currency)
                ->toApiDto()
        );
    }

    /**
     * Set currency.
     *
     * @param SetRequest $request
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function set(SetRequest $request): Response
    {
        return $this->inTransaction(function () use ($request): void {
            $this->getCurrenciesService()
                ->setCurrency(
                    ...$request->validated()
                );
        });
    }

    /**
     * Delete currency.
     *
     * @param DeleteRequest $request
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function delete(DeleteRequest $request): Response
    {
        return $this->inTransaction(function () use ($request): void {
            $this->getCurrenciesService()
                ->deleteCurrency($request->validated());
        });
    }

    /**
     * Get {@see DataGetterService::class}.
     *
     * @return DataGetterService
     */
    private function getDataGetterService(): DataGetterService
    {
        return app(DataGetterService::class);
    }

    /**
     * Get {@see CurrenciesService::class}.
     *
     * @return CurrenciesService
     */
    private function getCurrenciesService(): CurrenciesService
    {
        return app(CurrenciesService::class);
    }
}
