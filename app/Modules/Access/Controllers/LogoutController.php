<?php

namespace App\Modules\Access\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Access\Requests\LogoutRequest;
use App\Modules\Access\Services\LogoutService;

/**
 * Activate controller.
 */
final class LogoutController extends Controller
{
    /** @var LogoutService Logout service. */
    private LogoutService $logoutService;

    /**
     * Constructor.
     *
     * @param LogoutService $logoutService
     */
    public function __construct(LogoutService $logoutService)
    {
        $this->logoutService = $logoutService;
    }

    /**
     * Activate static page.
     *
     * @param \App\Modules\Access\Requests\LogoutRequest $request
     * @return array
     */
    public function logout(LogoutRequest $request): array
    {
        $this->logoutService->logout();

        return ['status' => true];
    }
}
