<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Repositories;

use App\Modules\Users\Models\User;
use App\Modules\Withdraw\Models\Withdraw;
use App\Modules\Core\Repositories\BaseRepository;
use Exception;
use Illuminate\Support\Collection;

/**
 * Withdraw repository.
 */
final class WithdrawRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Withdraw $model
     */
    public function __construct(protected Withdraw $model)
    {}

    /**
     * Withdrawn amount in this month.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return float
     * @throws Exception
     */
    public function withdrawnAmountThisMonth(User $user): float
    {
        $currentDate = new \DateTime();
        $fromDate = $currentDate->format('Y-m-01 00:00:00');
        $endData = (new \DateTime($fromDate))->modify('+1 MONTH -1 DAY')->format('Y-m-d 23:59:59');

        return (float)$this->model
            ->newQuery()
            ->selectRaw('sum(amount_usdt) as sum_usdt')
            ->where('users_id', $user->id)
            ->where('status', '!=', Withdraw::STATUS_DECLINED)
            ->whereBetween('created_at', [$fromDate, $endData])
            ->first()
            ->sum_usdt;
    }

    /**
     * Get by user.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return Collection
     */
    public function getByUser(User $user): Collection
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $user->id)
            ->whereNotNull('crypto_currencies_id')
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}
