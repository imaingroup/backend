<?php
declare(strict_types=1);

namespace App\Modules\Messages\Services;

use App\Modules\Messages\Models\MessageRead;
use App\Modules\Users\Models\User;
use App\Modules\Messages\Exceptions\ChatAlreadyExistsException;
use App\Modules\Messages\Exceptions\ChatException;
use App\Modules\Messages\Exceptions\ChatUserNotFoundException;
use App\Modules\Messages\Exceptions\InvitationException;
use App\Modules\Messages\Models\Chat;
use App\Modules\Messages\Models\ChatUser;
use App\Modules\Messages\Models\Contact;
use App\Modules\Messages\Models\Message;
use App\Modules\Messages\Repositories\ChatAcceptedInvitationRepository;
use App\Modules\Messages\Repositories\ChatDeletedRepository;
use App\Modules\Messages\Repositories\ChatRepository;
use App\Modules\Messages\Repositories\ChatUserRepository;
use App\Modules\Messages\Repositories\ContactRepository;
use App\Modules\Messages\Repositories\MessageDeletedRepository;
use App\Modules\Messages\Repositories\MessageReadRepository;
use App\Modules\Messages\Repositories\MessageRepository;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\ExpectedValues;
use JetBrains\PhpStorm\Pure;
use function sprintf;

/**
 * Messages service.
 */
final class MessageService
{
    /**
     * Constructor.
     *
     * @param ContactRepository $contactRepository
     * @param ChatRepository $chatRepository
     * @param ChatUserRepository $chatUserRepository
     * @param MessageRepository $messageRepository
     * @param MessageReadRepository $messageReadRepository
     * @param MessageDeletedRepository $messageDeletedRepository
     * @param ChatAcceptedInvitationRepository $chatAcceptedInvitationRepository
     * @param ChatDeletedRepository $chatDeletedRepository
     */
    public function __construct(
        private ContactRepository $contactRepository,
        private ChatRepository $chatRepository,
        private ChatUserRepository $chatUserRepository,
        private MessageRepository $messageRepository,
        private MessageReadRepository $messageReadRepository,
        private MessageDeletedRepository $messageDeletedRepository,
        private ChatAcceptedInvitationRepository $chatAcceptedInvitationRepository,
        private ChatDeletedRepository $chatDeletedRepository
    )
    {
    }

    /**
     * Get contact list.
     *
     * @param User $user
     * @return array
     */
    #[ArrayShape([
        'chats' => "array",
        'legend' => "string[]"
    ])]
    public function getContactList(User $user): array
    {
        $contacts = $this->contactRepository->getByUser($user->id);
        $data = [
            'chats' => [],
            'legend' => Contact::CHAT_GROUPS
        ];

        foreach ($contacts as $contact) {
            $groupId = $this->getGroupContact($contact);

            if (!isset($data['chats'][$groupId])) {
                $data['chats'][$groupId] = [];
            }

            $userContact = $contact->contact;
            $chat = $this->getSingleChat($contact);

            $data['chats'][$groupId][] = [
                'id' => $contact->sid,
                'username' => $userContact->username,
                'avatar' => Storage::disk('local')->url($userContact->avatar),
                'chat_id' => $chat?->sid,
                'promo_code' => $userContact->promo_code
            ];
        }

        return $data;
    }

    /**
     * Get chat list.
     *
     * @param User $user
     * @return array
     * @throws Exception
     */
    public function getChatList(User $user): array
    {
        $chats = $this->chatRepository->getChats(
            $user->id,
            $this->messageRepository->getMessagesQuery($user->id, true)
        );
        $data = collect([]);
        $isAdmin = $user->hasRole(User::ROLE_ADMIN_NAME) || $user->hasRole(User::ROLE_MODERATOR);

        foreach ($chats as $chat) {
            $users = [];
            $readOnly = true;

            /** @var User $chatUser */
            foreach ($chat->chatUsersWithContactSid($user) as $chatUser) {
                $users[] = [
                    'username' => $chatUser->user->username,
                    'avatar' => Storage::disk('local')->url($chatUser->user->avatar),
                    'promo_code' => $chatUser->user->promo_code,
                    'added' => $chatUser->user->id == $user->id
                        || $this->contactRepository->existsContact($user->id, $chatUser->user->id),
                    'is_owner' => $chatUser->is_owner,
                    'id' => $chatUser->sid,
                    'contact_id' => $chatUser->contact_sid,
                    'edit' => false,
                    'delete' => false
                ];

                if ($chatUser->is_owner) {
                    $readOnly = false;
                }
            }

            $data->add([
                'id' => $chat->sid,
                'name' => $chat->getChatName($user),
                'avatar' => $chat->getAvatarUrl($user),
                'required_accept_invitation' => $this->checkRequiredAcceptInvitation($user, $chat),
                'last_message_at' => $chat->last_message_at,
                'count_new' => $this->countNewMessages($user, $chat),
                'readonly' => $readOnly,
                'is_group' => $chat->is_group,
                'users' => $users,
                'edit' => $isAdmin,
                'delete' => $isAdmin,
                'sort' => new DateTime($chat->last_message_at ?? $chat->created_at->format('Y-m-d H:i:s'))
            ]);
        }

        return $data
            ->sortByDesc('sort')
            ->map(function ($item) {
                unset($item['sort']);

                return $item;
            })
            ->values()
            ->toArray();
    }

    /**
     * Conversation start.
     *
     * @param Contact $contact
     * @return Chat
     * @throws ChatAlreadyExistsException
     */
    public function conversationStart(Contact $contact): Chat
    {
        $chat = $this->getSingleChat($contact);

        if ($chat) {
            $this->chatDeletedRepository->deleteByChat($chat->id, $contact->users_id);
        } else {
            $chat = $this->createSingleChat($contact);

            $this->deleteChat($chat, $contact->contact, false);
        }

        return $chat;
    }

    /**
     * Add contact to user.
     *
     * @param User $user
     * @param User $invitedUser
     * @return Contact
     */
    public function addContact(User $user, User $invitedUser): Contact
    {
        $contact = $this->contactRepository->getContact($user->id, $invitedUser->id);

        if ($contact) {
            $contact->deleted = false;
            $contact->save();
        } else {
            $contact = $this->contactRepository->create([
                'users_id' => $user->id,
                'contact_users_id' => $invitedUser->id,
            ]);
        }

        return $contact;
    }

    /**
     * Get group identifier by contact.
     *
     * @param Contact $contact
     * @return int
     */
    public function getGroupContact(Contact $contact): int
    {
        $user = $contact->user;
        $contactUser = $contact->contact;

        if ($contactUser->hasRole(User::ROLE_ADMIN_NAME)) {
            return Contact::GROUP_ADMINISTRATION;
        } else if ($contact->blocked) {
            return Contact::GROUP_BLACK_LIST;
        } else if ($user->team_member_id === $contactUser->id) {
            return Contact::GROUP_TEAM_LEADER;
        } else if ($user->id === $contactUser->team_member_id) {
            return Contact::GROUP_TEAMMATES;
        }

        return Contact::GROUP_USERS;
    }

    /**
     * Send message to contact.
     *
     * @param User $user
     * @param Contact $contact
     * @param string $message
     * @param Message|null $parentMessage
     * @return array
     * @throws ChatAlreadyExistsException
     */
    #[ArrayShape(['chat_id' => "int", 'message_id' => "int"])]
    public function sendMessageToContact(
        User $user,
        Contact $contact,
        string $message,
        Message $parentMessage = null
    ): array
    {
        $chat = $this->getSingleChat($contact);

        if (!$chat) {
            $chat = $this->createSingleChat($contact);
        }

        return [
            'chat_id' => $chat->sid,
            'message_id' => $this->sendMessageToChat($user, $chat, $message, $parentMessage)['message_id']
        ];
    }

    /**
     * Send message to chat.
     *
     * @param User $user
     * @param Chat $chat
     * @param string $message
     * @param Message|null $parentMessage
     * @return array
     */
    #[ArrayShape(['chat_id' => "int", 'message_id' => "int"])]
    public function sendMessageToChat(
        User $user,
        Chat $chat,
        string $message,
        Message $parentMessage = null
    ): array
    {
        $this->chatDeletedRepository->deleteByChat($chat->id);

        $message = $this->messageRepository->create([
            'message' => $message,
            'author_users_id' => $user->id,
            'messages_chats_id' => $chat->id,
            'parent_id' => $parentMessage?->id
        ]);

        return [
            'chat_id' => $chat->sid,
            'message_id' => $message->sid
        ];
    }

    /**
     * Send message to chat or contact.
     *
     * @param User $user
     * @param Contact|Chat $messageTarget
     * @param string $message
     * @param Message|null $parentMessage
     * @return array
     * @throws ChatAlreadyExistsException
     */
    #[ArrayShape(['chat_id' => "int", 'message_id' => "int"])]
    public function sendMessage(
        User $user,
        Contact|Chat $messageTarget,
        string $message,
        Message $parentMessage = null
    ): array
    {
        if ($messageTarget instanceof Contact) {
            return $this->sendMessageToContact($user, $messageTarget, $message, $parentMessage);
        } else {
            return $this->sendMessageToChat($user, $messageTarget, $message, $parentMessage);
        }
    }

    /**
     * Edit message.
     *
     * @param Message $message
     * @param string $messageText
     */
    public function editMessage(
        Message $message,
        string $messageText
    ): void
    {
        $message->message = $messageText;

        $message->save();
    }

    /**
     * Create single chat.
     *
     * @param Contact $contact
     * @return Chat
     * @throws ChatAlreadyExistsException
     */
    public function createSingleChat(Contact $contact): Chat
    {
        if ($this->getSingleChat($contact)) {
            throw new ChatAlreadyExistsException();
        }

        $chat = $this->chatRepository->create([
            'is_group' => false
        ]);

        $this->chatUserRepository->create([
            'messages_chats_id' => $chat->id,
            'users_id' => $contact->users_id,
            'is_owner' => true
        ]);

        $this->chatUserRepository->create([
            'messages_chats_id' => $chat->id,
            'users_id' => $contact->contact_users_id,
            'is_owner' => false
        ]);

        return $chat;
    }

    /**
     * Create group chat.
     *
     * @param Contact[] $contacts
     * @param string $name
     * @param UploadedFile|null $avatar
     * @return Chat
     * @throws ChatException
     */
    public function createGroupChat(array $contacts, string $name, UploadedFile $avatar = null): Chat
    {
        if (count($contacts) === 0) {
            throw new ChatException('For create group chat required 1 contact as minimum.');
        }

        $chat = $this->chatRepository->create([
            'name' => $name,
            'is_group' => true,
            'avatar' => $avatar ? $avatar->store('chats', 'public') : null
        ]);

        $this->chatUserRepository->create([
            'messages_chats_id' => $chat->id,
            'users_id' => $contacts[0]->users_id,
            'is_owner' => true
        ]);

        foreach ($contacts as $contact) {
            $this->chatUserRepository->create([
                'messages_chats_id' => $chat->id,
                'users_id' => $contact->contact_users_id,
                'is_owner' => false
            ]);
        }

        return $chat;
    }

    /**
     * Edit name and avatar for group chat.
     *
     * @param Chat $chat
     * @param string $name
     * @param UploadedFile|null $avatar
     * @return void
     */
    public function editGroupChat(Chat $chat, string $name, UploadedFile $avatar = null): void
    {
        if ($avatar) {
            if ($chat->avatar) {
                Storage::disk('public')->delete($chat->avatar);
            }

            $chat->avatar = $avatar->store('chats', 'public');
        }

        $chat->name = $name;

        $chat->save();
    }

    /**
     * Leave group chat.
     *
     * @param User $user
     * @param Chat $chat
     * @throws ChatUserNotFoundException
     * @throws ChatAlreadyExistsException
     */
    public function leaveGroupChat(User $user, Chat $chat): void
    {
        $chatUser = $this->chatUserRepository->getChatUser($chat->id, $user->id, false);

        if (!$chatUser) {
            throw new ChatUserNotFoundException('Chat user no found');
        }

        if ($chat->isOwner($user)) {
            $this->sendMessage(
                $user,
                $chat,
                __('messages.last_message', ['admin' => $user->username])
            );
        }

        $this->deleteChatUser($chatUser);
    }

    /**
     * Check access for read this chat chat.
     *
     * @param User $user
     * @param User $invitedUser
     * @return bool
     */
    public function checkAccessAddContact(User $user, User $invitedUser): bool
    {
        if ($user->id === $invitedUser->id) {
            return false;
        }

        $contact = $this->contactRepository->getContact($user->id, $invitedUser->id);

        return !$contact || $contact->deleted;
    }

    /**
     * Check access for read this chat chat.
     *
     * @param User $user
     * @param Chat $chat
     * @return bool
     */
    public function checkAccessChat(User $user, Chat $chat): bool
    {
        return $this->chatRepository->checkAccessChat($chat->id, $user->id);
    }

    /**
     * Check access to read this message.
     *
     * @param User $user
     * @param Message $message
     * @param Contact|Chat|null $messageTarget
     * @param bool $noCheckDeleted
     * @return bool
     */
    public function checkAccessMessage(
        User $user,
        Message $message,
        Contact|Chat $messageTarget = null,
        bool $noCheckDeleted = false
    ): bool
    {
        if (!$this->checkAccessChat($user, $message->chat)
            || (
                !$noCheckDeleted &&
                $this->checkDeletedMessage($user, $message)
            )
        ) {
            return false;
        } else if ($messageTarget) {
            $chat = $messageTarget instanceof Contact
                ? $this->getSingleChat($messageTarget)
                : $messageTarget;

            if ($chat && $chat->id !== $message->chat->id) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check access edit message.
     *
     * @param User $user
     * @param Message $message
     * @return bool
     */
    public function checkAccessEditMessage(User $user, Message $message): bool
    {
        return $this->checkAccessMessage($user, $message) &&
            (
                $user->hasRole(User::ROLE_ADMIN_NAME) ||
                $user->hasRole(User::ROLE_MODERATOR)
            );
    }

    /**
     * Check access for delete message.
     *
     * @param User $user
     * @param Message $message
     * @return bool
     */
    public function checkAccessDeleteMessageForAll(User $user, Message $message): bool
    {
        return (
                $user->hasRole(User::ROLE_ADMIN_NAME) ||
                $user->hasRole(User::ROLE_MODERATOR)
            ) && $this->checkAccessMessage($user, $message);
    }

    /**
     * Check access for delete chat.
     *
     * @param User $user
     * @param Chat $chat
     * @return bool
     */
    public function checkAccessDeleteChat(User $user, Chat $chat): bool
    {
        return $this->checkAccessChat($user, $chat) &&
            !$this->chatDeletedRepository->checkDeleted($chat->id, $user->id);
    }

    /**
     * Check access write in chat.
     *
     * @param User $user
     * @param Chat $chat
     * @return bool
     */
    public function checkAccessWriteInChat(User $user, Chat $chat): bool
    {
        return $this->checkAccessChat($user, $chat) &&
            $chat->chatUserOwner &&
            (
                $chat->isOwner($user) ||
                $user->hasRole(User::ROLE_ADMIN_NAME) ||
                $chat->checkAcceptedInvitation($user)
            );
    }

    /**
     * Check access edit chat.
     *
     * @param User $user
     * @param Chat $chat
     * @return bool
     */
    public function checkAccessEditChat(User $user, Chat $chat): bool
    {
        return $chat->isOwner($user) && $chat->is_group;
    }

    /**
     * Check access leave group chat.
     *
     * @param User $user
     * @param Chat $chat
     * @return bool
     */
    public function checkAccessLeaveGroupChat(User $user, Chat $chat): bool
    {
        return $chat->is_group &&
            $this->checkAccessChat($user, $chat);
    }

    /**
     * Check access contact.
     *
     * @param User $user
     * @param Contact $contact
     * @return bool
     */
    #[Pure]
    public function checkAccessContact(User $user, Contact $contact): bool
    {
        return $contact->isOwner($user) && !$contact->deleted;
    }

    /**
     * Check access contact.
     *
     * @param User $user
     * @param Contact $contact
     * @return bool
     */
    public function checkAccessDeleteContact(User $user, Contact $contact): bool
    {
        return $this->checkAccessContact($user, $contact) &&
            !$contact->contact->hasRole(User::ROLE_ADMIN_NAME);
    }

    /**
     * Check access write in chat.
     *
     * @param User $user
     * @param Contact $contact
     * @return bool
     */
    public function checkAccessWriteInContact(User $user, Contact $contact): bool
    {
        if (!$this->checkAccessContact($user, $contact)) {
            return false;
        }

        $chat = $this->getSingleChat($contact);

        return $chat ? $this->checkAccessWriteInChat($user, $chat) : true;
    }

    /**
     * Check access delete user form group chat.
     *
     * @param User $user
     * @param Chat $chat
     * @param ChatUser $chatUser
     * @return bool
     */
    public function checkAccessDeleteUserFromGroupChat(User $user, Chat $chat, ChatUser $chatUser): bool
    {
        return $chat->is_group &&
            $chat->isOwner($user)
            && $chatUser->checkChat($chat)
            && !$chatUser->is_owner
            && !$chatUser->deleted;
    }

    /**
     * Check access add user to group chat.
     *
     * @param User $user
     * @param Chat $chat
     * @param Contact $contact
     * @return bool
     */
    public function checkAccessAddUserToGroupChat(User $user, Chat $chat, Contact $contact): bool
    {
        return $chat->is_group &&
            $chat->isOwner($user)
            && $contact->isOwner($user)
            && !$this->chatRepository->existsChatUser($chat->id, $contact->contact_users_id);
    }

    /**
     * Check access block user.
     *
     * @param User $user
     * @param User $blockedUser
     * @return bool
     */
    public function checkAccessBlockUser(User $user, User $blockedUser): bool
    {
        $contact = $this->contactRepository->getContact($user->id, $blockedUser->id);

        return $user->id !== $blockedUser->id &&
            !$blockedUser->hasRole(User::ROLE_ADMIN_NAME) &&
            (!$contact || ($contact && !$contact->blocked));
    }

    /**
     * Check access unblock contact.
     *
     * @param User $user
     * @param Contact $contact
     * @return bool
     */
    #[Pure]
    public function checkAccessUnblockContact(User $user, Contact $contact): bool
    {
        return $this->checkAccessContact($user, $contact) &&
            $contact->blocked;
    }

    /**
     * Get limit add users to group chat.
     *
     * @param Chat $chat
     * @return int
     */
    public function getLimitAddUsersToGroupChat(Chat $chat): int
    {
        return ChatUser::LIMIT_USERS_IN_GROUP_CHAT - $chat->chatUsers()->count();
    }

    /**
     * Check required accepted invitation.
     *
     * @param User $user
     * @param Chat $chat
     * @return bool
     */
    public function checkRequiredAcceptInvitation(User $user, Chat $chat): bool
    {
        return !$chat->isOwner($user) &&
            !$user->hasRole(User::ROLE_ADMIN_NAME) &&
            !$chat->checkAcceptedInvitation($user);
    }

    /**
     * Get messages.
     *
     * @param User $user
     * @param Contact|Chat $messageSource
     * @param int|null $limit
     * @param int|null $type
     * @param Message|null $afterMessage
     * @param string $order
     * @return array
     */
    public function getMessages(
        User $user,
        Contact|Chat $messageSource,
        ?int $limit,
        int $type = null,
        Message $afterMessage = null,
        #[ExpectedValues(Message::ORDERS)] string $order = 'DESC'
    ): array
    {
        $data = collect();

        if ($messageSource instanceof Contact) {
            $messageSource = $this->getSingleChat($messageSource);

            if (!$messageSource) {
                return [];
            }
        }

        $messages = $this->messageRepository->getByChatId(
            $messageSource->id,
            $user->id,
            $limit,
            $type,
            $afterMessage?->id,
        );

        foreach ($messages as $message) {
            $this->registerMessageObserve($message, $user);
            $data->add($this->getArrayMessage($user, $message, true));
            $this->markMessageAsRead($message, $user);
        }

        return array_values(
            $data->sortBy(callback: 'real_id', descending: $order === 'DESC')
                ->map(function (array $item) {
                    unset($item['real_id']);

                    return $item;
                })
                ->toArray()
        );
    }

    /**
     * Observe message.
     *
     * @param User $user
     * @param Message $messageFrom
     * @param Message|null $messageTo
     * @return array
     */
    public function observe(User $user, Message $messageFrom, ?Message $messageTo = null): array
    {
        $messages = [];
        $params = [$messageFrom->id];

        if ($messageTo) {
            $params[] = $messageTo->id;
        }

        $observed = $observed = $this->messageRepository->getByChatIdObserve(
            $messageFrom->messages_chats_id,
            $user->id,
            $params,
            true
        );

        foreach ($observed as $observe) {
            if ($observe->checkDeleted($user)) {
                $messages[] = [
                    'id' => $observe->sid,
                    'deleted' => true,
                ];
            } else if ($this->hasChanges($observe, $user)) {
                $messages[] = $this->getArrayMessage($user, $observe);
                $this->registerMessageObserve($observe, $user);
            }
        }

        return $messages;
    }

    /**
     * Get message as array.
     *
     * @param User $user
     * @param Message $message
     * @param bool $withRealId
     * @return array
     */
    #[ArrayShape([
        'id' => "int",
        'message' => "string",
        'read' => "bool|null",
        'author' => "array",
        'parent' => "array|null",
        'parent_del' => "bool",
        'created_at' => "string",
        'real_id' => "int",
    ])]
    public function getArrayMessage(User $user, Message $message, bool $withRealId = false): array
    {
        $parentMessage = $message->parent;
        $parent = null;
        $parentDel = false;

        if ($parentMessage) {
            $parentDel = $this->checkDeletedMessage($user, $parentMessage);

            if (!$parentDel) {
                $parent = $this->getArrayMessage($user, $parentMessage);
            }
        }

        $data = [
            'id' => $message->sid,
            'message' => $message->message,
            'read' => $message->isAuthor($user) ? $message->checkRead() : null,
            'author' => [
                'username' => $message->author_username,
                'avatar' => Storage::disk('local')->url($message->author_avatar),
            ],
            'parent' => $parent,
            'parent_del' => $parentDel,
            'created_at' => $message->created_at->format('Y-m-d H:i:s'),
        ];

        if ($withRealId) {
            $data['real_id'] = $message->id;
        }

        return $data;
    }

    /**
     * Mark message as read if this user is not author.
     *
     * @param Message $message
     * @param User $user
     */
    public function markMessageAsRead(Message $message, User $user): void
    {
        if (!$message->isAuthor($user) && !$message->checkReadUser($user)) {
            $data = [
                'messages_id' => $message->id,
                'users_id' => $user->id
            ];

            $this->messageReadRepository->updateOrCreate($data, $data);
        }
    }

    /**
     * Accept invitation.
     *
     * @param User $user
     * @param Chat $chat
     * @throws InvitationException
     */
    public function acceptInvitation(User $user, Chat $chat): void
    {
        if ($chat->checkAcceptedInvitation($user)) {
            throw new \App\Modules\Messages\Exceptions\InvitationException('Invitation already accepted.');
        }

        $this->chatAcceptedInvitationRepository->create([
            'messages_chats_id' => $chat->id,
            'users_id' => $user->id
        ]);
    }

    /**
     * Delete chat user.
     * (soft delete)
     *
     * @param ChatUser $chatUser
     */
    public function deleteChatUser(ChatUser $chatUser): void
    {
        $chatUser->deleted = true;
        $chatUser->save();
    }

    /**
     * Add user to chat.
     *
     * @param Chat $chat
     * @param User $newUser
     * @return ChatUser
     */
    public function addUserToChat(Chat $chat, User $newUser): ChatUser
    {
        $chatUser = $this->chatUserRepository->getChatUser($chat->id, $newUser->id, true);

        if ($chatUser) {
            $chatUser->deleted = false;
            $chatUser->save();
        } else {
            $chatUser = $this->chatUserRepository->create([
                'messages_chats_id' => $chat->id,
                'users_id' => $newUser->id,
                'is_owner' => false
            ]);
        }

        return $chatUser;
    }

    /**
     * Clean messages history.
     *
     * @param User $user
     * @param Chat $chat
     */
    public function cleanMessages(User $user, Chat $chat): void
    {
        foreach ($this->messageRepository
                     ->getIdsByChatId($chat->id, $user->id) as $messageId) {
            $this->messageDeletedRepository->create([
                'messages_id' => $messageId,
                'users_id' => $user->id
            ]);
        }
    }

    /**
     * Check deleted message.
     *
     * @param User $user
     * @param Message $message
     * @return bool
     */
    public function checkDeletedMessage(User $user, Message $message): bool
    {
        return $message->deleted ||
            $this->messageDeletedRepository->checkDeleted($message->id, $user->id);
    }

    /**
     * Delete message.
     *
     * @param User $user
     * @param Message $message
     * @param bool $forAll
     */
    public function deleteMessage(User $user, Message $message, bool $forAll): void
    {
        if ($forAll) {
            $message->deleted = true;

            $message->save();
        } else {
            $this->messageDeletedRepository->create([
                'messages_id' => $message->id,
                'users_id' => $user->id
            ]);
        }
    }

    /**
     * Delete chat.
     *
     * @param Chat $chat
     * @param User $user
     * @param bool $cleanMessages
     */
    public function deleteChat(Chat $chat, User $user, bool $cleanMessages = true): void
    {
        if ($cleanMessages) {
            $this->cleanMessages($user, $chat);
        }

        if (!$this->chatDeletedRepository->checkDeleted($chat->id, $user->id)) {
            $this->chatDeletedRepository->create([
                'messages_chats_id' => $chat->id,
                'users_id' => $user->id
            ]);
        }
    }

    /**
     * Delete contact.
     *
     * @param Contact $contact
     */
    public function deleteContact(Contact $contact): void
    {
        $contact->deleted = true;

        $contact->save();
    }

    /**
     * Block user.
     *
     * @param User $user
     * @param User $blockedUser
     * @return Contact
     * @throws ChatUserNotFoundException
     * @throws ChatAlreadyExistsException
     */
    public function userBlock(User $user, User $blockedUser): Contact
    {
        $contact = $this->contactRepository->getContact($user->id, $blockedUser->id);

        if (!$contact) {
            $contact = $this->addContact($user, $blockedUser);
        }

        $chats = $this->chatRepository->getChatsByContact($user->id, $blockedUser->id);

        foreach ($chats as $chat) {
            if ($chat->is_group) {
                if ($chat->isOwner($blockedUser)) {
                    $this->leaveGroupChat($user, $chat);
                }
            } else {
                $this->deleteChat($chat, $user, false);
            }
        }

        foreach ($this->chatRepository
                     ->getGroupChatsByOwner($user->id) as $chat) {
            if ($chat->hasUser($blockedUser)) {
                $this->leaveGroupChat($blockedUser, $chat);
            }
        }

        $contact->blocked = true;

        $contact->save();

        return $contact;
    }

    /**
     * Contact unblock.
     *
     * @param Contact $contact
     */
    public function contactUnblock(Contact $contact): void
    {
        $contact->blocked = false;

        $contact->save();
    }

    /**
     * Search messages.
     *
     * @param User $user
     * @param string $search
     * @param Chat|null $chat
     * @param int $limit
     * @return array
     */
    public function search(
        User $user,
        string $search,
        ?Chat $chat = null,
        int $limit = Message::SEARCH_LIMIT
    ): array
    {
        $chatIds = $chat ?
            [$chat->id] :
            $this->chatRepository
                ->getChats($user->id)
                ->pluck('id')
                ->toArray();

        $data = [];

        foreach (
            $this->messageRepository
                ->search($user->id, $chatIds, $search, $limit) as $message
        ) {
            $data[] = [
                'id' => $message->sid,
                'message' => $message->message,
                'created_at' => $message->created_at->format('Y-m-d H:i:s'),
                'author' => [
                    'username' => $message->author_username,
                    'avatar' => Storage::disk('local')->url($message->author_avatar),
                ],
                'chat' => [
                    'id' => $message->chat->sid,
                    'name' => $message->chat->getChatName($user),
                    'avatar' => $message->chat->getAvatarUrl($user)
                ],
            ];
        }

        return $data;
    }

    /**
     * Count new message in all chats.
     *
     * @param User $user
     * @param Chat|null $chat
     * @return int
     */
    public function countNewMessages(User $user, Chat $chat = null): int
    {
        return $this->messageRepository->countUnread(
            $chat ?
                [$chat->id] :
                $this->chatRepository->getChatsQuery($user->id),
            $user->id
        );
    }

    /**
     * Get admin chat list.
     *
     * @param User $user
     * @return array
     * @throws Exception
     */
    public function getAdminChatList(User $user): array
    {
        $chats = $this->chatRepository->getAdminChats(
            $this->messageRepository->getMessagesQuery($user->id, true)
        );
        $data = collect([]);

        foreach ($chats as $chat) {
            $users = [];

            /** @var User $chatUser */
            foreach ($chat->chatUsers as $chatUser) {
                $users[] = [
                    'id' => $chatUser->user->id,
                    'username' => $chatUser->user->username,
                    'avatar' => Storage::disk('local')->url($chatUser->user->avatar),
                    'promo_code' => $chatUser->user->promo_code,
                    'is_owner' => $chatUser->is_owner,
                ];
            }

            $data->add([
                'id' => $chat->id,
                'sid' => $chat->sid,
                'name' => $chat->getAdminChatName(),
                'avatar' => $chat->getAdminAvatarUrl(),
                'count' => $chat->count_messages,
                'count_new' => $chat->count_messages_new,
                'is_group' => $chat->is_group,
                'users' => $users,
                'created_at' => $chat->created_at->format('Y-m-d H:i:s'),
                'last_message_at' => $chat->last_message_at,
                'sort' => new DateTime($chat->last_message_at ?? $chat->created_at->format('Y-m-d H:i:s'))
            ]);
        }

        return $data
            ->sortByDesc('sort')
            ->map(function ($item) {
                unset($item['sort']);

                return $item;
            })
            ->values()
            ->toArray();
    }

    /**
     * Get admin messages list.
     *
     * @param User $user
     * @param Chat $chat
     * @return array
     */
    public function getAdminMessagesList(User $user, Chat $chat): array
    {
        $data = [];

        $messages = $this->messageRepository->getByChatId(
            $chat->id,
            $user->id
        );

        foreach ($messages as $message) {
            $this->markMessageAsReadAdmin($message);

            $data[] = $this->getArrayMessage($user, $message);
        }

        return $data;
    }

    /**
     * Mark message as read admin.
     *
     * @param Message $message
     */
    public function markMessageAsReadAdmin(Message $message): void
    {
        $message->viewed_admin = true;

        $message->save();
    }

    /**
     * Get contact between users.
     *
     * @param User $user
     * @param User $contactUser
     * @return Contact|null
     */
    public function getContactBetweenUsers(User $user, User $contactUser): ?Contact
    {
        return $this->contactRepository
            ->getContact($user->id, $contactUser->id);
    }

    /**
     * Migrate contact to other user with all data.
     * If new user have chat with contact, chats will merge.
     *
     * @param Contact $contact
     * @param User $newUser
     * @throws Exception
     */
    public function migrateContact(Contact $contact, User $newUser): void
    {
        $oldUser = $contact->user;
        $oldChat = $this->chatRepository->getSingleChatByContact(
            $newUser->id,
            $contact->contact_users_id
        );

        $chat = $this->chatRepository->getSingleChatByContact(
            $oldUser->id,
            $contact->contact_users_id
        );

        if ($chat) {
            if ($oldChat) {
                //merge messages
                $this->migrateMessages($chat, $oldUser, $newUser, $oldChat);
                $chat->delete();
            } else {
                //move chat
                $this->migrateChat($chat, $oldUser, $newUser);
            }
        }

        if (!$this->contactRepository->existsContact(
            $newUser->id,
            $contact->contact_users_id
        )) {
            $contact->users_id = $newUser->id;

            $contact->save();
        } else {
            $contact->delete();
        }
    }

    /**
     * Migrate group chats.
     *
     * @param User $currentUser
     * @param User $newUser
     * @param Collection $contactUsers
     */
    public function migrateGroupChats(User $currentUser, User $newUser, Collection $contactUsers): void
    {
        $chats = $this->chatRepository->getGroupChatsByOwner($currentUser->id);

        foreach ($chats as $chat) {
            $otherUserInChat = false;

            foreach ($chat->chatUsers as $chatUser) {
                if (
                    !$contactUsers
                        ->where('id', $chatUser->users_id)
                        ->first() &&
                    $chatUser->users_id !== $currentUser->id
                ) {
                    $otherUserInChat = true;

                    break;
                }
            }

            if (!$otherUserInChat) {
                //move chat
                $this->migrateChat($chat, $currentUser, $newUser);
            } else {
                //remove $contactUsers from this chat.
                foreach ($chat->chatUsers as $chatUser) {
                    if ($contactUsers
                        ->where('id', $chatUser->users_id)
                        ->first()) {
                        $this->deleteChatUser($chatUser);
                    }
                }
            }
        }
    }

    /**
     * Migrate chats/group chats/messages
     * from user to new user by contact users.
     *
     * @param User $currentUser
     * @param User $newUser
     * @param Collection $contactUsers
     * @throws Exception
     */
    public function migrate(User $currentUser, User $newUser, Collection $contactUsers): void
    {
        foreach ($contactUsers as $contactUser) {
            $contact = $this->getContactBetweenUsers($currentUser, $contactUser);

            if ($contact) {
                $this->migrateContact($contact, $newUser);
            }
        }

        $this->migrateGroupChats($currentUser, $newUser, $contactUsers);
    }

    /**
     * Get single chat.
     *
     * @param Contact $contact
     * @return Chat|null
     */
    public function getSingleChat(Contact $contact): ?Chat
    {
        return $this->chatRepository->getSingleChatByContact(
            $contact->users_id,
            $contact->contact_users_id,
        );
    }

    /**
     * Migrate chat.
     *
     * @param Chat $chat
     * @param User $oldUser
     * @param User $newUser
     */
    private function migrateChat(Chat $chat, User $oldUser, User $newUser): void
    {
        $chatUser = $chat->chatUsers
            ->where('users_id', $oldUser->id)
            ->first();
        $chatUser->users_id = $newUser->id;

        $chatUser->save();
        $this->migrateMessages($chat, $oldUser, $newUser);
    }

    /**
     * Migrate messages.
     *
     * @param Chat $chat
     * @param User $oldUser
     * @param User $newUser
     * @param Chat|null $newChat
     */
    private function migrateMessages(Chat $chat, User $oldUser, User $newUser, Chat $newChat = null): void
    {
        foreach ($chat->messages as $message) {
            if ($newChat) {
                $message->messages_chats_id = $newChat->id;
            }

            if ($message->author_users_id === $oldUser->id) {
                $message->author_users_id = $newUser->id;
            }

            $message->save();
        }
    }

    /**
     * Register message for observing.
     *
     * @param Message $message
     * @param User $user
     */
    private function registerMessageObserve(Message $message, User $user): void
    {
        $messageData = $this->getArrayMessage($user, $message, true);
        $key = sprintf('messages_%s_%s', $messageData['id'], $user->id);
        $crc32 = crc32(json_encode($messageData));

        Cache::put($key, $crc32);
    }

    /**
     * Check message changes.
     *
     * @param Message $message
     * @param User $user
     * @return bool
     */
    private function hasChanges(Message $message, User $user): bool
    {
        $messageData = $this->getArrayMessage($user, $message, true);
        $key = sprintf('messages_%s_%s', $messageData['id'], $user->id);
        $crc32 = crc32(json_encode($messageData));

        return (int)Cache::get($key) !== $crc32;
    }
}
