<?php
declare(strict_types=1);

namespace App\Modules\Money\Services;

use App\Modules\Money\Models\Transaction;
use App\Modules\Money\Repositories\TransactionRepository;

/**
 * Transaction stats service.
 */
final class TransactionStatsService
{
    /**
     * Sum abc bought for deposited.
     */
    public function sumAbcBoughtDeposited(): float
    {
        $absTransactions = $this->getTransactionRepository()
            ->getByTypeAndCurrency(Transaction::TYPE_EXCHANGE_USDT_ABC, Transaction::CURRENCY_ABC);
        $sum = 0;

        /** @var Transaction $transaction */
        foreach ($absTransactions as $transaction) {
            if ($transaction->parent &&
                $transaction->parent->isCurrency(Transaction::CURRENCY_USDT_DEPOSITED)
            ) {
                $sum += $transaction->amount;
            }
        }

        return $sum;
    }

    /**
     * Sum abc bought for dividends.
     */
    public function sumAbcBoughtDividends(): float
    {
        $absTransactions = $this->getTransactionRepository()
            ->getByTypeAndCurrency(Transaction::TYPE_EXCHANGE_USDT_ABC, Transaction::CURRENCY_ABC);
        $sum = 0;

        /** @var Transaction $transaction */
        foreach ($absTransactions as $transaction) {
            $sumDeposited = 0;
            $sumDividends = 0;

            if ($transaction->parent &&
                $transaction->parent->isCurrency(Transaction::CURRENCY_USDT_DEPOSITED)
            ) {
                $parentTransaction = $transaction->parent;
                $sumDeposited += $parentTransaction->amount;

                if($parentTransaction->parent &&
                    $parentTransaction->parent->isCurrency(Transaction::CURRENCY_USDT_DIVIDENTS)
                ) {
                    $sumDividends += $parentTransaction->parent->amount;
                }
            }

            $totalSum = $sumDeposited+$sumDividends;

            if($totalSum > 0) {
                $percentDividends = $sumDividends / $totalSum * 100;
                $sum += $transaction->amount / 100 * $percentDividends;
            }
        }

        return $sum;
    }

    /**
     * Get {@see TransactionRepository::class}.
     *
     * @return TransactionRepository
     */
    private function getTransactionRepository(): TransactionRepository
    {
        return app(TransactionRepository::class);
    }
}
