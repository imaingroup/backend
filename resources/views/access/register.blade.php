<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title translate="page_access.page.register.title">@lang('page_access.page.register.title')</title>
    <link rel="stylesheet" href="/assets/register/css/global.css">
    <link rel="stylesheet" href="/assets/register/css/styles.css">
    <link rel="stylesheet" href="/assets/register/css/adaptive.css">
</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

    <!-- Main -->
    <main class="main registration-page">
        <div class="inner">

            <div class="logo rL">
                <a class="rL flex d_col cent a_cent cover"
                   style="background-image: url('/assets/register/images/logo-bg.png');" href="/">
                    <img src="/assets/register/images/logo.png" alt="">
                </a>
            </div>

            <!-- Registration -->
            <div class="reg">

                <div class="reg-header">
                    <h1 translate="page_access.page.register.header_registration">@lang('page_access.page.register.header_registration')</h1>
                    <p><span
                            translate="page_access.page.register.header_back_to">@lang('page_access.page.register.header_back_to')</span>
                        <a href="#"
                           translate="page_access.page.register.header_login">@lang('page_access.page.register.header_login')</a>
                    </p>
                </div>


                <form id="reg-form" class="reg-form" action="/" method="POST">

                    <div class="reg-form-group">
                        <label for="email">
                            <span
                                translate="page_access.page.register.email.label">@lang('page_access.page.register.email.label')</span>
                            <span class="error email" style="display:none;"></span>
                        </label>
                        <input id="email" class="inputbox rL" name="email" type="email"
                               translate="page_access.page.register.email.placeholder"
                               placeholder="@lang('page_access.page.register.email.placeholder')" autocomplete="off">
                    </div>

                    <div class="reg-form-group">
                        <label for="username">
                            <span
                                translate="page_access.page.register.username.label">@lang('page_access.page.register.username.label')</span>
                            <span class="error username" style="display:none;"></span>
                        </label>
                        <input id="username" class="inputbox rL" name="username" type="text"
                               translate="page_access.page.register.username.placeholder"
                               placeholder="@lang('page_access.page.register.username.placeholder')" autocomplete="off">
                    </div>

                    <div class="reg-form-group">
                        <label for="password">
                            <span
                                translate="page_access.page.register.password.label">@lang('page_access.page.register.password.label')</span>
                            <span class="error password" style="display:none;"></span>
                        </label>
                        <input id="password" class="inputbox rL" name="password" type="password"
                               translate="page_access.page.register.password.placeholder"
                               placeholder="@lang('page_access.page.register.password.placeholder')" autocomplete="off">
                    </div>

                    <div class="reg-form-group">
                        <label for="password_confirmation">
                            <span
                                translate="page_access.page.register.repeat_password.label">@lang('page_access.page.register.repeat_password.label')</span>
                            <span class="error password_confirmation" style="display:none;"></span>
                        </label>
                        <input id="password_confirmation" class="inputbox rL" name="password_confirmation"
                               type="password" translate="page_access.page.register.repeat_password.placeholder"
                               placeholder="@lang('page_access.page.register.repeat_password.placeholder')"
                               autocomplete="off">
                    </div>

                    <div class="reg-form-group">
                        <label for="promo_code">
                            <span translate="page_access.page.register.promo_code.label">@lang('page_access.page.register.promo_code.label')</span> <span
                                class="low">@lang('page_access.page.register.promo_code.optional')</span>
                            <span class="error promo_code" style="display:none;"></span>
                        </label>
                        <input id="promo_code" class="inputbox" name="promo_code" type="text"
                               translate="page_access.page.register.promo_code.placeholder"
                               placeholder="@lang('page_access.page.register.promo_code.placeholder')"
                               autocomplete="off">
                    </div>

                    <div class="reg-form-check">
                        <input id="is_accepted_terms" name="is_accepted_terms" type="checkbox">
                        <label for="is_accepted_terms">
                            <b></b>
                            <span
                                translate="page_access.page.register.check.label">@lang('page_access.page.register.check.label')</span>
                        </label>
                        <a href="#"
                           translate="page_access.page.register.check.link">@lang('page_access.page.register.check.link')</a>
                        <span class="error is_accepted_terms" style="display:none;"></span>
                    </div>

                    <div class="reg-form-group">
                        <div>
                            <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                        </div>
                        <span class="error g-recaptcha-response" style="display:none;"></span>
                    </div>

                    <div class="reg-form-group">
                        <span class="error global" style="display: none"></span>
                    </div>

                    <button class="reg-form-btn" type="submit"
                            translate="page_access.page.register.submit">@lang('page_access.page.register.submit')</button>

                </form>
            </div>
            <!-- ./ Registration -->


        </div>
        <div id="subfooter"></div>
    </main>
    <!-- ./ Main -->

</div>
<!-- ./ Wrapper -->

<!-- Footer -->
<div id="footer">
    <p class="footer-copyright" translate="base.copyright">@lang('base.copyright')</p>
</div>
<!-- ./ Footer -->


<!-- Scripts -->
<script src="/assets/register/js/jquery-3.5.1.js"></script>
<script src="/assets/register/js/jquery.validate.js"></script>
<script src="/assets/register/js/script.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<!-- ./ Scripts -->

<link rel="stylesheet" href="/assets/register/css/fonts.css">


</body>
</html>
