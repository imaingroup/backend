<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Add field deposit min busdt.
 */
final class AddFieldDepositMinBSUDT extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->float('deposit_min_busdt', 16, 8)
                ->after('deposit_min_usdt')
                ->default(50);

            $table->boolean('is_block_busdt')
                ->default(false)
                ->after('is_block_usdt');
        });

        DB::transaction(function() {
            $typeId = DB::table('data_types')
                ->where('name', 'settings_deposit')
                ->first()
                ->id;

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'deposit_min_busdt',
                    'type' => 'number',
                    'display_name' => 'Deposit min busdt',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 0
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'is_block_busdt',
                    'type' => 'checkbox',
                    'display_name' => 'BUSDT block',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 3
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->dropColumn('deposit_min_busdt');
            $table->dropColumn('is_block_busdt');
        });

        DB::transaction(function() {
            $typeId = DB::table('data_types')
                ->where('name', 'settings_deposit')
                ->first()
                ->id;

            DB::table('data_rows')
                ->where('data_type_id', $typeId)
                ->whereIn('field', [
                    'deposit_min_busdt',
                    'is_block_busdt',
                ])
                ->delete();
        });
    }
}
