<?php
return [
    'mass_money_operation' => '[0,*]The administrator (user id: :admin_user_id (:admin_username)) added :amount :currency
all users who activated their account are not banned
and logged into your account at least 1 time. :count_affected affected by this operation
users. Total added amount :total_amount :currency. Date of transaction :date_operation.|[-0,-*]The administrator
(user id: :admin_user_id (:admin_username)) has withdrawn money :amount :currency
all users who activated their account are not banned
and logged into your account at least 1 time. This operation affected :count_affected
users. Total amount withdrawn :total_amount :currency. Operation date :date_operation.',

    'moved_teammates' => 'The administrator (user id: :admin_user_id (:admin_username)) move teammates from :old_team_lead team lead
to :new_team_lead new team lead. This operation affected :count_teammates teammates.'
];
