<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * This migration updates admin.
 */
final class UpdateAdmin extends Migration
{
    private const DATA_C = [
        'App\Http\Controllers\Voyager\VoyagerUsersController' => 'App\Modules\Admin\Controllers\VoyagerUsersController',
        'App\Http\Controllers\Voyager\VoyagerProfitCardsController' => 'App\Modules\Admin\Controllers\VoyagerProfitCardsController',
        'App\Http\Controllers\Voyager\VoyagerWithdrawController' => 'App\Modules\Admin\Controllers\VoyagerWithdrawController',
        'App\Http\Controllers\Voyager\VoyagerTidHistoryController' => 'App\Modules\Admin\Controllers\VoyagerTidHistoryController',
        'App\Http\Controllers\Voyager\VoyagerTasksController' => 'App\Modules\Admin\Controllers\VoyagerTasksController',
    ];

    private const DATA_M = [
        'App\Models\Deposit' => 'App\Modules\Deposites\Models\Deposit',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::beginTransaction();

        foreach (self::DATA_C as $key => $new) {
            DB::table('data_types')
                ->where('controller', $key)
                ->update([
                    'controller' => $new
                ]);
        }


        foreach (self::DATA_M as $key => $new) {
            DB::table('data_types')
                ->where('model_name', $key)
                ->update([
                    'model_name' => $new
                ]);
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::beginTransaction();

        foreach (array_flip(self::DATA_C) as $key => $new) {
            DB::table('data_types')
                ->where('controller', $key)
                ->update([
                    'controller' => $new
                ]);
        }


        foreach (array_flip(self::DATA_M) as $key => $new) {
            DB::table('data_types')
                ->where('model_name', $key)
                ->update([
                    'model_name' => $new
                ]);
        }

        DB::commit();
    }
}
