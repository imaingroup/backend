<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->longText('images')->nullable(true)->change();
            $table->longText('video')->nullable(true)->change();
            $table->text('external_preview')->nullable(true)->change();
            $table->longText('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->longText('images')->nullable(false)->change();
            $table->longText('video')->nullable(false)->change();
            $table->text('external_preview')->nullable(false)->change();
            $table->dropColumn('text');
        });
    }
}
