<?php


namespace App\Modules\Core\Exceptions;


use Exception;

/**
 * This exception may be throws in data service.
 */
final class DataException extends Exception
{

}
