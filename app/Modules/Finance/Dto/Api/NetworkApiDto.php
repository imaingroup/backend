<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use Illuminate\Support\Collection;

/**
 * NetworkApiDto.
 *
 * @property-read string $name
 * @property-read int $type
 * @property-read int|null $id
 * @property-read Collection|TokenApiDto[] $currencies
 */
final class NetworkApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param string $name
     * @param int $type
     * @param int|null $id
     * @param Collection $currencies
     */
    public function __construct(
        protected string $name,
        protected int $type,
        protected ?int $id,
        protected Collection $currencies,
    )
    {
    }
}
