<?php
declare(strict_types=1);

namespace App\Modules\Money\Repositories;

use App\Modules\Money\Models\Transaction;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * Transaction repository.
 */
final class TransactionRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Transaction $model
     */
    public function __construct(protected Transaction $model)
    {}

    /**
     * Get by type and currency.
     *
     * @param int $type
     * @param int $currency
     * @return Collection
     */
    public function getByTypeAndCurrency(
        #[ExpectedValues(Transaction::TYPES_LIST)] int $type,
        #[ExpectedValues(Transaction::CURRENCY_LIST)] int $currency,
    ): Collection
    {
        return $this->model
            ->newQuery()
            ->with('parent')
            ->with('parent.parent')
            ->where('type', $type)
            ->where('currency', $currency)
            ->get();
    }
}
