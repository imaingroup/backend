<?php

namespace App\Modules\Investments\Models;

use App\Modules\Core\Models\Model;

final class TradeAccruals extends Model
{
    protected $table = 'trades_accruals';
}
