<?php
declare(strict_types=1);

use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Create crypto networks.
 */
final class CreateCryptoNetworks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('crypto_networks', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->tinyInteger('type');
            $table->timestamps();
            $table->unique(['code', 'type'], 'crypto_networks_code_type_unique');
        });

        DB::transaction(function () {
            collect(
                array_merge(EthereumNetworkEnum::cases(), BitcoinNetworkEnum::cases())
            )
                ->each(function (UnitEnum|EthereumNetworkEnum|BitcoinNetworkEnum $enum) {
                    DB::table('crypto_networks')
                        ->insert([
                            'code' => $enum->value,
                            'type' => $enum->type(),
                            'created_at' => now(),
                            'updated_at' => now(),
                        ]);
                });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('crypto_networks');
    }
}
