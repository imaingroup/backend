<?php
declare(strict_types=1);

namespace App\Modules\Stats\Dto\Services;


use App\Modules\Core\Dto\BaseDto;

/**
 * Статистика покупок.
 *
 * @property-read float $sumCards
 * @property-read float $sumBonusCards
 * @property-read float $sumBoughtDeposited
 * @property-read float $sumBoughtDividends
 */
final class PurchaseStatsDto extends BaseDto
{
    /**
     * Конструктор.
     *
     * @param float $sumCards
     * @param float $sumBonusCards
     * @param float $sumBoughtDeposited
     * @param float $sumBoughtDividends
     */
    public function __construct(
        protected float $sumCards,
        protected float $sumBonusCards,
        protected float $sumBoughtDeposited,
        protected float $sumBoughtDividends,
    )
    {

    }
}
