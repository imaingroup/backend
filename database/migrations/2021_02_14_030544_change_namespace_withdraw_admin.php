<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * This migration changes namespace
 * for model withdraw in admin.
 */
final class ChangeNamespaceWithdrawAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('data_types')
            ->where('model_name', 'App\Models\Withdraw')
            ->update([
                'model_name' => 'App\Modules\Withdraw\Models\Withdraw'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('data_types')
            ->where('model_name', 'App\Modules\Withdraw\Models\Withdraw')
            ->update([
                'model_name' => 'App\Models\Withdraw'
            ]);
    }
}
