<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Withdraw;

/**
 * NotSupportedTokenException.
 */
class NotSupportedTokenException extends WithdrawException
{
}
