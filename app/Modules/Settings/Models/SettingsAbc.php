<?php
declare(strict_types=1);

namespace App\Modules\Settings\Models;

use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Core\Models\Model;
use Carbon\Carbon;
use TCG\Voyager\Traits\Translatable;

/**
 * Settings abc.
 *
 * @property-read int $id
 * @property string $total_supply_title
 * @property float $total_supply
 * @property string $soft_cap_title
 * @property float $soft_cap
 * @property string $abc_sold_title
 * @property float $abc_sold
 * @property float $abc_sold_luft
 * @property string $abc_left_title
 * @property float|null $counting_off_trigger
 * @property boolean $counting_off
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read float $abcLeft
 */
final class SettingsAbc extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;

    protected $translatable = ['total_supply_title', 'soft_cap_title', 'abc_sold_title', 'abc_left_title'];

    protected $table = 'settings_abc';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'total_supply_title' => 'string',
        'total_supply' => 'float',
        'soft_cap_title' => 'string',
        'soft_cap' => 'float',
        'abc_sold_title' => 'string',
        'abc_sold' => 'float',
        'abc_sold_luft' => 'float',
        'abc_left_title' => 'string',
        'counting_off_trigger' => 'float',
        'counting_off' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Events for model.
     */
    protected static function booted()
    {
        self::saving(function (self $model) {
            if (!$model->counting_off && $model->abcLeft <= $model->counting_off_trigger) {
                $model->counting_off = true;
            }

            if ($model->isDirty('abc_sold') && $model->counting_off) {
                $model->abc_sold = $model->getOriginal('abc_sold');
            }
        });
    }

    /**
     * Get "abcLeft" attribute.
     *
     * @return float
     */
    public function getAbcLeftAttribute(): float
    {
        return $this->soft_cap - $this->abc_sold;
    }
}
