<?php
declare(strict_types=1);

namespace App\Modules\Investments\Commands;

use App\Modules\Logs\Loggers\Logger;
use App\Modules\Core\Traits\TransactionTrait;
use App\Modules\Investments\Services\StatsService;
use App\Modules\Security\Services\Identification\IdentificationService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Console\Command;
use Illuminate\Validation\ValidationException;

/**
 * This command update trade option default
 * data for previous month.
 */
final class TradeOptionStatsPreviousMonthUpdateCommand extends Command
{
    use TransactionTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'investments:trade_options:stats:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command need for update multiaccounts data';

    /**
     * Create a new command instance.
     *
     * @param \App\Modules\Security\Services\Identification\IdentificationService $identificationService
     * @param \App\Modules\Logs\Loggers\Logger $logger
     */
    public function __construct(
        private IdentificationService $identificationService,
        private Logger $logger
    )
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function handle(): int
    {
        throw new \RuntimeException();

        return $this->inTransaction(function(): int {
            $this->getStatsService()->updatePreviousPercent();

            return 0;
        }, function(): int {
            return -1;
        });
    }

    /**
     * Get StatsService.
     *
     * @return StatsService
     */
    private function getStatsService(): StatsService
    {
        return app(StatsService::class);
    }
}
