<?php
declare(strict_types=1);

namespace App\Modules\Messages\Models;

use App\Modules\Core\Models\Model;

final class ChatAcceptedInvitation extends Model
{
    protected $table = 'messages_chats_accepted_invitations';
}
