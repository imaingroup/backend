@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Manage currencies')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-folder"></i> Manage currencies
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid" id="currencies-control">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body" v-if="loaded">
                        <div>
                            <h2>Available currencies</h2>

                            <table class="table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Network</th>
                                    <th>Deposit min</th>
                                    <th>Deposit block</th>
                                    <th>Conf min</th>
                                    <th>Luft deposit</th>
                                    <th>Luft withdraw</th>
                                    <th>Custom rate</th>
                                    <th>Rate</th>
                                    <th>Order</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="item in data">
                                    <td><img
                                            v-if="item.image"
                                            :src="item.image"
                                            style="width: 25px; height: 25px;"/></td>
                                    <td>
                                        <div>@{{ item.name }}</div>
                                        <div>@{{ item.fullName }}</div>

                                    </td>
                                    <td>@{{ item.network }}</td>
                                    <td>@{{ item.depositMin }}</td>
                                    <td>@{{ item.depositBlock ? 'yes' : 'no' }}</td>
                                    <td>@{{ item.confirmationsMin }}</td>
                                    <td>@{{ item.luftDeposit }}</td>
                                    <td>@{{ item.luftWithdraw }}</td>
                                    <td>@{{ item.customRate ? 'yes' : 'no' }}</td>
                                    <td>@{{ item.rate }} USDT</td>
                                    <td>@{{ item.order }}</td>
                                    <td>
                                        <button class="btn btn-sm btn-primary"
                                                @click.prevent="editCurrency(item)" :disabled="networks.length === 0">
                                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                        </button>
                                        <button class="btn btn-sm btn-warning"
                                                @click.prevent="deleteCurrency(item.id)">
                                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Delete</span>
                                        </button>
                                    </td>
                                </tr>
                                <tr v-if="data.length === 0">
                                    <td colspan="9">The currencies haven't added yet.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div>
                            <h2>Set currency</h2>

                            <div>
                                <select v-model="network">
                                    <option :value="null">Select network</option>
                                    <option v-for="network in networks" :value="network.id">@{{ network.name }} [@{{
                                        getNetworkTextType(network.type) }}]
                                    </option>
                                </select>

                                <select v-model="currency" v-if="network">
                                    <option :value="null">Select currency</option>
                                    <option v-for="currencyItem in currenciesByNetwork" :value="currencyItem.id"
                                            v-if="currencyItem.stats">
                                        @{{
                                        currencyItem.name }} [@{{ currencyItem.symbol }}] [@{{ !currencyItem.address ?
                                        'Main/' :
                                        ''
                                        }}@{{
                                        currencyItem.stats.count }} transactions/@{{ currencyItem.stats.amount }}
                                        amount]
                                    </option>
                                    <option :value="currencyItem.id" v-else>
                                        @{{ currencyItem.name }} [@{{ currencyItem.symbol }}]
                                    </option>
                                </select>
                            </div>
                            <form role="form" @submit.prevent="setCurrency">
                                <div class="parameters" v-if="currency && selectedCurrency">
                                    <h3>Currency information</h3>

                                    <div class="list" v-if="selectedCurrency.tokenType">
                                        <span class="opt">Token tracker</span>
                                        <span class="value"><a :href="trackerUrl"
                                                               target="_blank">@{{ trackerUrl }}</a></span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Type</span>
                                        <span class="value">@{{ selectedCurrency.tokenType ? selectedCurrency.tokenType : 'Main currency' }}</span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Name</span>
                                        <span class="value">@{{ selectedCurrency.name }}</span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Code</span>
                                        <span class="value">@{{ selectedCurrency.symbol }}</span>
                                    </div>
                                    <div class="list" v-if="selectedCurrency.stats">
                                        <span class="opt">Transactions</span>
                                        <span class="value">@{{ selectedCurrency.stats.count }}</span>
                                    </div>
                                    <div class="list" v-if="selectedCurrency.stats">
                                        <span class="opt">Amount</span>
                                        <span class="value">@{{ selectedCurrency.stats.amount }}</span>
                                    </div>
                                    <div class="list" v-if="selectedCurrency.stats">
                                        <span class="opt">Receivers</span>
                                        <span class="value">
                                        <span v-if="selectedCurrency.stats.receivers">@{{ selectedCurrency.stats.receivers }}</span>
                                        <span v-else style="color: green">Getting...</span>
                                    </span>
                                    </div>
                                    <div class="list" v-if="selectedCurrency.stats">
                                        <span class="opt">Senders</span>
                                        <span class="value">
                                        <span v-if="selectedCurrency.stats.senders">@{{ selectedCurrency.stats.senders }}</span>
                                        <span v-else style="color: green">Getting...</span>
                                    </span>
                                    </div>
                                    <div class="list" v-if="selectedCurrency.stats">
                                        <span class="opt">From date</span>
                                        <span class="value">
                                        <span v-if="selectedCurrency.stats.fromDate">@{{ selectedCurrency.stats.fromDate }}</span>
                                        <span v-else style="color: green">Getting...</span>
                                    </span>
                                    </div>
                                    <div class="list" v-if="selectedCurrency.stats">
                                        <span class="opt">Till date</span>
                                        <span class="value">
                                        <span v-if="selectedCurrency.stats.tillDate">@{{ selectedCurrency.stats.tillDate }}</span>
                                        <span v-else style="color: green">Getting...</span>
                                    </span>
                                    </div>
                                    <div class="list" v-if="selectedCurrency.stats">
                                        <span class="opt">Days</span>
                                        <span class="value">
                                        <span
                                            v-if="selectedCurrency.stats.days">@{{ selectedCurrency.stats.days }}</span>
                                        <span v-else style="color: green">Getting...</span>
                                    </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Rate</span>
                                        <span class="value">
                                        <span
                                            v-if="marketNotAvailable"
                                            style="color: green">Market data not available.</span>
                                        <span v-else-if="selectedCurrency.market">@{{ selectedCurrency.market.usdtPrice }} USDT</span>
                                        <span v-else style="color: green">Getting...</span>
                                    </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Rate Updated At</span>
                                        <span class="value">
                                        <span
                                            v-if="marketNotAvailable"
                                            style="color: green">Market data not available.</span>
                                        <span v-else-if="selectedCurrency.market">@{{ selectedCurrency.market.priceUpdatedAt }}</span>
                                        <span v-else style="color: green">Getting...</span>
                                    </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Image</span>
                                        <span class="value">
                                        <span
                                            v-if="marketNotAvailable"
                                            style="color: green">Market data not available.</span>
                                        <span
                                            v-else-if="selectedCurrency.market && !selectedCurrency.market.image"></span>
                                        <span v-else-if="selectedCurrency.market"><img
                                                :src="selectedCurrency.market.image"
                                                style="width: 50px; height: 50px;"/></span>
                                        <span v-else style="color: green">Getting...</span>
                                    </span>
                                    </div>

                                    <h3>Form data</h3>

                                    <div class="list">
                                        <span class="opt">Deposit min</span>
                                        <span class="value">
                                            <input class="form-control"
                                                   v-model="form.deposit_min">
                                        <div v-for="error in errors.deposit_min"
                                             class="text-error">@{{ error }}
                                            </div>
                                        </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Deposit block</span>
                                        <span class="value">
                                        <div @click.prevent="form.deposit_block=!form.deposit_block" class="toggle btn"
                                             :class="{'btn-default': !form.deposit_block, off: !form.deposit_block, 'btn-primary': form.deposit_block}"
                                             data-toggle="toggle"
                                             style="width: 68.3px; height: 29px;"><input type="checkbox"
                                                                                         v-model="form.deposit_block"
                                                                                         class="toggleswitch"><div
                                                class="toggle-group"><label class="btn btn-primary toggle-on">On</label><label
                                                    class="btn btn-default active toggle-off">Off</label><span
                                                    class="toggle-handle btn btn-default"></span></div></div>


                                        <div v-for="error in errors.deposit_block"
                                             class="text-error">@{{ error }}
                                            </div>
                                    </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Confirmations min</span>
                                        <span class="value"><input class="form-control"
                                                                   v-model="form.confirmations_min">
                                        <div v-for="error in errors.confirmations_min"
                                             class="text-error">@{{ error }}
                                            </div>
                                        </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Luft deposit</span>
                                        <span class="value"><input class="form-control"
                                                                   v-model="form.luft_deposit">
                                        <div v-for="error in errors.luft_deposit"
                                             class="text-error">@{{ error }}
                                            </div>
                                        </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Luft withdraw</span>
                                        <span class="value"><input class="form-control"
                                                                   v-model="form.luft_withdraw">
                                        <div v-for="error in errors.luft_withdraw"
                                             class="text-error">@{{ error }}
                                            </div>
                                        </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Rate</span>
                                        <span class="value">
                                            <input class="form-control" v-if="form.custom_rate"
                                                   v-model="form.rate">
                                            <input class="form-control" v-else :disabled="true"
                                                   :value="selectedCurrency.market ? selectedCurrency.market.usdtPrice : null">

                                        <div v-for="error in errors.rate"
                                             class="text-error">@{{ error }}
                                            </div>
                                        </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Custom rate</span>
                                        <span class="value">
                                            <div @click.prevent="form.custom_rate=!form.custom_rate"
                                                 class="toggle btn"
                                                 :class="{'btn-default': !form.custom_rate, off: !form.custom_rate, 'btn-primary': form.custom_rate}"
                                                 data-toggle="toggle"
                                                 style="width: 68.3px; height: 29px;"><input type="checkbox"
                                                                                             v-model="form.custom_rate"
                                                                                             class="toggleswitch"><div
                                                    class="toggle-group"><label
                                                        class="btn btn-primary toggle-on">On</label><label
                                                        class="btn btn-default active toggle-off">Off</label><span
                                                        class="toggle-handle btn btn-default"></span></div></div>

                                            <div v-for="error in errors.custom_rate"
                                                 class="text-error">@{{ error }}
                                            </div>
                                        </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Name</span>
                                        <span class="value"><input class="form-control" v-model="form.name">
                                        <div v-for="error in errors.name"
                                             class="text-error">@{{ error }}
                                            </div>
                                        </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Full name</span>
                                        <span class="value"><input class="form-control" v-model="form.full_name">
                                        <div v-for="error in errors.full_name"
                                             class="text-error">@{{ error }}
                                            </div>
                                        </span>
                                    </div>
                                    <div class="list">
                                        <span class="opt">Order</span>
                                        <span class="value"><input class="form-control" v-model="form.order"
                                                                   type="number">
                                        <div v-for="error in errors.order"
                                             class="text-error">@{{ error }}
                                            </div>
                                        </span>
                                    </div>
                                    <div class="list">
                                    <span class="value">
                                        <div style="display: flex">
                                                        <button type="button"
                                                                @click.prevent="currency = null"
                                                                style="margin-right: 15px"
                                                                class="form-control btn btn-sm btn-default">Close</button>
                                                        <button type="submit"
                                                                class="form-control btn btn-sm btn-primary">Save</button>
                                                    </div>
                                    </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="panel-body" v-else>
                        loading data...
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .select-wallet .active {
            color: black;
            font-weight: bold;
        }

        .select-wallet a {
            cursor: pointer;
            margin-right: 30px;
        }

        .parameters {
            margin-top: 20px;
        }

        .parameters .list {
            margin-bottom: 5px;
        }

        .parameters .list .opt {
            font-weight: bold;
            min-width: 140px;
            display: inline-block;
        }

        .parameters .list .opt:after {
            content: ":";
        }

        .parameters .list .value {
            display: inline-block;
        }

        .dangerous {
            background: red;
        }
    </style>
@stop
@push('javascript')
    <script>
        const NetworkTypes = {
            1: 'Bitcoin',
            2: 'Ethereum',
        };

        const Trackers = {
            'ethereum': 'https://etherscan.io',
            'ethclassic': 'https://etcblockexplorer.com',
            'bsc': 'https://bscscan.com',
            'velas': 'https://evmexplorer.velas.com',
            'matic': 'https://polygonscan.com',
            'klaytn': 'https://scope.klaytn.com',
        };

        new Vue({
            el: '#currencies-control',
            data() {
                return {
                    loaded: false,
                    loading: false,
                    data: [],
                    networks: [],
                    network: null,
                    currency: null,
                    marketNotAvailable: false,
                    formDefaults: {
                        deposit_min: null,
                        deposit_block: false,
                        confirmations_min: 1,
                        luft_deposit: 0,
                        luft_withdraw: 0,
                        rate: null,
                        custom_rate: false,
                        name: '',
                        order: 0,
                    },
                    form: {},
                    errors: {},
                };
            },
            async created() {
                this.getData();
                this.getCurrencies();
            },
            computed: {
                selectedNetwork() {
                    return this.networks.find((network) => network.id === this.network);
                },
                currenciesByNetwork() {
                    return this.selectedNetwork ? this.selectedNetwork.currencies : [];
                },
                selectedCurrency() {
                    return this.currenciesByNetwork.find((currency) => currency.id === this.currency);
                },
                trackerUrl() {
                    return this.selectedCurrency ?
                        Trackers[this.selectedNetwork.name] + '/address/' + this.selectedCurrency.address
                        : null;
                },
            },
            methods: {
                findNetworkByCurrencyId(currencyId) {
                    return this.networks.find((network) => {
                        return !!network.currencies.find((currency) => currency.id === currencyId);
                    });
                },
                findAvailableCurrency(currencyId) {
                    return this.data.find((currency) => currency.id === currencyId);
                },
                async getData() {
                    this.data = (await axios.get('{{ route('voyager.settings.currencies.data') }}'))
                        .data
                        .data;

                    this.loaded = true;
                },
                async setCurrency() {
                    if (this.loading) {
                        return;
                    }

                    this.loading = true;
                    this.errors = {};

                    await axios.put('{{ sprintf('/%s/settings/currencies/set/', config('voyager.prefix')) }}' + this.currency, this.form)
                        .then(async () => {
                            await this.getData();
                            this.currency = null;
                            this.network = null;

                            toastr.success('Currency settings have saved successfully.');
                        })
                        .catch(e => {
                            if (e.response.status === 422) {
                                this.errors = e.response.data.errors;
                            } else if ([401, 403].includes(e.response.status)) {
                                toastr.error('You are not authorized.');
                            } else {
                                toastr.error('Server error.');
                            }
                        })
                        .finally(() => this.loading = false);
                },
                async getCurrencies() {
                    this.networks = (await axios.get('{{ route('voyager.settings.currencies.list') }}'))
                        .data
                        .data;
                    this.loaded = true;
                },
                async getCurrencyDetails() {
                    const currencyDetails = (await axios.get('{{ sprintf('/%s/settings/currencies/detail/', config('voyager.prefix')) }}' + this.currency))
                        .data
                        .data;

                    const networkIndex = this.networks.findIndex((network) => network.id === this.network);
                    const currencyIndex = this.networks[networkIndex].currencies.findIndex((currency) => currency.id === this.currency);

                    if (currencyDetails) {
                        const network = this.networks[networkIndex];
                        network.currencies[currencyIndex] = currencyDetails;

                        this.$set(this.networks, networkIndex, network);
                    } else {
                        toastr.error('Can\'t to receive currency details.');
                    }
                },
                getNetworkTextType(type) {
                    return NetworkTypes[type];
                },
                editCurrency(currency) {
                    this.currency = null;
                    this.network = this.findNetworkByCurrencyId(currency.id).id;
                    this.currency = currency.id;
                },
                async deleteCurrency(currencyId) {
                    if (this.loading || !confirm('Are you sure that you have to remove this currency ?')) {
                        return;
                    }

                    this.loading = true;

                    await axios.delete('{{ sprintf('/%s/settings/currencies/delete/', config('voyager.prefix')) }}' + currencyId)
                        .then(async () => {
                            this.currency = null;
                            this.network = null;
                            await this.getData();

                            toastr.success('Currency has deleted.');
                        })
                        .catch(e => {
                            if (e.response.status === 422) {
                                this.errors = e.response.data.errors;
                            } else if ([401, 403].includes(e.response.status)) {
                                toastr.error('You are not authorized.');
                            } else {
                                toastr.error('Server error.');
                            }
                        })
                        .finally(() => this.loading = false);
                },
            },
            watch: {
                async currency() {
                    this.marketNotAvailable = false;

                    if (this.selectedCurrency) {
                        this.form = {...this.formDefaults};

                        const availableCurrency = this.findAvailableCurrency(this.selectedCurrency.id);

                        if (availableCurrency) {
                            this.form = {
                                ...this.formDefaults,
                                ...{
                                    deposit_min: availableCurrency.depositMin,
                                    deposit_block: availableCurrency.depositBlock,
                                    confirmations_min: availableCurrency.confirmationsMin,
                                    luft_deposit: availableCurrency.luftDeposit,
                                    luft_withdraw: availableCurrency.luftWithdraw,
                                    rate: availableCurrency.rate,
                                    custom_rate: availableCurrency.customRate,
                                    name: availableCurrency.name,
                                    full_name: availableCurrency.fullName,
                                    order: availableCurrency.order,
                                }
                            };
                        }

                        await this.getCurrencyDetails();

                        if (!this.selectedCurrency.market) {
                            ``
                            this.marketNotAvailable = true;
                        }
                    }
                },
            },
        });
    </script>
@endpush
