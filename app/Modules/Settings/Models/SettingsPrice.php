<?php
declare(strict_types=1);

namespace App\Modules\Settings\Models;

use App\Modules\Core\Models\Model;
use Carbon\Carbon;

/**
 * Settings price model.
 *
 * @property-read int $id
 * @property float $abc_price
 * @property float $referral_abc_commission
 * @property float $promo_code_bonus
 * @property Carbon|null $created_at
 * @property Carbon|null updated_at
 * @property float $referral_usdt_commission
 */
final class SettingsPrice extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'abc_price' => 'float',
        'referral_abc_commission' => 'float',
        'promo_code_bonus' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'referral_usdt_commission' => 'float',
    ];
}
