<?php
declare(strict_types=1);

namespace App\Modules\Users\Models;

use App\Modules\Languages\Models\Lang;
use App\Modules\Investments\Models\Trade;
use App\Modules\ProfitCards\Models\ProfitCardBuyHistory;
use App\Modules\Core\Traits\LockTrait;
use App\Modules\Investments\Models\TradeOption;
use App\Modules\Users\Dto\Api\UserShortDtoApi;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;

/**
 * User model.
 *
 * @property-read int $id
 * @property-read string $username
 * @property bool $not_counted
 * @property bool $tid_default
 * @property int $multi_accounts
 * @property Carbon|null $tids_allowable_at
 * @property-read Collection|UserCustomTidOptions[] $customTidOptions
 * @property-read bool $isTidsAllowable
 */
final class User extends \TCG\Voyager\Models\User
{
    use HasFactory, Notifiable, LockTrait;

    /** @var int admin user id for add to contact at register. */
    const ADMIN_USER_ID_FOR_CONTACTS = 1;
    const USERS_ALLOWED_SEND_MONEY = [1];

    const ROLE_ADMIN_NAME = 'admin';
    const ROLE_MODERATOR = 'moderator';
    const ROLE_USER_NAME = 'user';

    const TYPE_ADMIN = 1;
    const TYPE_NOT_ACTIVATED = 2;
    const TYPE_BLOCKED = 3;
    const TYPE_USER = 4;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const STATUS_USER = 1;
    const STATUS_INVESTOR = 2;
    const STATUS_PROMOTER = 3;

    const DEFAULT_AVATAR = 'users/default.png';

    const STATUSES = [
        self::STATUS_USER => 'User',
        self::STATUS_INVESTOR => 'Investor',
        self::STATUS_PROMOTER => 'Promoter'
    ];

    private static array $types = [
        self::TYPE_ADMIN => 'Admin',
        self::TYPE_NOT_ACTIVATED => 'Not Activated',
        self::TYPE_BLOCKED => 'Blocked',
        self::TYPE_USER => 'User'
    ];

    public const ALLOWED_PASSWORD_SYMBOLS = 'a-zA-Z0-9!=@#$%^&*,{}|!"№;:?\'*()\[\],.\/<>+~_-';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'username' => 'string',
        'email_verified_at' => 'datetime',
        'is_2fa' => 'boolean',
        'google2fa_secret' => 'encrypted',
        'enabled_force_withdraw' => 'boolean',
        'multi_accounts' => 'integer',
        'not_counted' => 'boolean',
        'tid_default' => 'boolean',
        'tids_allowable_at' => 'datetime',
    ];

    /**
     * {@inheritDoc}
     */
    public function getRouteKeyName(): string
    {
        return 'promo_code';
    }

    /**
     * {@inheritDoc}
     */
    public function save(array $options = [])
    {
        if ($this->role_id == 1) {
            $this->type = self::TYPE_ADMIN;
        } else if ($this->is_blocked) {
            $this->type = self::TYPE_BLOCKED;
        } else if (!$this->email_verified_at) {
            $this->type = self::TYPE_NOT_ACTIVATED;
        } else {
            $this->type = self::TYPE_USER;
        }

        return parent::save();
    }

    /**
     * Get type accessor
     *
     * @return mixed|string
     */
    public function getTypeBrowseAttribute()
    {
        return isset(self::$types[$this->type]) ? self::$types[$this->type] : $this->type;
    }

    /**
     * Get team leader user.
     *
     * @return BelongsTo
     */
    public function teamLead(): BelongsTo
    {
        return $this->belongsTo(User::class, 'team_member_id');
    }

    /**
     * Relation with User (teammates).
     *
     * @return HasMany
     */
    public function teammates(): HasMany
    {
        return $this->hasMany(User::class, 'team_member_id')
            ->orderBy('id', 'ASC');
    }

    /**
     * Get user trades.
     *
     * @return HasMany
     */
    public function trades(): HasMany
    {
        return $this->hasMany(Trade::class, 'users_id');
    }

    /**
     * Get relation with language.
     *
     * @return BelongsTo
     */
    public function lang(): BelongsTo
    {
        return $this->belongsTo(Lang::class, 'langs_id');
    }

    /**
     * Get relation with TradeOption.
     *
     * @return BelongsToMany
     */
    public function allowedOptions(): BelongsToMany
    {
        return $this->belongsToMany(TradeOption::class, 'users_trade_options_rights', 'users_id', 'trade_options_id');
    }

    /**
     * Check that option allowed.
     *
     * @param TradeOption $tradeOption
     * @return bool
     */
    public function checkAllowedOption(TradeOption $tradeOption): bool
    {
        return $this->allowedOptions->where('id', $tradeOption->id)->count() > 0;
    }

    /**
     * Relation with UserTidExpanse.
     *
     * @return HasMany
     */
    public function tidsExpanses(): HasMany
    {
        return $this->hasMany(UserTidExpanse::class, 'users_id');
    }

    /**
     * Relation with {@see UserCustomTidOptions::class}.
     *
     * @return HasMany
     */
    public function customTidOptions(): HasMany
    {
        return $this->hasMany(UserCustomTidOptions::class, 'users_id');
    }

    /**
     * Profit cards history.
     *
     * @return HasMany
     */
    public function profitCards(): HasMany
    {
        return $this->hasMany(ProfitCardBuyHistory::class, 'users_id');
    }

    /**
     * Get UserTidExpanse by TradeOption.
     *
     * @param TradeOption $tradeOption
     * @return UserTidExpanse|null
     */
    public function getTidExpanseByOption(TradeOption $tradeOption): ?UserTidExpanse
    {
        return $this->tidsExpanses
            ->where('trade_options_id', $tradeOption->id)
            ->first();
    }

    /**
     * Get {@see UserCustomTidOptions::class} by {@see TradeOption::class}.
     *
     * @param TradeOption $tradeOption
     * @return UserCustomTidOptions|null
     */
    public function getCustomTidOptionsByOption(TradeOption $tradeOption): ?UserCustomTidOptions
    {
        return $this->customTidOptions
            ->where('trade_options_id', $tradeOption->id)
            ->first();
    }

    /**
     * Get all balances data.
     *
     * @return array
     */
    public function balances(): array
    {
        return [
            'balance_usdt' => $this->balance_usdt,
            'deposited_usdt' => $this->deposited_usdt,
            'dividents_usdt' => $this->dividents_usdt,
            'balance_abc' => $this->balance_abc, //available abc balance
            'traded_abc_balance' => $this->traded_abc_balance,
            'frozen_abc_balance' => $this->frozen_abc_balance,
            'balance_total_abc' => $this->balance_total_abc,
            'unrealized_pnl' => $this->unrealized_pnl,
            'floating_pnl' => $this->floating_pnl,
            'bot_total_profit' => $this->bot_total_profit,
            'total_invested_balance' => $this->total_invested_balance,
            'total_withdraw' => $this->total_withdraw,
            'total_deposited' => $this->total_deposited,
            'total_received_abc' => $this->total_received_abc,
        ];
    }

    /**
     * Get relation with User.
     *
     * @return HasMany
     */
    public function messagesContacts(): HasMany
    {
        return $this->hasMany(\App\Modules\Messages\Models\Contact::class, 'users_id');
    }

    /**
     * Get attribute "isTidsAllowable".
     *
     * @return bool
     */
    public function getIsTidsAllowableAttribute(): bool
    {
        return $this->tids_allowable_at &&
            $this->tids_allowable_at > now();
    }

    /**
     * Get readable user status attribute.
     *
     * @return string|null
     */
    public function getReadableUserStatusAttribute(): ?string
    {
        return self::STATUSES[$this->user_status] ?? null;
    }

    public function toApiDto(): UserShortDtoApi
    {
        return new UserShortDtoApi(
            $this->id,
            $this->username,
        );
    }
}
