<?php


namespace App\Modules\Logs\Repositories;

use App\Modules\Logs\Models\OperationLog;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Task repository.
 */
final class OperationLogRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Logs\Models\OperationLog $model
     */
    public function __construct(protected OperationLog $model)
    {
    }
}
