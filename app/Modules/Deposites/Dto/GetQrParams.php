<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Dto;

use App\Modules\Core\Dto\BaseDto;

/**
 * DTO for get qr code.
 */
final class GetQrParams extends BaseDto
{
    /**
     * Constructor
     *
     * @param int $width
     * @param int $height
     * @param string $address
     * @param float $amount
     */
    public function __construct(
        protected int $width,
        protected int $height,
        protected string $address,
        protected float $amount,
    )
    {
    }

    /**
     * Get width.
     *
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * Get height.
     *
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * Get address.
     *
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * Get amount.
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}
