<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds title_when_it_end field
 * to settings_timer table.
 */
final class AddTimerTitleWhenItEndFieldToSettingsTimer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_timer', function (Blueprint $table) {
            $table->string('title_when_it_end', 255)->after('title')->nullable(true);
        });

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 35,
                'field' => 'title_when_it_end',
                'type' => 'text',
                'display_name' => 'Timer title when it end',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|scalar|max:255"}}',
                'order' => 6
            ]);

        DB::table('data_rows')
            ->where([
                'data_type_id' => 35,
                'field' => 'description'
            ])
            ->update([
                'type' => 'text_area'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_timer', function (Blueprint $table) {
            $table->dropColumn('title_when_it_end');
        });

        DB::table('data_rows')
            ->where([
                'data_type_id' => 35,
                'field' => 'title_when_it_end'
            ])
            ->delete();
    }
}
