<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds field eth_from
 * to crypto_transactions table.
 */
final class AddFieldEthFromToCryptoTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('crypto_transactions', function (Blueprint $table) {
            $table->string('eth_from', 100)
                ->nullable(true)
                ->after('txid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('crypto_transactions', function (Blueprint $table) {
            $table->dropColumn('eth_from');
        });
    }
}
