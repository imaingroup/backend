<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds enabled_force_withdraw field to users table.
 */
final class AddFieldEnabledForceWithdrawToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('enabled_force_withdraw')->default(false);
        });

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 1,
                'field' => 'enabled_force_withdraw',
                'type' => 'checkbox',
                'display_name' => 'Enabled Force Withdraw',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{}',
                'order' => 64
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('enabled_force_withdraw');
        });
    }
}
