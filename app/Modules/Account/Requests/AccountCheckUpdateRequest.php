<?php

namespace App\Modules\Account\Requests;

use App\Modules\Core\Services\DataService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This request class need for validating data from register form.
 */
final class AccountCheckUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['m' => "string", 'version' => "string"])]
    public function rules(): array
    {
        return [
            'm' => 'required|integer',
            'version' => 'required|integer',
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape(['rules' => "array", 'version' => "int"])]
    public function validated(): array
    {
        $data = parent::validated();
        $list = [];
        $value = (int)$data['m'];

        $maskList = array_values(DataService::TYPES_DIGITS);
        $rules = array_flip(DataService::TYPES_DIGITS);

        foreach ($maskList as $mask) {
            if(($value & $mask) === $mask) {
                $list[] = $rules[$mask];
            }
        }

        return [
            'rules' => $list,
            'version' => (int)$this['version'],
        ];
    }
}
