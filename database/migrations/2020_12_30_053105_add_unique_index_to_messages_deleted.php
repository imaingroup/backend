<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration adds unique index
 * for messages_deleted table.
 */
final class AddUniqueIndexToMessagesDeleted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('create unique index messages_deleted_messages_id_users_id_uindex on messages_deleted (messages_id, users_id)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop index messages_deleted_messages_id_users_id_uindex on messages_deleted');
    }
}
