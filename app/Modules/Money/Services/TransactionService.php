<?php


namespace App\Modules\Money\Services;


use App\Modules\Core\Models\Task;
use App\Modules\Money\Models\Transaction;
use App\Modules\Users\Models\User;

/**
 * Transaction service.
 */
final class TransactionService
{
    /**
     * Create transaction.
     *
     * @param User $user
     * @param User $initiator
     * @param int $type
     * @param float $amount
     * @param float $balanceAfter
     * @param string $comment
     * @param array $data
     * @param int $currency
     * @param \DateTime $dt
     * @param int $operationType
     * @param \App\Modules\Money\Models\Transaction|null $relatedTransaction
     * @param \App\Modules\Core\Models\Task|null $task
     * @return Transaction
     */
    public function create(
        User $user,
        User $initiator,
        int $type,
        float $amount,
        float $balanceAfter,
        string $comment,
        array $data,
        int $currency,
        \DateTime $dt,
        int $operationType,
        Transaction $relatedTransaction = null,
        Task $task = null
    )
    {
        $transaction = new Transaction();
        $transaction->users_id = $user->id;
        $transaction->initiator_users_id = $initiator->id;
        $transaction->type = $type;
        $transaction->amount = $amount;
        $transaction->balance_after = $balanceAfter;
        $transaction->comment = $comment;
        $transaction->data = \json_encode($data);
        $transaction->currency = $currency;
        $transaction->transaction_at = $dt;
        $transaction->operation_type = $operationType;
        $transaction->related_transaction_id = $relatedTransaction ? $relatedTransaction->id : null;
        $transaction->tasks_id = $task->id ?? null;
        $transaction->save();

        return $transaction;
    }
}
