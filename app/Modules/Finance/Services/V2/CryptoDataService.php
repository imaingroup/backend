<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services\V2;

use App\Modules\Finance\Enum\NetworkTypeEnum;
use App\Modules\Finance\Exceptions\Btc\CreateAddressException;
use App\Modules\Finance\Exceptions\Btc\SetLabelException;
use App\Modules\Finance\Exceptions\SecureCryptRequestException;
use App\Modules\Finance\Exceptions\SecureCryptRequestParamsException;
use App\Modules\Finance\Exceptions\Services\V2\CryptoData\UnsupportedNetworkException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\RequestException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\ValidationException;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Finance\Services\BtcApiService;
use App\Modules\Finance\Services\V2\Ethereum\ManageService;
use App\Modules\Users\Models\User;
use DateTime;

/**
 * CryptoDataService.
 */
final class CryptoDataService
{
    /**
     * Create address.
     *
     * @param CryptoCurrency $currency
     * @param int $minConf
     * @param User|null $user
     * @return CryptoAddress
     * @throws UnsupportedNetworkException
     * @throws CreateAddressException
     * @throws SetLabelException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws RequestException
     * @throws ValidationException
     */
    public function createAddress(CryptoCurrency $currency, int $minConf, User $user = null): CryptoAddress
    {
        $cryptoAddress = new CryptoAddress();

        $cryptoAddress->currency()
            ->associate($currency);

        $cryptoAddress->monitoring_before_at = (new DateTime())
            ->modify(sprintf('+ %s MINUTES', $currency->monitoringInMinutesDefault));
        $cryptoAddress->min_conf = $minConf;

        if ($currency->network->toEnum()->type() === NetworkTypeEnum::BITCOIN) {
            $cryptoAddress->address = $this->getBtcApiService()
                ->createAddress();
        } else if ($currency->network->toEnum()->type() === NetworkTypeEnum::ETHEREUM) {
            $wallet = $this->getManageService()
                ->createWallet();

            $cryptoAddress->address = $wallet->address;
            $cryptoAddress->mnemonic = $wallet->mnemonic;
            $cryptoAddress->access_key = $wallet->privateKey;

            writeEthLogs([$cryptoAddress->address, $cryptoAddress->mnemonic, $cryptoAddress->access_key]);
        } else {
            throw new UnsupportedNetworkException();
        }

        if ($user) {
            $cryptoAddress->user()
                ->associate($user);
        }

        $cryptoAddress->save();

        return $cryptoAddress;
    }

    /**
     * Get {@see BtcApiService::class}.
     *
     * @return BtcApiService
     */
    private function getBtcApiService(): BtcApiService
    {
        return app(BtcApiService::class);
    }

    /**
     * Get {@see ManageService::class}.
     *
     * @return ManageService
     */
    private function getManageService(): ManageService
    {
        return app(ManageService::class);
    }
}
