<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldDataToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->tinyInteger('currency');
            $table->longText('data');
            $table->dropColumn('context');
            $table->unsignedBigInteger('initiator_users_id');
            $table->float('balance_after', 16, 8);

            $table->foreign('initiator_users_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('currency');
            $table->dropColumn('data');
            $table->string('context', 255);
            $table->dropColumn('initiator_users_id');
            $table->dropColumn('balance_after');
        });
    }
}
