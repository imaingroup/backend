<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates settings_team table.
 */
final class CreateSettingsTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_team', function (Blueprint $table) {
            $table->id();
            $table->text('activate_usdt_commission_link')->nullable(true);
            $table->timestamps();
        });

        DB::table('settings_team')
            ->insert([
                'activate_usdt_commission_link' => null
            ]);

        $id = DB::table('data_types')
            ->insertGetId([
                'name' => 'settings_team',
                'slug' => 'settings-team',
                'display_name_singular' => 'TEAM',
                'display_name_plural' => 'TEAM',
                'icon' => 'voyager-settings',
                'model_name' => 'App\Modules\Settings\Models\SettingsTeam',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);

        DB::table('data_rows')
            ->insert([
                'data_type_id' => $id,
                'field' => 'activate_usdt_commission_link',
                'type' => 'text',
                'display_name' => 'Activate USDT commission link',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '{"validation":{"rule":"nullable|scalar|max:10000"}}',
                'order' => 0
            ]);

        $permissions = ['browse', 'read', 'edit', 'add', 'delete'];

        foreach ($permissions as $permission) {
            $pId = DB::table('permissions')
                ->insertGetId([
                    'key' => sprintf('%s_settings_team', $permission),
                    'table_name' => 'settings_team',
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()
                ]);

            if ($permission === 'edit') {
                DB::table('permission_role')
                    ->insert([
                        'permission_id' => $pId,
                        'role_id' => 1
                    ]);
            }
        }

        DB::table('menu_items')
            ->insert([
                'menu_id' => 1,
                'title' => 'TEAM',
                'url' => '/XI400PGNB/settings-team/1/edit',
                'target' => '_self',
                'icon_class' => 'voyager-settings',
                'color' => '#000000',
                'parent_id' => 22,
                'order' => 7,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_team');

        DB::table('data_types')
            ->where('name', 'settings_team')
            ->delete();

        DB::table('permissions')
            ->where('table_name', 'settings_team')
            ->delete();

        DB::table('menu_items')
            ->where('title', 'TEAM')
            ->where('url', '/XI400PGNB/settings-team/1/edit')
            ->delete();
    }
}
