<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services\V2\Ethereum;

use App\Modules\Finance\Dto\Api\Ethereum\Manage\BalanceDto;
use App\Modules\Finance\Dto\Api\Ethereum\Manage\EstimatedGasDto;
use App\Modules\Finance\Dto\Api\Ethereum\Manage\SentDto;
use App\Modules\Finance\Dto\Api\Ethereum\Manage\WalletDto;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\RequestException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\ValidationException;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Illuminate\Support\Facades\Validator;
use stdClass;
use Throwable;

/**
 * ManageService. It can create wallets
 * and transfer money and tokens in all supported networks with ethereum type.
 */
final class ManageService
{
    private const CONNECT_TIMEOUT = 1;
    private const READ_TIMEOUT = 300;

    /**
     * Create wallet.
     *
     * @return WalletDto
     * @throws RequestException
     * @throws ValidationException
     */
    public function createWallet(): WalletDto
    {
        $data = $this->request('POST', '/wallets/create');

        if (Validator::make(
            (array)$data->data, [
                'address' => 'required|string|ethereum',
                'mnemonic' => 'required|string|count_words:12',
                'privateKey' => 'required|string'
            ]
        )->fails()) {
            throw new ValidationException();
        }

        return new WalletDto(
            $data->data->address,
            $data->data->mnemonic,
            $data->data->privateKey,
        );
    }

    /**
     * Migrate wallet.
     *
     * @param string $wallet
     * @param string $password
     * @return WalletDto
     * @throws RequestException
     * @throws ValidationException
     */
    public function migrateWallet(string $wallet, string $password): WalletDto
    {
        $data = $this->request('POST', '/wallets/migrate', [
            'wallet' => $wallet,
            'password' => $password,
        ]);

        if (Validator::make(
            (array)$data->data, [
                'address' => 'required|string|ethereum',
                'privateKey' => 'required|string'
            ]
        )->fails()) {
            throw new ValidationException();
        }

        return new WalletDto(
            $data->data->address,
            null,
            $data->data->privateKey,
        );
    }

    /**
     * Get balance of main network currency.
     *
     * @param EthereumNetworkEnum $network
     * @param string $address
     * @return BalanceDto
     * @throws RequestException
     * @throws ValidationException
     */
    public function getBalance(EthereumNetworkEnum $network, string $address): BalanceDto
    {
        $data = $this->request('GET', sprintf('/address/balance/%s/%s', $network->value, $address));

        if (Validator::make(
            (array)$data, [
                'status' => 'required|accepted',
                'balance' => 'required|numeric|gte:0',
            ]
        )->fails()) {
            throw new ValidationException();
        }

        return new BalanceDto(
            $network,
            $address,
            $data->balance,
            null
        );
    }

    /**
     * Get balance of token.
     *
     * @param EthereumNetworkEnum $network
     * @param string $token
     * @param string $address
     * @return BalanceDto
     * @throws RequestException
     * @throws ValidationException
     */
    public function getTokenBalance(EthereumNetworkEnum $network, string $token, string $address): BalanceDto
    {
        $data = $this->request('GET', sprintf('/token/balance/%s/%s/%s', $network->value, $token, $address));

        if (Validator::make(
            (array)$data, [
                'status' => 'required|accepted',
                'balance' => 'required||numeric|gte:0',
            ]
        )->fails()) {
            throw new ValidationException();
        }

        return new BalanceDto(
            $network,
            $address,
            $data->balance,
            $token
        );
    }

    /**
     * Estimate gas for main currency in network.
     *
     * @param EthereumNetworkEnum $network
     * @param string $from
     * @param string $to
     * @param string $amount
     * @return EstimatedGasDto
     * @throws RequestException
     * @throws ValidationException
     */
    public function estimate(EthereumNetworkEnum $network, string $from, string $to, string $amount): EstimatedGasDto
    {
        $data = $this->request('POST', '/estimate', [
            'network' => $network->value,
            'from' => $from,
            'to' => $to,
            'amount' => $amount,
        ]);

        if (Validator::make(
            (array)$data, [
                'status' => 'required|accepted',
                'gas' => 'required|numeric',
            ]
        )->fails()) {
            throw new ValidationException();
        }

        return new EstimatedGasDto(
            $network,
            null,
            $from,
            $to,
            $amount,
            (float)$data->gas,
        );
    }

    /**
     * Estimate gas for erc20 transfer.
     *
     * @param EthereumNetworkEnum $network
     * @param string $contract
     * @param string $to
     * @param float $amount
     * @param int $decimals
     * @param string $privateKey
     * @return EstimatedGasDto
     * @throws RequestException
     * @throws ValidationException
     */
    public function estimateErc20(EthereumNetworkEnum $network, string $contract, string $to, string $amount, int $decimals, string $privateKey): EstimatedGasDto
    {
        $data = $this->request('POST', '/estimate/erc20', [
            'network' => $network->value,
            'contract' => $contract,
            'to' => $to,
            'amount' => $amount,
            'decimals' => $decimals,
            'privateKey' => $privateKey,
        ]);

        if (Validator::make(
            (array)$data, [
                'status' => 'required|accepted',
                'gas' => 'required|numeric',
            ]
        )->fails()) {
            throw new ValidationException();
        }

        return new EstimatedGasDto(
            $network,
            $contract,
            null,
            $to,
            $amount,
            (float)$data->gas,
        );
    }

    /**
     * Send main currency in network.
     *
     * @param EthereumNetworkEnum $network
     * @param string $from
     * @param string $to
     * @param string $amount
     * @param string $privateKey
     * @return SentDto
     * @throws RequestException
     * @throws ValidationException
     */
    public function send(EthereumNetworkEnum $network, string $from, string $to, string $amount, string $privateKey): SentDto
    {
        $data = $this->request('POST', '/send', [
            'network' => $network->value,
            'from' => $from,
            'to' => $to,
            'amount' => $amount,
            'privateKey' => $privateKey,
        ]);

        if (Validator::make(
            (array)$data, [
                'status' => 'required|accepted',
                'txid' => 'required|crypto_tx',
                'amount' => 'required|numeric|gt:0',
            ]
        )->fails()) {
            throw new ValidationException();
        }

        return new SentDto(
            $network,
            null,
            $from,
            $to,
            $amount,
            $data->amount,
            $data->txid,
        );
    }

    /**
     * Send tokens in network.
     *
     * @param EthereumNetworkEnum $network
     * @param string $contract
     * @param string $to
     * @param string $amount
     * @param int $decimals
     * @param string $privateKey
     * @return SentDto
     * @throws RequestException
     * @throws ValidationException
     */
    public function sendTokens(EthereumNetworkEnum $network, string $contract, string $to, string $amount, int $decimals, string $privateKey): SentDto
    {
        $data = $this->request('POST', '/send/tokens', [
            'network' => $network->value,
            'contract' => $contract,
            'to' => $to,
            'amount' => $amount,
            'decimals' => $decimals,
            'privateKey' => $privateKey,
        ]);

        if (Validator::make(
            (array)$data, [
                'status' => 'required|accepted',
                'txid' => 'required|crypto_tx',
                'amount' => 'required|numeric|gt:0',
            ]
        )->fails()) {
            throw new ValidationException();
        }

        return new SentDto(
            $network,
            $contract,
            null,
            $to,
            $amount,
            $data->amount,
            $data->txid,
        );
    }

    /**
     * Execute request.
     *
     * @param string $method
     * @param string $path
     * @param array $query
     * @return stdClass|array
     * @throws RequestException
     */
    private function request(string $method, string $path, array $query = []): StdClass|array
    {
        $log = [];

        $log['code'] = null;

        try {
            $client = new Client();

            $body = [
                'allow_redirects' => false,
                'connect_timeout' => self::CONNECT_TIMEOUT,
                'read_timeout' => self::READ_TIMEOUT,
                'on_stats' => function (TransferStats $stats) use (&$log) {
                    $log['stats'] = $stats->getHandlerStats();

                    if ($stats->hasResponse()) {
                        $log['code'] = $stats->getResponse()->getStatusCode();
                        $log['response']['headers'] = $stats->getResponse()->getHeaders();
                        $log['response']['content'] = json_decode($stats->getResponse()->getBody()->__toString(), true);
                    }
                }
            ];

            if (strtoupper($method) === 'GET') {
                $body['query'] = $query;
            } else {
                $body['json'] = $query;
            }

            $body['headers'] = [
                'Content-Type' => 'application/json',
            ];

            $log['request'] = $body;
            unset($log['request']['on_stats']);

            $request = $client->request($method, sprintf('%s%s', config('finance.ethereum.endpoint'), $path), $body);

            if ($request->getStatusCode() == 200) {
                return json_decode($request->getBody()->__toString());
            } else {
                throw new RequestException('Status code not equal 200');
            }
        } catch (Throwable $e) {
            throw new RequestException($e->getMessage(), 0, $e);
        }
    }
}
