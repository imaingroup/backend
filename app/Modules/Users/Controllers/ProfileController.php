<?php

namespace App\Modules\Users\Controllers;

use App\Modules\Users\Exceptions\ProfileException;
use App\Http\Controllers\Controller;
use App\Modules\Users\Models\User;
use App\Modules\Users\Requests\ProfileAccountSaveRequest;
use App\Modules\Users\Requests\ProfileContentRequest;
use App\Modules\Users\Requests\ProfileDisable2FaRequest;
use App\Modules\Users\Requests\ProfileEnable2FaRequest;
use App\Modules\Users\Requests\ProfileNotificationsSaveRequest;
use App\Modules\Users\Requests\ProfilePasswordChangeRequest;
use App\Modules\Users\Requests\ProfileSet2FaRequest;
use App\Modules\Users\Services\ProfileService;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException;
use PragmaRX\Google2FA\Exceptions\InvalidCharactersException;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;

/**
 * Profile controller.
 */
final class ProfileController extends Controller
{
    /** @var \App\Modules\Users\Services\ProfileService Profile service. */
    private ProfileService $profileService;

    /**
     * Constructor.
     *
     * @param ProfileService $profileService
     */
    public function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }

    /**
     * Get profile content.
     *
     * @param \App\Modules\Users\Requests\ProfileContentRequest $request
     * @return array
     */
    public function content(ProfileContentRequest $request): array
    {
        return [
            'data' => [
                $this->profileService->content(Auth::user())
            ]
        ];
    }

    /**
     * Save account data.
     *
     * @param ProfileAccountSaveRequest $request
     * @return bool[]
     * @throws Exception
     */
    public function accountSave(ProfileAccountSaveRequest $request): Response
    {
        return $this->inTransaction(function() use($request): void {
            $user = Auth::user();

            User::refreshAndLockForUpdate($user);

            $this->profileService->save($user, $request->validated());
        });
    }

    /**
     * Change password.
     *
     * @param ProfilePasswordChangeRequest $request
     * @return array
     */
    public function changePassword(ProfilePasswordChangeRequest $request): array
    {
        $this->profileService->changePassword(Auth::user(), $request->validated()['password']);

        return ['status' => true];
    }

    /**
     * Save notifications.
     *
     * @param \App\Modules\Users\Requests\ProfileNotificationsSaveRequest $request
     * @return array
     */
    public function saveNotifications(ProfileNotificationsSaveRequest $request): array
    {
        $this->profileService->saveNotifications(Auth::user(), $request->validated());

        return ['status' => true];
    }

    /**
     * Enable 2fa.
     *
     * @param \App\Modules\Users\Requests\ProfileEnable2FaRequest $request
     * @return array
     * @throws ProfileException
     * @throws IncompatibleWithGoogleAuthenticatorException
     * @throws InvalidCharactersException
     * @throws SecretKeyTooShortException
     */
    public function enable2fa(ProfileEnable2FaRequest $request): array
    {
        $data = $this->profileService->enable2fa(Auth::user());

        $request->session()->put('google2fa_secret', $data['secret']);

        return [
            'data' => $data
        ];
    }

    /**
     * Set 2fa.
     *
     * @param ProfileSet2FaRequest $request
     * @return array
     */
    public function set2fa(ProfileSet2FaRequest $request): array
    {
        $this->profileService->set2fa(Auth::user(), $request->getSession()->get('google2fa_secret'));

        return ['status' => true];
    }

    /**
     * Disable 2fa.
     *
     * @param ProfileDisable2FaRequest $request
     * @return array
     */
    public function disable2fa(ProfileDisable2FaRequest $request): array
    {
        $this->profileService->disable2fa(Auth::user());

        return ['status' => true];
    }
}
