<?php


namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsDeposit;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Settings abc repository.
 */
final class SettingsDepositRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param SettingsDeposit $model
     */
    public function __construct(protected SettingsDeposit $model)
    {}

    /**
     * Get settings model.
     *
     * @return SettingsDeposit
     */
    public function get(): SettingsDeposit
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
