<?php
declare(strict_types=1);

namespace App\Modules\Security\Commands;

use App\Modules\Security\Services\Identification\UpdaterService;
use Illuminate\Console\Command;
use Throwable;

/**
 * This command need for monitoring transactions.
 */
final class DetectMultiAccountsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'security:multi_accounts:detect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command need for update multiaccounts data';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws Throwable
     */
    public function handle(): int
    {
       $this->getUpdaterService()
            ->updateMultiAccounts();

        return 0;
    }

    /**
     * Get {@see UpdaterService::class}.
     *
     * @return UpdaterService
     */
    private function getUpdaterService(): UpdaterService
    {
        return app(UpdaterService::class);
    }
}
