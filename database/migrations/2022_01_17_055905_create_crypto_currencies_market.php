<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * CreateCryptoCurrenciesMarket.
 */
final class CreateCryptoCurrenciesMarket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('crypto_currencies_market', function (Blueprint $table) {
            $table->primary('crypto_currencies_id');
            $table->unsignedBigInteger('crypto_currencies_id');
            $table->foreign('crypto_currencies_id', 'crypto_currencies_market_crypto_currencies_id_foreign')
                ->references('id')
                ->on('crypto_currencies')
                ->cascadeOnDelete();
            $table->string('image')
                ->nullable(true);
            $table->unsignedDouble('price');
            $table->dateTime('price_updated_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('crypto_currencies_market');
    }
}
