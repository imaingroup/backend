<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\TidHistory;

use Illuminate\Foundation\Http\FormRequest;

/**
 * This request class validate data for delete feature history request.
 */
final class AdminProfitHistoryDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
