<?php
declare(strict_types=1);

namespace App\Modules\Notifications\Mails;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

/**
 * This class sends emails.
 */
final class MailSender
{
    /**
     * Send email.
     *
     * @param string $to
     * @param Mailable $mailable
     */
    public static function send(string $to, Mailable $mailable): void
    {
        $blockedMails = config('mail.blocked_mails');

        if (in_array($to, $blockedMails)) {
            return;
        }

        Mail::to($to)->send($mailable);
    }
}
