<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Change crypto tables.
 */
final class ChangeCryptoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::drop('settings_bitcoin');
        Schema::drop('bitcoin_logs');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::create('settings_bitcoin', function (Blueprint $table) {
            $table->id();
            $table->string('uri_api', 255);
            $table->string('username', 255);
            $table->string('password', 255);
            $table->tinyInteger('minconf');
            $table->smallInteger('monitoring_transactions_minutes')->default('120');
            $table->dateTime('token_expiration_at')->nullable(true);
            $table->text('token')->nullable(true);
            $table->timestamps();
            $table->float('min_commission', 5)->default(0);
            $table->float('max_commission', 5)->default(0);
            $table->float('reserve', 5, 2)->default(0);
            $table->boolean('locked_withdraw')->default(false);
            $table->dateTime('locked_withdraw_at')->nullable(true);
        });

        Schema::create('bitcoin_logs', function (Blueprint $table) {
            $table->id();
            $table->string('uri', 255);
            $table->string('method', 20);
            $table->smallInteger('code')->nullable(true);
            $table->longText('log');
            $table->unsignedBigInteger('users_id')->nullable(true);
            $table->timestamps();

            $table->foreign('users_id')
                ->references('id')->on('users');
        });

        DB::table('settings_bitcoin')
            ->insert([
                'uri_api' => 'http://104.219.250.132:46789',
                'username' => 'admin',
                'password' => 'G5Tjhq789',
                'minconf' => 2,
                'monitoring_transactions_minutes' => 1440,
                'min_commission' => 10,
                'max_commission' => 20,
                'reserve' => 1,
            ]);
    }
}
