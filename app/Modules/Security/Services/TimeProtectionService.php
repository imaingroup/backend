<?php
declare(strict_types=1);

namespace App\Modules\Security\Services;

use App\Modules\Core\Libraries\NtpClient\UdpNtpClient;
use App\Modules\Core\Models\Time;
use App\Modules\Core\Repositories\TimeRepository;
use App\Modules\Logs\Loggers\Logger;
use DateTime;
use Throwable;

/**
 * TimeProtectionService.
 */
final class TimeProtectionService
{
    private const FORMAT = 'Y-m-d';
    private const MAX_ERRORS = 10;

    /**
     * Check time.
     */
    public function checkTime(): bool
    {
        $times = $this->getTimeRepository()
            ->allItems();

        if ($times->count() !== count(Time::SERVERS)) {
            return false;
        }

        $serverAt = new DateTime();
        $dates = collect([$serverAt->format(self::FORMAT)]);
        $errors = 0;

        $times->each(function (Time $time) use (&$errors, $dates) {
            $errors += $time->errors;
            $dates[] = $time->updated_at->format(self::FORMAT);

            if ($time->errors > 0) {
                $dates[] = $time->server_at->format(self::FORMAT);
            }
        });

        return $dates->unique()->count() === 1 &&
            $errors < self::MAX_ERRORS;
    }

    /**
     * Update time label.
     */
    public function updateTime(): void
    {
        foreach (Time::SERVERS as $server) {
            $timestamp = $this->getNtpTime($server);

            if ($timestamp > 0) {
                $this->getTimeRepository()
                    ->updateOrCreate(
                        ['server' => $server],
                        [
                            'server' => $server,
                            'errors' => 0,
                            'server_at' => (new DateTime())
                                ->setTimestamp($timestamp),
                        ]
                    );
            } else {
                $this->getTimeRepository()
                    ->updateTimeErrors($server, ($timestamp > 0));
            }
        }
    }

    /**
     * Get ntp time.
     *
     * @param string $server
     * @return int
     */
    private function getNtpTime(string $server): int
    {
        try {
            return (new UdpNtpClient($server, 123))
                ->getUnixTime();
        } catch (Throwable $e) {
            $this->getLogger()
                ->registerException($e);

            return -1;
        }
    }

    /**
     * Получить TimeRepository.
     *
     * @return TimeRepository
     */
    private function getTimeRepository(): TimeRepository
    {
        return app(TimeRepository::class);
    }

    /**
     * Получить Logger.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
