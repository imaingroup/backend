<?php
declare(strict_types=1);

namespace App\Modules\Finance\Enum;

use App\Modules\Finance\Contracts\Enum\NetworkEnumContract;

/**
 * EthereumNetworkEnum.
 */
enum EthereumNetworkEnum: string implements NetworkEnumContract
{
    case ETHEREUM = 'ethereum';
    case ETH_CLASSIC = 'ethclassic';
    case BSC = 'bsc';
    case VELAS = 'velas';
    case MATIC = 'matic';

    /**
     * Get network type.
     *
     * @return NetworkTypeEnum
     */
    public function type(): NetworkTypeEnum
    {
        return NetworkTypeEnum::ETHEREUM;
    }
}
