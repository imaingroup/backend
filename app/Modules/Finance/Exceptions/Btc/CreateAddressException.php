<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * CreateAddressException.
 */
final class CreateAddressException extends BtcException
{
}
