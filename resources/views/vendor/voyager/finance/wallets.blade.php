@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Manage wallets')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-folder"></i> Manage wallets
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid" id="ethereum-control">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body" v-if="loaded">
                        <div class="select-wallet form-group col-md-{{ $display_options->width ?? 12 }}">
                            <a class="btc" v-if="btcWallet" v-bind:class="{active: btcWallet.network === currentWallet}"
                               @click.prevent="currentWallet = btcWallet.network">
                                BTC: @{{ (btcWallet.balance * btcWallet.token.market.usdtPrice).toFixed(2) }}$</a>
                            <a class="eth" v-if="gethWallet"
                               v-bind:class="{active: gethWallet.network === currentWallet}"
                               @click.prevent="currentWallet = gethWallet.network">
                                GETH: @{{ gethWallet.balance.toFixed(2) }}$
                            </a>
                        </div>

                        <div v-if="btcWallet && btcWallet.network === currentWallet">
                            <form @submit.prevent="sendBtcMoney">
                                <div class="form-group">
                                    <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                        <div><h3>BTC wallet</h3></div>
                                        <div class="parameters">
                                            <div class="list">
                                                <span class="opt">Balance coins</span>
                                                <span class="value">@{{ btcWallet.balance }}</span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Balance usdt</span>
                                                <span class="value">@{{ (btcWallet.balance * btcWallet.token.market.usdtPrice).toFixed(2) }}</span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Rate</span>
                                                <span class="value">@{{ btcWallet.token.market.usdtPrice }}</span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Rate updated</span>
                                                <span class="value">@{{ dateFormat(btcWallet.token.market.priceUpdatedAt) }}</span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Calculate</span>
                                                <span class="value">
                                                    <input type="number" value="" placeholder="Amount in usdt"
                                                           class="form-control" v-model="calculateBtc"/>
                                                </span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Calculated result</span>
                                                <span class="value">@{{ calculatedBtc }}</span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                        <div><h4>Withdraw money</h4></div>
                                        <div class="form-group">
                                            <input type="text" placeholder="BTC address"
                                                   class="form-control" v-model="formBtc.to"/>
                                            <div v-if="errors.to" v-for="error in errors.to"
                                                 class="text-error">@{{ error }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="number" step="0.00000001"
                                                   placeholder="Amount in coins"
                                                   class="form-control" min="0.00000001" v-model="formBtc.amount"/>
                                            <div>
                                                Amount in USDT: @{{ sendBtcAmountUsdt }}
                                            </div>
                                            <div v-if="errors.amount" v-for="error in errors.amount"
                                                 class="text-error">@{{ error }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="text" placeholder="2FA"
                                                   class="form-control" v-model="formBtc.g2fa"/>
                                            <div v-if="errors.g2fa" v-for="error in errors.g2fa"
                                                 class="text-error">@{{ error }}
                                            </div>
                                        </div>
                                        <div class="form-group" v-if="errors.global">
                                            <div v-for="error in errors.global"
                                                 class="text-error">@{{ error }}
                                            </div>
                                        </div>

                                        <input type="submit" style="margin-top: 15px"
                                               value="Send money" class="form-control"/>
                                    </div>
                                </div>
                            </form>

                            <div class="form-group col-md-{{ $display_options->width ?? 12 }}">
                                <div>
                                    <h4>History</h4>
                                </div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Users id</th>
                                        <th>Status</th>
                                        <th>To</th>
                                        <th>Amount</th>
                                        <th>Txid</th>
                                        <th>Created at</th>
                                        <th>Updated at</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="task in btcWallet.history">
                                        <td>@{{ task.id }}</td>
                                        <td>@{{ task.users_id }}</td>
                                        <td v-bind:style="{color: getColorByStatus(task.status)}">
                                            @{{ task.readableStatus}}
                                        </td>
                                        <td>@{{ task.data.to }}</td>
                                        <td>@{{ task.data.amount }}</td>
                                        <td>
                                            <a v-bind:href="'https://www.blockchain.com/en/btc/tx/'+task.post_data.txid"
                                               target="_blank" v-if="task.post_data && task.post_data.txid">view</a>
                                            <span v-else>N/A</span>
                                        </td>
                                        <td>@{{ dateFormat(task.createdAt) }}</td>
                                        <td>@{{ dateFormat(task.updatedAt) }}</td>
                                    </tr>
                                    <tr v-if="btcWallet.history.length === 0">
                                        <td colspan="8">No data.</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div v-if="gethWallet && gethWallet.network === currentWallet">
                            <form @submit.prevent="sendGethMoney">
                                <div class="form-group">
                                    <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                        <div><h3>Ethereum type wallet</h3></div>
                                        <div class="parameters">
                                            <div class="list">
                                                <span class="opt">List coins</span>
                                                <span class="value">
                                                    <div v-for="coin in Object.keys(gethCoins)">
                                                        <a href="#"
                                                           :class="{active: currentGethAsset && currentGethAsset.id === Number(coin)}"
                                                           @click.prevent="currentGethAsset = findGethTokenById(coin)">@{{ findGethTokenById(coin).symbol }}: @{{ gethCoins[coin] }} $ | [@{{ findGethTokenById(coin).network }}] @{{ findGethTokenById(coin).address ? findGethTokenById(coin).address : 'native' }}</a>
                                                    </div>
                                                    <div>
                                                        <a href="#" :class="{active: currentGethAsset === null}"
                                                           @click.prevent="currentGethAsset = null">All: @{{ gethWalletUSDBalance(false).toFixed(2) }} $</a>
                                                    </div>
                                                    <div>
                                                        <a href="#" :class="{active: currentGethAsset === 0}"
                                                           @click.prevent="currentGethAsset = 0">Archived: @{{ gethWalletUSDBalance(true).toFixed(2) }} $</a>
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Balance usdt</span>
                                                <span class="value">@{{ currentGethAsset ? getBalanceByTokenId(currentGethAsset.id, true, false) : gethWalletUSDBalance(currentGethAsset === 0).toFixed(2) }}</span>
                                            </div>
                                            <div class="list" v-if="currentGethAsset">
                                                <span class="opt">Balance coins</span>
                                                <span
                                                    class="value">@{{ getBalanceByTokenId(currentGethAsset.id) }}</span>
                                            </div>
                                            <div class="list" v-if="currentGethAsset">
                                                <span class="opt">Rate</span>
                                                <span class="value">@{{ currentGethAsset.market.usdtPrice }}</span>
                                            </div>
                                            <div class="list" v-if="currentGethAsset">
                                                <span class="opt">Rate updated</span>
                                                <span class="value">@{{ dateFormat(currentGethAsset.market.priceUpdatedAt) }}</span>
                                            </div>
                                            <div class="list" v-if="currentGethAsset">
                                                <span class="opt">Calculate</span>
                                                <span class="value">
                                                    <input type="number" value="" placeholder="Amount in usdt"
                                                           class="form-control" v-model="calculateGeth"/>
                                                </span>
                                            </div>
                                            <div class="list" v-if="currentGethAsset">
                                                <span class="opt">Calculated result</span>
                                                <span class="value">@{{ calculatedGeth }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-{{ $display_options->width ?? 12 }} "
                                         v-if="withdrawGethAddress">
                                        <div><h4>Withdraw money</h4></div>
                                        <div class="parameters">
                                            <div class="list">
                                                <span class="opt">From address</span>
                                                <span class="value">
                                                    <div>@{{ withdrawGethAddress.address }}</div>
                                                    <div><a :href="trackerGethFromUrl" target="_blank">Show on @{{ trackerGethUrl }}</a></div>
                                                </span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Direct access</span>
                                                <span class="value">
                                                    <div v-if="mnemonic">
                                                        <div class="parameters">
                                                            <div class="list">
                                                                <span class="opt">Private key</span>
                                                                <span class="value" style="color: blue">@{{ mnemonic.privateKey }}</span>
                                                            </div>
                                                            <div class="list">
                                                                <span class="opt">Mnemonic</span>
                                                                <span class="value" style="color: blue">@{{ mnemonic.mnemonic }}</span>
                                                            </div>
                                                            <div class="list">
                                                                <span class="opt empty"></span>
                                                                <span class="value">
                                                                    <button type="button"
                                                                            @click.prevent="mnemonic = null"
                                                                            class="form-control btn btn-sm btn-primary">Hide</button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div v-else>
                                                        <input type="text" placeholder="2FA"
                                                               class="form-control" v-model="formGethMnemonic['2fa']"/>
                                                        <div v-if="errors['2fa']" v-for="error in errors['2fa']"
                                                             class="text-error">@{{ error }}
                                                        </div>
                                                        <button type="button" @click.prevent="mnemonicGethMoney"
                                                                style="margin-right: 15px"
                                                                class="form-control btn btn-sm btn-primary">Get mnemonic and private key</button>
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Network</span>
                                                <span
                                                    class="value">@{{ withdrawGethAddress.currency.network }}</span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Balance usdt</span>
                                                <span class="value">@{{ getBalanceByAddress(withdrawGethAddress).toFixed(2) }} summary for all assets</span>
                                            </div>
                                            <div class="list">
                                                <span class="opt">Select asset</span>
                                                <span class="value">
                                                    <select placeholder="Select asset" class="form-control"
                                                            v-model="formGeth.balance_id">
                                                        <option value=""></option>
                                                        <option v-for="balance in withdrawGethAddress.balances"
                                                                v-bind:value="balance.id" v-if="balance.balance > 0">
                                                            @{{ balance.token.symbol }} [@{{ balance.token.network }}]: @{{ balance.balance }}
                                                        </option>
                                                    </select>
                                                    <div v-if="errors.from" v-for="error in errors.from"
                                                         class="text-error">
                                                        @{{ error }}
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="list" v-if="selectedBalance">
                                                <span class="opt">Selected asset</span>
                                                <span class="value">
                                                     <div class="parameters">
                                                        <div class="list">
                                                            <span class="opt">Token Name</span>
                                                            <span class="value">@{{ selectedBalance.token.name }}
                                                            <img v-if="selectedBalance.token.market && selectedBalance.token.market.image"
                                                                 style="width: 25px; height: 25px"
                                                                 :src="selectedBalance.token.market.image"/>
                                                            </span>
                                                        </div>
                                                        <div class="list">
                                                            <span class="opt">Token Symbol</span>
                                                            <span
                                                                class="value">@{{ selectedBalance.token.symbol }}</span>
                                                        </div>
                                                        <div class="list">
                                                            <span class="opt">Token Network</span>
                                                            <span class="value">@{{ selectedBalance.token.network }} <span v-if="selectedBalance.token.market">[@{{ selectedBalance.token.market.network }}]</span></span>
                                                        </div>
                                                        <div class="list" v-if="selectedBalance.token.address">
                                                            <span class="opt">Contract address</span>
                                                            <span class="value">
                                                                @{{ selectedBalance.token.address }}
                                                                <div><a :href="trackerGethContractUrl" target="_blank">Show on @{{ trackerGethUrl }}</a></div>
                                                            </span>
                                                        </div>
                                                        <div class="list" v-if="!selectedBalance.token.address">
                                                            <span class="opt">Explorer</span>
                                                            <span class="value">
                                                                <a :href="trackerGethUrl" target="_blank">@{{ trackerGethUrl }}</a>
                                                            </span>
                                                        </div>
                                                        <div class="list">
                                                            <span class="opt">Token type</span>
                                                            <span class="value">@{{ selectedBalance.token.address ?  'ERC20' : 'native' }}</span>
                                                        </div>
                                                        <div class="list">
                                                            <span class="opt">Balance</span>
                                                            <span class="value">@{{ selectedBalance.balance }}</span>
                                                        </div>
                                                     </div>
                                                </span>
                                            </div>
                                            <div class="list" v-if="selectedBalance">
                                                <span class="opt">To address</span>
                                                <span class="value" style="min-width: 50%">
                                                    <input type="text"
                                                           :placeholder="selectedBalance.token.network + ' network address'"
                                                           class="form-control" v-model="formGeth.to"
                                                           style="min-width: 50%"/>
                                                    <div v-if="errors.to" v-for="error in errors.to"
                                                         class="text-error">@{{ error }}
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="list" v-if="selectedBalance">
                                                <span class="opt">Amount in tokens</span>
                                                <span class="value">
                                                    <input type="number" step="0.00000001" placeholder="Amount"
                                                           v-if="formGeth.withdraw_all"
                                                           class="form-control" :disabled="true"/>
                                                    <input type="number" step="0.00000001" placeholder="Amount" v-else
                                                           class="form-control" v-model="formGeth.amount"/>

                                                    <div>
                                                        Amount in USDT: @{{ sendGethAmountUsdt }}
                                                    </div>
                                                    <div v-if="errors.amount" v-for="error in errors.amount"
                                                         class="text-error">@{{ error }}
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="list" v-if="selectedBalance">
                                                <span class="opt">Withdraw all</span>
                                                <span class="value">
                                                    <div @click.prevent="formGeth.withdraw_all=!formGeth.withdraw_all"
                                                         class="toggle btn"
                                                         :class="{'btn-default': !formGeth.withdraw_all, off: !formGeth.withdraw_all, 'btn-primary': formGeth.withdraw_all}"
                                                         data-toggle="toggle"
                                                         style="width: 68.3px; height: 29px;"><input type="checkbox"
                                                                                                     v-model="formGeth.withdraw_all"
                                                                                                     class="toggleswitch"><div
                                                            class="toggle-group"><label
                                                                class="btn btn-primary toggle-on">On</label><label
                                                                class="btn btn-default active toggle-off">Off</label><span
                                                                class="toggle-handle btn btn-default"></span></div></div>

                                                    <div v-if="errors.amount" v-for="error in errors.withdraw_all"
                                                         class="text-error">@{{ error }}
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="list" v-if="selectedBalance">
                                                <span class="opt">2FA</span>
                                                <span class="value">
                                                    <input type="text" placeholder="2FA"
                                                           class="form-control" v-model="formGeth['2fa']"/>
                                                    <div v-if="errors['2fa']" v-for="error in errors['2fa']"
                                                         class="text-error">@{{ error }}
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="list" v-if="selectedBalance">
                                                <span class="opt empty"></span>
                                                <span class="value">
                                                    <div class="form-group" v-if="errors.global">
                                                        <div v-for="error in errors.global"
                                                             class="text-error">@{{ error }}
                                                        </div>
                                                    </div>

                                                    <div v-if="loading" style="color: green">
                                                        processing...
                                                    </div>

                                                    <div v-if="gas" style="color: blue">
                                                        Gas will take @{{ gas.gas }}
                                                    </div>

                                                    <div style="display: flex">
                                                        <button type="button"
                                                                @click.prevent="withdrawGethAddress = null"
                                                                style="margin-right: 15px"
                                                                class="form-control btn btn-sm btn-default">Close</button>
                                                        <button type="button" @click.prevent="estimateGethMoney"
                                                                style="margin-right: 15px"
                                                                class="form-control btn btn-sm btn-primary">Estimate</button>
                                                        <button type="submit"
                                                                class="form-control btn btn-sm btn-primary">Send money</button>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-{{ $display_options->width ?? 12 }}">
                                        <div>
                                            <h4>Addresses</h4>
                                        </div>

                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th></th>
                                                <th>Address</th>
                                                <th>Assets</th>
                                                <th>Balance USDT</th>
                                                <th>Dates</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr v-for="address in activeGethAddresses">
                                                <td>
                                                    <div>ID: @{{ address.id }}</div>
                                                    <div>User: @{{ address.user.id }} [@{{ address.user.username }}]
                                                    </div>
                                                </td>
                                                <td>
                                                    <img v-if="address.currency.market && address.currency.market.image"
                                                         style="width: 25px; height: 25px"
                                                         :src="address.currency.market.image"/>
                                                </td>
                                                <td>
                                                    <div>@{{ address.address }}</div>
                                                    <div>Main asset: @{{ address.currency.symbol }} [@{{
                                                        address.currency.network }}]
                                                    </div>
                                                    <div>
                                                        <a :href="getTrackerUrlByAddress(address.currency.network, address.address)"
                                                           target="_blank">Show on @{{
                                                            getTrackerUrlByNetwork(address.currency.network) }}</a>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div v-for="balance in address.balances">
                                                        <div v-if="balance.balance > 0">@{{ balance.token.symbol }}: @{{
                                                            balance.balance }} | [@{{
                                                            balance.token.network }}] @{{ balance.token.address ?
                                                            'ERC20' : 'native' }}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>@{{ getBalanceByAddress(address).toFixed(2) }}</td>
                                                <td>
                                                    <div>Created At</div>
                                                    <div>@{{ dateFormat(address.createdAt) }}</div>
                                                    <div>Checked at</div>
                                                    <div>@{{ dateFormat(address.lastCheckAt) }}</div>
                                                </td>
                                                <td>
                                                    <div v-if="!address.isArchived">
                                                        <a href="#" title="Withdraw" style="width: 100px"
                                                           class="btn btn-sm btn-primary pull-right"
                                                           @click.prevent="withdrawGethAddress = address">
                                                            <span class="hidden-xs hidden-sm">Withdraw</span>
                                                        </a>
                                                    </div>

                                                    <div v-if="!address.isArchived">
                                                        <a href="#"
                                                           class="btn btn-sm btn-default pull-right"
                                                           style="width: 100px"
                                                           @click.prevent="archiveGethAddress(address.id)">
                                                            <span class="hidden-xs hidden-sm">Archive</span>
                                                        </a>
                                                    </div>
                                                    <div v-if="address.isArchived">
                                                        <a href="#"
                                                           class="btn btn-sm btn-default pull-right"
                                                           style="width: 100px"
                                                           @click.prevent="unarchivedGethAddress(address.id)">
                                                            <span class="hidden-xs hidden-sm">Unarchived</span>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr v-if="activeGethAddresses.length === 0">
                                                <td colspan="7">No data.</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <div>
                                            <h4>History</h4>
                                        </div>
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Status</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Amount</th>
                                                <th>TX</th>
                                                <th>Dates</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr v-for="task in activeGethHistory"
                                                v-bind:class="{dangerous: task.status === 5}">
                                                <td>
                                                    <div>ID: @{{ task.id }}</div>
                                                    <div>User: @{{ task.user.id }} [@{{ task.user.username }}]
                                                    </div>
                                                </td>
                                                <td v-bind:style="{color: getColorByStatus(task.status)}">
                                                    @{{ task.readableStatus}}
                                                    <span
                                                        v-if="task.status === 5">This status required contact with developer.</span>
                                                </td>
                                                <td>@{{ task.data.address.address }}
                                                    <div>
                                                        <a :href="getTrackerUrlByAddress(task.data.address.currency.network, task.data.address.address)"
                                                           target="_blank">Show on @{{
                                                            getTrackerUrlByNetwork(task.data.address.currency.network)
                                                            }}</a></div>
                                                </td>
                                                <td>
                                                    @{{ task.data.to }}
                                                    <div>
                                                        <a :href="getTrackerUrlByAddress(task.data.address.currency.network, task.data.to)"
                                                           target="_blank">Show on @{{
                                                            getTrackerUrlByNetwork(task.data.address.currency.network)
                                                            }}</a></div>
                                                </td>
                                                <td>
                                                    <div>@{{ task.data.amount }} (@{{ (task.data.amount *
                                                        task.data.balance.token.market.usdtPrice).toFixed(2) }}) USD
                                                    </div>
                                                    <div>@{{ task.data.balance.token.symbol }} [@{{
                                                        task.data.balance.token.network }}] @{{
                                                        task.data.balance.token.address ?
                                                        'ERC20' : 'native' }}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div v-if="task.postData && task.postData.sent">
                                                        <div>@{{ task.postData.sent.sentAmount }} (@{{
                                                            (task.postData.sent.sentAmount *
                                                            task.data.balance.token.market.usdtPrice).toFixed(2) }}) USD
                                                        </div>
                                                        <div>
                                                            <a :href="getTrackerUrlByTx(task.data.address.currency.network, task.postData.sent.tx)"
                                                               target="_blank">Show TX on @{{
                                                                getTrackerUrlByNetwork(task.data.address.currency.network)
                                                                }}</a></div>
                                                    </div>
                                                    <div v-else>
                                                        N/A
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>Created At</div>
                                                    <div>@{{ dateFormat(task.createdAt) }}</div>
                                                    <div>Checked at</div>
                                                    <div>@{{ dateFormat(task.updatedAt) }}</div>
                                                </td>
                                            </tr>
                                            <tr v-if="activeGethHistory.length === 0">
                                                <td colspan="7">No data.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="panel-body" v-else>
                        loading data...
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .active {
            color: black;
            font-weight: bold;
        }

        .select-wallet a {
            cursor: pointer;
            margin-right: 30px;
        }

        .parameters .list {
            margin-bottom: 10px;
        }

        .parameters .list .opt {
            font-weight: bold;
            min-width: 140px;
            display: inline-block;
            vertical-align: top;
        }

        .parameters .list .opt:after {
            content: ":";
        }

        .parameters .list .opt.empty:after {
            content: initial;
        }

        .parameters .list .value {
            display: inline-block;
        }

        .dangerous {
            background: red;
        }
    </style>
@stop
@push('javascript')
    <script>
        const Trackers = {
            'ethereum': 'https://etherscan.io',
            'ethclassic': 'https://etcblockexplorer.com',
            'bsc': 'https://bscscan.com',
            'velas': 'https://evmexplorer.velas.com',
            'matic': 'https://polygonscan.com',
            'klaytn': 'https://scope.klaytn.com',
        };

        new Vue({
            el: '#ethereum-control',
            data: () => {
                return {
                    loaded: false,
                    loading: false,
                    formGethDefaults: {
                        balance_id: null,
                        amount: null,
                        withdraw_all: false,
                        to: null,
                        '2fa': null,
                    },
                    formGethMnemonicDefaults: {
                        '2fa': null,
                    },
                    formGeth: {},
                    formGethMnemonic: {},
                    formBtc: {
                        to: null,
                        amount: null,
                        g2fa: null,
                    },
                    errors: {},
                    updating: null,
                    data: [],
                    typeWallet: null,
                    calculateBtc: 0,
                    calculatedBtc: 0,
                    calculateGeth: 0,
                    calculatedGeth: 0,
                    calculateUsdt: 0,
                    gethTaskTxDetails: null,
                    default: {
                        to: {!! request()->get('address') ? sprintf('\'%s\'', request()->get('address')) : 'null'  !!},
                        amount: {{ request()->get('amount') ? (float) request()->get('amount') : 'null' }},
                        type: {!! request()->get('type') ? sprintf('\'%s\'', request()->get('type')) : 'null'  !!},
                    },
                    commissionUsdt: null,
                    currentWallet: null,
                    currentGethAsset: null,
                    withdrawGethAddress: null,
                    gas: null,
                    mnemonic: null,
                }
            },
            computed: {
                sendBtcAmountUsdt() {
                    return (this.formBtc.amount * this.btcWallet.token.market.usdtPrice).toFixed(2);
                },
                sendGethAmountUsdt() {
                    return this.selectedBalance && this.selectedBalance.token.market ?
                        (this.formGeth.amount * this.selectedBalance.token.market.usdtPrice).toFixed(2) :
                        0;
                },
                btcWallet() {
                    return this.data.find((wallet) => wallet.network === 1);
                },
                gethWallet() {
                    return this.data.find((wallet) => wallet.network === 2);
                },
                gethCoins() {
                    const coins = {};

                    this.gethWallet.addresses
                        .filter((address) => !address.isArchived)
                        .forEach((address) => {
                            address.balances.forEach((balance) => {
                                if (!coins[balance.token.id] && balance.balance > 0) {
                                    coins[balance.token.id] = this.getBalanceByTokenId(balance.token.id, true, false);
                                }
                            });
                        });

                    return coins;
                },
                activeGethAddresses() {
                    return this.gethWallet.addresses
                        .filter((address) => {
                            if (this.currentGethAsset !== 0) {
                                return !address.isArchived;
                            }

                            return address.isArchived;
                        })
                        .filter((address) => {
                            if (this.currentGethAsset) {
                                let found = false;

                                address.balances.forEach((balance) => {
                                    if (balance.token.id === this.currentGethAsset.id) {
                                        found = true;
                                    }
                                });

                                return found;
                            }

                            return true;
                        })
                        .sort((addressA, addressB) => {
                            if (this.getBalanceByAddress(addressA) < this.getBalanceByAddress(addressB)) {
                                return 1;
                            } else if (this.getBalanceByAddress(addressA) > this.getBalanceByAddress(addressB)) {
                                return -1;
                            }

                            return 0;
                        });
                },
                activeGethHistory() {
                    return this.currentGethAsset ?
                        this.gethWallet.history.filter((history) => history.data.balance.token.id === this.currentGethAsset.id)
                        : this.gethWallet.history;
                },
                selectedBalance() {
                    return this.formGeth.balance_id ?
                        this.findBalanceById(this.formGeth.balance_id) :
                        null;
                },
                trackerGethUrl() {
                    return this.withdrawGethAddress
                        ? this.getTrackerUrlByNetwork(this.withdrawGethAddress.currency.network) : null;
                },
                trackerGethContractUrl() {
                    return this.selectedBalance && this.selectedBalance.token.address
                        ?
                        this.getTrackerUrlByAddress(this.selectedBalance.token.network, this.selectedBalance.token.address) : null;
                },
                trackerGethFromUrl() {
                    return this.withdrawGethAddress ?
                        this.getTrackerUrlByAddress(this.withdrawGethAddress.currency.network, this.withdrawGethAddress.address) : null;
                },
            },
            watch: {
                calculateBtc(value) {
                    this.calculatedBtc = (value / this.btcWallet.token.market.usdtPrice).toFixed(8);
                },
                calculateGeth(value) {
                    this.calculatedGeth = this.currentGethAsset && this.currentGethAsset.market ? (value / this.currentGethAsset.market.usdtPrice).toFixed(8) : 0;
                },
                currentWallet() {
                    this.setFormDefaults();
                },
                currentGethAsset() {
                    this.withdrawGethAddress = null;
                    this.setFormDefaults();
                },
                withdrawGethAddress() {
                    this.setFormDefaults();
                },
                'formGeth.withdraw_all'(value) {
                    if (value) {
                        this.formGeth.amount = null;
                    }
                },
            },
            methods: {
                getTrackerUrlByNetwork(network) {
                    return Trackers[network];
                },
                getTrackerUrlByAddress(network, address) {
                    return Trackers[network] + '/address/' + address;
                },
                getTrackerUrlByTx(network, tx) {
                    return Trackers[network] + '/tx/' + tx;
                },
                getBalanceByTokenId(tokenId, inUSD = false, archived = null) {
                    let balanceAsset = 0;

                    this.gethWallet.addresses
                        .filter((address) => {
                            if (archived === null) {
                                return true;
                            }

                            return address.isArchived === archived;
                        })
                        .forEach((address) => {
                            address.balances.forEach((balance) => {
                                if (balance.token.id === Number(tokenId) && balance.token.market) {
                                    balanceAsset += inUSD ?
                                        balance.balance * balance.token.market.usdtPrice :
                                        balance.balance;
                                }
                            });
                        });

                    return inUSD ? balanceAsset.toFixed(2) : balanceAsset;
                },
                getBalanceByAddress(address) {
                    let balanceAsset = 0;

                    address.balances.forEach((balance) => {
                        if(balance.token.market) {
                            balanceAsset += balance.balance * balance.token.market.usdtPrice;
                        }
                    });

                    return balanceAsset;
                },
                findGethTokenById(tokenId) {
                    let token = null;

                    this.gethWallet.addresses.forEach((address) => {
                        address.balances.forEach((balance) => {
                            if (balance.token.id === Number(tokenId)) {
                                token = balance.token;
                            }
                        });
                    });

                    return token;
                },
                findBalanceById(balanceId) {
                    let balanceModel = null;

                    this.gethWallet.addresses.forEach((address) => {
                        address.balances.forEach((balance) => {
                            if (balance.id === Number(balanceId)) {
                                balanceModel = balance;
                            }
                        });
                    });

                    return balanceModel;
                },
                gethWalletUSDBalance(archived = null) {
                    let balance = 0;

                    this.gethWallet.addresses
                        .filter((address) => {
                            if (archived === null) {
                                return true;
                            }

                            return address.isArchived === archived;
                        })
                        .forEach((address) => {
                            balance = balance + this.getBalanceByAddress(address);
                        });

                    return balance;
                },
                getColorByStatus(status) {
                    if (status === 3) {
                        return 'red';
                    } else if (status === 4) {
                        return 'green';
                    }

                    return 'gray';
                },
                sendBtcMoney() {
                    if (this.loading ||
                        !this.formBtc.to ||
                        !this.formBtc.amount ||
                        !confirm('You want to transfer ' + this.formBtc.amount + ' btc money to this wallet ' + this.formBtc.to + ' ?')) {
                        return;
                    }

                    this.loading = true;

                    axios.post('{{ route('voyager.finance.bitcoin.send') }}', this.formBtc).then(() => {
                        this.formBtc.to = null;
                        this.formBtc.amount = null;
                        this.formEth.g2fa = null;
                        this.errors = {};

                        toastr.success('Money sent, see details in transaction list.', 'Bitcoin operation.');
                    }).catch((e) => {
                        if (e.response.status === 422) {
                            this.errors = e.response.data.errors;
                        } else if ([401, 403].includes(e.response.status)) {
                            toastr.error('You are not authorized.', 'Bitcoin operation.');
                        } else {
                            toastr.error('Server error.', 'Bitcoin operation.');
                        }
                    }).finally(() => {
                        this.loading = false;
                    });
                },
                async mnemonicGethMoney() {
                    if (this.loading) {
                        return;
                    }

                    this.errors = {};

                    this.loading = true;

                    axios.post('{{ sprintf('/%s/finance/geth/mnemonic/', config('voyager.prefix')) }}' + this.withdrawGethAddress.id, this.formGethMnemonic).then((data) => {
                        this.mnemonic = data.data.data;
                    }).catch((e) => {
                        if (e.response.status === 422) {
                            this.errors = e.response.data.errors;
                        } else if ([401, 403].includes(e.response.status)) {
                            toastr.error('You are not authorized.', 'Geth type operation.');
                        } else {
                            toastr.error('Server error.', 'Geth type operation.');
                        }
                    }).finally(() => {
                        this.loading = false;
                    });
                },
                async estimateGethMoney() {
                    if (this.loading) {
                        return;
                    }

                    this.errors = {};

                    this.loading = true;

                    axios.post('{{ route('voyager.finance.geth.estimate') }}', this.formGeth).then((data) => {
                        this.gas = data.data.data;
                    }).catch((e) => {
                        if (e.response.status === 422) {
                            this.errors = e.response.data.errors;
                        } else if ([401, 403].includes(e.response.status)) {
                            toastr.error('You are not authorized.', 'Geth type operation.');
                        } else {
                            toastr.error('Server error.', 'Geth type operation.');
                        }
                    }).finally(() => {
                        this.loading = false;
                    });
                },
                async sendGethMoney() {
                    if (this.loading) {
                        return;
                    }

                    if (!confirm(`Are you sure that you want to transfer ${this.formGeth.amount} to ${this.formGeth.to} in ${this.selectedBalance.token.symbol} [${this.selectedBalance.token.network}]`)) {
                        return;
                    }

                    this.errors = {};

                    this.loading = true;

                    axios.post('{{ route('voyager.finance.geth.send') }}', this.formGeth).then(() => {
                        this.setFormDefaults();

                        toastr.success('Money sent, see details in transaction list.', 'Geth type operation.');
                    }).catch((e) => {
                        if (e.response.status === 422) {
                            this.errors = e.response.data.errors;
                        } else if ([401, 403].includes(e.response.status)) {
                            toastr.error('You are not authorized.', 'Geth type operation.');
                        } else {
                            toastr.error('Server error.', 'Geth type operation.');
                        }
                    }).finally(() => {
                        this.loading = false;
                    });
                },
                async archiveGethAddress(addressId) {
                    axios.put('{{ sprintf('/%s/finance/geth/archive/', config('voyager.prefix')) }}' + addressId)
                        .then(async (data) => {
                            await this.updateData();

                            toastr.success('Wallet archived.', 'Geth type operation.');
                        }).catch((e) => {
                        if (e.response.status === 422) {
                            this.errors = e.response.data.errors;
                        } else if ([401, 403].includes(e.response.status)) {
                            toastr.error('You are not authorized.', 'Geth type operation.');
                        } else {
                            toastr.error('Server error.', 'Geth type operation.');
                        }
                    });
                },
                async unarchivedGethAddress(addressId) {
                    axios.put('{{ sprintf('/%s/finance/geth/unarchived/', config('voyager.prefix')) }}' + addressId)
                        .then(async (data) => {
                            await this.updateData();

                            toastr.success('Wallet unarchived.', 'Geth type operation.');
                        }).catch((e) => {
                        if (e.response.status === 422) {
                            this.errors = e.response.data.errors;
                        } else if ([401, 403].includes(e.response.status)) {
                            toastr.error('You are not authorized.', 'Geth type operation.');
                        } else {
                            toastr.error('Server error.', 'Geth type operation.');
                        }
                    });
                },
                async updateData() {
                    await axios.get('{{ route('voyager.finance.wallets.data') }}').then((r) => {
                        this.data = r.data.data.wallets;
                        this.loaded = true;
                    }).catch((e) => {
                        if (e.response) {
                            toastr.error('Server error.', 'Update data.');
                        }
                    });
                },
                enableMonitoring() {
                    this.updating = setInterval(() => {
                        this.updateData();
                    }, 10000);
                },
                dateFormat(date) {
                    return (new Date(date))
                        .toLocaleDateString("en-US", {
                            year: 'numeric',
                            month: 'numeric',
                            day: 'numeric',
                            hour: 'numeric',
                            minute: 'numeric',
                            second: 'numeric'
                        })
                },
                ethToUsdt(amount) {
                    return (amount * this.data.ethRate).toFixed(2);
                },
                setFormDefaults() {
                    this.formGeth = {...this.formGethDefaults}
                    this.formGethMnemonic = {...this.formGethMnemonicDefaults}
                    this.errors = {};
                    this.gas = null;
                    this.mnemonic = null;
                },
            },
            beforeDestroy() {
                clearInterval(this.updating)
            },
            async created() {
                window.WI = this;
                this.setFormDefaults();

                await this.updateData();

                this.currentWallet = this.data.length > 0 ?
                    this.data[0].network : null;

                this.enableMonitoring();
            }
        });
    </script>
@endpush
