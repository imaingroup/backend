<?php
declare(strict_types=1);

namespace App\Modules\Messages\Models;

use App\Modules\Users\Models\User;
use App\Modules\Core\Models\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Chat model.
 */
final class Chat extends Model
{
    protected $table = 'messages_chats';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_group' => 'boolean',
    ];

    /**
     * {@inheritDoc}
     */
    public function getRouteKeyName(): string
    {
        return 'sid';
    }

    /**
     * Events for model.
     */
    protected static function booted()
    {
        Chat::creating(function (Chat $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }

    /**
     * Relation with ChatUser.
     *
     * @return HasMany
     */
    public function chatUsers(): HasMany
    {
        return $this
            ->hasMany(ChatUser::class, 'messages_chats_id')
            ->where('messages_chats_users.deleted', false)
            ->with('user');
    }

    /**
     * Relation with Message.
     *
     * @return HasMany
     */
    public function messages(): HasMany
    {
        return $this->hasMany(Message::class, 'messages_chats_id');
    }

    /**
     * Get chat users with contact sid.
     * Required user for get contact sid.
     *
     * @param User $user
     * @return Collection
     */
    public function chatUsersWithContactSid(User $user): Collection
    {
        return $this
            ->chatUsers()
            ->select('messages_chats_users.*', 'mc.sid as contact_sid')
            ->leftJoin('messages_contacts as mc', function (JoinClause $join) use ($user) {
                $join->on('messages_chats_users.users_id', '=', 'mc.contact_users_id')
                    ->where('mc.users_id', '=', $user->id);
            })
            ->get();
    }

    /**
     * Relation with HasMany.
     *
     * @return HasMany
     */
    public function acceptedInvitations(): HasMany
    {
        return $this->hasMany(ChatAcceptedInvitation::class, 'messages_chats_id');
    }

    /**
     * Check that user is owner for current chat.
     *
     * @param User $user
     * @return bool
     */
    public function isOwner(\App\Modules\Users\Models\User $user): bool
    {
        return $this->chatUsers()
            ->where('users_id', $user->id)
            ->where('is_owner', true)
            ->exists();
    }

    /**
     * Check accepted invitation for this user.
     *
     * @param User $user
     * @return bool
     */
    public function checkAcceptedInvitation(\App\Modules\Users\Models\User $user): bool
    {
        return $this->hasMany(ChatAcceptedInvitation::class, 'messages_chats_id')
            ->where('users_id', $user->id)
            ->exists();
    }

    /**
     * Check that user is owner for current chat.
     *
     * @param User $user
     * @return bool
     */
    public function hasUser(User $user): bool
    {
        return $this->chatUsers()
            ->where('messages_chats_users.users_id', $user->id)
            ->exists();
    }

    /**
     * Check that user is owner for current chat.
     *
     * @return User|null
     */
    public function getOwnerAttribute(): ?User
    {
        return $this->chatUsers()
            ->where('is_owner', true)
            ->first()
            ?->user;
    }

    /**
     * Check that user is owner for current chat.
     *
     * @return ChatUser|null
     */
    public function getChatUserOwnerAttribute(): ?ChatUser
    {
        return $this->chatUsers()
            ->where('is_owner', true)
            ->first();
    }

    /**
     * Get chat name for user.
     *
     * @param User $user
     * @return string|null
     */
    public function getChatName(User $user): ?string
    {
        return $this->is_group ?
            $this->name :
            $this->getSingleChatNameForUser($user);
    }

    /**
     * Get chat name for admin.
     *
     * @return string|null
     */
    public function getAdminChatName(): ?string
    {
        $chatUsers = $this->chatUsers;

        if ($this->is_group) {
            return $this->name;
        }

        return $chatUsers->count() == 2 ?
            sprintf(
                'Chat between users %s and %s',
                $chatUsers->get(0)
                    ->user
                    ->username,
                $chatUsers->get(1)
                    ->user
                    ->username
            ) :
            'Single chat';
    }

    /**
     * Get avatar url for user.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return string|null
     */
    public function getAvatarUrl(\App\Modules\Users\Models\User $user): ?string
    {
        $avatar = $this->is_group ?
            ($this->avatar ?? $this->owner?->avatar) :
            $this->getSingleChatAvatarForUser($user);

        return $avatar ?
            Storage::disk('local')->url($avatar)
            : Storage::disk('local')->url(User::DEFAULT_AVATAR);
    }

    /**
     * Get admin avatar url for user.
     *
     * @return string|null
     */
    public function getAdminAvatarUrl(): ?string
    {
        $avatar = $this->is_group ?
            ($this->avatar ?? $this->owner?->avatar) :
            null;

        return $avatar ?
            Storage::disk('local')->url($avatar)
            : Storage::disk('local')->url(User::DEFAULT_AVATAR);
    }

    /**
     * Get single chat name for user.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return string|null
     */
    private function getSingleChatNameForUser(User $user): ?string
    {
        return $this->chatUsers()
            ->where('users_id', '!=', $user->id)
            ->first()
            ?->user
            ?->username;
    }

    /**
     * Get single chat avatar for user.
     *
     * @param User $user
     * @return string|null
     */
    private function getSingleChatAvatarForUser(\App\Modules\Users\Models\User $user): ?string
    {
        return $this->chatUsers()
            ->where('users_id', '!=', $user->id)
            ->first()
            ?->user
            ?->avatar;
    }
}
