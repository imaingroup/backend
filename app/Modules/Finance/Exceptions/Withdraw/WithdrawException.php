<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Withdraw;

use App\Modules\Finance\Exceptions\Ethereum\EthereumException;

/**
 * WithdrawException.
 */
abstract class WithdrawException extends EthereumException
{
}
