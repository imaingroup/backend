<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum\Erc20;


/**
 * NoTransferMethodException.
 */
final class NoTransferMethodException extends Erc20Exception
{
}
