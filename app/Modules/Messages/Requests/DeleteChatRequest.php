<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation delete chat request.
 */
final class DeleteChatRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param MessageService $messageService
     */
    public function __construct(
        private MessageService $messageService
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user !== null
            && $this->messageService->checkAccessDeleteChat($user, $this->chat);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'user' => "App\\Modules\\Users\\Models\\User",
        'chat' => "App\\Modules\\Messages\\Models\\Chat",
    ])]
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'chat' => $this->chat
        ];
    }
}
