<?php
declare(strict_types=1);

namespace App\Modules\Logs\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This request class need for validating send log request.
 */
final class SendLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['log' => "string"])]
    public function rules(): array
    {
        return [
            'log' => 'required|string|max:100000',
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape(['log' => "mixed"])]
    public function validated(): array
    {
        $log = str_ireplace(
            ['<?php', '<?', '?>'],
            '',
            file_get_contents("php://input")
        );

        return [
            'log' => sprintf(
                '%s | User id: %s',
                $log,
                Auth::user()->id ?? 'not logged'
            ),
        ];
    }
}
