<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * NotSyncedException.
 */
final class NotSyncedException extends EthereumException
{
}
