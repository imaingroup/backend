<?php
declare(strict_types=1);

namespace App\Modules\Exchange\Services;

use App\Modules\Money\Services\TransactionStatsService;

/**
 * Exchange service.
 */
final class ExchangeStatsService
{
    /**
     * Sum abc bought for deposited.
     */
    public function sumAbcBoughtDeposited(): float
    {
        return $this->getTransactionStatsService()
            ->sumAbcBoughtDeposited();
    }
    /**
     * Sum abc bought for dividends.
     */
    public function sumAbcBoughtDividends(): float
    {
        return $this->getTransactionStatsService()
            ->sumAbcBoughtDividends();
    }

    /**
     * Get {@see TransactionStatsService::class}.
     *
     * @return TransactionStatsService
     */
    private function getTransactionStatsService(): TransactionStatsService
    {
        return app(TransactionStatsService::class);
    }
}
