<?php
declare(strict_types=1);

namespace App\Modules\Security\Requests;

use App\Modules\Core\Services\ClientInfoService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use function is_integer;
use function is_string;
use function preg_match;
use function sprintf;
use function str_contains;
use function stripos;
use function strlen;
use function substr;

/**
 * This request class need for validating data from register form.
 */
final class IdentificationSetInfoRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Core\Services\ClientInfoService $clientInfoService
     */
    public function __construct(private ClientInfoService $clientInfoService)
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'action' => 'nullable|integer|min:0|max:65535',
            'userid' => 'required|scalar|max:255',
            'id1' => 'nullable|scalar|max:255',
            'id2' => 'nullable|scalar|max:255',
            'user_agent' => 'required|scalar|max:255',
            'browser' => 'nullable|scalar|max:255',
            'browser_version' => 'nullable|scalar|max:255',
            'browser_major_version' => 'nullable|scalar|max:255',
            'is_ie' => 'nullable|boolean',
            'is_chrome' => 'nullable|boolean',
            'is_firefox' => 'nullable|boolean',
            'is_safari' => 'nullable|boolean',
            'is_opera' => 'nullable|boolean',
            'engine' => 'nullable|scalar|max:255',
            'engine_version' => 'nullable|scalar|max:255',
            'platform' => 'nullable|scalar|max:255',
            'os' => 'nullable|scalar|max:255',
            'os_version' => 'nullable|scalar|max:255',
            'is_windows' => 'nullable|boolean',
            'is_linux' => 'nullable|boolean',
            'is_ubuntu' => 'nullable|boolean',
            'is_solaris' => 'nullable|boolean',
            'device' => 'nullable|scalar|max:255',
            'device_type' => 'nullable|scalar|max:255',
            'device_vendor' => 'nullable|scalar|max:255',
            'cpu' => 'nullable|scalar|max:255',
            'is_mobile' => 'nullable|boolean',
            'is_mobile_android' => 'nullable|boolean',
            'is_mobile_opera' => 'nullable|boolean',
            'is_mobile_windows' => 'nullable|boolean',
            'is_mobile_blackberry' => 'nullable|boolean',
            'is_mobile_ios' => 'nullable|boolean',
            'is_iphone' => 'nullable|boolean',
            'is_ipod' => 'nullable|boolean',
            'color_depth' => 'nullable|scalar|max:255',
            'current_resolution' => 'nullable|scalar|max:255',
            'available_resolution' => 'nullable|scalar|max:255',
            'device_xdpi' => 'nullable|scalar|max:255',
            'device_ydpi' => 'nullable|scalar|max:255',
            'plugins' => 'nullable|scalar|max:max:65535',
            'is_java' => 'nullable|boolean',
            'java_version' => 'nullable|scalar|max:255',
            'is_flash' => 'nullable|boolean',
            'flash_version' => 'nullable|scalar|max:255',
            'is_silverlight' => 'nullable|boolean',
            'silverlight_version' => 'nullable|scalar|max:255',
            'mime_types' => 'nullable|scalar|max:255',
            'is_mime_types' => 'nullable|boolean',
            'is_font' => 'nullable|boolean',
            'fonts' => 'nullable|scalar|max:65535',
            'is_local_storage' => 'nullable|boolean',
            'is_session_storage' => 'nullable|boolean',
            'is_cookie' => 'nullable|boolean',
            'time_zone' => 'nullable|scalar|max:255',
            'language' => 'nullable|scalar|max:255',
            'languages' => 'nullable|scalar|max:255',
            'system_language' => 'nullable|scalar|max:255',
            'is_canvas' => 'nullable|boolean',
            'canvas_hash' => 'nullable|scalar|max:255',
            'local_storage' => 'nullable|scalar|max:255',
            'cookie' => 'nullable|scalar|max:255'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): array
    {
        $data = parent::validated();
        $data['users_id'] = Auth::user()?->id;
        $data['ip'] = $this->clientInfoService->getRealIp();
        $data['country'] = $this->clientInfoService->getCountry();

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareForValidation(): void
    {
        foreach ($this->rules() as $field => $rule) {
            $value = $this[$field];

            if (is_string($rule) && is_string($value)) {
                if (str_contains($rule, 'scalar')) {
                    $value = is_scalar($value) ? (string)$value : $value;

                    if (is_string($value)) {
                        if (preg_match('/.*max:(\d+).*/', $rule, $matches)) {
                            $length = (int)$matches[1];

                            if (strlen($value) > $length) {
                                $value = sprintf('%s...', substr($value, 0, $length - 3));
                            }
                        }
                    }
                } else if (str_contains($rule, 'boolean')) {
                    $value = stripos($value, '1') === 0 ||
                        stripos($value, 'true') === 0;
                }

                $this->merge([
                    $field => $value,
                ]);
            }
        }

        $this->merge([
            'action' => (int)$this['action'],
        ]);
    }
}
