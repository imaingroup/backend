<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Models\Chat;
use App\Modules\Messages\Models\Contact;
use App\Modules\Messages\Models\Message;
use App\Modules\Messages\Repositories\ChatRepository;
use App\Modules\Messages\Repositories\ContactRepository;
use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation send message request.
 */
final class SendMessageRequest extends FormRequest
{
    static public Contact|Chat|null $messageSource = null;
    static public ?Message $message = null;

    /**
     * Constructor.
     *
     * @param ContactRepository $contactRepository
     * @param ChatRepository $chatRepository
     * @param MessageService $messageService
     */
    public function __construct(
        private ContactRepository $contactRepository,
        private ChatRepository $chatRepository,
        private MessageService $messageService
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        if(!(self::$messageSource instanceof Contact) &&
            !(self::$messageSource instanceof Chat)
        ) {
            return false;
        }

        if (!$user) {
            return false;
        } else if (self::$messageSource instanceof Contact
            && !$this->messageService->checkAccessWriteInContact($user, self::$messageSource)
        ) {
            return false;
        } else if (self::$messageSource instanceof Chat
            && !$this->messageService->checkAccessWriteInChat(
                $user, self::$messageSource
            )) {
            return false;
        } else if (self::$message instanceof Message
            && !$this->messageService->checkAccessMessage($user, self::$message, self::$messageSource)
        ) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['message' => "string"])]
    public function rules(): array
    {
        return [
            'message' => 'required|scalar|max:1000|max_word_length:50'
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'user' => "App\Modules\Users\Models\User",
        'message' => "string"
    ])]
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'message' => parent::validated()['message']
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'message' => is_scalar($this->message) ? trim((string)$this->message) : null,
        ]);
    }
}
