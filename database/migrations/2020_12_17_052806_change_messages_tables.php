<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds index for sid column in messages_contacts table.
 */
final class ChangeMessagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages_contacts', function (Blueprint $table) {
            $table->unique('sid');
        });

        Schema::table('messages_chats', function (Blueprint $table) {
            $table->unsignedBigInteger('sid')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages_contacts', function (Blueprint $table) {
            $table->dropUnique('messages_contacts_sid_unique');
        });

        Schema::table('messages_chats', function (Blueprint $table) {
            $table->dropColumn('sid');
        });
    }
}
