<?php
declare(strict_types=1);

namespace App\Modules\Data\Requests;

use App\Modules\Core\Attributes\Updatable;
use App\Modules\Data\Dto\Params\DataParamsDto;
use App\Modules\Data\Dto\Params\DataRouteParamsDto;
use App\Modules\Data\Dto\Params\RouteActionParamsDto;
use App\Modules\Data\Exceptions\RouteResolveException;
use App\Modules\Data\Exceptions\RouteResolveHttpMethodException;
use App\Modules\Data\Exceptions\RouteResolveTargetException;
use App\Modules\Logs\Loggers\Logger;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Router;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Throwable;

/**
 * This request class need for validating data from register form.
 */
final class DataRequest extends FormRequest
{
    /** @var Collection Routes. */
    private Collection $routes;

    public const SUPPORTED_METHODS = [
        'GET',
        'POST',
        'PUT',
        'DELETE',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['methods' => "string", 'always' => "array"])]
    public function rules(): array
    {
        return [
            'methods' => 'required|array|min:1|max:10',
            'always' => [
                'nullable',
                Rule::in([0,1]),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape(['params' => DataParamsDto::class])]
    public function validated(): array
    {
        $dto = new DataParamsDto(
            collect(),
            (int)$this['always'] === 1,
            Auth::user()
        );

        foreach ($this['methods'] as $data) {
            $dto->methods->add(new DataRouteParamsDto(
                $data['route'],
                collect($data['parameters']),
                collect($data['request']),
                (int)$data['version'],
                $this->routes->get($data['route'])
            ));
        }

        return [
            'params' => $dto,
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function (Validator $validator) {
            if (count($validator->failed()) === 0) {
                $this->after($validator);
            }
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    private function after(Validator $validator): void
    {
        $list = collect();
        $message = 'Some route is invalid.';

        try {
            foreach ($this['methods'] as $data) {
                if (count($data) !== 4 ||
                    !(isset($data['route']) && is_string($data['route'])) ||
                    !(isset($data['parameters']) && is_array($data['parameters'])) ||
                    !(isset($data['request']) && is_array($data['request'])) ||
                    !(isset($data['version']) && is_numeric($data['version'])) ||
                    $data['route'] === 'data'
                ) {
                    $validator->errors()
                        ->add('methods', $message);

                    return;
                }

                try {
                    route($data['route'], $data['parameters']);
                } catch (RouteNotFoundException) {
                    $validator->errors()
                        ->add('methods', $message);

                    return;
                }

                $list->put($data['route'], $this->getAction($data['route']));
            }
        }
        catch (Throwable $e) {
            $this->getLogger()
                ->registerException($e);

            $validator->errors()
                ->add('methods', $message);

            return;
        }

        $this->routes = $list;
    }

    /**
     * Get action.
     *
     * @param string $route
     * @return RouteActionParamsDto
     * @throws ReflectionException
     * @throws RouteResolveException
     * @throws RouteResolveTargetException
     * @throws RouteResolveHttpMethodException
     */
    private function getAction(string $route): RouteActionParamsDto
    {
        $routeObject = $this->getRouter()
            ->getRoutes()
            ->getByName($route);

        $action = $routeObject?->action;

        if(!is_array($action) ||
            !isset($action['controller']) ||
            !is_string($action['controller'])
        ) {
            throw new RouteResolveException();
        }

        $methods = collect($routeObject->methods())
            ->intersect(self::SUPPORTED_METHODS);

        if(!isset($methods[0])) {
            throw new RouteResolveHttpMethodException();
        }

        $explode = explode('@', $action['controller']);

        if(count($explode) !== 2 ||
            !class_exists($explode[0]) ||
            !method_exists($explode[0], $explode[1])
        ) {
            throw new RouteResolveTargetException();
        }

        $reflection = new ReflectionMethod($explode[0], $explode[1]);
        /** @var Updatable|null $updatableAttribute */
        $updatableAttribute = null;

        foreach($reflection->getAttributes(Updatable::class) as $attribute) {
            $updatableAttribute = $attribute->newInstance();
        }

        return new RouteActionParamsDto(
            $explode[0],
            $explode[1],
            $methods[0],
            $updatableAttribute
        );
    }

    /**
     * Get Router.
     *
     * @return Router
     */
    private function getRouter(): Router
    {
        return app('router');
    }

    /**
     * Get Logger.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
