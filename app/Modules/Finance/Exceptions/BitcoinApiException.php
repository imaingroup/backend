<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions;

/**
 * Bitcoin api exception.
 */
final class BitcoinApiException extends FinanceException
{
}
