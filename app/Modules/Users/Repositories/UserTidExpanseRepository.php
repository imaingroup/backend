<?php


namespace App\Modules\Users\Repositories;

use App\Modules\Users\Models\UserTidExpanse;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * User tid expanse repository.
 */
final class UserTidExpanseRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param UserTidExpanse $model
     */
    public function __construct(protected UserTidExpanse $model)
    {}
}
