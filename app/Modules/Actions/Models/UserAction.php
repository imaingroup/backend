<?php

namespace App\Modules\Actions\Models;

use App\Modules\Core\Models\Model;

/**
 * User action model.
 */
final class UserAction extends Model
{
    const ACTION_BUY_PROFIT_CARD = 1;
    const ACTION_DEPOSIT = 2;
    const ACTION_WITHDRAW = 3;
    const ACTION_TRANSFER = 4;
    const ACTION_COMMISSION_USDT = 5;
    const ACTION_COMMISSION_ABC = 6;
    const ACTION_EXCHANGE_USDT_ABC = 7;

    const TYPES = [
        self::ACTION_BUY_PROFIT_CARD => 'TRADING CARD',
        self::ACTION_DEPOSIT => 'DEPOSIT',
        self::ACTION_WITHDRAW => 'WITHDRAWAL',
        self::ACTION_TRANSFER => 'TRANSFER',
        self::ACTION_COMMISSION_USDT => 'COMMISSION (USDT)',
        self::ACTION_COMMISSION_ABC => 'COMMISSION (IPRO)',
        self::ACTION_EXCHANGE_USDT_ABC => 'Exchanged USDT to IPRO',
    ];

    const RECENT_ACTIVITIES = [
        self::ACTION_BUY_PROFIT_CARD,
        self::ACTION_DEPOSIT,
        self::ACTION_TRANSFER,
        self::ACTION_COMMISSION_USDT,
        self::ACTION_COMMISSION_ABC,
        self::ACTION_EXCHANGE_USDT_ABC
    ];

    /**
     * Events for model.
     */
    protected static function booted(): void
    {
        static::creating(function (UserAction $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }

    /**
     * Get action in text representation.
     *
     * @return string|null
     */
    public function textAction(): ?string
    {
        return self::TYPES[$this->action] ?? $this->action;
    }
}
