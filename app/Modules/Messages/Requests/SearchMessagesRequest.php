<?php
declare(strict_types=1);


namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation search messages request.
 */
final class SearchMessagesRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Messages\Services\MessageService $messageService
     */
    public function __construct(
        private \App\Modules\Messages\Services\MessageService $messageService
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            (!$this->chat || ($this->chat && $this->messageService->checkAccessChat($user, $this->chat)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['search' => "string"])]
    public function rules(): array
    {
        return [
            'search' => 'required|scalar|max:100'
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'user' => "App\Modules\Users\Models\User",
        'message' => "string",
        'search' => "string"
    ])]
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'chat' => $this->chat,
            'search' => $this->search
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'search' => is_scalar($this->search) ? trim((string)$this->search) : $this->search,
        ]);
    }
}
