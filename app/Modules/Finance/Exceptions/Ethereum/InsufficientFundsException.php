<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * InsufficientFundsException.
 */
final class InsufficientFundsException extends EthereumException
{
}
