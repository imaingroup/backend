<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services;

use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use App\Modules\Finance\Exceptions\Btc\CreateAddressException;
use App\Modules\Finance\Exceptions\Btc\EstimateSmartFeeException;
use App\Modules\Finance\Exceptions\Btc\GetBalanceException;
use App\Modules\Finance\Exceptions\Btc\GeTransactionException;
use App\Modules\Finance\Exceptions\Btc\KeyNotFoundException;
use App\Modules\Finance\Exceptions\Btc\ListTransactionsException;
use App\Modules\Finance\Exceptions\Btc\ListUnspentException;
use App\Modules\Finance\Exceptions\Btc\NotEnoughBalanceException;
use App\Modules\Finance\Exceptions\Btc\SendException;
use App\Modules\Finance\Exceptions\Btc\SendIncorrectSettingsException;
use App\Modules\Finance\Exceptions\Btc\SetLabelException;
use App\Modules\Finance\Exceptions\Btc\SetTxFeeException;
use App\Modules\Finance\Exceptions\Btc\StatusException;
use App\Modules\Finance\Exceptions\SecureCryptRequestException;
use App\Modules\Finance\Exceptions\SecureCryptRequestParamsException;
use App\Modules\Finance\Services\V2\DataGetterService;
use Merkeleon\PhpCryptocurrencyAddressValidation\Validation;
use stdClass;
use Throwable;
use ValueError;

/**
 * BTC api service.
 */
final class BtcApiService
{
    private const HOW_LONG_UNLOCK_ACCOUNT_SECONDS = 100;

    /** @var string */
    private string $key;

    /**
     * Constructor.
     *
     * @throws KeyNotFoundException
     */
    public function __construct()
    {
        $this->loadKey();
    }

    /**
     * Create BTC address.
     *
     * @return string
     * @throws CreateAddressException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws SetLabelException
     */
    public function createAddress(): string
    {
        $data = $this->request(
            'getnewaddress',
            [],
        );

        try {
            $validator = Validation::make('BTC');

            if (!$validator->validate($data->result)) {
                throw new ValueError();
            }
        } catch (Throwable $e) {
            throw new CreateAddressException(previous: $e);
        }

        $this->setLabel($data->result, $data->result);

        return $data->result;
    }

    /**
     * Set label for address.
     *
     * @param string $address
     * @param string $label
     * @return void
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws SetLabelException
     */
    public function setLabel(string $address, string $label): void
    {
        $data = $this->request(
            'setlabel',
            [$address, $label],
        );

        if ($data->result !== null) {
            throw new SetLabelException();
        }
    }

    /**
     * Check status.
     *
     * @throws StatusException
     */
    public function checkStatus(): void
    {
        try {
            $this->getBalance();
        } catch (Throwable $e) {
            throw new StatusException(previous: $e);
        }
    }

    /**
     * Get balance.
     *
     * @return float
     * @throws GetBalanceException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    public function getBalance(): float
    {
        $data = $this->request(
            'getbalance',
            [],
        );

        if (!is_numeric($data->result)) {
            throw new GetBalanceException();
        }

        return (float)$data->result;
    }

    /**
     * Get address balance.
     *
     * @param string $address
     * @return float
     * @throws GetBalanceException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    public function getAddressBalance(string $address): float
    {
        $data = $this->request(
            'getreceivedbyaddress',
            [$address],
        );

        if (!is_numeric($data->result)) {
            throw new GetBalanceException();
        }

        return (float)$data->result;
    }

    /**
     * Get income bitcoin transactions which no spent.
     *
     * @param int $minConf
     * @param int $maxConf
     * @return array
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws ListUnspentException
     */
    public function listUnspent(int $minConf, int $maxConf): array
    {
        $data = $this->request('listunspent', [$minConf, $maxConf]);

        if (!is_array($data->result)) {
            throw new ListUnspentException();
        }

        return $data->result;
    }

    /**
     * Check is income transaction.
     * It will be work before money from this transaction no spent.
     *
     * @param string $txid
     * @return bool
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws ListUnspentException
     */
    public function isIncomeTransaction(string $txid): bool
    {
        foreach ($this->listUnspent(0, 999999999) as $tx) {
            if ($tx->txid == $txid) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get income transactions by label.
     *
     * @param string $label
     * @return array
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws ListUnspentException
     * @throws ListTransactionsException
     */
    public function getIncomeTransactions(string $label): array
    {
        $list = [];

        foreach ($this->listTransactions($label) as $transaction) {
            if ($this->isIncomeTransaction($transaction->txid)) {
                $list[] = $transaction;
            }
        }

        return $list;
    }

    /**
     * Get all transactions by label.
     *
     * @return array
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws ListTransactionsException
     */
    public function listTransactions(string $label): array
    {
        $data = $this->request('listtransactions', [$label]);

        if (!is_array($data->result)) {
            throw new ListTransactionsException();
        }

        return $data->result;
    }

    /**
     * Get transaction details by txid.
     *
     * @param string $txid
     * @return StdClass
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws GeTransactionException
     */
    public function getTransaction(string $txid): StdClass
    {
        $data = $this->request('gettransaction', [$txid]);

        if (!($data->result instanceof StdClass)) {
            throw new GeTransactionException();
        }

        return $data->result;
    }

    /**
     * Validate address.
     *
     * @param string $address
     * @return bool
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    public function validateAddress(string $address): bool
    {
        $data = $this->request('validateaddress', [$address]);

        return (isset($data->result->isvalid) && $data->result->isvalid === true);
    }

    /**
     * Send money.
     *
     * @param string $to
     * @param float $amount
     * @return string
     * @throws EstimateSmartFeeException
     * @throws GetBalanceException
     * @throws NotEnoughBalanceException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws SendException
     * @throws SetTxFeeException
     * @throws SendIncorrectSettingsException
     */
    public function sendMoney(string $to, float $amount): string
    {
        $cryptoCurrency = $this->getDataGetterService()
            ->findMainCurrencyByNetwork(BitcoinNetworkEnum::BITCOIN);
        $rate = $cryptoCurrency->market->price;

        if (config('finance.btc.min_commission') <= 0 ||
            config('finance.btc.max_commission') <= 0 ||
            config('finance.btc.min_commission') > config('finance.btc.max_commission') ||
            $rate <= 0
        ) {
            throw new SendIncorrectSettingsException();
        }

        $balance = $this->getBalance();
        $fee = $this->calculateFee(
            config('finance.btc.min_commission'),
            config('finance.btc.max_commission'),
            $rate
        )->feerate;

        if (($balance + $fee + config('finance.btc.reserve')) < $amount) {
            throw new NotEnoughBalanceException();
        }

        $this->setTxFee($fee);

        $data = $this->request('sendtoaddress', [$to, $amount]);

        if (!is_string($data->result) || strlen($data->result) === 0) {
            throw new SendException();
        }

        return $data->result;
    }

    /**
     * Set transaction fee.
     *
     * @param float $amount
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     * @throws SetTxFeeException
     */
    public function setTxFee(float $amount): void
    {
        $data = $this->request('settxfee', [$amount]);

        if ($data->result !== true) {
            throw new SetTxFeeException();
        }
    }

    /**
     * Estimate smart fee.
     *
     * @param int $nBlocks
     * @return float
     * @throws EstimateSmartFeeException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    public function estimateSmartFee(int $nBlocks): float
    {
        $data = $this->request('estimatesmartfee', [$nBlocks]);

        if (!isset($data->result->feerate) || !is_float($data->result->feerate)) {
            throw new EstimateSmartFeeException();
        }

        return $data->result->feerate;
    }

    /**
     * Calculate fee by parameters.
     *
     * @param float $minApproximatelyCommission
     * @param float $maxApproximatelyCommission
     * @param float $rate
     * @return stdClass
     * @throws EstimateSmartFeeException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    public function calculateFee(float $minApproximatelyCommission, float $maxApproximatelyCommission, float $rate): stdClass
    {
        $min = 1;
        $max = 1008;

        $fee = $this->getAndCheckCommission(100, null, $minApproximatelyCommission, $maxApproximatelyCommission, $rate);

        if ($fee->checkStatus) {
            return $fee;
        }

        $amount = $fee->amount;

        if ($amount > $maxApproximatelyCommission) {
            for ($i = 100; $i <= 1000; $i += 10) {
                $fee = $this->getAndCheckCommission($i, true, $minApproximatelyCommission, $maxApproximatelyCommission, $rate);

                if ($fee->checkStatus) {
                    return $fee;
                }
            }

            $fee = $this->getAndCheckCommission($max, true, $minApproximatelyCommission, $maxApproximatelyCommission, $rate);

            if ($fee->checkStatus) {
                return $fee;
            }
        } else if ($amount < $minApproximatelyCommission) {
            for ($i = 100; $i > 0; $i -= 10) {
                $fee = $this->getAndCheckCommission($i, false, $minApproximatelyCommission, $maxApproximatelyCommission, $rate);

                if ($fee->checkStatus) {
                    return $fee;
                }
            }

            $fee = $this->getAndCheckCommission($min, false, $minApproximatelyCommission, $maxApproximatelyCommission, $rate);

            if ($fee->checkStatus) {
                return $fee;
            }
        }

        return $this->getAndCheckCommission($max, null, $minApproximatelyCommission, $maxApproximatelyCommission, $rate);
    }

    /**
     * Get and check commission by parameters.
     *
     * @param int $blocks
     * @param bool|null $add
     * @param float $minApproximatelyCommission
     * @param float $maxApproximatelyCommission
     * @param float $rate
     * @return stdClass
     * @throws EstimateSmartFeeException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    private function getAndCheckCommission(int $blocks, ?bool $add, float $minApproximatelyCommission, float $maxApproximatelyCommission, float $rate): stdClass
    {
        $res = new stdClass();
        $fee = $this->estimateSmartFee($blocks);
        $amount = $fee * $rate;
        $res->feerate = $fee;
        $res->blocks = $blocks;
        $res->amount = $amount;

        if ($add === null && $amount >= $minApproximatelyCommission && $amount <= $maxApproximatelyCommission) {
            $res->checkStatus = true;
        } else if ($add === true && $amount <= $maxApproximatelyCommission) {
            $res->checkStatus = true;
        } else if ($add === false && $amount >= $minApproximatelyCommission) {
            $res->checkStatus = true;
        } else if ($add === false && $blocks === 1 && $amount < $minApproximatelyCommission) {
            $res->checkStatus = true;
        } else {
            $res->checkStatus = false;
        }

        return $res;
    }

    /**
     * Execute request.
     *
     * @param string $method
     * @param array $params
     * @return stdClass
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    private function request(string $method, array $params = []): StdClass
    {
        return $this->requestMulti(
            [['method' => $method, 'params' => $params]],
        )[0];
    }

    /**
     * Execute request.
     *
     * @param array $requests
     * @param array $filter
     * @return array
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    private function requestMulti(array $requests, array $filter = []): array
    {
        return $this->getCryptJsonRpcClientService()
            ->requestMulti(
                $this->key,
                config('finance.btc.rpc.secret'),
                config('finance.btc.rpc.address'),
                $requests,
                [
                    config('finance.btc.rpc.username'),
                    config('finance.btc.rpc.password'),
                ],
                $filter
            );
    }

    /**
     * Load key.
     *
     * @throws KeyNotFoundException
     */
    private function loadKey()
    {
        $file = base_path() . '/.btc.key';

        if (!file_exists($file)) {
            throw new KeyNotFoundException();
        }

        $key = file_get_contents($file);

        if ($key === false) {
            throw new KeyNotFoundException();
        }

        $this->key = $key;
    }

    /**
     * Get CryptJsonRpcClientService.
     *
     * @return CryptJsonRpcClientService
     */
    private function getCryptJsonRpcClientService(): CryptJsonRpcClientService
    {
        return app(CryptJsonRpcClientService::class);
    }

    /**
     * Get {@see DataGetterService::class}.
     *
     * @return DataGetterService
     */
    private function getDataGetterService(): DataGetterService
    {
        return app(DataGetterService::class);
    }
}
