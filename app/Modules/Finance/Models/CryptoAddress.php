<?php
declare(strict_types=1);

namespace App\Modules\Finance\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Finance\Dto\Api\CryptoAddressApiDto;
use App\Modules\Finance\Dto\Api\CryptoAddressMnemonicApiDto;
use App\Modules\Users\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Crypto address model.
 *
 * @property-read int $id
 * @property-read int $crypto_currencies_id
 * @property int $type
 * @property string $address
 * @property string $client_id
 * @property Carbon|null $monitoring_before_at
 * @property int $min_conf
 * @property int $cnt_check
 * @property string $eth_password
 * @property string $mnemonic
 * @property string $access_key
 * @property Carbon|null $last_check_at
 * @property int $users_id
 * @property bool $is_archived
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read CryptoCurrency|null $currency
 * @property-read User|null $user
 * @property-read Collection|CryptoTransaction[] $cryptoTransactions
 * @property-read Collection|CryptoBalance[] $balances
 * @property-read float $summaryUSDBalance
 */
final class CryptoAddress extends Model
{
    public const TYPE_BTC = 1;
    public const TYPE_ETH = 2;
    public const TYPE_LTC = 3;
    public const TYPE_XMR = 4;
    public const TYPE_USDT = 5;
    public const TYPE_BNB = 6;
    public const TYPE_BUSDT = 7;

    public const ERC20_TOKEN_USDT_ADDRESS = '0xdac17f958d2ee523a2206206994597c13d831ec7';

    public const LABEL_BTC = 'BTC';
    public const LABEL_ETH = 'ETH';
    public const LABEL_USDT = 'USDT';
    public const LABEL_BNB = 'BNB';
    public const LABEL_BUSDT = 'BUSDT';

    public const SUPPORTED_CURRENCIES = [
        self::TYPE_BTC => self::LABEL_BTC,
        self::TYPE_ETH => self::LABEL_ETH,
        self::TYPE_USDT => self::LABEL_USDT,
        self::TYPE_BNB => self::LABEL_BNB,
        self::TYPE_BUSDT => self::LABEL_BUSDT,
    ];

    public const SUPPORTED_CURRENCIES_TYPES = [
        self::TYPE_BTC,
        self::TYPE_ETH,
        self::TYPE_USDT,
        self::TYPE_BNB,
        self::TYPE_BUSDT,
    ];

    /** {@inheritdoc} */
    protected $table = 'crypto_addresses';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'crypto_currencies_id' => 'integer',
        'type' => 'integer',
        'address' => 'string',
        'client_id' => 'string',
        'monitoring_before_at' => 'datetime',
        'min_conf' => 'integer',
        'cnt_check' => 'integer',
        'eth_password' => 'encrypted',
        'mnemonic' => 'encrypted',
        'access_key' => 'encrypted',
        'last_check_at' => 'datetime',
        'users_id' => 'integer',
        'is_archived' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Relation with {@see CryptoCurrency::class}.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_currencies_id');
    }

    /**
     * Relation with {@see User::class}.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Get relation with {@see CryptoTransaction::class}..
     *
     * @return HasMany
     */
    public function cryptoTransactions(): HasMany
    {
        return $this->hasMany(CryptoTransaction::class, 'crypto_addresses_id');
    }

    /**
     * Get relation with {@see CryptoBalance::class}..
     *
     * @return HasMany
     */
    public function balances(): HasMany
    {
        return $this->hasMany(CryptoBalance::class, 'crypto_addresses_id');
    }

    /**
     * Get attribute "summaryUSDBalance".
     *
     * @return float
     */
    public function getSummaryUSDBalanceAttribute(): float
    {
        $balance = 0;

        $this->balances
            ->each(function (CryptoBalance $balanceModel) use (&$balance) {
                if ($balanceModel->currency->market) {
                    $balance += $balanceModel->balance * $balanceModel->currency->market->price;
                }
            });

        return $balance;
    }

    /**
     * To api dto.
     *
     * @return CryptoAddressApiDto
     */
    public function toApiDto(): CryptoAddressApiDto
    {
        return new CryptoAddressApiDto(
            $this->id,
            $this->currency->toApiDto(),
            $this->address,
            $this->is_archived,
            $this->balances->map(fn(CryptoBalance $balance) => $balance->toApiDto()),
            $this->user?->toApiDto(),
            $this->last_check_at,
            $this->created_at,
            $this->updated_at,
        );
    }

    /**
     * To mnemonic api dto.
     *
     * @return CryptoAddressMnemonicApiDto
     */
    public function toMnemonicApiDto(): CryptoAddressMnemonicApiDto
    {
        return new CryptoAddressMnemonicApiDto(
            $this->access_key,
            $this->mnemonic,
        );
    }
}
