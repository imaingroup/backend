<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services\V2;

use App\Modules\Finance\Dto\Api\TokenApiDto;
use App\Modules\Finance\Enum\CoingeckoCoinEnum;
use App\Modules\Finance\Enum\CoingeckoNetworkEnum;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Exceptions\Services\BitQuery\RequestException;
use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Finance\Models\CryptoCurrencyMarket;
use App\Modules\Finance\Models\CryptoCurrencyStats;
use App\Modules\Finance\Repositories\CryptoCurrencyRepository;
use App\Modules\Finance\Repositories\CryptoNetworkRepository;
use App\Modules\Finance\Services\V2\Ethereum\TokenListService;
use App\Modules\Finance\Services\V2\Rate\RateService;
use App\Modules\Settings\Models\DepositCurrency;
use App\Modules\Settings\Services\CurrenciesService;

/**
 * Data fetcher service.
 */
final class DataFetcherService
{
    /**
     * Update currencies.
     *
     * @return void
     * @throws RequestException
     */
    public function updateCurrencies(): void
    {
        collect(EthereumNetworkEnum::cases())
            ->each(fn(EthereumNetworkEnum $network) => $this->updateCurrenciesByNetwork($network));
    }

    /**
     * Update currencies by network.
     *
     * @param EthereumNetworkEnum $network
     * @return void
     * @throws RequestException
     */
    public function updateCurrenciesByNetwork(EthereumNetworkEnum $network): void
    {
        $this->getTokenListService()
            ->getTokensByNetwork($network)
            ->filter(function (TokenApiDto $token) {
                return $token->tokenType === 'ERC20' || $token->tokenType === null;
            })
            ->each(fn(TokenApiDto $token) => $this->updateCurrency($token, $network));
    }

    /**
     * Update currencies.
     *
     * @param TokenApiDto $token
     * @param EthereumNetworkEnum $network
     * @return void
     */
    public function updateCurrency(TokenApiDto $token, EthereumNetworkEnum $network): void
    {
        $currency = $this->getCryptoCurrencyRepository()
            ->findByAddressAndNetwork($token->address, $network);

        if (!$currency) {
            $currency = new CryptoCurrency();
            $currency->symbol = $token->symbol;
            $currency->name = $token->name;
            $currency->decimals = $token->decimals;
            $currency->address = $token->address;
            $currency->tokenType = $token->tokenType;
            $currency->network()
                ->associate(
                    $this->getCryptoNetworkRepository()
                        ->findByNetwork($network)
                );

            $currency->save();
        }

        $stats = $currency->stats;

        if (!$stats) {
            $stats = new CryptoCurrencyStats();
            $stats->currency()
                ->associate($currency);
        }

        $stats->count = $token->stats->count;
        $stats->amount = $token->stats->amount;

        $stats->save();
    }

    /**
     * Update currencies market.
     *
     * @return void
     * @throws RequestException
     */
    public function updateCurrenciesMarket(): void
    {
        $this->getCurrenciesService()
            ->getCurrencies()
            ->filter(fn(DepositCurrency $currency) => !$currency->custom_rate)
            ->each(function (DepositCurrency $currency) {
                $this->updateCurrencyMarket($currency->currency);

                if (config('finance.coingecko.requests_delay') > 0) {
                    sleep(config('finance.coingecko.requests_delay'));
                }
            });
    }

    /**
     * Update currency stats.
     *
     * @param CryptoCurrency $currency
     * @return void
     * @throws RequestException
     */
    public function updateCurrencyStats(CryptoCurrency $currency): void
    {
        $token = $currency->address ?
            $this->getTokenListService()
                ->getDetailToken($currency->address, $currency->network->toEnum())
            : $this->getTokenListService()
                ->getDetailMainCurrency($currency->network->toEnum());

        if ($token) {
            $stats = $currency->stats;

            if (!$stats) {
                $stats = new CryptoCurrencyStats();
                $stats->currency()
                    ->associate($currency);
            }

            $stats->count = $token->stats->count;
            $stats->amount = $token->stats->amount;
            $stats->senders = $token->stats->senders;
            $stats->receivers = $token->stats->receivers;
            $stats->days = $token->stats->days;
            $stats->from_date = $token->stats->fromDate;
            $stats->till_date = $token->stats->tillDate;

            $stats->save();
        }
    }

    /**
     * Update currency market.
     *
     * @param CryptoCurrency $currency
     * @return void
     */
    public function updateCurrencyMarket(CryptoCurrency $currency): void
    {
        try {
            $marketData = $currency->address ?
                $this->getRateService()
                    ->getAssetMarketData(
                        CoingeckoNetworkEnum::fromNetwork($currency->network->toEnum()),
                        $currency->address
                    ) :
                $this->getRateService()
                    ->getCoinData(
                        CoingeckoCoinEnum::fromNetwork($currency->network->toEnum()),
                    );
        } catch (RequestException) {
            $marketData = null;
        }

        if ($marketData && (!$currency->depositCurrency || !$currency->depositCurrency->custom_rate)) {
            $market = $currency->market;

            if (!$market) {
                $market = new CryptoCurrencyMarket();
                $market->currency()
                    ->associate($currency);
            }

            $market->image = $marketData->image;
            $market->price = $marketData->usdtPrice;
            $market->price_updated_at = now();

            $market->save();
        }
    }

    /**
     * Set currency market.
     *
     * @param CryptoCurrency $currency
     * @param float $rate
     * @return CryptoCurrencyMarket
     */
    public function setCurrencyMarket(CryptoCurrency $currency, float $rate): CryptoCurrencyMarket
    {
        $market = $currency->market;

        if (!$market) {
            $market = new CryptoCurrencyMarket();
            $market->currency()
                ->associate($currency);
        }

        $market->price = $rate;
        $market->price_updated_at = now();

        $market->save();

        return $market;
    }

    /**
     * Get {@see TokenListService::class}.
     *
     * @return TokenListService
     */
    private function getTokenListService(): TokenListService
    {
        return app(TokenListService::class);
    }

    /**
     * Get {@see RateService::class}.
     *
     * @return RateService
     */
    private function getRateService(): RateService
    {
        return app(RateService::class);
    }

    /**
     * Get {@see CryptoCurrencyRepository::class}.
     *
     * @return CryptoCurrencyRepository
     */
    private function getCryptoCurrencyRepository(): CryptoCurrencyRepository
    {
        return app(CryptoCurrencyRepository::class);
    }

    /**
     * Get {@see CryptoNetworkRepository::class}.
     *
     * @return CryptoNetworkRepository
     */
    private function getCryptoNetworkRepository(): CryptoNetworkRepository
    {
        return app(CryptoNetworkRepository::class);
    }

    /**
     * Get {@see CurrenciesService::class}.
     *
     * @return CurrenciesService
     */
    private function getCurrenciesService(): CurrenciesService
    {
        return app(CurrenciesService::class);
    }
}
