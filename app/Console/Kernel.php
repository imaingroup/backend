<?php
declare(strict_types=1);

namespace App\Console;

use App\Modules\Finance\Commands\BtcMonitoringCommand;
use App\Modules\Finance\Commands\GenerateKeyCommand;
use App\Modules\Finance\Commands\V2\CryptoData\ProcessingAllCommand;
use App\Modules\Finance\Commands\V2\CryptoData\ProcessingCommand;
use App\Modules\Finance\Commands\V2\CryptoData\UpdateBalancesCommand;
use App\Modules\Finance\Commands\V2\DataFetcher\MarketCommand;
use App\Modules\Finance\Commands\V2\DataFetcher\TokensCommand;
use App\Modules\Finance\Commands\V2\Migrations\MigrateCryptoDataToV2Command;
use App\Modules\Finance\Commands\V2\Migrations\MigrateEthereumKeystoreToV2Command;
use App\Modules\Investments\Commands\TradingCommand;
use App\Modules\Investments\Commands\TradingImitatePnlCommand;
use App\Modules\Security\Commands\DetectMultiAccountsCommand;
use App\Modules\Security\Commands\UpdateTimeCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Console kernel.
 */
final class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        DetectMultiAccountsCommand::class,
        BtcMonitoringCommand::class,
        GenerateKeyCommand::class,
        TradingImitatePnlCommand::class,
        TradingCommand::class,
        UpdateTimeCommand::class,
        TokensCommand::class,
        ProcessingCommand::class,
        ProcessingAllCommand::class,
        UpdateBalancesCommand::class,
        MarketCommand::class,
        MigrateCryptoDataToV2Command::class,
        MigrateEthereumKeystoreToV2Command::class,
        DetectMultiAccountsCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('time:update')
            ->everyFiveMinutes()
            ->runInBackground()
            ->withoutOverlapping();

        $schedule->command('queue:work --memory=1024 --timeout=300 --max-time=1440')
            ->everyMinute()
            ->runInBackground()
            ->withoutOverlapping();

        $tradingTimeList = ['00:01', '00:30', '01:30', '02:30', '03:00', '05:00'];

        foreach ($tradingTimeList as $time) {
            $schedule->command('trading:process')
                ->dailyAt($time)
                ->runInBackground()
                ->withoutOverlapping();
        }

        $schedule->command('trading:imitate-pnl')
            ->hourlyAt(0)
            ->runInBackground()
            ->withoutOverlapping();

        $schedule->command('finance:data_processing')
            ->everyThreeMinutes()
            ->runInBackground()
            ->withoutOverlapping();

        $schedule->command('finance:data_update_balances')
            ->everyThreeMinutes()
            ->runInBackground()
            ->withoutOverlapping();

        $schedule->command('finance:data_fetcher_market')
            ->everyTenMinutes()
            ->runInBackground()
            ->withoutOverlapping();

        if (config('finance.btc.enable_monitoring')) {
            $schedule->command('finance:btc_monitoring')
                ->everyMinute()
                ->runInBackground()
                ->withoutOverlapping();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
