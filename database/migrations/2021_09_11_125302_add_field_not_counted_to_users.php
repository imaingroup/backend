<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Add field not counted to users.
 */
final class AddFieldNotCountedToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('not_counted')
                ->default(false);
        });

        DB::transaction(function () {
            $typeId = DB::table('data_types')
                ->where('name', 'users')
                ->first()
                ->id;

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'not_counted',
                    'type' => 'checkbox',
                    'display_name' => 'Not counted account',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 74
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('not_counted');
        });

        DB::transaction(function() {
            $typeId = DB::table('data_types')
                ->where('name', 'users')
                ->first()
                ->id;

            DB::table('data_rows')
                ->where('data_type_id', $typeId)
                ->whereIn('field', [
                    'not_counted',
                ])
                ->delete();
        });
    }
}
