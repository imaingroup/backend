<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation clean messages request.
 */
final class CleanMessagesRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Messages\Services\MessageService $messageService
     */
    public function __construct(
        private \App\Modules\Messages\Services\MessageService $messageService
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user !== null
            && $this->messageService->checkAccessChat($user, $this->chat);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape(['chat' => "App\\Modules\\Messages\\Models\\Chat"])]
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'chat' => $this->chat
        ];
    }
}
