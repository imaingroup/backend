<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Requests;

use App\Modules\Finance\Enum\NetworkTypeEnum;
use App\Modules\Settings\Services\CurrenciesService;
use App\Modules\Users\Models\User;
use App\Modules\Withdraw\Models\Withdraw;
use App\Modules\Withdraw\Services\WithdrawService;
use App\Modules\Settings\Repositories\SettingsDepositRepository;
use App\Modules\Users\Services\UserService;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;
use Merkeleon\PhpCryptocurrencyAddressValidation\Validation;
use Throwable;
use function sprintf;

/**
 * This class validates withdraw content request.
 */
final class WithdrawCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'currency' => "array",
        'address' => "string",
        'amount_usdt' => "string",
        'accepted_rules' => "string",
        '2fa_code' => "string"
    ])]
    public function rules(): array
    {
        return [
            'currency' => 'required|integer|exists:deposit_currencies,crypto_currencies_id',
            'address' => 'required|scalar',
            'amount_usdt' => 'required|numeric|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'accepted_rules' => 'scalar|accepted',
            '2fa_code' => 'required|scalar'
        ];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape([
        'user' => User::class,
        'currency' => "integer",
        'address' => "string",
        'amount' => "float"
    ])]
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'currency' => $this->getCurrenciesService()
                ->find((int)$this['currency']),
            'address' => $this['address'],
            'amount' => (float)$this['amount_usdt'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function (Validator $validator) {
            if (count($validator->failed()) === 0) {
                $this->after($validator);
            }
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @throws Exception
     */
    private function after(Validator $validator): void
    {
        $user = Auth::user();
        $settings = $this->getSettingsDepositRepository()->get();
        $currency = $this->getCurrenciesService()
            ->find((int)$this['currency']);

        if (!$user->enabled_force_withdraw && $settings->is_withdraw_block) {
            $validator->errors()->add('global', 'The withdrawal feature is temporarily disabled');
            return;
        }

        if ($user->is_withdraw_block) {
            $validator->errors()->add('global', 'The withdrawal feature is temporarily disabled for this account');
            return;
        }

        if (!$user->is_2fa) {
            $validator->errors()->add('2fa_code', 'Please activate 2FA');
            return;
        }

        if (!$this->getUserService()->check2fa($user, $this['2fa_code'])) {
            $validator->errors()->add('2fa_code', 'Incorrect 2FA');
            return;
        }

        $amountUsdt = (float)$this['amount_usdt'];

        if ($settings->disabled_dividends_withdraw && $user->deposited_usdt < $amountUsdt) {
            $validator->errors()->add('amount_usdt', 'Insufficient USDT funds');
            return;
        } else if (!$settings->disabled_dividends_withdraw && $user->balance_usdt < $amountUsdt) {
            $validator->errors()->add('amount_usdt', 'Insufficient USDT funds');
            return;
        }

        if ($this->getWithdrawService()->calculateWithdrawLimit($user) < $amountUsdt) {
            $validator->errors()->add('amount_usdt', 'Withdrawal amount limit is exceeded');
            return;
        }

        if ($amountUsdt < Withdraw::MINIMUM_AMOUNT_USDT) {
            $validator->errors()->add('amount_usdt', sprintf('Min. %s USDT is allowed to withdraw', Withdraw::MINIMUM_AMOUNT_USDT));
            return;
        }

        if ($currency->currency->network->toEnum()->type() === NetworkTypeEnum::BITCOIN) {
            try {
                $validator = Validation::make('BTC');
                $validator->validate($this['address']);
            } catch (Throwable) {
                $validator->errors()
                    ->add('address', 'For this payment required correct BTC address.');
            }
        } else if ($currency->currency->network->toEnum()->type() === NetworkTypeEnum::ETHEREUM) {
            try {
                $validator = Validation::make('ETH');
                $validator->validate($this['address']);
            } catch (Throwable) {
                $validator->errors()
                    ->add('address', sprintf('For this payment required correct %s address.', $currency->currency->market->coingeckoNetworkEnum->value));
            }
        }
    }

    /**
     * Get SettingsDepositRepository.
     *
     * @return SettingsDepositRepository
     */
    private function getSettingsDepositRepository(): SettingsDepositRepository
    {
        return app(SettingsDepositRepository::class);
    }

    /**
     * Get UserService.
     *
     * @return UserService
     */
    private function getUserService(): UserService
    {
        return app(UserService::class);
    }

    /**
     * Get WithdrawService.
     *
     * @return WithdrawService
     */
    private function getWithdrawService(): WithdrawService
    {
        return app(WithdrawService::class);
    }

    /**
     * Get {@see CurrenciesService::class}.
     *
     * @return CurrenciesService
     */
    private function getCurrenciesService(): CurrenciesService
    {
        return app(CurrenciesService::class);
    }
}
