<?php

namespace App\Modules\Access\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Access\Requests\ResetPasswordRequest;
use App\Modules\Users\Models\User;
use App\Modules\Access\Services\ResetPasswordService;

/**
 * Reset password controller.
 */
final class ResetPasswordController extends Controller
{
    /** @var ResetPasswordService Reset password service. */
    private ResetPasswordService $resetPasswordService;

    /**
     * Construct.
     *
     * @param ResetPasswordService $resetPasswordService
     */
    public function __construct(ResetPasswordService $resetPasswordService)
    {
        $this->resetPasswordService = $resetPasswordService;
    }

    /**
     * Processing form with new password.
     *
     * @param User $user
     * @param ResetPasswordRequest $request
     * @return array
     */
    public function send(User $user, ResetPasswordRequest $request): array
    {
        $this->resetPasswordService->changePassword($user, $request->validated()['password']);

        return ['status' => true];
    }
}
