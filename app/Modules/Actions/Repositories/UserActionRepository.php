<?php
declare(strict_types=1);

namespace App\Modules\Actions\Repositories;

use App\Modules\Users\Models\User;
use App\Modules\Actions\Models\UserAction;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;

final class UserActionRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param UserAction $model
     */
    public function __construct(protected UserAction $model)
    {}

    /**
     * Get last activities.
     *
     * @param int $limit
     * @param array|null $actions
     * @return Collection
     */
    public function getLast(int $limit, array $actions = null): Collection
    {
        if ($actions !== null && count($actions) === 0) {
            return collect();
        }

        $query = $this->model
            ->newQuery()
            ->select('user_actions.*')
            ->join('users', 'user_actions.users_id', '=', 'users.id')
            ->where('users.not_counted', false)
            ->orderBy('user_actions.id', 'DESC')
            ->limit($limit);

        if ($actions !== null && count($actions) > 0) {
            $query->whereIn('user_actions.action', $actions);
        }

        return $query
            ->get();
    }

    /**
     * Get user activities.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return Collection
     */
    public function getByUser(User $user): Collection
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $user->id)
            ->orderBy('id', 'DESC')
            ->get();
    }
}
