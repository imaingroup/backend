<?php

namespace App\Modules\Users\Requests;

use App\Modules\Users\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Throwable;

/**
 * This class need for validating profile account save request.
 */
final class ProfileAccountSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $user = Auth::user();

        return [
            'username' => \sprintf(
                'required|scalar|unique:users,username,%s,username|max:255|regex:/^[a-zA-Z0-9]+$/',
                addslashes($user->username)
            ),
            'avatar' => 'nullable|image|dimensions:max_width=800,max_height=800|mimes:jpeg,jpg,png|max:1024',
            'country' => [
                'nullable',
                'scalar',
                Rule::in(array_keys(config('country')))
            ],
            'birthdate' => 'nullable|scalar|date',
            'gender' => [
                'nullable',
                'scalar',
                Rule::in([User::GENDER_MALE, User::GENDER_FEMALE])
            ],
            'bio' => 'nullable|scalar|max:5000',
            'is_trader' => [
                'nullable',
                'scalar',
                Rule::in(['0', '1'])
            ],
            'is_hide_from_public' => [
                'nullable',
                'scalar',
                Rule::in(['0', '1'])
            ],
            'btc_address' => 'nullable|scalar|bitcoin',
            'eth_address' => 'nullable|scalar|ethereum',
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator): void
    {
        if ($this['birthdate'] && is_scalar($this['birthdate'])) {
            try {
                $birthdate = new \DateTime($this['birthdate']);

                if ($birthdate < new \DateTime('01-01-1900') || $birthdate > (new \DateTime())->modify('- 18 YEARS')) {
                    $validator->errors()->add('birthdate', 'Incorrect date of birth');
                    return;
                }
            }
            catch (Throwable $e) {}
        }
    }
}
