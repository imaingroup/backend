<?php
declare(strict_types=1);

namespace App\Modules\Settings\Models;

use App\Modules\Core\Models\Model;
use Carbon\Carbon;

/**
 * Setting imperium.
 *
 * @property-read int $id
 * @property string $download_win_url
 * @property string $download_mac_url
 * @property string $version
 * @property float $rate_previous
 * @property float $rate
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
final class SettingsImperium extends Model
{
    /** {@inheritdoc}  */
    protected $table = 'settings_imperium';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'download_win_url' => 'string',
        'download_mac_url' => 'string',
        'version' => 'string',
        'rate_previous' => 'float',
        'rate' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
