@extends('voyager::bread-base.read')

@section('append_view_content')
    <style>
        table .accurate, table .accurate a {
            color: green;
        }

        table .high_probably, table .high_probably a {
            color: red;
        }

        table .accurate a, table .high_probably a {
            text-decoration: none;
        }

        table .accurate a:hover, table .high_probably a:hover {
            text-decoration: underline;
        }

        table .line-through-text {
            text-decoration: line-through;
        }
    </style>
    <div>
        <hr style="margin:0;"/>
        <div class="panel-heading" style="border-bottom:0;">
            <h3 class="panel-title">Proft card history</h3>
        </div>
        <div class="panel-body" style="padding-top:0;">
            <table class="table">
                <tr>
                    <th>Purchased ID</th>
                    <th>Card</th>
                    <th>Purchased at</th>
                    <th>Transaction ID</th>
                    <th>Deatails</th>
                </tr>
                @foreach($dataTypeContent->profitCards as $card)
                    <tr>
                        <td>
                            <a href="{{ sprintf('/%s/profit-card-buy-histories/%s', config('voyager.prefix'), $card->id) }}"
                               target="_blank">{{ $card->id }}</a>
                        </td>
                        <td>
                            <a href="{{ sprintf('/%s/profit-cards/%s', config('voyager.prefix'), $card->card->id) }}"
                               target="_blank">{{ $card->card->id }}: {{ $card->card->title }}</a>
                        </td>
                        <td>
                            {{ $card->created_at }}
                        </td>
                        <td>
                            <a href="{{ sprintf('/%s/transactions/%s', config('voyager.prefix'), $card->transaction->id) }}"
                               target="_blank">{{ $card->transaction->id }}</a>
                        </td>
                        <td>
                            @if($card->transaction->parent)
                                Withdrawn: {{ $card->transaction->parent->amount }} {{ $card->transaction->parent->currencyReadable }}
                                ,
                            @endif

                            Added: {{ $card->transaction->amount }} {{ $card->transaction->currencyReadable }}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="5">No profit card history.</td>
                </tr>
            </table>
        </div>
    </div>
    <div id="extend_user_info">
        <template>
            <hr style="margin:0;"/>
            <div class="panel-heading" style="border-bottom:0;">
                <h3 class="panel-title">Related accounts</h3>
            </div>
            <div class="panel-body" style="padding-top:0;">
                <table class="table"
                       v-if="(relatedAccurate.length > 0) || (relatedProbably.length > 0)">
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Creation date</th>
                        <th>Last login</th>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-decoration: underline;font-weight: bold">Accurate</td>
                    </tr>
                    <tr v-for="user in relatedAccurate" class="accurate">
                        <td>
                            <a v-bind:href="'{{ sprintf('/%s/users/', config('voyager.prefix')) }}'+user.id"
                               target="_blank">@{{ user.id }}</a>
                        </td>
                        <td>
                            <a v-bind:href="'{{ sprintf('/%s/users/', config('voyager.prefix')) }}'+user.id"
                               target="_blank">@{{ user.username }}</a>
                        </td>
                        <td>
                            <a v-bind:href="'{{ sprintf('/%s/users/', config('voyager.prefix')) }}'+user.id"
                               target="_blank">@{{ user.email }}</a>
                        </td>
                        <td>@{{ dateFormat(user.createdAt) }}</td>
                        <td>@{{ dateFormat(user.lastLoginAt) }}</td>
                    </tr>
                    <tr>
                        <td colspan="5" v-if="relatedAccurate.length === 0">No related
                            accounts.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-decoration: underline;font-weight: bold">Probably</td>
                    </tr>
                    <tr v-for="user in relatedProbably" class="high_probably">
                        <td>
                            <a v-bind:href="'{{ sprintf('/%s/users/', config('voyager.prefix')) }}'+user.id"
                               target="_blank">@{{ user.id }}</a>
                        </td>
                        <td>
                            <a v-bind:href="'{{ sprintf('/%s/users/', config('voyager.prefix')) }}'+user.id"
                               target="_blank">@{{ user.username }}</a>
                        </td>
                        <td>
                            <a v-bind:href="'{{ sprintf('/%s/users/', config('voyager.prefix')) }}'+user.id"
                               target="_blank">@{{ user.email }}</a>
                        </td>
                        <td>@{{ dateFormat(user.createdAt) }}</td>
                        <td>@{{ dateFormat(user.lastLoginAt) }}</td>
                    </tr>
                    <tr>
                        <td colspan="5" v-if="relatedProbably.length === 0">No
                            related accounts.
                        </td>
                    </tr>
                </table>
            </div>

            <hr style="margin:0;"/>
            <div class="panel-heading" style="border-bottom:0;">
                <h3 class="panel-title">History</h3>
            </div>
            <div class="panel-body" style="padding-top:0;">
                <table class="table" v-if="data.history && data.history.length > 0">
                    <tr>
                        <th>ID</th>
                        <th>Action</th>
                        <th>Email or username</th>
                        <th>IP</th>
                        <th>Country</th>
                        <th>Browser</th>
                        <th>Platform</th>
                        <th>Device</th>
                        <th>Resolution</th>
                        <th>Time zone</th>
                        <th>Languages</th>
                        <th>Is accurate</th>
                        <th>Datetime</th>
                    </tr>
                    <tr v-for="(historyItem, index) in data.history" v-if="index < showHistory"
                        v-bind:class="{accurate: historyItem.is_accurate, high_probably: historyItem.is_high_probably}">
                        <td>@{{ historyItem.id }}</td>
                        <td>@{{ toReadableAction(historyItem.action) }}</td>
                        <td v-bind:class="{'line-through-text': !historyItem.usersId}">
                            <a v-bind:href="'{{ sprintf('/%s/users/', config('voyager.prefix')) }}'+historyItem.usersId"
                               v-if="historyItem.usersId" target="_blank">@{{ historyItem.userId }}</a>
                            <span v-else>@{{ historyItem.userId }}</span>
                        </td>
                        <td>
                            <a href="" @click.prevent="viewInfoByIp(historyItem.ip)">@{{ historyItem.ip }}</a>
                        </td>
                        <td>@{{ historyItem.country }}</td>
                        <td>@{{ historyItem.browser }}: @{{ historyItem.browserMajorVersion }}</td>
                        <td>@{{ historyItem.platform }}: @{{ historyItem.osVersion }}</td>
                        <td>
                            <span v-if="historyItem.device">
                                @{{ historyItem.device }}/@{{ historyItem.deviceType }}/@{{ historyItem.deviceVendor }}
                            </span>
                            <span v-else-if="historyItem.deviceType">
                                @{{ historyItem.deviceType }}
                            </span>
                            <span v-else>
                                N/A
                            </span>
                        </td>
                        <td>@{{ historyItem.currentResolution }}</td>
                        <td>@{{ historyItem.timeZone }}</td>
                        <td>@{{ historyItem.languages }}</td>
                        <td>@{{ historyItem.isAccurate ? 'Yes' : 'No' }}</td>
                        <td>@{{ dateFormat(historyItem.createdAt) }}</td>
                    </tr>
                    <tr v-if="showHistory < data.history.length">
                        <td colspan="13">
                            <button class="btn btn-sm btn-warning" @click.prevent="showHistory += 100">
                                <span class="hidden-xs hidden-sm">Show more</span>
                            </button>
                            <button class="btn btn-sm btn-warning" @click.prevent="showHistory = data.history.length">
                                <span class="hidden-xs hidden-sm">Show all</span>
                            </button>
                        </td>
                    </tr>
                </table>
                <span v-if="!data.history || data.history.length === 0">No history data.</span>
            </div>

            <hr style="margin:0;"/>
            <div class="panel-heading" style="border-bottom:0;">
                <h3 class="panel-title">Related ips</h3>
            </div>
            <div class="panel-body" style="padding-top:0;">
                <table class="table"
                       v-if="data.ips && data.ips.length > 0">
                    <tr>
                        <th>IP</th>
                        <th>Is accurate</th>
                        <th>First date</th>
                    </tr>
                    <tr v-for="ip in data.ips"
                        v-bind:class="{accurate: ip.isAccurate, high_probably: !ip.isAccurate}">
                        <td>
                            <a href="" @click.prevent="viewInfoByIp(ip.ip)">@{{ ip.ip }}</a>
                        </td>
                        <td>@{{ ip.isAccurate ? 'Yes' : 'No' }}</td>
                        <td>@{{ dateFormat(ip.firstDate) }}</td>
                    </tr>
                </table>
                <span
                    v-if="!data.ips || data.ips.length === 0">No related ips.</span>
            </div>

            <hr style="margin:0;"/>
            <div class="panel-heading" style="border-bottom:0;">
                <h3 class="panel-title">Related browser/devices</h3>
            </div>
            <div class="panel-body" style="padding-top:0;">
                <table class="table"
                       v-if="data.browsers && data.browsers.length > 0">
                    <tr>
                        <th>User agent</th>
                        <th>Is accurate</th>
                        <th>First date</th>
                    </tr>
                    <tr v-for="userAgent in data.browsers"
                        v-bind:class="{accurate: userAgent.isAccurate, high_probably: !userAgent.isAccurate}">
                        <td>@{{ userAgent.browser }}</td>
                        <td>@{{ userAgent.isAccurate ? 'Yes' : 'No' }}</td>
                        <td>@{{ dateFormat(userAgent.firstDate) }}</td>
                    </tr>
                </table>
                <span
                    v-if="!data.browsers || data.browsers.length === 0">No related browser or devices.</span>
            </div>
            <v-dialog/>
        </template>
    </div>
@endsection

@push('javascript')
    <script>
        new Vue({
            el: '#extend_user_info',
            data: () => {
                return {
                    data: {},
                    readableActions: {!! json_encode(\App\Modules\Security\Models\UserIdentification::ACTIONS)  !!},
                    showHistory: 100,
                }
            },
            computed: {
                relatedAccurate() {
                    return this.data.relatedUsers ?
                        this.data.relatedUsers.filter((user) => user.isAccurate) : [];
                },
                relatedProbably() {
                    return this.data.relatedUsers ?
                        this.data.relatedUsers.filter((user) => !user.isAccurate) : [];
                },
            },
            methods: {
                load() {
                    axios.get('{{ route('voyager.users.identifications.get', $dataTypeContent->id) }}').then((r) => {
                        this.data = r.data.data;
                    }).catch((e) => {
                        toastr.error('Server error.', 'Get identifications data.');
                    });
                },
                toReadableAction(action) {
                    return this.readableActions[action];
                },
                dateFormat(date) {
                    var options = {
                        weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                        hour: 'numeric',
                        minute: 'numeric',
                        second: 'numeric'
                    };
                    return (new Date(date)).toLocaleDateString("en-US", options)
                },
                viewInfoByIp(ip) {
                    viewInfoByIp(ip);
                },
            },
            created() {
                this.load();
            },
        });


        function viewInfoByIp(ip) {
            window.open('{{ route('voyager.users.ip.info') }}?ip=' + ip, 'Ip info', 'width=600,height=400,scrollbars=yes');
        }

        new function () {
            const lastIpTag = $("h3:contains('Last Ip')")
                .parent()
                .next()
                .find('p');
            const ipText = lastIpTag.text().trim();

            lastIpTag.html('<a href="" onclick="viewInfoByIp(\'' + ipText + '\'); return false;">' + ipText + '</a>');
        }
    </script>
@endpush
