<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * GetHighestBlockNumberException.
 */
final class GetHighestBlockNumberException extends EthereumException
{
}
