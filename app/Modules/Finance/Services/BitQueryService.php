<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services;

use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Exceptions\Services\BitQuery\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use stdClass;
use Throwable;
use function sprintf;

/**
 * Bit query service.
 */
final class BitQueryService
{
    private const CONNECT_TIMEOUT = 5;
    private const READ_TIMEOUT = 100;
    private const ENDPOINT = 'https://graphql.bitquery.io';

    /**
     * Get income transfers.
     *
     * @param EthereumNetworkEnum $network
     * @param string $address
     * @return stdClass
     * @throws RequestException
     */
    public function getIncomeTransfers(EthereumNetworkEnum $network, string $address): stdClass
    {
        $query = <<<QUERY
        {
  ethereum(network: %s) {
    transfers(receiver: {is: "%s"}, options: {desc: "block.height"}) {
      block {
        height
      }
      transaction {
        hash
        txFrom {
          address
        }
      }
      amount
      currency {
        tokenType
        address
        symbol
      }
    }
  }
}
QUERY;

        return $this->request(sprintf($query, $network->value, $address));
    }

    /**
     * Get the highest block.
     *
     * @param EthereumNetworkEnum $network
     * @return stdClass
     * @throws RequestException
     */
    public function getHighestBlock(EthereumNetworkEnum $network): stdClass
    {
        $query = <<<QUERY
{
  ethereum(network: %s) {
    blocks(options: {limit: 1, desc: "height"}) {
      height
    }
  }
}

QUERY;

        return $this->request(sprintf($query, $network->value));
    }

    /**
     * Get top tokens.
     *
     * @param EthereumNetworkEnum $network
     * @param int $limit
     * @param bool $fullStats
     * @return stdClass
     * @throws RequestException
     */
    public function getTopTokens(EthereumNetworkEnum $network, int $limit, bool $fullStats = false): stdClass
    {
        $statsFields = $fullStats ? <<<STATS
senders: count(uniq: senders)
receivers: count(uniq: receivers)
days: count(uniq: dates)
from_date: minimum(of: date)
till_date: maximum(of: date)
STATS: '';


        $query = <<<QUERY
{
  ethereum(network: %s) {
    transfers(options: {desc: "count", limit: %s}) {
      currency {
        decimals
        name
        symbol
        address
        tokenType
      }
      count
      amount
      %s
    }
  }
}

QUERY;

        return $this->request(sprintf($query, $network->value, $limit, $statsFields));
    }

    /**
     * Get detail token.
     *
     * @param string $address
     * @param EthereumNetworkEnum $network
     * @return stdClass
     * @throws RequestException
     */
    public function getDetailToken(string $address, EthereumNetworkEnum $network): stdClass
    {
        $query = <<<QUERY
{
  ethereum(network: %s) {
    transfers {
      currency(currency: {is: "%s"}) {
        decimals
        name
        symbol
        address
        tokenType
      }
      count
      senders: count(uniq: senders)
      receivers: count(uniq: receivers)
      days: count(uniq: dates)
      from_date: minimum(of: date)
      till_date: maximum(of: date)
      amount
    }
  }
}

QUERY;

        return $this->request(sprintf($query, $network->value, $address));
    }

    /**
     * Execute request.
     *
     * @param string $query
     * @return stdClass|array
     * @throws RequestException
     */
    private function request(string $query): StdClass|array
    {
        $log = [];

        $log['code'] = null;

        try {
            $client = new Client();

            $body = [
                'allow_redirects' => false,
                'connect_timeout' => self::CONNECT_TIMEOUT,
                'read_timeout' => self::READ_TIMEOUT,
                'on_stats' => function (TransferStats $stats) use (&$log) {
                    $log['stats'] = $stats->getHandlerStats();

                    if ($stats->hasResponse()) {
                        $log['code'] = $stats->getResponse()->getStatusCode();
                        $log['response']['headers'] = $stats->getResponse()->getHeaders();
                        $log['response']['content'] = json_decode($stats->getResponse()->getBody()->__toString(), true);
                    }
                }
            ];

            $body['body'] = json_encode(['query' => $query]);
            $body['headers'] = [
                'Content-Type' => 'application/json',
                'X-API-KEY' => config('finance.bitquery.graphql.endpoint')
            ];

            $log['request'] = $body;
            unset($log['request']['on_stats']);

            $request = $client->request('POST', self::ENDPOINT, $body);

            if ($request->getStatusCode() == 200) {
                return json_decode($request->getBody()->__toString());
            } else {
                throw new RequestException('Status code not equal 200');
            }
        } catch (Throwable $e) {
            throw new RequestException($e->getMessage(), 0, $e);
        }
    }
}
