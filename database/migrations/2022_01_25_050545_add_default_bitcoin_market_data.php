<?php
declare(strict_types=1);

use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * AddDefaultBitcoinMarketData.
 */
final class AddDefaultBitcoinMarketData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::transaction(function () {
            $btcId = DB::table('crypto_currencies')
                ->where([
                    'crypto_networks_id' => DB::table('crypto_networks')
                        ->where('code', BitcoinNetworkEnum::BITCOIN->value)
                        ->first()
                        ->id,
                    'symbol' => 'BTC',
                ])
                ->whereNull('address')
                ->first()
                ->id;

            if (!DB::table('crypto_currencies_market')
                ->where('crypto_currencies_id', $btcId)
                ->exists()) {
                DB::table('crypto_currencies_market')
                    ->insert([
                        'crypto_currencies_id' => $btcId,
                        'price' => 35952,
                        'price_updated_at' => now(),
                    ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        //
    }
}
