<?php
declare(strict_types=1);

namespace App\Modules\Core\Validation;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Validation\Validator;

/**
 * CountWords.
 */
final class CountWords implements Rule
{
    public const NAME = 'count_words';
    private int $countWords;

    /**
     * Constructor.
     *
     * @param int $countWords
     */
    public function __construct(int $countWords = 12)
    {
        $this->countWords = $countWords;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, mixed $value): bool
    {
        return str_word_count($value) === $this->countWords;
    }

    /**
     * Validate.
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     * @param Validator $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        if (isset($parameters[0]) && is_numeric($parameters[0])) {
            $this->countWords = (int)$parameters[0];
        }

        return $this->passes($attribute, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'The :attribute must be exactly ' . $this->countWords . ' words.';
    }
}
