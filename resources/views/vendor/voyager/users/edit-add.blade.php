@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::bread.edit-add')

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            <div class="form-group col-md-12"
                                 style="padding-left: 0;padding-right: 0; border: 1px dashed #BCBCBF;">
                                <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                    <h2>Edit user</h2>
                                </div>
                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                    @php
                                        $display_options = $row->details->display ?? NULL;
                                        if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                            $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                        }
                                    @endphp
                                    @if (isset($row->details->legend) && isset($row->details->legend->text))
                                        <legend class="text-{{ $row->details->legend->align ?? 'center' }}"
                                                style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                    @endif

                                    <div
                                        class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 6 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label class="control-label"
                                               for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')

                                        @if (isset($row->details->view))
                                            @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                        @elseif ($row->type == 'relationship')
                                            @include('voyager::formfields.relationship', ['options' => $row->details])
                                        @else
                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach

                                        @if ($errors->has($row->field))
                                            @foreach ($errors->get($row->field) as $error)
                                                <span class="help-block">{{ $error }}</span>
                                            @endforeach
                                        @endif
                                    </div>
                                @endforeach
                            </div>

                            @if($edit)
                                <div class="form-group col-md-12 custom_tid_options"
                                     style="padding-left: 0;padding-right: 0; border: 1px dashed #BCBCBF;">
                                    <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                        <h2>Custom TID options</h2>
                                    </div>

                                    @foreach($options as $option)
                                        <div class="col-md-3 custom_tid_options_form">
                                            <label class="control-label"
                                                   for="name">Option {{ $option->id }}</label>

                                            <div>
                                                <input type="text" class="form-control" step="1" min="0"
                                                       name="custom_tids[{{ $option->id }}][title]"
                                                       placeholder="Title"
                                                       maxlength="255"
                                                       value="{{ $dataTypeContent->getCustomTidOptionsByOption($option)->title ?? "" }}"/>
                                            </div>
                                            <div>
                                                <input type="number" class="form-control" step="1" min="0"
                                                       name="custom_tids[{{ $option->id }}][period_min]"
                                                       placeholder="Period (min)"
                                                       value="{{ $dataTypeContent->getCustomTidOptionsByOption($option)->period_min ?? "" }}"/>
                                            </div>
                                            <div>
                                                <input type="number" class="form-control" step="1" min="0"
                                                       name="custom_tids[{{ $option->id }}][period_max]"
                                                       placeholder="Period (max)"
                                                       value="{{ $dataTypeContent->getCustomTidOptionsByOption($option)->period_max ?? "" }}"/>
                                            </div>
                                            <div>
                                                <input type="number" class="form-control" step="1" min="0"
                                                       name="custom_tids[{{ $option->id }}][amount_min]"
                                                       placeholder="Amount (min)"
                                                       value="{{ $dataTypeContent->getCustomTidOptionsByOption($option)->amount_min ?? "" }}"/>
                                            </div>
                                            <div>
                                                <input type="number" class="form-control" step="1" min="0"
                                                       name="custom_tids[{{ $option->id }}][amount_max]"
                                                       placeholder="Amount (max)"
                                                       value="{{ $dataTypeContent->getCustomTidOptionsByOption($option)->amount_max ?? "" }}"/>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <div class="form-group col-md-12"
                                     style="padding-left: 0;padding-right: 0; border: 1px dashed #BCBCBF;">
                                    <div id="container-investements-options" class="col-md-6"
                                         style="padding-left: 0;padding-right: 0;">
                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                            <h2>Investments options</h2>
                                        </div>

                                        @foreach($options as $option)
                                            <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                                <label class="control-label"
                                                       for="name">Enable {{ $option->title }}</label>
                                                <br>
                                                <input type="checkbox" name="is_active_option[{{ $option->id }}]"
                                                       class="toggleswitch"
                                                       @if($dataTypeContent->checkAllowedOption($option))checked=""@endif>
                                            </div>

                                            <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                                <label class="control-label"
                                                       for="name">{{ $option->title }}</label>

                                                <input type="number" class="form-control" step="1" min="0"
                                                       name="options_expanses[{{ $option->id }}]"
                                                       value="{{ $dataTypeContent->getTidExpanseByOption($option)->tid_expanse ?? 0 }}"/>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div id="container-send-abc" class="form-group col-md-6"
                                         style="padding-left: 0;padding-right: 0;">
                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                            <h2>Money transfer</h2>
                                        </div>

                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                            <label class="control-label" for="name">Available: <span
                                                    id="transfer_balance_abc">{{ $dataTypeContent->balance_abc }}</span>
                                                ABC</label><br/>

                                            <input type="hidden" name="transfer_from_id"
                                                   value="{{ $dataTypeContent->id }}"/>
                                            <input type="number" name="transfer_amount" value="" placeholder="Сумма"
                                                   min="0.00000001" step="0.00000001"
                                                   max="{{ $dataTypeContent->balance_abc }}"/>
                                            <input type="text" name="transfer_promo_code" value=""
                                                   placeholder="Промо код"/>

                                            <div class="text-error transfer_from_id" style="display: none"></div>
                                            <div class="text-error transfer_amount" style="display: none"></div>
                                            <div class="text-error transfer_promo_code" style="display: none"></div>

                                            <input type="button" id="transfer_send" style="margin-top: 15px"
                                                   value="Transfer" class="form-control"/>
                                        </div>
                                    </div>

                                    @if($dataTypeContent->id === Auth::user()->id)
                                        <div id="container-2fa" class="form-group col-md-6"
                                             style="padding-left: 0;padding-right: 0;">
                                            <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                                <hr/>
                                                <h2>2FA</h2>
                                            </div>

                                            <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                                <div id="2fa_status"
                                                     @if(!$dataTypeContent->is_2fa)style="display: none" @endif><span>Already configured</span>
                                                </div>

                                                <div>
                                                    <input type="text" readonly id="2fa_code_view"
                                                           style="display: none;margin: 0 auto"/>
                                                    <div id="2fa_qrcode_view"
                                                         style="display: none;margin: 0 auto"></div>
                                                    <input type="button" id="2fa_init_confirm" value="Confirm"
                                                           style="display: none;margin: 0 auto"/>
                                                </div>

                                                <div class="text-error google2fa_status" style="display: none"></div>

                                                <input type="button" id="2fa_init_enabling" style="margin-top: 15px"
                                                       value="{{ $dataTypeContent->is_2fa ? 'Update' : 'Configure' }}"
                                                       class="form-control"/>
                                            </div>
                                        </div>
                                    @endif

                                    <div id="container-send-abc" class="form-group col-md-6"
                                         style="padding-left: 0;padding-right: 0;">
                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                            <hr/>
                                            <h2>Password change</h2>
                                        </div>

                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">

                                            <div id="container_change_password" style="display: none">
                                                <input type="password" name="password" placeholder="Password">
                                                <input type="password" name="password_confirmation"
                                                       placeholder="Password confirmation">
                                                <input type="button" id="button_change_password" value="Save">

                                                <div class="text-error password" style="display: none"></div>
                                            </div>

                                            <input type="button" id="change_password" style="margin-top: 15px"
                                                   value="Change password" class="form-control"/>
                                        </div>
                                    </div>

                                    <div id="container-change-balance" class="form-group col-md-6"
                                         style="padding-left: 0;padding-right: 0;">
                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                            <hr/>
                                            <h2>Change balance</h2>
                                        </div>

                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                            <div>If you want to deposit money, please enter a positive number. If you
                                                want
                                                to write off money, enter a negative number.
                                            </div>
                                            <br/>
                                            <label class="control-label" for="name">
                                                Available: <span
                                                    id="change_balance_available_abc">{{ $dataTypeContent->balance_abc }}</span>
                                                ABC</label>&nbsp;
                                            Available: <span
                                                id="change_balance_available_deposited_usdt">{{ $dataTypeContent->deposited_usdt }}</span>
                                            USDT (Deposited)</label>
                                            Available: <span
                                                id="change_balance_available_dividents_usdt">{{ $dataTypeContent->dividents_usdt }}</span>
                                            USDT (Dividents)</label>
                                            <br/>

                                            <input type="hidden" name="change_balance_user_id"
                                                   value="{{ $dataTypeContent->id }}"/>
                                            <input type="number" name="change_balance_amount" value=""
                                                   placeholder="Amount"
                                                   step="0.00000001"/>
                                            <select name="change_balance_type">
                                                <option value="abc">ABC</option>
                                                <option value="usdt_deposited">USDT (Deposited)</option>
                                                <option value="usdt_dividents">USDT (Dividents)</option>
                                            </select>
                                            <input type="checkbox" value="1" name="change_balance_commission_free"
                                                   id="change_balance_commission_free"/>
                                            <label for="change_balance_commission_free">Commission free</label>

                                            <div class="text-error change_balance_amount" style="display: none"></div>
                                            <div class="text-error change_balance_type" style="display: none"></div>

                                            <input type="button" id="change_balance_send" style="margin-top: 15px"
                                                   value="Change balance" class="form-control"/>
                                        </div>
                                    </div>

                                    <div id="container-promo-code-change" class="form-group col-md-6"
                                         style="padding-left: 0;padding-right: 0;">
                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                            <hr/>
                                            <h2>Promo code change</h2>
                                            <span>Current Promo Code: <span style="font-weight: bold"
                                                                            id="current_promo_code">{{ $dataTypeContent->promo_code }}</span></span>
                                        </div>

                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">

                                            <input type="button" id="change_promo_code" style="margin-top: 15px"
                                                   value="Change promo code" class="form-control"/>
                                        </div>
                                    </div>

                                    <div id="container-move-teammates" class="form-group col-md-6"
                                         style="padding-left: 0;padding-right: 0;">
                                        <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                            <hr/>
                                            <h2>Move teammates</h2>

                                            <div class="teammates-item" v-for="(teammate, id) in teammates">
                                                <label v-bind:for="'teammates_'+id">@{{ id }}: @{{ teammate }}</label>
                                                <input type="checkbox" name="teammates[]" v-bind:value="id"
                                                       v-bind:id="'teammates_'+id" v-model="form.teammates"/>
                                            </div>
                                            <div class="text-error" v-if="errors.teammates"
                                                 v-for="error in errors.teammates">@{{ error }}
                                            </div>

                                            <input type="text" v-model="form.new_team"
                                                   placeholder="New team (promocode)"
                                                   class="form-control"/>
                                            <div class="text-error" v-if="errors.new_team"
                                                 v-for="error in errors.new_team">@{{ error }}
                                            </div>

                                            <input type="button" id="move_teammates" style="margin-top: 15px"
                                                   value="Move teammates" class="form-control"
                                                   v-on:click.prevent="moveTeammates"/>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </div><!-- panel-body -->
                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit"
                                        class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>

                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('css')
    <style>
        .text-error {
            color: red;
        }

        .teammates-item, .teammates-item div {
            width: 250px;
            display: inline-block;
        }

        .teammates-item label {
            min-width: 200px;
        }

        .custom_tid_options .custom_tid_options_form div {
            margin-bottom: 20px;
        }
    </style>
@stop

@push('javascript')
    <script>
        $().ready(function () {
            $('#transfer_send').click(function () {
                var fields = [
                    'transfer_from_id',
                    'transfer_amount',
                    'transfer_promo_code'
                ];

                $.post('{{ route('voyager.users.transfer_abc') }}', $('input[name*=transfer]').serialize(), function (data) {
                    if (data.status === true) {
                        var newBalanceAbc = parseFloat(parseFloat($('#transfer_balance_abc').text()) - parseFloat($('input[name*=transfer_amount]').val())).toFixed(8);

                        $('#transfer_balance_abc').text(newBalanceAbc);

                        for (var i in fields) {
                            $('.text-error.' + fields[i]).hide();
                        }

                        $('input[name=transfer_amount]').val('');
                        $('input[name=transfer_promo_code]').val('');

                        toastr.success('Money transferred.', 'Transfer ABC.');
                    } else {
                        $('.text-error.transfer_amount').show();
                        $('.text-error.transfer_amount').text('Error processing transaction.');
                    }
                }).fail(function (err) {
                    if (err.status == 422) {
                        for (var i in fields) {
                            $('.text-error.' + fields[i]).hide();
                            try {
                                var errorText = err.responseJSON.errors[fields[i]][0];
                                $('.text-error.' + fields[i]).show();
                                $('.text-error.' + fields[i]).text(errorText);
                            } catch {
                            }
                        }
                    } else if (err.status == 401) {
                        $('.text-error.transfer_amount').show();
                        $('.text-error.transfer_amount').text('You are not authorized.');
                    } else if (err.status == 500) {
                        $('.text-error.transfer_amount').show();
                        $('.text-error.transfer_amount').text('Server error.');
                    }
                });
            });

            $('#2fa_init_enabling').click(function () {
                $.post('{{ route('voyager.users.2fa_init') }}', function (data) {
                    if (!data.status) {
                        $('.text-error.google2fa_status').show();
                        $('.text-error.google2fa_status').text('QR code generation error.');

                        $('#2fa_code_view').hide();
                        $('#2fa_qrcode_view').hide();
                        $('#2fa_init_confirm').hide();
                    } else {
                        $('#2fa_code_view').val(data.google2fa_secret);
                        $('#2fa_qrcode_view').html(data.qr_image);

                        $('#2fa_code_view').show();
                        $('#2fa_qrcode_view').show();
                        $('#2fa_init_confirm').show();
                    }
                }).fail(function () {
                    $('.text-error.google2fa_status').show();
                    $('.text-error.google2fa_status').text('QR code generation error');

                    $('#2fa_code_view').hide();
                    $('#2fa_qrcode_view').hide();
                    $('#2fa_init_confirm').hide();
                });
            });

            $('#2fa_init_confirm').click(function () {
                if (confirm('After you click OK, two-factor authentication will be installed. Make sure you scan the QR code in the app. Confirm ?')) {
                    $.post('{{ route('voyager.users.2fa_confirm') }}', function (data) {
                        if (data.status) {
                            $('#2fa_code_view').hide();
                            $('#2fa_qrcode_view').hide();
                            $('#2fa_init_confirm').hide();
                            $('.text-error.google2fa_status').hide();
                            $('#2fa_status').show();

                            $('#2fa_init_enabling').attr('value', 'Update');

                            toastr.success('2FA configured.', '2FA.');
                        } else {
                            $('.text-error.google2fa_status').show();
                            $('.text-error.google2fa_status').text('Confirmation error.');
                        }
                    }).fail(function () {
                        $('.text-error.google2fa_status').show();
                        $('.text-error.google2fa_status').text('Confirmation error.');
                    });
                }
            });

            $('#change_password').click(function () {
                $('#container_change_password').show();
                $('#change_password').hide();
            });

            $('#button_change_password').click(function () {
                $.post('{{ route('voyager.users.password.change', $dataTypeContent->id) }}', $('input[name*=password]').serialize(), function (data) {
                    if (data.status === true) {
                        $('.text-error.password').hide();

                        $('input[name=password]').val('');
                        $('input[name=password_confirmation]').val('');

                        toastr.success('Password changed.', 'Change password.');
                    } else {
                        $('.text-error.password').show();
                        $('.text-error.password').text('Password change error.');
                    }
                }).fail(function (err) {
                    if (err.status == 422) {
                        $('.text-error.password').hide();
                        try {
                            var errorText = err.responseJSON.errors['password'][0];
                            $('.text-error.password').show();
                            $('.text-error.password').text(errorText);
                        } catch {
                        }
                    } else if (err.status == 401) {
                        $('.text-error.password').show();
                        $('.text-error.password').text('You are not authorized.');
                    } else {
                        $('.text-error.password').show();
                        $('.text-error.password').text('Server error.');
                    }
                });
            });

            //change balance
            $('#change_balance_send').click(function () {
                var fields = [
                    'change_balance_amount',
                    'change_balance_type'
                ];

                $.post('{{ route('voyager.users.balance.change', $dataTypeContent->id) }}', $('[name*=change_balance]').serialize(), function (data) {
                    if (data.status === true) {
                        let balanceType = $('select[name*=change_balance_type]').val();

                        if (balanceType == 'abc') {
                            var newBalanceAbc = parseFloat(parseFloat($('#change_balance_available_abc').text()) + parseFloat($('input[name*=change_balance_amount]').val())).toFixed(8);
                            $('#change_balance_available_abc').text(newBalanceAbc);
                            $('#transfer_balance_abc').text(newBalanceAbc);
                        } else if (balanceType == 'usdt_deposited') {
                            var newBalanceUsdt = parseFloat(parseFloat($('#change_balance_available_deposited_usdt').text()) + parseFloat($('input[name*=change_balance_amount]').val())).toFixed(8);
                            $('#change_balance_available_deposited_usdt').text(newBalanceUsdt);
                        } else {
                            var newBalanceUsdt = parseFloat(parseFloat($('#change_balance_available_dividents_usdt').text()) + parseFloat($('input[name*=change_balance_amount]').val())).toFixed(8);
                            $('#change_balance_available_dividents_usdt').text(newBalanceUsdt);
                        }

                        for (var i in fields) {
                            $('.text-error.' + fields[i]).hide();
                        }

                        $('input[name=change_balance_amount]').val('');

                        toastr.success('Balance changed.', 'Balance change.');
                    } else {
                        $('.text-error.change_balance_amount').show();
                        $('.text-error.change_balance_amount').text('Error processing transaction.');
                    }
                }).fail(function (err) {
                    if (err.status == 422) {
                        for (var i in fields) {
                            $('.text-error.' + fields[i]).hide();
                            try {
                                var errorText = err.responseJSON.errors[fields[i]][0];
                                $('.text-error.' + fields[i]).show();
                                $('.text-error.' + fields[i]).text(errorText);
                            } catch {
                            }
                        }
                    } else if (err.status == 401) {
                        $('.text-error.change_balance_amount').show();
                        $('.text-error.change_balance_amount').text('You are not authorized.');
                    } else {
                        $('.text-error.change_balance_amount').show();
                        $('.text-error.change_balance_amount').text('Server error.');
                    }
                });
            });

            //change promo code
            const buttonChangePromoCode = $('#change_promo_code');
            let sendedChangePromoCodeRequest = false;

            buttonChangePromoCode.click(() => {
                if (sendedChangePromoCodeRequest) {
                    return;
                }

                if (!confirm('Are you sure you want to change the promo code?')) {
                    return;
                }

                sendedChangePromoCodeRequest = true;

                axios.post('{{ route('voyager.users.promo_code.change', $dataTypeContent->id) }}').then((r) => {
                    toastr.success('Promo code was changed.', 'Change promo code.');
                    $('#current_promo_code').text(r.data.data.promo_code);
                }).catch((e) => {
                    if (e.response.status == 422) {
                        toastr.error('Error when attempt change promo code.', 'Change promo code.');
                    } else if (e.response.status == 403) {
                        toastr.error('You are not authorized.', 'Change promo code.');
                    } else {
                        toastr.error('Server error.', 'Change promo code.');
                    }
                }).finally(() => {
                    sendedChangePromoCodeRequest = false;
                })
            });
        });
    </script>

    <script>
        new Vue({
            el: '#container-move-teammates',
            data: () => {
                return {
                    loading: false,
                    form: {
                        new_team: null,
                        teammates: []
                    },
                    errors: {},
                    teammates: {!! json_encode($dataTypeContent->teammates->pluck('username', 'id')->toArray()) !!}
                }
            },
            methods: {
                moveTeammates() {
                    if (this.loading || !confirm('Do you confirm this action ?')) {
                        return;
                    }

                    this.loading = true;

                    axios.post('{{ sprintf('/%s/users/teammates/move/%s', config('voyager.prefix'), $dataTypeContent->promo_code) }}', this.form).then(() => {
                        this.removeMovedTeammates();

                        this.form.new_team = null;
                        this.form.teammates = [];
                        this.errors = {};

                        toastr.success('Selected team members have been moved to another team lead.', 'Move teammates.');
                    }).catch((e) => {
                        if (e.response.status === 422) {
                            this.errors = e.response.data.errors;
                        } else if (e.response.status == 401) {
                            toastr.error('You are not authorized.', 'Move teammates.');
                        } else if (e.response.status == 403) {
                            toastr.error('Action forbidden.', 'Move teammates.');
                        } else {
                            toastr.error('Server error.', 'Move teammates.');
                        }

                        if (e.response.status !== 422) {
                            this.errors = {};
                        }
                    }).finally(() => {
                        this.loading = false;
                    });
                },
                removeMovedTeammates() {
                    this.form.teammates.forEach((teammate) => {
                        this.$delete(this.teammates, teammate)
                    });
                }
            },
        });
    </script>
@endpush
