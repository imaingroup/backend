<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum\Swap;

use App\Modules\Finance\Exceptions\Ethereum\EthereumException;

/**
 * SwapException.
 */
abstract class SwapException extends EthereumException
{
}
