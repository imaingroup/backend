<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation
 * edit group chat request.
 */
final class EditGroupChatRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param MessageService $messageService
     */
    public function __construct(private MessageService $messageService)
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user
            && $this->messageService->checkAccessEditChat($user, $this->chat);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'name' => "string",
        'avatar' => "string"
    ])]
    public function rules(): array
    {
        return [
            'name' => 'required|scalar|max:255',
            'avatar' => 'nullable|image|dimensions:max_width=800,max_height=800|mimes:jpeg,jpg,png|max:1024'
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'chat' => "App\\Modules\\Messages\\Models\\Chat",
        'name' => 'string',
        'avatar' => 'Illuminate\Http\UploadedFile|null',
    ])]
    public function validated(): array
    {
        $validated = parent::validated();

        $data = [];
        $data['chat'] = $this->chat;
        $data['name'] = $validated['name'];
        $data['avatar'] = $validated['avatar'] ?? null;

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'name' => is_scalar($this->name) ? trim((string)$this->name) : $this->name
        ]);
    }
}
