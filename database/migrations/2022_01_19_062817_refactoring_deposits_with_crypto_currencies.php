<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * RefactoringDepositsWithCryptoCurrencies.
 */
final class RefactoringDepositsWithCryptoCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->unsignedBigInteger('crypto_currencies_id')
                ->after('users_id')
                ->nullable(true);
            $table->foreign('crypto_currencies_id', 'deposits_crypto_currencies_id_foreign')
                ->references('id')
                ->on('crypto_currencies')
                ->restrictOnDelete();
            $table->unsignedSmallInteger('type_coin')
                ->nullable(true)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->dropForeign('deposits_crypto_currencies_id_foreign');
            $table->dropColumn('crypto_currencies_id');
            $table->unsignedSmallInteger('type_coin')
                ->nullable(false)
                ->change();
        });
    }
}
