<?php
declare(strict_types=1);

namespace App\Modules\Security\Models;

use App\Modules\Core\Models\Model;
use Carbon\Carbon;

/**
 * UserIdentificationUserAgent.
 *
 * @property-read int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read UserIdentification $userIdentification
 */
final class UserIdentificationUserAgent extends Model
{
    /** {@inheritdoc} */
    protected $table = 'users_identifications_user_agent';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
