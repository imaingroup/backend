<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Add field ra types to settings_dashboard.
 */
final class AddFieldRATypesInSettingsDashboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('settings_dashboard', function (Blueprint $table) {
            $table->string('ra_types')->default('');
        });

        DB::table('data_rows')
            ->insert([
                'data_type_id' => DB::table('data_types')
                    ->where('name', 'settings_dashboard')
                    ->first()
                    ->id,
                'field' => 'ra_types',
                'type' => 'select_multiple',
                'display_name' => 'Recent activity types',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => '{"options":{"1":"TRADING CARD","2":"DEPOSIT","3":"WITHDRAWAL","4":"TRANSFER","5":"COMMISSION (USDT)","6":"COMMISSION (IPRO)","7":"Exchanged USDT to IPRO"}}',
                'order' => 7
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('settings_dashboard', function (Blueprint $table) {
            $table->dropColumn('ra_types');
        });

        DB::table('data_rows')
            ->where([
                'data_type_id' => DB::table('data_types')
                    ->where('name', 'settings_dashboard')
                    ->first()
                    ->id,
                'field' => 'ra_types'
            ])
            ->delete();
    }
}
