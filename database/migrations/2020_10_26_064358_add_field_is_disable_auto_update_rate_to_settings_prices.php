<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds field is_disable_auto_update_rate to table settings_prices.
 */
class AddFieldIsDisableAutoUpdateRateToSettingsPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->boolean('is_disable_auto_update_rate')->after('rate_updated_at')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->dropColumn('is_disable_auto_update_rate');
        });
    }
}
