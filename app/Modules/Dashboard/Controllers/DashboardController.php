<?php

namespace App\Modules\Dashboard\Controllers;

use App\Modules\Core\Exceptions\DataException;
use App\Http\Controllers\Controller;
use App\Modules\Dashboard\Requests\DashboardContentRequest;
use App\Modules\Dashboard\Services\DashboardService;
use App\Modules\Core\Services\DataService;
use Illuminate\Support\Facades\Auth;

/**
 * Dashboard controller.
 */
final class DashboardController extends Controller
{
    /** @var DashboardService Dashboard service. */
    private DashboardService $dashboardService;

    /** @var DataService Data service. */
    private DataService $dataService;

    /**
     * Construct.
     *
     * @param DashboardService $dashboardService
     * @param DataService $dataService
     */
    public function __construct(DashboardService $dashboardService, DataService $dataService)
    {
        $this->dashboardService = $dashboardService;
        $this->dataService = $dataService;
    }

    /**
     * Get content.
     *
     * @param DashboardContentRequest $request
     * @return array
     * @throws DataException
     */
    public function content(DashboardContentRequest $request): array
    {
        return $this->dataService->getData(DataService::TYPE_DASHBOARD_CONTENT, [Auth::user()]);
    }
}
