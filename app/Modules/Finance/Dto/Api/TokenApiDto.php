<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Finance\Contracts\Enum\NetworkEnumContract;

/**
 * TokenApiDto.
 *
 * @property-read int $decimals
 * @property-read string $name
 * @property-read string $symbol
 * @property-read string|null $address
 * @property-read string|null $tokenType
 * @property-read int|null $id
 * @property-read NetworkEnumContract&\BackedEnum $network
 * @property-read TokenStatsApiDto|null $stats
 * @property-read MarketShortDataApiDto|null $market
 */
final class TokenApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $decimals
     * @param string $name
     * @param string $symbol
     * @param string|null $address
     * @param string|null $tokenType
     * @param int|null $id
     * @param \BackedEnum&NetworkEnumContract $network
     * @param TokenStatsApiDto|null $stats
     * @param MarketShortDataApiDto|null $market
     */
    public function __construct(
        protected int $decimals,
        protected string $name,
        protected string $symbol,
        protected ?string $address,
        protected ?string $tokenType,
        protected ?int $id,
        protected NetworkEnumContract&\BackedEnum $network,
        protected ?TokenStatsApiDto $stats,
        protected ?MarketShortDataApiDto $market,
    )
    {
    }
}
