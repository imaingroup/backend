<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * GetTransactionException.
 */
final class GetTransactionException extends EthereumException
{
}
