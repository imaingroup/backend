<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Exceptions;

/**
 * WithdrawCreateException.
 */
final class WithdrawCreateException extends WithdrawException
{
}
