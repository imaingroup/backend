<?php

namespace App\Modules\Account\Requests;

use App\Modules\Account\Repositories\NotificationRepository;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This class need for validating view notification request.
 */
final class ViewNotificationRequest extends FormRequest
{
    /** @var \App\Modules\Account\Repositories\NotificationRepository Notification repository. */
    private NotificationRepository $notificationRepository;

    /**
     * Constructor.
     *
     * @param \App\Modules\Account\Repositories\NotificationRepository $notificationRepository
     */
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'notification_id' => 'required|scalar|exists:notifications,sid'
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws Exception
     */
    public function after(Validator $validator): void
    {
        if (is_scalar($this['notification_id'])) {
            $notification = $this->notificationRepository->findBySid((int)$this['notification_id']);

            if ($notification) {
                if (new \DateTime($notification->enable_before_at) <= new \DateTime()) {
                    $validator->errors()->add('notification_id', 'Notification has expired');
                    return;
                }

                $user = Auth::user();

                if($notification->haveRead($user)) {
                    $validator->errors()->add('notification_id', 'This notification was already read');
                    return;
                }

                if($notification->haveDeleted($user)) {
                    $validator->errors()->add('notification_id', 'This notification was deleted');
                    return;
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validated(): array
    {
        $data = parent::validated();
        $data['notification'] = $this->notificationRepository->findBySid((int)$data['notification_id']);

        return $data;
    }
}
