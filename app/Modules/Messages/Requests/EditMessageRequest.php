<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Models\Message;
use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation edit message request.
 */
final class EditMessageRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param MessageService $messageService
     */
    public function __construct(
        private MessageService $messageService
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            $this->message instanceof Message &&
            $this->messageService->checkAccessEditMessage($user, $this->message);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['message_text' => "string"])]
    public function rules(): array
    {
        return [
            'message_text' => 'required|scalar|max:1000'
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'message' => "App\\Modules\\Messages\\Models\\Message",
        'messageText' => "string"
    ])]
    public function validated(): array
    {
        return [
            'message' => $this->message,
            'messageText' => parent::validated()['message_text']
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'message_text' => is_scalar($this->message_text) ? trim((string)$this->message_text) : $this->message_text,
        ]);
    }
}
