<?php return [
	'site_name' => 'Insider Protocol',
	'copyright' => '© 2020 Insider Protocol',
	'form' => [
		'errors' => [
			'code_401' => 'You are not authorized',
			'code_403' => 'You don\'t have permission for this action',
			'other' => 'Server error',
		],
	],
];
