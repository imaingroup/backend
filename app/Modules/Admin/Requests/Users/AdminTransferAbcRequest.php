<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Users;

use App\Modules\Users\Repositories\UserRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

final class AdminTransferAbcRequest extends FormRequest
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transfer_from_id' => 'required|scalar|exists:App\Modules\Users\Models\User,id',
            'transfer_amount' => 'required|scalar',
            'transfer_promo_code' => 'required|scalar|exists:App\Modules\Users\Models\User,promo_code'
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator)
    {
        if ($this->get('transfer_amount', 0) < 0.00000001) {
            $validator->errors()->add('transfer_amount', 'Minimum amount for transfer should be great than or equals 0.00000001');
            return;
        }

        $sender = $this->userRepository->find($this->get('transfer_from_id', 0));
        $recipient = $this->userRepository->findByPromoCode($this->get('transfer_promo_code') ?? 'abc');

        if (!$sender || !$recipient) {
            return;
        }

        if ($sender->id === $recipient->id) {
            $validator->errors()->add('transfer_from_id', 'You doesn\'t have opportunity sending money to myself');
            return;
        }

        if ($this->get('transfer_amount', 0) > $sender->balance_abc) {
            $validator->errors()->add('transfer_amount', 'Sender doesn\'t have enough money in ABC balance');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validated(): array
    {
        $data = parent::validated();

        $data['sender'] = $this->userRepository->find($data['transfer_from_id']);
        $data['recipient'] = $this->userRepository->findByPromoCode($data['transfer_promo_code']);
        $data['transfer_amount'] = (float)$data['transfer_amount'];

        return $data;
    }
}
