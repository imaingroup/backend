<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api\Ethereum\Manage;

use App\Modules\Core\Dto\BaseDto;

/**
 * WalletDto.
 *
 * @property-read string $address
 * @property-read string|null $mnemonic
 * @property-read string $privateKey
 */
final class WalletDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param string $address
     * @param string|null $mnemonic
     * @param string $privateKey
     */
    public function __construct(
        protected string $address,
        protected ?string $mnemonic,
        protected string $privateKey,
    ) {

    }
}
