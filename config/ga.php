<?php
return [
    'enabled' => (bool)env('GA_ENABLED', false),
    'code' => env('GA_CODE', ''),
    'allowed_domain' => env('GA_ALLOWED_DOMAIN', ''),
    'disabled_ips' => array_filter(array_map(fn($ip) => trim($ip),
        explode(',', env('GA_DISABLED_IPS', ''))
    ), fn($ip) => !empty($ip)),
];
