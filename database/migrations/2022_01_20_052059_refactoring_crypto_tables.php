<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * RefactoringCryptoTables.
 */
final class RefactoringCryptoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->dropColumn(['eth_balance', 'usdt_balance']);
        });

        Schema::table('crypto_transactions', function (Blueprint $table) {
            $table->unsignedSmallInteger('type_old')
                ->after('crypto_addresses_id')
                ->nullable(true);
        });

        DB::transaction(function () {
            DB::table('crypto_transactions')
                ->where('usdt_amount', '>', 0)
                ->update([
                    'amount' => DB::raw('usdt_amount'),
                    'type_old' => 5,
                ]);
        });

        Schema::table('crypto_transactions', function (Blueprint $table) {
            $table->dropColumn(['usdt_amount']);
            $table->unsignedBigInteger('crypto_currencies_id')
                ->after('id')
                ->nullable(true);
            $table->foreign('crypto_currencies_id', 'crypto_transactions_crypto_currencies_id_foreign')
                ->references('id')
                ->on('crypto_currencies')
                ->restrictOnDelete();
        });

        Schema::create('crypto_balances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('crypto_addresses_id')
                ->nullable(true);
            $table->foreign('crypto_addresses_id', 'crypto_balances_crypto_addresses_id_foreign')
                ->references('id')
                ->on('crypto_addresses')
                ->restrictOnDelete();
            $table->unsignedBigInteger('crypto_currencies_id')
                ->nullable(true);
            $table->foreign('crypto_currencies_id', 'crypto_balances_crypto_currencies_id_foreign')
                ->references('id')
                ->on('crypto_currencies')
                ->restrictOnDelete();
            $table->unsignedDouble('balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('crypto_addresses', function (Blueprint $table) {
            $table->float('usdt_balance', 16, 8)
                ->after('access_key')
                ->nullable(true);
            $table->float('eth_balance', 16, 8)
                ->after('access_key')
                ->nullable(true);
        });

        Schema::table('crypto_transactions', function (Blueprint $table) {
            $table->dropForeign('crypto_transactions_crypto_currencies_id_foreign');
            $table->dropColumn('crypto_currencies_id');

            $table->float('usdt_amount', 16, 8)
                ->after('amount')
                ->nullable(true);
        });

        DB::transaction(function () {
            DB::table('crypto_transactions')
                ->where('type_old', 5)
                ->update([
                    'usdt_amount' => DB::raw('amount'),
                ]);
        });

        Schema::table('crypto_transactions', function (Blueprint $table) {
            $table->dropColumn('type_old');
        });

        Schema::drop('crypto_balances');
    }
}
