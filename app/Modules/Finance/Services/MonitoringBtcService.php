<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services;

use App\Facades\DB;
use App\Modules\Core\Exceptions\TransactionAlreadyStartedException;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Repositories\CryptoAddressRepository;
use App\Modules\Finance\Repositories\CryptoTransactionRepository;
use DateTime;
use Throwable;

/**
 * Monitoring btc service.
 * This class supporting method for update all observed addresses and transactions data.
 */
final class MonitoringBtcService
{
    /**
     * Run BTC monitoring.
     *
     * @throws Throwable
     * @throws TransactionAlreadyStartedException
     */
    public function runBtcMonitoring(): void
    {
        $api = $this->getBtcApiService();
        $ctRepository = $this->getCryptoTransactionRepository();

        if (DB::transactionLevel() !== 0) {
            throw new TransactionAlreadyStartedException();
        }

        $api->checkStatus();

        foreach ($this->getCryptoAddressRepository()
                     ->getObserved(CryptoAddress::TYPE_BTC) as $ca) {
            try {
                DB::beginTransaction();

                $ca = $this->getCryptoAddressRepository()
                    ->getAndLockForUpdate($ca->id);

                $transactions = $api->getIncomeTransactions($ca->address);

                foreach ($transactions as $transaction) {
                    $ct = $ctRepository->getByTxid($transaction->txid, true);

                    if ($ct) {
                        $ct->confirmations = $transaction->confirmations;

                        $ct->save();
                    } else {
                        $ctRepository->create([
                            'txid' => $transaction->txid,
                            'amount' => $transaction->amount,
                            'confirmations' => $transaction->confirmations,
                            'crypto_addresses_id' => $ca->id
                        ]);
                    }
                }

                $transactions = $ctRepository->getObserved(CryptoAddress::TYPE_BTC);

                foreach($transactions as $ct) {
                    $data = $api->getTransaction($ct->txid);
                    $ct = $ctRepository->getAndLockForUpdate($ct->id);
                    $ct->confirmations = $data->confirmations;

                    $ct->save();
                }

                $ca->cnt_check += 1;
                $ca->last_check_at = new DateTime();

                $ca->save();

                DB::commit();
            } catch (Throwable $e) {
                DB::rollBack();

                throw $e;
            }
        }
    }

    /**
     * Get CryptoAddressRepository.
     *
     * @return CryptoAddressRepository
     */
    private function getCryptoAddressRepository(): CryptoAddressRepository
    {
        return app(CryptoAddressRepository::class);
    }

    /**
     * Get CryptoTransactionRepository.
     *
     * @return CryptoTransactionRepository
     */
    private function getCryptoTransactionRepository(): CryptoTransactionRepository
    {
        return app(CryptoTransactionRepository::class);
    }

    /**
     * Get BtcApiService.
     *
     * @return BtcApiService
     */
    private function getBtcApiService(): BtcApiService
    {
        return app(BtcApiService::class);
    }
}
