<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration fixed default value in double fields.
 */
class FixedDefaultValueInDoubleFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('alter table settings_prices modify luft_deposit_btc_price double(16,8) default 0 not null');
        DB::statement('alter table settings_prices modify referral_abc_commission double(16,8) default 0 not null');

        DB::statement('alter table tid_history modify percent double(16,8) default 0 not null');
        DB::statement('alter table tid_history_trade_option modify percent double(16,8) default 0 not null');

        DB::statement('alter table users modify referral_abc_commission double(16,8) default 0 not null');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table settings_prices modify luft_deposit_btc_price double(16,8) not null');
        DB::statement('alter table settings_prices modify referral_abc_commission double(16,8) not null');

        DB::statement('alter table tid_history modify percent double(16,8) not null');
        DB::statement('alter table tid_history_trade_option modify percent double(16,8) not null');

        DB::statement('alter table users modify referral_abc_commission double(16,8) not null');
    }
}
