<?php
declare(strict_types=1);

namespace App\Modules\Logs\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Users\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Operation log model.
 */
final class OperationLog extends Model
{
    const TYPE_ADMIN_MASS_MONEY_OPERATION = 1;
    const TYPE_ADMIN_MOVED_TEAMMATES = 2;

    const TYPES = [
        self::TYPE_ADMIN_MASS_MONEY_OPERATION => 'Mass money operation',
        self::TYPE_ADMIN_MOVED_TEAMMATES => 'Move teammates'
    ];

    const BIND_TYPE_TO_DESCRIPTION = [
        self::TYPE_ADMIN_MASS_MONEY_OPERATION => 'operation_log.mass_money_operation',
        self::TYPE_ADMIN_MOVED_TEAMMATES => 'operation_log.moved_teammates'
    ];

    /** {@inheritdoc} */
    protected $table = 'operation_logs';

    /** {@inheritdoc} */
    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'users_id' => 'integer',
        'data' => 'json',
        'description' => 'string',
        'tasks_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * Get readable type.
     *
     * @return string|null
     */
    public function getReadableTypeAttribute(): ?string
    {
        return static::TYPES[$this->type] ?? $this->type;
    }

    /**
     * Get relation with User.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Get data in string for admin.
     *
     * @return mixed|string
     */
    public function getDataReadAttribute()
    {
        return $this->data ? json_encode($this->data) : null;
    }
}
