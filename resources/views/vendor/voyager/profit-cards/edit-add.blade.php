@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::bread-base.edit-add')

@section('append_form_fields')
    @if($edit)
        <div class="form-group">
            <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                <hr/>
                <h2>TID's expanses</h2>
            </div>

            @foreach($options as $option)
                <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                    <label class="control-label"
                           for="name">{{ $option->title }}</label>

                    <input type="number" class="form-control" step="1" min="0"
                           name="options_expanses[{{ $option->id }}]" value="{{ $dataTypeContent->getTidExpanseByOption($option)->tid_expanse ?? 0 }}"/>
                </div>
            @endforeach
        </div>
    @endif
@endsection
