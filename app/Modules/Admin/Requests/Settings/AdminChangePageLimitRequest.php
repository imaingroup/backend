<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * This request class validates request for change page limit.
 */
final class AdminChangePageLimitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'data_type' => 'required|exists:data_types,name',
            'limit' => [
                'required',
                Rule::in([10,15,25,50,100])
            ]
        ];
    }
}
