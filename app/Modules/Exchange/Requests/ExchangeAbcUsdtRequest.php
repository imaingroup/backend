<?php

namespace App\Modules\Exchange\Requests;


use App\Modules\Settings\Repositories\SettingsDepositRepository;
use App\Modules\Settings\Repositories\SettingsPriceRepository;
use App\Modules\Users\Services\UserService;
use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This class validates create deposit request.
 */
final class ExchangeAbcUsdtRequest extends FormRequest
{
    /** @var \App\Modules\Settings\Repositories\SettingsPriceRepository Settings price repository. */
    private SettingsPriceRepository $settingsPriceRepository;

    /** @var SettingsDepositRepository Settings deposit repository. */
    private SettingsDepositRepository $settingsDepositRepository;

    /** @var \App\Modules\Users\Services\UserService User service. */
    private UserService $userService;

    /**
     * Construct.
     *
     * @param SettingsPriceRepository $settingsPriceRepository
     * @param \App\Modules\Settings\Repositories\SettingsDepositRepository $settingsDepositRepository
     * @param UserService $userService
     */
    public function __construct(
        SettingsPriceRepository $settingsPriceRepository,
        SettingsDepositRepository $settingsDepositRepository,
        UserService $userService
    )
    {
        $this->settingsPriceRepository = $settingsPriceRepository;
        $this->settingsDepositRepository = $settingsDepositRepository;
        $this->userService = $userService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'amount_abc' => 'required|scalar|regex:/^\d{1,8}(\.\d{1,2})?$/',
            'accepted_rules' => 'scalar|accepted',
            '2fa_code' => 'required|scalar'
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws Exception
     */
    public function after(Validator $validator)
    {
        $user = Auth::user();

        if($this->settingsDepositRepository->get()->disabled_exchange_abc_usdt) {
            $validator->errors()->add('global', 'Exchange is temporarily disabled');
            return;
        }

        if(!$user->is_2fa) {
            $validator->errors()->add('2fa_code', 'Please activate 2FA');
            return;
        }

        if(is_scalar($this['2fa_code'])) {
            if (!$this->userService->check2fa($user, (string)$this['2fa_code'])) {
                $validator->errors()->add('2fa_code', 'Incorrect 2FA');
                return;
            }
        }

        if(is_scalar($this['amount_abc'])) {
            if($user->balance_abc < (float) $this['amount_abc']) {
                $validator->errors()->add('amount_abc', 'Insufficient IPRO funds');
                return;
            }
        }
    }
}
