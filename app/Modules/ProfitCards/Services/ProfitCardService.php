<?php
declare(strict_types=1);

namespace App\Modules\ProfitCards\Services;

use App\Modules\Money\Exceptions\MoneyTransferException;
use App\Modules\ProfitCards\Enums\Models\ProfitCard\Type;
use App\Modules\ProfitCards\Exceptions\ProfitCardException;
use App\Modules\Notifications\Jobs\BuyProfitCardEmailJob;
use App\Modules\ProfitCards\Models\ProfitCard;
use App\Modules\ProfitCards\Models\ProfitCardBuyHistory;
use App\Modules\Settings\Models\SettingsAbc;
use App\Modules\Money\Models\Transaction;
use App\Modules\Users\Models\User;
use App\Modules\Actions\Models\UserAction;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\ProfitCards\Repositories\ProfitCardBuyHistoryRepository;
use App\Modules\Settings\Repositories\SettingsAbcRepository;
use App\Modules\Investments\Repositories\TradeOptionRepository;
use App\Modules\Investments\Services\InvestmentService;
use App\Modules\Money\Services\MoneyTransferService;
use App\Modules\PromoCodes\Services\PromoCodeService;
use App\Modules\Actions\Services\UserActionService;
use Illuminate\Support\Facades\Auth;

/**
 * Profit card service.
 */
final class ProfitCardService
{
    /**
     * Constructor.
     *
     * @param SettingsAbcRepository $settingsAbcRepository
     * @param MoneyTransferService $moneyTransferService
     * @param UserActionService $userActionService
     * @param PromoCodeService $promoCodeService
     * @param TradeOptionRepository $tradeOptionRepository
     * @param InvestmentService $investmentService
     * @param ProfitCardBuyHistoryRepository $profitCardBuyHistoryRepository
     */
    public function __construct(
        private SettingsAbcRepository          $settingsAbcRepository,
        private MoneyTransferService           $moneyTransferService,
        private UserActionService              $userActionService,
        private PromoCodeService               $promoCodeService,
        private TradeOptionRepository          $tradeOptionRepository,
        private InvestmentService              $investmentService,
        private ProfitCardBuyHistoryRepository $profitCardBuyHistoryRepository
    )
    {
    }

    /**
     * Buy profit card.
     *
     * @param ProfitCard $profitCard
     * @param User $user
     * @throws MoneyTransferException
     * @throws ProfitCardException
     * @throws TransactionNotStarted
     */
    public function buy(ProfitCard $profitCard, User $user)
    {
        $settingAbc = $this->settingsAbcRepository->get();

        ProfitCard::refreshAndLockForUpdate($profitCard);
        User::refreshAndLockForUpdate($user);
        SettingsAbc::refreshAndLockForUpdate($settingAbc);

        if ($profitCard->price > $user->deposited_usdt) {
            throw new ProfitCardException('You doesn\'t have enough money in USDT deposited balance for buying this card.');
        } else if (!$profitCard->is_visible || !($profitCard->available_count > 0 || !$profitCard->stop_sell_after_zero)) {
            throw new ProfitCardException('This card is not available.');
        }

        $profitCard->available_count--;
        $profitCard->save();

        $transactionWithdraw = $this->moneyTransferService->moneyOperation(
            $user,
            Auth::check() ? Auth::user() : $user,
            $profitCard->price / -1,
            Transaction::CURRENCY_USDT_DEPOSITED,
            Transaction::TYPE_BUY_PROFIT_CARD
        );

        $amountAbc = $profitCard->abc + $profitCard->bonus;

        if ($profitCard->typeEnum === Type::TRADING) {
            $transactionAdd = $this->moneyTransferService->moneyOperation(
                $user,
                Auth::check() ? Auth::user() : $user,
                $amountAbc,
                Transaction::CURRENCY_ABC,
                Transaction::TYPE_BUY_PROFIT_CARD,
                $transactionWithdraw
            );
        }

        $user->total_invested_team += $profitCard->price;

        if ($user->user_status === User::STATUS_USER) {
            $user->user_status = User::STATUS_INVESTOR;
        }

        $this->extendExpanses($profitCard, $user);
        $user->save();

        $history = new ProfitCardBuyHistory();
        $history->profit_cards_id = $profitCard->id;
        $history->users_id = $user->id;
        $history->price = $profitCard->price;
        $history->abc = $profitCard->abc;
        $history->bonus = $profitCard->bonus;
        $history->related_transactions_id = $transactionAdd->id ?? $transactionWithdraw->id;
        $history->save();

        if (!$user->not_counted) {
            $settingAbc->abc_sold += $amountAbc;
            $settingAbc->save();
        }

        $this->userActionService->register(
            $user,
            UserAction::ACTION_BUY_PROFIT_CARD,
            [
                'profit_card_buy_histories' => $history->id
            ]
        );

        $this->promoCodeService->addCommissionAfterBuyProfitCard($history);

        if ($user->is_enabled_purchase_notifications) {
            BuyProfitCardEmailJob::dispatch($history);
        }
    }

    /**
     * Get sum of purchased cards for all period.
     *
     * @return float
     */
    public function getSumPurchasedCards(): float
    {
        return $this->profitCardBuyHistoryRepository
            ->getSumPurchasedCards();
    }

    /**
     * Get sum bonus of purchased cards for all period.
     *
     * @return float
     */
    public function getSumBonusPurchasedCards(): float
    {
        return $this->profitCardBuyHistoryRepository
            ->getSumBonusPurchasedCards();
    }

    /**
     * Extend expanses.
     *
     * @param ProfitCard $profitCard
     * @param User $user
     * @throws TransactionNotStarted
     */
    private function extendExpanses(ProfitCard $profitCard, User $user)
    {
        User::refreshAndLockForUpdate($user);

        //turn on tids
        $options = $this->tradeOptionRepository->getByRequiredCard($profitCard->id);

        foreach ($options as $option) {
            $this->investmentService->enableTid($user, $option);
        }

        //extend expanses
        if ($this->profitCardBuyHistoryRepository->hasBoughtById($user->id, $profitCard->id)) {
            foreach ($profitCard->tidsExpanses as $optionExpanse) {
                $this->investmentService->applyTidExpanse(
                    $user,
                    $optionExpanse->tradeOption,
                    $optionExpanse->tid_expanse
                );
            }

            $user->withdrawals_expanse += $profitCard->withdrawals_expanse;
            $user->save();
        }

        if ($profitCard->typeEnum === Type::EXPANDED) {
            $user->tids_allowable_at = ($user->tids_allowable_at ?? now())->modify('+1 YEAR');
            $user->save();
        }
    }
}
