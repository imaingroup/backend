<?php


namespace App\Modules\Investments\Exceptions;


use Exception;

/**
 * This exception may be throws in tid history service.
 */
final class TidHistoryException extends Exception
{

}
