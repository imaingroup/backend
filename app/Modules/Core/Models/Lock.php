<?php
declare(strict_types=1);

namespace App\Modules\Core\Models;

use Carbon\Carbon;

/**
 * Lock model.
 *
 * @property-read int $id
 * @property int $users_id
 * @property int $action
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
final class Lock extends Model
{
    const ACTION_SITE_UPDATE = 1;
    const ACTION_GET_DATA = 2;

    const ACTIONS = [
        self::ACTION_SITE_UPDATE,
        self::ACTION_GET_DATA,
    ];

    /** {@inheritdoc} */
    protected $table = 'locks';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'users_id' => 'integer',
        'action' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
