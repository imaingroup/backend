<?php
declare(strict_types=1);

namespace App\Modules\Security\Exceptions;

/**
 * This exception may be throws in IdentificationService.
 */
final class IdentificationException extends SecurityException
{
}
