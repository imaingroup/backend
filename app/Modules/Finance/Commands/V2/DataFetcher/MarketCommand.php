<?php
declare(strict_types=1);

namespace App\Modules\Finance\Commands\V2\DataFetcher;

use App\Modules\Finance\Exceptions\Services\BitQuery\RequestException;
use App\Modules\Finance\Services\V2\DataFetcherService;
use Illuminate\Console\Command;

/**
 * Market fetcher command.
 */
final class MarketCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finance:data_fetcher_market';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finance data fetcher market';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws RequestException
     */
    public function handle(): int
    {
        $this->getDataFetcherService()
            ->updateCurrenciesMarket();

        return 0;
    }

    /**
     * Get {@see DataFetcherService::class}.
     *
     * @return DataFetcherService
     */
    private function getDataFetcherService(): DataFetcherService
    {
        return app(DataFetcherService::class);
    }
}
