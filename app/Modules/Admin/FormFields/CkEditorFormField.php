<?php

namespace App\Modules\Admin\FormFields;


use Illuminate\Contracts\View\View;
use TCG\Voyager\FormFields\AbstractHandler;

/**
 * CkEditor field for voyager admin.
 */
final class CkEditorFormField extends AbstractHandler
{
    /** @var string Field name. */
    protected $codename = 'CK Editor';

    /**
     * Create field content.
     *
     * @param $row
     * @param $dataType
     * @param $dataTypeContent
     * @param $options
     * @return View
     */
    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('voyager.formfields.ckeditor', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent
        ]);
    }
}
