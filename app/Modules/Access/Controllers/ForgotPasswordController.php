<?php

namespace App\Modules\Access\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Access\Requests\ForgotPasswordRequest;
use App\Modules\Access\Services\ForgotPasswordService;

/**
 * Forgot password controller.
 */
final class ForgotPasswordController extends Controller
{
    /** @var ForgotPasswordService Forgot password service. */
    private ForgotPasswordService $forgotPasswordService;

    /**
     * Constructor.
     *
     * @param \App\Modules\Access\Services\ForgotPasswordService $forgotPasswordService
     */
    public function __construct(ForgotPasswordService $forgotPasswordService)
    {
        $this->forgotPasswordService = $forgotPasswordService;
    }

    /**
     * Resend action link.
     *
     * @param ForgotPasswordRequest $request
     * @return array
     */
    public function send(ForgotPasswordRequest $request): array
    {
        $this->forgotPasswordService->send($request->validated()['user']);

        $request->getSession()->put('forgot_password_request_at', new \DateTime());

        return ['status' => true];
    }
}
