<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates messages_chats_deleted table.
 */
final class CreateMessagesChatsDeleted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('messages_chats_deleted', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('messages_chats_id');
            $table->unsignedBigInteger('users_id');
            $table->timestamps();

            $table->foreign('messages_chats_id')
                ->references('id')
                ->on('messages_chats')
                ->onDelete('CASCADE');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->unique(['messages_chats_id', 'users_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('messages_chats_deleted');
    }
}
