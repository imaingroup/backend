<?php
declare(strict_types=1);

use App\Facades\DB;
use App\Modules\Finance\Models\CryptoAddress;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration change rate_history table.
 */
final class ChangeTableRateHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('rate_history', function (Blueprint $table) {
            $table->unsignedSmallInteger('type')
                ->nullable(true)
                ->after('id')
                ->index();
        });

        DB::beginTransaction();

        DB::table('rate_history')
            ->update([
                'type' => CryptoAddress::TYPE_BTC
            ]);

        DB::commit();

        Schema::table('rate_history', function (Blueprint $table) {
            $table->unsignedSmallInteger('type')
                ->nullable(false)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('rate_history', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
