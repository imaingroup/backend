<?php

namespace App\Modules\News\Models;

use App\Modules\News\Models\NewsCategory;
use App\Modules\News\Models\NewsLike;
use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Users\Models\User;
use App\Modules\Core\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Traits\Translatable;
use Throwable;

final class News extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;

    protected $translatable = ['title', 'external_preview', 'catalog_preview', 'text'];

    /**
     * Events for model.
     */
    protected static function booted()
    {
        static::creating(function (News $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }

    /**
     * Get category relation.
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(NewsCategory::class, 'news_categories_id');
    }

    /**
     * Get relation with NewsLike.
     *
     * @return HasMany
     */
    public function likes(): HasMany
    {
        return $this->hasMany(NewsLike::class, 'news_id');
    }

    /**
     * Check that user already liked this news.
     *
     * @param User $user
     * @return bool
     */
    public function liked(\App\Modules\Users\Models\User $user): bool
    {
        return $this->likes()
            ->where('users_id', $user->id)
            ->exists();
    }

    /**
     * Get video manifest.
     *
     * @return array|null
     */
    public function getVideo()
    {
        try {
            $json = \json_decode($this->video, true, 3, JSON_THROW_ON_ERROR);

            if (count($json) > 0) {
                $data = $json[0];
                $data['download_link'] = Storage::disk('local')->url($data['download_link']);

                return $data;
            }
        } catch (Throwable $e) {
        }

        return null;
    }
}
