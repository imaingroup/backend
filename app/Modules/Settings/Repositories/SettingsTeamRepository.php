<?php


namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsTeam;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Settings team repository.
 */
final class SettingsTeamRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Settings\Models\SettingsTeam $model
     */
    public function __construct(protected SettingsTeam $model)
    {}

    /**
     * Get settings model.
     *
     * @return SettingsTeam
     */
    public function get(): SettingsTeam
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
