<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * RemoveRateHistoryTable.
 */
final class RemoveRateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::drop('rate_history');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::create('rate_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('type')
                ->index();
            $table->unsignedFloat('rate', 12, 4);
            $table->unsignedTinyInteger('provider');
            $table->dateTime('provider_at')->nullable(true);
            $table->boolean('is_error')->index();
            $table->timestamps();

            $table->index('created_at');
        });
    }
}
