<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\Users\Repositories\UserRepository;
use App\Modules\Users\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Http\Controllers\VoyagerAuthController as BaseVoyagerAuthController;

final class VoyagerAuthController extends BaseVoyagerAuthController
{
    private UserRepository $userRepository;
    private UserService $userService;

    public function __construct(UserRepository $userRepository, UserService $userService)
    {
        $this->userRepository = $userRepository;
        $this->userService = $userService;
    }

    public function postLogin(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, false)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function validateLogin(Request $request)
    {
        $data = [
            $this->username() => $request[$this->username()],
            'password' => $request['password'],
            '2fa' => trim(str_replace(' ', '', $request['2fa'])),
        ];

        $validator = Validator::make($data, [
            $this->username() => 'required|exists:users,email|string',
            'password' => 'required|string'
        ]);

        if($validator->fails()) {
            $validator->validate();
        }

        $validator->after(function ($validator) use($data) {
            $user = $this->userRepository->findByEmail($data[$this->username()]);

            if($user->is_2fa && (empty($data['2fa']) || !$this->userService->check2fa($user, $data['2fa']))) {
                $validator->errors()->add('2fa', '2fa is invalid');
            }
        });

        $validator->validate();
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param Request $request
     * @return bool
    */
    protected function attemptLogin(Request $request): bool
    {
        return $this->guard()->attempt(
            $this->credentials($request), false
        );
    }
}
