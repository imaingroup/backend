<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum\Swap;

/**
 * TradeAndSendException.
 */
class TradeAndSendException extends SwapException
{
}
