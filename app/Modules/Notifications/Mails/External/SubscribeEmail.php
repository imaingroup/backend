<?php
declare(strict_types=1);

namespace App\Modules\Notifications\Mails\External;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Subscribe mail class.
 */
final class SubscribeEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new mail instance.
     */
    public function __construct(
        private string $email,
    )
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->subject('New subscriber')
            ->markdown('emails.external.subscribe', [
                'email' => $this->email
            ]);
    }
}
