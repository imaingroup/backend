<?php
declare(strict_types=1);

namespace App\Modules\Investments\Exceptions;

/**
 * ImitationPnlException.
 */
final class ImitationPnlException extends InvestmentException
{
}
