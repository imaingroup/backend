<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * Change withdraw in admin.
 */
final class ChangeWithdrawEditInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $typeId = DB::table('data_types')
            ->where('name', 'withdraws')
            ->first()
            ->id;

        DB::beginTransaction();

        DB::table('data_rows')
            ->where('data_type_id', $typeId)
            ->where('field', 'txid')
            ->update([
                'read' => 1,
                'edit' => 1,
                'details' => '{"validation":{"rule":"nullable|string|crypto_tx|required_if:status,2"}}',
                'order' => 8
            ]);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        $typeId = DB::table('data_types')
            ->where('name', 'withdraws')
            ->first()
            ->id;

        DB::beginTransaction();

        DB::table('data_rows')
            ->where('data_type_id', $typeId)
            ->where('field', 'txid')
            ->update([
                'read' => 1,
                'edit' => 0,
                'details' => '{}',
                'order' => 12
            ]);

        DB::commit();
    }
}
