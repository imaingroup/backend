<?php
declare(strict_types=1);

namespace App\Modules\Finance\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Finance\Dto\Api\TokenStatsApiDto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Crypto currency stats model.
 *
 * @property-read int $crypto_currencies_id
 * @property int $count
 * @property int|null $senders
 * @property int|null $receivers
 * @property int|null $days
 * @property Carbon|null $from_date
 * @property Carbon|null $till_date
 * @property float|null $amount
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read CryptoCurrency $currency
 */
final class CryptoCurrencyStats extends Model
{
    /** {@inheritdoc} */
    protected $table = 'crypto_currencies_stats';

    /** {@inheritdoc} */
    protected $primaryKey = 'crypto_currencies_id';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'crypto_currencies_id' => 'integer',
        'count' => 'integer',
        'senders' => 'integer',
        'receivers' => 'integer',
        'days' => 'integer',
        'from_date' => 'date',
        'till_date' => 'date',
        'amount' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Relation with {@see CryptoCurrency::class}.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_currencies_id');
    }

    /**
     * To api dto.
     *
     * @return TokenStatsApiDto
     */
    public function toApiDto(): TokenStatsApiDto
    {
        return new TokenStatsApiDto(
            $this->count,
            $this->senders,
            $this->receivers,
            $this->days,
            $this->from_date,
            $this->till_date,
            $this->amount,
        );
    }
}
