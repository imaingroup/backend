<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_deposit', function (Blueprint $table) {
            $table->id();
            $table->boolean('is_deposit_block');
            $table->boolean('is_withdraw_block');
            $table->float('withdraw_limit', 16, 8);
            $table->boolean('is_enabled_exchange_abc_usdt');
            $table->string('text_exchange_abc_usdt', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_deposit');
    }
}
