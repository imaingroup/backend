<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration adds Messages to admin menu.
 */
final class AddMenuMessagesInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('menu_items')
            ->insert([
                'menu_id' => 1,
                'title' => 'Messages',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-mail',
                'color' => '#000000',
                'parent_id' => 40,
                'order' => 4,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
                'route' => 'voyager.messages.list'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('menu_items')
            ->where('parent_id', 40)
            ->where('route', 'voyager.messages.list')
            ->delete();
    }
}
