<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\TidHistory;

use App\Modules\Investments\Repositories\TradeOptionRepository;
use App\Modules\Investments\Repositories\TidHistoryRepository;
use DateTime;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * This request class validate data for update percent data.
 */
final class AdminProfitHistoryUpdatePercentRequest extends FormRequest
{
    /** @var TradeOptionRepository Trade option repository. */
    private TradeOptionRepository $tradeOptionRepository;

    /** @var \App\Modules\Investments\Repositories\TidHistoryRepository Tid history repository. */
    private TidHistoryRepository $tidHistoryRepository;

    /**
     * Constructor.
     *
     * @param \App\Modules\Investments\Repositories\TidHistoryRepository $tidHistoryRepository
     * @param TradeOptionRepository $tradeOptionRepository
     */
    public function __construct(TidHistoryRepository $tidHistoryRepository, TradeOptionRepository $tradeOptionRepository)
    {
        $this->tradeOptionRepository = $tradeOptionRepository;
        $this->tidHistoryRepository = $tidHistoryRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => 'required|integer|exists:tid_history,id',
            'option' => 'required|integer|min:0',
            'percent' => 'required|scalar|regex:/^-?\d{1,8}(\.\d{0,2})?$/'
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator): void
    {
        $option = (int)$this['option'];
        $tidHistory = $this->tidHistoryRepository->find((int)$this['id']);

        if ($tidHistory) {
            if (new DateTime($tidHistory->date) < new DateTime()) {
                $validator->errors()->add('id', 'You can edit only feature history');
                return;
            }
        }

        if ($option > 0 && !$this->tradeOptionRepository->getByIndex($option - 1)) {
            $validator->errors()->add('option', 'This option doesn\'t exist');
            return;
        }
    }
}
