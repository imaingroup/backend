<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds sid fields to tables for hide real ids.
 */
final class AddSidToTables extends Migration
{
    private array $data = [
        'langs',
        'notifications',
        'videos',
        'profit_cards',
        'charts',
        'trade_options',
        'news_categories',
        'news',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->data as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->unsignedBigInteger('sid')->unique()->nullable(true)->after('id');
            });

            foreach (DB::table($table)->get() as $item) {
                DB::table($table)
                    ->where('id', $item->id)
                    ->update([
                        'sid' => mt_rand(100000000000, 999999999999)
                    ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->data as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn('sid');
            });
        }
    }
}
