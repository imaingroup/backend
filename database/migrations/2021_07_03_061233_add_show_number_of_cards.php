<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds show number of cards.
 */
final class AddShowNumberOfCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('profit_cards', function (Blueprint $table) {
            $table->boolean('show_number_of_cards')
                ->default(false)
                ->after('is_visible');
        });

        DB::transaction(function() {
            DB::table('data_rows')
                ->insert([
                    'data_type_id' =>  $this->getDataType()->id,
                    'field' => 'show_number_of_cards',
                    'type' => 'checkbox',
                    'display_name' => 'Show number of cards',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 10
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('profit_cards', function (Blueprint $table) {
            $table->dropColumn('show_number_of_cards');
        });

        DB::transaction(function() {
            DB::table('data_rows')
                ->where('data_type_id', $this->getDataType()->id)
                ->where('field', 'show_number_of_cards')
                ->delete();
        });
    }

    /**
     * Get data type.
     *
     * @return stdClass
     */
    private function getDataType(): stdClass
    {
        return DB::table('data_types')
            ->where('name', 'profit_cards')
            ->first();
    }
}
