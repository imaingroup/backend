<?php
declare(strict_types=1);

namespace App\Modules\Finance\Contracts\Enum;

use App\Modules\Finance\Enum\NetworkTypeEnum;

/**
 * NetworkEnumContract.
 */
interface NetworkEnumContract
{
    /**
     * Get network type.
     *
     * @return NetworkTypeEnum
     */
    public function type(): NetworkTypeEnum;
}
