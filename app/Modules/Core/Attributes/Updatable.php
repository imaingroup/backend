<?php
declare(strict_types=1);

namespace App\Modules\Core\Attributes;

use App\Modules\Core\Traits\ProtectedProperties;
use Attribute;

/**
 * Attribute Updatable.
 *
 * @property-read bool $isPublic
 */
#[Attribute(Attribute::TARGET_METHOD)]
final class Updatable
{
    use ProtectedProperties;

    /**
     * Constructor.
     *
     * @param bool $isPublic
     */
    public function __construct(
        protected bool $isPublic = false,
    ) {}
}
