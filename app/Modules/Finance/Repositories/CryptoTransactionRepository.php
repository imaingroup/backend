<?php
declare(strict_types=1);

namespace App\Modules\Finance\Repositories;

use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Models\CryptoTransaction;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * CryptoTransaction repository.
 */
final class CryptoTransactionRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param CryptoTransaction $model
     */
    public function __construct(protected CryptoTransaction $model)
    {
    }

    /**
     * Get by txid.
     *
     * @param string $txid
     * @param bool $lockForUpdate
     * @return CryptoTransaction|null
     */
    public function getByTxid(string $txid, bool $lockForUpdate = false): ?CryptoTransaction
    {
        $query = $this->model
            ->newQuery()
            ->where('txid', $txid);

        if ($lockForUpdate) {
            $query->lockForUpdate();
        }

        return $query->first();
    }

    /**
     * Get observed transactions.
     *
     * @param int $type
     * @return Collection
     */
    public function getObserved(
        #[ExpectedValues(CryptoAddress::SUPPORTED_CURRENCIES_TYPES)] int $type
    ): Collection
    {
        return $this->model
            ->newQuery()
            ->select('crypto_transactions.*')
            ->join('crypto_addresses', 'crypto_transactions.crypto_addresses_id', '=', 'crypto_addresses.id')
            ->whereColumn('crypto_addresses.min_conf', '>', 'crypto_transactions.confirmations')
            ->where('crypto_addresses.type', $type)
            ->orderBy('id', 'ASC')
            ->get();
    }
}
