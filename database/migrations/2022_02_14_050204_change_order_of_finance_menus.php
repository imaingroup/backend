<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * ChangeOrderOfFinanceMenus.
 */
final class ChangeOrderOfFinanceMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::transaction(function () {
            DB::table('menu_items')
                ->where('title', 'Global')
                ->where('parent_id', DB::table('menu_items')->where('title', 'Finance')->first()->id)
                ->update([
                    'order' => 4,
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {

        DB::transaction(function () {
            DB::table('menu_items')
                ->where('title', 'Global')
                ->where('parent_id', DB::table('menu_items')->where('title', 'Finance')->first()->id)
                ->update([
                    'order' => 0,
                ]);
        });
    }
}
