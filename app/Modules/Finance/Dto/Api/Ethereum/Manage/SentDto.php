<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api\Ethereum\Manage;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Finance\Enum\EthereumNetworkEnum;

/**
 * SentDto.
 *
 * @property-read EthereumNetworkEnum $network
 * @property-read string|null $contract
 * @property-read string|null $from
 * @property-read string $to
 * @property-read float $amount
 * @property-read float $sentAmount
 * @property-read string $tx
 */
final class SentDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param EthereumNetworkEnum $network
     * @param string|null $contract
     * @param string|null $from
     * @param string $to
     * @param float $amount
     * @param float $sentAmount
     * @param string $tx
     */
    public function __construct(
        protected EthereumNetworkEnum $network,
        protected ?string $contract,
        protected ?string $from,
        protected string $to,
        protected string $amount,
        protected string $sentAmount,
        protected string $tx,
    ) {
    }
}
