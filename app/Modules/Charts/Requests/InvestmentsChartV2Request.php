<?php
declare(strict_types=1);

namespace App\Modules\Charts\Requests;

use App\Modules\Charts\Services\InvestmentsChartService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * This class need for validation investments chart request.
 */
final class InvestmentsChartV2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            (!$this->trade || $this->trade->isOwner($user));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'period' => [
                'nullable',
                Rule::in(InvestmentsChartService::CHART_PERIODS)
            ]
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'trade' => $this->trade,
            'period' => $this->period
        ];
    }
}
