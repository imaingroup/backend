<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * SetLabelException.
 */
final class SetLabelException extends BtcException
{
}
