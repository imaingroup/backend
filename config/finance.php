<?php

return [
    'btc' => [
        'rpc' => [
            'address' => env('CRYPTO_BTC_RPC_ADDRESS'),
            'username' => env('CRYPTO_BTC_RPC_USERNAME'),
            'password' => env('CRYPTO_BTC_RPC_PASSWORD'),
            'secret' => env('CRYPTO_BTC_RPC_SECRET'),
        ],
        'min_conf' => (int)env('CRYPTO_BTC_MIN_CONF', 2),
        'monitoring_minutes' => (int)env('CRYPTO_BTC_MONITORING_MINUTES', 120),
        'min_commission' => (float)env('CRYPTO_BTC_MIN_COMMISSION', 1),
        'max_commission' => (float)env('CRYPTO_BTC_MAX_COMMISSION', 5),
        'reserve' => (float)env('CRYPTO_BTC_RESERVE', 10),
        'enable_monitoring' => (bool)env('ENABLE_BTC_MONITORING', 0),
    ],
    'eth' => [
        'default' => env('CRYPTO_ETH_DEFAULT_NETWORK'),
        'mainnet' => [
            'rpc' => [
                'address' => env('CRYPTO_ETH_MAINNET_RPC_ADDRESS'),
                'secret' => env('CRYPTO_ETH_MAINNET_RPC_SECRET'),
            ],
            'min_conf' => (int)env('CRYPTO_ETH_MAINNET_MIN_CONF', 2),
            'monitoring_minutes' => (int)env('CRYPTO_ETH_MAINNET_MONITORING_MINUTES', 120),
            'enable_monitoring' => (bool)env('CRYPTO_ETH_MAINNET_ENABLE', 0),
            'key_file' => env('CRYPTO_ETH_MAINNET_KEY_FILE'),
        ],
        'ropsten' => [
            'rpc' => [
                'address' => env('CRYPTO_ETH_ROPSTEN_RPC_ADDRESS'),
                'secret' => env('CRYPTO_ETH_ROPSTEN_RPC_SECRET'),
            ],
            'min_conf' => (int)env('CRYPTO_ETH_ROPSTEN_MIN_CONF', 2),
            'monitoring_minutes' => (int)env('CRYPTO_ETH_ROPSTEN_MONITORING_MINUTES', 120),
            'enable_monitoring' => (bool)env('CRYPTO_ETH_ROPSTEN_ENABLE', 0),
            'key_file' => env('CRYPTO_ETH_ROPSTEN_KEY_FILE'),
        ],
    ],
    'bsc' => [
        'mainnet' => [
            'rpc' => [
                'address' => env('CRYPTO_BSC_MAINNET_RPC_ADDRESS'),
                'secret' => env('CRYPTO_BSC_MAINNET_RPC_SECRET'),
            ],
            'min_conf' => (int)env('CRYPTO_BSC_MAINNET_MIN_CONF', 2),
            'monitoring_minutes' => (int)env('CRYPTO_BSC_MAINNET_MONITORING_MINUTES', 120),
            'enable_monitoring' => (bool)env('CRYPTO_BSC_MAINNET_ENABLE', 0),
            'key_file' => env('CRYPTO_BSC_MAINNET_KEY_FILE'),
        ],
    ],
    'bitquery' => [
        'graphql' => [
            'endpoint' => env('BITQUERY_GRAPHQL_ENDPOINT', ''),
            'sleep_per_query' => (int)env('BITQUERY_GRAPHQL_SLEEP_PER_QUERY', 0)
        ]
    ],
    'ethereum' => [
        'endpoint' => env('ETHEREUM_ENDPOINT'),
        'requests_delay' => (int)env('ETHEREUM_REQUESTS_DELAY', 0),
    ],
    'coingecko' => [
      'requests_delay' => (int)env('COINGECKO_REQUESTS_DELAY', 0),
    ],
];
