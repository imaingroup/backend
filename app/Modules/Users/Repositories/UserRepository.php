<?php
declare(strict_types=1);


namespace App\Modules\Users\Repositories;

use App\Modules\Users\Models\User;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

final class UserRepository extends BaseRepository
{
    /**
     * Constructor..
     *
     * @param User $model
     */
    public function __construct(protected User $model)
    {
    }

    public function findByEmail(string $email): ?User
    {
        return $this->model->newQuery()
            ->where('email', $email)
            ->first();
    }

    public function findByUsername(string $username): ?User
    {
        return $this->model->newQuery()
            ->where('username', $username)
            ->first();
    }

    public function findByPromoCode(string $promoCode): ?User
    {
        return $this->model->newQuery()
            ->where('promo_code', $promoCode)
            ->first();
    }

    public function findByRememberToken(string $token): ?User
    {
        return $this->model->newQuery()
            ->where('remember_token', $token)
            ->first();
    }

    /**
     * Find by email token.
     *
     * @param string $token
     * @return \App\Modules\Users\Models\User|null
     */
    public function findByEmailToken(string $token): ?User
    {
        return $this->model->newQuery()
            ->where('email_token', $token)
            ->first();
    }

    /**
     * Get geo stats by ip address country which users used at registration moment.
     *
     * @return Collection
     */
    public function getGeoStats(): Collection
    {
        return $this->model
            ->newQuery()
            ->selectRaw('COUNT(*) as count, ip_country')
            ->where('ip_country', '!=', '')
            ->where('ip_country', '!=', 'NOT_FOUND')
            ->groupBy('ip_country')
            ->orderBy('count', 'DESC')
            ->orderBy('ip_country', 'ASC')
            ->get()
            ->pluck(['count'], 'ip_country');
    }

    /**
     * Get referrals.
     *
     * @param int $teamLeadId
     * @return Collection
     */
    public function getReferrals(int $teamLeadId): Collection
    {
        return $this->model
            ->newQuery()
            ->where('team_member_id', $teamLeadId)
            ->orderBy('id', 'DESC')
            ->get();
    }

    /**
     * Get users which activated their accounts
     * and logged less once.
     *
     * @return Collection
     */
    public function getUsersLoggedLessOnce(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('type', User::TYPE_USER)
            ->where('last_activity_at', '>', DB::raw('email_verified_at'))
            ->get();
    }

    /**
     * Get users which activated their accounts
     * and logged less once.
     *
     * @param int $type
     * @return Collection
     */
    public function getByType(int $type): Collection
    {
        return $this->model
            ->newQuery()
            ->where('type', $type)
            ->get();
    }

    /**
     * Get users by username or email list.
     *
     * @param Collection|array $list
     * @return Collection
     */
    public function getUsersByUsernameOrEmailList(Collection|array $list): Collection
    {
        if (count($list) === 0) {
            return collect();
        }

        return $this->model
            ->newQuery()
            ->whereIn('email', $list)
            ->orWhereIn('username', $list)
            ->get();
    }

    /**
     * Get users ids by username or email list.
     *
     * @param Collection $list
     * @return Collection
     */
    public function getUsersIdsByUsernameOrEmailList(Collection $list): Collection
    {
        if (count($list) === 0) {
            return collect();
        }

        return $this->model
            ->newQuery()
            ->select(['id', 'email', 'username'])
            ->whereIn('email', $list)
            ->orWhereIn('username', $list)
            ->get();
    }

    /**
     * Get by ids.
     *
     * @param Collection|array $ids
     * @return Collection
     */
    public function getUsersIdsByIds(Collection|array $ids): Collection
    {
        if (count($ids) === 0) {
            return collect();
        }

        return $this->model
            ->newQuery()
            ->select(['id', 'email', 'username'])
            ->whereIn('id', $ids)
            ->get();
    }

    /**
     * Get users by username list, email list or ids list.
     *
     * @param Collection|array $list
     * @return Collection
     */
    public function getUsersByCollection(Collection|array $list): Collection
    {
        $allowQuery = false;
        $query = $this->model
            ->newQuery()
            ->orderBy('id', 'ASC');

        foreach ($list as $key => $data) {
            if (count($data) > 0) {
                $query->orWhereIn($key, $data);
                $allowQuery = true;
            }
        }

        if (!$allowQuery) {
            return collect();
        }

        return $query->get();
    }
}
