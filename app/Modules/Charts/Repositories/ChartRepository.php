<?php


namespace App\Modules\Charts\Repositories;

use App\Modules\Charts\Models\Chart;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * Chart repository.
 */
final class ChartRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Chart $model
     */
    public function __construct(protected Chart $model)
    {}

    /**
     * Get charts.
     *
     * @return Collection
     */
    public function get(): Collection
    {
        return $this->model
            ->newQuery()
            ->orderBy('sort', 'ASC')
            ->get();
    }
}
