<?php
declare(strict_types=1);

namespace App\Modules\Core\Models;

use App\Modules\Core\Traits\LockTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * Abstract model.
 */
abstract class Model extends BaseModel
{
    use HasFactory, LockTrait;
}
