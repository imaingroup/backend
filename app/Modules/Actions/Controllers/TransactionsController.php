<?php

namespace App\Modules\Actions\Controllers;

use App\Modules\Core\Exceptions\DataException;
use App\Http\Controllers\Controller;
use App\Modules\Actions\Requests\TransactionsListRequest;
use App\Modules\Core\Services\DataService;
use Illuminate\Support\Facades\Auth;

/**
 * Transactions controller.
 */
final class TransactionsController extends Controller
{
    /** @var DataService Data service. */
    private DataService $dataService;

    /**
     * Constructor.
     *
     * @param \App\Modules\Core\Services\DataService $dataService
     */
    public function __construct(DataService $dataService)
    {
        $this->dataService = $dataService;
    }

    /**
     * Transaction list.
     *
     * @param \App\Modules\Actions\Requests\TransactionsListRequest $request
     * @return array
     * @throws \App\Modules\Core\Exceptions\DataException
     */
    public function list(TransactionsListRequest $request): array
    {
        return $this->dataService->getData(DataService::TYPE_TRANSACTIONS_LIST, [Auth::user()]);
    }
}
