<?php

use App\Modules\Investments\Models\TidHistory;
use App\Modules\Investments\Models\TidHistoryTradeOption;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migrations fixes double data types in tables.
 */
class FixedFloatingTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('alter table settings_prices modify luft_deposit_btc_price double(16,8) not null');
        DB::statement('alter table settings_prices modify referral_abc_commission double(16,8) not null');

        DB::statement('alter table tid_history modify percent double(16,8) not null');
        DB::statement('alter table tid_history_trade_option modify percent double(16,8) not null');

        DB::statement('alter table users modify referral_abc_commission double(16,8) not null');

        TidHistory::query()
            ->where('percent', -0)
            ->update([
                'percent' => 0
            ]);

        TidHistoryTradeOption::query()
            ->where('percent', -0)
            ->update([
                'percent' => 0
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
