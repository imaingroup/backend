<?php
declare(strict_types=1);

namespace App\Modules\Finance\Enum;

use App\Modules\Finance\Contracts\Enum\NetworkEnumContract;

/**
 * NetworkEnum.
 */
enum BitcoinNetworkEnum: string implements NetworkEnumContract
{
    case BITCOIN = 'bitcoin';

    /**
     * Get network type.
     *
     * @return NetworkTypeEnum
     */
    public function type(): NetworkTypeEnum
    {
        return NetworkTypeEnum::BITCOIN;
    }
}
