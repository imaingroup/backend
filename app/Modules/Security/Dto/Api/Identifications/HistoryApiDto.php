<?php
declare(strict_types=1);

namespace App\Modules\Security\Dto\Api\Identifications;

use App\Modules\Core\Dto\BaseDto;
use Illuminate\Support\Collection;

/**
 * HistoryApiDto.
 *
 * @property-read Collection|RelatedUserApiDto[] $relatedUsers
 * @property-read Collection|HistoryItemApiDto[] $history
 * @property-read Collection|IPApiDto $ips
 * @property-read Collection|BrowserApiDto $browsers
 */
final class HistoryApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param Collection $relatedUsers
     * @param Collection $history
     * @param Collection $ips
     * @param Collection $browsers
     */
    public function __construct(
        protected Collection $relatedUsers,
        protected Collection $history,
        protected Collection $ips,
        protected Collection $browsers,
    )
    {

    }
}
