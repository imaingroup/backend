<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creating table bitcoin_logs
 * for store request log to bitcoin server.
 */
class CreateBitcoinLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitcoin_logs', function (Blueprint $table) {
            $table->id();
            $table->string('uri', 255);
            $table->string('method', 20);
            $table->smallInteger('code')->nullable(true);
            $table->longText('log');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitcoin_logs');
    }
}
