<?php

use App\Modules\ProfitCards\Models\ProfitCardBuyHistory;
use App\Modules\Users\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migrations adds field user_status to users table.
 */
class AddFieldUserStatusToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedTinyInteger('user_status')->default(User::STATUS_USER);
        });

        foreach(ProfitCardBuyHistory::query()
            ->select('users_id')
            ->groupBy('users_id')
            ->get() as $data) {
            $user = User::query()
                ->find($data->users_id);
            $user->user_status = User::STATUS_INVESTOR;
            $user->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('user_status');
        });
    }
}
