<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Requests\Settings\AdminChangePageLimitRequest;
use App\Modules\Admin\Repositories\DataTypeRepository;
use TCG\Voyager\Http\Controllers\VoyagerSettingsController as BaseVoyagerSettingsController;

/**
 * Voyager settings controller.
 */
final class VoyagerSettingsController extends BaseVoyagerSettingsController
{
    /** @var DataTypeRepository Data type repository. */
    private DataTypeRepository $dataTypeRepository;

    /**
     * Constructor.
     *
     * @param DataTypeRepository $dataTypeRepository
     */
    public function __construct(DataTypeRepository $dataTypeRepository)
    {
        $this->dataTypeRepository = $dataTypeRepository;
    }

    /**
     * Change page limit.
     *
     * @param \App\Modules\Admin\Requests\Settings\AdminChangePageLimitRequest $request
     * @return array
     */
    public function changePageLimit(AdminChangePageLimitRequest $request): array
    {
        $data = $request->validated();

        $this->dataTypeRepository->updatePageSize($data['data_type'], $data['limit']);

        return ['status' => true];
    }
}
