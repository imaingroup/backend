<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration changing type for field percent in table tid_history.
 */
class ChangeFieldTypePercentInTidHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tid_history', function (Blueprint $table) {
            $table->float('percent', 10, 4)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tid_history', function (Blueprint $table) {
            $table->float('percent', 8, 2)->change();
        });
    }
}
