<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Services\V2\CryptoData;

use App\Modules\Finance\Exceptions\FinanceException;

/**
 * UnsupportedNetworkException.
 */
final class UnsupportedNetworkException extends FinanceException
{

}
