<?php
declare(strict_types=1);

namespace App\Modules\ProfitCards\Requests;

use App\Modules\ProfitCards\Models\ProfitCard;
use App\Modules\ProfitCards\Repositories\ProfitCardRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This request class need for validating data when client want to buy a profit card.
 */
final class ProfitCardBuyRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param ProfitCardRepository $profitCardRepository
     */
    public function __construct(private ProfitCardRepository $profitCardRepository)
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_id' => 'required|scalar|exists:profit_cards,sid'
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator)
    {
        /** @var ProfitCard $profitCard */
        $profitCard = $this->profitCardRepository->findBySid((int)$this['card_id']);

        if ($profitCard) {
            if(!$profitCard->is_visible || !($profitCard->available_count > 0 || !$profitCard->stop_sell_after_zero)) {
                $validator->errors()->add('card_id', 'This card is not available');

                return;
            }

            if($profitCard->price > Auth::user()->deposited_usdt) {
                $validator->errors()->add('balance', 'Insufficient deposited USDT funds');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validated(): array
    {
        $data = parent::validated();
        $data['card'] = $this->profitCardRepository->findBySid((int)$data['card_id']);

        return $data;
    }
}
