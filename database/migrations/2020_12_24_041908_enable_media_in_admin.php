<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration enable media in admin.
 */
final class EnableMediaInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('menu_items')
            ->where('parent_id', 18)
            ->where('url', '/XI400PGNB/media')
            ->delete();

        DB::table('menu_items')
            ->insert([
                'menu_id' => 1,
                'title' => 'Media',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-images',
                'color' => '#000000',
                'parent_id' => 18,
                'order' => 4,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
                'route' => 'voyager.media.index'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menu_items')
            ->where('parent_id', 18)
            ->where('route', 'voyager.media.index')
            ->delete();
    }
}
