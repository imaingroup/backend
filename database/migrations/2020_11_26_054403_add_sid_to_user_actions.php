<?php

use App\Modules\Actions\Models\UserAction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds sid field to user actions table
 * for hide the real id.
 */
final class AddSidToUserActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_actions', function (Blueprint $table) {
            $table->unsignedBigInteger('sid')->unique()->nullable(true)->after('id');
        });

        foreach(UserAction::all() as $action) {
            $action->sid = mt_rand(100000000000, 999999999999);
            $action->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_actions', function (Blueprint $table) {
            $table->dropColumn('sid');
        });
    }
}
