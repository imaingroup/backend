<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration change tables for add
 * info how many users have accounts.
 */
final class DetectMultiAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('users_identifications', function (Blueprint $table) {
            $table->dropIndex(['user_agent']);
            $table->dropIndex(['browser']);
            $table->dropIndex(['browser_version']);
            $table->dropIndex(['browser_major_version']);
            $table->dropIndex(['is_ie']);
            $table->dropIndex(['is_chrome']);
            $table->dropIndex(['is_firefox']);
            $table->dropIndex(['is_safari']);
            $table->dropIndex(['is_opera']);
            $table->dropIndex(['engine']);
            $table->dropIndex(['engine_version']);
            $table->dropIndex(['platform']);
            $table->dropIndex(['os']);
            $table->dropIndex(['os_version']);
            $table->dropIndex(['is_windows']);
            $table->dropIndex(['is_linux']);
            $table->dropIndex(['is_ubuntu']);
            $table->dropIndex(['is_solaris']);
            $table->dropIndex(['device']);
            $table->dropIndex(['device_type']);
            $table->dropIndex(['device_vendor']);
            $table->dropIndex(['cpu']);
            $table->dropIndex(['is_mobile']);
            $table->dropIndex(['is_mobile_android']);
            $table->dropIndex(['is_mobile_opera']);
            $table->dropIndex(['is_mobile_windows']);
            $table->dropIndex(['is_mobile_blackberry']);
            $table->dropIndex(['is_mobile_ios']);
            $table->dropIndex(['is_iphone']);
            $table->dropIndex(['is_ipod']);
            $table->dropIndex(['color_depth']);
            $table->dropIndex(['current_resolution']);
            $table->dropIndex(['available_resolution']);
            $table->dropIndex(['device_xdpi']);
            $table->dropIndex(['device_ydpi']);
            $table->dropIndex(['is_java']);
            $table->dropIndex(['java_version']);
            $table->dropIndex(['is_flash']);
            $table->dropIndex(['flash_version']);
            $table->dropIndex(['is_silverlight']);
            $table->dropIndex(['silverlight_version']);
            $table->dropIndex(['mime_types']);
            $table->dropIndex(['is_mime_types']);
            $table->dropIndex(['is_font']);
            $table->dropIndex(['is_local_storage']);
            $table->dropIndex(['is_session_storage']);
            $table->dropIndex(['is_cookie']);
            $table->dropIndex(['time_zone']);
            $table->dropIndex(['language']);
            $table->dropIndex(['languages']);
            $table->dropIndex(['system_language']);
            $table->dropIndex(['is_canvas']);
            $table->dropIndex(['canvas_hash']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('multi_accounts')->index();
        });

        DB::beginTransaction();

        $userDataId = DB::table('data_types')
            ->where('name', 'users')
            ->first()
            ->id;

        DB::table('data_rows')
            ->insert([
                'data_type_id' => $userDataId,
                'field' => 'multi_accounts',
                'type' => 'number',
                'display_name' => 'Accounts',
                'required' => 0,
                'browse' => 1,
                'read' => 0,
                'edit' => 0,
                'add' => 0,
                'delete' => 0,
                'details' => '{}',
                'order' => 24
            ]);

        DB::table('data_rows')
            ->where(
                [
                    'data_type_id' => $userDataId,
                    'field' => 'balance_usdt'
                ]
            )
            ->update([
                'display_name' => 'USDT'
            ]);

        DB::table('data_rows')
            ->where(
                [
                    'data_type_id' => $userDataId,
                    'field' => 'balance_abc'
                ]
            )
            ->update([
                'display_name' => 'ABC'
            ]);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('users_identifications', function (Blueprint $table) {
            $table->index('user_agent');
            $table->index('browser');
            $table->index('browser_version');
            $table->index('browser_major_version');
            $table->index('is_ie');
            $table->index('is_chrome');
            $table->index('is_firefox');
            $table->index('is_safari');
            $table->index('is_opera');
            $table->index('engine');
            $table->index('engine_version');
            $table->index('platform');
            $table->index('os');
            $table->index('os_version');
            $table->index('is_windows');
            $table->index('is_linux');
            $table->index('is_ubuntu');
            $table->index('is_solaris');
            $table->index('device');
            $table->index('device_type');
            $table->index('device_vendor');
            $table->index('cpu');
            $table->index('is_mobile');
            $table->index('is_mobile_android');
            $table->index('is_mobile_opera');
            $table->index('is_mobile_windows');
            $table->index('is_mobile_blackberry');
            $table->index('is_mobile_ios');
            $table->index('is_iphone');
            $table->index('is_ipod');
            $table->index('color_depth');
            $table->index('current_resolution');
            $table->index('available_resolution');
            $table->index('device_xdpi');
            $table->index('device_ydpi');
            $table->index('is_java');
            $table->index('java_version');
            $table->index('is_flash');
            $table->index('flash_version');
            $table->index('is_silverlight');
            $table->index('silverlight_version');
            $table->index('mime_types');
            $table->index('is_mime_types');
            $table->index('is_font');
            $table->index('is_local_storage');
            $table->index('is_session_storage');
            $table->index('is_cookie');
            $table->index('time_zone');
            $table->index('language');
            $table->index('languages');
            $table->index('system_language');
            $table->index('is_canvas');
            $table->index('canvas_hash');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('multi_accounts');
        });

        DB::beginTransaction();

        $userDataId = DB::table('data_types')
            ->where('name', 'users')
            ->first()
            ->id;

        DB::table('data_rows')
            ->where([
                'data_type_id' => $userDataId,
                'field' => 'multi_accounts',
            ])
            ->delete();

        DB::table('data_rows')
            ->where(
                [
                    'data_type_id' => $userDataId,
                    'field' => 'balance_usdt'
                ]
            )
            ->update([
                'display_name' => 'Available USDT total balance'
            ]);

        DB::table('data_rows')
            ->where(
                [
                    'data_type_id' => $userDataId,
                    'field' => 'balance_abc'
                ]
            )
            ->update([
                'display_name' => 'Available ABC balance'
            ]);

        DB::commit();
    }
}
