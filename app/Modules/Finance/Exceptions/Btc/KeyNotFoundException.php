<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * Key not found exception.
 */
final class KeyNotFoundException extends BtcException
{
}
