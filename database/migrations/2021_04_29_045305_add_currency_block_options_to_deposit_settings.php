<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds currency block options
 * to deposit settings.
 */
final class AddCurrencyBlockOptionsToDepositSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->boolean('is_block_eth')
                ->default(false)
                ->after('is_deposit_block');
            $table->boolean('is_block_btc')
                ->default(false)
                ->after('is_deposit_block');
            $table->boolean('is_block_usdt')
                ->default(false)
                ->after('is_deposit_block');
        });

        DB::beginTransaction();

        $typeId = DB::table('data_types')
            ->where('name', 'settings_deposit')
            ->first()
            ->id;

        DB::table('data_rows')
            ->where('field', 'is_deposit_block')
            ->where('data_type_id', $typeId)
            ->update([
                'display_name' => 'All deposit block',
            ]);

        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'is_block_usdt',
                'type' => 'checkbox',
                'display_name' => 'USDT block',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{}',
                'order' => 1
            ]);


        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'is_block_btc',
                'type' => 'checkbox',
                'display_name' => 'BTC block',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{}',
                'order' => 1
            ]);

        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'is_block_eth',
                'type' => 'checkbox',
                'display_name' => 'ETH block',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{}',
                'order' => 1
            ]);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->dropColumn(['is_block_eth', 'is_block_btc', 'is_block_usdt']);
        });

        $typeId = DB::table('data_types')
            ->where('name', 'settings_deposit')
            ->first()
            ->id;

        DB::beginTransaction();

        DB::table('data_rows')
            ->where('field', 'is_deposit_block')
            ->where('data_type_id', $typeId)
            ->update([
                'display_name' => 'Deposit Block',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', $typeId)
            ->whereIn('field', [
                'is_block_usdt',
                'is_block_btc',
                'is_block_eth',
            ])
            ->delete();

        DB::commit();
    }
}
