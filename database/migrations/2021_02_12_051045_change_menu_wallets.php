<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * This migration changes wallets.
 */
final class ChangeMenuWallets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('menu_items')
            ->where('route', 'voyager.finance.ethereum')
            ->update([
                'title' => 'Wallets',
                'route' => 'voyager.finance.wallets'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('menu_items')
            ->where('route', 'voyager.finance.wallet')
            ->update([
                'title' => 'Ethereum',
                'route' => 'voyager.finance.ethereum'
            ]);
    }
}
