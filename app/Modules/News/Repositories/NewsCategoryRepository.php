<?php


namespace App\Modules\News\Repositories;

use App\Modules\News\Models\NewsCategory;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * New category repository.
 */
final class NewsCategoryRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param NewsCategory $model
     */
    public function __construct(protected NewsCategory $model)
    {}

    /**
     * Get all active categories.
     *
     * @return Collection
     */
    public function getAllActive(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('is_visible', true)
            ->orderBy('sort', 'ASC')
            ->get();
    }
}
