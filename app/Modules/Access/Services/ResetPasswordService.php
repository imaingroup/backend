<?php


namespace App\Modules\Access\Services;

use App\Modules\Notifications\Jobs\PasswordWasResetEmailJob;
use App\Modules\Users\Models\User;

/**
 * Reset password service.
 */
final class ResetPasswordService
{
    /**
     * Change user password.
     *
     * @param \App\Modules\Users\Models\User $user
     * @param string $password
     */
    public function changePassword(User $user, string $password): void
    {
        $user->refresh();

        $user->reset_token = null;
        $user->reset_token_valid_at = null;
        $user->password = bcrypt($password);

        $user->save();

        if ($user->is_enabled_password_change_notifications) {
            PasswordWasResetEmailJob::dispatch($user);
        }
    }
}
