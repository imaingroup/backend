<?php
declare(strict_types=1);

namespace App\Modules\Messages\Exceptions;

use Exception;

/**
 * Base exception for message service.
 */
class MessageException extends Exception
{

}
