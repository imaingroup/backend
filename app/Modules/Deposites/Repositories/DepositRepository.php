<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Deposites\Models\Deposit;
use Illuminate\Support\Collection;

/**
 * Deposit repository.
 */
final class DepositRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Deposit $model
     */
    public function __construct(protected Deposit $model)
    {}

    /**
     * Get last invoice.
     *
     * @param int $userId
     * @return Deposit|null
     */
    public function getLastInvoice(int $userId): ?Deposit
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $userId)
            ->orderBy('id', 'DESC')
            ->first();
    }

    /**
     * Get observed deposits.
     *
     * @return Collection
     */
    public function getObserved(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('status', '!=', Deposit::STATUS_FINISHED)
            ->orderBy('id', 'ASC')
            ->get();
    }
}
