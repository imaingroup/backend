<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Add statement image fields to settings_landing table.
 */
class AddStatementImageFieldsToSettingsLanding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_landing', function (Blueprint $table) {
            $table->text('statement_image_1')->nullable(true);
            $table->text('statement_image_2')->nullable(true);
        });

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 39,
                'field' => 'statement_image_1',
                'type' => 'image',
                'display_name' => 'Statement Image 1',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '{"validation":{"rule":"image"}}',
                'order' => 10
            ]);

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 39,
                'field' => 'statement_image_2',
                'type' => 'image',
                'display_name' => 'Statement Image 2',
                'required' => 0,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '{"validation":{"rule":"image"}}',
                'order' => 11
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_landing', function (Blueprint $table) {
            $table->dropColumn('statement_image_1');
            $table->dropColumn('statement_image_2');
        });

        DB::table('data_rows')
            ->where('data_type_id', 39)
            ->where('field', 'statement_image_1')
            ->delete();

        DB::table('data_rows')
            ->where('data_type_id', 39)
            ->where('field', 'statement_image_2')
            ->delete();
    }
}
