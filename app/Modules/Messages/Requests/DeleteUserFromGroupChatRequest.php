<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation
 * delete user from group chat request.
 */
final class DeleteUserFromGroupChatRequest extends FormRequest
{
    public function __construct(private MessageService $messageService)
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user
            && $this->messageService->checkAccessDeleteUserFromGroupChat(
                $user,
                $this->chat,
                $this->chatUser
            );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'chatUser' => 'App\Modules\Messages\Models\ChatUser'
    ])]
    public function validated(): array
    {
        return ['chatUser' => $this->chatUser];
    }
}
