<?php

namespace App\Modules\Users\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * This class need for validating profile notifications save request.
 */
final class ProfileNotificationsSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $user = Auth::user();

        return [
            'enable_deposit_notifications' => ['nullable', 'scalar', Rule::in(['0', '1'])],
            'enable_withdrawal_notifications' => ['nullable', 'scalar', Rule::in(['0', '1'])],
            'enable_purchase_notifications' => ['nullable', 'scalar', Rule::in(['0', '1'])],
            'enable_password_change_notifications' => ['nullable', 'scalar', Rule::in(['0', '1'])],
        ];
    }
}
