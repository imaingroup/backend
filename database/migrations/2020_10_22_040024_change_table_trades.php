<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration changing table trades.
 */
class ChangeTableTrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->dropColumn('tid');
            $table->float('unrealized_pnl', 16, 8);
            $table->date('opened_at')->change();
            $table->integer('period');
            $table->boolean('is_open');
            $table->dateTime('closed_at')->nullable(true);
            $table->float('rate_abc_usdt', 16, 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->string('tid', 255);
            $table->dropColumn('unrealized_pnl');
            $table->dateTime('opened_at')->change();
            $table->dropColumn('period');
            $table->dropColumn('is_open');
            $table->dropColumn('closed_at');
            $table->dropColumn('rate_abc_usdt');
        });
    }
}
