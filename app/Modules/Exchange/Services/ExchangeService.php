<?php


namespace App\Modules\Exchange\Services;


use App\Modules\Exchange\Exceptions\ExchangeException;
use App\Modules\Money\Exceptions\MoneyTransferException;
use App\Modules\Money\Models\Transaction;
use App\Modules\Users\Models\User;
use App\Modules\Actions\Models\UserAction;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Settings\Repositories\SettingsPriceRepository;
use App\Modules\Money\Services\MoneyTransferService;
use App\Modules\Actions\Services\UserActionService;
use Illuminate\Support\Facades\Auth;

/**
 * Exchange service.
 */
final class ExchangeService
{
    /** @var SettingsPriceRepository Settings price repository. */
    private SettingsPriceRepository $settingsPriceRepository;

    /** @var MoneyTransferService Money transfer service. */
    private MoneyTransferService $moneyTransferService;

    /** @var UserActionService User action service. */
    private UserActionService $userActionService;

    /**
     * Constructor.
     *
     * @param SettingsPriceRepository $settingsPriceRepository
     * @param MoneyTransferService $moneyTransferService
     * @param UserActionService $userActionService
     */
    public function __construct(
        SettingsPriceRepository $settingsPriceRepository,
        MoneyTransferService    $moneyTransferService,
        UserActionService       $userActionService
    )
    {
        $this->settingsPriceRepository = $settingsPriceRepository;
        $this->moneyTransferService = $moneyTransferService;
        $this->userActionService = $userActionService;
    }

    /**
     * Exchange abc to usdt.
     *
     * @param User $user
     * @param float $amount
     * @throws ExchangeException
     * @throws MoneyTransferException
     * @throws TransactionNotStarted
     */
    public function exchangeAbcUsdt(User $user, float $amount): void
    {
        User::refreshAndLockForUpdate($user);

        if ($user->balance_abc < $amount) {
            throw new ExchangeException('You doesn\'t have enough money in ABC balance');
        }

        $amountUsdt = $amount * $this->settingsPriceRepository->get()->abc_price;

        $transaction = $this->moneyTransferService->moneyOperation(
            $user,
            Auth::check() ? Auth::user() : $user,
            $amount / -1,
            Transaction::CURRENCY_ABC,
            Transaction::TYPE_EXCHANGE_ABC_USDT
        );

        $this->moneyTransferService->moneyOperation(
            $user,
            Auth::check() ? Auth::user() : $user,
            $amountUsdt,
            Transaction::CURRENCY_USDT_DIVIDENTS,
            Transaction::TYPE_EXCHANGE_ABC_USDT,
            $transaction
        );
    }

    /**
     * Exchange usdt to abc.
     *
     * @param User $user
     * @param float $amount
     * @throws ExchangeException
     * @throws MoneyTransferException
     * @throws TransactionNotStarted
     */
    public function exchangeUsdtAbc(User $user, float $amount): void
    {
        User::refreshAndLockForUpdate($user);

        if ($amount <= 0) {
            throw new ExchangeException('Amount must be great than 0');
        }

        if ($user->balance_usdt < $amount) {
            throw new ExchangeException('You doesn\'t have enough money in USDT balance');
        }

        $amountAbc = $amount / $this->settingsPriceRepository->get()->abc_price;

        $usdtDividends = 0;
        $usdtDeposited = 0;

        if ($user->dividents_usdt > 0) {
            if ($user->dividents_usdt < $amount) {
                $usdtDividends = $user->dividents_usdt;
                $usdtDeposited = $amount - $usdtDividends;
            } else {
                $usdtDividends = $amount;
            }
        } else {
            $usdtDeposited = $amount;
        }

        $transaction = $usdtDividends > 0 ?
            $this->moneyTransferService->moneyOperation(
                $user,
                Auth::check() ? Auth::user() : $user,
                $usdtDividends / -1,
                Transaction::CURRENCY_USDT_DIVIDENTS,
                Transaction::TYPE_EXCHANGE_USDT_ABC
            ) : null;

        if ($usdtDeposited > 0) {
            $transaction = $this->moneyTransferService->moneyOperation(
                $user,
                Auth::check() ? Auth::user() : $user,
                $usdtDeposited / -1,
                Transaction::CURRENCY_USDT_DEPOSITED,
                Transaction::TYPE_EXCHANGE_USDT_ABC,
                $transaction
            );
        }

        $transactionAdd = $this->moneyTransferService->moneyOperation(
            $user,
            Auth::check() ? Auth::user() : $user,
            $amountAbc,
            Transaction::CURRENCY_ABC,
            Transaction::TYPE_EXCHANGE_USDT_ABC,
            $transaction
        );

        $this->userActionService->register(
            $user,
            UserAction::ACTION_EXCHANGE_USDT_ABC,
            [
                'transaction' => $transactionAdd->id
            ]
        );
    }
}
