<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions;

use Exception;

/**
 * Abstract finance exception.
 */
abstract class FinanceException extends Exception
{
}
