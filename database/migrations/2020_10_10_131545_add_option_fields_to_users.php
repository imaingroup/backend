<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOptionFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_tid_block')->default(false);
            $table->boolean('is_deposit_block')->default(false);
            $table->boolean('is_withdraw_block')->default(false);
            $table->boolean('is_withdraw_default')->default(true);
            $table->float('withdraw_limit', 16, 8)->default(0.0);
            $table->boolean('is_promo_code_commission_default')->default(true);
            $table->float('promo_code_commission', 16, 8)->default(0.0);
            $table->boolean('is_promo_code_bonus_default')->default(true);
            $table->float('promo_code_bonus', 16, 8)->default(0.0);
            $table->string('team_member', 255)->nullable(true)->index();
            $table->string('last_ip', 20)->nullable(true);
            $table->float('total_invested_balance', 16, 8)->default(0.0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_tid_block');
            $table->dropColumn('is_deposit_block');
            $table->dropColumn('is_withdraw_block');
            $table->dropColumn('is_withdraw_default');
            $table->dropColumn('withdraw_limit');
            $table->dropColumn('is_promo_code_commission_default');
            $table->dropColumn('promo_code_commission');
            $table->dropColumn('is_promo_code_bonus_default');
            $table->dropColumn('promo_code_bonus');
            $table->dropColumn('team_member');
            $table->dropColumn('last_ip');
            $table->dropColumn('total_invested_balance');
        });
    }
}
