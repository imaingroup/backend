<?php

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds field previous_percent
 * to table trade_options.
 */
final class AddPreviousPercentToTradeOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->float('previous_percent', 16, 8)
                ->after('accrual_period')
                ->nullable(true)
                ->default(null);
        });

        DB::table('data_rows')
            ->insert([
                'data_type_id' => DB::table('data_types')
                    ->where('name', 'trade_options')
                    ->first()
                    ->id,
                'field' => 'previous_percent',
                'type' => 'number',
                'display_name' => 'Previous Percent',
                'required' => 0,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => '{"validation":{"rule":"nullable|numeric"}}',
                'order' => 22
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->dropColumn('previous_percent');
        });

        DB::table('data_rows')
            ->where([
                'data_type_id' => DB::table('data_types')
                    ->where('name', 'trade_options')
                    ->first()
                    ->id,
                'field' => 'previous_percent',
            ])
            ->delete();
    }
}
