<?php


namespace App\Modules\Investments\Repositories;

use App\Modules\Investments\Models\TradeAccruals;
use App\Modules\Core\Repositories\BaseRepository;
use DateTime;

/**
 * Trade accruals repository.
 */
final class TradeAccrualsRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Investments\Models\TradeAccruals $model
     */
    public function __construct(protected TradeAccruals $model)
    {}

    /**
     * Exist by trade id and date.
     *
     * @param int $tradeId
     * @param DateTime $date
     * @return bool
     */
    public function existByDate(int $tradeId, DateTime $date): bool
    {
        return $this->model
            ->newQuery()
            ->where('trades_id', $tradeId)
            ->where('date', $date)
            ->exists();
    }

    /**
     * Get last by trade id.
     *
     * @param int $tradeId
     * @return \App\Modules\Investments\Models\TradeAccruals|null
     */
    public function getLast(int $tradeId): ?TradeAccruals
    {
        return $this->model
            ->newQuery()
            ->where('trades_id', $tradeId)
            ->orderBy('date', 'DESC')
            ->limit(1)
            ->first();
    }
}
