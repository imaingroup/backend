<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ShowColumnInTidHistoryInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('data_rows')
            ->where('data_type_id', 44)
            ->where('field', 'id')
            ->update([
                'type' => 'number',
                'browse' => 1
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('data_rows')
            ->where('data_type_id', 44)
            ->where('field', 'id')
            ->update([
                'type' => 'number',
                'browse' => 0
            ]);
    }
}
