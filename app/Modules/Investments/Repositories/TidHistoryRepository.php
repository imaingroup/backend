<?php


namespace App\Modules\Investments\Repositories;

use App\Modules\Investments\Models\TidHistory;
use App\Modules\Core\Repositories\BaseRepository;
use DateTime;
use Illuminate\Database\Eloquent\Collection;

/**
 * Tid history repository.
 */
final class TidHistoryRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Investments\Models\TidHistory $model
     */
    public function __construct(protected TidHistory $model)
    {}

    /**
     * Find by date.
     *
     * @param DateTime $date
     * @return TidHistory|null
     */
    public function findByDate(DateTime $date): ?TidHistory
    {
        return $this->model
            ->newQuery()
            ->where('date', $date->format('Y-m-d'))
            ->first();
    }

    /**
     * Get data when date great then parameter.
     *
     * @param DateTime $gtDate
     * @return Collection
     */
    public function getByGtDate(DateTime $gtDate): Collection
    {
        return $this->model
            ->newQuery()
            ->where('date', '>', $gtDate)
            ->orderBy('date', 'ASC')
            ->get();
    }

    /**
     * Delete feature data.
     */
    public function deleteFeatureData(): void
    {
        $this->model
            ->newQuery()
            ->where('date', '>', new DateTime())
            ->delete();
    }
}
