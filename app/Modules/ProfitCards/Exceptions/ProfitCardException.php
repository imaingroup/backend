<?php


namespace App\Modules\ProfitCards\Exceptions;


use Exception;

/**
 * This exception may throwing in profit card service.
 */
final class ProfitCardException extends Exception
{

}
