<?php


namespace App\Modules\Core\Validation;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use stdClass;
use Throwable;

/**
 * This class validating recaptcha.
 */
final class ReCaptcha
{
    /**
     * Validation recaptcha.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator): bool
    {
        try {
            $data = $this->executeRequest(env('GOOGLE_RECAPTCHA_SECRET_V3'), $value);

            if (!$data->success) {
                $data = $this->executeRequest(env('GOOGLE_RECAPTCHA_SECRET_V2'), $value);
            }

            if (isset($data->score) && (float)$data->score < 0.5) {
                return false;
            }

            return $data->success;
        } catch (Throwable) {
            return false;
        }
    }

    /**
     * Execute request for validate token.
     *
     * @param string $secret
     * @param string $token
     * @return StdClass
     * @throws GuzzleException
     */
    private function executeRequest(string $secret, string $token): stdClass
    {
        $client = new Client();
        $response = $client->post('https://www.google.com/recaptcha/api/siteverify',
            [
                'form_params' =>
                    [
                        'secret' => $secret,
                        'response' => $token
                    ]
            ]
        );

        return json_decode((string)$response->getBody());
    }
}
