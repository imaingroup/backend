<?php

if(strpos($_SERVER['REQUEST_URI'], '/api/') !== false) {
    header('Content-Type: application/json', true, 503);
    echo '{"status": false, "maintance": true}';
}
else {?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Insider Protocol</title>
    <meta charset="UTF-8">
    <meta content="width=device-width" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="telephone=no" name="format-detection">
    <meta content="address=no" name="format-detection">
    <style>
        @font-face {
            font-family: 'titillium';
            src: url('/fonts/titilliumweb-light.eot');
            src: url('/fonts/titilliumweb-light.eot?#iefix') format('embedded-opentype'),
            url('/fonts/titilliumweb-light.woff') format('woff'),
            url('/fonts/titilliumweb-light.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        html, body {
            height: 100%;
        }
        body {
            padding: 0;
            background: url(/images/background.jpg) center/cover no-repeat;
            font-family: 'titillium';
            font-size: 1vw;
            line-height: 1;
        }
        .telegram,
        .youtube {
            width: 16.2em;
            height: 3.65em;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: 4.5em;
            background: center/contain no-repeat;
        }
        .telegram {
            margin-left: -8.5em;
            background-image: url(/images/telegram.png);
        }
        .telegram:hover {
            background-image: url(/images/telegram-hover.png);
        }
        .youtube {
            margin-left: 6.5em;
            background-image: url(/images/youtube.png);
        }
        .youtube:hover {
            background-image: url(/images/youtube-hover.png);
        }
        footer {
            position: absolute;
            right: 0;
            bottom: 5em;
            left: 0;
            font-size: .65em;
            color: #5d858d;
            text-align: center;
            text-transform: uppercase;
        }
        footer span {
            display: inline-block;
            font-family: Arial, Helvetica, sans-serif;
            vertical-align: middle;
        }
    </style>
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script>
        setInterval(function () {
            $.get('/api/landing/content', function() {
                window.location.reload();
            });
        }, 1000);
    </script>
</head>
<body>
<a target="_blank" href="https://t.me/insiderprotocol" class="telegram"></a>
<a target="_blank" href="https://www.youtube.com/channel/UCmlbF0cq4Mul4EyDB5Na9JA" class="youtube"></a>
<footer><span>&copy;</span> <?php echo date('Y', time());?> Trading Platform</footer>
</body>
</html>
<?php } ?>
