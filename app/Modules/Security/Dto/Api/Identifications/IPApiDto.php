<?php
declare(strict_types=1);

namespace App\Modules\Security\Dto\Api\Identifications;

use App\Modules\Core\Dto\BaseDto;

/**
 * HistoryApiDto.
 *
 * @property-read string|null $ip
 * @property-read string|null $country
 * @property-read string|null $firstDate
 * @property-read bool $isAccurate
 */
final class IPApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param string|null $ip
     * @param string|null $country
     * @param string|null $firstDate
     * @param bool $isAccurate
     */
    public function __construct(
        protected ?string $ip,
        protected ?string $country,
        protected ?string $firstDate,
        protected bool $isAccurate,
    )
    {
    }

    /**
     * Get key.
     *
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->ip;
    }
}
