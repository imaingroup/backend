<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * GasPriceException.
 */
final class GasPriceException extends EthereumException
{
}
