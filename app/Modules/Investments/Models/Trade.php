<?php

namespace App\Modules\Investments\Models;

use App\Modules\Investments\Models\TradeHistory;
use App\Modules\Money\Models\Transaction;
use App\Modules\Core\Models\Model;
use App\Modules\Investments\Models\TradeOption;
use App\Modules\Users\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use function sprintf;

/**
 * Trade model.
 */
final class Trade extends Model
{
    /**
     * {@inheritDoc}
     */
    public function getRouteKeyName(): string
    {
        return 'tid';
    }

    /**
     * Trade history list relation.
     *
     * @return HasMany
     */
    public function history(): HasMany
    {
        return $this->hasMany(TradeHistory::class, 'trades_id');
    }

    /**
     * Trade option relation.
     *
     * @return BelongsTo
     */
    public function tradeOption(): BelongsTo
    {
        return $this->belongsTo(TradeOption::class, 'trade_options_id');
    }

    /**
     * User relation.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Transaction related relation.
     *
     * @return BelongsTo
     */
    public function transactionRelated(): BelongsTo
    {
        return $this->belongsTo(\App\Modules\Money\Models\Transaction::class, 'related_transactions_id');
    }

    /**
     * Get tid number in text representation.
     *
     * @return string
     */
    public function getTidNumberText(): string
    {
        return sprintf('T%s', $this->tid);
    }

    /**
     * Check is owner by user.
     *
     * @param User $user
     * @return bool
     */
    public function isOwner(User $user): bool
    {
        return $this->users_id === $user->id;
    }

    /**
     * Export.
     *
     * @return array
     */
    public function export(): array
    {
        return [
            'id' => $this->id,
            'opened_at' => $this->opened_at,
            'trade_options_id' => $this->trade_options_id,
            'users_id' => $this->users_id,
            'amount' => $this->amount,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'unrealized_pnl' => $this->unrealized_pnl,
            'period' => $this->period,
            'is_open' => $this->is_open,
            'closed_at' => $this->closed_at,
            'rate_abc_usdt' => $this->rate_abc_usdt,
            'closed_pnl' => $this->closed_pnl,
            'is_money_released' => $this->is_money_released,
            'related_transactions_id' => $this->related_transactions_id,
            'floating_pnl' => $this->floating_pnl,
            'tid' => $this->tid,
            'accrual_period' => $this->accrual_period,
            'accruals' => $this->accruals,
        ];
    }
}
