<?php
declare(strict_types=1);

namespace App\Modules\Investments\Observers;

use App\Modules\Investments\Models\TradeOption;
use App\Modules\Admin\Services\TidHistoryService;

/**
 * Observer for creating or changing model TradeOption.
 */
final class TradeOptionObserver
{
    /** @var \App\Modules\Admin\Services\TidHistoryService Tid history service. */
    private TidHistoryService $tidHistoryService;

    /**
     * Construct.
     *
     * @param TidHistoryService $tidHistoryService
     */
    public function __construct(TidHistoryService $tidHistoryService)
    {
        $this->tidHistoryService = $tidHistoryService;
    }

    /**
     * Handle the trade option "created" event.
     *
     * @param TradeOption $tradeOption
     * @return void
     */
    public function created(TradeOption $tradeOption): void
    {
        $this->tidHistoryService->createForTradeOption($tradeOption);
    }

    /**
     * Handle the trade option "updating" event.
     *
     * @param TradeOption $tradeOption
     * @return void
     */
    public function updating(TradeOption $tradeOption): void
    {
        if ($tradeOption->isDirty('percent')) {
            $this->tidHistoryService->updateForTradeOption($tradeOption);
        }
    }
}
