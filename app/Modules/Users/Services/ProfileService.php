<?php


namespace App\Modules\Users\Services;


use App\Modules\Users\Exceptions\ProfileException;
use App\Modules\Users\Models\User;
use DateTime;
use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException;
use PragmaRX\Google2FA\Exceptions\InvalidCharactersException;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;
use PragmaRX\Google2FALaravel\Google2FA;

/**
 * Profile service.
 */
final class ProfileService
{
    /**
     * Get profile content.
     *
     * @param User $user
     * @return array
     */
    public function content(User $user): array
    {
        $data = [
            'account' => [
                'username' => $user->username,
                'email' => $user->email,
                'avatar' => Storage::disk('local')->url($user->avatar),
                'country' => $user->country,
                'birthdate' => $user->birthdate,
                'gender' => $user->gender,
                'bio' => $user->bio,
                'is_trader' => $user->is_trader,
                'is_hide_from_public' => $user->is_hide_from_public,
                'btc_address' => $user->btc,
                'eth_address' => $user->eth,
                'promo_code' => $user->promo_code,
                'promo_link' => urldecode(url('/', sprintf('#/register/%s', $user->promo_code))),
            ],
            '2fa' => [
                'enabled' => $user->is_2fa,
            ],
            'notifications' => [
                'enable_deposit_notifications' => $user->is_enabled_deposit_notifications,
                'enable_withdrawal_notifications' => $user->is_enabled_withdrawal_notifications,
                'enable_purchase_notifications' => $user->is_enabled_purchase_notifications,
                'enable_password_change_notifications' => $user->is_enabled_password_change_notifications
            ]
        ];

        $data['countries'] = config('country');

        return $data;
    }

    /**
     * Save profile data.
     *
     * @param User $user
     * @param array $data
     * @throws Exception
     */
    public function save(User $user, array $data): void
    {
        $user->refresh();

        $user->username = $data['username'];

        if (isset($data['avatar'])) {
            /** @var UploadedFile $avatar */
            $avatar = $data['avatar'];
            $ext = mime_content_type($avatar->getPathname()) == 'image/png' ? 'png' : 'jpg';
            $path = \sprintf('users/%s.%s', Str::random(40), $ext);

            if ($user->avatar && $user->avatar !== User::DEFAULT_AVATAR) {
                Storage::disk('public')->delete($user->avatar);
            }

            Storage::disk('public')->put($path, file_get_contents($avatar->getPathname()));

            $user->avatar = $path;
        }

        $user->country = $data['country'] ?? null;
        $user->birthdate = isset($data['birthdate']) ? new DateTime($data['birthdate']) : null;
        $user->gender = $data['gender'] ?? null;
        $user->bio = $data['bio'] ?? null;
        $user->is_trader = $data['is_trader'] ?? 0;
        $user->is_hide_from_public = $data['is_hide_from_public'] ?? 0;
        $user->btc = $data['btc_address'] ?? null;
        $user->eth = $data['eth_address'] ?? null;

        $user->save();
    }

    /**
     * Change password.
     *
     * @param User $user
     * @param string $password
     */
    public function changePassword(User $user, string $password): void
    {
        $user->refresh();

        $user->password = bcrypt($password);

        $user->save();
    }

    /**
     * Save notifications.
     *
     * @param User $user
     * @param array $data
     */
    public function saveNotifications(User $user, array $data): void
    {
        $user->refresh();

        $user->is_enabled_deposit_notifications = $data['enable_deposit_notifications'] ?? 0;
        $user->is_enabled_withdrawal_notifications = $data['enable_withdrawal_notifications'] ?? 0;
        $user->is_enabled_purchase_notifications = $data['enable_purchase_notifications'] ?? 0;
        $user->is_enabled_password_change_notifications = $data['enable_password_change_notifications'] ?? 0;

        $user->save();
    }

    /**
     * Enable 2fa.
     *
     * @param User $user
     * @return array
     * @throws ProfileException
     * @throws IncompatibleWithGoogleAuthenticatorException
     * @throws InvalidCharactersException
     * @throws SecretKeyTooShortException
     */
    public function enable2fa(User $user): array
    {
        if ($user->is_2fa) {
            throw new ProfileException('You need disable 2fa as first');
        }

        /** @var Google2FA $google2fa */
        $google2fa = app('pragmarx.google2fa');
        $google2faSecret = $google2fa->generateSecretKey();

        $qrImage = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $google2faSecret
        );

        return [
            'secret' => $google2faSecret,
            'qr_image' => $qrImage
        ];
    }

    /**
     * Enable and set 2fa secret code.
     *
     * @param User $user
     * @param string $secret
     */
    public function set2fa(User $user, string $secret): void
    {
        $user->refresh();

        $user->is_2fa = true;
        $user->google2fa_secret = $secret;

        $user->save();
    }

    /**
     * Disable 2fa.
     *
     * @param User $user
     */
    public function disable2fa(User $user): void
    {
        $user->refresh();

        $user->is_2fa = false;
        $user->google2fa_secret = null;

        $user->save();
    }
}
