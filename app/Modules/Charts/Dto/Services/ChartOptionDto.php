<?php
declare(strict_types=1);

namespace App\Modules\Charts\Dto\Services;

use App\Modules\Core\Dto\BaseDto;
use Illuminate\Support\Collection;

/**
 * Chart by option dto.
 */
final class ChartOptionDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param Collection $ttl
     * @param Collection $pnl
     * @param Collection $days
     * @param Collection $optionInfo
     */
    public function __construct(
        protected Collection $ttl,
        protected Collection $pnl,
        protected Collection $days,
        protected Collection $optionInfo,
    )
    {
    }
}
