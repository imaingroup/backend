<?php
declare(strict_types=1);

namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsStyles;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Settings team repository.
 */
final class SettingsStylesRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param SettingsStyles $model
     */
    public function __construct(protected SettingsStyles $model)
    {}

    /**
     * Get settings model.
     *
     * @return SettingsStyles
     */
    public function get(): SettingsStyles
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
