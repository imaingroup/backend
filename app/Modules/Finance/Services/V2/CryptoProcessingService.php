<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services\V2;

use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Enum\NetworkTypeEnum;
use App\Modules\Finance\Events\CryptoTransactionUpdatedEvent;
use App\Modules\Finance\Exceptions\Services\BitQuery\RequestException;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Models\CryptoBalance;
use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Finance\Models\CryptoTransaction;
use App\Modules\Finance\Repositories\CryptoAddressRepository;
use App\Modules\Finance\Repositories\CryptoBalanceRepository;
use App\Modules\Finance\Repositories\CryptoCurrencyRepository;
use App\Modules\Finance\Repositories\CryptoTransactionRepository;
use App\Modules\Finance\Services\BitQueryService;
use App\Modules\Finance\Services\V2\Ethereum\ManageService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use stdClass;

/**
 * CryptoProcessingService.
 */
final class CryptoProcessingService
{
    /**
     * Constructor.
     *
     * @param Collection $_cacheHighestBlocks
     */
    public function __construct(
        private Collection $_cacheHighestBlocks = new Collection(),
    )
    {
    }

    /**
     * Update transactions with ethereum type.
     *
     * @param bool $all
     * @return void
     * @throws RequestException
     * @throws TransactionNotStarted
     */
    public function updateTransactionsEthereumType(bool $all = false): void
    {
        $addresses = $all ?
            $this->getCryptoAddressRepository()
                ->getByNetworkType(NetworkTypeEnum::ETHEREUM) :
            $this->getCryptoAddressRepository()
                ->getObservedByNetworkType(NetworkTypeEnum::ETHEREUM);

        $addresses
            ->each(function (CryptoAddress $address) {
                DB::transaction(function () use ($address) {
                    CryptoAddress::refreshAndLockForUpdate($address);

                    $network = $address->currency->network->toEnum();
                    $highestBlock = $this->getHighestBlockEthereumType($network);

                    collect(
                        $this->getBitQueryService()
                            ->getIncomeTransfers($network, $address->address)
                            ->data
                            ->ethereum
                            ->transfers
                    )
                        ->each(fn(stdClass $transfer) => $this->processingEthereumTransfer($transfer, $network, $address, $highestBlock));

                    $address->last_check_at = now();
                    $address->cnt_check = $address->cnt_check + 1;

                    $address->save();

                    if (config('finance.bitquery.graphql.sleep_per_query') > 0) {
                        sleep(config('finance.bitquery.graphql.sleep_per_query'));
                    }
                });
            });
    }

    /**
     * Update balances with ethereum type.
     *
     * @return void
     */
    public function updateBalancesEthereumType(): void
    {
        $this->getCryptoAddressRepository()
            ->getObservedBalancesByNetworkType(NetworkTypeEnum::ETHEREUM)
            ->each(function (CryptoAddress $address) {
                $this->updateBalanceEthereumType($address);

                if (config('finance.ethereum.requests_delay') > 0) {
                    sleep(config('finance.ethereum.requests_delay'));
                }
            });
    }

    /**
     * Update balance ethereum type.
     *
     * @param CryptoAddress $address
     * @return void
     */
    public function updateBalanceEthereumType(CryptoAddress $address): void
    {
        DB::transaction(function () use ($address) {
            CryptoAddress::refreshAndLockForUpdate($address);

            $nativeCurrency = $this->getDataGetterService()
                ->findMainCurrencyByNetwork($address->currency->network->toEnum());

            $nativeBalance = $this->getManageService()
                ->getBalance($nativeCurrency->network->toEnum(), $address->address);

            $this->updateBalance(
                $address,
                $nativeCurrency,
                (float)((float)$nativeBalance->balance / pow(10, $nativeCurrency->decimals))
            );

            $address->balances
                ->filter(fn(CryptoBalance $cryptoBalance) => !$cryptoBalance->currency->isMainCurrencyInNetwork)
                ->each(function (CryptoBalance $cryptoBalance) use ($address) {
                    $balance = $this->getManageService()
                        ->getTokenBalance($cryptoBalance->currency->network->toEnum(), $cryptoBalance->currency->address, $address->address);

                    $this->updateBalance(
                        $address,
                        $cryptoBalance->currency,
                        (float)((float)$balance->balance / pow(10, $cryptoBalance->currency->decimals))
                    );
                });

            $address->last_check_at = now();
            $address->cnt_check = $address->cnt_check + 1;

            $address->save();
        });
    }

    /**
     * Processing ethereum transfer.
     *
     * @param stdClass $transfer
     * @param EthereumNetworkEnum $network
     * @param CryptoAddress $address
     * @param int $highestBlock
     * @return void
     */
    private function processingEthereumTransfer(stdClass $transfer, EthereumNetworkEnum $network, CryptoAddress $address, int $highestBlock): void
    {
        $currencyModel = $this->getCryptoCurrencyRepository()
            ->findByAddressAndNetwork(
                $transfer->currency->address === '-' ? null : $transfer->currency->address,
                $network
            );

        if ($currencyModel) {
            if (!$currencyModel->market) {
                $this->getDataFetcherService()
                    ->updateCurrencyMarket($currencyModel);

                $currencyModel->refresh();

                if (!$currencyModel->market) {
                    $this->getDataFetcherService()
                        ->setCurrencyMarket($currencyModel, 0);

                    $currencyModel->refresh();
                }
            }

            $transaction = $this->getCryptoTransactionRepository()
                ->getByTxid($transfer->transaction->hash);

            if (!$transaction) {
                $transaction = new CryptoTransaction();
                $transaction->currency()
                    ->associate($currencyModel);
                $transaction->txid = $transfer->transaction->hash;
                $transaction->eth_from = $transfer->transaction->txFrom->address;
                $transaction->amount = $transfer->amount;
                $transaction->address()
                    ->associate($address);

                $this->updateBalance($address, $currencyModel, (float)$transfer->amount, true);
            }

            $transaction->confirmations = $highestBlock - $transfer->block->height;
            $this->updateBalance($address, $currencyModel, (float)$transfer->amount, true);
            $updated = $transaction->isDirty();

            $transaction->save();

            if ($updated) {
                event(new CryptoTransactionUpdatedEvent($transaction));
            }
        }
    }

    /**
     * Update balance.
     *
     * @param CryptoAddress $address
     * @param CryptoCurrency $currency
     * @param float $amount
     * @param bool $onlyNew
     * @return void
     */
    private function updateBalance(CryptoAddress $address, CryptoCurrency $currency, float $amount, bool $onlyNew = false): void
    {
        $balance = $this->getCryptoBalanceRepository()
            ->findByAddressAndCurrency($address->id, $currency->id);

        if (!$balance) {
            $balance = new CryptoBalance();
            $balance->address()
                ->associate($address);
            $balance->currency()
                ->associate($currency);
            $balance->balance = $amount;
        }

        if (!$onlyNew) {
            $balance->balance = $amount;
        }

        $balance->save();
    }

    /**
     * Get the highest block height in ethereum networks.
     *
     * @param EthereumNetworkEnum $network
     * @return int
     * @throws RequestException
     */
    private function getHighestBlockEthereumType(EthereumNetworkEnum $network): int
    {
        if ($this->_cacheHighestBlocks->has($network->value)) {
            return $this->_cacheHighestBlocks->get($network->value);
        }

        $blockHeight = $this->getBitQueryService()
            ->getHighestBlock($network)
            ->data
            ->ethereum
            ->blocks[0]
            ->height;

        $this->_cacheHighestBlocks->put($network->value, $blockHeight);

        return $blockHeight;
    }

    /**
     * Get {@see CryptoAddressRepository::class}.
     *
     * @return CryptoAddressRepository
     */
    private function getCryptoAddressRepository(): CryptoAddressRepository
    {
        return app(CryptoAddressRepository::class);
    }

    /**
     * Get {@see CryptoTransactionRepository::class}.
     *
     * @return CryptoTransactionRepository
     */
    private function getCryptoTransactionRepository(): CryptoTransactionRepository
    {
        return app(CryptoTransactionRepository::class);
    }

    /**
     * Get {@see CryptoCurrencyRepository::class}.
     *
     * @return CryptoCurrencyRepository
     */
    private function getCryptoCurrencyRepository(): CryptoCurrencyRepository
    {
        return app(CryptoCurrencyRepository::class);
    }

    /**
     * Get {@see CryptoBalanceRepository::class}.
     *
     * @return CryptoBalanceRepository
     */
    private function getCryptoBalanceRepository(): CryptoBalanceRepository
    {
        return app(CryptoBalanceRepository::class);
    }

    /**
     * Get {@see BitQueryService::class}.
     *
     * @return BitQueryService
     */
    private function getBitQueryService(): BitQueryService
    {
        return app(BitQueryService::class);
    }

    /**
     * Get {@see ManageService::class}.
     *
     * @return ManageService
     */
    private function getManageService(): ManageService
    {
        return app(ManageService::class);
    }

    /**
     * Get {@see DataGetterService::class}.
     *
     * @return DataGetterService
     */
    private function getDataGetterService(): DataGetterService
    {
        return app(DataGetterService::class);
    }

    /**
     * Get {@see DataFetcherService::class}.
     *
     * @return DataFetcherService
     */
    private function getDataFetcherService(): DataFetcherService
    {
        return app(DataFetcherService::class);
    }
}
