<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds deleted field
 * to messages_chats_users table.
 */
final class AddFieldDeleteToChatUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages_chats_users', function (Blueprint $table) {
            $table->boolean('deleted')->after('is_owner')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages_chats_users', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });
    }
}
