<?php

namespace App\Modules\Settings\Models;

use App\Modules\Core\Models\Model;

final class SettingsTeam extends Model
{
    protected $table = 'settings_team';
}
