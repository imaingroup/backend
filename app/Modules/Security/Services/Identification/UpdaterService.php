<?php
declare(strict_types=1);

namespace App\Modules\Security\Services\Identification;

use App\Modules\Security\Models\UserIdentification;
use App\Modules\Security\Models\UserIdentificationRelation;
use App\Modules\Security\Models\UserIdentificationUserRelation;
use App\Modules\Users\Models\User;
use App\Modules\Users\Repositories\UserRepository;
use Throwable;

/**
 * Identification service.
 */
final class UpdaterService
{
    /**
     * Update multi accounts.
     *
     * @throws Throwable
     */
    public function updateMultiAccounts(): void
    {
        $limit = 100;

        $count = $this->getUserRepository()->count();
        $parts = ceil($count / $limit);

        for ($i = 0; $i < $parts; $i++) {
            $this->getUserRepository()
                ->getWithLimitOffset($limit, $i * $limit)
                ->each(fn(User $user) => $this->updateMultiAccountsByUser($user));
        }
    }

    /**
     * Update multi accounts by user.
     *
     * @param User $user
     * @return void
     */
    public function updateMultiAccountsByUser(User $user): void
    {
        $report = $this->getIdentificationService()
            ->getByUser($user, false);
        $user->multi_accounts = $report->getRelatedAccurate()->count() +
            $report->getRelatedHighProbably()->count();

        $report->getRelatedAccurate()
            ->each(function (User $userRelated) use ($report, $user) {
                $this->updateUserToUserRelation($user, $userRelated, true);

                $report->relatedIdentificationsAccurate
                    ->each(fn(UserIdentification $identification) => $this->updateUserIdentification($identification, $userRelated, true));
            });

        $report->getRelatedHighProbably()
            ->each(function (User $userRelated) use ($report, $user) {
                $this->updateUserToUserRelation($user, $userRelated, false);

                $report->relatedIdentificationsHighProbably
                    ->each(fn(UserIdentification $identification) => $this->updateUserIdentification($identification, $userRelated, false));
            });

        $user->save();
    }

    /**
     * Update user to user relation.
     *
     * @param User $mainUser
     * @param User $user
     * @param bool $isAccurate
     * @return void
     */
    public function updateUserToUserRelation(User $mainUser, User $user, bool $isAccurate): void
    {
        if (!UserIdentificationUserRelation::query()
                ->where('users_main_id', $mainUser->id)
                ->where('users_id', $user->id)
                ->exists() && $mainUser->id !== $user->id
        ) {
            $relation = new UserIdentificationUserRelation();
            $relation->mainUser()
                ->associate($mainUser);
            $relation->user()
                ->associate($user);
            $relation->is_accurate = $isAccurate;
            $relation->save();
        }
    }

    /**
     * Update user identification.
     *
     * @param UserIdentification|int $identification
     * @param User|int $user
     * @param bool $isAccurate
     * @return void
     */
    public function updateUserIdentification(UserIdentification|int $identification, User|int $user, bool $isAccurate): void
    {
        if (!UserIdentificationRelation::query()
            ->where('users_identifications_id', $identification instanceof UserIdentification ? $identification->id : $identification)
            ->where('users_id', $user instanceof User ? $user->id : $user)
            ->exists()) {
            $relation = new UserIdentificationRelation();

            if ($identification instanceof UserIdentification) {
                $relation->userIdentification()
                    ->associate($identification);
            } else {
                $relation->users_identifications_id = $identification;
            }

            if ($user instanceof User) {
                $relation->user()
                    ->associate($user);
            } else {
                $relation->users_id = $user;
            }

            $relation->is_accurate = $isAccurate;
            $relation->save();
        }
    }

    /**
     * Get {@see IdentificationService::class}.
     *
     * @return IdentificationService
     */
    private function getIdentificationService(): IdentificationService
    {
        return app(IdentificationService::class);
    }

    /**
     * Get {@see UserRepository::class}.
     *
     * @return UserRepository
     */
    private function getUserRepository(): UserRepository
    {
        return app(UserRepository::class);
    }
}
