<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Exceptions;

/**
 * UnsupportedCurrencyException.
 */
final class UnsupportedCurrencyException extends WithdrawException
{
}
