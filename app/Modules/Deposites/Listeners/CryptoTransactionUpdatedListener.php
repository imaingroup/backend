<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Listeners;

use App\Facades\DB;
use App\Modules\Deposites\Services\DepositService;
use App\Modules\Finance\Events\CryptoTransactionUpdatedEvent;

/**
 * CryptoTransactionUpdatedListener.
 */
final class CryptoTransactionUpdatedListener
{
    /**
     * Handle.
     *
     * @param CryptoTransactionUpdatedEvent $event
     * @return void
     */
    public function handle(CryptoTransactionUpdatedEvent $event)
    {
        DB::afterCommit(fn() => $this->getDepositService()->monitoring());
    }

    /**
     * Get {@see DepositService::class}.
     *
     * @return DepositService
     */
    private function getDepositService(): DepositService
    {
        return app(DepositService::class);
    }
}
