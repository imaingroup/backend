<?php
declare(strict_types=1);

namespace App\Modules\Finance\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Finance\Enum\NetworkTypeEnum;
use App\Modules\Finance\Models\CryptoAddress;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

/**
 * CryptoAddress repository.
 */
final class CryptoAddressRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param CryptoAddress $model
     */
    public function __construct(protected CryptoAddress $model)
    {
    }

    /**
     * Get observed addresses.
     *
     * @param int|array $currency
     * @return Collection
     */
    public function getObserved(int|array $currency): Collection
    {
        $query = $this->getBaseObservedQuery()
            ->orderBy('last_check_at', 'ASC');

        if (is_int($currency)) {
            $query
                ->where('type', $currency);
        } else {
            $query
                ->whereIn('type', $currency);
        }

        return $query->get();
    }

    /**
     * Get observed addresses by network type.
     *
     * @param NetworkTypeEnum $networkType
     * @return Collection
     */
    public function getByNetworkType(NetworkTypeEnum $networkType): Collection
    {
        return $this->model
            ->newQuery()
            ->whereHas('currency', static function(Builder $builder) use($networkType) {
                $builder->whereHas('network', static function(Builder $builder) use($networkType) {
                    $builder->where('type', $networkType->value);
                });
            })
            ->orderBy('last_check_at', 'ASC')
            ->get();
    }

    /**
     * Get observed addresses by network type.
     *
     * @param NetworkTypeEnum $networkType
     * @return Collection
     */
    public function getObservedByNetworkType(NetworkTypeEnum $networkType): Collection
    {
        return $this->model
            ->newQuery()
            ->where(static function(Builder $builder) {
                $builder->where('monitoring_before_at', '>=', new DateTime())
                    ->orWhereHas('cryptoTransactions', static function(Builder $builder) {
                        $builder->where('confirmations', '<', DB::raw('crypto_addresses.min_conf'));
                    });
            })
            ->where('is_archived', false)
            ->whereHas('currency', static function(Builder $builder) use($networkType) {
                $builder->whereHas('network', static function(Builder $builder) use($networkType) {
                    $builder->where('type', $networkType->value);
                });
            })
            ->orderBy('last_check_at', 'ASC')
            ->get();
    }

    /**
     * Get observed balances addresses by network type.
     *
     * @param NetworkTypeEnum $networkType
     * @return Collection
     */
    public function getObservedBalancesByNetworkType(NetworkTypeEnum $networkType): Collection
    {
        return $this->model
            ->newQuery()
            ->where(static function(Builder $builder) {
                $builder->where('monitoring_before_at', '>=', new DateTime())
                    ->orWhereHas('balances', static function(Builder $builder) {
                        $builder->where('balance', '>', 0);
                    });
            })
            ->where('is_archived', false)
            ->whereHas('currency', static function(Builder $builder) use($networkType) {
                $builder->whereHas('network', static function(Builder $builder) use($networkType) {
                    $builder->where('type', $networkType->value);
                });
            })
            ->orderBy('last_check_at', 'ASC')
            ->get();
    }

    /**
     * Get observed balances addresses by network type.
     *
     * @param NetworkTypeEnum $networkType
     * @return Collection
     */
    public function getWithBalancesByNetworkType(NetworkTypeEnum $networkType): Collection
    {
        return $this->model
            ->newQuery()
            ->where(static function(Builder $builder) {
                $builder->whereHas('balances', static function(Builder $builder) {
                        $builder->where('balance', '>', 0);
                    });
            })
            ->whereHas('currency', static function(Builder $builder) use($networkType) {
                $builder->whereHas('network', static function(Builder $builder) use($networkType) {
                    $builder->where('type', $networkType->value);
                });
            })
            ->get();
    }

    /**
     * Find by address.
     *
     * @param string $address
     * @return CryptoAddress|null
     */
    public function findByAddress(string $address): ?CryptoAddress
    {
        return $this->model
            ->newQuery()
            ->where('address', $address)
            ->first();
    }

    /**
     * Get ETH addresses with balance.
     *
     * @param bool $onlyEth
     * @return Collection
     */
    public function getEthWithBalance(bool $onlyEth = false): Collection
    {
        $query = $this->model
            ->newQuery()
            ->where('type', CryptoAddress::TYPE_ETH)
            ->where('eth_balance', '>', 0)
            ->orderBy('eth_balance', 'DESC');

        if ($onlyEth) {
            $query->where('usdt_balance', '=', 0);
        } else {
            $query->orWhere('usdt_balance', '>', 0)
                ->orderBy('usdt_balance', 'DESC');
        }

        return $query->get();
    }

    /**
     * Get addresses with balances by types.
     *
     * @param array $types
     * @return Collection
     */
    public function getAddressesWithBalanceByTypes(array $types): Collection
    {
        $query = $this->model
            ->newQuery()
            ->whereIn('type', $types)
            ->where(static function(Builder $builder) {
                $builder->where('eth_balance', '>', 0)
                    ->orWhere('usdt_balance', '>', 0);
            })
            ->orderBy('eth_balance', 'DESC')
            ->orderBy('usdt_balance', 'DESC');

        return $query->get();
    }

    /**
     * Get ETH addresses with balance.
     *
     * @return Collection
     */
    public function getUsdtWithBalance(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('type', CryptoAddress::TYPE_ETH)
            ->where('usdt_balance', '>', 0)
            ->orderBy('usdt_balance', 'DESC')
            ->get();
    }

    /**
     * Get by address.
     *
     * @param string $address
     * @param bool $lockForUpdate
     * @return CryptoAddress|null
     */
    public function getByAddress(string $address, bool $lockForUpdate = false): ?CryptoAddress
    {
        $query = $this->model
            ->newQuery()
            ->where('address', $address);

        if ($lockForUpdate) {
            $query->lockForUpdate();
        }

        return $query->first();
    }

    /**
     * Get base observed query.
     *
     * @return Builder
     */
    private function getBaseObservedQuery(): Builder
    {
        return $this->model
            ->newQuery()
            ->where('monitoring_before_at', '>=', new DateTime());
    }
}
