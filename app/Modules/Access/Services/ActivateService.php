<?php


namespace App\Modules\Access\Services;

use App\Modules\Notifications\Jobs\SendActivationAccountEmailJob;
use App\Modules\Users\Models\User;

/**
 * Service for activate user accounts.
 */
final class ActivateService
{
    /**
     * Activate.
     *
     * @param \App\Modules\Users\Models\User $user
     */
    public function activate(User $user): void
    {
        $user->refresh();

        $user->email_verified_at = new \DateTime();

        $user->save();
    }

    /**
     * Resend activation link.
     *
     * @param \App\Modules\Users\Models\User $user
     */
    public function resend(User $user): void
    {
        SendActivationAccountEmailJob::dispatch($user);
    }
}
