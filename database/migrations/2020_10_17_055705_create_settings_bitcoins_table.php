<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creating table settings_bitcoins
 * for store bitcoin settings and token.
 */
class CreateSettingsBitcoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_bitcoin', function (Blueprint $table) {
            $table->id();
            $table->string('uri_api', 255);
            $table->string('username', 255);
            $table->string('password', 255);
            $table->tinyInteger('minconf');
            $table->dateTime('token_expiration_at')->nullable(true);
            $table->text('token')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_bitcoin');
    }
}
