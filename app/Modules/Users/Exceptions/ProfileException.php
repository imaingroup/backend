<?php


namespace App\Modules\Users\Exceptions;


use Exception;

/**
 * This exception may throwing in profile service.
 */
final class ProfileException extends Exception
{

}
