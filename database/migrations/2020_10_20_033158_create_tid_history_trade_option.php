<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creating table tid_history_trade_option
 * for storing percent data for trade options.
 */
class CreateTidHistoryTradeOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tid_history_trade_option', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tid_history_id');
            $table->unsignedBigInteger('trade_options_id');
            $table->float('percent', 8, 2);
            $table->timestamps();

            $table->foreign('tid_history_id')
                ->references('id')
                ->on('tid_history')
                ->onDelete('CASCADE');

            $table->foreign('trade_options_id')
                ->references('id')
                ->on('trade_options')
                ->onDelete('CASCADE');

            $table->unique(['tid_history_id', 'trade_options_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tid_history_trade_option');
    }
}
