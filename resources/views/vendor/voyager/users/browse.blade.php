@extends('voyager::bread.browse')

@push('javascript')
    <script>
        $().ready(function () {
            //actions preview
            $('.activate-mass-confirm, .block-mass-confirm, .unblock-mass-confirm').parent().submit(function (e) {
                if (!confirm('Do you confirm the selected action?')) {
                    e.preventDefault();
                }
            });
        });
    </script>
@endpush
