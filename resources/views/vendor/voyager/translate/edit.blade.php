@extends('voyager::master')

@section('page_title', __('voyager::generic.edit').' Translates')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-folder"></i> Translates
    </h1>
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="{{ route('voyager.translate.save', ['lang' => $lang->short_name]) }}"
                          method="POST">

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">
                            <div id="container-features" class="form-group">
                                <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                    <span style="color: black">When you are finished editing the language files, contact the technician to add the changed files to the repository in case you move the project to another server.</span>
                                </div>
                            </div>
                        </div><!-- panel-body -->

                        @foreach($data as $key => $translate)
                        <div class="panel-body">
                            <div id="container-features" class="form-group">
                                <div class="form-group col-md-{{ $display_options->width ?? 12 }} ">
                                    @if(!$loop->first)<hr/> @endif
                                    <h2>{{ ucfirst($key) }}</h2>
                                </div>

                                @include('voyager::translate.partials.form-field', ['translate' => $translate, 'name_prefix' => $key])
                            </div>
                        </div><!-- panel-body -->
                        @endforeach

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit"
                                        class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <style>
        input.error {
            border: 1px solid red;
        }
    </style>
@stop

@push('javascript')

@endpush
