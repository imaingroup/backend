<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * LimitCommissionException.
 */
final class LimitCommissionException extends EthereumException
{
}
