<?php

namespace App\Modules\Users\Requests;

use App\Modules\Users\Models\User;
use App\Modules\Users\Services\UserService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;

/**
 * This class need for validating password change request.
 */
final class ProfilePasswordChangeRequest extends FormRequest
{
    /** @var \App\Modules\Users\Services\UserService User service. */
    private UserService $userService;

    /**
     * Constructor.
     *
     * @param \App\Modules\Users\Services\UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'current_password' => 'required|scalar',
            'password' => [
                'required',
                'scalar',
                'min:6',
                'max:20',
                sprintf('regex:/^[%s]+$/', User::ALLOWED_PASSWORD_SYMBOLS)
            ],
            'password_confirmation' => 'nullable|scalar',
            '2fa_code' => 'required|scalar'
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator): void
    {
        $user = Auth::user();

        if (!Hash::check($this['current_password'], $user->password)) {
            $validator->errors()->add('current_password', 'Incorrect password');
            return;
        }

        if ($this->get('password') != $this->get('password_confirmation')) {
            $validator->errors()->add('password_confirmation', 'Passwords didn\'t match');
        }

        if (!$user->is_2fa) {
            $validator->errors()->add('2fa_code', 'Please activate 2FA');
            return;
        }

        if (is_scalar($this['2fa_code'])) {
            try {
                if (!$this->userService->check2fa($user, (string)$this['2fa_code'])) {
                    $validator->errors()->add('2fa_code', 'Incorrect 2FA');
                    return;
                }
            } catch (SecretKeyTooShortException $e) {
                $validator->errors()->add('2fa_code', '2FA error');
                return;
            }
        }
    }
}
