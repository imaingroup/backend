<?php
declare(strict_types=1);

namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Requests\Finance\AdminFinanceBtcSendRequest;
use App\Modules\Admin\Requests\Finance\AdminFinanceGethArchiveRequest;
use App\Modules\Admin\Requests\Finance\AdminFinanceGethEstimateRequest;
use App\Modules\Admin\Requests\Finance\AdminFinanceGethMnemonicRequest;
use App\Modules\Admin\Requests\Finance\AdminFinanceGethSendRequest;
use App\Modules\Admin\Requests\Finance\AdminFinanceGethUnarchiveRequest;
use App\Modules\Admin\Requests\Finance\AdminFinanceGlobalAddMoneyRequest;
use App\Modules\Admin\Requests\Finance\AdminFinanceWalletsDataRequest;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Finance\Exceptions\Btc\GetBalanceException;
use App\Modules\Finance\Exceptions\SecureCryptRequestException;
use App\Modules\Finance\Exceptions\SecureCryptRequestParamsException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\RequestException;
use App\Modules\Finance\Services\Admin\FinanceService;
use App\Modules\Users\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Facades\Voyager;

/**
 * Admin finance controller.
 */
final class VoyagerFinanceController extends VoyagerBaseController
{
    /**
     * View global actions.
     *
     * @return View
     */
    public function global(): View
    {
        return Voyager::view('voyager::finance.global');
    }

    /**
     * View wallets page.
     *
     * @return View
     */
    public function wallets(): View
    {
        return Voyager::view('voyager::finance.wallets');
    }

    /**
     * Get wallets data.
     *
     * @param AdminFinanceWalletsDataRequest $request
     * @return ResponseDto
     * @throws GetBalanceException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    public function walletsData(AdminFinanceWalletsDataRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getFinanceService()
                ->getWallets()
        );
    }

    /**
     * Mnemonic geth type money.
     *
     * @param AdminFinanceGethMnemonicRequest $request
     * @return ResponseDto
     */
    public function gethMnemonic(AdminFinanceGethMnemonicRequest $request): ResponseDto
    {
        return new ResponseDto(
            $request->validated()
                ->toMnemonicApiDto()
        );
    }

    /**
     * Archive geth type crypto address.
     *
     * @param AdminFinanceGethArchiveRequest $request
     * @return ResponseDto
     */
    public function gethArchive(AdminFinanceGethArchiveRequest $request): ResponseDto
    {
        $this->getFinanceService()
            ->archive($request->validated());

        return new ResponseDto();
    }

    /**
     * Unarchive geth type crypto address.
     *
     * @param AdminFinanceGethUnarchiveRequest $request
     * @return ResponseDto
     */
    public function gethUnarchived(AdminFinanceGethUnarchiveRequest $request): ResponseDto
    {
        $this->getFinanceService()
            ->unarchived($request->validated());

        return new ResponseDto();
    }

    /**
     * Estimate geth type money.
     *
     * @param AdminFinanceGethEstimateRequest $request
     * @return ResponseDto
     * @throws RequestException
     * @throws \App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\ValidationException
     */
    public function gethEstimate(AdminFinanceGethEstimateRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getFinanceService()
                ->gethEstimate($request->validated())
        );
    }

    /**
     * Send geth type money.
     *
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function gethSend(): Response
    {
        return $this->inTransaction(function (): void {
            User::refreshAndLockForUpdate(Auth::user());

            /** @var AdminFinanceGethSendRequest $request */
            $request = app(AdminFinanceGethSendRequest::class);

            $this->getFinanceService()
                ->gethSend($request->validated());
        });
    }

    /**
     * Send btc money.
     *
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function bitcoinSend(): Response
    {
        return $this->inTransaction(function (): void {
            User::refreshAndLockForUpdate(Auth::user());

            $this->getFinanceService()
                ->bitcoinSend(
                    ...app(AdminFinanceBtcSendRequest::class)->validated()
                );
        });
    }

    /**
     * Add money to all users.
     *
     * @param AdminFinanceGlobalAddMoneyRequest $request
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function massMoneyOperation(AdminFinanceGlobalAddMoneyRequest $request): Response
    {
        return $this->inTransaction(function () use ($request): void {
            $data = $request->validated();

            $this->getFinanceService()
                ->massMoneyOperation(
                    $data['amount'],
                    $data['currency'],
                    Auth::user()
                );
        });
    }

    /**
     * Get {@see FinanceService::class}.
     *
     * @return FinanceService
     */
    private function getFinanceService(): FinanceService
    {
        return app(FinanceService::class);
    }
}
