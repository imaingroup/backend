<?php


namespace App\Modules\Teams\Services;


use App\Modules\PromoCodes\Services\PromoCodeService;
use App\Modules\Users\Models\User;
use App\Modules\Settings\Repositories\SettingsTeamRepository;
use App\Modules\Users\Repositories\UserRepository;

/**
 * Team service.
 */
final class TeamService
{
    /** @var PromoCodeService Promo code service. */
    private PromoCodeService $promoCodeService;

    /** @var UserRepository User repository. */
    private UserRepository $userRepository;

    /** @var SettingsTeamRepository Settings team repository. */
    private SettingsTeamRepository $settingsTeamRepository;

    /**
     * Constructor.
     *
     * @param PromoCodeService $promoCodeService
     * @param UserRepository $userRepository
     * @param SettingsTeamRepository $settingsTeamRepository
     */
    public function __construct(PromoCodeService $promoCodeService, UserRepository $userRepository, SettingsTeamRepository $settingsTeamRepository)
    {
        $this->promoCodeService = $promoCodeService;
        $this->userRepository = $userRepository;
        $this->settingsTeamRepository = $settingsTeamRepository;
    }

    /**
     * Get team data by user.
     * @param \App\Modules\Users\Models\User $user
     * @return array
     */
    public function getData(User $user): array
    {
        $data = [];
        $data['content'] = [
            'activate_usdt_commission_link' => $this->settingsTeamRepository->get()->activate_usdt_commission_link
        ];

        $data['info'] = [
            'email' => $user->email,
            'status' => $user->readableUserStatus,
            'commission_abc' => $this->promoCodeService->getAbcCommission($user),
            'commission_usdt' => $user->user_status === User::STATUS_PROMOTER ?
                $this->promoCodeService->getUsdtCommission($user) :
                null
        ];

        $referrals = [];
        $referralList = $this->userRepository->getReferrals($user->id);
        $number = $referralList->count();

        foreach ($this->userRepository->getReferrals($user->id) as $referral) {
            $referrals[] = [
                'number' => $number,
                'username' => $referral->username,
                'email' => $referral->email,
                'team_qty' => $referral->team_quantity,
                'status' => $referral->readableUserStatus,
                'total_deposited' => $referral->total_deposited_team,
                'earned_abc' => $referral->earned_commission_abc,
                'earned_usdt' => $referral->user_status === User::STATUS_PROMOTER ?
                    $referral->earned_commission_usdt :
                    null,
                'commission_abc' => $referral->teamlead_earned_abc,
                'commission_usdt' => $referral->teamlead_earned_usdt,
            ];

            $number--;
        }

        $data['referrals'] = $referrals;

        return $data;
    }
}
