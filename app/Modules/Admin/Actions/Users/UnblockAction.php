<?php


namespace App\Modules\Admin\Actions\Users;

use App\Modules\Users\Repositories\UserRepository;
use TCG\Voyager\Actions\AbstractAction;

final class UnblockAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Unblock Selected';
    }

    public function getPolicy()
    {
        return 'mass';
    }

    public function getIcon()
    {
        return '';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary unblock-mass-confirm',
        ];
    }

    public function getDefaultRoute()
    {
        return null;
    }

    public function massAction(array $ids, string $referer)
    {
        $count = 0;
        /** @var UserRepository $userRepository */
        $userRepository = app(UserRepository::class);
        foreach ($ids as $id) {
            $user = $userRepository->find($id);
            if($user) {
                $count++;
                $user->is_blocked = false;
                $user->save();
            }
        }

        if($count > 0) {
            $message = [
                'message' => 'Пользователи разблокированы',
                'alert-type' => 'success',
            ];
        }
        else {
            $message = [
                'message' => 'Не выбраны пользователи для разблокировки',
                'alert-type' => 'error',
            ];
        }

        return redirect()->route('voyager.users.index')->with($message);
    }
}
