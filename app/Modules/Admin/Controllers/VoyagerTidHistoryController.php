<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\Investments\Exceptions\TidHistoryException;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Admin\Requests\TidHistory\AdminGenerateProfitHistoryRequest;
use App\Modules\Admin\Requests\TidHistory\AdminProfitHistoryDeleteRequest;
use App\Modules\Admin\Requests\TidHistory\AdminProfitHistoryUpdatePercentRequest;
use App\Modules\Investments\Repositories\TidHistoryRepository;
use App\Modules\Admin\Services\TidHistoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Admin controller for view and generate tid history data.
 */
final class VoyagerTidHistoryController extends VoyagerBaseController
{
    /** @var \App\Modules\Admin\Services\TidHistoryService Tid history admin service. */
    private TidHistoryService $tidHistoryService;

    /** @var TidHistoryRepository Tid history repository. */
    private TidHistoryRepository $tidHistoryRepository;

    /** @var \App\Modules\Logs\Loggers\Logger Logger service. */
    protected Logger $logger;

    /**
     * Constructor.
     *
     * @param TidHistoryService $tidHistoryService
     * @param TidHistoryRepository $tidHistoryRepository
     * @param \App\Modules\Logs\Loggers\Logger $logger
     */
    public function __construct(TidHistoryService $tidHistoryService, TidHistoryRepository $tidHistoryRepository, Logger $logger)
    {
        $this->tidHistoryService = $tidHistoryService;
        $this->tidHistoryRepository = $tidHistoryRepository;
        $this->logger = $logger;
    }

    public function index(Request $request)
    {
        $this->tidHistoryService->addOptionColumns();

        $view = parent::index($request);

        return $view;
    }

    public function generate(AdminGenerateProfitHistoryRequest $request)
    {
        try {
            DB::beginTransaction();

            $data = $request->validated();

            $this->tidHistoryService->generate(
                $data['period'],
                $data['percent'],
                $data['high_max'],
                $data['low_max'],
                $data['positive_days'],
                $data['loss_sum_percent'],
            );

            DB::commit();

            return response(['status' => true], 200);
        } catch (Throwable $e) {
            DB::rollBack();

            $this->logger->rollbackTransaction($e, $request->validated());

            return response(['status' => false], 503);
        }
    }

    /**
     * Update percent item.
     *
     * @param AdminProfitHistoryUpdatePercentRequest $request
     * @return Response
     * @throws \App\Modules\Investments\Exceptions\TidHistoryException
     */
    public function updatePercent(\App\Modules\Admin\Requests\TidHistory\AdminProfitHistoryUpdatePercentRequest $request): Response
    {
        $data = $request->validated();

        $this->tidHistoryService->updatePercent(
            $data['id'],
            $data['option'],
            $data['percent']
        );

        return response(['status' => true], 200);
    }

    /**
     * Delete feature history.
     *
     * @param \App\Modules\Admin\Requests\TidHistory\AdminProfitHistoryDeleteRequest $request
     * @return Response
     */
    public function deleteHistory(\App\Modules\Admin\Requests\TidHistory\AdminProfitHistoryDeleteRequest $request): Response
    {
        $this->tidHistoryRepository->deleteFeatureData();

        return response(['status' => true], 200);
    }
}
