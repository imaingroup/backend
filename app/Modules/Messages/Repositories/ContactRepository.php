<?php
declare(strict_types=1);

namespace App\Modules\Messages\Repositories;

use App\Modules\Messages\Models\Contact;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * Contact repository.
 */
final class ContactRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Messages\Models\Contact $model
     */
    public function __construct(protected \App\Modules\Messages\Models\Contact $model)
    {}

    /**
     * Check that exists contact.
     *
     * @param int $userId
     * @param int $contactUserId
     * @return bool
     */
    public function existsContact(int $userId, int $contactUserId): bool
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $userId)
            ->where('contact_users_id', $contactUserId)
            ->exists();
    }

    /**
     * Get contact.
     *
     * @param int $userId
     * @param int $contactUserId
     * @return \App\Modules\Messages\Models\Contact|null
     */
    public function getContact(int $userId, int $contactUserId): ?Contact
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $userId)
            ->where('contact_users_id', $contactUserId)
            ->first();
    }

    /**
     * Get contacts by user.
     *
     * @param int $userId
     * @return Collection
     */
    public function getByUser(int $userId): Collection
    {
        return $this->model
            ->newQuery()
            ->from('messages_contacts', 'c')
            ->select('c.*')
            ->leftJoin('users', 'users.id', '=', 'c.contact_users_id')
            ->where('c.users_id', $userId)
            ->where('c.deleted', false)
            ->with('user')
            ->with('contact')
            ->orderBy('users.username', 'ASC')
            ->get();
    }
}
