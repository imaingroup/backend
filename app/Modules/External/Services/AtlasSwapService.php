<?php
declare(strict_types=1);

namespace App\Modules\External\Services;

use App\Modules\Notifications\Mails\External\ContactEmail;
use App\Modules\Notifications\Mails\External\ContactMainEmail;
use App\Modules\Notifications\Mails\External\SubscribeEmail;
use App\Modules\Notifications\Mails\MailSender;

/**
 * Atlas swap service.
 */
final class AtlasSwapService
{
    /**
     * Create new contact.
     *
     * @param string $first_name
     * @param string $last_name
     * @param string $subject
     * @param string $email
     * @param string $message
     * @return void
     */
    public function contact(
        string $first_name,
        string $last_name,
        string $subject,
        string $email,
        string $message
    ): void
    {
        MailSender::send(
            config('mail.external_admin_email'),
            new ContactEmail(
                $first_name,
                $last_name,
                $subject,
                $email,
                $message
            )
        );
    }

    /**
     * Create new main contact.
     *
     * @param string $name
     * @param string $subject
     * @param string $email
     * @param string $message
     * @return void
     */
    public function contactMain(
        string $name,
        string $subject,
        string $email,
        string $message
    ): void
    {
        MailSender::send(
            config('mail.external_admin_email'),
            new ContactMainEmail(
                $name,
                $subject,
                $email,
                $message
            )
        );
    }

    /**
     * Create new subscribe.
     *
     * @param string $email
     * @return void
     */
    public function subscribe(string $email): void
    {
        MailSender::send(config('mail.external_admin_email'), new SubscribeEmail($email));
    }
}
