<?php

use App\Modules\Investments\Models\Trade;
use App\Modules\Investments\Models\TradeOption;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Models\DataRow;

/**
 * This migration creates and changes tables for accrual by options.
 */
class AccrualByOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->unsignedSmallInteger('accrual_period')->nullable(true);
        });

        $data = [
            1 => TradeOption::ACCRUAL_DAILY,
            2 => TradeOption::ACCRUAL_30_DAYS,
            3 => TradeOption::ACCRUAL_60_DAYS,
            4 => TradeOption::ACCRUAL_END,
        ];

        foreach ($data as $id => $type) {
            $option = TradeOption::query()->find($id);
            $option->accrual_period = $type;
            $option->save();
        }

        Schema::table('trade_options', function (Blueprint $table) {
            $table->unsignedSmallInteger('accrual_period')->nullable(false)->change();
        });

        $adminSort18 = [80 => 1,
            81 => 2,
            82 => 3,
            83 => 4,
            85 => 5,
            86 => 6,
            87 => 14,
            88 => 15,
            89 => 17,
            90 => 18,
            273 => 7,
            274 => 8,
            275 => 9,
            276 => 11,
            277 => 10,
            278 => 19,
            385 => 12,
            386 => 13,
        ];

        foreach ($adminSort18 as $id => $order) {
            $row = DataRow::query()->find($id);
            $row->order = $order;
            $row->save();
        }

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 18,
                'field' => 'accrual_period',
                'type' => 'select_dropdown',
                'display_name' => 'Accrual Period',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '{"validation":{"rule":"required"},"options":{"1":"End","2":"Daily","3":"30 days","4":"60 days"}}',
                'order' => 16
            ]);

        Schema::table('trades', function (Blueprint $table) {
            $table->unsignedSmallInteger('accrual_period')->nullable(true);
            $table->float('accruals', 16, 8)->default(0);
        });

        foreach (Trade::all() as $trade) {
            $trade->accrual_period = $trade->tradeOption->accrual_period;

            if ($trade->is_money_released) {
                $trade->accruals = $trade->closed_pnl;
            }

            $trade->save();
        }

        Schema::create('trades_accruals', function (Blueprint $table) {
            $table->id();
            $table->float('amount', 16, 8);
            $table->date('date');
            $table->unsignedBigInteger('trades_id');
            $table->timestamps();

            $table->foreign('trades_id')
                ->references('id')
                ->on('trades')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->dropColumn('accrual_period');
        });

        Schema::table('trades', function (Blueprint $table) {
            $table->dropColumn('accrual_period');
        });

        DB::table('data_rows')
            ->where([
                'data_type_id' => 18,
                'field' => 'accrual_period'
            ])
            ->delete();

        Schema::dropIfExists('trades_accruals');
    }
}
