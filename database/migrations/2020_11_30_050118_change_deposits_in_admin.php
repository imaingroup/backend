<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Change view deposits in admin.
 */
class ChangeDepositsInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('data_rows')
            ->where('data_type_id', 23)
            ->where('field', 'tid')
            ->delete();

        DB::table('data_rows')
            ->where('data_type_id', 23)
            ->where('field', 'type_coin')
            ->update([
                'type' => 'select_dropdown',
                'details' => '{"validation":{"rule":"required"},"options":{"1":"BTC"}}'
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 23)
            ->where('field', 'status')
            ->update([
                'type' => 'select_dropdown',
                'details' => '{"validation":{"rule":"required"},"options":{"1":"New","2":"Closed","3":"Finished"}}'
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 23)
            ->where('field', 'created_at')
            ->update([
                'display_name' => 'Creation date'
            ]);

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 23,
                'field' => 'count_transactions',
                'type' => 'number',
                'display_name' => 'Count transactions',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '{}',
                'order' => 9
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('data_rows')
            ->where('data_type_id', 23)
            ->where('field', 'type_coin')
            ->update([
                'type' => 'number',
                'details' => '{"validation":{"rule":"required"}}'
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 23)
            ->where('field', 'status')
            ->update([
                'type' => 'number',
                'details' => '{"validation":{"rule":"required"}}'
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 23)
            ->where('field', 'count_transactions')
            ->delete();
    }
}
