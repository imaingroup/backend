<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds chart fields
 * to trade_options and admin
 * settings.
 */
final class AddChartFieldsToTradeOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->boolean('chart_enabled')->default(false);
            $table->text('statement_link')->nullable(true);
        });

        DB::table('trade_options')
            ->where('id', '!=', 4)
            ->update([
                'chart_enabled' => true
            ]);

        DB::table('data_rows')
            ->insert([
                'data_type_id' => DB::table('data_types')
                    ->where('name', 'trade_options')
                    ->first()
                    ->id,
                'field' => 'chart_enabled',
                'type' => 'checkbox',
                'display_name' => 'Chart enabled',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => '{}',
                'order' => 20
            ]);

        DB::table('data_rows')
            ->insert([
                'data_type_id' => DB::table('data_types')
                    ->where('name', 'trade_options')
                    ->first()
                    ->id,
                'field' => 'statement_link',
                'type' => 'text',
                'display_name' => 'Statement link',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => '{"validation":{"rule":"nullable|scalar|max:65535"}}',
                'order' => 21
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->dropColumn(['chart_enabled', 'statement_link']);
        });

        DB::table('data_rows')
            ->where([
                'data_type_id' => DB::table('data_types')
                    ->where('name', 'trade_options')
                    ->first()
                    ->id,
            ])
            ->whereIn('field', ['chart_enabled', 'statement_link'])
            ->delete();
    }
}
