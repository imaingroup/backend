<?php


namespace App\Modules\Admin\Actions\Users;

use App\Modules\Users\Repositories\UserRepository;
use TCG\Voyager\Actions\AbstractAction;

final class BlockAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Block Selected';
    }

    public function getPolicy()
    {
        return 'mass';
    }

    public function getIcon()
    {
        return '';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary block-mass-confirm',
        ];
    }

    public function getDefaultRoute()
    {
        return null;
    }

    public function massAction(array $ids, string $referer)
    {
        $count = 0;
        /** @var \App\Modules\Users\Repositories\UserRepository $userRepository */
        $userRepository = app(UserRepository::class);
        foreach ($ids as $id) {
            $user = $userRepository->find($id);
            if($user) {
                $count++;
                $user->is_blocked = true;
                $user->save();
            }
        }

        if($count > 0) {
            $message = [
                'message' => 'Пользователи заблокированы',
                'alert-type' => 'success',
            ];
        }
        else {
            $message = [
                'message' => 'Не выбраны пользователи для блокировки',
                'alert-type' => 'error',
            ];
        }

        return redirect()->route('voyager.users.index')->with($message);
    }
}
