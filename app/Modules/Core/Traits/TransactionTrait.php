<?php
declare(strict_types=1);

namespace App\Modules\Core\Traits;

use App\Modules\Data\Dto\Services\DispatchedDto;
use App\Modules\Logs\Loggers\Logger;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use ReflectionFunction;
use Throwable;

/**
 * This trait execute Closure in transaction.
 */
trait TransactionTrait
{
    /**
     * Execute closure in transaction.
     *
     * @param Closure $closure
     * @param Closure|null $catchResponse
     * @return mixed
     * @throws AuthorizationException
     * @throws ValidationException
     */
    protected function inTransaction(
        Closure $closure,
        Closure $catchResponse = null,
    ): mixed
    {
        $defaultCatchReturn = true;

        try {
            $dataPostProcessor = null;
            $dataResponse = null;
            $returnType = (new ReflectionFunction($closure))
                ->getReturnType();
            $defaultReturn = $returnType
                && ($returnType->getName() === 'void');
            $returnCatchType = $catchResponse ? (new ReflectionFunction($catchResponse))
                ->getReturnType() : null;
            $defaultCatchReturn = !$returnCatchType || $returnCatchType->getName() === 'void';

            DB::beginTransaction();

            $data = $closure();

            if ($data instanceof DispatchedDto && $data->isRollback()) {
                DB::rollBack();

                $bestCode = match (true) {
                    $data->is422() => 422,
                    $data->is403() => 403,
                    default => 503
                };

                return response($data->jsonSerialize(), $bestCode);
            } else {
                DB::commit();
            }

            return $defaultReturn
                ? response(['status' => true])
                : $data;
        } catch (ValidationException | AuthorizationException $e) {
            throw $e;
        } catch (Throwable $e) {
            DB::rollBack();

            $this->getLogger()->rollbackTransaction(
                $e,
                request()->all()
            );

            $data = null;

            if ($catchResponse) {
                $data = $catchResponse();
            }

            return $defaultCatchReturn
                ? response(['status' => false], 503)
                : $data;
        }
    }

    /**
     * Get logger.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
