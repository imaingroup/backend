<?php


namespace App\Modules\ProfitCards\Repositories;

use App\Modules\ProfitCards\Models\ProfitCardTidExpanse;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Profit card tid expanse repository.
 */
final class ProfitCardTidExpanseRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param ProfitCardTidExpanse $model
     */
    public function __construct(protected ProfitCardTidExpanse $model)
    {}
}
