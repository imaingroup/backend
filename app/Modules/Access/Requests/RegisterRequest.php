<?php
declare(strict_types=1);

namespace App\Modules\Access\Requests;

use App\Modules\Users\Models\User;
use App\Modules\Security\Models\UserIdentification;
use App\Modules\Security\Services\Identification\IdentificationService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use function sprintf;

/**
 * This request class need for validating data from register form.
 */
final class RegisterRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param IdentificationService $identificationService
     */
    public function __construct(
        private IdentificationService $identificationService
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $recaptchaEnabled = (int)env('GOOGLE_RECAPTCHA_ENABLED') === 1;
        $data = [];

        if ($recaptchaEnabled) {
            $data['g-recaptcha-response'] = 'required|scalar|recaptcha';
        }

        return array_merge($data, [
            'email' => 'required|scalar|email:rfc,dns|unique:users,email|max:255',
            'username' => 'required|scalar|unique:users,username|max:255|regex:/^[a-zA-Z0-9]+$/',
            'password' => [
                'required',
                'scalar',
                'min:6',
                'max:20',
                sprintf('regex:/^[%s]+$/', User::ALLOWED_PASSWORD_SYMBOLS)
            ],
            'password_confirmation' => 'nullable|scalar',
            'promo_code' => 'nullable|scalar|exists:users,promo_code',
            'is_accepted_terms' => 'scalar|accepted'
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function messages()
    {
        return [
            //email
            'email.required' => 'page_access.form.register.email_incorrect',
            'email.email' => 'page_access.form.register.email_incorrect',
            'email.unique' => 'page_access.form.register.email_already_use',
            'email.max' => 'page_access.form.register.email_incorrect',
            //username
            'username.required' => 'page_access.form.register.username_incorrect',
            'username.unique' => 'page_access.form.register.username_already_use',
            'username.max' => 'page_access.form.register.username_incorrect',
            'username.regex' => 'page_access.form.register.username_invalid_pattern',
            //password
            'password.required' => 'page_access.form.register.password_incorrect',
            'password.min' => 'page_access.form.register.password_is_too_short',
            'password.max' => 'page_access.form.register.password_is_too_long',
            'password.regex' => 'page_access.form.register.password_invalid_pattern',
            //promo code
            'promo_code.exists' => 'page_access.form.register.promo_code_wrong',
            //terms
            'is_accepted_terms.accepted' => 'page_access.form.register.terms_is_not_accepted',
            //recaptcha
            'g-recaptcha-response.required' => 'page_access.form.register.captcha_incorrect',
            'g-recaptcha-response.recaptcha' => 'page_access.form.register.captcha_incorrect'
        ];
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator)
    {
        if ($this->get('password') != $this->get('password_confirmation')) {
            $validator->errors()->add('password_confirmation', 'page_access.form.register.password_didnt_match');
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * {@inheritDoc}
     */
    protected function failedValidation(Validator $validator): void
    {
        $this->identificationService
            ->confirmLastIdentification(UserIdentification::ACTION_REGISTER_ERROR);

        parent::failedValidation($validator);
    }
}
