<?php
declare(strict_types=1);

namespace App\Modules\Core\Exceptions\Dto;

use Exception;

/**
 * NotAllowedChangingPropertyException
 */
class NotAllowedChangingPropertyException extends Exception
{
}

