<?php

namespace App\Modules\Notifications\Models;

use App\Modules\Core\Models\Model;

final class NotificationUser extends Model
{
    protected $table = 'notifications_users';
}
