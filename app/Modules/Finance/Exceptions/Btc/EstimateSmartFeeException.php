<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * EstimateSmartFeeException.
 */
final class EstimateSmartFeeException extends BtcException
{
}
