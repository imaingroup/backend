<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Models\Contact;
use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation start conversation request.
 */
final class StartConversationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            $this->contact instanceof Contact &&
            $this->getMessageService()->checkAccessContact($user, $this->contact);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'contact' => Contact::class,
    ])]
    public function validated(): array
    {
        return [
            'contact' => $this->contact,
        ];
    }

    /**
     * Get MessageService.
     *
     * @return MessageService
     */
    private function getMessageService(): MessageService
    {
        return app(MessageService::class);
    }
}
