<?php


namespace App\Modules\Admin\Services;


use App\Modules\Admin\Exceptions\TranslateException;
use Illuminate\Support\Facades\Config;

final class TranslateService
{
    const ORIGINAL_FILES = '_original';
    const PATH = '%s/../../../../resources/lang/%s';

    public function loadForEdit(string $lang)
    {
        $path = \sprintf(self::PATH, __DIR__, self::ORIGINAL_FILES);
        $files = array_filter(\scandir($path), function ($value) {
            return ($value !== '.' && $value !== '..');
        });

        $data = [];
        foreach ($files as $file) {
            $originalLangData = include(\sprintf(self::PATH . '/%s', __DIR__, self::ORIGINAL_FILES, $file));
            $currentLangData = include(\sprintf(self::PATH . '/%s', __DIR__, $lang, $file));
            $fileName = explode('.', $file)[0];

            $data[$fileName] = $this->loadLangFields($originalLangData, $currentLangData);
        }

        return $data;
    }

    private function loadLangFields(array $originalFields, ?array $currentFields)
    {
        $data = [];

        foreach ($originalFields as $key => $value) {
            if (!is_array($value)) {
                $data[$key] = [
                    'value' => isset($currentFields[$key]) ? $currentFields[$key] : '',
                    'original_value' => $value
                ];
            } else {
                $data[$key] = $this->loadLangFields($value, isset($currentFields[$key]) ? $currentFields[$key] : null);
            }
        }

        return $data;
    }

    public function updateLanguageFiles(string $lang, array $data)
    {
        foreach ($data as $key => $value) {
            $file = \sprintf(self::PATH . '/%s.php', __DIR__, $lang, $key);

            $dumper = new \Nette\PhpGenerator\Dumper();
            $dump = \sprintf('<?php return %s;', \str_replace('\' => null', '\' => \'\'', $dumper->dump($value)));

            file_put_contents($file, $dump);
        }
    }

    /**
     * This method load all translates
     * for file list for all languages
     * in special format.
     *
     * @param array $files
     * @throws TranslateException
     */
    public function getTranslates(array $files): array
    {
        $data = [];
        $translates = [];

        foreach ($files as $file) {
            $data = array_merge($data, $this->getTranslatesFile($file, self::ORIGINAL_FILES));
        }

        foreach (array_keys(Config::get('app.locales')) as $locale) {
            $translateItem = [];

            foreach ($files as $file) {
                $translateItem = array_merge($translateItem, $this->getTranslatesFile($file, $locale));
            }

            $translates[$locale] = $translateItem;
        }

        foreach ($data as $const => $translate) {
            foreach (array_keys(Config::get('app.locales')) as $locale) {
                if (!isset($translates[$locale][$const])) {
                    throw new TranslateException(\sprintf('Not found %s const for %s lang', $const, $locale));
                }

                if (!is_array($data[$const])) {
                    $data[$const] = [];
                }

                $data[$const][$locale] = $translates[$locale][$const];
            }
        }

        return $data;
    }

    /**
     * Get translates for file in special format.
     *
     * @param string $file
     * @param string $lang
     * @return array
     * @throws TranslateException
     */
    public function getTranslatesFile(string $file, string $lang): array
    {
        $path = \sprintf(self::PATH . '/%s.php', __DIR__, $lang, $file);
        if (!\file_exists($path)) {
            throw new TranslateException(\sprintf('File in path %s not exists', $path));
        }

        $data = include($path);
        if (!is_array($data)) {
            throw new TranslateException(\sprintf('File in path %s not language file', $path));
        }

        return $this->toLines($file, $data);
    }

    /**
     * Convert array to lines
     *
     * @param string $prefix
     * @param array $data
     * @return array
     */
    public function toLines(string $prefix, array $data): array
    {
        $result = [];

        foreach ($data as $key => $value) {
            $fullKey = !empty($prefix) ? \sprintf('%s.%s', $prefix, $key) : $key;

            if (is_array($value)) {
                $result = array_merge($result, $this->toLines($fullKey, $value));
            } else {
                $result[$fullKey] = $value;
            }
        }

        return $result;
    }
}
