<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * Add coin type for deposites admin.
 */
final class UpdatesAdmin4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::transaction(function() {
            $type = DB::table('data_types')
                ->where('slug', 'deposits')
                ->first();

            DB::table('data_rows')
                ->where('data_type_id', $type->id)
                ->where('field', 'type_coin')
                ->update([
                    'details' => '{"validation":{"rule":"required|min:1|max:2"},"options":{"1":"BTC", "2":"ETH"}}',
                ]);

            $type = DB::table('data_types')
                ->where('slug', 'trades')
                ->first();

            DB::table('data_rows')
                ->where('data_type_id', $type->id)
                ->where('field', 'users_id')
                ->update([
                    'type' => 'number',
                ]);

            $type = DB::table('data_types')
                ->where('slug', 'profit-card-buy-histories')
                ->first();

            DB::table('data_rows')
                ->where('data_type_id', $type->id)
                ->where('field', 'users_id')
                ->update([
                    'type' => 'number',
                ]);

            $type = DB::table('data_types')
                ->where('slug', 'user-actions')
                ->first();

            DB::table('data_rows')
                ->where('data_type_id', $type->id)
                ->where('field', 'users_id')
                ->update([
                    'type' => 'number',
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::transaction(function() {
            $type = DB::table('data_types')
                ->where('slug', 'deposits')
                ->first();

            DB::table('data_rows')
                ->where('data_type_id', $type->id)
                ->where('field', 'type_coin')
                ->update([
                    'details' => '{"validation":{"rule":"required|min:1|max:1"},"options":{"1":"BTC"}}',
                ]);

            $type = DB::table('data_types')
                ->where('slug', 'trades')
                ->first();

            DB::table('data_rows')
                ->where('data_type_id', $type->id)
                ->where('field', 'users_id')
                ->update([
                    'type' => 'text',
                ]);

            $type = DB::table('data_types')
                ->where('slug', 'profit-card-buy-histories')
                ->first();

            DB::table('data_rows')
                ->where('data_type_id', $type->id)
                ->where('field', 'users_id')
                ->update([
                    'type' => 'text',
                ]);

            $type = DB::table('data_types')
                ->where('slug', 'user-actions')
                ->first();

            DB::table('data_rows')
                ->where('data_type_id', $type->id)
                ->where('field', 'users_id')
                ->update([
                    'type' => 'text',
                ]);
        });
    }
}
