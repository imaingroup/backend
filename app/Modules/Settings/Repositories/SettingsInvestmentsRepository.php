<?php


namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsInvestments;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Settings investments repository.
 */
final class SettingsInvestmentsRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Settings\Models\SettingsInvestments $model
     */
    public function __construct(protected SettingsInvestments $model)
    {}

    /**
     * Get settings model.
     *
     * @return \App\Modules\Settings\Models\SettingsInvestments
     */
    public function get(): SettingsInvestments
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
