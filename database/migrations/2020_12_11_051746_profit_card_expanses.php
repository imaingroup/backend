<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration change db for expanse limit for profit cards.
 */
final class ProfitCardExpanses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profit_cards', function (Blueprint $table) {
            $table->float('withdrawals_expanse', 16)->after('bonus')->default(0);
        });

        $list = [
            1 => 500,
            2 => 2000,
            3 => 7000,
            4 => 12000,
        ];

        foreach ($list as $id => $expanse) {
            DB::table('profit_cards')
                ->where('id', $id)
                ->update([
                    'withdrawals_expanse' => $expanse
                ]);
        }

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 12,
                'field' => 'withdrawals_expanse',
                'type' => 'number',
                'display_name' => 'Withdrawals expanse',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|numeric|min:0"}}',
                'order' => 8
            ]);

        Schema::create('profit_cards_tid_options_expanses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('profit_cards_id');
            $table->unsignedBigInteger('trade_options_id');
            $table->unsignedSmallInteger('tid_expanse');
            $table->timestamps();

            $table->unique(['profit_cards_id', 'trade_options_id'], 'profit_cards_tid_options_expanses_unique');

            $table->foreign('profit_cards_id')
                ->references('id')
                ->on('profit_cards')
                ->onDelete('CASCADE');

            $table->foreign('trade_options_id')
                ->references('id')
                ->on('trade_options')
                ->onDelete('CASCADE');
        });

        $list = [
            [1, 1, 2],
            [1, 2, 0],
            [1, 3, 0],

            [2, 1, 2],
            [2, 2, 2],
            [2, 3, 1],

            [3, 1, 5],
            [3, 2, 5],
            [3, 3, 3],

            [4, 1, 10],
            [4, 2, 10],
            [4, 3, 5],
        ];

        foreach ($list as $item) {
            DB::table('profit_cards_tid_options_expanses')
                ->insert([
                    'profit_cards_id' => $item[0],
                    'trade_options_id' => $item[1],
                    'tid_expanse' => $item[2],
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()
                ]);
        }

        DB::table('data_types')
            ->where('id', 12)
            ->update([
                'controller' => 'App\Http\Controllers\VoyagerProfitCardsController'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profit_cards', function (Blueprint $table) {
            $table->dropColumn('withdrawals_expanse');
        });

        Schema::dropIfExists('profit_cards_tid_options_expanses');

        DB::table('data_rows')
            ->where([
                'data_type_id' => 12,
                'field' => 'withdrawals_expanse'
            ])
            ->delete();

        DB::table('data_types')
            ->where('id', 12)
            ->update([
                'controller' => null
            ]);
    }
}
