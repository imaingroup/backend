<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds disabled_dividends_withdraw field
 * to settings_deposit table.
 */
class AddFieldDisabledDividendsWithdrawToSettingsDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->boolean('disabled_dividends_withdraw')->default(false);
        });

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 36,
                'field' => 'disabled_dividends_withdraw',
                'type' => 'checkbox',
                'display_name' => 'Disabled Dividends Withdraw',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '{}',
                'order' => 4
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->dropColumn('disabled_dividends_withdraw');
        });

        DB::table('data_rows')
            ->where('data_type_id', 36)
            ->where('field', 'disabled_dividends_withdraw')
            ->delete();
    }
}
