<?php

namespace App\Modules\Investments\Models;

use App\Modules\Investments\Models\TidHistoryTradeOption;
use App\Modules\Core\Models\Model;

final class TidHistory extends Model
{
    protected $table = 'tid_history';

    protected $perPage = 1000;

    /**
     * Events for model.
     */
    protected static function booted()
    {
        static::saved(function (TidHistory $model) {
            TidHistory::query()
                ->where('id', $model->id)
                ->where('percent', -0)
                ->update([
                    'percent' => 0
                ]);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function __get($key)
    {
        if(preg_match('/option_(\d+)/', $key, $matches)) {
            $id = (int) $matches[1];

            $tradeOptionPercent = $this->getTradeOptionHistory($this->id, $id);
            return $tradeOptionPercent ? $tradeOptionPercent->roundedPercent : null;
        }

        return $this->getAttribute($key);
    }

    /**
     * Get tid history trade option model.
     *
     * @param int $tidHistoryId
     * @param int $tradeOptionId
     * @return TidHistoryTradeOption|null
     */
    public function getTradeOptionHistory(int $tidHistoryId, int $tradeOptionId): ?TidHistoryTradeOption
    {
        return TidHistoryTradeOption::query()
            ->where('tid_history_id', $tidHistoryId)
            ->where('trade_options_id', $tradeOptionId)
            ->first();
    }

    /**
     * Get percent accessor.
     *
     * @return float|null
     */
    public function getPercentBrowseAttribute(): ?float
    {
        return $this->percent !== null ? round($this->percent, 2) : null;
    }
}
