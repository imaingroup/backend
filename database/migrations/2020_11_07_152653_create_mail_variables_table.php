<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates mails_variables table,
 * it need for store mail's variables.
 */
class CreateMailVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails_variables', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('variable', 255);
            $table->unsignedBigInteger('mails_id');
            $table->timestamps();

            $table->foreign('mails_id')
                ->references('id')
                ->on('mails')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mails_variables');
    }
}
