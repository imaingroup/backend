<?php
declare(strict_types=1);


namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Services\MessageService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation delete message request.
 */
final class DeleteMessageRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Messages\Services\MessageService $messageService
     */
    public function __construct(
        private MessageService $messageService
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user !== null &&
            $this->messageService->checkAccessMessage($user, $this->message);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['for_all' => "array"])]
    public function rules(): array
    {
        return [
            'for_all' => [
                'nullable',
                Rule::in([1])
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape([
        'user' => "App\\Modules\\Users\\Models\\User",
        'message' => "string",
        'forAll' => "bool"
    ])]
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'message' => $this->message,
            'forAll' => $this->for_all !== null
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function (Validator $validator) {
            if (count($validator->failed()) === 0) {
                $this->after($validator);
            }
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     */
    private function after(Validator $validator): void
    {
        if ($this->for_all && !$this->messageService->checkAccessDeleteMessageForAll(Auth::user(), $this->message)) {
            $validator->errors()->add('for_all', 'This message does not allowed for delete for all.');

            return;
        }
    }
}
