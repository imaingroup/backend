<?php

namespace App\Modules\Notifications\Mails;

use App\Modules\Notifications\Mails\AbstractEmail;
use App\Modules\Notifications\Models\Mail;
use App\Modules\Users\Models\User;

/**
 * Reset password link email.
 */
final class PasswordWasResetEmail extends AbstractEmail
{
    /** @var \App\Modules\Users\Models\User User model. */
    private User $user;

    /**
     * Create a new mail instance.
     *
     * @param \App\Modules\Users\Models\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    protected function getVariables(): array
    {
        return [
            '{sitename}' => __('base.site_name'),
            '{username}' => protectFromXss($this->user->username)
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getMailId(): int
    {
        return Mail::MAIL_PASSWORD_WAS_RESET;
    }
}
