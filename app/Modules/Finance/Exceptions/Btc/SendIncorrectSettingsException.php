<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * SendIncorrectSettingsException.
 */
final class SendIncorrectSettingsException extends BtcException
{
}
