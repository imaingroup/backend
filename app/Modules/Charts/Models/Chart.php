<?php

namespace App\Modules\Charts\Models;

use App\Modules\Core\Models\Model;

final class Chart extends Model
{
    /**
     * Events for model.
     */
    protected static function booted()
    {
        static::creating(function (Chart $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }
}
