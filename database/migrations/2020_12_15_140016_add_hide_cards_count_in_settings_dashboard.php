<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds hide_number_of_cards field
 * to settings_dashboard table.
 */
final class AddHideCardsCountInSettingsDashboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_dashboard', function (Blueprint $table) {
            $table->boolean('hide_number_of_cards')->default(false);
        });

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 49,
                'field' => 'hide_number_of_cards',
                'type' => 'checkbox',
                'display_name' => 'Hide the number of cards',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => '{}',
                'order' => 4
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_dashboard', function (Blueprint $table) {
            $table->dropColumn('hide_number_of_cards');
        });

        DB::table('data_rows')
            ->where([
                'data_type_id' => 49,
                'field' => 'hide_number_of_cards'
            ])
            ->delete();
    }
}
