<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Tasks;

use App\Modules\Core\Models\Task;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * This class validates request for get tasks.
 */
final class AdminTasksGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'types' => 'required|array|min:1',
            'types.*' => [
                'required',
                'integer',
                Rule::in(array_keys(Task::TYPES))
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'types' => is_array($this->types)
                ? array_map(fn($item) => is_integer($item) ? (int)$item : $item, $this->types)
                : $this->types
        ]);
    }
}
