<?php
declare(strict_types=1);

namespace App\Modules\Finance\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Models\CryptoCurrency;
use Illuminate\Database\Eloquent\Builder;

/**
 * CryptoCurrencyRepository.
 */
final class CryptoCurrencyRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param CryptoCurrency $model
     */
    public function __construct(protected CryptoCurrency $model)
    {
    }

    /**
     * Find by address and network.
     *
     * @param string|null $address
     * @param EthereumNetworkEnum|BitcoinNetworkEnum $network
     * @return CryptoCurrency|null
     */
    public function findByAddressAndNetwork(?string $address, EthereumNetworkEnum|BitcoinNetworkEnum $network): ?CryptoCurrency
    {
        return $this->model->newQuery()
            ->whereHas('network', static function(Builder $builder) use($network) {
               $builder->where('code', $network->value)
                   ->where('type', $network->type());
            })
            ->where('address', $address)
            ->first();
    }
}
