<?php
declare(strict_types=1);


use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration adds menu
 * for global finance action.
 */
final class AddMenuFinanceGlobalInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('menu_items')
            ->insert([
                'menu_id' => 1,
                'title' => 'Global',
                'url' => '',
                'target' => '_self',
                'icon_class' => 'voyager-wallet',
                'color' => '#000000',
                'parent_id' => 27,
                'order' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
                'route' => 'voyager.finance.global'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('menu_items')
            ->where('parent_id', 27)
            ->where('route', 'voyager.finance.global')
            ->delete();
    }
}
