<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * GetSha3Exception.
 */
final class GetSha3Exception extends EthereumException
{
}
