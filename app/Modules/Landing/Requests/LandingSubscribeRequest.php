<?php

namespace App\Modules\Landing\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * This request class need for validating data from landing subscribe form.
 */
final class LandingSubscribeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|scalar|email:rfc,dns|unique:subscribes,email|max:255'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function messages()
    {
        return [
            'email.required' => 'landing.form.validation.subscribe.email_incorrect',
            'email.email' => 'landing.form.validation.subscribe.email_incorrect',
            'email.max' => 'landing.form.validation.subscribe.email_incorrect',
            'email.unique' => 'landing.form.validation.subscribe.email_unique'
        ];
    }
}
