<?php
declare(strict_types=1);

namespace App\Modules\Finance\Enum;

/**
 * NetworkTypeEnum.
 */
enum NetworkTypeEnum: int
{
    case BITCOIN = 1;
    case ETHEREUM = 2;
}
