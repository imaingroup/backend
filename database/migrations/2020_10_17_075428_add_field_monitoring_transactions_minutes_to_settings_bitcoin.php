<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adding field monitoring_transactions_minutes to settings_bitcoin
 * for specify how long time need monitoring transactions.
 */
class AddFieldMonitoringTransactionsMinutesToSettingsBitcoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_bitcoin', function (Blueprint $table) {
            $table->smallInteger('monitoring_transactions_minutes')->after('minconf')->default('120');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_bitcoin', function (Blueprint $table) {
            $table->dropColumn('monitoring_transactions_minutes');
        });
    }
}
