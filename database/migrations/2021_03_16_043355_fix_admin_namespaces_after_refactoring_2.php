<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * This migration fixes model's namespaces in admin
 * after refactoring modules version 2.
 */
class FixAdminNamespacesAfterRefactoring2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::beginTransaction();

        DB::table('data_rows')
            ->where('data_type_id', 11)
            ->where('field', 'video_belongsto_lang_relationship')
            ->update([
                'details' => '{"model":"App\\\\Modules\\\\Languages\\\\Models\\\\Lang","table":"langs","type":"belongsTo","column":"langs_id","key":"id","label":"name","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 1)
            ->where('field', 'user_belongsto_lang_relationship')
            ->update([
                'details' => '{"model":"App\\\\Modules\\\\Languages\\\\Models\\\\Lang","table":"langs","type":"belongsTo","column":"langs_id","key":"id","label":"name","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::beginTransaction();


        DB::table('data_rows')
            ->where('data_type_id', 11)
            ->where('field', 'video_belongsto_lang_relationship')
            ->update([
                'details' => '{"model":"App\\\\Models\\\\Lang","table":"langs","type":"belongsTo","column":"langs_id","key":"id","label":"name","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', 1)
            ->where('field', 'user_belongsto_lang_relationship')
            ->update([
                'details' => '{"model":"App\\\\Models\\\\Lang","table":"langs","type":"belongsTo","column":"langs_id","key":"id","label":"name","pivot_table":"charts","pivot":"0","taggable":"0"}',
            ]);

        DB::commit();
    }
}
