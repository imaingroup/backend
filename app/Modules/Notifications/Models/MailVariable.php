<?php

namespace App\Modules\Notifications\Models;

use App\Modules\Core\Models\Model;

final class MailVariable extends Model
{
    protected $table = 'mails_variables';
}
