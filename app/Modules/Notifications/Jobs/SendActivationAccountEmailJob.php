<?php

namespace App\Modules\Notifications\Jobs;

use App\Modules\Notifications\Mails\ActivationAccountEmail;
use App\Modules\Notifications\Mails\MailSender;
use App\Modules\Users\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Send activation mail job.
 */
final class SendActivationAccountEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Modules\Users\Models\User User model. */
    private User $user;

    /**
     * Create a new job instance.
     *
     * @param \App\Modules\Users\Models\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MailSender::send($this->user->email, new ActivationAccountEmail($this->user));
    }
}
