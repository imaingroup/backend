<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto;

/**
 * Immutable DTO.
 */
interface Immutable
{
}
