<?php


namespace App\Modules\Deposites\Exceptions;


use Exception;

/**
 * This exception may be throw in deposit service.
 */
final class DepositServiceException extends Exception
{

}
