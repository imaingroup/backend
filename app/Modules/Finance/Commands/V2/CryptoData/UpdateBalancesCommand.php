<?php
declare(strict_types=1);

namespace App\Modules\Finance\Commands\V2\CryptoData;

use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\RequestException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\ValidationException;
use App\Modules\Finance\Services\V2\CryptoProcessingService;
use Illuminate\Console\Command;

/**
 * Update balances command.
 */
final class UpdateBalancesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finance:data_update_balances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finance data update balances.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->getCryptoProcessingService()
            ->updateBalancesEthereumType();

        return 0;
    }

    /**
     * Get {@see CryptoProcessingService::class}.
     *
     * @return CryptoProcessingService
     */
    private function getCryptoProcessingService(): CryptoProcessingService
    {
        return app(CryptoProcessingService::class);
    }
}
