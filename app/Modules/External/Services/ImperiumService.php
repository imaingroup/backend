<?php
declare(strict_types=1);

namespace App\Modules\External\Services;

use App\Modules\External\Dto\Api\Imperium\Rates;
use App\Modules\Notifications\Mails\External\Imperium\ReportEmail;
use App\Modules\Notifications\Mails\MailSender;
use App\Modules\Settings\Repositories\SettingsImperiumRepository;

/**
 * Imperium service.
 */
final class ImperiumService
{
    /**
     * Get wallet download win url.
     *
     * @return string|null
     */
    public function getWalletDownloadWinUrl(): ?string
    {
        return $this->getSettingsImperiumRepository()
            ->get()
            ?->download_win_url;
    }

    /**
     * Get wallet download mac url.
     *
     * @return string|null
     */
    public function getWalletDownloadMacUrl(): ?string
    {
        return $this->getSettingsImperiumRepository()
            ->get()
            ?->download_mac_url;
    }

    /**
     * Get wallet version.
     *
     * @return string|null
     */
    public function getWalletVersion(): ?string
    {
        return $this->getSettingsImperiumRepository()
            ->get()
            ?->version;
    }

    /**
     * Get rates.
     *
     * @return Rates
     */
    public function getRates(): Rates
    {
        $settings = $this->getSettingsImperiumRepository()
            ->get();

        return new Rates(
            $settings->rate_previous,
            $settings->rate,
        );
    }

    /**
     * Send report.
     *
     * @param string $message
     * @return void
     */
    public function sendReport(string $message): void
    {
        MailSender::send(
            config('mail.external_admin_imperium_email'),
            new ReportEmail(
                $message
            )
        );
    }

    /**
     * Get {@see SettingsImperiumRepository::class}.
     *
     * @return SettingsImperiumRepository
     */
    private function getSettingsImperiumRepository(): SettingsImperiumRepository
    {
        return app(SettingsImperiumRepository::class);
    }
}
