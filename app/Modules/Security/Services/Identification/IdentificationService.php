<?php
declare(strict_types=1);

namespace App\Modules\Security\Services\Identification;

use App\Modules\Security\Dto\Services\IdentificationsDataDto;
use App\Modules\Security\Jobs\ProcessIdentificationUpdating;
use App\Modules\Security\Models\UserIdentification;
use App\Modules\Security\Models\UserIdentificationRelation;
use App\Modules\Security\Models\UserIdentificationUserAgent;
use App\Modules\Security\Models\UserIdentificationUserRelation;
use App\Modules\Security\Repositories\UserIdentificationRelationRepository;
use App\Modules\Security\Repositories\UserIdentificationRepository;
use App\Modules\Users\Models\User;
use App\Modules\Users\Repositories\UserRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use function collect;
use function session;

/**
 * Identification service.
 */
final class IdentificationService
{
    public const ID_FIELDS_USERS = [
        'users_id',
        'userid',
    ];

    public const ID_FIELDS_ACCURATE = [
        'users_id',
        'userid',
        'local_storage',
        'cookie'
    ];

    public const ID_FIELDS_HIGH_PROBABLY = [
        'users_id',
        'userid',
        'local_storage',
        'cookie',
        'id1',
        'id2',
    ];

    public const BROWSER_FIELDS = [
        'user_agent',
        'browser',
        'browser_version',
        'browser_major_version',
        'is_ie',
        'is_chrome',
        'is_firefox',
        'is_safari',
        'is_opera',
        'engine',
        'engine_version',
        'platform',
        'os',
        'os_version',
        'is_windows',
        'is_linux',
        'is_ubuntu',
        'is_solaris',
        'device',
        'device_type',
        'device_vendor',
        'cpu',
        'is_mobile',
        'is_mobile_android',
        'is_mobile_opera',
        'is_mobile_windows',
        'is_mobile_blackberry',
        'is_mobile_ios',
        'is_iphone',
        'is_ipod',
    ];

    public const IDENTIFICATION_FIELDS = [
        'users_id',
        'ip',
        'country',
        'action',
        'userid',
        'id1',
        'id2',
        'color_depth',
        'current_resolution',
        'available_resolution',
        'device_xdpi',
        'device_ydpi',
        'plugins',
        'is_java',
        'java_version',
        'is_flash',
        'flash_version',
        'is_silverlight',
        'silverlight_version',
        'mime_types',
        'is_mime_types',
        'is_font',
        'fonts',
        'is_local_storage',
        'is_session_storage',
        'is_cookie',
        'time_zone',
        'language',
        'languages',
        'system_language',
        'is_canvas',
        'canvas_hash',
        'local_storage',
        'cookie',
    ];

    /**
     * Constructor.
     *
     * @param UserIdentificationRepository $userIdentificationRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        private UserIdentificationRepository $userIdentificationRepository,
        private UserRepository               $userRepository
    )
    {
    }

    /**
     * Save identification data.
     *
     * @param array $data
     * @return UserIdentification
     */
    public function saveDate(array $data): UserIdentification
    {
        /** @var UserIdentificationUserAgent|null $userAgent */
        $userAgent = UserIdentificationUserAgent::query()
            ->where('user_agent', strtolower($data['user_agent']))
            ->first();

        if (!$userAgent) {
            $userAgent = new UserIdentificationUserAgent();

            collect(self::BROWSER_FIELDS)
                ->each(function (string $field) use ($userAgent, $data) {
                    $userAgent->$field = array_key_exists($field, $data) ? $data[$field] : null;
                });

            $userAgent->save();
        }

        $userIdentification = new UserIdentification();
        $userIdentification->userAgent()
            ->associate($userAgent);

        collect(self::IDENTIFICATION_FIELDS)
            ->each(function (string $field) use ($userIdentification, $data) {
                $userIdentification->$field = array_key_exists($field, $data) ? $data[$field] : null;
            });

        $userIdentification->save();

        //ProcessIdentificationUpdating::dispatch($userIdentification);

        session()->put('userIdentification', $userIdentification);

        return $userIdentification;
    }

    /**
     * Confirm last identification.
     *
     * @param int $action
     * @param User|null $user
     * @return UserIdentification|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function confirmLastIdentification(int $action, User $user = null): ?UserIdentification
    {
        $session = session();
        $userIdentification = null;

        if ($session->has('userIdentification')) {
            /** @var UserIdentification $userIdentification */
            $userIdentification = $session->get('userIdentification');

            if (Auth::check() || $user) {
                $userIdentification->user()
                    ->associate(
                        Auth::user() ? Auth::user() : $user
                    );
            }

            $userIdentification->action = $action;
            $userIdentification->save();

            //ProcessIdentificationUpdating::dispatch($userIdentification);

            $session->remove('userIdentification');
        }

        return $userIdentification;
    }

    /**
     * Update user identification relations.
     *
     * @param UserIdentification $identification
     * @return void
     */
    public function updateUserIdentificationRelations(UserIdentification $identification): void
    {
        $identification->refresh();
        $this->updateUserIdentificationRelationsByFields($identification, collect(self::ID_FIELDS_HIGH_PROBABLY), false);
        $this->updateUserIdentificationRelationsByFields($identification, collect(self::ID_FIELDS_ACCURATE), true);
    }

    /**
     * Update user identification relations.
     *
     * @param UserIdentification $identification
     * @param Collection $fields
     * @param bool $isAccurate
     * @return void
     */
    public function updateUserIdentificationRelationsByFields(UserIdentification $identification, Collection $fields, bool $isAccurate): void
    {
        $list = collect();
        $fields
            ->each(fn(string $field) => $list->put($field, collect($identification->$field)));
        $users = collect();

        $updateRelations = collect();

        if ($identification->user) {
            $updateRelations->add(['users_identifications_id' => $identification->id, 'users_id' => $identification->users_id, 'is_accurate' => $isAccurate]);

            $users->put($identification->users_id, $identification->users_id);
        }

        $ids = $this->getUserIdentificationRelationRepository()
            ->getUsersIdsByByIdentifiersCollection($list, $isAccurate);

        $ids2 = $this->userIdentificationRepository
            ->getIdsByCollection($list);

        $ids
            ->each(static function (UserIdentificationRelation $identificationRelated) use ($identification, $isAccurate, $users, $updateRelations, $ids2) {
                $users->put($identificationRelated->users_id, $identificationRelated->users_id);

                $updateRelations->put(
                    $identification->id . $identificationRelated->users_id,
                    ['users_identifications_id' => $identification->id, 'users_id' => $identificationRelated->users_id, 'is_accurate' => $isAccurate]
                );

                if ($identification->users_id) {
                    $updateRelations->put(
                        $identificationRelated->users_identifications_id . $identification->users_id,
                        ['users_identifications_id' => $identificationRelated->users_identifications_id, 'users_id' => $identification->users_id, 'is_accurate' => $isAccurate]
                    );
                }
            });

        if ($identification->user) {
            $ids2->each(function (UserIdentification $identification1) use ($updateRelations, $identification, $isAccurate, $users) {
                $users->put($identification1->users_id, $identification1->users_id);

                $updateRelations->put(
                    $identification1->id . $identification1->users_id,
                    ['users_identifications_id' => $identification1->id, 'users_id' => $identification->users_id, 'is_accurate' => $isAccurate]
                );
            });
        }

        $users = $users
            ->values()
            ->filter(fn(mixed $value) => is_int($value));

        $ids2->each(function (UserIdentification $identification1) use ($updateRelations, $users, $isAccurate) {
            $users->each(function (int $user) use ($identification1, $updateRelations, $users, $isAccurate) {
                $updateRelations->put(
                    $identification1->id . $user,
                    ['users_identifications_id' => $identification1->id, 'users_id' => $user, 'is_accurate' => $isAccurate]
                );
            });
        });

        $updateArray = [];
        if ($isAccurate) {
            $updateArray[] = 'is_accurate';
        }

        UserIdentificationRelation::query()
            ->upsert($updateRelations->toArray(), ['users_identifications_id', 'users_id'], $updateArray);

        $updateUsersRelations = collect();

        $users
            ->each(function (int $mainUserId) use ($updateUsersRelations, $users, $isAccurate) {
                $users->each(function (int $userId) use ($mainUserId, $updateUsersRelations, $isAccurate) {
                    $updateUsersRelations->add(['users_main_id' => $mainUserId, 'users_id' => $userId, 'is_accurate' => $isAccurate]);
                });

                $users->each(function (int $userId) use ($mainUserId, $updateUsersRelations, $isAccurate) {
                    $updateUsersRelations->add(['users_main_id' => $userId, 'users_id' => $mainUserId, 'is_accurate' => $isAccurate]);
                });
            });

        UserIdentificationUserRelation::query()
            ->upsert(
                $updateUsersRelations->toArray(),
                ['users_main_id', 'users_id'],
                $updateArray
            );

        if ($identification->user) {
            $identification->user->multi_accounts = UserIdentificationUserRelation::query()
                ->where('users_main_id', $identification->user->id)
                ->count();
            $identification->user->save();
        }

        $users
            ->each(function (int $mainUserId) {
                User::query()
                    ->where('id', $mainUserId)
                    ->update([
                        'multi_accounts' => UserIdentificationUserRelation::query()
                            ->where('users_main_id', $mainUserId)
                            ->count(),
                    ]);
            });
    }

    /**
     * Get identification data by user.
     *
     * @param User $user
     * @param bool $withHistory
     * @return IdentificationsDataDto
     */
    public function getByUser(User $user, bool $withHistory = true): IdentificationsDataDto
    {
        $accurateIdentifiers = $this->getIdentifiersByUser($user, collect(self::ID_FIELDS_ACCURATE)->toArray());
        $highProbablyIdentifiers = $this->getIdentifiersByUser($user, collect(self::ID_FIELDS_HIGH_PROBABLY)->toArray());

        $accurate = $this->getRelatedUsers($accurateIdentifiers);
        $highProbably = $this->getRelatedUsers($highProbablyIdentifiers)
            ->whereNotIn('id', $accurate->pluck('id'));
        $history = null;

        if ($withHistory) {
            $history1 = $this->getHistory($accurateIdentifiers, true);
            $history2 = $this->getHistory($highProbablyIdentifiers, false)
                ->whereNotIn('id', $history1->pluck('id'));
            $history = $history1
                ->merge($history2)
                ->sortBy('created_at');
        }

        return new IdentificationsDataDto(
            $accurate->values(),
            $highProbably->values(),
            $history?->values(),
            $history ? $this->getRelatedUniqueByColumn('ip', $history)->values() : null,
            $history ? $this->getRelatedUniqueByColumn('user_agent', $history)->values() : null,
            $this->userIdentificationRepository
                ->getIdsByCollection($accurateIdentifiers),
            $this->userIdentificationRepository
                ->getIdsByCollection($highProbablyIdentifiers),
        );
    }

    /**
     * Get history.
     *
     * @param Collection $identifiers
     * @param bool $isAccurate
     * @return Collection
     */
    public function getHistory(Collection $identifiers, bool $isAccurate): Collection
    {
        $history = $this->userIdentificationRepository->getByCollection($identifiers);

        return $history->map(function ($item) use ($isAccurate) {
            $item['is_accurate'] = $isAccurate;
            $item['is_high_probably'] = !$isAccurate;

            return $item;
        });
    }

    /**
     * Get related users.
     *
     * @param Collection $identifiers
     * @return Collection
     */
    public function getRelatedUsers(Collection $identifiers): Collection
    {
        $usersId = $this->userIdentificationRepository->getUserIdsByCollection($identifiers);
        $collection = collect([
            'id' => $usersId,
            'username' => $identifiers->get('userid')->values(),
            'email' => $identifiers->get('userid')->values(),
        ]);

        return $this->userRepository->getUsersByCollection($collection);
    }

    /**
     * Get all identifiers by user.
     *
     * @param User $user
     * @param array $fields
     * @return Collection
     */
    public function getIdentifiersByUser(User $user, array $fields): Collection
    {
        $identifiers = $this->getIdentifiers($user, $fields);

        foreach ($this->userRepository->getUsersByUsernameOrEmailList($identifiers->get('userid')) as $userItem) {
            $identifiers = $this->mergeIdentifiers($identifiers, $this->getIdentifiers($userItem, $fields));
        }

        foreach ($this->userRepository->getByIds($identifiers->get('users_id')) as $userItem) {
            $identifiers = $this->mergeIdentifiers($identifiers, $this->getIdentifiers($userItem, $fields));
        }

        //get usernames,emails
        $users = $this->userRepository
            ->getUsersIdsByIds($identifiers->get('users_id'));

        $identifiers = $this->mergeIdentifiers(
            $identifiers,
            $this->getIdentifiersByColumn(
                'userid', $users->pluck('username')
                ->merge($users->pluck('email')),
                $fields
            )
        );

        $crs32hash = crc32($identifiers->toJson());
        $max = 1000;

        while ($max > 0) {
            $max--;

            $identifiers = $this->getIdentifiers($identifiers, $fields);
            $crs32hashNew = crc32($identifiers->toJson());

            if ($crs32hash === $crs32hashNew) {
                break;
            }

            $crs32hash = $crs32hashNew;
        }

        return $identifiers;
    }

    /**
     * Get identifiers by user.
     *
     * @param Collection|User $data
     * @param array $fields
     * @return Collection
     */
    private function getIdentifiers(Collection|User $data, array $fields): Collection
    {
        $identifiers = $data instanceof User ?
            $this->getIdentifiersByColumn('users_id', $data->id, $fields) :
            $data;

        if ($data instanceof User) {
            $identifiers = $this->mergeIdentifiers(
                $identifiers,
                $this->getIdentifiersByColumn(
                    'userid',
                    [$data->email, $data->username],
                    $fields
                )
            );
        }

        foreach ($fields as $field) {
            $identifiers = $this->mergeIdentifiers(
                $identifiers,
                $this->getIdentifiersByColumn($field, $identifiers->get($field), $fields)
            );
        }

        return $identifiers;
    }

    /**
     * Get identifiers by column and value.
     *
     * @param string $column
     * @param mixed $value
     * @param array $fields
     * @return Collection
     */
    private function getIdentifiersByColumn(string $column, mixed $value, array $fields): Collection
    {
        $data = collect();

        foreach ($fields as $field) {
            $data->put(
                $field,
                $this->userIdentificationRepository
                    ->getIdentifiersByColumn($column, $value, $field)
            );
        }

        return $data;
    }

    /**
     * Merge identifiers.
     *
     * @param Collection $collection
     * @param Collection $new
     * @return Collection
     */
    private function mergeIdentifiers(Collection $collection, Collection $new): Collection
    {
        foreach ($new as $key => $item) {
            $values = $collection->has($key) ?
                $collection->get($key)->merge($item) :
                $collection->put($key, $item);

            $collection->put(
                $key,
                $values->unique()
            );
        }

        return $collection;
    }

    /**
     * Get unique related data by column
     * with first_date.
     *
     * @param string $field
     * @param Collection $history
     * @return Collection
     */
    private function getRelatedUniqueByColumn(string $field, Collection $history): Collection
    {
        $items = $history->whereNotNull($field)
            ->groupBy($field)
            ->keys();
        $data = collect();

        foreach ($items as $item) {
            $historyItem = $history->where($field, $item)
                ->first();

            $data->add([
                $field => $item,
                'first_date' => $historyItem->created_at,
                'is_accurate' => $historyItem->is_accurate
            ]);
        }

        return $data;
    }

    /**
     * Get {@see UpdaterService::class}.
     *
     * @return UpdaterService
     */
    private function getUpdaterService(): UpdaterService
    {
        return app(UpdaterService::class);
    }

    /**
     * Get {@see UserIdentificationRelationRepository::class}.
     *
     * @return UserIdentificationRelationRepository
     */
    private function getUserIdentificationRelationRepository(): UserIdentificationRelationRepository
    {
        return app(UserIdentificationRelationRepository::class);
    }
}
