<?php
declare(strict_types=1);

namespace App\Modules\Finance\Services\Admin;

use App\Modules\Core\Models\Task;
use App\Modules\Core\Services\TaskService;
use App\Modules\Finance\Dto\Api\Ethereum\Manage\EstimatedGasDto;
use App\Modules\Finance\Dto\Api\Finance\WalletApiDto;
use App\Modules\Finance\Dto\Api\Finance\WalletsApiDto;
use App\Modules\Finance\Dto\Services\Admin\Finance\EthereumSendDto;
use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use App\Modules\Finance\Enum\NetworkTypeEnum;
use App\Modules\Finance\Exceptions\Btc\GetBalanceException;
use App\Modules\Finance\Exceptions\SecureCryptRequestException;
use App\Modules\Finance\Exceptions\SecureCryptRequestParamsException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\RequestException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\ValidationException;
use App\Modules\Finance\Models\CryptoAddress;
use App\Modules\Finance\Repositories\CryptoAddressRepository;
use App\Modules\Finance\Repositories\CryptoCurrencyRepository;
use App\Modules\Finance\Services\BtcApiService;
use App\Modules\Finance\Services\V2\Ethereum\ManageService;
use App\Modules\Money\Models\Transaction;
use App\Modules\Users\Models\User;

/**
 * Admin service for finance operations.
 */
final class FinanceService
{
    /** @var array */
    private const MONEY_CURRENCY_TO_TASK_TYPE = [
        Transaction::CURRENCY_USDT_DEPOSITED => Task::TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DEPOSITED,
        Transaction::CURRENCY_USDT_DIVIDENTS => Task::TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DIVIDENDS,
        Transaction::CURRENCY_ABC => Task::TYPE_ADMIN_MASS_MONEY_OPERATION_ABC
    ];

    /**
     * This method add delayed task for add/withdraw money to/from all users.
     *
     * @param float $amount
     * @param int $currency
     * @param User $user
     * @return Task
     */
    public function massMoneyOperation(float $amount, int $currency, User $user): Task
    {
        $task = $this->getTaskService()
            ->new(
                static::MONEY_CURRENCY_TO_TASK_TYPE[$currency],
                collect(['amount' => $amount, 'currency' => $currency]),
                $user
            );

        return $task;
    }

    /**
     * Get wallets.
     *
     * @return WalletsApiDto
     * @throws GetBalanceException
     * @throws SecureCryptRequestException
     * @throws SecureCryptRequestParamsException
     */
    public function getWallets(): WalletsApiDto
    {
        try {
            $btcBalance = $this->getBtcApiService()
                ->getBalance();
        } catch (\App\Modules\Finance\Exceptions\Btc\KeyNotFoundException) {
            $btcBalance = 0;
        }

        return new WalletsApiDto(
            collect([
                new WalletApiDto(
                    NetworkTypeEnum::BITCOIN,
                    $btcBalance,
                    $this->getCryptoCurrencyRepository()
                        ->findByAddressAndNetwork(null, BitcoinNetworkEnum::BITCOIN)
                        ->toApiDto(),
                    collect(),
                    $this->getTaskService()
                        ->getByType(Task::TYPE_ADMIN_SEND_BTC_MONEY)
                        ->map(fn(Task $task) => $task->toApiDto())
                ),
                new WalletApiDto(
                    NetworkTypeEnum::ETHEREUM,
                    $this->getFinanceService()
                        ->getSummaryUSDBalanceOfEthereumType(),
                    null,
                    $this->getCryptoAddressRepository()
                        ->getWithBalancesByNetworkType(NetworkTypeEnum::ETHEREUM)
                        ->map(fn(CryptoAddress $address) => $address->toApiDto()),
                    $this->getTaskService()
                        ->getByType(Task::TYPE_ADMIN_SEND_ETHEREUM_TYPE_MONEY)
                        ->map(fn(Task $task) => $task->toApiDto())
                )
            ])
        );
    }

    /**
     * Geth esitamate.
     *
     * @param EthereumSendDto $ethereumSend
     * @return EstimatedGasDto
     * @throws RequestException
     * @throws ValidationException
     */
    public function gethEstimate(EthereumSendDto $ethereumSend): EstimatedGasDto
    {
        return $ethereumSend->balance->currency->isMainCurrencyInNetwork ?
            $this->getManageService()
                ->estimate(
                    $ethereumSend->balance->currency->network->toEnum(),
                    $ethereumSend->balance->address->address,
                    $ethereumSend->to,
                    $ethereumSend->amount->innerValue(),
                ) :
            $this->getManageService()
                ->estimateErc20(
                    $ethereumSend->balance->currency->network->toEnum(),
                    $ethereumSend->balance->currency->address,
                    $ethereumSend->to,
                    $ethereumSend->amount->innerValue(),
                    $ethereumSend->balance->currency->decimals,
                    $ethereumSend->balance->address->access_key
                );

    }

    /**
     * This method adding delayed task for send money with geth networks.
     *
     * @param EthereumSendDto $ethereumSend
     * @return Task
     */
    public function gethSend(EthereumSendDto $ethereumSend): Task
    {
        return $this->getTaskService()
            ->new(
                Task::TYPE_ADMIN_SEND_ETHEREUM_TYPE_MONEY,
                collect([
                    'address' => $ethereumSend->balance->address->toApiDto(),
                    'balance' => $ethereumSend->balance->toApiDto(),
                    'to' => $ethereumSend->to,
                    'amount' => $ethereumSend->amount->innerValue(),
                ]),
                $ethereumSend->user,
                true
            );
    }

    /**
     * This method add delayed task for send money from btc.
     *
     * @param User $user
     * @param string $to
     * @param float $amount
     * @return Task
     */
    public function bitcoinSend(User $user, string $to, float $amount): Task
    {
        return $this->getTaskService()
            ->new(
                Task::TYPE_ADMIN_SEND_BTC_MONEY,
                collect([
                    'to' => $to,
                    'amount' => $amount,
                ]),
                $user,
                true
            );
    }

    /**
     * Archive.
     *
     * @param CryptoAddress $address
     * @return void
     */
    public function archive(CryptoAddress $address): void
    {
        $address->is_archived = true;

        $address->save();
    }

    /**
     * Unarchived.
     *
     * @param CryptoAddress $address
     * @return void
     */
    public function unarchived(CryptoAddress $address): void
    {
        $address->is_archived = false;

        $address->save();
    }

    /**
     * Get {@see CryptoAddressRepository::class}.
     *
     * @return CryptoAddressRepository
     */
    private function getCryptoAddressRepository(): CryptoAddressRepository
    {
        return app(CryptoAddressRepository::class);
    }

    /**
     * Get {@see CryptoCurrencyRepository::class}.
     *
     * @return CryptoCurrencyRepository
     */
    private function getCryptoCurrencyRepository(): CryptoCurrencyRepository
    {
        return app(CryptoCurrencyRepository::class);
    }

    /**
     * Get {@see BtcApiService::class}.
     *
     * @return BtcApiService
     */
    private function getBtcApiService(): BtcApiService
    {
        return app(BtcApiService::class);
    }

    /**
     * Get {@see TaskService::class}.
     *
     * @return TaskService
     */
    private function getTaskService(): TaskService
    {
        return app(TaskService::class);
    }

    /**
     * Get {@see ManageService::class}.
     *
     * @return ManageService
     */
    private function getManageService(): ManageService
    {
        return app(ManageService::class);
    }

    /**
     * Get {@see \App\Modules\Finance\Services\FinanceService::class}.
     *
     * @return \App\Modules\Finance\Services\FinanceService
     */
    private function getFinanceService(): \App\Modules\Finance\Services\FinanceService
    {
        return app(\App\Modules\Finance\Services\FinanceService::class);
    }
}
