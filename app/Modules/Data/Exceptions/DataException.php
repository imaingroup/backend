<?php
declare(strict_types=1);

namespace App\Modules\Data\Exceptions;

use Exception;

/**
 * Abstract DataException.
 */
abstract class DataException extends Exception
{
}
