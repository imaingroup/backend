<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Create settings imperium.
 */
final class CreateSettingsImperium extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('settings_imperium', function (Blueprint $table) {
            $table->id();
            $table->text('download_win_url')
                ->nullable(true);
            $table->text('download_mac_url')
                ->nullable(true);
            $table->string('version')
                ->nullable(true);
            $table->timestamps();
        });

        DB::transaction(function() {
            DB::table('settings_imperium')
                ->insert([
                    'download_win_url' => '',
                    'download_mac_url' => '',
                    'version' => '',
                ]);

            $id = DB::table('data_types')
                ->insertGetId([
                    'name' => 'settings_imperium',
                    'slug' => 'settings-imperium',
                    'display_name_singular' => 'IMPERIUM',
                    'display_name_plural' => 'IMPERIUM',
                    'icon' => 'voyager-settings',
                    'model_name' => 'App\Modules\Settings\Models\SettingsImperium',
                    'generate_permissions' => 1,
                    'server_side' => 0,
                    'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}',
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'version',
                    'type' => 'text',
                    'display_name' => 'Version',
                    'required' => 0,
                    'browse' => 1,
                    'read' => 1,
                    'edit' => 1,
                    'add' => 1,
                    'delete' => 1,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:255"}}',
                    'order' => 0
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'download_win_url',
                    'type' => 'text',
                    'display_name' => 'Download WIN url',
                    'required' => 0,
                    'browse' => 1,
                    'read' => 1,
                    'edit' => 1,
                    'add' => 1,
                    'delete' => 1,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:10000"}}',
                    'order' => 1
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'download_mac_url',
                    'type' => 'text',
                    'display_name' => 'Download MAC url',
                    'required' => 0,
                    'browse' => 1,
                    'read' => 1,
                    'edit' => 1,
                    'add' => 1,
                    'delete' => 1,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:10000"}}',
                    'order' => 2
                ]);

            $permissions = ['browse', 'read', 'edit', 'add', 'delete'];

            foreach ($permissions as $permission) {
                $pId = DB::table('permissions')
                    ->insertGetId([
                        'key' => sprintf('%s_settings_imperium', $permission),
                        'table_name' => 'settings_imperium',
                        'created_at' => new DateTime(),
                        'updated_at' => new DateTime()
                    ]);

                if ($permission === 'edit') {
                    DB::table('permission_role')
                        ->insert([
                            'permission_id' => $pId,
                            'role_id' => 1
                        ]);
                }
            }

            DB::table('menu_items')
                ->insert([
                    'menu_id' => 1,
                    'title' => 'IMPERIUM',
                    'url' => '/XI400PGNB/settings-imperium/1/edit',
                    'target' => '_self',
                    'icon_class' => 'voyager-settings',
                    'color' => '#000000',
                    'parent_id' => 22,
                    'order' => 7,
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::transaction(function() {
            DB::table('data_types')
                ->where('name', 'settings_imperium')
                ->delete();

            DB::table('permissions')
                ->where('table_name', 'settings_imperium')
                ->delete();

            DB::table('menu_items')
                ->where('title', 'IMPERIUM')
                ->where('url', '/XI400PGNB/settings-imperium/1/edit')
                ->delete();
        });

        Schema::dropIfExists('settings_imperium');
    }
}
