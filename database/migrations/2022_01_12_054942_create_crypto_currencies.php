<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Create crypto currencies.
 */
final class CreateCryptoCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('crypto_currencies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('crypto_networks_id');
            $table->foreign('crypto_networks_id', 'crypto_currencies_crypto_networks_id_foreign')
                ->references('id')
                ->on('crypto_networks')
                ->restrictOnDelete();
            $table->string('name');
            $table->string('symbol');
            $table->unsignedInteger('decimals');
            $table->string('address')
                ->nullable(true);
            $table->string('tokenType')
                ->nullable(true);
            $table->timestamps();
            $table->unique(['address', 'crypto_networks_id'], 'crypto_currencies_address_crypto_networks_id_unique');
        });

        Schema::create('crypto_currencies_stats', function (Blueprint $table) {
            $table->primary('crypto_currencies_id');
            $table->unsignedBigInteger('crypto_currencies_id');
            $table->foreign('crypto_currencies_id', 'crypto_currencies_stats_crypto_currencies_id_foreign')
                ->references('id')
                ->on('crypto_currencies')
                ->cascadeOnDelete();
            $table->unsignedInteger('count');
            $table->unsignedInteger('senders')
                ->nullable(true);
            $table->unsignedInteger('receivers')
                ->nullable(true);
            $table->unsignedInteger('days')
                ->nullable(true);
            $table->date('from_date')
                ->nullable(true);
            $table->date('till_date')
                ->nullable(true);
            $table->double('amount')
                ->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('crypto_currencies_stats');
        Schema::dropIfExists('crypto_currencies');
    }
}
