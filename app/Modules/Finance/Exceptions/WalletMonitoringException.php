<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions;

/**
 * Wallet exception.
 */
final class WalletMonitoringException extends FinanceException
{
}
