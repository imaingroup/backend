<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds field disabled_recent_activities
 * to settings_dashboard table.
 */
class AddFieldDisabledRecentActivitiesToSettingsDashboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_dashboard', function (Blueprint $table) {
            $table->boolean('disabled_recent_activities')->default(false);
        });

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 49,
                'field' => 'disabled_recent_activities',
                'type' => 'checkbox',
                'display_name' => 'Disabled Recent Activities',
                'required' => 1,
                'browse' => 1,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '{}',
                'order' => 3
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_dashboard', function (Blueprint $table) {
            $table->dropColumn('disabled_recent_activities');
        });

        DB::table('data_rows')
            ->where('data_type_id', 49)
            ->where('field', 'disabled_recent_activities')
            ->delete();
    }
}
