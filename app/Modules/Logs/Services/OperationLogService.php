<?php
declare(strict_types=1);

namespace App\Modules\Logs\Services;

use App\Modules\Logs\Models\OperationLog;
use App\Modules\Core\Models\Task;
use App\Modules\Users\Models\User;
use App\Modules\Logs\Repositories\OperationLogRepository;

/**
 * Operation log service.
 */
final class OperationLogService
{
    /** @var OperationLogRepository Operation log repository. */
    private OperationLogRepository $operationLogRepository;

    /**
     * Constructor.
     *
     * @param \App\Modules\Logs\Repositories\OperationLogRepository $operationLogRepository
     */
    public function __construct(OperationLogRepository $operationLogRepository)
    {
        $this->operationLogRepository = $operationLogRepository;
    }

    /**
     * Add operation log.
     *
     * @param int $type
     * @param User $user
     * @param float|null $textChoice
     * @param array $data
     * @param Task|null $task
     * @return \App\Modules\Logs\Models\OperationLog
     */
    public function add(
        int $type,
        User $user,
        ?float $textChoice,
        array $data,
        Task $task = null
    ): OperationLog
    {
        return $this->operationLogRepository->create([
            'type' => $type,
            'users_id' => $user->id,
            'data' => $data,
            'description' => trans_choice(
                OperationLog::BIND_TYPE_TO_DESCRIPTION[$type],
                $textChoice,
                $data
            ),
            'tasks_id' => $task->id ?? null
        ]);
    }
}
