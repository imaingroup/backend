<?php

namespace App\Modules\News\Controllers;


use App\Modules\Core\Exceptions\DataException;
use App\Http\Controllers\Controller;
use App\Modules\News\Requests\NewsCategoriesRequest;
use App\Modules\News\Requests\NewsItemRequest;
use App\Modules\News\Requests\NewsLikeRequest;
use App\Modules\News\Requests\NewsRequest;
use App\Modules\News\Models\News;
use App\Modules\Core\Services\DataService;
use App\Modules\News\Services\NewsService;
use Illuminate\Support\Facades\Auth;

/**
 * News controller.
 */
final class NewsController extends Controller
{
    /** @var \App\Modules\News\Services\NewsService New service. */
    private NewsService $newsService;

    /** @var DataService Data service. */
    private DataService $dataService;

    /**
     * Constructor.
     *
     * @param \App\Modules\News\Services\NewsService $newsService
     * @param \App\Modules\Core\Services\DataService $dataService
     */
    public function __construct(NewsService $newsService, DataService $dataService)
    {
        $this->newsService = $newsService;
        $this->dataService = $dataService;
    }

    /**
     * Get news.
     *
     * @param \App\Modules\News\Requests\NewsRequest $request
     * @return array
     * @throws DataException
     */
    public function list(NewsRequest $request): array
    {
        $data = $request->validated();

        return $this->dataService->getData(
            DataService::TYPE_NEWS_LIST,
            [$data['sort'], $data['category'], $data['search'], $data['page']]
        );
    }

    /**
     * Get news item.
     *
     * @param News $news
     * @param \App\Modules\News\Requests\NewsItemRequest $request
     * @return array
     */
    public function item(News $news, NewsItemRequest $request): array
    {
        return [
            'data' => [
                'news' => $this->newsService->getNewsItem($news, Auth::user())
            ]
        ];
    }

    /**
     * Get categories.
     *
     * @param NewsCategoriesRequest $request
     * @return array
     * @throws DataException
     */
    public function categories(NewsCategoriesRequest $request): array
    {
        return $this->dataService->getData(DataService::TYPE_NEWS_CATEGORIES);
    }

    /**
     * Like news.
     *
     * @param News $news
     * @param \App\Modules\News\Requests\NewsLikeRequest $request
     * @return array
     */
    public function like(News $news, NewsLikeRequest $request): array
    {
        $this->newsService->like($news, Auth::user());

        return ['status' => true];
    }
}
