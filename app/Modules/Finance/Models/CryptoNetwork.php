<?php
declare(strict_types=1);

namespace App\Modules\Finance\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Finance\Contracts\Enum\NetworkEnumContract;
use App\Modules\Finance\Dto\Api\NetworkApiDto;
use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use BackedEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Crypto network model.
 *
 * @property-read int $id
 * @property string $code
 * @property int $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|CryptoCurrency[] $getCurrenciesSortedByCount
 */
final class CryptoNetwork extends Model
{
    /** {@inheritdoc} */
    protected $table = 'crypto_networks';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'type' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Relation with {@see CryptoCurrency::class}.
     *
     * @return HasMany
     */
    public function getCurrenciesSortedByCount(): HasMany
    {
        return $this->hasMany(CryptoCurrency::class, 'crypto_networks_id')
            ->select('crypto_currencies.*')
            ->leftJoin('crypto_currencies_stats as s', 'crypto_currencies.id', '=', 's.crypto_currencies_id')
            ->orderBy('crypto_currencies.tokenType')
            ->orderBy('s.count', 'desc')
            ->limit(500);
    }

    /**
     * To api dto.
     *
     * @return NetworkApiDto
     */
    public function toApiDto(): NetworkApiDto
    {
        return new NetworkApiDto(
            $this->code,
            $this->type,
            $this->id,
            $this->getCurrenciesSortedByCount
                ->map(fn(CryptoCurrency $currency) => $currency->toApiDto())
        );
    }

    /**
     * To enum.
     *
     * @return BackedEnum&NetworkEnumContract
     */
    public function toEnum(): NetworkEnumContract&BackedEnum
    {
        return EthereumNetworkEnum::tryFrom($this->code) ?? BitcoinNetworkEnum::tryFrom($this->code);
    }
}
