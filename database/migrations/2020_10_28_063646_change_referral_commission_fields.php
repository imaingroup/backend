<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migrations changes referral commission fields.
 */
class ChangeReferralCommissionFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('is_promo_code_commission_default', 'is_referral_abc_commission_default');
            $table->renameColumn('promo_code_commission', 'referral_abc_commission');

            $table->boolean('is_referral_usdt_commission_default')->default(true);
            $table->float('referral_usdt_commission', 16, 8)->default(0);
        });

        Schema::table('settings_prices', function (Blueprint $table) {
            $table->renameColumn('referral_commission', 'referral_abc_commission');

            $table->float('referral_usdt_commission', 16, 8)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('is_referral_abc_commission_default', 'is_promo_code_commission_default');
            $table->renameColumn('referral_abc_commission', 'promo_code_commission');

            $table->dropColumn('is_referral_usdt_commission_default');
            $table->dropColumn('referral_usdt_commission');
        });

        Schema::table('settings_prices', function (Blueprint $table) {
            $table->renameColumn('referral_abc_commission', 'referral_commission');

            $table->dropColumn('referral_usdt_commission');
        });
    }
}
