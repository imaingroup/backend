<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Trade history table.
 */
class CreateTradeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('trades_id');
            $table->float('profit_usdt', 16, 8);
            $table->float('profit_abc', 16, 8);
            $table->float('interest_rate', 16, 8);
            $table->date('date');
            $table->boolean('is_no_trade');
            $table->timestamps();

            $table->foreign('trades_id')
                ->references('id')
                ->on('trades')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_history');
    }
}
