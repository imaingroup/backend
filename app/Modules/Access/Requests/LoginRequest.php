<?php
declare(strict_types=1);

namespace App\Modules\Access\Requests;

use App\Modules\Security\Models\UserIdentification;
use App\Modules\Security\Services\Identification\IdentificationService;
use App\Modules\Users\Repositories\UserRepository;
use App\Modules\Users\Services\UserService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;

/**
 * This request class need for validating data from register form.
 */
final class LoginRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param UserRepository $userRepository
     * @param \App\Modules\Users\Services\UserService $userService
     * @param \App\Modules\Security\Services\Identification\IdentificationService $identificationService
     */
    public function __construct(
        private UserRepository $userRepository,
        private UserService $userService,
        private IdentificationService $identificationService
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $recaptchaEnabled = (int)env('GOOGLE_RECAPTCHA_ENABLED') === 1;
        $data = [];

        if ($recaptchaEnabled) {
            $data['g-recaptcha-response'] = 'required|scalar|recaptcha';
        }

        return array_merge($data, [
            'email' => 'required|scalar',
            'password' => 'required|scalar',
            '2fa' => 'nullable|scalar',
            'is_remember_me' => [
                'nullable',
                'scalar',
                Rule::in(['0', '1'])
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function validated()
    {
        $data = parent::validated();
        $data['is_remember_me'] = isset($data['is_remember_me']) && $data['is_remember_me'] == '1';

        $emailOrUsername = (string)$data['email'];
        $user = $this->userRepository->findByEmail($emailOrUsername);

        if (!$user) {
            $user = $this->userRepository->findByUsername($emailOrUsername);
        }

        $data['email'] = $user->email;

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function failedValidation(Validator $validator): void
    {
        $this->identificationService
            ->confirmLastIdentification(UserIdentification::ACTION_LOGIN_ERROR);

        parent::failedValidation($validator);
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     * @throws SecretKeyTooShortException
     */
    private function after(Validator $validator)
    {
        $emailOrUsername = (string)$this['email'];
        $user = $this->userRepository->findByEmail($emailOrUsername);

        if (!$user) {
            $user = $this->userRepository->findByUsername($emailOrUsername);

            if (!$user) {
                $validator->errors()->add('email', 'Email or username is incorrect');
                return;
            }
        }

        if (!Hash::check($this['password'], $user->password)) {
            $validator->errors()->add('password', 'Email or password is incorrect');
            return;
        }

        if (!$user->email_verified_at) {
            $this->session()->put('current_user', $user);

            $validator->errors()->add('activated', 'Your account still isn\'t activated');
            return;
        }

        if ($user->is_blocked) {
            $validator->errors()->add('blocked', 'Your account was blocked');
            $validator->errors()->add('block_reason', $user->block_reason);
            $validator->errors()->add('blocked_at', $user->blocked_at);

            return;
        }

        if ($user->is_2fa && !$this['2fa']) {
            $validator->errors()->add('2fa', '2FA required');
            return;
        }

        if ($user->is_2fa) {
            $string2fa = trim(str_replace(' ', '', (string)$this['2fa']));

            if (!$this->userService->check2fa($user, $string2fa)) {
                $validator->errors()->add('2fa', 'Incorrect 2FA');
                return;
            }
        }
    }
}
