<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Controllers;

use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Core\Exceptions\DataException;
use App\Modules\Deposites\Exceptions\DepositServiceException;
use App\Http\Controllers\Controller;
use App\Modules\Users\Models\User;
use App\Modules\Deposites\Requests\CreateDepositRequest;
use App\Modules\Deposites\Requests\CreateNewDepositRequest;
use App\Modules\Deposites\Requests\DepositDataRequest;
use App\Modules\Deposites\Requests\HistoryRequest;
use App\Modules\Deposites\Services\DepositService;
use App\Modules\Core\Services\DataService;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Deposit controller.
 */
final class DepositController extends Controller
{
    /**
     * Make deposit.
     *
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function make(): Response
    {
        return $this->inTransaction(function (): void {
            $user = Auth::user();

            User::refreshAndLockForUpdate($user);

            $this->getDepositService()
                ->make(...app(CreateDepositRequest::class)->validated());
        });
    }

    /**
     * Get deposits data.
     *
     * @param DepositDataRequest $request
     * @return ResponseDto
     * @throws Exception
     */
    public function data(DepositDataRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getDepositService()
                ->getData(Auth::user())
        );
    }

    /**
     * Make new deposit.
     *
     * @param CreateNewDepositRequest $request
     * @return array
     * @throws DepositServiceException
     */
    #[ArrayShape(['status' => "bool"])]
    public function makeNew(CreateNewDepositRequest $request): array
    {
        $this->getDepositService()
            ->closeLastInvoice(Auth::user());

        return ['status' => true];
    }

    /**
     * Get transactions history.
     *
     * @param HistoryRequest $request
     * @return ResponseDto
     */
    public function history(HistoryRequest $request): ResponseDto
    {
        return new ResponseDto(
            $this->getDepositService()
                ->history($request->validated())
        );
    }

    /**
     * Get {@see DepositService::class}.
     *
     * @return DepositService
     */
    private function getDepositService(): DepositService
    {
        return app(DepositService::class);
    }

    /**
     * Get {@see DataService::class}.
     *
     * @return DataService
     */
    private function getDataService(): DataService
    {
        return app(DataService::class);
    }
}
