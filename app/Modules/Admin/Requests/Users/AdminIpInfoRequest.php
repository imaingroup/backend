<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Users;


use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Get info by ip.
 */
final class AdminIpInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['ip' => "string"])]
    public function rules(): array
    {
        return [
            'ip' => 'required|ip'
        ];
    }
}
