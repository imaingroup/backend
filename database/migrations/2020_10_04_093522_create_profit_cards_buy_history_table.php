<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfitCardsBuyHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profit_card_buy_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('profit_cards_id');
            $table->unsignedBigInteger('users_id');
            $table->float('price', 16, 8);
            $table->integer('abc');
            $table->float('bonus', 16, 8);
            $table->timestamps();

            $table->foreign('profit_cards_id')->references('id')->on('profit_cards');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profit_card_buy_histories');
    }
}
