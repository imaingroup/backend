<?php


namespace App\Modules\Landing\Services;


use App\Modules\Admin\Exceptions\TranslateException;
use App\Modules\Notifications\Jobs\SendContactFormAdminEmailJob;
use App\Modules\Landing\Models\Contact;
use App\Modules\Landing\Models\Subscribe;
use App\Modules\Users\Models\User;
use App\Modules\Languages\Repositories\LangRepository;
use App\Modules\News\Repositories\NewsRepository;
use App\Modules\Settings\Repositories\SettingsLandingRepository;
use App\Modules\Settings\Repositories\SettingsLinkRepository;
use App\Modules\Settings\Repositories\SettingsTimerRepository;
use App\Modules\Settings\Services\AbcSettingsService;
use App\Modules\Languages\Services\LanguageService;
use App\Modules\Admin\Services\TranslateService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * This service need form generate data from landing
 * and save data from landing.
 */
final class LandingService
{
    /** @var NewsRepository News repository. */
    private NewsRepository $newsRepository;

    /** @var SettingsLandingRepository Settings landin repository. */
    private SettingsLandingRepository $settingsLandingRepository;

    /** @var SettingsLinkRepository Settings link repository. */
    private SettingsLinkRepository $settingsLinkRepository;

    /** @var \App\Modules\Settings\Services\AbcSettingsService Abc settings service. */
    private AbcSettingsService $abcSettingsService;

    /** @var \App\Modules\Settings\Repositories\SettingsTimerRepository Settings timer repository. */
    private SettingsTimerRepository $settingsTimerRepository;

    /** @var LangRepository Lang repository. */
    private LangRepository $langRepository;

    /** @var LanguageService Language service. */
    private \App\Modules\Languages\Services\LanguageService $languageService;

    /** @var TranslateService Translate service. */
    private TranslateService $translateService;

    /**
     * Constructor.
     *
     * @param NewsRepository $newsRepository
     * @param \App\Modules\Settings\Repositories\SettingsLandingRepository $settingsLandingRepository
     * @param SettingsLinkRepository $settingsLinkRepository
     * @param \App\Modules\Settings\Services\AbcSettingsService $abcSettingsService
     * @param \App\Modules\Settings\Repositories\SettingsTimerRepository $settingsTimerRepository
     * @param \App\Modules\Languages\Repositories\LangRepository $langRepository
     * @param LanguageService $languageService
     * @param \App\Modules\Admin\Services\TranslateService $translateService
     */
    public function __construct(
        NewsRepository $newsRepository,
        SettingsLandingRepository $settingsLandingRepository,
        SettingsLinkRepository $settingsLinkRepository,
        \App\Modules\Settings\Services\AbcSettingsService $abcSettingsService,
        SettingsTimerRepository $settingsTimerRepository,
        LangRepository $langRepository,
        LanguageService $languageService,
        TranslateService $translateService
    )
    {
        $this->newsRepository = $newsRepository;
        $this->settingsLandingRepository = $settingsLandingRepository;
        $this->settingsLinkRepository = $settingsLinkRepository;
        $this->abcSettingsService = $abcSettingsService;
        $this->settingsTimerRepository = $settingsTimerRepository;
        $this->langRepository = $langRepository;
        $this->languageService = $languageService;
        $this->translateService = $translateService;
    }

    /**
     * Get content for landing page.
     *
     * @return array[]
     * @throws \App\Modules\Admin\Exceptions\TranslateException
     */
    public function getContent(): array
    {
        $news = $this->newsRepository->getLandingNews(3);
        $data = [
            'news' => [],
            'settings' => [
                'landing' => $this->settingsLandingRepository->get(),
                'links' => [],
                'abc' => $this->abcSettingsService->getData(),
                'timer' => $this->settingsTimerRepository->get(),
                'current_time_utc' => (new \DateTime())->format('Y-m-d H:i:s'),
                'langs' => [],
                'language' => $this->languageService->getLocale(),
                'translates' => $this->translateService->getTranslates(
                    [
                        'base',
                        'landing'
                    ]
                )
            ],
            'authenticated' => Auth::check()
        ];

        $data['settings']['landing']->video_main = $data['settings']['landing']->getVideoMain();
        $data['settings']['landing']->video_manifest = $data['settings']['landing']->getVideoManifest();

        if ($data['settings']['landing']->statement_image_1) {
            $data['settings']['landing']->statement_image_1 = Storage::disk('local')->url($data['settings']['landing']->statement_image_1);
        }

        if ($data['settings']['landing']->statement_image_2) {
            $data['settings']['landing']->statement_image_2 = Storage::disk('local')->url($data['settings']['landing']->statement_image_2);
        }

        foreach ($this->langRepository->all() as $lang) {
            $data['settings']['langs'][] = [
                'id' => $lang->sid,
                'short_name' => $lang->short_name,
                'name' => $lang->name,
                'icon' => Storage::disk('local')->url($lang->icon)
            ];
        }

        foreach ($news as $newsItem) {
            $data['news'][] = [
                'id' => $newsItem->sid,
                'title' => $newsItem->getTranslates('title'),
                'image' => Storage::disk('local')->url($newsItem->preview_image),
                'content' => $newsItem->getTranslates('catalog_preview')
            ];
        }

        $data['settings']['landing']->ito_title = $data['settings']['landing']->getTranslates('ito_title');
        $data['settings']['landing']->ito_text = $data['settings']['landing']->getTranslates('ito_text');
        $data['settings']['landing']->what_is_abc_text = $data['settings']['landing']->getTranslates('what_is_abc_text');
        $data['settings']['landing']->bottom_text = $data['settings']['landing']->getTranslates('bottom_text');

        unset($data['settings']['landing']->id);
        unset($data['settings']['landing']->translations);
        unset($data['settings']['landing']->created_at);
        unset($data['settings']['landing']->updated_at);

        foreach ($this->settingsLinkRepository->getAll() as $link) {
            $data['settings']['links'][] = [
                'href' => $link->href,
                'is_social' => $link->is_social,
                'is_product' => $link->is_product,
                'sort' => $link->sort,
                'is_visible' => $link->is_visible,
                'class' => $link->class,
                'title' => $link->getTranslates('title')
            ];
        }

        $data['settings']['timer']->title = $data['settings']['timer']->getTranslates('title');
        $data['settings']['timer']->title_when_it_end = $data['settings']['timer']->getTranslates('title_when_it_end');
        $data['settings']['timer']->text_when_it_end = $data['settings']['timer']->getTranslates('text_when_it_end');
        $data['settings']['timer']->description = $data['settings']['timer']->getTranslates('description');
        unset($data['settings']['timer']->translations);
        unset($data['settings']['timer']->id);
        unset($data['settings']['timer']->created_at);
        unset($data['settings']['timer']->updated_at);

        return $data;
    }

    /**
     * Create new contact.
     *
     * @param array $data
     * @param User|null $user
     * @return \App\Modules\Landing\Models\Contact
     */
    public function createContact(array $data, ?User $user = null): Contact
    {
        $contact = new Contact();
        $contact->name = $data['name'];
        $contact->email = $data['email'];
        $contact->message = $data['message'];
        $contact->users_id = $user ? $user->id : null;
        $contact->save();

        SendContactFormAdminEmailJob::dispatch($contact);

        return $contact;
    }

    /**
     * Create new subscribe.
     *
     * @param string $email
     * @param User|null $user
     * @return Subscribe
     */
    public function createSubscribe(string $email, ?User $user = null): Subscribe
    {
        $subscribe = new Subscribe();
        $subscribe->email = $email;
        $subscribe->users_id = $user ? $user->id : null;
        $subscribe->save();

        return $subscribe;
    }
}
