<?php

namespace App\Modules\Languages\Models;

use App\Modules\Core\Models\Model;

final class Lang extends Model
{
    protected $table = 'langs';

    /**
     * Events for model.
     */
    protected static function booted()
    {
        static::creating(function (Lang $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }
}
