<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * StatusException.
 */
final class StatusException extends BtcException
{
}
