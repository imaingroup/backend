<?php
declare(strict_types=1);

namespace App\Modules\Messages\Models;

use App\Modules\Users\Models\User;
use App\Modules\Core\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Contact model.
 */
final class Contact extends Model
{
    const GROUP_ADMINISTRATION = 1;
    const GROUP_TEAM_LEADER = 2;
    const GROUP_TEAMMATES = 3;
    const GROUP_USERS = 4;
    const GROUP_BLACK_LIST = 5;

    const CHAT_GROUPS = [
        self::GROUP_ADMINISTRATION => 'Administration',
        self::GROUP_TEAM_LEADER => 'Team leader',
        self::GROUP_TEAMMATES => 'Teammates',
        self::GROUP_USERS => 'Users',
        self::GROUP_BLACK_LIST => 'Black list',
    ];

    protected $table = 'messages_contacts';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'users_id' => 'integer',
        'contact_users_id' => 'integer',
        'sid' => 'integer',
        'deleted' => 'boolean',
        'blocked' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * {@inheritDoc}
     */
    public function getRouteKeyName(): string
    {
        return 'sid';
    }

    /**
     * Events for model.
     */
    protected static function booted()
    {
        Contact::creating(function (Contact $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }

    /**
     * Get relation with User.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Get relation with User.
     *
     * @return BelongsTo
     */
    public function contact(): BelongsTo
    {
        return $this->belongsTo(User::class, 'contact_users_id');
    }

    /**
     * This user is owner for this contact.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return bool
     */
    public function isOwner(\App\Modules\Users\Models\User $user): bool
    {
        return $this->users_id === $user->id;
    }
}
