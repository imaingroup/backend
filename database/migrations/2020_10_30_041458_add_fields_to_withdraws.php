<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds fields to withdraws table.
 */
class AddFieldsToWithdraws extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdraws', function (Blueprint $table) {
            $table->float('frozen_usdt_deposited', 16, 8);
            $table->float('frozen_usdt_dividends', 16, 8);
            $table->string('txid', 255)->nullable(true);
            $table->dateTime('confirmed_at')->nullable(true);
            $table->dateTime('declined_at')->nullable(true);
            $table->unsignedBigInteger('related_transactions_id');

            $table->foreign('related_transactions_id')
                ->references('id')
                ->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table withdraws drop foreign key withdraws_related_transactions_id_foreign;');

        Schema::table('withdraws', function (Blueprint $table) {
            $table->dropColumn('frozen_usdt_deposited');
            $table->dropColumn('frozen_usdt_dividends');
            $table->dropColumn('txid');
            $table->dropColumn('confirmed_at');
            $table->dropColumn('declined_at');
            $table->dropColumn('related_transactions_id');
        });
    }
}
