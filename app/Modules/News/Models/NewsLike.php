<?php

namespace App\Modules\News\Models;

use App\Modules\Core\Models\Model;

final class NewsLike extends Model
{
    protected $table = 'news_likes';
}
