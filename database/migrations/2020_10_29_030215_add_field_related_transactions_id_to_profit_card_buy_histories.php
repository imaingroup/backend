<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds related_transactions_id field
 * to profit_card_buy_histories table.
 */
class AddFieldRelatedTransactionsIdToProfitCardBuyHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profit_card_buy_histories', function (Blueprint $table) {
            $table->unsignedBigInteger('related_transactions_id');

            $table->foreign('related_transactions_id')
                ->references('id')
                ->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table profit_card_buy_histories drop foreign key profit_card_buy_histories_related_transactions_id_foreign;');

        Schema::table('profit_card_buy_histories', function (Blueprint $table) {
            $table->dropColumn('related_transactions_id');
        });
    }
}
