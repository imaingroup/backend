<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

use App\Modules\Finance\Exceptions\FinanceException;

/**
 * Abstract btc exception.
 */
abstract class BtcException extends FinanceException
{
}
