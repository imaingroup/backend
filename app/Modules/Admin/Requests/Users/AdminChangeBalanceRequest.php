<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Users;

use App\Modules\Money\Models\Transaction;
use App\Modules\Users\Repositories\UserRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * This request class validates request when
 * admin changing balance in admin.
 */
final class AdminChangeBalanceRequest extends FormRequest
{
    /** @var \App\Modules\Users\Repositories\UserRepository User repository. */
    private UserRepository $userRepository;

    /**
     * Constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'change_balance_user_id' => 'required|scalar|exists:App\Modules\Users\Models\User,id',
            'change_balance_amount' => 'required|scalar|regex:/^(-)?\d*(\.\d+)?$/',
            'change_balance_type' => [
                'required',
                'scalar',
                Rule::in(['abc', 'usdt_deposited', 'usdt_dividents']),
            ],
            'change_balance_commission_free' => [
                'nullable',
                'scalar',
                Rule::in([1]),
            ]
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator)
    {
        $amount = (float)$this->get('change_balance_amount');

        $user = $this->userRepository->find($this->get('change_balance_user_id', 0));

        if (!$user) {
            return;
        }

        if ($this['change_balance_type'] == 'abc') {
            $balance = $user->balance_abc;
        } else if ($this['change_balance_type'] == 'usdt_deposited') {
            $balance = $user->deposited_usdt;
        } else {
            $balance = $user->dividents_usdt;
        }

        if ($amount < 0 && abs($amount) > $balance) {
            $validator->errors()->add('change_balance_amount', \sprintf('Balance type %s does not have enough money', $this['change_balance_type']));
        }
    }

    /**
     * {@inheritdoc }
     */
    public function validated(): array
    {
        $data = parent::validated();

        if ($this['change_balance_type'] == 'abc') {
            $currency = Transaction::CURRENCY_ABC;
        } else if ($this['change_balance_type'] == 'usdt_deposited') {
            $currency = Transaction::CURRENCY_USDT_DEPOSITED;
        } else {
            $currency = Transaction::CURRENCY_USDT_DIVIDENTS;
        }

        $data['change_balance_amount'] = (float)$data['change_balance_amount'];
        $data['currency'] = $currency;
        $data['change_balance_commission_free'] = isset($data['change_balance_commission_free']);

        return $data;
    }
}
