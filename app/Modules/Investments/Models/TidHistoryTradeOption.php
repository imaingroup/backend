<?php

namespace App\Modules\Investments\Models;

use App\Modules\Core\Models\Model;

final class TidHistoryTradeOption extends Model
{
    protected $table = 'tid_history_trade_option';

    /**
     * Events for model.
     */
    protected static function booted()
    {
        static::saved(function (TidHistoryTradeOption $model) {
            TidHistoryTradeOption::query()
                ->where('id', $model->id)
                ->where('percent', -0)
                ->update([
                    'percent' => 0
                ]);
        });
    }

    /**
     * Get rounded percent.
     *
     * @return float|null
     */
    public function getRoundedPercentAttribute(): ?float
    {
        if ($this->percent !== null) {
            return round($this->percent, 2);
        }

        return $this->percent;
    }
}
