<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds deleted field
 * to messages_contacts table.
 */
final class AddFieldDeletedToContact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        /*Schema::table('messages_contacts', function (Blueprint $table) {
            $table->boolean('deleted')
                ->default(false)
                ->after('sid');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        /*Schema::table('messages_contacts', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });*/
    }
}
