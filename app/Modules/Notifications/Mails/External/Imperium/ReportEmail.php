<?php
declare(strict_types=1);

namespace App\Modules\Notifications\Mails\External\Imperium;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Contact mail class.
 */
final class ReportEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new mail instance.
     *
     * @param string $message
     */
    public function __construct(
        private string $message
    )
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->subject('Report about a bug in the Imperium wallet.')
            ->markdown('emails.external.imperium.report', [
                'message' => $this->message,
            ]);
    }
}
