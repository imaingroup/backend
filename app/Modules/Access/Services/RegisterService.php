<?php
declare(strict_types=1);

namespace App\Modules\Access\Services;

use App\Modules\Messages\Exceptions\ContactExistsException;
use App\Modules\Notifications\Jobs\SendActivationAccountEmailJob;
use App\Modules\Users\Models\User;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Security\Models\UserIdentification;
use App\Modules\Users\Repositories\UserRepository;
use App\Modules\Core\Services\ClientInfoService;
use App\Modules\Security\Services\Identification\IdentificationService;
use App\Modules\Languages\Services\LanguageService;
use App\Modules\Messages\Services\MessageService;
use App\Modules\PromoCodes\Services\PromoCodeService;
use Illuminate\Support\Str;

/**
 * Register service.
 */
final class RegisterService
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Users\Repositories\UserRepository $userRepository
     * @param \App\Modules\Languages\Services\LanguageService $languageService
     * @param PromoCodeService $promoCodeService
     * @param MessageService $messageService
     * @param \App\Modules\Core\Services\ClientInfoService $clientInfoService
     * @param \App\Modules\Security\Services\Identification\IdentificationService $identificationService
     */
    public function __construct(
        private UserRepository $userRepository,
        private LanguageService $languageService,
        private PromoCodeService $promoCodeService,
        private MessageService $messageService,
        private \App\Modules\Core\Services\ClientInfoService $clientInfoService,
        private \App\Modules\Security\Services\Identification\IdentificationService $identificationService
    )
    {
    }

    /**
     * Register user processing.
     *
     * @param array $data
     * @return \App\Modules\Users\Models\User
     * @throws TransactionNotStarted
     */
    public function register(array $data): User
    {
        $teamLeadUser = null;
        if (!empty($data['promo_code'])) {
            $teamLeadUser = $this->userRepository->findByPromoCode($data['promo_code']);
        }

        $user = $this->userRepository->create([
            'email' => $data['email'],
            'username' => $data['username'],
            'name' => $data['username'],
            'btc' => '',
            'promo_code' => $this->promoCodeService->generatePromoCode(),
            'langs_id' => $this->languageService->getLanguage()->id,
            'ip' => $this->clientInfoService->getRealIp(),
            'ip_country' => $this->clientInfoService->getCountry() ?? '',
            'last_activity_at' => new \DateTime(),
            'password' => bcrypt($data['password']),
            'email_token' => Str::random(60),
            'team_member_id' => $teamLeadUser ? $teamLeadUser->id : null
        ]);

        $this->addDefaultContacts($user);

        if ($teamLeadUser) {
            User::refreshAndLockForUpdate($teamLeadUser);

            $teamLeadUser->team_quantity++;

            $teamLeadUser->save();
        }

        $this->promoCodeService->addBonusAfterRegister($user);

        SendActivationAccountEmailJob::dispatch($user);

        $this->identificationService
            ->confirmLastIdentification(UserIdentification::ACTION_REGISTER_SUCCESS, $user);

        return $user;
    }

    /**
     * Add default contacts at register.
     *
     * @param User $user
     */
    public function addDefaultContacts(User $user)
    {
        try {
            $this->messageService->addContact(
                $user,
                $this->userRepository->find(User::ADMIN_USER_ID_FOR_CONTACTS)
            );
        } catch (ContactExistsException $e) {
        }

        $teamLead = $user->teamLead;

        if ($teamLead) {
            try {
                $this->messageService->addContact(
                    $user,
                    $teamLead
                );
            } catch (ContactExistsException $e) {
            }

            try {
                $this->messageService->addContact(
                    $teamLead,
                    $user
                );
            } catch (ContactExistsException $e) {
            }
        }
    }
}
