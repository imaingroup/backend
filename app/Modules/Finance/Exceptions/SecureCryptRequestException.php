<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions;

use JetBrains\PhpStorm\Pure;
use stdClass;
use Throwable;

/**
 * SecureCryptRequest exception.
 */
final class SecureCryptRequestException extends FinanceException
{
    /**
     * Constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param stdClass|null $error
     */
    #[Pure]
    public function __construct($message = "", $code = 0, Throwable $previous = null, public ?stdClass $error = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
