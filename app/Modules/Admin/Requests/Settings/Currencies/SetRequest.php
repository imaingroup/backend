<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Settings\Currencies;

use App\Modules\Finance\Models\CryptoCurrency;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Set currency request.
 *
 * @property-read CryptoCurrency|mixed $cryptoCurrency
 */
final class SetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->cryptoCurrency instanceof CryptoCurrency;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'deposit_min' => "string",
        'deposit_block' => "string",
        'confirmations_min' => "string",
        'luft_deposit' => "string",
        'luft_withdraw' => "string",
        'rate' => "string",
        'custom_rate' => "string",
        'name' => "string",
        'full_name' => "string",
        'order' => "string",
    ])]
    public function rules(): array
    {
        return [
            'deposit_min' => 'required|numeric|gt:0|max:99999999.99',
            'deposit_block' => 'required|boolean',
            'confirmations_min' => 'required|integer|min:0|max:10000',
            'luft_deposit' => 'required|numeric|gte:0|max:99999999.99',
            'luft_withdraw' => 'required|numeric|gte:0|max:99999999.99',
            'rate' => 'nullable|numeric|gte:0|max:9999999999999999.9999999999999999',
            'custom_rate' => 'required|boolean',
            'name' => 'required|scalar|max:255',
            'full_name' => 'required|scalar|max:255',
            'order' => 'required|integer|min:-2147483647|max:2147483647',
        ];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape([
        'currency' => CryptoCurrency::class,
        'depositMin' => "float",
        'depositBlock' => "boolean",
        'confirmationsMin' => "int",
        'luftDeposit' => "float",
        'luftWithdraw' => "float",
        'rate' => "float|null",
        'customRate' => "boolean",
        'name' => "string",
        'fullName' => "string",
        'order' => "integer"
    ])]
    public function validated(): array
    {
        $customRate = filter_var($this['custom_rate'], FILTER_VALIDATE_BOOLEAN);

        return [
            'currency' => $this->cryptoCurrency,
            'depositMin' => (float)$this['deposit_min'],
            'depositBlock' => filter_var($this['deposit_block'], FILTER_VALIDATE_BOOLEAN),
            'confirmationsMin' => (int)$this['confirmations_min'],
            'luftDeposit' => (float)$this['luft_deposit'],
            'luftWithdraw' => (float)$this['luft_withdraw'],
            'rate' => $customRate ? (float)$this['rate'] : null,
            'customRate' => $customRate,
            'name' => $this['name'],
            'fullName' => $this['full_name'],
            'order' => (int)$this['order'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()
            ->after(function (Validator $validator) {
                if (count($validator->failed()) === 0) {
                    $this->after($validator);
                }
            });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    private function after(Validator $validator): void
    {
        $customRate = filter_var($this['custom_rate'], FILTER_VALIDATE_BOOLEAN);

        if ($customRate && empty($this['rate'])) {
            $validator->errors()
                ->add('rate', 'Rate must be specified if custom rate is true.');
        } else if (!$customRate && !$this->cryptoCurrency->market) {
            $validator->errors()
                ->add('rate', 'This currency required to set custom rate.');
        }
    }
}
