<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Settings\Currencies;

use App\Modules\Settings\Models\DepositCurrency;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Delete currency request.
 *
 * @property-read DepositCurrency|mixed $depositCurrency
 */
final class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->depositCurrency instanceof DepositCurrency;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): DepositCurrency
    {
        return $this->depositCurrency;
    }
}
