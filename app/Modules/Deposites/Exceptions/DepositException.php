<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Exceptions;

use Exception;

/**
 * Abstract DepositException.
 */
abstract class DepositException extends Exception
{
}
