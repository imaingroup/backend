<?php


namespace App\Modules\Core\Services;


/**
 * Client info service.
 */
class ClientInfoService
{
    /**
     * Get real client ip.
     *
     * @return string|null
     */
    public function getRealIp(): ?string
    {
        $request = request();
        $cfIp = $request->server('HTTP_CF_CONNECTING_IP');

        return $cfIp ?? $request->getClientIp();
    }

    /**
     * Get user ip country.
     *
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return request()->server('HTTP_CF_IPCOUNTRY');
    }
}
