<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration changes luft fields.
 */
class ChangeLuftFieldsInSettingsPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->renameColumn('luft_btc_price', 'luft_deposit_btc_price');
            $table->float('luft_withdraw_btc_price')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->renameColumn('luft_deposit_btc_price', 'luft_btc_price');
            $table->dropColumn('luft_withdraw_btc_price');
        });
    }
}
