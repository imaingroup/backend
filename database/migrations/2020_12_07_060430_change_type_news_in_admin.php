<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration change type for field text to "CK Editor".
 */
final class ChangeTypeNewsInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('data_rows')
            ->where('data_type_id', 10)
            ->where('field', 'text')
            ->update([
                'type' => 'CK Editor'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('data_rows')
            ->where('data_type_id', 10)
            ->where('field', 'text')
            ->update([
                'type' => 'text_area'
            ]);
    }
}
