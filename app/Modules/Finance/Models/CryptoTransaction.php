<?php
declare(strict_types=1);

namespace App\Modules\Finance\Models;

use App\Modules\Core\Models\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Crypto transaction model.
 *
 * @property-read int $id
 * @property-read int $crypto_currencies_id
 * @property string $txid
 * @property string $eth_from
 * @property float $amount
 * @property integer $confirmations
 * @property-read integer $crypto_addresses_id
 * @property-read integer|null $type_old
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read CryptoAddress $address
 * @property-read CryptoCurrency $currency
 */
final class CryptoTransaction extends Model
{
    /** {@inheritdoc} */
    protected $table = 'crypto_transactions';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'crypto_currencies_id' => 'integer',
        'txid' => 'string',
        'eth_from' => 'string',
        'amount' => 'float',
        'usdt_amount' => 'float',
        'confirmations' => 'integer',
        'crypto_addresses_id' => 'integer',
        'type_old' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Relation with {@see CryptoAddress::class}.
     *
     * @return BelongsTo
     */
    public function address(): BelongsTo
    {
        return $this->belongsTo(CryptoAddress::class, 'crypto_addresses_id');
    }

    /**
     * Relation with {@see CryptoCurrency::class}.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_currencies_id');
    }
}
