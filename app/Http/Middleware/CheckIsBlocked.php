<?php


namespace App\Http\Middleware;


use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * This middleware stopping session if client was blocked.
 */
final class CheckIsBlocked
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (Auth::check() && Auth::user()->is_blocked) {
            Auth::guard()->logout();

            if($request->ajax()) {
                throw new AuthenticationException('Unauthenticated');
            }
            else {
                return abort(403);
            }
        }

        return $next($request);
    }
}
