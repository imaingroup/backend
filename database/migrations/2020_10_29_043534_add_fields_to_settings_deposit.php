<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds fields to table settings_deposit.
 */
class AddFieldsToSettingsDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->dropColumn('is_enabled_exchange_abc_usdt');
            $table->dropColumn('text_exchange_abc_usdt');
            $table->float('withdraw_commission', 16, 8);
            $table->boolean('disabled_exchange_abc_usdt')->default(false);
            $table->string('reason_disabled_exchange_abc_usdt', 255)->nullable(true);
            $table->text('withdraw_description')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->boolean('is_enabled_exchange_abc_usdt')->default(false);
            $table->string('text_exchange_abc_usdt', 255)->nullable(true);

            $table->dropColumn('withdraw_commission');
            $table->dropColumn('disabled_exchange_abc_usdt');
            $table->dropColumn('reason_disabled_exchange_abc_usdt');
            $table->dropColumn('withdraw_description');
        });
    }
}
