<?php
declare(strict_types=1);

namespace App\Modules\Notifications\Mails\External;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Contact main mail class.
 */
final class ContactMainEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new mail instance.
     */
    public function __construct(
        private string $name,
        private string $subjectMail,
        private string $email,
        private string $message
    )
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->subject('Contact request')
            ->markdown('emails.external.contact_main', [
                'name' => $this->name,
                'subject' => $this->subjectMail,
                'email' => $this->email,
                'message' => $this->message,
            ]);
    }
}
