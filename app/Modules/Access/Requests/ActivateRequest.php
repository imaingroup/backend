<?php

namespace App\Modules\Access\Requests;

use App\Modules\Users\Repositories\UserRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This request class need for validating data activate account.
 */
final class ActivateRequest extends FormRequest
{
    /** @var UserRepository User repository. */
    private UserRepository $userRepository;

    /**
     * Constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'token' => 'required|scalar|string|exists:users,email_token'
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    public function after(Validator $validator)
    {
        if(is_string($this['token'])) {
            $user = $this->userRepository->findByEmailToken($this['token']);

            if ($user && $user->email_verified_at) {
                $validator->errors()->add('already_activated', 'The account was already activated');
                return;
            }
        }
    }

    /**
     * Get validated data.
     *
     * @return array
     */
    public function validated(): array
    {
        $data = parent::validated();
        $data['user'] = $this->userRepository->findByEmailToken($data['token']);

        return $data;
    }
}
