<?php
declare(strict_types=1);

namespace App\Modules\Finance\Commands\V2\CryptoData;

use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Finance\Exceptions\Services\BitQuery\RequestException;
use App\Modules\Finance\Services\V2\CryptoProcessingService;
use Illuminate\Console\Command;

/**
 * Processing all command.
 */
final class ProcessingAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finance:data_processing_all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finance data processing: search transactions and update confirmations for all wallets.';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws TransactionNotStarted
     * @throws RequestException
     */
    public function handle(): int
    {
        $this->getCryptoProcessingService()
            ->updateTransactionsEthereumType(true);

        return 0;
    }

    /**
     * Get {@see CryptoProcessingService::class}.
     *
     * @return CryptoProcessingService
     */
    private function getCryptoProcessingService(): CryptoProcessingService
    {
        return app(CryptoProcessingService::class);
    }
}
