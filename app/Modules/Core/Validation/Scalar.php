<?php


namespace App\Modules\Core\Validation;


/**
 * This class validating type is scalar value.
 */
final class Scalar
{
    /**
     * Validate.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator): bool
    {
        return is_scalar($value);
    }
}
