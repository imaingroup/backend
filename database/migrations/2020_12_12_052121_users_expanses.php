<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migration change users table
 * and create users_tids_expanses table
 * for store user's expanses.
 */
final class UsersExpanses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->float('withdrawals_expanse', 16)->after('withdraw_limit')->default(0);
        });

        Schema::create('users_tids_expanses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('trade_options_id');
            $table->unsignedSmallInteger('tid_expanse');
            $table->timestamps();

            $table->unique(['users_id', 'trade_options_id'], 'users_tids_expanses_unique');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->foreign('trade_options_id')
                ->references('id')
                ->on('trade_options')
                ->onDelete('CASCADE');
        });

        DB::table('data_rows')
            ->insert([
                'data_type_id' => 1,
                'field' => 'withdrawals_expanse',
                'type' => 'number',
                'display_name' => 'Withdrawals expanse',
                'required' => 1,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 1,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|numeric|min:0"}}',
                'order' => 63
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('withdrawals_expanse');
        });

        Schema::dropIfExists('users_tids_expanses');

        DB::table('data_rows')
            ->where([
                'data_type_id' => 1,
                'field' => 'withdrawals_expanse'
            ])
            ->delete();
    }
}
