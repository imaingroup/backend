<?php

namespace App\Modules\Settings\Models;

use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Core\Models\Model;
use TCG\Voyager\Traits\Translatable;

final class SettingsLink extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;
    protected $translatable = ['title'];

}
