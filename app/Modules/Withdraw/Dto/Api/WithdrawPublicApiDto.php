<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Finance\Dto\Api\TokenApiDto;
use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Settings\Dto\Api\DepositCurrencyApiDto;
use DateTime;
use Litipk\BigNumbers\Decimal;

/**
 * WithdrawPublicApiDto.
 *
 * @property-read DateTime $createdAt
 * @property-read float $amountUsdt
 * @property-read float $amountCoins
 * @property-read DepositCurrencyApiDto $currency
 * @property-read string|null $txid
 * @property-read int $status
 * @property-read string $statusReadable
 */
final class WithdrawPublicApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param DateTime $createdAt
     * @param float $amountUsdt
     * @param float $amountCoins
     * @param TokenApiDto $currency
     * @param string|null $txid
     * @param int $status
     * @param string $statusReadable
     * @param string $assetName
     * @param string $assetFullName
     */
    public function __construct(
        protected DateTime $createdAt,
        protected float $amountUsdt,
        protected float $amountCoins,
        protected TokenApiDto $currency,
        protected ?string $txid,
        protected int $status,
        protected string $statusReadable,
        protected string $assetName,
        protected string $assetFullName,
    ) {

    }
}
