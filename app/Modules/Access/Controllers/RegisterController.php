<?php

namespace App\Modules\Access\Controllers;

use App\Modules\Admin\Exceptions\TranslateException;
use App\Http\Controllers\Controller;
use App\Modules\Access\Requests\RegisterRequest;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Access\Services\RegisterService;
use App\Modules\Admin\Services\TranslateService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

/**
 * Register controller.
 */
final class RegisterController extends Controller
{
    /** @var \App\Modules\Access\Services\RegisterService Register service. */
    private RegisterService $registerService;

    /** @var Logger Logger service. */
    private Logger $logger;

    /** @var \App\Modules\Admin\Services\TranslateService Translate service. */
    private TranslateService $translateService;

    /**
     * Constructor.
     *
     * @param \App\Modules\Access\Services\RegisterService $registerService
     * @param \App\Modules\Logs\Loggers\Logger $logger
     * @param \App\Modules\Admin\Services\TranslateService $translateService
     */
    public function __construct(RegisterService $registerService, Logger $logger, TranslateService $translateService)
    {
        $this->registerService = $registerService;
        $this->logger = $logger;
        $this->translateService = $translateService;
    }

    /**
     * Register static page.
     *
     * @return Application|Factory|View
     */
    public function page()
    {
        return view('access.register');
    }

    public function pageSuccess()
    {
        return [
            'You registered'
        ];
    }

    /**
     * Get content action.
     * @return array
     * @throws TranslateException
     */
    public function content(): array
    {
        return [
            'data' =>
                [
                    'translates' => $this->translateService->getTranslates(
                        [
                            'base',
                            'page_access'
                        ]
                    )
                ]
        ];
    }

    /**
     * Register send action.
     *
     * @param RegisterRequest $request
     * @return Response
     */
    public function send(RegisterRequest $request): Response
    {
        try {
            DB::beginTransaction();
            $this->registerService->register($request->validated());

            DB::commit();

            return response(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();

            $this->logger->rollbackTransaction($e, $request->validated());

            return response(['status' => false], 503);
        }
    }
}
