<?php
declare(strict_types=1);

namespace App\Modules\Finance\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Finance\Dto\Api\TokenApiDto;
use App\Modules\Finance\Enum\NetworkTypeEnum;
use App\Modules\Finance\Exceptions\Services\V2\CryptoData\UnsupportedNetworkException;
use App\Modules\Settings\Models\DepositCurrency;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

/**
 * Crypto currency model.
 *
 * @property-read int $id
 * @property-read int $crypto_networks_id
 * @property string $name
 * @property string $symbol
 * @property integer $decimals
 * @property string $address
 * @property string $tokenType
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read CryptoCurrencyStats|null $stats
 * @property-read CryptoCurrencyMarket|null $market
 * @property-read DepositCurrency|null $depositCurrency
 * @property-read CryptoNetwork $network
 * @property-read int $monitoringInMinutesDefault
 * @property-read bool $isMainCurrencyInNetwork
 */
final class CryptoCurrency extends Model
{
    /** {@inheritdoc} */
    protected $table = 'crypto_currencies';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'crypto_networks_id' => 'integer',
        'name' => 'string',
        'symbol' => 'string',
        'decimals' => 'integer',
        'address' => 'string',
        'tokenType' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Relation with {@see CryptoCurrencyStats::class}.
     *
     * @return HasOne
     */
    public function stats(): HasOne
    {
        return $this->hasOne(CryptoCurrencyStats::class, 'crypto_currencies_id');
    }

    /**
     * Relation with {@see CryptoCurrencyMarket::class}.
     *
     * @return HasOne
     */
    public function market(): HasOne
    {
        return $this->hasOne(CryptoCurrencyMarket::class, 'crypto_currencies_id');
    }

    /**
     * Relation with {@see DepositCurrency::class}.
     *
     * @return HasOne
     */
    public function depositCurrency(): HasOne
    {
        return $this->hasOne(DepositCurrency::class, 'crypto_currencies_id');
    }

    /**
     * Relation with {@see CryptoNetwork::class}.
     *
     * @return BelongsTo
     */
    public function network(): BelongsTo
    {
        return $this->belongsTo(CryptoNetwork::class, 'crypto_networks_id');
    }

    /**
     * Get attribute "monitoringInMinutesDefault".
     *
     * @return int
     * @throws UnsupportedNetworkException
     */
    public function getMonitoringInMinutesDefaultAttribute(): int
    {
        if($this->network->toEnum()->type() === NetworkTypeEnum::BITCOIN) {
            return (int)config('finance.btc.monitoring_minutes');
        }
        else if($this->network->toEnum()->type() === NetworkTypeEnum::ETHEREUM) {
            return (int)config(sprintf('finance.eth.%s.monitoring_minutes', config('finance.eth.default')));
        }
        else {
            throw new UnsupportedNetworkException();
        }
    }

    /**
     * Get attribute "isMainCurrencyInNetwork".
     *
     * @return bool
     */
    public function getIsMainCurrencyInNetworkAttribute(): bool
    {
        return $this->tokenType === null && $this->address === null;
    }

    /**
     * To api dto.
     *
     * @param bool $withRelations
     * @return TokenApiDto
     */
    public function toApiDto(bool $withRelations = true): TokenApiDto
    {
        return new TokenApiDto(
            $this->decimals,
            $this->name,
            $this->symbol,
            $this->address,
            $this->tokenType,
            $this->id,
            $this->network->toEnum(),
            $withRelations ? $this->stats?->toApiDto() : null,
            $this->market?->toApiDto(),
        );
    }
}
