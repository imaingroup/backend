<?php
declare(strict_types=1);

namespace App\Modules\Core\Services;

use App\Facades\DB;
use App\Modules\Users\Models\User;
use App\Modules\Core\Exceptions\LockNotExistsException;
use App\Modules\Core\Exceptions\TransactionAlreadyStartedException;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Core\Models\Lock;
use App\Modules\Core\Repositories\LockRepository;
use Illuminate\Database\QueryException;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * Lock service.
 */
final class LockService
{
    /**
     * Try create lock row.
     *
     * @param User $user
     * @param int $action
     * @throws TransactionAlreadyStartedException
     */
    public function tryCreateLock(
        User $user,
        #[ExpectedValues(Lock::ACTIONS)] int $action
    ): void
    {
        if (DB::transactionLevel() !== 0) {
            throw new TransactionAlreadyStartedException();
        }

        try {
            $this->getLockRepository()->create([
                'users_id' => $user->id,
                'action' => $action,
            ]);
        } catch (QueryException) {}
    }

    /**
     * Lock.
     *
     * @param User $user
     * @param int $action
     * @throws TransactionNotStarted
     * @throws LockNotExistsException
     */
    public function lock(
        User $user,
        #[ExpectedValues(Lock::ACTIONS)] int $action
    )
    {
        if (DB::transactionLevel() === 0) {
            throw new TransactionNotStarted();
        }

        if(!$this->getLockRepository()
            ->selectLock($user->id, $action)) {
            throw new LockNotExistsException();
        }
    }

    /**
     * Get LockRepository.
     *
     * @return LockRepository
     */
    private function getLockRepository(): LockRepository
    {
        return app(LockRepository::class);
    }
}
