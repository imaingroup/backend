<?php
declare(strict_types=1);

namespace App\Modules\Users\Services;

use App\Modules\Logs\Models\OperationLog;
use App\Modules\Logs\Services\OperationLogService;
use App\Modules\PromoCodes\Services\PromoCodeService;
use App\Modules\Users\Models\User;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Messages\Services\MessageService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;
use PragmaRX\Google2FALaravel\Google2FA;

/**
 * User service.
 */
final class UserService
{
    /**
     * Constructor.
     *
     * @param PromoCodeService $promoCodeService
     * @param MessageService $messageService
     * @param OperationLogService $operationLogService
     */
    public function __construct(
        private PromoCodeService $promoCodeService,
        private MessageService $messageService,
        private OperationLogService $operationLogService
    )
    {
    }

    public function update2fa(User $user, string $secret)
    {
        $user->refresh();

        $user->is_2fa = true;
        $user->google2fa_secret = $secret;

        $user->save();
    }

    /**
     * Check 2fa.
     *
     * @param User $user
     * @param string $code
     * @param string|null $secret
     * @return mixed
     * @throws SecretKeyTooShortException
     */
    public function check2fa(User $user, string $code, string $secret = null)
    {
        $string2fa = trim(str_replace(' ', '', $code));

        /** @var Google2FA $google2fa */
        $google2fa = app('pragmarx.google2fa');

        return $google2fa->verifyGoogle2FA($secret ? $secret : $user->google2fa_secret, $string2fa);
    }

    /**
     * Change user password.
     *
     * @param User $user
     * @param string $password
     */
    public function changePassword(User $user, string $password)
    {
        $user->refresh();

        $user->password = bcrypt($password);

        $user->save();
    }

    /**
     * Change promo code.
     *
     * @param User $user
     */
    public function changePromoCode(User $user)
    {
        $user->refresh();

        $user->promo_code = $this->promoCodeService->generatePromoCode();

        $user->save();
    }

    /**
     * Get balances by user.
     *
     * @param User $user
     * @return array
     *
     * @todo move this to user.
     */
    public function getBalances(User $user): array
    {
        return [
            'balance_usdt' => $user->balance_usdt,
            'deposited_usdt' => $user->deposited_usdt,
            'dividents_usdt' => $user->dividents_usdt,
            'unrealized_pnl' => $user->unrealized_pnl + $user->floating_pnl,
            'balance_total_abc' => $user->balance_total_abc,
            'balance_abc' => $user->balance_abc,
            'traded_abc_balance' => $user->traded_abc_balance,
            'frozen_abc_balance' => $user->frozen_abc_balance,
            'grand_total_profit' => $user->bot_total_profit
        ];
    }

    /**
     * Move teammates.
     *
     * @param User $teamLead
     * @param User $newTeamLead
     * @param Collection $teammates
     * @throws TransactionNotStarted
     */
    public function moveTeammates(User $teamLead, User $newTeamLead, Collection $teammates): void
    {
        User::refreshAndLockForUpdate($teamLead);
        User::refreshAndLockForUpdate($newTeamLead);

        $this->messageService->migrate($teamLead, $newTeamLead, $teammates);

        foreach ($teammates as $teammate) {
            $teammate->team_member_id = $newTeamLead->id;

            $teammate->save();
        }

        //finance data
        $sumTeamLeadEarnedAbc = $teammates->sum('teamlead_earned_abc');
        $sumTeamLeadEarnedUsdt = $teammates->sum('teamlead_earned_usdt');
        $teamLead->team_quantity -= $teammates->count();
        $teamLead->earned_commission_abc -= $sumTeamLeadEarnedAbc;
        $teamLead->earned_commission_usdt -= $sumTeamLeadEarnedUsdt;
        $newTeamLead->team_quantity += $teammates->count();
        $newTeamLead->earned_commission_abc += $sumTeamLeadEarnedAbc;
        $newTeamLead->earned_commission_usdt += $sumTeamLeadEarnedUsdt;

        $teamLead->save();
        $newTeamLead->save();

        //write logs.
        $initiator = Auth::user() ?? $teamLead;

        $this->operationLogService->add(
            OperationLog::TYPE_ADMIN_MOVED_TEAMMATES,
            $initiator,
            null,
            [
                'admin_user_id' => $initiator->id,
                'admin_username' => $initiator->username,
                'old_team_lead' => $teamLead->id,
                'new_team_lead' => $newTeamLead->id,
                'count_teammates' => $teammates->count()
            ]
        );
    }
}
