<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Services;

use App\Modules\Actions\Models\UserAction;
use App\Modules\Actions\Services\UserActionService;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Money\Exceptions\MoneyTransferException;
use App\Modules\Money\Models\Transaction;
use App\Modules\Money\Services\MoneyTransferService;
use App\Modules\Notifications\Jobs\WithdrawDeclinedEmailJob;
use App\Modules\Notifications\Jobs\WithdrawProcessedEmailJob;
use App\Modules\Settings\Models\DepositCurrency;
use App\Modules\Settings\Repositories\SettingsDepositRepository;
use App\Modules\Settings\Repositories\SettingsPriceRepository;
use App\Modules\Settings\Services\CurrenciesService;
use App\Modules\Users\Models\User;
use App\Modules\Withdraw\Dto\Api\DataApiDto;
use App\Modules\Withdraw\Exceptions\WithdrawCreateException;
use App\Modules\Withdraw\Exceptions\WithdrawServiceException;
use App\Modules\Withdraw\Models\Withdraw;
use App\Modules\Withdraw\Repositories\WithdrawRepository;
use DateTime;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Withdraw service.
 */
final class WithdrawService
{
    /**
     * Get data.
     *
     * @param User $user
     * @return DataApiDto
     * @throws Exception
     */
    public function data(User $user): DataApiDto
    {
        $balances = $user->balances();
        $settingsDeposit = $this->getSettingsDepositRepository()->get();
        $settingsPrices = $this->getSettingsPriceRepository()->get();

        return new DataApiDto(
            $this->getCurrenciesService()
                ->getCurrencies()
                ->map(fn(DepositCurrency $currency) => $currency->toApiDto()),
            (float)$balances['balance_usdt'],
            (float)$balances['balance_abc'],
            (bool)$user->is_2fa,
            $user->is_2fa && !$settingsDeposit->disabled_exchange_abc_usdt,
            $settingsDeposit->reason_disabled_exchange_abc_usdt,
            $this->checkEnabledWithdraw($user),
            $settingsDeposit->withdraw_description,
            $settingsPrices->abc_price,
            $this->calculateWithdrawLimit($user),
            (float)Withdraw::MINIMUM_AMOUNT_USDT,
            $user->btc,
            $user->eth,
        );
    }

    /**
     * Calculate withdraw limit.
     *
     * @param User $user
     * @return float
     * @throws Exception
     */
    public function calculateWithdrawLimit(User $user): float
    {
        $settingsDeposit = $this->getSettingsDepositRepository()->get();
        $limit = $user->is_withdraw_default ? $settingsDeposit->withdraw_limit : $user->withdraw_limit;
        $limit += $user->withdrawals_expanse;
        $limit -= $this->getWithdrawRepository()->withdrawnAmountThisMonth($user);

        return $limit;
    }

    /**
     * History.
     *
     * @param User $user
     * @return Collection
     */
    public function history(User $user): Collection
    {
        return $this->getWithdrawRepository()
            ->getByUser($user)
            ->map(fn(Withdraw $withdraw) => $withdraw->toPublicApiDto());
    }

    /**
     * Create withdraw request.
     *
     * @param User $user
     * @param DepositCurrency $currency
     * @param string $address
     * @param float $amount
     * @return Withdraw
     * @throws MoneyTransferException
     * @throws TransactionNotStarted
     * @throws WithdrawCreateException
     */
    public function create(
        User            $user,
        DepositCurrency $currency,
        string          $address,
        float           $amount
    ): Withdraw
    {
        $settings = $this->getSettingsDepositRepository()->get();

        User::refreshAndLockForUpdate($user);

        if ($amount <= 0) {
            throw new WithdrawCreateException('Amount must be great than zero');
        }

        if ($settings->disabled_dividends_withdraw && $user->deposited_usdt < $amount) {
            throw new WithdrawCreateException('You doesn\'t have enough money or exceed withdraw limit');
        } else if (!$settings->disabled_dividends_withdraw && $user->balance_usdt < $amount) {
            throw new WithdrawCreateException('You doesn\'t have enough money or exceed withdraw limit');
        } else if ($amount > $this->calculateWithdrawLimit($user)) {
            throw new WithdrawCreateException('You doesn\'t have enough money or exceed withdraw limit');
        }

        $frozenUsdtDeposited = 0;
        $frozenUsdtDividends = 0;

        if ($user->deposited_usdt > 0) {
            if ($user->deposited_usdt < $amount) {
                $frozenUsdtDeposited = $user->deposited_usdt;
                $frozenUsdtDividends = $amount - $frozenUsdtDeposited;
            } else {
                $frozenUsdtDeposited = $amount;
            }
        } else {
            $frozenUsdtDividends = $amount;
        }

        $transaction = $frozenUsdtDeposited > 0 ?
            $this->getMoneyTransferService()->moneyOperation(
                $user,
                Auth::check() ? Auth::user() : $user,
                $frozenUsdtDeposited / -1,
                Transaction::CURRENCY_USDT_DEPOSITED,
                Transaction::TYPE_WITHDRAW
            ) : null;

        if ($frozenUsdtDividends > 0) {
            $transaction = $this->getMoneyTransferService()->moneyOperation(
                $user,
                Auth::check() ? Auth::user() : $user,
                $frozenUsdtDividends / -1,
                Transaction::CURRENCY_USDT_DIVIDENTS,
                Transaction::TYPE_WITHDRAW,
                $transaction
            );
        }

        $rate = $currency->currency->market->price;
        $amountInCoins = $amount / $rate;

        $withdraw = new Withdraw();
        $withdraw->user()
            ->associate($user);
        $withdraw->currency()
            ->associate($currency->currency);
        $withdraw->address = $address;
        $withdraw->amount_usdt = $amount;
        $withdraw->amount_coin = $amountInCoins;
        $withdraw->rate = $rate;
        $withdraw->status = Withdraw::STATUS_PENDING;
        $withdraw->frozen_usdt_deposited = $frozenUsdtDeposited;
        $withdraw->frozen_usdt_dividends = $frozenUsdtDividends;
        $withdraw->transaction()
            ->associate($transaction);
        $withdraw->save();

        return $withdraw;
    }

    /**
     * Process request.
     * This method required rule no active transaction.
     *
     * @param \App\Modules\Withdraw\Models\Withdraw $withdraw
     * @throws \App\Modules\Withdraw\Exceptions\WithdrawServiceException
     * @throws Throwable
     */
    public function processRequest(Withdraw $withdraw): void
    {
        if (DB::transactionLevel() !== 0) {
            throw new WithdrawServiceException('Transaction already started. This method required rule no active transaction.');
        }

        try {
            DB::beginTransaction();
            Withdraw::refreshAndLockForUpdate($withdraw);

            $user = $withdraw->user;

            if ($withdraw->status !== Withdraw::STATUS_CONFIRMED) {
                throw new WithdrawServiceException('This request doesn\'t have status confirmed.');
            }

            if ($withdraw->processed) {
                throw new WithdrawServiceException('This request already processed.');
            }

            $withdraw->frozen_usdt_deposited = 0;
            $withdraw->frozen_usdt_dividends = 0;
            $withdraw->processed = true;
            $withdraw->processed_at = new DateTime();

            $withdraw->save();

            if ($user->is_enabled_withdrawal_notifications) {
                WithdrawProcessedEmailJob::dispatch($withdraw);
            }

            $this->getUserActionService()->register(
                $user,
                UserAction::ACTION_WITHDRAW,
                [
                    'withdraw' => $withdraw->id
                ]
            );

            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();

            $this->getLogger()->rollbackTransaction($e, $withdraw);

            throw $e;
        }
    }

    /**
     * Decline withdraw request.
     *
     * @param Withdraw $withdraw
     * @throws MoneyTransferException
     * @throws WithdrawServiceException
     * @throws TransactionNotStarted
     */
    public function declineRequest(Withdraw $withdraw): void
    {
        Withdraw::refreshAndLockForUpdate($withdraw);

        if ($withdraw->status !== Withdraw::STATUS_PENDING) {
            throw new WithdrawServiceException('This request doesn\'t have status pending');
        }

        if ($withdraw->declined_at) {
            throw new WithdrawServiceException('This request doesn\'t already declined');
        }

        $user = $withdraw->user;
        $relatedTransaction = $withdraw->transaction;

        if ($withdraw->frozen_usdt_deposited > 0) {
            $this->getMoneyTransferService()->moneyOperation(
                $user,
                Auth::check() ? Auth::user() : $user,
                $withdraw->frozen_usdt_deposited,
                Transaction::CURRENCY_USDT_DEPOSITED,
                Transaction::TYPE_WITHDRAW_CANCELLATION,
                $relatedTransaction
            );
        }

        if ($withdraw->frozen_usdt_dividends > 0) {
            $this->getMoneyTransferService()->moneyOperation(
                $user,
                Auth::check() ? Auth::user() : $user,
                $withdraw->frozen_usdt_dividends,
                Transaction::CURRENCY_USDT_DIVIDENTS,
                Transaction::TYPE_WITHDRAW_CANCELLATION,
                $relatedTransaction
            );
        }

        $withdraw->frozen_usdt_deposited = 0;
        $withdraw->frozen_usdt_dividends = 0;
        $withdraw->status = Withdraw::STATUS_DECLINED;
        $withdraw->declined_at = new DateTime();

        $withdraw->save();

        if ($user->is_enabled_withdrawal_notifications) {
            WithdrawDeclinedEmailJob::dispatch($withdraw);
        }
    }

    /**
     * Check enabled withdraw for user.
     *
     * @param \App\Modules\Users\Models\User $user
     * @return bool
     */
    public function checkEnabledWithdraw(User $user): bool
    {
        $settingsDeposit = $this->getSettingsDepositRepository()->get();
        $enabledWithdraw = (bool)$user->is_2fa;

        if ($enabledWithdraw) {
            if ((!$user->enabled_force_withdraw && $settingsDeposit->is_withdraw_block) || $user->is_withdraw_block) {
                $enabledWithdraw = false;
            }
        }

        return $enabledWithdraw;
    }

    /**
     * Get Logger.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }

    /**
     * Get UserActionService.
     *
     * @return UserActionService
     */
    private function getUserActionService(): UserActionService
    {
        return app(UserActionService::class);
    }

    /**
     * Get MoneyTransferService.
     *
     * @return \App\Modules\Money\Services\MoneyTransferService
     */
    private function getMoneyTransferService(): MoneyTransferService
    {
        return app(\App\Modules\Money\Services\MoneyTransferService::class);
    }

    /**
     * Get WithdrawRepository.
     *
     * @return WithdrawRepository
     */
    private function getWithdrawRepository(): WithdrawRepository
    {
        return app(WithdrawRepository::class);
    }

    /**
     * Get SettingsPriceRepository.
     *
     * @return \App\Modules\Settings\Repositories\SettingsPriceRepository
     */
    private function getSettingsPriceRepository(): SettingsPriceRepository
    {
        return app(SettingsPriceRepository::class);
    }

    /**
     * Get SettingsDepositRepository.
     *
     * @return SettingsDepositRepository
     */
    private function getSettingsDepositRepository(): SettingsDepositRepository
    {
        return app(SettingsDepositRepository::class);
    }

    /**
     * Get {@see CurrenciesService::class}.
     *
     * @return CurrenciesService
     */
    private function getCurrenciesService(): CurrenciesService
    {
        return app(CurrenciesService::class);
    }
}
