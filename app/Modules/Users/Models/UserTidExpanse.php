<?php

namespace App\Modules\Users\Models;

use App\Modules\Core\Models\Model;

/**
 * This model storing expanse data for tid and user.
 */
final class UserTidExpanse extends Model
{
    protected $table = 'users_tids_expanses';
}
