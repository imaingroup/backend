<?php
declare(strict_types=1);

namespace App\Modules\ProfitCards\Enums\Models\ProfitCard;

/**
 * Type enum.
 */
enum Type: string
{
    case TRADING = 'trading';
    case EXPANDED = 'expanded';
}
