<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Btc;

/**
 * GeTransactionException.
 */
final class GeTransactionException extends BtcException
{
}
