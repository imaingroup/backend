<?php


namespace App\Modules\Access\Exceptions;

/**
 * This exception need for use in login service.
 */
final class LoginException extends \Exception
{

}
