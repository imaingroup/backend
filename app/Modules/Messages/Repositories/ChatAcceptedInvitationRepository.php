<?php
declare(strict_types=1);


namespace App\Modules\Messages\Repositories;

use App\Modules\Messages\Models\ChatAcceptedInvitation;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Chat accepted invitation repository.
 */
final class ChatAcceptedInvitationRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param ChatAcceptedInvitation $model
     */
    public function __construct(protected ChatAcceptedInvitation $model)
    {}
}
