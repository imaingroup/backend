<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * CreateUsersIdentificationsUsersRelations.
 */
final class CreateUsersIdentificationsUsersRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('users_identifications_users_relations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_main_id');
            $table->foreign('users_main_id', 'uiur_users_main_id_foreign')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();
            $table->unsignedBigInteger('users_id');
            $table->foreign('users_id', 'uiur_relations_users_id_foreign')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();
            $table->boolean('is_accurate');
            $table->timestamps();
            $table->unique(['users_main_id', 'users_id'], 'users_identifications_users_relations_relations_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::drop('users_identifications_users_relations');
    }
}
