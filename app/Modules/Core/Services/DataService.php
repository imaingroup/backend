<?php
declare(strict_types=1);

namespace App\Modules\Core\Services;

use App\Modules\Account\Services\AccountService;
use App\Modules\Actions\Services\UserActionService;
use App\Modules\Admin\Exceptions\TranslateException;
use App\Modules\Charts\Services\InvestmentsChartService;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Core\Exceptions\DataException;
use App\Modules\Dashboard\Services\DashboardService;
use App\Modules\Deposites\Services\DepositService;
use App\Modules\Investments\Models\Trade;
use App\Modules\Investments\Services\InvestmentService;
use App\Modules\Messages\Services\MessageService;
use App\Modules\News\Services\NewsService;
use App\Modules\Teams\Services\TeamService;
use App\Modules\Users\Models\User;
use App\Modules\Users\Services\UserService;
use App\Modules\Withdraw\Services\WithdrawService;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use function crc32;
use function join;
use function sprintf;

/**
 * Data service.
 * This service is proxy for access to data methods in other services.
 * It need for fast auto detect all changes.
 */
final class DataService
{
    const TYPE_ACCOUNT_GLOBAL = 'account_global';
    const TYPE_DASHBOARD_CONTENT = 'dashboard_content';
    const TYPE_NEWS_LIST = 'news_list';
    const TYPE_NEWS_CATEGORIES = 'news_categories';
    const TYPE_INVESTMENTS_OPTIONS = 'investments_options';
    const TYPE_INVESTMENTS_HISTORY = 'investments_history';
    const TYPE_INVESTMENTS_CHART = 'investments_chart';
    /** @deprecated */
    const TYPE_INVESTMENTS_CHART_BY_TRADE = 'investments_chart_by_trade';
    const TYPE_TRANSACTIONS_LIST = 'transactions_list';
    const TYPE_TEAM_GET = 'team_get';
    const TYPE_INVESTMENTS_BALANCES = 'investments_balances';
    const TYPE_MESSAGES_CONTACTS = 'messages_contacts';
    const TYPE_MESSAGES_CHATS = 'messages_chats';

    const TYPES_DIGITS = [
        self::TYPE_ACCOUNT_GLOBAL => 1,
        self::TYPE_DASHBOARD_CONTENT => 2,
        self::TYPE_NEWS_LIST => 4,
        self::TYPE_NEWS_CATEGORIES => 8,
        self::TYPE_INVESTMENTS_OPTIONS => 16,
        self::TYPE_INVESTMENTS_HISTORY => 32,
        self::TYPE_INVESTMENTS_CHART => 64,
        self::TYPE_INVESTMENTS_CHART_BY_TRADE => 128,
        self::TYPE_TRANSACTIONS_LIST => 256,
        self::TYPE_TEAM_GET => 4096,
        self::TYPE_INVESTMENTS_BALANCES => 8192,
        self::TYPE_MESSAGES_CONTACTS => 16384,
        self::TYPE_MESSAGES_CHATS => 32768
    ];

    private const METHODS = [
        self::TYPE_ACCOUNT_GLOBAL => 'getAccountGlobal',
        self::TYPE_DASHBOARD_CONTENT => 'getDashboardContent',
        self::TYPE_NEWS_LIST => 'getNewsList',
        self::TYPE_NEWS_CATEGORIES => 'getNewsCategories',
        self::TYPE_INVESTMENTS_OPTIONS => 'getInvestmentsOptions',
        self::TYPE_INVESTMENTS_HISTORY => 'getInvestmentsHistory',
        self::TYPE_INVESTMENTS_CHART => 'getInvestmentsChart',
        self::TYPE_INVESTMENTS_CHART_BY_TRADE => 'getInvestmentsChartByTrade',
        self::TYPE_TRANSACTIONS_LIST => 'getTransactionsList',
        self::TYPE_TEAM_GET => 'getTeamGet',
        self::TYPE_INVESTMENTS_BALANCES => 'getInvestmentsBalances',
        self::TYPE_MESSAGES_CONTACTS => 'getMessagesContacts',
        self::TYPE_MESSAGES_CHATS => 'getMessagesChats'
    ];

    const SESSION_PREFIX = 'data_auto_update_';

    /**
     * Constructor.
     *
     * @param AccountService $accountService
     * @param DashboardService $dashboardService
     * @param NewsService $newsService
     * @param InvestmentService $investmentService
     * @param InvestmentsChartService $chartService
     * @param UserService $userService
     * @param UserActionService $userActionService
     * @param TeamService $teamService
     * @param MessageService $messageService
     */
    public function __construct(
        private AccountService $accountService,
        private DashboardService $dashboardService,
        private NewsService $newsService,
        private InvestmentService $investmentService,
        private InvestmentsChartService $chartService,
        private UserService $userService,
        private UserActionService $userActionService,
        private TeamService $teamService,
        private MessageService $messageService
    )
    {
    }

    /**
     * Register event. It need for detecting event of update data.
     *
     * @param string $event
     * @param array $data
     * @param array $parameters
     * @throws DataException
     */
    public function register(string $event, array $data, array $parameters = []): void
    {
        Cache::put(
            $this->getCacheKey($event), [
                'data' => $data,
                'parameters' => $this->preSerialize($parameters)
            ]
        );
    }

    /**
     * Get old data.
     *
     * @param string $event
     * @return array|null
     */
    public function getOldData(string $event): ?array
    {
        $old = Cache::get($this->getCacheKey($event));

        if (is_array($old) && isset($old['parameters']) && is_array($old['parameters'])) {
            $old['parameters'] = $this->preDeserialize($old['parameters']);
        }

        return $old;
    }

    /**
     * Get data.
     *
     * @param string $type
     * @param array $parameters
     * @param bool $withoutRegister
     * @return array|ResponseDto
     * @throws DataException
     */
    public function getData(string $type, array $parameters = [], bool $withoutRegister = false): array|ResponseDto
    {
        if (!in_array($type, array_keys(self::METHODS))) {
            throw new DataException(sprintf('Unknown type %s', $type));
        }

        $data = call_user_func_array(array($this, self::METHODS[$type]), $parameters);

        if (!$withoutRegister) {
            $this->register($type, $data instanceof ResponseDto ? (array)$data->data : $data, $parameters);
        }

        return $data;
    }

    /**
     * Get updated data.
     *
     * @param array $rules
     * @param int $version
     * @return array
     * @throws DataException
     */
    public function getUpdatedData(array $rules, int $version): array
    {
        if (!Cache::has($this->updateCounterName($rules))) {
            Cache::increment($this->updateCounterName($rules));
        }

        $newData = [];
        $hasUpdates = false;

        foreach ($rules as $rule) {
            $old = $this->getOldData($rule);

            if ($old) {
                $oldData = (array)$old['data'];
                $parameters = $old['parameters'];

                foreach ($parameters as $parameter) {
                    if ($parameter instanceof User) {
                        $parameter->refresh();
                    }
                }

                $data = (array)$this->getData($rule, $parameters, true);

                if (array_key_exists('version', $data)) {
                    unset($data['version']);
                }

                if (!$this->compareData($rule, $oldData, $data)) {
                    $this->register($rule, $data, $parameters);

                    $newData[$rule] = $data;
                    $hasUpdates = true;
                } else if ($version < (int)Cache::get($this->updateCounterName($rules))) {
                    $newData[$rule] = $data;
                }
            }
        }

        if ($hasUpdates) {
            Cache::increment($this->updateCounterName($rules));
        }

        $newData['version'] = (int)Cache::get($this->updateCounterName($rules));

        return $newData;
    }

    /**
     * Compare data by rule.
     *
     * @param string $rule
     * @param array $oldData
     * @param array $newData
     * @return bool
     */
    private function compareData(string $rule, array $oldData, array $newData): bool
    {
        if ($rule === self::TYPE_DASHBOARD_CONTENT) {
            $oldData['data']['current_time_utc'] = null;
            $newData['data']['current_time_utc'] = null;
        }

        if (array_key_exists('version', $oldData)) {
            unset($oldData['version']);
        }

        if (array_key_exists('version', $newData)) {
            unset($newData['version']);
        }

        return crc32(json_encode($oldData)) === crc32(json_encode($newData));
    }

    /**
     * Get update counter name.
     *
     * @param array $rules
     * @return string
     */
    private function updateCounterName(array $rules): string
    {
        return sprintf('update_counter_%s_%s', crc32(join('', $rules)), Auth::user()?->id);
    }

    /**
     * Get account global data.
     *
     * @param User $user
     * @return array
     * @throws TranslateException
     */
    private function getAccountGlobal(User $user): array
    {
        return ['data' => $this->accountService->global($user)];
    }

    /**
     * Get dashboard content.
     *
     * @param User $user
     * @return array
     */
    private function getDashboardContent(User $user): array
    {
        return ['data' => $this->dashboardService->content($user)];
    }

    /**
     * Get news list.
     *
     * @param string $sort
     * @param int|null $category
     * @param string|null $search
     * @param int $page
     * @return array
     */
    private function getNewsList(string $sort = 'ASC', ?int $category = null, ?string $search = null, int $page = 1): array
    {
        return [
            'data' => [
                'news' => $this->newsService->getNews($sort, $category, $search, $page)
            ]
        ];
    }

    /**
     * Get new categories.
     *
     * @return array
     */
    private function getNewsCategories(): array
    {
        return [
            'data' => [
                'categories' => $this->newsService->getCategories()
            ]
        ];
    }

    /**
     * Get investments balances.
     *
     * @param User $user
     * @return array
     */
    private function getInvestmentsBalances(User $user): array
    {
        return [
            'data' => [
                'balances' => $this->userService->getBalances($user)
            ]
        ];
    }

    /**
     * Get investments options.
     *
     * @param User $user
     * @return array
     */
    private function getInvestmentsOptions(User $user): array
    {
        return [
            'data' => [
                'options' => $this->investmentService->options($user)
            ]
        ];
    }

    /**
     * Get investments history.
     *
     * @param User $user
     * @return array
     * @throws Exception
     */
    private function getInvestmentsHistory(User $user): array
    {
        return [
            'data' => [
                'history' => $this->investmentService->history($user)
            ]
        ];
    }

    /**
     * Get investments chart.
     *
     * @param User $user
     * @param Trade|null $trade
     * @param string|null $period
     * @return array
     */
    private function getInvestmentsChart(User $user, Trade $trade = null, string $period = null): array
    {
        return [
            'data' => [
                'chart' => $this->chartService->chartV2($user, $trade, $period)
            ]
        ];
    }

    /**
     * Get transactions list.
     *
     * @param User $user
     * @return array
     */
    private function getTransactionsList(User $user): array
    {
        return [
            'data' => [
                'transactions' => $this->userActionService->list($user)
            ]
        ];
    }

    /**
     * Get team data.
     *
     * @param User $user
     * @return array
     */
    private function getTeamGet(User $user): array
    {
        return [
            'data' => [
                'team' => $this->teamService->getData($user)
            ]
        ];
    }

    /**
     * Get messages contacts.
     *
     * @param User $user
     * @return array
     */
    private function getMessagesContacts(User $user): array
    {
        return [
            'data' => [
                'contacts' => $this->messageService->getContactList($user)
            ]
        ];
    }

    /**
     * Get messages chats.
     *
     * @param User $user
     * @return array
     * @throws Exception
     */
    private function getMessagesChats(User $user): array
    {
        return [
            'data' => [
                'chats' => $this->messageService->getChatList($user)
            ]
        ];
    }

    /**
     * Get cache key.
     *
     * @param string $event
     * @return string
     */
    private function getCacheKey(string $event): string
    {
        return sprintf('%s%s%s', self::SESSION_PREFIX, $event, Auth::user()?->id);
    }

    /**
     * Pre serialize.
     *
     * @param array $parameters
     * @return array
     * @throws DataException
     */
    private function preSerialize(array $parameters = []): array
    {
        foreach ($parameters as $key => $parameter) {
            if ($parameter instanceof Model) {
                if (!$parameter->exists) {
                    throw new DataException('Unsaved model want be serialize');
                }

                $parameters[$key] = [
                    'type' => '_model',
                    'class' => $parameter::class,
                    'id' => $parameter->id,
                ];
            }
        }

        return $parameters;
    }

    /**
     * Pre deserialize.
     *
     * @param array $parameters
     * @return array
     */
    private function preDeserialize(array $parameters = []): array
    {
        foreach ($parameters as $key => $parameter) {
            if (is_array($parameter) && isset($parameter['type']) && $parameter['type'] === '_model') {
                $parameters[$key] = $parameter['class']::find($parameter['id']);
            }
        }

        return $parameters;
    }
}
