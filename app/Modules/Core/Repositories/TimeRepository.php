<?php
declare(strict_types=1);

namespace App\Modules\Core\Repositories;

use App\Modules\Core\Models\Time;
use Illuminate\Support\Collection;

/**
 * Time repository.
 */
final class TimeRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param Time $model
     */
    public function __construct(protected Time $model)
    {
    }

    /**
     * All items.
     *
     * @return Collection
     */
    public function allItems(): Collection
    {
       return $this->model->newQuery()
            ->get();
    }

    /**
     * Update time errors.
     *
     * @param string $server
     * @param bool $success
     */
    public function updateTimeErrors(string $server, bool $success): void
    {
        $query = $this->model
            ->newQuery()
            ->where('server', $server);

        if($success) {
            $query->update([
                'errors' => 0
            ]);
        }
        else {
            $query->increment('errors');
        }
    }
}
