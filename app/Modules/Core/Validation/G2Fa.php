<?php
declare(strict_types=1);

namespace App\Modules\Core\Validation;

use App\Modules\Logs\Loggers\Logger;
use App\Modules\Users\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;
use Throwable;

/**
 * Validate that 2fa is correct.
 */
final class G2Fa
{
    /**
     * Validate.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        $user = Auth::user();

        if (!is_string($value) || !$user) {
            return false;
        }

        if ($user->is_2fa) {
            try {
                if (!$this->getUserService()->check2fa($user, $value)) {
                    return false;
                }
            } catch (Throwable $e) {
                $this->getLogger()->registerException($e);

                return false;
            }
        }

        return true;
    }

    /**
     * Get UserService.
     *
     * @return UserService
     */
    private function getUserService(): UserService
    {
        return app(UserService::class);
    }

    /**
     * Get Logger.
     *
     * @return \App\Modules\Logs\Loggers\Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
