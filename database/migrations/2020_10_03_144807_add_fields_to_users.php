<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //base fields
            $table->string('username', 255)->unique();
            $table->string('country', 255)->nullable(true);
            $table->date('birthdate')->nullable(true);
            $table->tinyInteger('gender')->nullable(true);
            $table->text('bio')->nullable(true);
            $table->boolean('is_trader')->default(true);
            $table->boolean('is_hide_from_public')->default(true);
            $table->string('btc', 255);
            $table->string('promo_code', 255)->unique();
            $table->unsignedBigInteger('langs_id');

            //notifications
            $table->boolean('is_enabled_deposit_notifications')->default(true);
            $table->boolean('is_enabled_withdrawal_notifications')->default(true);
            $table->boolean('is_enabled_purchase_notifications')->default(true);
            $table->boolean('is_enabled_password_change_notifications')->default(true);

            //2fa
            $table->boolean('is_2fa')->default(false);

            //other
            $table->string('ip', 20);
            $table->string('ip_country', 255);
            $table->boolean('is_accepted_cookies')->default(false);
            $table->dateTime('last_activity_at');

            //balance fields
            $table->float('balance_usdt', 16, 8)->default(0);
            $table->float('dividents_usdt', 16, 8)->default(0);
            $table->float('balance_abc', 16, 8)->default(0);
            $table->float('traded_abc_balance', 16, 8)->default(0);
            $table->float('frozen_abc_balance', 16, 8)->default(0);
            $table->float('bot_total_profit', 16, 8)->default(0);
            $table->float('total_withdraw', 16, 8)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('username');
            $table->dropColumn('country');
            $table->dropColumn('birthdate');
            $table->dropColumn('gender');
            $table->dropColumn('bio');
            $table->dropColumn('is_trader');
            $table->dropColumn('is_hide_from_public');
            $table->dropColumn('btc');
            $table->dropColumn('promo_code');
            $table->dropColumn('langs_id');
            $table->dropColumn('is_enabled_deposit_notifications');
            $table->dropColumn('is_enabled_withdrawal_notifications');
            $table->dropColumn('is_enabled_purchase_notifications');
            $table->dropColumn('is_enabled_password_change_notifications');
            $table->dropColumn('is_2fa');
            $table->dropColumn('ip');
            $table->dropColumn('ip_country');
            $table->dropColumn('is_accepted_cookies');
            $table->dropColumn('last_activity_at');
            $table->dropColumn('balance_usdt');
            $table->dropColumn('dividents_usdt');
            $table->dropColumn('balance_abc');
            $table->dropColumn('traded_abc_balance');
            $table->dropColumn('frozen_abc_balance');
            $table->dropColumn('bot_total_profit');
            $table->dropColumn('total_withdraw');
        });
    }
}
