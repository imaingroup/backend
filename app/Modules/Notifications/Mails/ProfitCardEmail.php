<?php

namespace App\Modules\Notifications\Mails;

use App\Modules\Notifications\Mails\AbstractEmail;
use App\Modules\Notifications\Models\Mail;
use App\Modules\ProfitCards\Models\ProfitCardBuyHistory;
use function protectFromXss;

/**
 * Profit card mail class.
 */
final class ProfitCardEmail extends AbstractEmail
{
    /** @var \App\Modules\ProfitCards\Models\ProfitCardBuyHistory Buy profit card. */
    private ProfitCardBuyHistory $profitCardBuyHistory;

    /**
     * Create a new mail instance.
     *
     * @param ProfitCardBuyHistory $profitCardBuyHistory
     */
    public function __construct(ProfitCardBuyHistory $profitCardBuyHistory)
    {
        $this->profitCardBuyHistory = $profitCardBuyHistory;
    }

    /**
     * {@inheritdoc}
     */
    protected function getVariables(): array
    {
        return [
            '{username}' => protectFromXss($this->profitCardBuyHistory->user->username),
            '{cardname}' => $this->profitCardBuyHistory->card->title,
            '{sitename}' => __('base.site_name')
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getMailId(): int
    {
        return Mail::MAIL_BUY_PROFIT_CARD;
    }
}
