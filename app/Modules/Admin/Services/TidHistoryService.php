<?php


namespace App\Modules\Admin\Services;


use App\Modules\Investments\Exceptions\TidHistoryException;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Investments\Models\TidHistory;
use App\Modules\Investments\Models\TidHistoryTradeOption;
use App\Modules\Investments\Models\TradeOption;
use App\Modules\Admin\Repositories\DataRowRepository;
use App\Modules\Admin\Repositories\DataTypeRepository;
use App\Modules\Investments\Repositories\TidHistoryRepository;
use App\Modules\Investments\Repositories\TidHistoryTradeOptionRepository;
use App\Modules\Investments\Repositories\TidRequestHistoryRepository;
use App\Modules\Investments\Repositories\TradeOptionRepository;
use Throwable;

/**
 * Tid history admin service.
 */
final class TidHistoryService
{
    /** @var \App\Modules\Investments\Repositories\TradeOptionRepository Trade option repository. */
    private TradeOptionRepository $tradeOptionRepository;

    /** @var DataTypeRepository Data type repository. */
    private DataTypeRepository $dataTypeRepository;

    /** @var DataRowRepository Data row repository. */
    private DataRowRepository $dataRowRepository;

    /** @var TidHistoryRepository Tid history repository. */
    private TidHistoryRepository $tidHistoryRepository;

    /** @var TidHistoryTradeOptionRepository Tid history trade option repository. */
    private TidHistoryTradeOptionRepository $tidHistoryTradeOptionRepository;

    /** @var TidRequestHistoryRepository Tid request history repository. */
    private TidRequestHistoryRepository $tidRequestHistoryRepository;

    /** @var Logger Logger. */
    private Logger $logger;

    const MODEL_NAME = 'App\Modules\Investments\Models\TidHistory';

    /**
     * Constructor.
     *
     * @param \App\Modules\Investments\Repositories\TradeOptionRepository $tradeOptionRepository
     * @param DataTypeRepository $dataTypeRepository
     * @param \App\Modules\Admin\Repositories\DataRowRepository $dataRowRepository
     * @param TidHistoryRepository $tidHistoryRepository
     * @param \App\Modules\Investments\Repositories\TidHistoryTradeOptionRepository $tidHistoryTradeOptionRepository
     * @param TidRequestHistoryRepository $tidRequestHistoryRepository
     * @param \App\Modules\Logs\Loggers\Logger $logger
     */
    public function __construct(
        TradeOptionRepository $tradeOptionRepository,
        DataTypeRepository $dataTypeRepository,
        DataRowRepository $dataRowRepository,
        TidHistoryRepository $tidHistoryRepository,
        TidHistoryTradeOptionRepository $tidHistoryTradeOptionRepository,
        TidRequestHistoryRepository $tidRequestHistoryRepository,
        Logger $logger
    )
    {
        $this->tradeOptionRepository = $tradeOptionRepository;
        $this->dataTypeRepository = $dataTypeRepository;
        $this->dataRowRepository = $dataRowRepository;
        $this->tidHistoryRepository = $tidHistoryRepository;
        $this->tidHistoryTradeOptionRepository = $tidHistoryTradeOptionRepository;
        $this->tidRequestHistoryRepository = $tidRequestHistoryRepository;
        $this->logger = $logger;
    }

    /**
     * Check and add options columns if not exists.
     */
    public function addOptionColumns(): void
    {
        $dataType = $this->dataTypeRepository->findByModelName(self::MODEL_NAME);

        $columnList = [];
        $orderList = [];
        $rows = $this->dataRowRepository->getByDataTypeId($dataType->id);

        foreach ($rows as $row) {
            $columnList[] = $row->field;
            $orderList[] = $row->order;
        }

        $optionColumnList = [];
        $options = $this->tradeOptionRepository->all();

        $i = 0;
        foreach ($options as $option) {
            $i++;
            $optionColumn = \sprintf('option_%s', $option->id);
            $optionColumnList[] = $optionColumn;

            if (!in_array($optionColumn, $columnList)) {
                $this->dataRowRepository->create([
                    'data_type_id' => $dataType->id,
                    'field' => $optionColumn,
                    'type' => 'number',
                    'display_name' => $option->title . ' %',
                    'required' => 0,
                    'browse' => 1,
                    'read' => 0,
                    'edit' => 0,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => max($orderList) + $i
                ]);
            }
        }

        foreach ($columnList as $item) {
            if (!in_array($item, $optionColumnList) && preg_match('/option_(\d+)/', $item)) {
                $row = $this->dataRowRepository->findByField($dataType->id, $item);
                $row->delete();
            }
        }
    }

    /**
     * Update percent for history.
     *
     * @param int $tidHistoryId
     * @param int $option
     * @param float $percent
     * @throws \App\Modules\Investments\Exceptions\TidHistoryException
     */
    public function updatePercent(int $tidHistoryId, int $option, float $percent)
    {
        if ($option < 0) {
            throw new TidHistoryException('Option must be great or equal zero');
        }

        $tidHistory = $this->tidHistoryRepository->find($tidHistoryId);

        if ($option === 0) {
            $tidHistory->percent = $percent;
            $tidHistory->save();
        } else {
            $tradeOption = $this->tradeOptionRepository->getByIndex($option - 1);
            $tidHistoryTradeOption = $this->tidHistoryTradeOptionRepository->findBy($tidHistory->id, $tradeOption->id);

            $tidHistoryTradeOption->percent = $percent;
            $tidHistoryTradeOption->save();
        }
    }

    /**
     * Generate tid history.
     *
     * @param int $period
     * @param float $percent
     * @param float $highMax
     * @param float $lowMax
     * @param int $positiveDays
     * @param float $lossSumPercent
     * @throws \App\Modules\Investments\Exceptions\TidHistoryException|Throwable
     */
    public function generate(int $period, float $percent, float $highMax, float $lowMax, int $positiveDays, float $lossSumPercent): void
    {
        $options = $this->tradeOptionRepository->all();

        $optionSequences = [];
        $defaultSequence = $this->generateSequence($period, $percent, $highMax, $lowMax, $positiveDays, $lossSumPercent, 3);

        foreach ($options as $option) {
            $optionSequences[$option->id] = $this->generateSequence($period, $percent, $highMax, $lowMax, $positiveDays, $lossSumPercent, 3);;
        }

        $date = new \DateTime();
        $date->modify('+ 1 DAY');

        foreach ($defaultSequence as $index => $number) {
            $tidHistory = $this->tidHistoryRepository->findByDate($date);

            if (!$tidHistory) {
                $tidHistory = new TidHistory();
                $tidHistory->date = $date;
                $tidHistory->time = '00:01:00';
            }

            $tidHistory->percent = $number;
            $tidHistory->save();

            foreach ($options as $option) {
                $tidHistoryTradeOption = $this->tidHistoryTradeOptionRepository->findBy($tidHistory->id, $option->id);

                if (!$tidHistoryTradeOption) {
                    $tidHistoryTradeOption = new TidHistoryTradeOption();
                    $tidHistoryTradeOption->tid_history_id = $tidHistory->id;
                    $tidHistoryTradeOption->trade_options_id = $option->id;
                }

                $defaultPercent = $optionSequences[$option->id][$index];

                $tidHistoryTradeOption->coefficient = $option->percent;
                $tidHistoryTradeOption->default_percent = $defaultPercent;
                $tidHistoryTradeOption->percent = $option->percent / 100 * $defaultPercent;
                $tidHistoryTradeOption->save();
            }

            $date->modify('+ 1 DAY');
        }

        $this->tidRequestHistoryRepository->create([
            'period' => $period,
            'percent' => $percent,
            'high_max' => $highMax,
            'low_max' => $lowMax,
            'positive_days' => $positiveDays,
            'loss_sum_percent' => $lossSumPercent
        ]);
    }

    /**
     * Update feature tid histories (now day + 1) for trade option.
     *
     * @param TradeOption $tradeOption
     */
    public function updateForTradeOption(TradeOption $tradeOption): void
    {
        $list = $this->tidHistoryTradeOptionRepository
            ->getByTradeOptionsId($tradeOption->id, new \DateTime());

        foreach ($list as $item) {
            if ($tradeOption->percent != $item->coefficient) {
                $item->coefficient = $tradeOption->percent;
                $item->percent = $item->coefficient / 100 * $item->default_percent;

                $item->save();
            }
        }
    }

    /**
     * Create profit history for new trade option.
     *
     * @param TradeOption $tradeOption
     */
    public function createForTradeOption(TradeOption $tradeOption): void
    {
        $list = $this->tidHistoryRepository->getByGtDate(new \DateTime());

        foreach ($list as $item) {
            $tidHistoryTradeOption = new TidHistoryTradeOption();
            $tidHistoryTradeOption->tid_history_id = $item->id;
            $tidHistoryTradeOption->trade_options_id = $tradeOption->id;
            $tidHistoryTradeOption->coefficient = $tradeOption->percent;
            $tidHistoryTradeOption->default_percent = $item->percent;
            $tidHistoryTradeOption->percent = $tidHistoryTradeOption->coefficient / 100 * $tidHistoryTradeOption->default_percent;
            $tidHistoryTradeOption->save();
        }
    }


    /**
     * Generate sequence.
     *
     * @param int $days
     * @param float $percent
     * @param float $high
     * @param float $low
     * @param int $maxCheckHighLow
     * @param int $positiveDays
     * @param float $lossSumPercent
     * @return array
     * @throws TidHistoryException|Throwable
     */
    public function generateSequence(
        int $days,
        float $percent,
        float $high,
        float $low,
        int $positiveDays,
        float $lossSumPercent,
        int $maxCheckHighLow): array
    {
        $sequence = null;

        for ($i = 0; $i < 20; $i++) {
            try {
                if ($percent >= 0) {
                    $sequence = $this->generatePositivePeriod($days, $percent, $high, $low, $positiveDays, $lossSumPercent, $maxCheckHighLow);
                } else {
                    $sequence = $this->generateNegativePeriod($days, $percent, $high, $low, $positiveDays, $lossSumPercent, $maxCheckHighLow);
                }

                break;
            } catch (Throwable $e) {
                if ($i === 19) {
                    throw $e;
                }
            }
        }

        return $sequence;
    }

    /**
     * Generate positive period.
     *
     * @param int $days
     * @param float $percent
     * @param float $high
     * @param float $low
     * @param int $positiveDays
     * @param float $lossSumPercent
     * @param int $maxCheckHighLow
     * @return array
     * @throws TidHistoryException
     */
    public function generatePositivePeriod(
        int $days,
        float $percent,
        float $high,
        float $low,
        int $positiveDays,
        float $lossSumPercent,
        int $maxCheckHighLow
    ): array
    {
        if (!($positiveDays > 0)) {
            throw new TidHistoryException('This positive days count doesn\'t support.');
        }

        if (!($percent > 0)) {
            throw new TidHistoryException('This number doesn\'t support.');
        }

        $positivePercent = $percent + $lossSumPercent;
        $negativeDays = $days - $positiveDays;

        $sequence = $this->generatePositive($positiveDays, $positivePercent, $high, $maxCheckHighLow);

        if ($negativeDays > 0) {
            $negativeSequence = $this->generatePositive($negativeDays, $lossSumPercent, abs($low), $maxCheckHighLow);
            foreach ($negativeSequence as $negativeSequenceItem) {
                $sequence[] = $negativeSequenceItem / -1;
            }
        }

        shuffle($sequence);

        $minSequence = min($sequence);
        $maxSequence = max($sequence);

        if ($minSequence < $low) {
            throw new TidHistoryException(sprintf('Detected not allowed low number. It is %s', $minSequence));
        } else if ($maxSequence > $high) {
            throw new TidHistoryException(sprintf('Detected not allowed high number. It is %s', $minSequence));
        }

        return $sequence;
    }

    /**
     * Generate negative period.
     *
     * @param int $days
     * @param float $percent
     * @param float $high
     * @param float $low
     * @param int $positiveDays
     * @param float $lossSumPercent
     * @param int $maxCheckHighLow
     * @return array
     * @throws \App\Modules\Investments\Exceptions\TidHistoryException
     */
    public function generateNegativePeriod(
        int $days,
        float $percent,
        float $high,
        float $low,
        int $positiveDays,
        float $lossSumPercent,
        int $maxCheckHighLow
    ): array
    {
        if (!($positiveDays >= 0)) {
            throw new TidHistoryException('This positive days count doesn\'t support.');
        }

        if ($percent >= 0) {
            throw new TidHistoryException('This percent doesn\'t support.');
        }

        if ($lossSumPercent < 0) {
            throw new TidHistoryException('This loss sum percent doesn\'t support.');
        }

        $negativePercent = abs($percent) + $lossSumPercent;
        $negativeDays = $days - $positiveDays;

        //var_dump('Negative sequence');
        //var_dump([$negativeDays, $negativePercent, abs($low)]);

        $sequence = [];
        $negativeSequence = $this->generatePositive($negativeDays, $negativePercent, abs($low), $maxCheckHighLow);
        foreach ($negativeSequence as $item) {
            $sequence[] = $item / -1;
        }

        //var_dump('Positive sequence');

        if ($positiveDays > 0) {
            $positivePercent = $lossSumPercent;
            $positiveSequence = $this->generatePositive($positiveDays, $positivePercent, $high, $maxCheckHighLow);

            foreach ($positiveSequence as $item) {
                $sequence[] = $item;
            }
        }

        shuffle($sequence);

        $minSequence = min($sequence);
        $maxSequence = max($sequence);

        if ($minSequence < $low) {
            throw new TidHistoryException(sprintf('Detected not allowed low number. It is %s', $minSequence));
        } else if ($maxSequence > $high) {
            throw new TidHistoryException(sprintf('Detected not allowed high number. It is %s', $minSequence));
        }

        return $sequence;
    }

    /**
     * Generate positive.
     *
     * @param float $groups
     * @param float $number
     * @param float $high
     * @param int $maxCheckHighLow
     * @return array
     * @throws TidHistoryException
     */
    private function generatePositive(float $groups, float $number, float $high, int $maxCheckHighLow): array
    {
        $sequence = $this->generateSequenceBase($groups, $number, $high);
        sort($sequence);

        $maxChecks = 1000;

        while ($maxChecks > 0) {
            $maxChecks--;

            foreach ($sequence as $key => $item) {
                if ($item > $high) {
                    $n = $item * 100;

                    $partItem = mt_rand(round($n * 0.4), round($n * 0.99)) / 100;
                    $newValueSequenceItem = $sequence[$key] - $partItem;
                    $sequence[$key] = $newValueSequenceItem;

                    $highNumberData = explode('.', $partItem);

                    if (isset($highNumberData[1])) {
                        $floatingPart = (float)sprintf('0.%s', $highNumberData[1]);
                        $sequence[array_rand($sequence)] += $floatingPart;
                    }

                    if ($highNumberData[0] > 0) {
                        $sequenceHigh = $this->generateSequenceBase($groups, (int)$highNumberData[0], $highNumberData[0] / 5);
                        rsort($sequenceHigh);

                        foreach ($sequence as $key1 => $item1) {
                            $before = $sequence[$key1];
                            if (($before + $sequenceHigh[$key1]) <= 0) {
                                $key2 = array_search(max($sequence), $sequence);
                                $sequence[$key2] += $sequenceHigh[$key1];
                            } else {
                                $sequence[$key1] += $sequenceHigh[$key1];
                            }
                        }
                    }
                }
            }

            if (max($sequence) <= $high) {
                break;
            }
        }

        $maxIterations = 1000;
        $divider = 2;

        while (max($sequence) > $high && $maxIterations > 0) {
            $maxIterations--;

            $originSequence = $sequence;
            sort($originSequence);

            if (count($sequence) === 1) {
                throw new TidHistoryException('Detected exception. Count groups must be great then 1');
            }

            rsort($sequence);

            foreach ($sequence as $key => $item) {
                if ($item > $high) {
                    $round1 = (int)round($item * 100 * 0.4);
                    $round2 = (int)round($item * 100 * 0.99);

                    $value = mt_rand($round1, $round2) / 100;
                    $partItem = $value / $divider;
                    $originSequence[array_search($item, $originSequence)] -= $value;

                    for ($i = 0; $i < $divider; $i++) {
                        $originSequence[$i] += $partItem;
                    }
                } else if ($item < 0) {
                    $originSequence[array_search($item, $originSequence)] = abs($item);
                    $originSequence[array_rand($originSequence)] += $item * 2;
                }
            }

            $sequence = $originSequence;
            $divider = $divider + 1 <= count($sequence) ? $divider + 1 : count($sequence);
        }

        $minSequence = min($sequence);
        $maxSequence = max($sequence);

        if (($minSequence < 0 || $maxSequence > $high || array_sum($sequence) - $number > 0.000001 || count($sequence) != $groups)
            && $maxCheckHighLow > 0) {

            $maxCheckHighLow--;

            $sequence = $this->generatePositive($groups, $number, $high, $maxCheckHighLow);
        }

        if ($minSequence < 0) {
            throw new TidHistoryException(sprintf('Detected not allowed number. It is %s', $minSequence));
        } else if ($maxSequence > $high) {
            throw new TidHistoryException(sprintf('Detected not allowed number by high max. High max %s. It is %s', $high, $minSequence));
        }

        shuffle($sequence);
        $sequence = array_values($sequence);

        return $sequence;
    }

    private function generateSequenceBase(float $groups, float $number, float $high): array
    {
        return $this->generatePseudoSequenceWithTotalSum($groups, $number, $high);
    }

    /**
     * Generate sequence with total sum.
     *
     * @param float $groups
     * @param float $number
     * @param float $recommendedHigh
     * @return array
     * @throws TidHistoryException
     */
    private function generatePseudoSequenceWithTotalSum(float $groups, float $number, float $recommendedHigh): array
    {
        return $this->generatePseudoSequenceWithTotalSumBase($groups, $number, $recommendedHigh);
    }

    /**
     * Generate sequence with total sum base method.
     *
     * @param float $groups
     * @param float $number
     * @param float $recommendedHigh
     * @return array
     * @throws \App\Modules\Investments\Exceptions\TidHistoryException
     */
    private function generatePseudoSequenceWithTotalSumBase(float $groups, float $number, float $recommendedHigh): array
    {
        if ($number === (float)0) {
            $fixData = [];
            for ($i = 1; $i <= $groups; $i++) {
                $fixData[] = 0;
            }

            return $fixData;
        }


        $target = $number * 100;
        $n = $groups;
        $addends = [];

        if ($number <= 0 || $recommendedHigh > $number) {
            throw new TidHistoryException(sprintf('Incorrect parameters detected. Number: %s, recommended high: %s', $number, $recommendedHigh));
        }

        $high = $recommendedHigh * 100;

        while ($n) {
            if (1 < $n--) {
                $max = $target - ($n - 1);

                if ($max < $high) {
                    $high = $max;

                    if (!($high > 0)) {
                        $fixNumber = $number / $groups;
                        $fixData = [];
                        for ($i = 1; $i <= $groups; $i++) {
                            $fixData[] = $fixNumber;
                        }

                        return $fixData;
                    }
                }

                $addend = mt_rand($high / 3, $high);

                $target -= $addend;
                $addends[] = $addend / 100;
            } else {
                $addends[] = $target / 100;
            }
        }

        return $addends;
    }
}
