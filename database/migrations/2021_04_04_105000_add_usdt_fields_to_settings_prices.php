<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add usdt fields to settings prices.
 */
final class AddUsdtFieldsToSettingsPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->float('usdt_usdt_rate', 16, 8)
                ->after('eth_usdt_rate')
                ->default(1);
            $table->dateTime('usdt_rate_updated_at')
                ->after('eth_rate_updated_at')
                ->default((new DateTime())->format('Y-m-d H:i:s'));
            $table->float('luft_deposit_usdt_price')
                ->default(0)
                ->after('luft_deposit_eth_price');
            $table->float('luft_withdraw_usdt_price')
                ->default(0)
                ->after('luft_withdraw_eth_price');
        });

        DB::beginTransaction();

        $typeId = DB::table('data_types')
            ->where('name', 'settings_prices')
            ->first()
            ->id;

        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'usdt_usdt_rate',
                'type' => 'number',
                'display_name' => 'Tether usdt Rate',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|numeric"}}',
                'order' => 3
            ]);
        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'usdt_rate_updated_at',
                'type' => 'timestamp',
                'display_name' => 'Usdt Rate Updated At',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|date"}}',
                'order' => 4
            ]);
        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'luft_deposit_usdt_price',
                'type' => 'number',
                'display_name' => 'Luft Deposit Usdt Price',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|numeric"}}',
                'order' => 6
            ]);
        DB::table('data_rows')
            ->insert([
                'data_type_id' => $typeId,
                'field' => 'luft_withdraw_usdt_price',
                'type' => 'number',
                'display_name' => 'Luft Withdraw Usdt Price',
                'required' => 0,
                'browse' => 0,
                'read' => 0,
                'edit' => 1,
                'add' => 0,
                'delete' => 0,
                'details' => '{"validation":{"rule":"required|numeric"}}',
                'order' => 7
            ]);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->dropColumn([
                'usdt_usdt_rate',
                'usdt_rate_updated_at',
                'luft_deposit_usdt_price',
                'luft_withdraw_usdt_price',
            ]);
        });

        $typeId = DB::table('data_types')
            ->where('name', 'settings_prices')
            ->first()
            ->id;

        DB::beginTransaction();
        DB::table('data_rows')
            ->where('data_type_id', $typeId)
            ->whereIn('field', [
                'usdt_usdt_rate',
                'usdt_rate_updated_at',
                'luft_deposit_usdt_price',
                'luft_withdraw_usdt_price',
            ])
            ->delete();
        DB::commit();
    }
}
