<?php

namespace App\Modules\Admin\Controllers;

use App\Modules\ProfitCards\Models\ProfitCard;
use App\Modules\Investments\Repositories\TradeOptionRepository;
use App\Modules\ProfitCards\Repositories\ProfitCardRepository;
use App\Modules\ProfitCards\Repositories\ProfitCardTidExpanseRepository;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

/**
 * Voyager profit cards custom controller.
 */
final class VoyagerProfitCardsController extends VoyagerBaseController
{
    /** @var TradeOptionRepository Trade option repository. */
    protected TradeOptionRepository $tradeOptionRepository;

    /** @var ProfitCardRepository Profit card repository. */
    protected ProfitCardRepository $profitCardRepository;

    /** @var \App\Modules\ProfitCards\Repositories\ProfitCardTidExpanseRepository Profit card tid expanse repository. */
    protected ProfitCardTidExpanseRepository $profitCardTidExpanseRepository;

    /**
     * Constructor.
     *
     * @param TradeOptionRepository $tradeOptionRepository
     * @param ProfitCardRepository $profitCardRepository
     * @param \App\Modules\ProfitCards\Repositories\ProfitCardTidExpanseRepository $profitCardTidExpanseRepository
     */
    public function __construct(
        TradeOptionRepository $tradeOptionRepository,
        ProfitCardRepository $profitCardRepository,
        ProfitCardTidExpanseRepository $profitCardTidExpanseRepository
    )
    {
        $this->tradeOptionRepository = $tradeOptionRepository;
        $this->profitCardRepository = $profitCardRepository;
        $this->profitCardTidExpanseRepository = $profitCardTidExpanseRepository;
    }

    /**
     * Edit user data.
     *
     * @param Request $request
     * @param $id
     * @return View
     */
    public function edit(Request $request, $id)
    {
        /** @var View $view */
        $view = parent::edit($request, $id);
        $view->with('options', $this->tradeOptionRepository->all());

        return $view;
    }

    /**
     * Update user data.
     *
     * @param Request $request
     * @param $id
     * @param Closure|null $extend
     * @return RedirectResponse|void
     * @throws ValidationException
     */
    public function update(Request $request, $id, Closure $extend = null)
    {
        return parent::update($request, $id, function (Request $request, ProfitCard $profitCard) {
            foreach ((array)$request->get('options_expanses') as $option => $value) {
                $optionModel = $this->tradeOptionRepository->find($option);

                if ($optionModel && is_scalar($value)) {
                    $tidExpanse = $profitCard->getTidExpanseByOption($optionModel);

                    if ($tidExpanse) {
                        $tidExpanse->tid_expanse = (int)$value;
                        $tidExpanse->save();
                    } else {
                        $this->profitCardTidExpanseRepository->create([
                            'profit_cards_id' => $profitCard->id,
                            'trade_options_id' => $optionModel->id,
                            'tid_expanse' => (int)$value
                        ]);
                    }
                }
            }
        });
    }
}
