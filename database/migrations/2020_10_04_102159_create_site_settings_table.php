<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->id();
            $table->text('bot_statement_link');
            $table->float('abc_price', 16, 8);
            $table->boolean('timer_enabled');
            $table->dateTime('timer_before');
            $table->string('timer_title', 255);
            $table->text('timer_description');
            $table->text('text_area_exchange');
            $table->float('rate_btc_usd', 16, 8);
            $table->dateTime('rate_updated_at');
            $table->longText('notifications_deposit_emails');
            $table->float('withdraw_fee', 16, 8);
            $table->boolean('is_view_balance_recent_transactions');
            $table->float('max_withdraw_per_month', 16, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
