<?php


namespace App\Modules\Admin\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use TCG\Voyager\Models\DataType;

/**
 * Data type repository.
 */
final class DataTypeRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param DataType $model
     */
    public function __construct(protected DataType $model)
    {}

    /**
     * Get DataType by model name.
     *
     * @param string $model
     * @return DataType|null
     */
    public function findByModelName(string $model): ?DataType
    {
        return $this->model
            ->newQuery()
            ->where('model_name', $model)
            ->first();
    }

    /**
     * Update page size for row.
     *
     * @param string $dataType
     * @param int $limit
     */
    public function updatePageSize(string $dataType, int $limit)
    {
        $dataType = $this->model
            ->newQuery()
            ->where('name', $dataType)
            ->first();

        $details = $dataType->details;
        $details->page_size = $limit;
        $dataType->details = $details;

        $dataType->save();
    }
}
