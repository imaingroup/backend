<?php
declare(strict_types=1);

namespace App\Modules\Settings\Models;

use App\Modules\Core\Traits\MultiLanguageFieldTrait;
use App\Modules\Core\Models\Model;
use Carbon\Carbon;
use TCG\Voyager\Traits\Translatable;

/**
 * SettingsDeposit.
 *
 * @property-read int $id
 * @property bool $is_deposit_block
 * @property bool $is_withdraw_block
 * @property float $withdraw_limit
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property float $withdraw_commission
 * @property bool $disabled_exchange_abc_usdt
 * @property string $reason_disabled_exchange_abc_usdt
 * @property string $withdraw_description
 * @property string $exchange_usdt_abc_text
 * @property bool $disabled_dividends_withdraw
 */
final class SettingsDeposit extends Model
{
    use Translatable;
    use MultiLanguageFieldTrait;

    protected $table = 'settings_deposit';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_deposit_block' => 'boolean',
        'is_withdraw_block' => 'boolean',
        'withdraw_limit' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'withdraw_commission' => 'float',
        'disabled_exchange_abc_usdt' => 'boolean',
        'reason_disabled_exchange_abc_usdt' => 'string',
        'withdraw_description' => 'string',
        'exchange_usdt_abc_text' => 'string',
        'disabled_dividends_withdraw' => 'boolean',
    ];

    protected $translatable = ['exchange_usdt_abc_text'];
}
