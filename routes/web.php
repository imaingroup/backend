<?php
declare(strict_types=1);

use App\Modules\Account\Controllers\AccountController;
use App\Modules\Access\Controllers\ActivateController;
use App\Modules\Admin\Controllers\VoyagerDepositController;
use App\Modules\Admin\Controllers\VoyagerSettingsCurrenciesController;
use App\Modules\Dashboard\Controllers\DashboardController;
use App\Modules\Data\Controllers\DataController;
use App\Modules\Exchange\Controllers\ExchangeController;
use App\Modules\Access\Controllers\ForgotPasswordController;
use App\Modules\External\Controllers\AtlasSwapController;
use App\Modules\External\Controllers\ImperiumController;
use App\Modules\Security\Controllers\IdentificationController;
use App\Modules\Landing\Controllers\LandingController;
use App\Modules\Languages\Controllers\LanguageController;
use App\Modules\Logs\Controllers\LogController;
use App\Modules\Access\Controllers\LoginController;
use App\Modules\Access\Controllers\LogoutController;
use App\Modules\ProfitCards\Controllers\ProfitCardController;
use App\Modules\Access\Controllers\RegisterController;
use App\Modules\Access\Controllers\ResetPasswordController;
use App\Modules\Teams\Controllers\TeamController;
use App\Modules\Actions\Controllers\TransactionsController;
use App\Modules\Deposites\Controllers\DepositController;
use App\Modules\News\Controllers\NewsController;
use App\Modules\Messages\Controllers\MessageController;
use App\Modules\Messages\Models\Message;
use App\Modules\Charts\Controllers\InvestmentsChartController;
use App\Modules\Investments\Controllers\InvestmentsController;
use App\Modules\Users\Controllers\ProfileController;
use App\Modules\Withdraw\Controllers\WithdrawController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    abort(403);
})->name('home');

//external
Route::match(['post'], '/api/external/subscribe', [AtlasSwapController::class, 'subscribe'])->name('external.subscribe');
Route::match(['post'], '/api/external/contact', [AtlasSwapController::class, 'contact'])->name('external.contact');
Route::match(['post'], '/api/external/contact/main', [AtlasSwapController::class, 'contactMain'])->name('external.contact.main');
Route::match(['get'], '/api/external/imperium/download/win', [ImperiumController::class, 'downloadWin'])->name('external.imperium.download.win');
Route::match(['get'], '/api/external/imperium/download/mac', [ImperiumController::class, 'downloadMac'])->name('external.imperium.download.mac');
Route::match(['get'], '/api/external/imperium/version', [ImperiumController::class, 'version'])->name('external.imperium.version');
Route::match(['get'], '/api/external/imperium/rates', [ImperiumController::class, 'rates'])->name('external.imperium.rates');
Route::match(['post'], '/api/external/imperium/report', [ImperiumController::class, 'report'])->name('external.imperium.report');

//landing
Route::match(['get'], '/api/landing/content', [LandingController::class, 'content'])->name('landing.content');
Route::match(['post'], '/api/landing/subscribe', [LandingController::class, 'subscribe'])->name('landing.subscribe');
Route::match(['post'], '/api/landing/contact', [LandingController::class, 'contact'])->name('landing.contact');

//set language
Route::match(['post'], '/api/language/set', [LanguageController::class, 'set'])->name('language.set');

//access routes
Route::match(['get'], '/access/register', [RegisterController::class, 'page'])->name('access.register');
Route::match(['get'], '/access/register/success', [RegisterController::class, 'pageSuccess'])->name('access.register.success');
Route::match(['post'], '/api/register/send', [RegisterController::class, 'send'])->name('register.send');
Route::match(['get'], '/api/register/content', [RegisterController::class, 'content'])->name('register.content');

//login
Route::match(['post'], '/api/login', [LoginController::class, 'login'])->name('login');
Route::match(['get'], '/api/account/auth/check', [AccountController::class, 'checkAuth'])->name('account.auth.check');

//activate
Route::match(['post'], '/api/access/activate', [ActivateController::class, 'activate'])->name('access.activate');
Route::match(['post'], '/api/activation/resend', [ActivateController::class, 'resend'])->name('activation.resend');
Route::match(['get'], '/api/activation/already', [ActivateController::class, 'alreadyActivated'])->name('activation.already');

//forgot
Route::match(['post'], '/api/forgot/send', [ForgotPasswordController::class, 'send'])->name('access.forgot.send');
Route::match(['post'], '/api/reset/{user:reset_token}', [ResetPasswordController::class, 'send'])->name('access.reset.send');

//vue log errors
Route::match(['post'], '/api/log/send', [LogController::class, 'sendLog'])->name('log.send');

//identification info
Route::match(['post'], '/api/user/info', [IdentificationController::class, 'setInfo'])->name('identification.info.set');

//public chars
Route::match(['get'], '/api/investments/chart/option/{tradeOptionChart}', [InvestmentsChartController::class, 'chartByOption'])->name('investments.chart.option');

//private routes
Route::middleware(['auth'])->group(function () {
    //data
    Route::match(['post'], '/api/data', [DataController::class, 'data'])->name('data');

    //global
    Route::match(['get'], '/api/account/global', [AccountController::class, 'global'])->name('account.global');
    Route::match(['post'], '/api/account/global/{tradeOptionChart}', [AccountController::class, 'global'])->name('account.global.post123');
    Route::match(['post'], '/api/account/check/update', [AccountController::class, 'checkUpdate'])->name('account.check.update');
    Route::match(['post'], '/api/account/notification/view', [AccountController::class, 'viewNotification'])->name('account.notification.view');
    Route::match(['post'], '/api/account/notifications/clear', [AccountController::class, 'clearNotifications'])->name('account.notifications.clear');

    //dashboard
    Route::match(['get'], '/api/dashboard/content', [DashboardController::class, 'content'])->name('dashboard.content');
    Route::match(['post'], '/api/profit-card/buy', [ProfitCardController::class, 'buy'])->name('profit_card.buy');

    //news
    Route::match(['get'], '/api/news/categories', [NewsController::class, 'categories'])->name('news.categories');
    Route::match(['get'], '/api/news/list', [NewsController::class, 'list'])->name('news.list');
    Route::match(['get'], '/api/news/item/{news:sid}', [NewsController::class, 'item'])->name('news.item');
    Route::match(['post'], '/api/news/item/{news:sid}/like', [NewsController::class, 'like'])->name('news.like');

    //investments
    Route::match(['get'], '/api/investments/balances', [InvestmentsController::class, 'balances'])->name('investments.balances');
    Route::match(['get'], '/api/investments/options', [InvestmentsController::class, 'options'])->name('investments.options');
    Route::match(['post'], '/api/investments/open', [InvestmentsController::class, 'open'])->name('investments.open');
    Route::match(['get'], '/api/investments/history', [InvestmentsController::class, 'history'])->name('investments.history');
    Route::match(['get'], '/api/investments/chart/v2/{trade?}', [InvestmentsChartController::class, 'chartV2'])->name('investments.chart.v2');

    //deposit
    Route::match(['post'], '/api/deposit/make', [DepositController::class, 'make'])->name('deposit.make');
    Route::match(['post'], '/api/deposit/make/new', [DepositController::class, 'makeNew'])->name('deposit.make.new');
    Route::match(['get'], '/api/deposit/data', [DepositController::class, 'data'])->name('deposit.data');
    Route::match(['get'], '/api/deposit/history', [DepositController::class, 'history'])->name('deposit.history');

    //withdraw
    Route::match(['get'], '/api/withdraw/info', [WithdrawController::class, 'data'])->name('withdraw.data');
    Route::match(['get'], '/api/withdraw/history', [WithdrawController::class, 'history'])->name('withdraw.history');
    Route::match(['post'], '/api/withdraw/create', [WithdrawController::class, 'create'])->name('withdraw.create');

    //exchange
    Route::match(['post'], '/api/exchange/abc/usdt', [ExchangeController::class, 'exchangeAbcUsdt'])->name('exchange.abc_usdt');
    Route::match(['post'], '/api/exchange/usdt/abc', [ExchangeController::class, 'exchangeUsdtAbc'])->name('exchange.usdt_abc');

    //profile
    Route::match(['get'], '/api/profile/content', [ProfileController::class, 'content'])->name('profile.content');
    Route::match(['post'], '/api/profile/account/save', [ProfileController::class, 'accountSave'])->name('profile.account.save');
    Route::match(['post'], '/api/profile/password/change', [ProfileController::class, 'changePassword'])->name('profile.password.change');
    Route::match(['post'], '/api/profile/notifications/save', [ProfileController::class, 'saveNotifications'])->name('profile.notifications.save');
    Route::match(['get'], '/api/profile/2fa/enable', [ProfileController::class, 'enable2fa'])->name('profile.2fa.enable');
    Route::match(['post'], '/api/profile/2fa/set', [ProfileController::class, 'set2fa'])->name('profile.2fa.set');
    Route::match(['post'], '/api/profile/2fa/disable', [ProfileController::class, 'disable2fa'])->name('profile.2fa.disable');

    //transactions
    Route::match(['get'], '/api/transactions', [TransactionsController::class, 'list'])->name('transactions');

    //team
    Route::match(['get'], '/api/team/get', [TeamController::class, 'get'])->name('team.get');

    //messages
    Route::match(['get'], '/api/messages/contacts/get', [MessageController::class, 'getContacts'])->name('messages.contacts.get');
    Route::match(['get'], '/api/messages/chats/get', [MessageController::class, 'getChats'])->name('messages.chats.get');
    Route::match(['post'], '/api/messages/conversation/start/{contact}', [MessageController::class, 'conversationStart'])->name('messages.conversation.start');
    Route::match(['post'], '/api/messages/contacts/add/{invitedUser}', [MessageController::class, 'addContact'])->name('messages.contacts.add');
    Route::match(['post'], '/api/messages/send/{messageSource}/{parentMessage?}', [MessageController::class, 'sendMessage'])->name('messages.send');
    Route::match(['get'], '/api/messages/get/{messageSource}/{type?}/{message?}', [MessageController::class, 'getMessages'])
        ->where('type', sprintf('^%s|%s$', Message::SELECT_TYPE_OLDER, Message::SELECT_TYPE_NEWEST))
        ->name('messages.get');
    Route::match(['get'], '/api/messages/observe/{messageFrom}/{messageTo?}', [MessageController::class, 'observe'])->name('messages.observe');
    Route::match(['post'], '/api/messages/invitation/{chat}/accept', [MessageController::class, 'acceptInvitation'])->name('messages.invitation.accept');
    Route::match(['post'], '/api/messages/group-chat/create', [MessageController::class, 'createGroupChat'])->name('messages.group_chat.create');
    Route::match(['post'], '/api/messages/group-chat/users/delete/{chat}/{chatUser}', [MessageController::class, 'deleteUserFromGroupChat'])->name('messages.group_chat.users.delete');
    Route::match(['post'], '/api/messages/group-chat/users/add/{chat}/{contact}', [MessageController::class, 'addUserToGroupChat'])->name('messages.group_chat.users.add');
    Route::match(['post'], '/api/messages/group-chat/edit/{chat}', [MessageController::class, 'editGroupChat'])->name('messages.group_chat.edit');
    Route::match(['post'], '/api/messages/group-chat/leave/{chat}', [MessageController::class, 'leaveGroupChat'])->name('messages.group_chat.leave');
    Route::match(['post'], '/api/messages/{chat}/clean', [MessageController::class, 'cleanMessages'])->name('messages.chat.clean');
    Route::match(['post'], '/api/messages/message/{message}/delete', [MessageController::class, 'deleteMessage'])->name('messages.message.delete');
    Route::match(['post'], '/api/messages/message/{message}/edit', [MessageController::class, 'editMessage'])->name('messages.message.edit');
    Route::match(['post'], '/api/messages/chat/{chat}/delete', [MessageController::class, 'deleteChat'])->name('messages.chat.delete');
    Route::match(['post'], '/api/messages/contact/{contact}/delete', [MessageController::class, 'deleteContact'])->name('messages.contact.delete');
    Route::match(['post'], '/api/messages/user/{blockedUser}/block', [MessageController::class, 'userBlock'])->name('messages.user.block');
    Route::match(['post'], '/api/messages/contact/{contact}/unblock', [MessageController::class, 'contactUnblock'])->name('messages.contact.unblock');
    Route::match(['get'], '/api/messages/search/{chat?}', [MessageController::class, 'search'])->name('messages.search');

    //logout
    Route::match(['get'], '/api/logout', [LogoutController::class, 'logout'])->name('logout');
});

//admin
Route::group(['prefix' => config('voyager.prefix')], function () {
    Voyager::routes();

    Route::group(['middleware' => 'admin.user'], function () {
        //users
        Route::match(['post'], '/users/2fa/init', 'App\Modules\Admin\Controllers\VoyagerUsersController@google2faInit')->name('voyager.users.2fa_init');
        Route::match(['post'], '/users/2fa/confirm', 'App\Modules\Admin\Controllers\VoyagerUsersController@google2faConfirm')->name('voyager.users.2fa_confirm');
        Route::match(['post'], '/users/{userId}/password/change', 'App\Modules\Admin\Controllers\VoyagerUsersController@passwordChange')->name('voyager.users.password.change');
        Route::match(['post'], '/users/{userId}/balance/change', 'App\Modules\Admin\Controllers\VoyagerUsersController@balanceChange')->name('voyager.users.balance.change');
        Route::match(['post'], '/users/{userId}/promo_code/change', 'App\Modules\Admin\Controllers\VoyagerUsersController@promoCodeChange')->name('voyager.users.promo_code.change');
        Route::match(['post'], '/users/abc/transfer', 'App\Modules\Admin\Controllers\VoyagerUsersController@transferAbc')->name('voyager.users.transfer_abc');
        Route::match(['post'], '/users/teammates/move/{teamLead}', 'App\Modules\Admin\Controllers\VoyagerUsersController@moveTeammates')->name('voyager.users.teammates.move');
        Route::match(['get'], '/users/{userId}/identifications/get', 'App\Modules\Admin\Controllers\VoyagerUsersController@getIdentifications')->name('voyager.users.identifications.get');
        Route::match(['get'], '/users/ip/info', 'App\Modules\Admin\Controllers\VoyagerUsersController@ipInfo')->name('voyager.users.ip.info');

        Route::match(['post'], '/deposits/{deposit}/add/transaction', [VoyagerDepositController::class, 'addTransaction'])->name('voyager.deposit.add.transaction');

        //settings
        Route::match(['get'], '/translate', 'App\Modules\Admin\Controllers\VoyagerTranslateController@list')->name('voyager.translate');
        Route::match(['get'], '/translate/{lang}', 'App\Modules\Admin\Controllers\VoyagerTranslateController@edit')->name('voyager.translate.edit');
        Route::match(['post'], '/translate/save/{lang}', 'App\Modules\Admin\Controllers\VoyagerTranslateController@save')->name('voyager.translate.save');
        Route::match(['post'], '/settings/page-limit/change', 'App\Modules\Admin\Controllers\VoyagerSettingsController@changePageLimit')->name('voyager.settings.page_limit.change');

        Route::prefix('/settings/currencies')
            ->as('voyager.settings.')
            ->group(function () {
                Route::get('/', [VoyagerSettingsCurrenciesController::class, 'main'])->name('currencies');
                Route::get('/data', [VoyagerSettingsCurrenciesController::class, 'data'])->name('currencies.data');
                Route::get('/currencies', [VoyagerSettingsCurrenciesController::class, 'currencies'])->name('currencies.list');
                Route::get('/detail/{cryptoCurrency}', [VoyagerSettingsCurrenciesController::class, 'currencyDetails'])->name('currencies.details');
                Route::put('/set/{cryptoCurrency}', [VoyagerSettingsCurrenciesController::class, 'set'])->name('currencies.set');
                Route::delete('/delete/{depositCurrency}', [VoyagerSettingsCurrenciesController::class, 'delete'])->name('currencies.delete');
            });

        //investments
        Route::match(['post'], '/tid-history/generate', 'App\Modules\Admin\Controllers\VoyagerTidHistoryController@generate')->name('voyager.tid_history.generate');
        Route::match(['post'], '/tid-history/percent/update', 'App\Modules\Admin\Controllers\VoyagerTidHistoryController@updatePercent')->name('voyager.tid_history.percent.update');
        Route::match(['post'], '/tid-history/delete', 'App\Modules\Admin\Controllers\VoyagerTidHistoryController@deleteHistory')->name('voyager.tid_history.delete');

        //finance
        Route::match(['get'], '/finance/global', 'App\Modules\Admin\Controllers\VoyagerFinanceController@global')->name('voyager.finance.global');
        Route::match(['get'], '/finance/wallets', 'App\Modules\Admin\Controllers\VoyagerFinanceController@wallets')->name('voyager.finance.wallets');
        Route::match(['get'], '/finance/wallets/data', 'App\Modules\Admin\Controllers\VoyagerFinanceController@walletsData')->name('voyager.finance.wallets.data');
        Route::match(['post'], '/finance/geth/mnemonic/{cryptoAddress}', 'App\Modules\Admin\Controllers\VoyagerFinanceController@gethMnemonic')->name('voyager.finance.geth.mnemonic');
        Route::match(['put'], '/finance/geth/archive/{cryptoAddress}', 'App\Modules\Admin\Controllers\VoyagerFinanceController@gethArchive')->name('voyager.finance.geth.archive');
        Route::match(['put'], '/finance/geth/unarchived/{cryptoAddress}', 'App\Modules\Admin\Controllers\VoyagerFinanceController@gethUnarchived')->name('voyager.finance.geth.unarchived');
        Route::match(['post'], '/finance/geth/estimate', 'App\Modules\Admin\Controllers\VoyagerFinanceController@gethEstimate')->name('voyager.finance.geth.estimate');
        Route::match(['post'], '/finance/geth/send', 'App\Modules\Admin\Controllers\VoyagerFinanceController@gethSend')->name('voyager.finance.geth.send');
       Route::match(['post'], '/finance/bitcoin/send', 'App\Modules\Admin\Controllers\VoyagerFinanceController@bitcoinSend')->name('voyager.finance.bitcoin.send');
        Route::match(['post'], '/money/add', 'App\Modules\Admin\Controllers\VoyagerFinanceController@massMoneyOperation')->name('voyager.finance.global.money.operation');
        Route::match(['get'], '/tasks/list/get', 'App\Modules\Admin\Controllers\VoyagerTasksController@get')->name('voyager.tasks.list.get');

        //messages
        Route::match(['get'], '/messages/list', 'App\Modules\Admin\Controllers\VoyagerMessagesController@list')->name('voyager.messages.list');
        Route::match(['get'], '/messages/chats/get', 'App\Modules\Admin\Controllers\VoyagerMessagesController@getChats')->name('voyager.messages.chats.get');
        Route::match(['get'], '/messages/{chat}/get', 'App\Modules\Admin\Controllers\VoyagerMessagesController@getMessages')->name('voyager.messages.chat.get');
    });
});
