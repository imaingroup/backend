<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api;

use App\Modules\Core\Dto\BaseDto;

/**
 * CryptoAddressMnemonicApiDto.
 *
 * @property-read string|null $privateKey
 * @property-read string|null $mnemonic
 */
final class CryptoAddressMnemonicApiDto extends BaseDto
{
    /**
     * Construct.
     *
     * @param string|null $privateKey
     * @param string|null $mnemonic
     */
    public function __construct(
        protected ?string $privateKey,
        protected ?string $mnemonic,
    )
    {

    }
}
