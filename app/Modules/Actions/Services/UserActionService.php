<?php


namespace App\Modules\Actions\Services;

use App\Modules\Money\Models\Transaction;
use App\Modules\Users\Models\User;
use App\Modules\Actions\Models\UserAction;
use App\Modules\Deposites\Repositories\DepositTransactionRepository;
use App\Modules\ProfitCards\Repositories\ProfitCardBuyHistoryRepository;
use App\Modules\Money\Repositories\TransactionRepository;
use App\Modules\Actions\Repositories\UserActionRepository;
use App\Modules\Withdraw\Repositories\WithdrawRepository;

/**
 * User actions service.
 */
final class UserActionService
{
    /** @var \App\Modules\Actions\Repositories\UserActionRepository User action repository. */
    private UserActionRepository $userActionRepository;

    /** @var ProfitCardBuyHistoryRepository Profit card buy history repository. */
    private ProfitCardBuyHistoryRepository $profitCardBuyHistoryRepository;

    /** @var DepositTransactionRepository Deposit transactions repository. */
    private DepositTransactionRepository $depositTransactionRepository;

    /** @var WithdrawRepository Withdraw repository. */
    private WithdrawRepository $withdrawRepository;

    /** @var \App\Modules\Money\Repositories\TransactionRepository Transaction repository. */
    private TransactionRepository $transactionRepository;

    /**
     * Constructor.
     *
     * @param \App\Modules\Actions\Repositories\UserActionRepository $userActionRepository
     * @param \App\Modules\ProfitCards\Repositories\ProfitCardBuyHistoryRepository $profitCardBuyHistoryRepository
     * @param DepositTransactionRepository $depositTransactionRepository
     * @param WithdrawRepository $withdrawRepository
     * @param \App\Modules\Money\Repositories\TransactionRepository $transactionRepository
     */
    public function __construct(
        UserActionRepository $userActionRepository,
        ProfitCardBuyHistoryRepository $profitCardBuyHistoryRepository,
        DepositTransactionRepository $depositTransactionRepository,
        WithdrawRepository $withdrawRepository,
        TransactionRepository $transactionRepository
    )
    {
        $this->userActionRepository = $userActionRepository;
        $this->profitCardBuyHistoryRepository = $profitCardBuyHistoryRepository;
        $this->depositTransactionRepository = $depositTransactionRepository;
        $this->withdrawRepository = $withdrawRepository;
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Register user action.
     *
     * @param User $user
     * @param string|int $action
     * @param array $data
     */
    public function register(User $user, string|int $action, array $data)
    {
        $this->userActionRepository->create([
            'users_id' => $user->id,
            'action' => $action,
            'data' => json_encode($data),
        ]);
    }

    /**
     * List activities.
     *
     * @param User $user
     * @return array
     */
    public function list(User $user): array
    {
        $actions = $this->userActionRepository->getByUser($user);
        $data = [];

        foreach ($actions as $action) {
            $additionalData = $this->getAdditionalData($action);

            $data[] = [
                'id' => $action->sid,
                'date' => $action->created_at->format('d/m/Y'),
                'time' => $action->created_at->format('H:i:s'),
                'type' => $action->textAction(),
                'amount' => is_numeric($additionalData['amount']) ? round((float)$additionalData['amount'], 2) : $additionalData['amount'],
                'status' => $additionalData['status'],
                'currency' => $additionalData['currency']
            ];
        }

        return $data;
    }

    /**
     * Get additional data by UserAction.
     *
     * @param UserAction $userAction
     * @return array
     */
    public function getAdditionalData(UserAction $userAction): array
    {
        $userActionData = json_decode($userAction->data);
        $data = [];
        $data['amount'] = null;
        $data['status'] = null;
        $data['currency'] = null;

        if ($userAction->action === UserAction::ACTION_BUY_PROFIT_CARD) {
            $profitCard = $this->profitCardBuyHistoryRepository->find($userActionData->profit_card_buy_histories);

            if ($profitCard) {
                $data['amount'] = $profitCard->card->title;
                $data['status'] = 'CONFIRMED';
                $data['currency'] = null;
            }
        } else if ($userAction->action === UserAction::ACTION_DEPOSIT) {
            $depositTransaction = $this->depositTransactionRepository->find($userActionData->deposits_transactions);

            if ($depositTransaction) {
                $data['amount'] = $depositTransaction->amount_usdt;
                $data['status'] = $depositTransaction->statusText();
                $data['currency'] = 'USDT';
            }
        } else if ($userAction->action === UserAction::ACTION_WITHDRAW) {
            $withdraw = $this->withdrawRepository->find($userActionData->withdraw);

            if ($withdraw) {
                $data['amount'] = $withdraw->amount_usdt;
                $data['status'] = $withdraw->statusText();
                $data['currency'] = 'USDT';
            }
        } else if (in_array($userAction->action, [
                UserAction::ACTION_TRANSFER,
                UserAction::ACTION_COMMISSION_USDT,
                UserAction::ACTION_COMMISSION_ABC,
                UserAction::ACTION_EXCHANGE_USDT_ABC
            ]
        )) {
            $transaction = $this->transactionRepository->find($userActionData->transaction);

            if ($transaction) {
                $currency = $transaction->currency === Transaction::CURRENCY_ABC ? 'IPRO' : 'USDT';

                $data['amount'] = $transaction->amount;
                $data['status'] = 'CONFIRMED';
                $data['currency'] = $currency;
            }
        }

        return $data;
    }
}
