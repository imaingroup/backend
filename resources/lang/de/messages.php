<?php
return [
    'last_message' => 'The chat admin (:admin) has left this group chat. Now the group chat is available in read-only mode.'
];
