<?php return [
	'failed' => 'These credentials don\'t match our records.',
	'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
];