<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration create unique index
 * for columns messages_chats_id, users_id
 * for messages_chats_accepted_invitations table.
 */
final class AddUniqueIndexForAcceptedInvitations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('messages_chats_accepted_invitations', function (Blueprint $table) {
            $table->unique(['messages_chats_id', 'users_id'], 'mcai_messages_chats_id_users_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('messages_chats_accepted_invitations', function (Blueprint $table) {
            $table->dropIndex('mcai_messages_chats_id_users_id');
        });
    }
}
