<?php


namespace App\Modules\Investments\Repositories;

use App\Modules\Investments\Models\TradeHistory;
use App\Modules\Core\Repositories\BaseRepository;
use DateTime;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;

/**
 * Trade history repository.
 */
final class TradeHistoryRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param TradeHistory $model
     */
    public function __construct(protected TradeHistory $model)
    {}

    /**
     * Get last deal.
     *
     * @param int $tradeId
     * @return \App\Modules\Investments\Models\TradeHistory|null
     */
    public function getLastDeal(int $tradeId): ?TradeHistory
    {
        return $this->model
            ->newQuery()
            ->where('trades_id', $tradeId)
            ->orderBy('date', 'DESC')
            ->first();
    }

    /**
     * Get last deals.
     *
     * @param int $tradeId
     * @param int|null $limit
     * @return Collection
     */
    public function getLastDeals(int $tradeId, int $limit = null): Collection
    {
        $query = $this->model
            ->newQuery()
            ->where('trades_id', $tradeId)
            ->orderBy('date', 'DESC');

        if ($limit !== null) {
            $query->limit($limit);
        }

        return $query->get();
    }

    /**
     * Get first deal by trade id list.
     *
     * @param array $tradeIds
     * @param DateTime|null $fromDate
     * @return TradeHistory|null
     */
    public function getFirstDealByTrades(array $tradeIds, DateTime $fromDate = null): ?TradeHistory
    {
        if (count($tradeIds) === 0) {
            return null;
        }

        $query = $this->model
            ->newQuery()
            ->select('th.*')
            ->from('trade_history', 'th')
            ->join('trades as t', function (JoinClause $query) {
                $query->whereColumn('th.trades_id', 't.id');
            })
            ->whereIn('t.id', $tradeIds)
            ->orderBy('th.date', 'ASC')
            ->limit(1);

        if($fromDate) {
            $query->where('th.date', '>=', $fromDate->format('Y-m-d'));
        }

        return $query->first();
    }
}
