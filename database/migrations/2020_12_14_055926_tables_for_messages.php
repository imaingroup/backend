<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * This migrations creates tables in db for storing messages data.
 */
final class TablesForMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //chat list
        Schema::create('messages_chats', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable(true); //only for group chats
            $table->string('avatar', 255)->nullable(true); //only for group chats
            $table->boolean('is_group');
            $table->timestamps();
        });

        //messages
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->text('message');
            $table->unsignedBigInteger('author_users_id');
            $table->unsignedBigInteger('messages_chats_id');
            $table->timestamps();

            $table->foreign('author_users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->foreign('messages_chats_id')
                ->references('id')
                ->on('messages_chats')
                ->onDelete('CASCADE');
        });

        DB::statement('ALTER TABLE messages ADD FULLTEXT fulltext_messages_message(message)');

        //message contacts
        Schema::create('messages_contacts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('contact_users_id');
            $table->unsignedBigInteger('sid');
            $table->boolean('deleted')->default(false);
            $table->boolean('blocked')->default(false);
            $table->timestamps();

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->foreign('contact_users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });

        //user's chats
        Schema::create('messages_chats_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('messages_chats_id');
            $table->unsignedBigInteger('users_id');
            $table->boolean('is_owner');
            $table->timestamps();

            $table->foreign('messages_chats_id')
                ->references('id')
                ->on('messages_chats')
                ->onDelete('CASCADE');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });

        //accepted invitations
        Schema::create('messages_chats_accepted_invitations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('messages_chats_id');
            $table->unsignedBigInteger('users_id');
            $table->timestamps();

            $table->foreign('messages_chats_id')
                ->references('id')
                ->on('messages_chats')
                ->onDelete('CASCADE');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });

        //deleted messages
        Schema::create('messages_deleted', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('messages_id');
            $table->unsignedBigInteger('users_id');
            $table->timestamps();

            $table->foreign('messages_id')
                ->references('id')
                ->on('messages')
                ->onDelete('CASCADE');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });

        //read messages
        Schema::create('messages_read', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('messages_id');
            $table->unsignedBigInteger('users_id');
            $table->timestamps();

            $table->foreign('messages_id')
                ->references('id')
                ->on('messages')
                ->onDelete('CASCADE');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages_deleted');
        Schema::dropIfExists('messages_read');
        Schema::dropIfExists('messages_chats_accepted_invitations');
        Schema::dropIfExists('messages_chats_users');
        Schema::dropIfExists('messages_contacts');
        Schema::dropIfExists('messages');
        Schema::dropIfExists('messages_chats');
    }
}
