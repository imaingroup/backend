<?php
declare(strict_types=1);

namespace App\Modules\Data\Exceptions;

/**
 * DispatchException.
 */
final class DispatchException extends DataException
{
}
