<?php


namespace App\Modules\PromoCodes\Exceptions;


use Exception;

/**
 * This exception may be throw in promo code service.
 */
final class PromoCodeException extends Exception
{

}
