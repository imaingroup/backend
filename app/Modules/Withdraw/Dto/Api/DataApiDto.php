<?php
declare(strict_types=1);

namespace App\Modules\Withdraw\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use Illuminate\Support\Collection;

/**
 * DataApiDto.
 *
 * @property-read Collection $currencies
 * @property-read float $usdtAvailableBalance
 * @property-read float $abcAvailableBalance
 * @property-read bool $enabled2fa
 * @property-read bool $enabledExchange
 * @property-read string $reasonDisabledExchangeAbcUsdt
 * @property-read bool $enabledWithdraw
 * @property-read string $withdrawDescription
 * @property-read float $abcPrice
 * @property-read float $withdrawLimit
 * @property-read float $minimumAmountUsdt
 * @property-read string|null $defaultBtc
 * @property-read string|null $defaultEth
 */
final class DataApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param Collection $currencies
     * @param float $usdtAvailableBalance
     * @param float $abcAvailableBalance
     * @param bool $enabled2fa
     * @param bool $enabledExchange
     * @param string $reasonDisabledExchangeAbcUsdt
     * @param bool $enabledWithdraw
     * @param string $withdrawDescription
     * @param float $abcPrice
     * @param float $withdrawLimit
     * @param float $minimumAmountUsdt
     * @param string|null $defaultBtc
     * @param string|null $defaultEth
     */
    public function __construct(
        public Collection $currencies,
        public float   $usdtAvailableBalance,
        public float   $abcAvailableBalance,
        public bool    $enabled2fa,
        public bool    $enabledExchange,
        public string  $reasonDisabledExchangeAbcUsdt,
        public bool    $enabledWithdraw,
        public string  $withdrawDescription,
        public float   $abcPrice,
        public float   $withdrawLimit,
        public float   $minimumAmountUsdt,
        public ?string $defaultBtc,
        public ?string $defaultEth,
    )
    {
    }
}
