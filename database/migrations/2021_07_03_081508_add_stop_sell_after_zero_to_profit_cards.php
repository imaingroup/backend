<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Add stop sell after zero to profit_cards.
 */
final class AddStopSellAfterZeroToProfitCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('profit_cards', function (Blueprint $table) {
            $table->string('stop_sell_after_zero')
                ->nullable(true)
                ->after('show_number_of_cards');
        });

        DB::transaction(function() {
            DB::table('data_rows')
                ->insert([
                    'data_type_id' =>  $this->getDataType()->id,
                    'field' => 'stop_sell_after_zero',
                    'type' => 'checkbox',
                    'display_name' => 'Stop sell after zero',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '',
                    'order' => 10
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('profit_cards', function (Blueprint $table) {
            $table->dropColumn('stop_sell_after_zero');
        });

        DB::transaction(function() {
            DB::table('data_rows')
                ->where('data_type_id', $this->getDataType()->id)
                ->where('field', 'stop_sell_after_zero')
                ->delete();
        });
    }

    /**
     * Get data type.
     *
     * @return stdClass
     */
    private function getDataType(): stdClass
    {
        return DB::table('data_types')
            ->where('name', 'profit_cards')
            ->first();
    }
}
