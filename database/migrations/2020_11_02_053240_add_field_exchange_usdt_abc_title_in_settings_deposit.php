<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds exchange usdt abc title in settings_deposit table.
 */
class AddFieldExchangeUsdtAbcTitleInSettingsDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->string('exchange_usdt_abc_text', 255)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->dropColumn('exchange_usdt_abc_text');
        });
    }
}
