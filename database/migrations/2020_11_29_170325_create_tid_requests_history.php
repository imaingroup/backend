<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creates tid_requests_history table
 * for store request for generation tid history.
 */
class CreateTidRequestsHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tid_requests_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('period');
            $table->float('percent', 8, 2);
            $table->unsignedSmallInteger('positive_days');
            $table->float('high_max', 8, 2);
            $table->float('low_max', 8, 2);
            $table->unsignedFloat('loss_sum_percent', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tid_requests_history');
    }
}
