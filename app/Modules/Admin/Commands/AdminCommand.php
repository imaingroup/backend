<?php

namespace App\Modules\Admin\Commands;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use TCG\Voyager\Commands\AdminCommand as BaseAdminCommand;

final class AdminCommand extends BaseAdminCommand
{
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        parent::handle();
    }

    protected function getUser($create = false)
    {
        $email = $this->argument('email');

        $model = Auth::guard(app('VoyagerGuard'))->getProvider()->getModel();
        $model = Str::start($model, '\\');

        // If we need to create a new user go ahead and create it
        if ($create) {
            $name = $this->ask('Enter the admin name');
            $username = $name;
            $password = $this->secret('Enter admin password');
            $confirmPassword = $this->secret('Confirm Password');

            // Ask for email if there wasnt set one
            if (!$email) {
                $email = $this->ask('Enter the admin email');
            }

            // check if user with given email exists

            if ($model::where('email', $email)->exists()) {
                $this->info("Can't create user. User with the email " . $email . ' exists already.');

                return;
            }

            if ($model::where('username', $email)->exists()) {
                $this->info("Can't create user. User with the username " . $username . ' exists already.');

                return;
            }

            // Passwords don't match
            if ($password != $confirmPassword) {
                $this->info("Passwords don't match");

                return;
            }

            $this->info('Creating admin account');

            return call_user_func($model . '::forceCreate', [
                'username' => $name,
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password),
                'btc' => '',
                'promo_code' => strtoupper(Str::random(32)),
                'langs_id' => 1,
                'ip' => 1,
                'ip_country' => '',
                'last_activity_at' => new \DateTime()
            ]);
        }

        return call_user_func($model . '::where', 'email', $email)->firstOrFail();
    }
}
