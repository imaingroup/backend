<?php
declare(strict_types=1);

namespace App\Modules\Core\Models;

use App\Modules\Core\Dto\Api\TaskApiDto;
use App\Modules\Logs\Models\OperationLog;
use App\Modules\Users\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\Pure;

/**
 * Tasks model.
 *
 * @property-read int $id
 * @property int $type
 * @property int $status
 * @property-read int $users_id
 * @property-read int $operation_logs_id
 * @property Collection $data
 * @property Collection|null $post_data
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $user
 * @property-read OperationLog $log
 * @property-read string $readableType
 * @property-read string $readableStatus
 */
final class Task extends Model
{
    const LIMIT = 15;

    const TYPE_ADMIN_MASS_MONEY_OPERATION_ABC = 1;
    const TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DEPOSITED = 2;
    const TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DIVIDENDS = 3;
    const TYPE_ADMIN_SEND_GETH_MONEY = 4;
    const TYPE_ADMIN_SEND_BTC_MONEY = 5;
    const TYPE_ADMIN_SEND_USDT_MONEY = 6;
    const TYPE_ADMIN_AUTO_TRANSFER_COMMISSIONS_TO_USDT_WALLETS = 7;
    const TYPE_ADMIN_AUTO_SEND_USDT_MONEY = 8;
    const TYPE_ADMIN_SEND_ETHEREUM_TYPE_MONEY = 9;

    const TYPES = [
        self::TYPE_ADMIN_MASS_MONEY_OPERATION_ABC => 'Mass change abc balance',
        self::TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DEPOSITED => 'Mass change usdt deposited balance',
        self::TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DIVIDENDS => 'Mass change usdt dividends balance',
        self::TYPE_ADMIN_SEND_GETH_MONEY => 'Admin send eth money',
        self::TYPE_ADMIN_SEND_BTC_MONEY => 'Admin send btc money',
        self::TYPE_ADMIN_SEND_USDT_MONEY => 'Admin send usdt money',
        self::TYPE_ADMIN_AUTO_TRANSFER_COMMISSIONS_TO_USDT_WALLETS => 'Auto transfer commissions to usdt wallets',
        self::TYPE_ADMIN_AUTO_SEND_USDT_MONEY => 'Admin auto send usdt money',
        self::TYPE_ADMIN_SEND_ETHEREUM_TYPE_MONEY => 'Admin send ethereum type money',
    ];

    /** @var int[] Only site currencies. */
    const TYPES_MONEY_GROUP = [
        self::TYPE_ADMIN_MASS_MONEY_OPERATION_ABC,
        self::TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DEPOSITED,
        self::TYPE_ADMIN_MASS_MONEY_OPERATION_USDT_DIVIDENDS,
    ];

    const STATUS_NEW = 1;
    const STATUS_IN_PROCESS = 2;
    const STATUS_FAILS = 3;
    const STATUS_PROCESSED = 4;
    const STATUS_FAILS_PARTIAL = 5;
    const STATUS_NORMAL_PARTIAL = 6;

    const STATUSES = [
        self::STATUS_NEW => 'New',
        self::STATUS_IN_PROCESS => 'In process',
        self::STATUS_FAILS => 'Fails',
        self::STATUS_PROCESSED => 'Processed',
        self::STATUS_FAILS_PARTIAL => 'Fails partial',
        self::STATUS_NORMAL_PARTIAL => 'Normal partial',
    ];

    public const AUTO_SEND_USDT_START = 1;
    public const AUTO_SEND_USDT_COMISSION_SENT = 2;
    public const AUTO_SEND_USDT_SENT = 3;

    /** {@inheritdoc} */
    protected $table = 'tasks';

    /** {@inheritdoc} */
    protected $casts = [
        'id' => 'integer',
        'type' => 'integer',
        'status' => 'integer',
        'users_id' => 'integer',
        'operation_logs_id' => 'integer',
        'data' => 'encrypted:collection',
        'post_data' => 'encrypted:collection',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * Get attribute "readableType".
     *
     * @return string|null
     */
    public function getReadableTypeAttribute(): ?string
    {
        return (string)(static::TYPES[$this->type] ?? $this->type);
    }

    /**
     * Get attribute "readableStatus".
     *
     * @return string|null
     */
    public function getReadableStatusAttribute(): ?string
    {
        return (string)(static::STATUSES[$this->status] ?? $this->status);
    }

    /**
     * Get relation with User.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Get relation with {@see OperationLog::class}.
     *
     * @return BelongsTo
     */
    public function log(): BelongsTo
    {
        return $this->belongsTo(OperationLog::class, 'operation_logs_id');
    }

    /**
     * Get data in string for admin.
     *
     * @return mixed|string
     */
    public function getDataReadAttribute()
    {
        return $this->data ? json_encode($this->data) : null;
    }

    /**
     * To api dto.
     *
     * @return TaskApiDto
     */
    #[Pure]
    public function toApiDto(): TaskApiDto
    {
        return new TaskApiDto(
            $this->id,
            $this->user->toApiDto(),
            $this->type,
            $this->status,
            $this->readableStatus,
            $this->data,
            $this->post_data,
            $this->created_at,
            $this->updated_at,
        );
    }
}
