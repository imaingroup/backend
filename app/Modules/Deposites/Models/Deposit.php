<?php
declare(strict_types=1);

namespace App\Modules\Deposites\Models;

use App\Modules\Finance\Models\CryptoCurrency;
use App\Modules\Users\Models\User;
use App\Modules\Core\Models\Model;
use App\Modules\Finance\Models\CryptoAddress;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Deposit model.
 *
 * @property-read int $id
 * @property-read int $users_id
 * @property-read int|null $crypto_currencies_id
 * @property float $amount_usdt
 * @property float $amount_coin
 * @property float $rate
 * @property int|null $type_coin
 * @property string $address
 * @property int $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $user
 * @property-read CryptoCurrency|null $currency
 * @property-read Collection|DepositTransaction[] $depositTransactions
 * @property-read string $currencyFullName
 */
final class Deposit extends Model
{
    const STATUS_NEW = 1; //when invoice was created.
    const STATUS_CLOSED = 2; //when expire awaiting time or executed method 'create new invoice'.
    const STATUS_FINISHED = 3; //when finished monitoring.

    const TYPE_BTC = CryptoAddress::TYPE_BTC; //type btc

    const MINIMUM_AMOUNT = 1; //minimum amount
    const LIMIT_INVOICE_MINUTES = 30; //limit creating new invoices in minutes
    const LIMIT_CREATE_NEW_INVOICE_MINUTES = 20;

    const MINCONF = 2; //minimum count confirmations for change status transaction to CONFIRMED
    const OBSERVATION_TIME_MINUTES = 1440; //observation time

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'users_id' => 'integer',
        'crypto_currencies_id' => 'integer',
        'amount_usdt' => 'float',
        'amount_coin' => 'float',
        'rate' => 'float',
        'type_coin' => 'integer',
        'address' => 'string',
        'status' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get relation with User
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /**
     * Get relation with {@see CryptoCurrency::class}.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(CryptoCurrency::class, 'crypto_currencies_id');
    }

    /**
     * Get relation with DepositTransaction
     *
     * @return HasMany
     */
    public function depositTransactions(): HasMany
    {
        return $this->hasMany(DepositTransaction::class, 'deposits_id');
    }

    /**
     * Get count transactions attribute.
     *
     * @return int
     */
    public function getCountTransactionsAttribute(): int
    {
        return $this->depositTransactions()->count();
    }

    /**
     * Get attribute "currencyFullName".
     *
     * @return string
     */
    public function getCurrencyFullNameAttribute(): string
    {
        return $this->currency ? sprintf(
            '%s [%s]',
            $this->currency
                ->symbol,
            $this->currency->network->toEnum()->value
        ) : '';
    }

    /**
     * Find transaction.
     *
     * @param string $txid
     * @return DepositTransaction|null
     */
    public function findTransaction(string $txid): ?DepositTransaction
    {
        return $this->depositTransactions
            ->where('txid', $txid)
            ->first();
    }
}
