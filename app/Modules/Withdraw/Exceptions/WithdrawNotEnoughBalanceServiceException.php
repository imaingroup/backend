<?php


namespace App\Modules\Withdraw\Exceptions;



use App\Modules\Withdraw\Exceptions\WithdrawServiceException;

/**
 * This exception may be throws if total confirmed wallet balance less than amount+fee+reserve
 */
final class WithdrawNotEnoughBalanceServiceException extends WithdrawServiceException
{

}
