<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Finance;

use App\Modules\Finance\Dto\Services\Admin\Finance\EthereumSendDto;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\RequestException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\ValidationException;
use App\Modules\Finance\Services\FinanceService;
use App\Modules\Finance\Services\V2\DataGetterService;
use App\Modules\Finance\Services\V2\Ethereum\ManageService;
use App\Modules\Users\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;
use Litipk\BigNumbers\Decimal;

/**
 * AdminFinanceGethSendRequest.
 */
final class AdminFinanceGethSendRequest extends FormRequest
{
    private Decimal $withdrawAmount;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            $user->hasRole(User::ROLE_ADMIN_NAME) &&
            in_array($user->id, User::USERS_ALLOWED_SEND_MONEY);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'balance_id' => "string",
        'to' => "string",
        'amount' => "string",
        'withdraw_all' => "string",
        '2fa' => "string"
    ])]
    public function rules(): array
    {
        return [
            'balance_id' => 'required|exists:crypto_balances,id',
            'to' => 'required|string|ethereum',
            'amount' => 'nullable|numeric|gt:0',
            'withdraw_all' => 'required|boolean',
            '2fa' => 'required|g2fa_install|g2fa',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): EthereumSendDto
    {
        return new EthereumSendDto(
            Auth::user(),
            $this->getFinanceService()
                ->findCryptoBalance((int)$this['balance_id']),
            $this['to'],
            $this->withdrawAmount,
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()
            ->after(function (Validator $validator) {
                if (count($validator->failed()) === 0) {
                    $this->after($validator);
                }
            });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @throws RequestException
     * @throws ValidationException
     */
    private function after(Validator $validator): void
    {
        if (!filter_var($this['withdraw_all'], FILTER_VALIDATE_BOOLEAN) &&
            (!isset($this['amount']) || (float)$this['amount'] <= 0)
        ) {
            $validator->errors()->add(
                'amount',
                'Must specify withdraw all or amount.'
            );

            return;
        }

        $balanceModel = $this->getFinanceService()
            ->findCryptoBalance((int)$this['balance_id']);
        $mainCurrency = $this->getDataGetterService()
            ->findMainCurrencyByNetwork($balanceModel->currency->network->toEnum());

        $mainBalance = $this->getManageService()
            ->getBalance(
                $balanceModel->currency->network->toEnum(),
                $balanceModel->address->address,
            );

        $balance = $balanceModel->currency->isMainCurrencyInNetwork ?
            $mainBalance :
            $this->getManageService()
                ->getTokenBalance(
                    $balanceModel->currency->network->toEnum(),
                    $balanceModel->currency->address,
                    $balanceModel->address->address,
                );

        $mainBalanceInNative = Decimal::create($mainBalance->balance)
            ->div(Decimal::create(10)->pow(Decimal::create($mainCurrency->decimals)));

        $balance = Decimal::create($balance->balance)
            ->div(Decimal::create(10)->pow(Decimal::create($balanceModel->currency->decimals)));

        $amount = Decimal::create(
            filter_var($this['withdraw_all'], FILTER_VALIDATE_BOOLEAN) ?
                $balance : (float)$this['amount']
        );

        if ($amount->isGreaterThan($balance)) {
            $validator->errors()->add(
                'amount',
                sprintf(
                    'You are trying to send amount %s (%s USD) that great then actual balance %s (%s USD)',
                    $amount->innerValue(),
                    $amount->mul(Decimal::create($balanceModel->currency->market->price))->innerValue(),
                    $balance->innerValue(),
                    $balance->mul(Decimal::create($balanceModel->currency->market->price))->innerValue()
                ),
            );

            return;
        }

        $gas = $balanceModel->currency->isMainCurrencyInNetwork ?
            $this->getManageService()
                ->estimate(
                    $balanceModel->currency->network->toEnum(),
                    $balanceModel->address->address,
                    $this['to'],
                    $amount->innerValue(),
                ) :
            $this->getManageService()
                ->estimateErc20(
                    $balanceModel->currency->network->toEnum(),
                    $balanceModel->currency->address,
                    $this['to'],
                    $amount->innerValue(),
                    $balanceModel->currency->decimals,
                    $balanceModel->address->access_key
                );

        $feeAmount = Decimal::create($gas->gas);

        if (filter_var($this['withdraw_all'], FILTER_VALIDATE_BOOLEAN)) {
            $withdrawAmount = $balanceModel->currency->isMainCurrencyInNetwork ?
                $amount->sub($feeAmount):
                $amount;
        } else {
            $withdrawAmount = Decimal::create((float)$this['amount']);
        }

        $this->withdrawAmount = $withdrawAmount;

        if ($feeAmount->isGreaterThan($mainBalanceInNative)) {
            $validator->errors()->add(
                'amount',
                sprintf(
                    'Gas will take %s (%s USD) but this wallet has %s (%s USD)',
                    $feeAmount->innerValue(),
                    $feeAmount->mul(Decimal::create($mainCurrency->market->price))->innerValue(),
                    $mainBalanceInNative->innerValue(),
                    $mainBalanceInNative->mul(Decimal::create($mainCurrency->market->price))->innerValue()
                ),
            );
        } else if ($balanceModel->currency->isMainCurrencyInNetwork &&
            $withdrawAmount->isGreaterThan($mainBalanceInNative->sub($feeAmount))
        ) {
            $validator->errors()->add(
                'amount',
                sprintf(
                    'Gas will take %s (%s USD) but this wallet has %s (%s USD) in native token and you are trying to send %s (%s USD)',
                    $feeAmount->innerValue(),
                    $feeAmount->mul(Decimal::create($mainCurrency->market->price)),
                    $mainBalanceInNative->innerValue(),
                    $mainBalanceInNative->mul(Decimal::create($mainCurrency->market->price))->innerValue(),
                    $withdrawAmount->innerValue(),
                    $withdrawAmount->mul(Decimal::create($mainCurrency->market->price))->innerValue()
                ),
            );
        } else if (!$balanceModel->currency->isMainCurrencyInNetwork &&
            $withdrawAmount > $balance
        ) {
            $validator->errors()->add(
                'amount',
                sprintf(
                    'You are trying to send %s (%s USD) but wallet has %s (%s) on this token',
                    $withdrawAmount->innerValue(),
                    $withdrawAmount->mul(Decimal::create($mainCurrency->market->price))->innerValue(),
                    $balance->innerValue(),
                    $balance->mul(Decimal::create($mainCurrency->market->price))->innerValue(),
                ),
            );
        }
    }

    /**
     * Get {@see ManageService::class}.
     *
     * @return ManageService
     */
    private function getManageService(): ManageService
    {
        return app(ManageService::class);
    }

    /**
     * Get {@see FinanceService::class}.
     *
     * @return FinanceService
     */
    private function getFinanceService(): FinanceService
    {
        return app(FinanceService::class);
    }

    /**
     * Get {@see DataGetterService::class}.
     *
     * @return DataGetterService
     */
    private function getDataGetterService(): DataGetterService
    {
        return app(DataGetterService::class);
    }
}
