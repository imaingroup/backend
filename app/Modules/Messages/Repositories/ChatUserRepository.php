<?php
declare(strict_types=1);


namespace App\Modules\Messages\Repositories;

use App\Modules\Messages\Models\ChatUser;
use App\Modules\Core\Repositories\BaseRepository;

/**
 * Chat user repository.
 */
final class ChatUserRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param ChatUser $model
     */
    public function __construct(protected \App\Modules\Messages\Models\ChatUser $model)
    {
    }

    /**
     * Exists user in chat.
     *
     * @param int $chatId
     * @param int $userId
     * @param bool $withDeleted
     * @return \App\Modules\Messages\Models\ChatUser|null
     */
    public function getChatUser(int $chatId, int $userId, bool $withDeleted): ?ChatUser
    {
        $query = $this->model
            ->newQuery()
            ->where('messages_chats_id', $chatId)
            ->where('users_id', $userId);

        if (!$withDeleted) {
            $query->where('deleted', false);
        }

        return $query->first();
    }
}
