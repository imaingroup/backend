<?php
declare(strict_types=1);

namespace App\Modules\Messages\Exceptions;

/**
 * Chat exception.
 */
final class ChatException extends MessageException
{

}
