<?php
declare(strict_types=1);

namespace App\Modules\Core\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Users\Dto\Api\UserShortDtoApi;
use DateTime;
use Illuminate\Support\Collection;

/**
 * TaskApiDto.
 *
 * @property-read int $id
 * @property-read UserShortDtoApi $user
 * @property-read int $type
 * @property-read int $status
 * @property-read string $readableStatus
 * @property-read Collection $data
 * @property-read Collection|null $postData
 * @property-read DateTime $createdAt
 * @property-read DateTime $updatedAt
 */
final class TaskApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param int $id
     * @param UserShortDtoApi $user
     * @param int $type
     * @param int $status
     * @param string $readableStatus
     * @param Collection $data
     * @param Collection|null $postData
     * @param DateTime $createdAt
     * @param DateTime $updatedAt
     */
    public function __construct(
        protected int $id,
        protected UserShortDtoApi $user,
        protected int $type,
        protected int $status,
        protected string $readableStatus,
        protected Collection $data,
        protected ?Collection $postData,
        protected DateTime $createdAt,
        protected DateTime $updatedAt,
    )
    {

    }
}
