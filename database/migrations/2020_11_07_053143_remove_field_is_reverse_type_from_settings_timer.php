<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration removes field is_reverse_type from settings_timer table.
 */
final class RemoveFieldIsReverseTypeFromSettingsTimer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_timer', function (Blueprint $table) {
            $table->dropColumn('is_reverse_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings_timer', function (Blueprint $table) {
            $table->boolean('is_reverse_type')->default(true);
        });
    }
}
