<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Models\Message;
use App\Modules\Messages\Services\MessageService;
use App\Modules\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation messages obserev request.
 */
final class ObserveMessagesRequest extends FormRequest
{
    static public ?Message $messageFrom = null;
    static public ?Message $messageTo = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $ms = $this->getMessageService();
        $user = Auth::user();

        return $user &&
            self::$messageFrom instanceof Message &&
            $ms->checkAccessMessage($user, self::$messageFrom, null, true) &&
            (
                !self::$messageTo ||
                (
                    self::$messageTo instanceof Message &&
                    $ms->checkAccessMessage($user, self::$messageTo, null, true) &&
                    self::$messageFrom->isSameChat(self::$messageTo)
                )
            );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape([
        'user' => User::class,
        'messageFrom' => Message::class,
        'messageTo' => Message::class|null,
    ])]
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'messageFrom' => self::$messageFrom,
            'messageTo' => self::$messageTo,
        ];
    }

    /**
     * Get MessageService.
     *
     * @return MessageService
     */
    private function getMessageService(): MessageService
    {
        return app(MessageService::class);
    }
}
