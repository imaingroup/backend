<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

use App\Modules\Finance\Exceptions\FinanceException;

/**
 * Abstract ethereum exception.
 */
abstract class EthereumException extends FinanceException
{
}
