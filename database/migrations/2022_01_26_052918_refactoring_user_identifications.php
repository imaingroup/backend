<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * RefactoringUserIdentifications.
 */
final class RefactoringUserIdentifications extends Migration
{
    private const FIELDS = [
        'user_agent',
        'browser',
        'browser_version',
        'browser_major_version',
        'is_ie',
        'is_chrome',
        'is_firefox',
        'is_safari',
        'is_opera',
        'engine',
        'engine_version',
        'platform',
        'os',
        'os_version',
        'is_windows',
        'is_linux',
        'is_ubuntu',
        'is_solaris',
        'device',
        'device_type',
        'device_vendor',
        'cpu',
        'is_mobile',
        'is_mobile_android',
        'is_mobile_opera',
        'is_mobile_windows',
        'is_mobile_blackberry',
        'is_mobile_ios',
        'is_iphone',
        'is_ipod',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_identifications_user_agent', function (Blueprint $table) {
            $table->id();
            $table->string('user_agent');
            $table->string('browser')
                ->nullable(true);
            $table->string('browser_version')
                ->nullable(true);
            $table->string('browser_major_version')
                ->nullable(true);
            $table->boolean('is_ie')
                ->nullable(true);
            $table->boolean('is_chrome')
                ->nullable(true);
            $table->boolean('is_firefox')
                ->nullable(true);
            $table->boolean('is_safari')
                ->nullable(true);
            $table->boolean('is_opera')
                ->nullable(true);
            $table->string('engine')
                ->nullable(true);
            $table->string('engine_version')
                ->nullable(true);
            $table->string('platform')
                ->nullable(true);
            $table->string('os')
                ->nullable(true);
            $table->string('os_version')
                ->nullable(true);
            $table->boolean('is_windows')
                ->nullable(true);
            $table->boolean('is_linux')
                ->nullable(true);
            $table->boolean('is_ubuntu')
                ->nullable(true);
            $table->boolean('is_solaris')
                ->nullable(true);
            $table->string('device')
                ->nullable(true);
            $table->string('device_type')
                ->nullable(true);
            $table->string('device_vendor')
                ->nullable(true);
            $table->string('cpu')
                ->nullable(true);
            $table->boolean('is_mobile')
                ->nullable(true);
            $table->boolean('is_mobile_android')
                ->nullable(true);
            $table->boolean('is_mobile_opera')
                ->nullable(true);
            $table->boolean('is_mobile_windows')
                ->nullable(true);
            $table->boolean('is_mobile_blackberry')
                ->nullable(true);
            $table->boolean('is_mobile_ios')
                ->nullable(true);
            $table->boolean('is_iphone')
                ->nullable(true);
            $table->boolean('is_ipod')
                ->nullable(true);
            $table->timestamps();
            $table->unique('user_agent', 'users_identifications_user_agent_user_agent_unique');
        });

        Schema::table('users_identifications', function (Blueprint $table) {
           $table->unsignedBigInteger('user_agent_id')
                ->after('id')
                ->nullable(true);
            $table->foreign('user_agent_id', 'users_identifications_user_agent_id_foreign')
                ->references('id')
                ->on('users_identifications_user_agent')
                ->cascadeOnDelete();
        });

        DB::transaction(function () {
            $isRunning = true;

            while ($isRunning) {
                $count = DB::table('users_identifications')
                    ->whereNotNull('user_agent')
                    ->limit(100)
                    ->get()
                    ->each(function (stdClass $identification) {
                        $userAgent = DB::table('users_identifications_user_agent')
                            ->where('user_agent', strtolower($identification->user_agent))
                            ->first();

                        if (!$userAgent) {
                            $data = [
                                'created_at' => now(),
                                'updated_at' => now(),
                            ];

                            collect(self::FIELDS)
                                ->each(function(string $field) use(&$data, $identification) {
                                    $data[$field] = $field === 'user_agent' ?
                                        strtolower($identification->user_agent) :
                                        $identification->$field;
                                });

                            $userAgentId = DB::table('users_identifications_user_agent')
                                ->insertGetId($data);
                        } else {
                            $userAgentId = $userAgent->id;
                        }

                        DB::table('users_identifications')
                            ->where('id', $identification->id)
                            ->update([
                                'user_agent_id' => $userAgentId,
                                'user_agent' => null,
                            ]);
                    })
                    ->count();

                if ($count === 0) {
                    $isRunning = false;
                }
            }
        });

        Schema::table('users_identifications', function (Blueprint $table) {
            $table->dropColumn(self::FIELDS);

            $table->unsignedBigInteger('user_agent_id')
                ->nullable(false)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_identifications', function (Blueprint $table) {
            $table->unsignedBigInteger('user_agent_id')
                ->nullable(true)
                ->change();
            $table->string('user_agent')
                ->nullable(true);
            $table->string('browser')
                ->nullable(true);
            $table->string('browser_version')
                ->nullable(true);
            $table->string('browser_major_version')
                ->nullable(true);
            $table->boolean('is_ie')
                ->nullable(true);
            $table->boolean('is_chrome')
                ->nullable(true);
            $table->boolean('is_firefox')
                ->nullable(true);
            $table->boolean('is_safari')
                ->nullable(true);
            $table->boolean('is_opera')
                ->nullable(true);
            $table->string('engine')
                ->nullable(true);
            $table->string('engine_version')
                ->nullable(true);
            $table->string('platform')
                ->nullable(true);
            $table->string('os')
                ->nullable(true);
            $table->string('os_version')
                ->nullable(true);
            $table->boolean('is_windows')
                ->nullable(true);
            $table->boolean('is_linux')
                ->nullable(true);
            $table->boolean('is_ubuntu')
                ->nullable(true);
            $table->boolean('is_solaris')
                ->nullable(true);
            $table->string('device')
                ->nullable(true);
            $table->string('device_type')
                ->nullable(true);
            $table->string('device_vendor')
                ->nullable(true);
            $table->string('cpu')
                ->nullable(true);
            $table->boolean('is_mobile')
                ->nullable(true);
            $table->boolean('is_mobile_android')
                ->nullable(true);
            $table->boolean('is_mobile_opera')
                ->nullable(true);
            $table->boolean('is_mobile_windows')
                ->nullable(true);
            $table->boolean('is_mobile_blackberry')
                ->nullable(true);
            $table->boolean('is_mobile_ios')
                ->nullable(true);
            $table->boolean('is_iphone')
                ->nullable(true);
            $table->boolean('is_ipod')
                ->nullable(true);
        });

        DB::transaction(function () {
            $isRunning = true;

            while ($isRunning) {
                $count = DB::table('users_identifications')
                    ->whereNotNull('user_agent_id')
                    ->limit(100)
                    ->get()
                    ->each(function (stdClass $identification) {
                        $userAgent = DB::table('users_identifications_user_agent')
                            ->where('id', $identification->user_agent_id)
                            ->first();

                        $data = [
                            'user_agent_id' => null,
                        ];

                        collect(self::FIELDS)
                            ->each(function(string $field) use(&$data, $userAgent) {
                                $data[$field] = $userAgent->$field;
                            });

                        DB::table('users_identifications')
                            ->where('id', $identification->id)
                            ->update($data);
                    })
                    ->count();

                if ($count === 0) {
                    $isRunning = false;
                }
            }
        });

        Schema::table('users_identifications', function (Blueprint $table) {
            $table->dropForeign('users_identifications_user_agent_id_foreign');
            $table->dropColumn('user_agent_id');
        });

        Schema::drop('users_identifications_user_agent');
    }
}
