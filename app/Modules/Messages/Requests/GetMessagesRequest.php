<?php
declare(strict_types=1);

namespace App\Modules\Messages\Requests;

use App\Modules\Messages\Models\Chat;
use App\Modules\Messages\Models\Contact;
use App\Modules\Messages\Models\Message;
use App\Modules\Messages\Services\MessageService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This class need for validation get messages request.
 */
final class GetMessagesRequest extends FormRequest
{
    static public Contact|Chat|null $messageSource = null;
    static public ?int $type = null;
    static public ?Message $message = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $ms = $this->getMessageService();
        $user = Auth::user();

        $chat = null;

        if (self::$messageSource instanceof Contact) {
            $chat = $ms->getSingleChat(self::$messageSource);
        } else if (self::$messageSource instanceof Chat) {
            $chat = self::$messageSource;
        }

        return $user &&
            (
                (
                    self::$messageSource instanceof Contact &&
                    $ms->checkAccessContact($user, self::$messageSource)
                ) ||
                (
                    self::$messageSource instanceof Chat &&
                    $ms->checkAccessChat($user, self::$messageSource)
                )
            ) &&
            (
                !self::$message ||
                (
                    self::$message instanceof Message &&
                    $ms->checkAccessMessage($user, self::$message, null, true) &&
                    $chat &&
                    self::$message->checkChat($chat)
                )
            );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['order' => "array"])]
    public function rules(): array
    {
        return [
            'order' => [
                'nullable',
                Rule::in(Message::ORDERS)
            ]
        ];
    }

    /**
     * {@inheritDoc}
     */
    #[ArrayShape([
        'user' => "App\\Modules\\Users\\Models\\User|null",
        'order' => "string",
    ])]
    public function validated(): array
    {
        return [
            'user' => Auth::user(),
            'order' => $this['order'] ?? Message::ORDER_DESC,
        ];
    }

    /**
     * Get MessageService.
     *
     * @return MessageService
     */
    private function getMessageService(): MessageService
    {
        return app(MessageService::class);
    }
}
