<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * This migration encrypt task data.
 */
final class EncryptTasksData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::beginTransaction();

        foreach (DB::table('tasks')
                     ->whereNotNull('data')
                     ->get() as $task) {
            DB::table('tasks')
                ->where('id', $task->id)
                ->update([
                    'data' => encrypt(collect(json_decode($task->data, true)), false),
                ]);
        }

        foreach (DB::table('tasks')
                     ->whereNotNull('post_data')
                     ->get() as $task) {
            DB::table('tasks')
                ->where('id', $task->id)
                ->update([
                    'post_data' => encrypt(collect(json_decode($task->post_data, true)), false),
                ]);
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::beginTransaction();

        foreach (DB::table('tasks')
                     ->whereNotNull('data')
                     ->get() as $task) {
            DB::table('tasks')
                ->where('id', $task->id)
                ->update([
                    'data' => decrypt($task->data, false),
                ]);
        }

        foreach (DB::table('tasks')
                     ->whereNotNull('post_data')
                     ->get() as $task) {
            DB::table('tasks')
                ->where('id', $task->id)
                ->update([
                    'post_data' => decrypt($task->post_data, false),
                ]);
        }

        DB::commit();
    }
}
