<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration adds previous_updated_at field
 * to trade_options table.
 */
final class PreviousUpdatedAtToTradeOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->dateTime('previous_updated_at')
                ->after('previous_percent')
                ->nullable();
        });

        DB::table('trade_options')
            ->whereNotNull('previous_percent')
            ->update([
                'previous_updated_at' => new DateTime()
            ]);

        DB::table('data_types')
            ->where('name', 'trade_options')
            ->update([
                'model_name' => 'App\Modules\Investments\Models\TradeOption'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('trade_options', function (Blueprint $table) {
            $table->dropColumn('previous_updated_at');
        });

        DB::table('data_types')
            ->where('name', 'trade_options')
            ->update([
                'model_name' => 'App\Models\TradeOption'
            ]);
    }
}
