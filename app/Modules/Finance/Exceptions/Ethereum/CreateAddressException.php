<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\Ethereum;

/**
 * CreateAddressException.
 */
final class CreateAddressException extends EthereumException
{
}
