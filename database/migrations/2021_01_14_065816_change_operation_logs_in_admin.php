<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/**
 * This migration change operation_logs
 * bread in admin.
 */
final class ChangeOperationLogsInAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::beginTransaction();

        $dataTypeOperationLogId = DB::table('data_types')
            ->where('name', 'operation_logs')
            ->first()
            ->id;

        DB::table('data_rows')
            ->where('data_type_id', $dataTypeOperationLogId)
            ->where('field', 'tasks_id')
            ->update([
                'display_name' => 'Tasks id'
            ]);

        DB::table('data_rows')
            ->where('data_type_id', $dataTypeOperationLogId)
            ->where('field', 'type')
            ->update([
                'details' => '{"options":{"1":"Mass money operation","2":"Move teammates"}}'
            ]);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
    }
}
