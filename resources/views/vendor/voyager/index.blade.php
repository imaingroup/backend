@extends('voyager::master')

@section('content')
    <div class="page-content">
        @include('voyager::alerts')
        @include('voyager::dimmers')
        <div class="analytics-container">
        <!--<?php $google_analytics_client_id = Voyager::setting("admin.google_analytics_client_id"); ?>
        @if (isset($google_analytics_client_id) && !empty($google_analytics_client_id))
            {{-- Google Analytics Embed --}}
                <div id="embed-api-auth-container"></div>
            @else
            <p style="border-radius:4px; padding:20px; background:#fff; margin:0; color:#999; text-align:center;">
{!! __('voyager::analytics.no_client_id') !!}
                <a href="https://console.developers.google.com" target="_blank">https://console.developers.google.com</a>
            </p>
@endif -->

            <div class="Dashboard Dashboard--full" id="analytics-dashboard" style="display: block">
                <ul class="FlexGrid FlexGrid--halves">
                    <li class="FlexGrid-item">
                        <div class="Chartjs">
                            <header class="Titles">
                                <h1 class="Titles-main">Statistics</h1>
                                <div class="Titles-sub">IPRO</div>
                            </header>
                            <div>
                                <div>Bought for a deposit:
                                    <strong>{{ number_format(num: $iproStats->sumBoughtDeposited, decimals: 2, thousands_separator: ' ') }}</strong>
                                </div>
                                <div>Bought for a dividends:
                                    <strong>{{ number_format(num: $iproStats->sumBoughtDividends, decimals: 2, thousands_separator: ' ') }}</strong>
                                </div>
                                <div>Received via cards:
                                    <strong>{{ number_format(num: $iproStats->sumCards, decimals: 2, thousands_separator: ' ') }}</strong>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="Dashboard Dashboard--full" id="analytics-dashboard" style="display: block">
                <ul class="FlexGrid FlexGrid--halves">
                    <li class="FlexGrid-item">
                        <div class="Chartjs">
                            <header class="Titles">
                                <h1 class="Titles-main">Trading Cards purchase history</h1>
                            </header>
                            <div>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Email</th>
                                        <th>Card Type</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($purchaseTradingHistory as $historyItem)
                                    <tr>
                                        <td>
                                            <a target="_blank" href="{{ route('voyager.users.index', ['key' => 'id', 'filter' => 'equals', 's' => $historyItem->user->id]) }}">{{ $historyItem->user->id }}</a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="{{ route('voyager.users.index', ['key' => 'id', 'filter' => 'equals', 's' => $historyItem->user->id]) }}">{{ $historyItem->user->email }}</a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="{{ route('voyager.profit-cards.index', ['key' => 'id', 'filter' => 'equals', 's' => $historyItem->card->id]) }}">{{ $historyItem->card->title }}</a>
                                        </td>
                                        <td>{{ $historyItem->created_at->format('Y-m-d') }}</td>
                                        <td>{{ $historyItem->created_at->format('H:i:s') }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="Dashboard Dashboard--full" id="analytics-dashboard" style="display: block">
                <ul class="FlexGrid FlexGrid--halves">
                    <li class="FlexGrid-item">
                        <div class="Chartjs">
                            <header class="Titles">
                                <h1 class="Titles-main">Expanded Cards purchase history</h1>
                            </header>
                            <div>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Email</th>
                                        <th>Card Type</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($purchaseExpandedHistory as $historyItem)
                                    <tr>
                                        <td>
                                            <a target="_blank" href="{{ route('voyager.users.index', ['key' => 'id', 'filter' => 'equals', 's' => $historyItem->user->id]) }}">{{ $historyItem->user->id }}</a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="{{ route('voyager.users.index', ['key' => 'id', 'filter' => 'equals', 's' => $historyItem->user->id]) }}">{{ $historyItem->user->email }}</a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="{{ route('voyager.profit-cards.index', ['key' => 'id', 'filter' => 'equals', 's' => $historyItem->card->id]) }}">{{ $historyItem->card->title }}</a>
                                        </td>
                                        <td>{{ $historyItem->created_at->format('Y-m-d') }}</td>
                                        <td>{{ $historyItem->created_at->format('H:i:s') }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <style>
        .Chartjs {
            font-size: 1.5em;
        }

        ul.FlexGrid {
            width: 75%;
        }
    </style>
@stop
