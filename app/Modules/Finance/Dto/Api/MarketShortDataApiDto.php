<?php
declare(strict_types=1);

namespace App\Modules\Finance\Dto\Api;

use App\Modules\Core\Dto\BaseDto;
use App\Modules\Finance\Enum\CoingeckoCoinEnum;
use App\Modules\Finance\Enum\CoingeckoNetworkEnum;
use DateTime;

/**
 * MarketShortDataApiDto.
 *
 * @property-read CoingeckoNetworkEnum|null $network
 * @property-read CoingeckoCoinEnum|null $coin
 * @property-read string|null $address
 * @property-read string|null $image
 * @property-read float $usdtPrice
 * @property-read DateTime|null $priceUpdatedAt
 */
final class MarketShortDataApiDto extends BaseDto
{
    /**
     * Constructor.
     *
     * @param CoingeckoNetworkEnum|null $network
     * @param CoingeckoCoinEnum|null $coin
     * @param string|null $address
     * @param string|null $image
     * @param string $usdtPrice
     * @param DateTime|null $priceUpdatedAt
     */
    public function __construct(
        protected ?CoingeckoNetworkEnum $network,
        protected ?CoingeckoCoinEnum $coin,
        protected ?string $address,
        protected ?string $image,
        protected string $usdtPrice,
        protected ?DateTime $priceUpdatedAt = null,
    ) {
    }
}
