<?php
declare(strict_types=1);

namespace App\Modules\Money\Services;

use App\Modules\Money\Exceptions\MoneyTransferException;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Core\Models\Task;
use App\Modules\Money\Models\Transaction;
use App\Modules\Users\Models\User;
use App\Modules\Actions\Models\UserAction;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Money\Services\TransactionService;
use App\Modules\Actions\Services\UserActionService;
use Exception;

/**
 * Money transfer service.
 */
final class MoneyTransferService
{
    /**
     * Constructor.
     *
     * @param TransactionService $transactionService
     * @param \App\Modules\Actions\Services\UserActionService $userActionService
     */
    public function __construct(
        private TransactionService $transactionService,
        private \App\Modules\Actions\Services\UserActionService $userActionService
    )
    {
    }

    /**
     * Send abc from user to user
     *
     * @param \App\Modules\Users\Models\User $sender
     * @param \App\Modules\Users\Models\User $recipient
     * @param User $initiator
     * @param float $amount
     * @throws Exception
     */
    public function sendAbc(User $sender, User $recipient, User $initiator, float $amount)
    {
        if ($amount < 0) {
            $throw = new MoneyTransferException('Amount can\'t less then zero');

            $this->getLogger()
                ->registerException($throw, [
                    'sender' => $sender->balances(),
                    'recipient' => $recipient->balances(),
                    'amount' => $amount,
                ]);

            throw $throw;
        }

        User::refreshAndLockForUpdate($sender);
        User::refreshAndLockForUpdate($recipient);

        if ($sender->balance_abc < $amount) {
            $throw = new MoneyTransferException('Sender doesn\'t have enough money in ABC balance');

            $this->getLogger()
                ->registerException($throw, [
                    'sender' => $sender->balances(),
                    'recipient' => $recipient->balances(),
                    'amount' => $amount,
                ]);

            throw $throw;
        }

        $transaction = $this->moneyOperation(
            $sender,
            $initiator,
            $amount / -1,
            Transaction::CURRENCY_ABC,
            Transaction::TYPE_TRANSFER_FROM_USER
        );

        $addTransaction = $this->moneyOperation(
            $recipient,
            $initiator,
            $amount,
            Transaction::CURRENCY_ABC,
            Transaction::TYPE_TRANSFER_TO_USER,
            $transaction
        );

        $this->userActionService->register(
            $recipient,
            UserAction::ACTION_TRANSFER,
            [
                'transaction' => $addTransaction->id
            ]
        );
    }

    /**
     * Add and withdraw money.
     *
     * @param \App\Modules\Users\Models\User $user
     * @param \App\Modules\Users\Models\User $initiator
     * @param float $amount
     * @param int $typeBalance Transaction::CURRENCY_USDT_DEPOSITED, Transaction::CURRENCY_USDT_DIVIDENTS, Transaction::CURRENCY_ABC
     * @param int|null $transactionType
     * @param \App\Modules\Money\Models\Transaction|null $relatedTransaction
     * @param \App\Modules\Core\Models\Task|null $task
     * @return \App\Modules\Money\Models\Transaction
     * @throws \App\Modules\Money\Exceptions\MoneyTransferException
     * @throws TransactionNotStarted
     */
    public function moneyOperation(
        User $user,
        User $initiator,
        float $amount,
        int $typeBalance,
        int $transactionType = null,
        Transaction $relatedTransaction = null,
        Task $task = null
    ): Transaction
    {
        User::refreshAndLockForUpdate($user);

        if ($typeBalance == Transaction::CURRENCY_ABC) {
            $balancesBefore = $user->balances();

            $user->balance_abc += $amount;
            $user->balance_total_abc += $amount;

            if ($amount > 0) {
                $user->total_received_abc += $amount;
            }

            $balancesAfter = $user->balances();
            $userBalanceAfter = $user->balance_abc;
        } else if ($typeBalance == Transaction::CURRENCY_USDT_DEPOSITED) {
            $balancesBefore = $user->balances();

            $user->balance_usdt += $amount;
            $user->deposited_usdt += $amount;

            if ($amount > 0) {
                $user->total_deposited_team += $amount;
                $user->total_deposited += $amount;
                $user->total_invested_balance += $amount;
            }

            $balancesAfter = $user->balances();
            $userBalanceAfter = $user->deposited_usdt;
        } else if ($typeBalance == Transaction::CURRENCY_USDT_DIVIDENTS) {
            $balancesBefore = $user->balances();

            $user->balance_usdt += $amount;
            $user->dividents_usdt += $amount;

            $balancesAfter = $user->balances();
            $userBalanceAfter = $user->dividents_usdt;
        } else {
            $throw = new MoneyTransferException('Unsupported operation');

            $this->getLogger()
                ->registerException($throw, [
                    'balances' => $user->balances(),
                    'amount' => $amount,
                    'typeBalance' => $typeBalance,
                    'transactionType' => $transactionType,
                ]);

            throw $throw;
        }

        $operationType = $amount < 0 ? Transaction::OPERATION_TYPE_WITHDRAW : Transaction::OPERATION_TYPE_ADD;

        $user->save();

        $transactionComment = ($amount < 0) ?
            \sprintf('Withdrawn %s %s for user %s with initiator %s', abs($amount), Transaction::$currencyName[$typeBalance], $user->id, $initiator->id) :
            \sprintf('Added %s %s for user %s with initiator %s', $amount, Transaction::$currencyName[$typeBalance], $user->id, $initiator->id);

        if (!$transactionType) {
            $transactionType = ($amount < 0 ? Transaction::TYPE_WITHDRAW_MONEY_ADMIN : Transaction::TYPE_ADD_MONEY_ADMIN);
        }

        return $this->transactionService->create(
            $user,
            $initiator,
            $transactionType,
            abs($amount),
            $userBalanceAfter,
            $transactionComment,
            [
                'users_id' => $user->id,
                'initiator_users_id' => $initiator->id,
                'balances_before' => $balancesBefore,
                'balances_after' => $balancesAfter,
                'context' => \sprintf('%s::%s', 'MoneyTransferService', 'moneyOperation')
            ],
            $typeBalance,
            new \DateTime(),
            $operationType,
            $relatedTransaction,
            $task
        );
    }

    /**
     * Add money to trading.
     *
     * @param \App\Modules\Users\Models\User $user
     * @param \App\Modules\Users\Models\User $initiator
     * @param float $amount
     * @return \App\Modules\Money\Models\Transaction
     * @throws \App\Modules\Money\Exceptions\MoneyTransferException
     * @throws TransactionNotStarted
     */
    public function addToTrading(User $user, User $initiator, float $amount): Transaction
    {
        User::refreshAndLockForUpdate($user);

        if ($amount < 0) {
            $throw = new MoneyTransferException('Amount can\'t less then zero');

            $this->getLogger()
                ->registerException($throw, [
                    'balances' => $user->balances(),
                    'amount' => $amount,
                ]);

            throw $throw;
        }

        if ($user->balance_abc < $amount) {
            $throw = new MoneyTransferException('User doesn\'t have enough money in ABC balance');

            $this->getLogger()
                ->registerException($throw, [
                    'balances' => $user->balances(),
                    'amount' => $amount,
                ]);

            throw $throw;
        }

        $transaction = $this->moneyOperation(
            $user,
            $initiator,
            $amount / -1,
            Transaction::CURRENCY_ABC,
            Transaction::TYPE_WITHDRAW_FOR_TRADING
        );

        $balancesBefore = $user->balances();

        $user->traded_abc_balance += $amount;
        $user->balance_total_abc += $amount;

        $user->save();

        $balancesAfter = $user->balances();

        $comment = \sprintf(
            'Money sent to trading for user %s for amount %s with initiator %s',
            $user->id,
            $amount,
            $initiator->id
        );

        return $this->transactionService->create(
            $user,
            $initiator,
            Transaction::TYPE_ADD_TO_TRADING,
            $amount,
            $user->balance_abc,
            $comment,
            [
                'users_id' => $user->id,
                'initiator_users_id' => $initiator->id,
                'balances_before' => $balancesBefore,
                'balances_after' => $balancesAfter,
                'context' => \sprintf('%s::%s', 'MoneyTransferService', 'addToTrading')
            ],
            Transaction::CURRENCY_ABC,
            new \DateTime(),
            Transaction::OPERATION_TYPE_ADD_TO_TRADING,
            $transaction
        );
    }

    /**
     * Withdraw money from trading.
     *
     * @param User $user
     * @param User $initiator
     * @param float $amount
     * @param \App\Modules\Money\Models\Transaction|null $relatedTransaction
     * @return \App\Modules\Money\Models\Transaction
     * @throws \App\Modules\Money\Exceptions\MoneyTransferException
     * @throws TransactionNotStarted
     */
    public function withdrawFromTrading(User $user, User $initiator, float $amount, Transaction $relatedTransaction = null): Transaction
    {
        User::refreshAndLockForUpdate($user);

        if ($amount < 0) {
            $throw = new MoneyTransferException('Amount can\'t less then zero');

            $this->getLogger()
                ->registerException($throw, [
                    'balances' => $user->balances(),
                    'amount' => $amount,
                ]);

            throw $throw;
        }

        if ($user->traded_abc_balance < $amount && !in_array($user->id, [709, 1069])) {
            $throw = new MoneyTransferException('User doesn\'t have enough money in traded ABC balance');

            $this->getLogger()
                ->registerException($throw, [
                    'balances' => $user->balances(),
                    'amount' => $amount,
                ]);

            throw $throw;
        }

        $balancesBefore = $user->balances();

        if ($user->traded_abc_balance < $amount) {
            $amount = $user->traded_abc_balance;
        }

        $user->traded_abc_balance -= $amount;
        $user->balance_total_abc -= $amount;
        $user->total_received_abc -= $amount;

        $user->save();

        $balancesAfter = $user->balances();

        $comment = \sprintf(
            'Money withdrawn from trading for user %s for amount %s with initiator %s',
            $user->id,
            $amount,
            $initiator->id
        );

        $transactionWithdraw = $this->transactionService->create(
            $user,
            $initiator,
            Transaction::TYPE_WITHDRAW_FROM_TRADING,
            $amount,
            $user->balance_abc,
            $comment,
            [
                'users_id' => $user->id,
                'initiator_users_id' => $initiator->id,
                'balances_before' => $balancesBefore,
                'balances_after' => $balancesAfter,
                'context' => \sprintf('%s::%s', 'MoneyTransferService', 'withdrawFromTrading')
            ],
            Transaction::CURRENCY_ABC,
            new \DateTime(),
            Transaction::OPERATION_TYPE_WITHDRAW_FROM_TRADING,
            $relatedTransaction
        );

        return $this->moneyOperation(
            $user,
            $initiator,
            $amount,
            Transaction::CURRENCY_ABC,
            Transaction::TYPE_ADD_FROM_TRADING,
            $transactionWithdraw
        );
    }

    /**
     * Get Logger.
     *
     * @return Logger
     */
    private function getLogger(): Logger
    {
        return app(Logger::class);
    }
}
