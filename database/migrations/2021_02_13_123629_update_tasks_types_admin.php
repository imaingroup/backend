<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * This migrations updates tasks types/statuses admin.
 */
final class UpdateTasksTypesAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::beginTransaction();

        $typeId = DB::table('data_types')
            ->where('name', 'tasks')
            ->first()
            ->id;

        DB::table('data_rows')
            ->where('data_type_id', $typeId)
            ->where('field', 'type')
            ->update([
                'details' => '{"options":{"1":"Mass change abc balance","2":"Mass change usdt deposited balance","3":"Mass change usdt dividends balance","4":"Admin send eth money","5":"Admin send btc money"}}',
            ]);


        DB::table('data_rows')
            ->where('data_type_id', $typeId)
            ->where('field', 'status')
            ->update([
                'details' => '{"options":{"1":"New","2":"In process","3":"Fails","4":"Processed","5":"Fails partial"}}',
            ]);

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::beginTransaction();

        $typeId = DB::table('data_types')
            ->where('name', 'tasks')
            ->first()
            ->id;

        DB::table('data_rows')
            ->where('data_type_id', $typeId)
            ->where('field', 'type')
            ->update([
                'details' => '{"options":{"1":"Mass change abc balance","2":"Mass change usdt deposited balance","3":"Mass change usdt dividends balance"}}',
            ]);

        DB::table('data_rows')
            ->where('data_type_id', $typeId)
            ->where('field', 'status')
            ->update([
                'details' => '{"options":{"1":"New","2":"In process","3":"Fails","4":"Processed"}}',
            ]);

        DB::commit();
    }
}
