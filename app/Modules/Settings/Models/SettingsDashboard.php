<?php
declare(strict_types=1);

namespace App\Modules\Settings\Models;

use App\Modules\Core\Models\Model;

/**
 * Settings dashboard.
 */
final class SettingsDashboard extends Model
{
    /** {@inheritdoc} */
    protected $table = 'settings_dashboard';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'statement_link' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'disabled_recent_activities' => 'boolean',
        'hide_number_of_cards' => 'boolean',
        'ra_types' => 'json'
    ];

    /**
     * Get recent activities types edit attribute.
     *
     * @return mixed
     */
    public function getRaTypesEditAttribute(): mixed
    {
        return $this->attributes['ra_types'];
    }

    /**
     * Get ra types attribute.
     *
     * @return mixed
     */
    public function getRaTypesAttribute(): mixed
    {
        if ($this->attributes['ra_types'] === null) {
            return null;
        }

        $value = is_scalar($this->attributes['ra_types']) ?
            json_decode($this->attributes['ra_types'], true) :
            $this->attributes['ra_types'];

        if (is_scalar($value)) {
            $value = json_decode($value, true);
        }

        return $value;
    }
}
