<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddFieldUserIdToBitcoinLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bitcoin_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('users_id')->nullable(true);

            $table->foreign('users_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table bitcoin_logs drop foreign key bitcoin_logs_users_id_foreign;');

        Schema::table('bitcoin_logs', function (Blueprint $table) {
            $table->dropColumn('users_id');
        });
    }
}
