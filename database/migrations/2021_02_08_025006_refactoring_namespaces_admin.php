<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * This migration updates admin after
 * refactoring namespaces.
 */
final class RefactoringNamespacesAdmin extends Migration
{
    private const DATA_M = [
        'App\Models\User' => 'App\Modules\Users\Models\User',
        'App\Models\Lang' => 'App\Modules\Languages\Models\Lang',
        'App\Models\UserActivity' => 'App\Modules\Users\Models\UserActivity',
        'App\Models\UserAction' => 'App\Modules\Actions\Models\UserAction',
        'App\Models\News' => 'App\Modules\News\Models\News',
        'App\Models\NewsCategory' => 'App\Modules\News\Models\NewsCategory',
        'App\Models\Video' => 'App\Modules\News\Models\Video',
        'App\Models\ProfitCard' => 'App\Modules\ProfitCards\Models\ProfitCard',
        'App\Models\ProfitCardBuyHistory' => 'App\Modules\ProfitCards\Models\ProfitCardBuyHistory',
        'App\Models\Chart' => 'App\Modules\Charts\Models\Chart',
        'App\Models\Trade' => 'App\Modules\Investments\Models\Trade',
        'App\Models\Transaction' => 'App\Modules\Money\Models\Transaction',
        'App\Models\SettingsPrice' => 'App\Modules\Settings\Models\SettingsPrice',
        'App\Models\SettingsTimer' => 'App\Modules\Settings\Models\SettingsTimer',
        'App\Models\SettingsDeposit' => 'App\Modules\Settings\Models\SettingsDeposit',
        'App\Models\SettingsLanding' => 'App\Modules\Settings\Models\SettingsLanding',
        'App\Models\SettingsLink' => 'App\Modules\Settings\Models\SettingsLink',
        'App\Models\SettingsAbc' => 'App\Modules\Settings\Models\SettingsAbc',
        'App\Models\SettingsInvestments' => 'App\Modules\Settings\Models\SettingsInvestments',
        'App\Models\SettingsDashboard' => 'App\Modules\Settings\Models\SettingsDashboard',
        'App\Models\SettingsTeam' => 'App\Modules\Settings\Models\SettingsTeam',
        'App\Models\TidHistory' => 'App\Modules\Investments\Models\TidHistory',
        'App\Models\Notification' => 'App\Modules\Account\Models\Notification',
        'App\Models\Subscribe' => 'App\Modules\Landing\Models\Subscribe',
        'App\Models\Contact' => 'App\Modules\Landing\Models\Contact',
        'App\Models\Mail' => 'App\Modules\Notifications\Models\Mail',
        'App\Models\MailVariable' => 'App\Modules\Notifications\Models\MailVariable',
        'App\Models\Task' => 'App\Modules\Core\Models\Task',
        'App\Models\OperationLog' => 'App\Modules\Logs\Models\OperationLog',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::beginTransaction();

        foreach (self::DATA_M as $key => $new) {
            DB::table('data_types')
                ->where('model_name', $key)
                ->update([
                    'model_name' => $new
                ]);
        }

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::beginTransaction();

        foreach (array_flip(self::DATA_M) as $key => $new) {
            DB::table('data_types')
                ->where('model_name', $key)
                ->update([
                    'model_name' => $new
                ]);
        }

        DB::commit();
    }
}
