<?php
declare(strict_types=1);

namespace App\Modules\Settings\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Settings\Models\SettingsImperium;

/**
 * Settings imperium repository.
 */
final class SettingsImperiumRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param SettingsImperium $model
     */
    public function __construct(protected SettingsImperium $model)
    {}

    /**
     * Get settings model.
     *
     * @return SettingsImperium
     */
    public function get(): SettingsImperium
    {
        return $this->model
            ->newQuery()
            ->first();
    }
}
