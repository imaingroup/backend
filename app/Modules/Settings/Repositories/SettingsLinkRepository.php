<?php


namespace App\Modules\Settings\Repositories;

use App\Modules\Settings\Models\SettingsLink;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Settings abc repository.
 */
final class SettingsLinkRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Settings\Models\SettingsLink $model
     */
    public function __construct(protected SettingsLink $model)
    {}

    /**
     * Get all links.
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('is_visible', true)
            ->orderBy('sort', 'ASC')
            ->get();
    }
}
