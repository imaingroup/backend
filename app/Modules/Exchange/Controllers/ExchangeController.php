<?php

namespace App\Modules\Exchange\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Exchange\Requests\ExchangeAbcUsdtRequest;
use App\Modules\Exchange\Requests\ExchangeUsdtAbcRequest;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Users\Models\User;
use App\Modules\Exchange\Services\ExchangeService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

/**
 * Exchange controller.
 */
final class ExchangeController extends Controller
{
    /** @var ExchangeService Exchange service. */
    private ExchangeService $exchangeService;

    /** @var \App\Modules\Logs\Loggers\Logger Logger service. */
    private Logger $logger;

    /**
     * Constructor.
     *
     * @param \App\Modules\Exchange\Services\ExchangeService $exchangeService
     * @param \App\Modules\Logs\Loggers\Logger $logger
     */
    public function __construct(ExchangeService $exchangeService, Logger $logger)
    {
        $this->exchangeService = $exchangeService;
        $this->logger = $logger;
    }

    /**
     * Exchange abc to usdt.
     *
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function exchangeAbcUsdt(): Response
    {
        return $this->inTransaction(function(): void {
            $user = Auth::user();

            User::refreshAndLockForUpdate($user);

            $data = app(ExchangeAbcUsdtRequest::class)->validated();

            $this->exchangeService->exchangeAbcUsdt($user, (float) $data['amount_abc']);
        });
    }

    /**
     * Exchange usdt to abc.
     *
     * @return Response
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function exchangeUsdtAbc(): Response
    {
        return $this->inTransaction(function(): void {
            $user = Auth::user();

            User::refreshAndLockForUpdate($user);

            $data = app(ExchangeUsdtAbcRequest::class)->validated();

            $this->exchangeService->exchangeUsdtAbc($user, (float) $data['amount_usdt']);
        });
    }
}
