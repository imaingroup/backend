<?php


namespace App\Facades;

use App\Modules\Core\Exceptions\DBException;
use Closure;
use Illuminate\Support\Facades\DB as DBBase;

/**
 * This class extend base DB facade.
 */
class DB extends DBBase
{
    /** @var array */
    private static array $transactionCallbacks = [];

    /** @var array */
    private static array $transactionAnyCallbacks = [];

    /** @var array */
    private static array $transactionRollbackCallbacks = [];

    /**
     * Add post transaction code.
     *
     * @param Closure $callback
     */
    public static function addPostTransactionLogic(Closure $callback)
    {
        static::$transactionCallbacks[] = $callback;

        if (static::transactionLevel() === 0) {
            static::executeTransactionCallbacks();
        }
    }

    /**
     * Add post transaction rollback logic.
     *
     * @param Closure $callback
     * @throws \App\Modules\Core\Exceptions\DBException
     */
    public static function addPostTransactionRollbackLogic(Closure $callback)
    {
        if (static::transactionLevel() === 0) {
            throw new DBException('Transaction not started.');
        }

        static::$transactionRollbackCallbacks[] = $callback;
    }

    /**
     * Add post transaction any logic.
     *
     * @param Closure $callback
     */
    public function addPostTransactionAnyLogic(Closure $callback)
    {
        static::$transactionAnyCallbacks[] = $callback;

        if (static::transactionLevel() === 0) {
            static::executeTransactionAnyCallbacks();
        }
    }

    /**
     * {@inheritDoc}
     */
    public static function beginTransaction(): void
    {
        static::cleanTransactionCallbacks();
        parent::beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public static function commit(): void
    {
        parent::commit();
        static::executeTransactionCallbacks();
        static::executeTransactionAnyCallbacks();
    }

    /**
     * {@inheritDoc}
     */
    public static function rollBack(int $toLevel = null): void
    {
        parent::rollBack();
        static::executeTransactionRollbackCallbacks();
        static::executeTransactionAnyCallbacks();
    }

    /**
     * Execute transaction confirmed callbacks.
     */
    private static function executeTransactionCallbacks(): void
    {
        $callbacks = static::$transactionCallbacks;
        static::$transactionCallbacks = [];

        static::executeCallbacks($callbacks);
    }

    /**
     * Execute transaction rollback callbacks.
     */
    private static function executeTransactionRollbackCallbacks(): void
    {
        $callbacks = static::$transactionRollbackCallbacks;
        static::$transactionRollbackCallbacks = [];

        static::executeCallbacks($callbacks);
    }

    /**
     * Execute transaction any callbacks.
     */
    private static function executeTransactionAnyCallbacks(): void
    {
        $callbacks = static::$transactionAnyCallbacks;
        static::$transactionAnyCallbacks = [];

        static::executeCallbacks($callbacks);
    }

    /**
     * Execute callbacks.
     *
     * @param array $callbacks
     */
    private static function executeCallbacks(array $callbacks)
    {
        foreach ($callbacks as $transactionCallback) {
            $transactionCallback();
        }
    }

    /**
     * Clean callbacks.
     */
    private static function cleanTransactionCallbacks(): void
    {
        static::$transactionCallbacks = [];
        static::$transactionAnyCallbacks = [];
        static::$transactionRollbackCallbacks = [];
    }
}
