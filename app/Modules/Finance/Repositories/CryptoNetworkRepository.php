<?php
declare(strict_types=1);

namespace App\Modules\Finance\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Finance\Enum\BitcoinNetworkEnum;
use App\Modules\Finance\Enum\EthereumNetworkEnum;
use App\Modules\Finance\Models\CryptoNetwork;
use Illuminate\Support\Collection;

/**
 * CryptoNetworkRepository.
 */
final class CryptoNetworkRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param CryptoNetwork $model
     */
    public function __construct(protected CryptoNetwork $model)
    {
    }

    /**
     * Find by network.
     *
     * @param EthereumNetworkEnum|BitcoinNetworkEnum $network
     * @return CryptoNetwork|null
     */
    public function findByNetwork(EthereumNetworkEnum|BitcoinNetworkEnum $network): ?CryptoNetwork
    {
        return $this->model->newQuery()
            ->where('code', $network->value)
            ->where('type', $network->type())
            ->first();
    }

    /**
     * Get sorted.
     *
     * @return Collection
     */
    public function getSorted(): Collection
    {
        return $this->model->newQuery()
            ->orderBy('code')
            ->orderBy('type')
            ->get();
    }
}
