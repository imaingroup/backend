@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Translates')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-folder"></i> Translates
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            Language
                                        </th>
                                        <th class="actions text-right dt-not-orderable">
                                            Actions
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($languages as $item)
                                    <tr>
                                        <td>
                                            {{ $item->name }} ({{ $item->short_name }})
                                        </td>
                                        <td class="no-sort no-click bread-actions">
                                            <a href="{{ route('voyager.translate.edit', ['lang' => $item->short_name]) }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@stop

@section('javascript')
    <!-- DataTables -->
    <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
@stop
