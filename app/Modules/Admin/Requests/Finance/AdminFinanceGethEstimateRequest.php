<?php
declare(strict_types=1);

namespace App\Modules\Admin\Requests\Finance;

use App\Modules\Finance\Dto\Services\Admin\Finance\EthereumSendDto;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\RequestException;
use App\Modules\Finance\Exceptions\Services\V2\Ethereum\Manage\ValidationException;
use App\Modules\Finance\Services\FinanceService;
use App\Modules\Finance\Services\V2\DataGetterService;
use App\Modules\Finance\Services\V2\Ethereum\ManageService;
use App\Modules\Users\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;
use Litipk\BigNumbers\Decimal;

/**
 * AdminFinanceGethEstimateRequest.
 */
final class AdminFinanceGethEstimateRequest extends FormRequest
{
    private Decimal $withdrawAmount;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $user = Auth::user();

        return $user &&
            $user->hasRole(User::ROLE_ADMIN_NAME) &&
            in_array($user->id, User::USERS_ALLOWED_SEND_MONEY);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape([
        'balance_id' => "string",
        'to' => "string",
        'amount' => "string",
        'withdraw_all' => "string",
        '2fa' => "string"
    ])]
    public function rules(): array
    {
        return [
            'balance_id' => 'required|exists:crypto_balances,id',
            'to' => 'required|string|ethereum',
            'amount' => 'nullable|numeric|gt:0',
            'withdraw_all' => 'required|boolean',
            '2fa' => 'required|g2fa_install|g2fa',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function validated(): EthereumSendDto
    {
        return new EthereumSendDto(
            Auth::user(),
            $this->getFinanceService()
                ->findCryptoBalance((int)$this['balance_id']),
            $this['to'],
            $this->withdrawAmount,
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()
            ->after(function (Validator $validator) {
                if (count($validator->failed()) === 0) {
                    $this->after($validator);
                }
            });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @throws RequestException
     * @throws ValidationException
     */
    private function after(Validator $validator): void
    {
        if (!filter_var($this['withdraw_all'], FILTER_VALIDATE_BOOLEAN) &&
            (!isset($this['amount']) || (float)$this['amount'] <= 0)
        ) {
            $validator->errors()->add(
                'amount',
                'Must specify withdraw all or amount.'
            );

            return;
        }

        $balanceModel = $this->getFinanceService()
            ->findCryptoBalance((int)$this['balance_id']);

        $balance = $balanceModel->currency->isMainCurrencyInNetwork ?
            $this->getManageService()
                ->getBalance(
                    $balanceModel->currency->network->toEnum(),
                    $balanceModel->address->address,
                ) :
            $this->getManageService()
                ->getTokenBalance(
                    $balanceModel->currency->network->toEnum(),
                    $balanceModel->currency->address,
                    $balanceModel->address->address,
                );

        $balance = Decimal::create($balance->balance)
            ->div(Decimal::create(10)->pow(Decimal::create($balanceModel->currency->decimals)));

        $amount = Decimal::create(
            filter_var($this['withdraw_all'], FILTER_VALIDATE_BOOLEAN) ?
                $balance : (float)$this['amount']
        );

        if ($amount->isGreaterThan($balance)) {
            $validator->errors()->add(
                'amount',
                sprintf(
                    'You are trying to send amount %s (%s USD) that great then actual balance %s (%s USD)',
                    $amount->innerValue(),
                    $amount->mul(Decimal::create($balanceModel->currency->market->price))->innerValue(),
                    $balance->innerValue(),
                    $balance->mul(Decimal::create($balanceModel->currency->market->price))->innerValue()
                ),
            );

            return;
        }

        $this->withdrawAmount = filter_var($this['withdraw_all'], FILTER_VALIDATE_BOOLEAN) ?
            $amount :
            Decimal::create((float)$this['amount']);
    }

    /**
     * Get {@see ManageService::class}.
     *
     * @return ManageService
     */
    private function getManageService(): ManageService
    {
        return app(ManageService::class);
    }

    /**
     * Get {@see FinanceService::class}.
     *
     * @return FinanceService
     */
    private function getFinanceService(): FinanceService
    {
        return app(FinanceService::class);
    }

    /**
     * Get {@see DataGetterService::class}.
     *
     * @return DataGetterService
     */
    private function getDataGetterService(): DataGetterService
    {
        return app(DataGetterService::class);
    }
}
