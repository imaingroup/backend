<?php
declare(strict_types=1);

namespace App\Modules\Data\Exceptions;

/**
 * RouteResolveTargetException.
 */
final class RouteResolveTargetException extends DataException
{
}
