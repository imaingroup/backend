<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creating table settings_landing for store landing settings.
 */
class CreateSettingsLandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_landing', function (Blueprint $table) {
            $table->id();
            $table->longText('video_main');
            $table->text('trade_report_link');
            $table->longText('video_manifest');
            $table->text('telegram_link');
            $table->string('ito_title', 255);
            $table->text('ito_text');
            $table->text('what_is_abc_text');
            $table->text('bottom_text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_landing');
    }
}
