<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migration creating table rate_history.
 */
class CreateRateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedFloat('rate', 12, 4);
            $table->unsignedTinyInteger('provider');
            $table->dateTime('provider_at')->nullable(true);
            $table->boolean('is_error')->index();
            $table->timestamps();

            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_history');
    }
}
