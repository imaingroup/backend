<?php

namespace App\Modules\Notifications\Mails;

use App\Modules\Notifications\Mails\AbstractEmail;
use App\Modules\Notifications\Models\Mail;
use App\Modules\Withdraw\Models\Withdraw;
use function protectFromXss;

/**
 * Withdraw declined mail class.
 */
final class WithdrawDeclinedEmail extends AbstractEmail
{
    /** @var Withdraw Withdraw. */
    private Withdraw $withdraw;

    /**
     * Create a new mail instance.
     *
     * @param Withdraw $withdraw
     */
    public function __construct(Withdraw $withdraw)
    {
        $this->withdraw = $withdraw;
    }

    /**
     * {@inheritdoc}
     */
    protected function getVariables(): array
    {
        $user = $this->withdraw->user;

        return [
            '{username}' => protectFromXss($user->username),
            '{amount}' => $this->withdraw->amount_usdt,
            '{sitename}' => __('base.site_name')
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getMailId(): int
    {
        return Mail::MAIL_WITHDRAW_DECLINED;
    }
}
