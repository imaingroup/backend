<?php

namespace App\Modules\Investments\Requests;

use App\Modules\Investments\Models\TradeOption;
use App\Modules\Investments\Repositories\TradeOptionRepository;
use App\Modules\Investments\Services\InvestmentService;
use App\Modules\Settings\Repositories\SettingsInvestmentsRepository;
use App\Modules\Users\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * This class need for validation opening tid request.
 */
final class OpenTIDRequest extends FormRequest
{
    /** @var SettingsInvestmentsRepository Settings investments repository. */
    private SettingsInvestmentsRepository $settingsInvestmentsRepository;

    /** @var TradeOptionRepository Trade option repository. */
    private TradeOptionRepository $tradeOptionRepository;

    /** @var InvestmentService Investment service. */
    private InvestmentService $investmentService;

    /**
     * Constructor.
     *
     * @param \App\Modules\Settings\Repositories\SettingsInvestmentsRepository $settingsInvestmentsRepository
     * @param TradeOptionRepository $tradeOptionRepository
     * @param InvestmentService $investmentService
     */
    public function __construct(
        SettingsInvestmentsRepository $settingsInvestmentsRepository,
        TradeOptionRepository $tradeOptionRepository,
        InvestmentService $investmentService
    )
    {
        $this->settingsInvestmentsRepository = $settingsInvestmentsRepository;
        $this->tradeOptionRepository = $tradeOptionRepository;
        $this->investmentService = $investmentService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'option_id' => 'required|scalar|exists:trade_options,sid',
            'period' => 'required|integer',
            'amount' => 'required|integer',
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            $this->after($validator);
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     */
    public function after(Validator $validator): void
    {
        /** @var User $user */
        $user = Auth::user();
        /** @var TradeOption|null $option */
        $option = $this->tradeOptionRepository->findBySid((int)$this['option_id']);
        $period = (int)$this['period'];
        $amount = (int)$this['amount'];

        if (!$this->settingsInvestmentsRepository->get()->is_enabled_tid && !$user->isTidsAllowable) {
            $validator->errors()->add('global', 'TID open operation is temporarily unavailable');
            return;
        }

        if ($user->is_tid_block) {
            $validator->errors()->add('global', 'You can\'t open any TID');
            return;
        }

        if (!$option) {
            return;
        }

        if (!$option->is_visible) {
            $validator->errors()->add('global', 'You can\'t open this TID option');
            return;
        }

        if ($option->getPeriodMinByUser($user) > $period || $option->getPeriodMaxByUser($user) < $period) {
            $validator->errors()->add('global', 'Incorrect period');
            return;
        }

        if ($option->getAmountMinByUser($user) > $amount || $option->getAmountMaxByUser($user) < $amount) {
            $validator->errors()->add('global', 'Incorrect amount');
            return;
        }

        if ($this->investmentService->getOpeningLimit($user, $option) < 1) {
            $validator->errors()->add('global', 'Limit for opening TID for this option');
            return;
        }

        if ($user->balance_abc < $amount) {
            $validator->errors()->add('global', 'Insufficient IPRO funds');
            return;
        }

        if (!$user->checkAllowedOption($option)) {
            $validator->errors()->add('global', 'Please buy a trading card for activating TID');
            return;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validated(): array
    {
        $data = parent::validated();
        $data['option'] = $this->tradeOptionRepository->findBySid((int)$data['option_id']);
        $data['period'] = (int)$data['period'];
        $data['amount'] = (int)$data['amount'];

        return $data;
    }
}
