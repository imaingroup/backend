<?php
declare(strict_types=1);
/*
 * This file is part of the NTPClient package.
 *
 * (c) Krzysztof Mazur <krz@ychu.pl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Modules\Core\Libraries\NtpClient;

use App\Modules\Core\Libraries\NtpClient\Exceptions\ConnectionRefusedException;
use App\Modules\Core\Libraries\NtpClient\Exceptions\ConnectionTimeoutException;
use App\Modules\Core\Libraries\NtpClient\Exceptions\UnableToConnectException;
use DateTime;
use DateTimeZone;

/**
 * @author Krzysztof Mazur <krz@ychu.pl>
 */
interface NtpClient
{
    /**
     * @return int
     * @throws ConnectionRefusedException
     * @throws ConnectionTimeoutException
     * @throws UnableToConnectException
     */
    public function getUnixTime(): int;

    /**
     * @param DateTimeZone $timezone
     * @return DateTime
     * @throws ConnectionRefusedException
     * @throws ConnectionTimeoutException
     * @throws UnableToConnectException
     */
    public function getTime(DateTimeZone $timezone = null): DateTime;
}
