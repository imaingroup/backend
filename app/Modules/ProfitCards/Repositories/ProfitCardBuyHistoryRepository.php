<?php
declare(strict_types=1);

namespace App\Modules\ProfitCards\Repositories;

use App\Modules\ProfitCards\Enums\Models\ProfitCard\Type;
use App\Modules\ProfitCards\Models\ProfitCardBuyHistory;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Support\Collection;

/**
 * Profit card buy history repository.
 */
final class ProfitCardBuyHistoryRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param ProfitCardBuyHistory $model
     */
    public function __construct(protected ProfitCardBuyHistory $model)
    {}

    /**
     * Has condition that user bought card.
     *
     * @param int $userId
     * @param int $cardId
     * @return bool
     */
    public function hasBoughtById(int $userId, int $cardId): bool
    {
        return $this->model
            ->newQuery()
            ->where('users_id', $userId)
            ->where('profit_cards_id', $cardId)
            ->exists();
    }

    /**
     * Get sum of purchased cards for all period.
     *
     * @return float
     */
    public function getSumPurchasedCards(): float
    {
        return (float)$this->model
            ->newQuery()
            ->sum('abc');
    }

    /**
     * Get sum bonus of purchased cards for all period.
     *
     * @return float
     */
    public function getSumBonusPurchasedCards(): float
    {
        return (float)$this->model
            ->newQuery()
            ->sum('bonus');
    }

    /**
     * Get latest list.
     *
     * @param int $limit
     * @return Collection
     */
    public function getLatest(int $limit): Collection
    {
        return $this->model
            ->newQuery()
            ->orderBy('id', 'DESC')
            ->limit($limit)
            ->get();
    }

    /**
     * Get the latest by type.
     *
     * @param Type $type
     * @param int $limit
     * @return Collection
     */
    public function getLatestByType(Type $type, int $limit): Collection
    {
        return $this->model
            ->newQuery()
            ->select('profit_card_buy_histories.*')
            ->join('profit_cards as pc', 'profit_card_buy_histories.profit_cards_id', '=', 'pc.id')
            ->orderBy('profit_card_buy_histories.id', 'DESC')
            ->where('pc.type', $type->value)
            ->limit($limit)
            ->get();
    }
}
