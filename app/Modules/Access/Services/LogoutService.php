<?php


namespace App\Modules\Access\Services;


use Illuminate\Support\Facades\Auth;

/**
 * Logout service.
 */
final class LogoutService
{
    public function logout(): void
    {
        Auth::guard()->logout();
    }
}
