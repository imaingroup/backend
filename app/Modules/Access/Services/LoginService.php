<?php
declare(strict_types=1);

namespace App\Modules\Access\Services;

use App\Modules\Access\Exceptions\LoginException;
use App\Modules\Users\Models\User;
use App\Modules\Security\Models\UserIdentification;
use App\Modules\Core\Services\ClientInfoService;
use App\Modules\Security\Services\Identification\IdentificationService;
use DateTime;
use Illuminate\Support\Facades\Auth;

/**
 * Login service.
 */
final class LoginService
{
    /**
     * Constructor.
     *
     * @param \App\Modules\Core\Services\ClientInfoService $clientInfoService
     * @param IdentificationService $identificationService
     */
    public function __construct(
        private \App\Modules\Core\Services\ClientInfoService $clientInfoService,
        private IdentificationService $identificationService
    )
    {
    }

    /**
     * Login to system.
     *
     * @param string $email
     * @param string $password
     * @param bool $isRemember
     * @return \App\Modules\Users\Models\User
     * @throws LoginException
     */
    public function login(string $email, string $password, bool $isRemember): User
    {
        if (!Auth::attempt(['email' => $email, 'password' => $password], $isRemember)) {
            throw new LoginException('You are not authorized');
        }

        $user = Auth::user();

        $user->refresh();

        $user->last_ip = $this->clientInfoService->getRealIp();
        $user->last_activity_at = new DateTime();

        $user->save();
        $this->identificationService
            ->confirmLastIdentification(UserIdentification::ACTION_LOGIN_SUCCESS);

        return $user;
    }
}
