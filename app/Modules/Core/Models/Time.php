<?php
declare(strict_types=1);

namespace App\Modules\Core\Models;

use Carbon\Carbon;

/**
 * Time model.
 *
 * @property string $server
 * @property int $errors
 * @property Carbon $server_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
final class Time extends Model
{
    public const TYPE_SERVER_UK = '0.uk.pool.ntp.org';
    public const TYPE_SERVER_US = '0.north-america.pool.ntp.org';
    public const TYPE_SERVER_CF = 'time.cloudflare.com';

    public const SERVERS = [
        self::TYPE_SERVER_UK,
        self::TYPE_SERVER_US,
        self::TYPE_SERVER_CF,
    ];

    /** {@inheritdoc} */
    protected $primaryKey = 'server';

    /** {@inheritdoc} */
    protected $table = 'time';

    /** {@inheritdoc} */
    protected $fillable = [
        'server',
        'errors',
        'server_at',
    ];

    /** {@inheritdoc} */
    protected $casts = [
        'server' => 'string',
        'errors' => 'integer',
        'server_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
