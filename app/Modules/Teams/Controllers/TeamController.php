<?php


namespace App\Modules\Teams\Controllers;


use App\Modules\Core\Exceptions\DataException;
use App\Http\Controllers\Controller;
use App\Modules\Teams\Requests\TeamGetRequest;
use App\Modules\Core\Services\DataService;
use Illuminate\Support\Facades\Auth;

/**
 * Team controller.
 */
final class TeamController extends Controller
{
    /** @var DataService Data service. */
    private DataService $dataService;

    /**
     * Constructor.
     *
     * @param \App\Modules\Core\Services\DataService $dataService
     */
    public function __construct(DataService $dataService)
    {
        $this->dataService = $dataService;
    }

    /**
     * Get team data.
     *
     * @param TeamGetRequest $request
     * @return array
     * @throws \App\Modules\Core\Exceptions\DataException
     */
    public function get(TeamGetRequest $request): array
    {
        return $this->dataService->getData(DataService::TYPE_TEAM_GET, [Auth::user()]);
    }
}
