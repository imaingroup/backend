<?php
declare(strict_types=1);

namespace App\Modules\Investments\Repositories;

use App\Modules\Investments\Models\TradeOption;
use App\Modules\Core\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Trade option repository.
 */
final class TradeOptionRepository extends BaseRepository
{
    /**
     * Constructor.
     *
     * @param TradeOption $model
     */
    public function __construct(protected TradeOption $model)
    {}

    /**
     * Get active options.
     *
     * @return Collection
     */
    public function getActive(): Collection
    {
        return $this->model
            ->newQuery()
            ->where('is_visible', true)
            ->orderBy('sort', 'ASC')
            ->get();
    }

    /**
     * Get by required card.
     *
     * @param int $cardId
     * @return Collection
     */
    public function getByRequiredCard(int $cardId): Collection
    {
        return $this->model
            ->newQuery()
            ->whereHas('requiredCards', function (Builder $query) use($cardId) {
                $query->where('profit_card_id', $cardId);
            })
            ->orderBy('sort', 'ASC')
            ->get();
    }

    /**
     * Get option by index.
     *
     * @param int $index
     * @return \App\Modules\Investments\Models\TradeOption|null
     */
    public function getByIndex(int $index): ?TradeOption
    {
        return $this->model
            ->newQuery()
            ->orderBy('id', 'ASC')
            ->offset($index)
            ->limit(1)
            ->first();
    }

    /**
     * Get active option by id.
     *
     * @param int $tradeOptionId
     * @return \App\Modules\Investments\Models\TradeOption|null
     */
    public function getActiveById(int $tradeOptionId): ?TradeOption
    {
        return $this->model
            ->newQuery()
            ->where('id', $tradeOptionId)
            ->where('is_visible', true)
            ->limit(1)
            ->first();
    }

    /**
     * Get active option by id.
     *
     * @param int $tradeOptionId
     * @return \App\Modules\Investments\Models\TradeOption|null
     */
    public function getWithChartEnabledById(int $tradeOptionId): ?TradeOption
    {
        return $this->model
            ->newQuery()
            ->where('id', $tradeOptionId)
            ->where('chart_enabled', true)
            ->limit(1)
            ->first();
    }
}
