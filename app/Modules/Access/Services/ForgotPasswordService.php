<?php
declare(strict_types=1);

namespace App\Modules\Access\Services;

use App\Modules\Notifications\Jobs\SendResetPasswordLInkEmailJob;
use App\Modules\Users\Models\User;
use App\Modules\Security\Models\UserIdentification;
use App\Modules\Security\Services\Identification\IdentificationService;
use Illuminate\Support\Str;

/**
 * Forgot password service.
 */
final class ForgotPasswordService
{
    /**
     * Constructor.
     *
     * @param IdentificationService $identificationService
     */
    public function __construct(private IdentificationService $identificationService)
    {
    }

    /**
     * Send reset password link.
     *
     * @param User $user
     */
    public function send(User $user): void
    {
        $user->refresh();

        $user->reset_token = Str::random(60);
        $user->reset_token_valid_at = (new \DateTime())->modify('+ 1 DAY');

        $user->save();

        SendResetPasswordLInkEmailJob::dispatch($user);

        $this->identificationService
            ->confirmLastIdentification(UserIdentification::ACTION_FORGOT_SUCCESS);
    }
}
