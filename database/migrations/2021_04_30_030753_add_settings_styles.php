<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Добавить настройки стилей.
 */
final class AddSettingsStyles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('settings_styles', function (Blueprint $table) {
            $table->id();
            $table->boolean('enable_custom_body_css');
            $table->string('body_cls_name')
                ->nullable(true);
            $table->text('body_css')
                ->nullable(true);

            $table->boolean('enable_custom_header_css');
            $table->string('header_cls_name')
                ->nullable(true);
            $table->text('header_css')
                ->nullable(true);

            $table->boolean('enable_custom_sidebar_css');
            $table->string('sidebar_cls_name')
                ->nullable(true);
            $table->text('sidebar_css')
                ->nullable(true);

            $table->boolean('enable_custom_buttons_css');
            $table->string('buttons_cls_name')
                ->nullable(true);
            $table->text('buttons_css')
                ->nullable(true);
            $table->timestamps();
        });

        DB::transaction(function() {
            DB::table('settings_styles')
                ->insert([
                    'enable_custom_body_css' => false,
                    'enable_custom_header_css' => false,
                    'enable_custom_sidebar_css' => false,
                    'enable_custom_buttons_css' => false,
                ]);

            $id = DB::table('data_types')
                ->insertGetId([
                    'name' => 'settings_styles',
                    'slug' => 'settings-styles',
                    'display_name_singular' => 'STYLES',
                    'display_name_plural' => 'STYLES',
                    'icon' => 'voyager-settings',
                    'model_name' => 'App\Modules\Settings\Models\SettingsStyles',
                    'generate_permissions' => 1,
                    'server_side' => 0,
                    'details' => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}',
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'enable_custom_body_css',
                    'type' => 'checkbox',
                    'display_name' => 'Enable custom body css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 0
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'body_cls_name',
                    'type' => 'text',
                    'display_name' => 'Body class css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:255"}',
                    'order' => 1
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'body_css',
                    'type' => 'text_area',
                    'display_name' => 'Body css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:65535"}',
                    'order' => 2
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'enable_custom_header_css',
                    'type' => 'checkbox',
                    'display_name' => 'Enable custom header css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 3
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'header_cls_name',
                    'type' => 'text',
                    'display_name' => 'Header class css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:255"}',
                    'order' => 4
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'header_css',
                    'type' => 'text_area',
                    'display_name' => 'Header css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:65535"}',
                    'order' => 5
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'enable_custom_sidebar_css',
                    'type' => 'checkbox',
                    'display_name' => 'Enable custom sidebar css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 6
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'sidebar_cls_name',
                    'type' => 'text',
                    'display_name' => 'Sidebar class css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:255"}',
                    'order' => 7
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'sidebar_css',
                    'type' => 'text_area',
                    'display_name' => 'Sidebar css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:65535"}',
                    'order' => 8
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'enable_custom_buttons_css',
                    'type' => 'checkbox',
                    'display_name' => 'Enable custom button css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 9
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'buttons_cls_name',
                    'type' => 'text',
                    'display_name' => 'Buttons class css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:255"}',
                    'order' => 10
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $id,
                    'field' => 'buttons_css',
                    'type' => 'text_area',
                    'display_name' => 'Buttons css',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"nullable|scalar|max:65535"}',
                    'order' => 11
                ]);

            $permissions = ['browse', 'read', 'edit', 'add', 'delete'];

            foreach ($permissions as $permission) {
                $pId = DB::table('permissions')
                    ->insertGetId([
                        'key' => sprintf('%s_settings_styles', $permission),
                        'table_name' => 'settings_styles',
                        'created_at' => new DateTime(),
                        'updated_at' => new DateTime()
                    ]);

                if ($permission === 'edit') {
                    DB::table('permission_role')
                        ->insert([
                            'permission_id' => $pId,
                            'role_id' => 1
                        ]);
                }
            }

            DB::table('menu_items')
                ->insert([
                    'menu_id' => 1,
                    'title' => 'STYLES',
                    'url' => '/XI400PGNB/settings-styles/1/edit',
                    'target' => '_self',
                    'icon_class' => 'voyager-settings',
                    'color' => '#000000',
                    'parent_id' => 22,
                    'order' => 8,
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('settings_styles');

        DB::transaction(function() {
            DB::table('data_types')
                ->where('name', 'settings_styles')
                ->delete();

            DB::table('permissions')
                ->where('table_name', 'settings_styles')
                ->delete();

            DB::table('menu_items')
                ->where('title', 'STYLES')
                ->where('url', '/XI400PGNB/settings-styles/1/edit')
                ->delete();
        });
    }
}
