<?php
declare(strict_types=1);

namespace App\Modules\Admin\Controllers;

use App\Modules\Admin\Requests\Users\AdminChangeBalanceRequest;
use App\Modules\Admin\Requests\Users\AdminChangePasswordRequest;
use App\Modules\Core\Dto\ResponseDto;
use App\Modules\Core\Exceptions\TransactionNotStarted;
use App\Modules\Investments\Models\TradeOption;
use App\Modules\Logs\Loggers\Logger;
use App\Modules\Money\Models\Transaction;
use App\Modules\Security\Services\Identification\GetterService;
use App\Modules\Users\Models\User;
use App\Modules\Admin\Requests\Users\AdminChangePromoCodeRequest;
use App\Modules\Admin\Requests\Users\AdminIpInfoRequest;
use App\Modules\Admin\Requests\Users\AdminMoveTeammatesRequest;
use App\Modules\Admin\Requests\Users\AdminTransferAbcRequest;
use App\Modules\Investments\Repositories\TradeOptionRepository;
use App\Modules\Investments\Services\InvestmentService;
use App\Modules\Security\Services\Identification\IdentificationService;
use App\Modules\Users\Models\UserCustomTidOptions;
use App\Modules\Users\Repositories\UserRepository;
use App\Modules\Core\Services\GeoIpService;
use App\Modules\Money\Services\MoneyTransferService;
use App\Modules\PromoCodes\Services\PromoCodeService;
use App\Modules\Users\Services\UserService;
use Closure;
use GeoIp2\Exception\AddressNotFoundException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use JetBrains\PhpStorm\ArrayShape;
use MaxMind\Db\Reader\InvalidDatabaseException;
use PragmaRX\Google2FALaravel\Google2FA;
use TCG\Voyager\Facades\Voyager;
use Throwable;

/**
 * Voyager user controller.
 */
final class VoyagerUsersController extends VoyagerBaseController
{
    private const SEARCH_FIELDS = [
        'id' => 'ID',
        'email' => 'Email',
        'username' => 'Username',
        'promo_code' => 'Promo code',
    ];

    /**
     * Constructor.
     *
     * @param PromoCodeService $promoCodeService
     * @param UserRepository $userRepository
     * @param MoneyTransferService $moneyTransferService
     * @param UserService $userService
     * @param TradeOptionRepository $tradeOptionRepository
     * @param InvestmentService $investmentService
     * @param IdentificationService $identificationService
     * @param GeoIpService $geoIpService
     * @param Logger $logger
     */
    public function __construct(
        private PromoCodeService      $promoCodeService,
        private UserRepository        $userRepository,
        private MoneyTransferService  $moneyTransferService,
        private UserService           $userService,
        private TradeOptionRepository $tradeOptionRepository,
        private InvestmentService     $investmentService,
        private IdentificationService $identificationService,
        private GeoIpService          $geoIpService,
        private Logger                $logger
    )
    {
        parent::__construct($logger);
    }

    /**
     * Index request.
     *
     * @param Request $request
     * @return View
     * @throws AuthorizationException
     */
    public function index(Request $request): View
    {
        /** @var View $view */
        $view = parent::index($request);

        $view->with('searchFields', self::SEARCH_FIELDS);

        return $view;
    }

    /**
     * Edit user data.
     *
     * @param Request $request
     * @param $id
     * @return View
     */
    public function edit(Request $request, $id): View
    {
        /** @var View $view */
        $view = parent::edit($request, $id);
        $view->with('options', $this->tradeOptionRepository->all());

        return $view;
    }

    /**
     * Profile page.
     *
     * @param Request $request
     * @return View
     */
    public function profile(Request $request): View
    {
        $route = '';
        $dataType = Voyager::model('DataType')->where('model_name', Auth::guard(app('VoyagerGuard'))->getProvider()->getModel())->first();
        if (!$dataType && app('VoyagerGuard') == 'web') {
            $route = route('voyager.users.edit', Auth::user()->getKey());
        } elseif ($dataType) {
            $route = route('voyager.' . $dataType->slug . '.edit', Auth::user()->getKey());
        }

        return Voyager::view('voyager::profile', compact('route'));
    }

    /**
     * Update user data.
     *
     * @param Request $request
     * @param $id
     * @param Closure|null $extend
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id, Closure $extend = null): RedirectResponse
    {
        if (Auth::user()->getKey() == $id) {
            $request->merge([
                'role_id' => Auth::user()->role_id,
                'user_belongstomany_role_relationship' => Auth::user()->roles->pluck('id')->toArray(),
            ]);
        }

        return parent::update($request, $id, function (Request $request, User $user) {
            $this->updateInvestmentsLimits($request, $user);
            $this->updateCustomTidOptions($request, $user);
        });
    }

    /**
     * Transfer abc from user to user.
     *
     * @param \App\Modules\Admin\Requests\Users\AdminTransferAbcRequest $request
     * @return Response
     */
    public function transferAbc(AdminTransferAbcRequest $request): Response
    {
        $validated = $request->validated();

        try {
            DB::beginTransaction();

            $this->moneyTransferService->sendAbc(
                $validated['sender'],
                $validated['recipient'],
                Auth::user(),
                $validated['transfer_amount']
            );

            DB::commit();

            return response(['status' => true], 200);
        } catch (Throwable $e) {
            DB::rollBack();

            $this->logger->rollbackTransaction($e, $validated);

            return response(['status' => false], 503);
        }
    }

    #[ArrayShape(['status' => "bool", 'google2fa_secret' => "string", 'qr_image' => "string"])]
    public function google2faInit(Request $request): array
    {
        /** @var Google2FA $google2fa */
        $google2fa = app('pragmarx.google2fa');
        $google2faSecret = $google2fa->generateSecretKey();

        $request->session()->put('google2fa_secret', $google2faSecret);

        $qrImage = $google2fa->getQRCodeInline(
            config('app.name'),
            Auth::user()->email,
            $google2faSecret
        );

        return [
            'status' => true,
            'google2fa_secret' => $google2faSecret,
            'qr_image' => $qrImage
        ];
    }

    public function google2faConfirm(Request $request): array
    {
        if (!$request->session()->has('google2fa_secret')) {
            return [
                'status' => false
            ];
        }

        $this->userService->update2fa(Auth::user(), $request->session()->get('google2fa_secret'));

        return [
            'status' => true
        ];
    }

    #[ArrayShape(['status' => "bool"])]
    public function passwordChange(User $user, AdminChangePasswordRequest $request): array
    {
        $validated = $request->validated();
        $this->userService->changePassword($user, $validated['password']);

        return ['status' => true];
    }

    public function balanceChange(User $user, AdminChangeBalanceRequest $request): Response
    {
        $validated = $request->validated();

        try {
            DB::beginTransaction();

            $transaction = $this->moneyTransferService->moneyOperation(
                $user,
                Auth::user(),
                $validated['change_balance_amount'],
                $validated['currency']
            );

            if ($validated['currency'] === Transaction::CURRENCY_USDT_DEPOSITED
                && !$validated['change_balance_commission_free']
                && $validated['change_balance_amount'] > 0
            ) {
                $this->promoCodeService->addCommissionAfterTransaction(
                    $transaction,
                    Transaction::TYPE_REFERRAL_COMMISSION_DEPOSIT_BY_ADMIN
                );
            }

            DB::commit();

            return response(['status' => true], 200);
        } catch (\Exception $e) {
            DB::rollBack();

            $this->logger->rollbackTransaction($e, $validated);

            return response(['status' => false], 503);
        }
    }

    /**
     * Change promo code.
     *
     * @param User $user
     * @param AdminChangePromoCodeRequest $request
     * @return Response
     */
    public function promoCodeChange(User $user, AdminChangePromoCodeRequest $request): Response
    {
        try {
            DB::beginTransaction();

            $this->userService->changePromoCode($user);

            DB::commit();

            return response(['data' => [
                'promo_code' => $user->promo_code
            ]]);
        } catch (Throwable $e) {
            DB::rollBack();

            $this->logger->rollbackTransaction($e, $request->all());

            return response(['status' => false], 503);
        }
    }

    /**
     * Move teammates.
     *
     * @param AdminMoveTeammatesRequest $request
     * @return Response
     */
    public function moveTeammates(AdminMoveTeammatesRequest $request): Response
    {
        try {
            DB::beginTransaction();

            $this->userService->moveTeammates(...$request->validated());

            DB::commit();

            return response(['status' => true]);
        } catch (Throwable $e) {
            DB::rollBack();

            $this->logger->rollbackTransaction($e, $request->all());

            return response(['status' => false], 503);
        }
    }

    /**
     * Get identifications data by user.
     *
     * @param User $user
     * @return ResponseDto
     * @throws Throwable
     */
    public function getIdentifications(User $user): ResponseDto
    {
        return new ResponseDto(
            $this->getGetterService()
                ->getHistory($user)
        );
    }

    /**
     * Get identifications data by user.
     *
     * @param AdminIpInfoRequest $request
     * @return View
     * @throws InvalidDatabaseException
     */
    #[ArrayShape(['data' => "array"])]
    public function ipInfo(AdminIpInfoRequest $request): View
    {
        try {
            $ip = $request->validated()['ip'];

            return view(
                'vendor.voyager.users.ip_info',
                [
                    'ip' => $ip,
                    'geo' => $this->geoIpService->getByIp($ip)
                ]
            );
        } catch (AddressNotFoundException) {
            abort(404);
        }
    }

    /**
     * Update investments limits after save user data.
     *
     * @param Request $request
     * @param User $user
     * @throws TransactionNotStarted
     */
    private function updateInvestmentsLimits(Request $request, User $user)
    {
        $options = $this->tradeOptionRepository->all();
        $allowedOptions = [];

        foreach ((array)$request->get('is_active_option') as $option => $value) {
            /** @var TradeOption $optionModel */
            $optionModel = $this->tradeOptionRepository->find($option);
            if ($optionModel) {
                $allowedOptions[] = $optionModel->id;

                $this->investmentService->enableTid($user, $optionModel);
            }
        }

        foreach ($options as $option) {
            if (!in_array($option->id, $allowedOptions)) {
                $this->investmentService->disableTid($user, $option);
            }
        }

        //tid's expanses
        foreach ((array)$request->get('options_expanses') as $option => $value) {
            /** @var TradeOption $optionModel */
            $optionModel = $this->tradeOptionRepository->find($option);

            if ($optionModel && is_scalar($value)) {
                $this->investmentService->applyTidExpanse(
                    $user,
                    $optionModel,
                    (int)$value,
                    false
                );
            }
        }
    }

    /**
     * Update investments limits after save user data.
     *
     * @param Request $request
     * @param User $user
     */
    private function updateCustomTidOptions(Request $request, User $user)
    {
        foreach ((array)$request->get('custom_tids') as $option => $data) {
            /** @var TradeOption $optionModel */
            $optionModel = $this->tradeOptionRepository->find((int)$option);
            if ($optionModel) {
                if (!Validator::make($data, [
                    'title' => 'required|scalar|max:255',
                    'period_min' => 'required|integer|gte:0',
                    'period_max' => 'required|integer|gt:period_min',
                    'amount_min' => 'required|integer|gte:0',
                    'amount_max' => 'required|integer|gt:amount_min',
                ])->fails()) {
                    $customTidOption = $user->getCustomTidOptionsByOption($optionModel);

                    if (!$customTidOption) {
                        $customTidOption = new UserCustomTidOptions();
                        $customTidOption->user()
                            ->associate($user);
                        $customTidOption->tradeOption()
                            ->associate($optionModel);
                    }

                    $customTidOption->title = $data['title'];
                    $customTidOption->period_min = $data['period_min'];
                    $customTidOption->period_max = $data['period_max'];
                    $customTidOption->amount_min = $data['amount_min'];
                    $customTidOption->amount_max = $data['amount_max'];
                    $customTidOption->save();
                }
            }
        }
    }

    /**
     * Get {@see GetterService::class}.
     *
     * @return GetterService
     */
    private function getGetterService(): GetterService
    {
        return app(GetterService::class);
    }
}
