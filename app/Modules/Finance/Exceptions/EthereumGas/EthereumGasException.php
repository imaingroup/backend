<?php
declare(strict_types=1);

namespace App\Modules\Finance\Exceptions\EthereumGas;


use App\Modules\Finance\Exceptions\FinanceException;

/**
 * EthereumGasException.
 */
abstract class EthereumGasException extends FinanceException
{
}
