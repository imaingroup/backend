<?php

namespace App\Modules\News\Models;

use App\Modules\Core\Models\Model;
use App\Modules\Languages\Models\Lang;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;
use Throwable;

final class Video extends Model
{
    /**
     * Events for model.
     */
    protected static function booted()
    {
        static::creating(function (Video $model) {
            $model->sid = mt_rand(100000000000, 999999999999);
        });
    }

    /**
     * Get video manifest.
     *
     * @return array|null
     */
    public function getVideo(): ?array
    {
        return $this->formatVideoField('file');
    }

    /**
     * Get formatted video field.
     *
     * @param string $field
     * @return array|null
     */
    private function formatVideoField(string $field): ?array
    {
        try {
            $json = \json_decode($this->$field, true, 3, JSON_THROW_ON_ERROR);

            if(count($json) > 0) {
                $data = $json[0];
                $data['download_link'] = Storage::disk('local')->url($data['download_link']);

                return $data;
            }
        }
        catch (Throwable $e) {}

        return null;
    }

    /**
     * Get relation with Lang
     *
     * @return BelongsTo
     */
    public function lang(): BelongsTo
    {
        return $this->belongsTo(Lang::class, 'langs_id');
    }
}
