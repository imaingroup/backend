<?php

use App\Modules\ProfitCards\Models\ProfitCardBuyHistory;
use App\Modules\Money\Models\Transaction;
use App\Modules\Users\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * This migrations adds team fields to users table.
 */
class AddTeamFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->float('total_deposited_team', 16, 8)->default(0);
            $table->float('total_invested_team', 16, 8)->default(0);
            $table->float('earned_commission_abc', 16, 8)->default(0);
            $table->float('earned_commission_usdt', 16, 8)->default(0);
            $table->unsignedInteger('team_quantity')->default(0);
        });

        foreach (User::all() as $user) {
            $user->total_deposited_team = $user->total_deposited;
            $user->total_invested_team = ProfitCardBuyHistory::query()
                ->where('users_id', $user->id)
                ->sum('price');
            $user->earned_commission_abc = Transaction::query()
                ->whereIn('type', [Transaction::TYPE_REFERRAL_COMMISSION_BUY_PROFIT_CARD])
                ->where('users_id', $user->id)
                ->sum('amount');
            $user->earned_commission_usdt = Transaction::query()
                ->whereIn('type', [Transaction::TYPE_REFERRAL_COMMISSION_DEPOSIT, Transaction::TYPE_REFERRAL_COMMISSION_DEPOSIT_BY_ADMIN])
                ->where('users_id', $user->id)
                ->sum('amount');
            $user->team_quantity = User::query()
                ->where('team_member_id', $user->id)
                ->count();
            $user->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('total_deposited_team');
            $table->dropColumn('total_invested_team');
            $table->dropColumn('earned_commission_abc');
            $table->dropColumn('earned_commission_usdt');
            $table->dropColumn('team_quantity');
        });
    }
}
