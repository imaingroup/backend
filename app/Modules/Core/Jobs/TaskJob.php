<?php

namespace App\Modules\Core\Jobs;

use App\Modules\Core\Models\Task;
use App\Modules\Core\Services\TaskService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;

/**
 * Job task.
 */
final class TaskJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Task Task model. */
    private Task $task;

    /**
     * Constructor.
     *
     * @param Task $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * Execute the job.
     *
     * @param TaskService $taskService
     * @return void
     * @throws Throwable
     */
    public function handle(TaskService $taskService)
    {
        $taskService->handleTask($this->task);
    }
}
