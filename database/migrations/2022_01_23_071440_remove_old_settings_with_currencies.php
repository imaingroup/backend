<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * RemoveOldSettingsWithCurrencies.
 */
final class RemoveOldSettingsWithCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->dropColumn([
                'btc_usdt_rate',
                'eth_usdt_rate',
                'usdt_usdt_rate',
                'rate_updated_at',
                'eth_rate_updated_at',
                'usdt_rate_updated_at',
                'is_disable_auto_update_rate',
                'luft_deposit_btc_price',
                'luft_deposit_eth_price',
                'luft_deposit_usdt_price',
                'luft_withdraw_btc_price',
                'luft_withdraw_eth_price',
                'luft_withdraw_usdt_price',
            ]);
        });

        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->dropColumn([
                'deposit_min_usdt',
                'deposit_min_busdt',
                'deposit_min_btc',
                'deposit_min_eth',
                'is_block_usdt',
                'is_block_busdt',
                'is_block_btc',
                'is_block_eth',
            ]);
        });

        DB::transaction(function () {
            DB::table('data_rows')
                ->where(
                    'data_type_id',
                    DB::table('data_types')
                        ->where('name', 'settings_prices')
                        ->first()
                        ->id
                )
                ->whereIn('field', [
                    'btc_usdt_rate',
                    'eth_usdt_rate',
                    'usdt_usdt_rate',
                    'rate_updated_at',
                    'eth_rate_updated_at',
                    'usdt_rate_updated_at',
                    'is_disable_auto_update_rate',
                    'luft_deposit_btc_price',
                    'luft_deposit_eth_price',
                    'luft_deposit_usdt_price',
                    'luft_withdraw_btc_price',
                    'luft_withdraw_eth_price',
                    'luft_withdraw_usdt_price',
                ])
                ->delete();

            DB::table('data_rows')
                ->where(
                    'data_type_id',
                    DB::table('data_types')
                        ->where('name', 'settings_deposit')
                        ->first()
                        ->id
                )
                ->whereIn('field', [
                    'deposit_min_usdt',
                    'deposit_min_busdt',
                    'deposit_min_btc',
                    'deposit_min_eth',
                    'is_block_usdt',
                    'is_block_busdt',
                    'is_block_btc',
                    'is_block_eth',
                ])
                ->delete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('settings_prices', function (Blueprint $table) {
            $table->float('luft_withdraw_btc_price', 16, 8)
                ->after('abc_price')
                ->default(0);
            $table->float('luft_deposit_btc_price', 16, 8)
                ->after('abc_price')
                ->default(0);
            $table->dateTime('rate_updated_at')
                ->after('abc_price')
                ->default((new DateTime())->format('Y-m-d H:i:s'));
            $table->float('btc_usdt_rate', 12, 4)
                ->after('abc_price')
                ->default(0);
        });

        Schema::table('settings_prices', function (Blueprint $table) {
            $table->boolean('is_disable_auto_update_rate')
                ->after('rate_updated_at')
                ->default(0);
        });

        Schema::table('settings_prices', function (Blueprint $table) {
            $table->float('eth_usdt_rate', 16, 8)
                ->after('btc_usdt_rate')
                ->default(1500);
            $table->dateTime('eth_rate_updated_at')
                ->after('rate_updated_at')
                ->default((new DateTime())->format('Y-m-d H:i:s'));
            $table->float('luft_deposit_eth_price')
                ->default(0)
                ->after('luft_deposit_btc_price');
            $table->float('luft_withdraw_eth_price')
                ->default(0)
                ->after('luft_withdraw_btc_price');
        });

        Schema::table('settings_prices', function (Blueprint $table) {
            $table->float('usdt_usdt_rate', 16, 8)
                ->after('eth_usdt_rate')
                ->default(1);
            $table->dateTime('usdt_rate_updated_at')
                ->after('eth_rate_updated_at')
                ->default((new DateTime())->format('Y-m-d H:i:s'));
            $table->float('luft_deposit_usdt_price')
                ->default(0)
                ->after('luft_deposit_eth_price');
            $table->float('luft_withdraw_usdt_price')
                ->default(0)
                ->after('luft_withdraw_eth_price');
        });

        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->float('deposit_min_usdt', 16, 8)
                ->after('id')
                ->default(50);
        });

        Schema::table('settings_deposit', function (Blueprint $table) {
            $table->float('deposit_min_busdt', 16, 8)
                ->after('deposit_min_usdt')
                ->default(50);

            $table->float('deposit_min_btc', 16, 8)
                ->after('deposit_min_usdt')
                ->default(10);

            $table->float('deposit_min_eth', 16, 8)
                ->after('deposit_min_btc')
                ->default(10);

            $table->boolean('is_block_usdt')
                ->default(false)
                ->after('is_deposit_block');

            $table->boolean('is_block_busdt')
                ->default(false)
                ->after('is_block_usdt');

            $table->boolean('is_block_btc')
                ->default(false)
                ->after('is_deposit_block');

            $table->boolean('is_block_eth')
                ->default(false)
                ->after('is_deposit_block');
        });

        DB::transaction(function() {
            $typeId = DB::table('data_types')
                ->where('name', 'settings_prices')
                ->first()
                ->id;

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'btc_usdt_rate',
                    'type' => 'number',
                    'display_name' => 'Btc Usdt Rate',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 3
                ]);
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'rate_updated_at',
                    'type' => 'timestamp',
                    'display_name' => 'Rate Updated At',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|date"}}',
                    'order' => 4
                ]);
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'luft_deposit_btc_price',
                    'type' => 'number',
                    'display_name' => 'Luft Deposit Btc Price',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 6
                ]);
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'luft_withdraw_btc_price',
                    'type' => 'number',
                    'display_name' => 'Luft Withdraw Btc Price',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 7
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'eth_usdt_rate',
                    'type' => 'number',
                    'display_name' => 'Eth Usdt Rate',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 3
                ]);
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'eth_rate_updated_at',
                    'type' => 'timestamp',
                    'display_name' => 'Eth Rate Updated At',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|date"}}',
                    'order' => 4
                ]);
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'luft_deposit_eth_price',
                    'type' => 'number',
                    'display_name' => 'Luft Deposit Eth Price',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 6
                ]);
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'luft_withdraw_eth_price',
                    'type' => 'number',
                    'display_name' => 'Luft Withdraw Eth Price',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 7
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'usdt_usdt_rate',
                    'type' => 'number',
                    'display_name' => 'Tether usdt Rate',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 3
                ]);
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'usdt_rate_updated_at',
                    'type' => 'timestamp',
                    'display_name' => 'Usdt Rate Updated At',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|date"}}',
                    'order' => 4
                ]);
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'luft_deposit_usdt_price',
                    'type' => 'number',
                    'display_name' => 'Luft Deposit Usdt Price',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 6
                ]);
            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'luft_withdraw_usdt_price',
                    'type' => 'number',
                    'display_name' => 'Luft Withdraw Usdt Price',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 7
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeId,
                    'field' => 'is_disable_auto_update_rate',
                    'type' => 'checkbox',
                    'display_name' => 'Is Disable Auto Update Rate',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 5
                ]);

            $typeSettingsDepositId = DB::table('data_types')
                ->where('name', 'settings_deposit')
                ->first()
                ->id;

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeSettingsDepositId,
                    'field' => 'deposit_min_usdt',
                    'type' => 'number',
                    'display_name' => 'Deposit min usdt',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 1
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeSettingsDepositId,
                    'field' => 'is_block_usdt',
                    'type' => 'checkbox',
                    'display_name' => 'USDT block',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 1
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeSettingsDepositId,
                    'field' => 'deposit_min_busdt',
                    'type' => 'number',
                    'display_name' => 'Deposit min busdt',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 0
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeSettingsDepositId,
                    'field' => 'is_block_busdt',
                    'type' => 'checkbox',
                    'display_name' => 'BUSDT block',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 3
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeSettingsDepositId,
                    'field' => 'deposit_min_btc',
                    'type' => 'number',
                    'display_name' => 'Deposit min btc',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 0
                ]);


            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeSettingsDepositId,
                    'field' => 'is_block_btc',
                    'type' => 'checkbox',
                    'display_name' => 'BTC block',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 1
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeSettingsDepositId,
                    'field' => 'deposit_min_eth',
                    'type' => 'number',
                    'display_name' => 'Deposit min eth',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{"validation":{"rule":"required|numeric"}}',
                    'order' => 0
                ]);

            DB::table('data_rows')
                ->insert([
                    'data_type_id' => $typeSettingsDepositId,
                    'field' => 'is_block_eth',
                    'type' => 'checkbox',
                    'display_name' => 'ETH block',
                    'required' => 0,
                    'browse' => 0,
                    'read' => 0,
                    'edit' => 1,
                    'add' => 0,
                    'delete' => 0,
                    'details' => '{}',
                    'order' => 1
                ]);
        });
    }
}
