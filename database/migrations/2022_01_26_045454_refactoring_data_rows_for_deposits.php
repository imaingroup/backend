<?php
declare(strict_types=1);

use App\Facades\DB;
use Illuminate\Database\Migrations\Migration;

/**
 * RefactoringDataRowsForDeposits.
 */
final class RefactoringDataRowsForDeposits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::transaction(function () {
            DB::table('data_rows')
                ->where(
                    'data_type_id',
                    DB::table('data_types')
                        ->where('name', 'deposits')
                        ->first()
                        ->id
                )
                ->where('field', 'type_coin')
                ->update([
                    'field' => 'currency_full_name',
                    'type' => 'text',
                    'details' => '{}',
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::transaction(function () {
            DB::table('data_rows')
                ->where(
                    'data_type_id',
                    DB::table('data_types')
                        ->where('name', 'deposits')
                        ->first()
                        ->id
                )
                ->where('field', 'currency_full_name')
                ->update([
                    'field' => 'type_coin',
                    'type' => 'select_dropdown',
                    'details' => '{"validation":{"rule":"required|min:1|max:5"}}',
                ]);
        });
    }
}
