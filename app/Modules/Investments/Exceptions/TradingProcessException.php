<?php
declare(strict_types=1);

namespace App\Modules\Investments\Exceptions;

/**
 * TradingProcessException.
 */
final class TradingProcessException extends InvestmentException
{
}
